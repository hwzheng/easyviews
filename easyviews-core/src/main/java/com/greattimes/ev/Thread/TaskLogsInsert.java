package com.greattimes.ev.Thread;


import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.service.IDataManageService;
import com.greattimes.ev.utils.SpringUtils;
import com.greattimes.ev.utils.StaticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import java.util.Date;
import java.util.Map;

public class TaskLogsInsert implements Runnable {
	
	Logger log = LoggerFactory.getLogger(TaskLogsInsert.class);
	
	public static Map<String,Integer> taskIdMap = null;

	@Override
	public void run() {
		log.info("定时任务日志插入线程启动");
		boolean running = true;
		while(running){
			try {
				TaskLogs taskLogs = StaticUtil.taskLogsQueue.take();
				IDataManageService dataManageService = SpringUtils.getBean("dataManageService");
				if(taskIdMap == null || taskIdMap.size()==0){
					//初始化timerIdMap
					taskIdMap = dataManageService.selectTaskIdMap();
				}
				Integer taskId = taskIdMap.get(taskLogs.getClassName());
				taskLogs.setTaskId(taskId);
				TaskLogs tl = new TaskLogs();
				BeanUtils.copyProperties(taskLogs, tl);
				tl.setInsertTime(new Date());
				taskLogs.setInsertTime(new Date());
				dataManageService.insertTaskLogs(taskLogs);
				log.info("任务日志插入成功！");
			} catch (Exception e) {
				e.printStackTrace();
				log.error("任务日志插入异常："+e.getMessage());
				continue;
			}
		}
		log.info("定时任务日志插入线程停止");
	}
}
