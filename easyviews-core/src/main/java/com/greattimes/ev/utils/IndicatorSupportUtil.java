package com.greattimes.ev.utils;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.Indicator;
import com.greattimes.ev.indicator.param.req.IndicatorIntervalParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class IndicatorSupportUtil {

    /**
     * 指标表名
     */
    private static final String T_BPMTRAN = "bpmTran";
    private static final String VM_TRANSACTION_INDICATOR = "vm_transaction_indicator";
    /**
     * 组件维度表名
     */
    private static final String TABLE_FIX_BPM = "bpmPre";
    private static final String VIEW_VM_BPM = "vm_transaction_pre";
    /**
     * 事件指标(事件+统计维度)
     */
    private static final String T_BPM_EXTEND_TRAN = "bpmExtendTran";
    private static final String VM_EXTEND_INDICATOR = "vm_extend_indicator";
    /**
     * 事件维度(all 统计维度+分析维度)
     */
    private static final String TABLE_FIX_EXTEND = "extendPre";
    private static final String VIEW_VM_EXTEND = "vm_extendPre";

    /**
     * 网络表
     */
    private static final String TABLE_FIX_NETWORK = "netPerformance";
    private static final String VIEW_VM_NETWORK = "vm_netPerformance_indicator";


    private static final String PREFIX = ",";


    private static final int INTERVAL_FIVE_SECONDS = 5;
    private static int INTERVAL_MINUTE = 60;
    private static final int INTERVAL_FIVE_MINUTE = 5 * 60;
    private static final int INTERVAL_HOUR = 60 * 60;
    private static final int INTERVAL_DAY = 24 * 60 * 60;


    public static List<Long> getFormatTimeList(Long startTime, Long endTime, int interval)  {
        if(startTime == null || endTime == null){
            throw new RuntimeException("传递的时间参数错误！");
        }
        if(startTime > endTime){
            throw new RuntimeException("传递的开始时间大于结束时间！");
        }
        List<Long> time = new ArrayList<>();
        if(startTime.equals(endTime)) {
        	time.add(startTime);
        }else {
	        for (Long i = startTime; i <endTime; i = i + interval) {
	            time.add(i);
	        }
        }
        return time;
    }

    /**
     * get time list interval default one minute  目前仅适用于辽宁事件台组装数据，单位一分钟
     *
     * @param startTime
     * @param endTime
     * @return
     */
    public static List<Long> getFormatTimeList(Long startTime, Long endTime) {
        if(startTime == null || endTime == null){
            throw new RuntimeException("传递的时间参数错误！");
        }
        if(startTime > endTime){
            throw new RuntimeException("传递的开始时间大于结束时间！");
        }
        List<Long> time = new ArrayList<>();
        if(startTime.longValue() == endTime.longValue()){
            time.add(startTime);
        }else{
            for (Long i = startTime; i < endTime; i = i + 60000) {
                time.add(i);
            }
        }
        return time;
    }

    /**
     * 获得格式化后的指标值列表
     *
     * @param baList
     * @param timeList
     * @param columnName
     * @param columnType
     * @return
     */
    public static List<String> getFormatValue(Integer uuid, List<Map<String, Object>> baList, List<Long> timeList,
                                              String columnName, String columnType) {
        Object value;
        List<String> valueList = new ArrayList<>();
        for (Long timestamp : timeList) {
            boolean flag = false;
            for (Map<String, Object> baMap : baList) {
                Long baTimeStamp = evUtil.getMapLongValue(baMap, "time");
                //Integer.parseInt(baMap.get("uuid").toString())
                if (timestamp.equals(baTimeStamp) &&
                        uuid.equals(evUtil.getMapIntegerValue(baMap, "uuid"))) {
                    value = baMap.get(columnName);
                    valueList.add(value == null ? null : value.toString());
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                valueList.add(null);
            }
        }
        return valueList;
    }

    /**
     * 按照列名获取指标集合(纵向获取值)
     *
     * @param baList
     * @param columnName
     * @return java.util.List<java.lang.Object>
     * @author NJ
     * @date 2018/12/29 10:39
     */
    public static List<String> getFormatValue(List<Map<String, Object>> baList, String columnName) {
        List<String> valueList = new ArrayList<>();
        baList.stream().forEach(x -> {
            valueList.add(valueOf(x.get(columnName)));
        });
        return valueList;
    }


    /**
     * 根据指标返回结果参数
     *
     * @param indicators
     * @param dataList
     * @param timeList
     * @return java.util.List<com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam>
     * @author NJ
     * @date 2018/10/23 16:38
     */
    public static List<IndicatorDataDetailParam> getIndicatorDataByIndicatorList(List<Indicator> indicators, List<Integer> uuids,
                                                                                 List<Map<String, Object>> dataList, List<Long> timeList) {
        List<IndicatorDataDetailParam> result = new ArrayList<>();
        for (Indicator indicator : indicators) {
            for (Integer id : uuids) {
                IndicatorDataDetailParam indicatorValueParam = new IndicatorDataDetailParam();
                indicatorValueParam.setId(indicator.getId());
                indicatorValueParam.setUuid(id);
                indicatorValueParam.setName(indicator.getName());
                indicatorValueParam.setUnit(indicator.getUnit());
                indicatorValueParam.setType(indicator.getType());
                indicatorValueParam.setValue(IndicatorSupportUtil.getFormatValue(id, dataList, timeList,
                        indicator.getColumnName(), indicator.getColumnType()));
                result.add(indicatorValueParam);
            }
        }
        return result;
    }


    /**
     * 维度筛选规则转化为sql语句条件
     *
     * @param rule
     * @return
     * @throws RuntimeException
     */
    public static String translateDimensionRule(int rule) throws RuntimeException {
        String sql;
        switch (rule) {
            case 1:
                sql = " IN ";
                break;
            case 2:
                sql = " NOT IN ";
                break;
            case 3:
                sql = " LIKE ";
                break;
            case 4:
                sql = " NOT LIKE ";
                break;
            case 5:
                sql = " LIKE ";
                break;
            case 6:
                sql = " LIKE ";
                break;
            case 7:
                sql = " = '-' ";
                break;
            case 8:
                sql = " !='-' ";
                break;
            default:
                throw new RuntimeException("筛选规则参数有误，参数(rule):" + rule);
        }
        return sql;
    }

    /**
     * 指标筛选规则转化为sql语句条件
     * @param rule
     * @return
     * @throws RuntimeException
     */
    private static String translateIndicatorRule(int rule) throws RuntimeException {
        String sql;
        switch (rule){
            case 1 :
                sql = " > ";
                break;
            case 2 :
                sql = " < ";
                break;
            case 3 :
                sql = " IN ";
                break;
            case 4 :
                sql = " NOT IN ";
                break;
            default:
                throw new RuntimeException("筛选规则参数有误，参数(rule):"+ rule);
        }
        return sql;
    }

    /**
     * 根据时间戳补数据
     *
     * @param data
     * @param timeList
     * @return
     * @author cgc
     * @date 2018年10月11日
     */
    public static List<Map<String, Object>> getForMatList(List<Map<String, Object>> data, List<Long> timeList) {
        List<Map<String, Object>> list = new ArrayList<>();
        for (Long timestamp : timeList) {
            boolean flag = false;
            for (Map<String, Object> baMap : data) {
                Long baTimeStamp = evUtil.getMapLongValue(baMap, "time");
                if (timestamp.equals(baTimeStamp)) {
                    list.add(baMap);
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                list.add(new HashMap<>());
            }
        }
        return list;
    }


    /**
     * 根据时间和维度补数据(含有对比)
     * @param data
     * @param timeList
     * @param dimension
     * @param dataMap
     * @param compareDataMap
     * @return void
     * @author NJ
     * @date 2019/4/8 11:12
     */
    @Deprecated
    public static void assembleCompareMultiData1(List<Map<String, Object>> data, List<Map<String, Object>> compareData, List<Long> timeList,
                                                List<Long> compareTimeList, String dimension,  Map<String, Map<Integer,List<String>>> dataMap, Map<String, Map<Integer,List<String>>> compareDataMap,
                                                Map<Integer, Map<String, Object>> columnMap, int compare, int intervalType) {
        Long baTimeStamp;
        String dimensionVal;
        boolean flag, compareFlag;
        for (Long timestamp : timeList) {
            for (Map.Entry<String, Map<Integer, List<String>>> entry : dataMap.entrySet()) {
                flag = false;
                for (Map<String, Object> baMap : data) {
                    if(intervalType == 0){
                        baTimeStamp = evUtil.getMapLongValue(baMap, "time");
                    }else{
                        baTimeStamp = DateUtils.getDateLong(evUtil.getMapStrValue(baMap, "date"));
                    }
                    dimensionVal = evUtil.getMapStrValue(baMap, dimension);
                    if (timestamp.equals(baTimeStamp) && entry.getKey().equals(dimensionVal)) {
                        //indicator map
                        for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                            daEntry.getValue().add(evUtil.getMapStr(baMap, evUtil.getMapStrValue(columnMap.get(daEntry.getKey()), "columnName")));
                        }
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                        daEntry.getValue().add(null);
                    }
                }
            }
        }

        //compare
        if(compare == 1 || compare == 2){
            for (Long timestamp : compareTimeList) {
                for (Map.Entry<String, Map<Integer, List<String>>> entry : compareDataMap.entrySet()) {
                    compareFlag = false;
                    for (Map<String, Object> baMap : compareData) {
                        dimensionVal = evUtil.getMapStrValue(baMap, dimension);
//                        baTimeStamp = evUtil.getMapLongValue(baMap, "time");
                        if(intervalType == 0){
                            baTimeStamp = evUtil.getMapLongValue(baMap, "time");
                        }else{
                            baTimeStamp = DateUtils.getDateLong(evUtil.getMapStrValue(baMap, "date"));
                        }
                        if (timestamp.equals(baTimeStamp) && entry.getKey().equals(dimensionVal)) {
                            for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                                daEntry.getValue().add(evUtil.getMapStr(baMap, evUtil.getMapStrValue(columnMap.get(daEntry.getKey()), "columnName")));
                            }
                            compareFlag = true;
                            break;
                        }
                    }
                    if (!compareFlag) {
                        for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                            daEntry.getValue().add(null);
                        }
                    }
                }
            }

        }
    }


    /**
     * 根据时间和维度补数据(含有对比)
     * @param data 注意时间范围按照时间戳排序正序
     * @param timeList
     * @param dimension
     * @param dataMap
     * @param compareDataMap
     * @return void
     * @author NJ
     * @date 2019/4/8 11:12
     */
    public static void assembleCompareMultiData(List<Map<String, Object>> data, List<Map<String, Object>> compareData, List<Long> timeList,
                                                List<Long> compareTimeList, String dimension,  Map<String, Map<Integer,List<String>>> dataMap, Map<String, Map<Integer,List<String>>> compareDataMap,
                                                Map<Integer, Map<String, Object>> columnMap, int compare, int intervalType) {
        Long baTimeStamp;
        String dimensionVal;
        boolean flag, compareFlag;

        Map<String, Object> resultMap;
        Iterator<Map<String, Object>> dataIterator;
        for (Long timestamp : timeList) {
            for (Map.Entry<String, Map<Integer, List<String>>> entry : dataMap.entrySet()) {
                flag = false;
                dataIterator = data.iterator();
                while(dataIterator.hasNext()){
                    resultMap = dataIterator.next();
                    if(intervalType == 0){
                        baTimeStamp = evUtil.getMapLongValue(resultMap, "time");
                    }else{
                        baTimeStamp = DateUtils.getDateLong(evUtil.getMapStrValue(resultMap, "date"));
                    }
                    //注意时间范围按照时间戳排序正序
                    if(baTimeStamp > timestamp){
                        break;
                    }
                    dimensionVal = evUtil.getMapStrValue(resultMap, dimension);
                    if (timestamp.equals(baTimeStamp) && entry.getKey().equals(dimensionVal)) {
                        //indicator map
                        for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                            daEntry.getValue().add(evUtil.getMapStr(resultMap, evUtil.getMapStrValue(columnMap.get(daEntry.getKey()), "columnName")));
                        }
                        flag = true;
                        dataIterator.remove();
                        break;
                    }
                }

                if (!flag) {
                    for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                        daEntry.getValue().add(null);
                    }
                }

            }
        }

        //compare
        if(compare == 1 || compare == 2){
            for (Long timestamp : compareTimeList) {
                for (Map.Entry<String, Map<Integer, List<String>>> entry : compareDataMap.entrySet()) {
                    compareFlag = false;
                    dataIterator = compareData.iterator();
                    while(dataIterator.hasNext()){
                        resultMap = dataIterator.next();
                        dimensionVal = evUtil.getMapStrValue(resultMap, dimension);
                        if(intervalType == 0){
                            baTimeStamp = evUtil.getMapLongValue(resultMap, "time");
                        }else{
                            baTimeStamp = DateUtils.getDateLong(evUtil.getMapStrValue(resultMap, "date"));
                        }
                        //注意时间范围按照时间戳排序正序
                        if(baTimeStamp > timestamp){
                            break;
                        }
                        if (timestamp.equals(baTimeStamp) && entry.getKey().equals(dimensionVal)) {
                            for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                                daEntry.getValue().add(evUtil.getMapStr(resultMap, evUtil.getMapStrValue(columnMap.get(daEntry.getKey()), "columnName")));
                            }
                            compareFlag = true;
                            dataIterator.remove();
                            break;
                        }
                    }
                    if (!compareFlag) {
                        for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
                            daEntry.getValue().add(null);
                        }
                    }
                }
            }
        }

    }


    /**
     * 根据uuid, timestamp寻找告警
     * @param uuid
     * @param timestamp
     * @param baList
     * @return
     */
    public static void getAlarmAmountMapByParams(Integer uuid, Long timestamp, List<Map<String, Object>> baList, Map<String, Object> result) {
        if(result == null){
            return ;
        }
        if(uuid == null || timestamp == null
                || evUtil.listIsNullOrZero(baList)){
            return ;
        }
        for (Map<String, Object> baMap : baList) {
            Long baTimeStamp = evUtil.getMapLongValue(baMap, "time");
            if(timestamp.equals(baTimeStamp) &&
                    uuid.equals(Integer.parseInt(baMap.get("uuid").toString()))){
                result.put("alarmAmount", evUtil.getMapIntValue(baMap, "amount"));
                String infoIdsStr = evUtil.getMapStrValue(baMap, "alarmInfoIds");
                if(StringUtils.isNotBlank(infoIdsStr)){
                    result.put("alarmInfoIds", StringUtils.split(infoIdsStr,","));
                }else{
                    result.put("alarmInfoIds", new ArrayList<>());
                }
                return ;
            }
        }
        result.put("alarmAmount", 0);
        result.put("alarmInfoIds", new ArrayList<>());
        return ;
    }


    public static IndicatorIntervalParam getIndicatorInterval(Long start, Long end,
                              ConfigurationCache configurationCache) {
        if(start == null || end == null){
            throw new RuntimeException("开始时间和结束时间不能为空！");
        }
        if(start > end){
            throw new RuntimeException("开始时间不能大于结束时间！");
        }

        IndicatorIntervalParam result = new IndicatorIntervalParam();
        List<Long> timeList = new ArrayList<>();
        int interval;
        //day-threshold
        long threshold_day = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_THRESHOLD_DAY))*60000L;
        //hour-threshold
        int threshold = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_THRESHOLD)) * 60 * 1000;
        int threshold_5= Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_THRESHOLD_FIVE)) * 60 * 1000;
        long timeDiff = end - start;
        if(timeDiff >= threshold_day){
            interval = INTERVAL_DAY;
            start = DateUtils.longToDayTimeBehind(start);
            end = DateUtils.longToDayTime(end);
            result.setType(3);
        }else if( timeDiff >= threshold){
            start=DateUtils.longToHourTimeBehind(start);
            end=DateUtils.longToHourTime(end);
            interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_HOUR));
            result.setType(1);
        }else if( timeDiff >=threshold_5 && timeDiff < threshold){
            start=DateUtils.longToFiveMinuteTimeBehind(start);
            end=DateUtils.longToFiveMinuteTime(end);
            interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_FIVE));
            result.setType(2);
        }else{
        	start=DateUtils.longToFiveSecondTimeBehind(start);
            end=DateUtils.longToFiveSecondTime(end);
            interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
            result.setType(0);
        }
        result.setInterval(interval);
        if(start > end){
            throw new RuntimeException("开始时间和结束时间不符合规范！");
        }
        if(start.equals(end)) {
        	timeList.add(start);
        }else {
            interval *= 1000;
	        for (Long i = start; i < end; i = i + interval) {
	            timeList.add(i);
	        }
        }
        result.setStart(start);
        result.setEnd(end);
        result.setTimeList(timeList);
        return result;
    }

    /***
     * 根据时长获取间隔
     * @author NJ
     * @date 2019/6/25 10:48
     * @param configurationCache
     * @param duration
     * @return java.lang.Integer
     */
    public static int getIntervalByDuration(ConfigurationCache configurationCache,Long duration) {
        if(duration == null || duration < 0){
            throw new RuntimeException("参数错误！");
        }
        duration *= 1000L;
        int interval;
        //day-threshold
        long threshold_day = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_THRESHOLD_DAY))*60000L;
        //hour-threshold
        int threshold = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_THRESHOLD)) * 60 * 1000;
        int threshold_5= Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_THRESHOLD_FIVE)) * 60 * 1000;
        if(duration >= threshold_day){
            interval = INTERVAL_DAY;
        }else if( duration >= threshold){
            interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_HOUR));
        }else if( duration >=threshold_5 && duration < threshold){
            interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL_FIVE));
        }else{
            interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
        }
        return interval;
    }

    /**
     * 根据传入的Interval进行处理查询的表的类型以及返回的时间数组
     * @author NJ
     * @date 2019/4/1 20:50
     * @param start
     * @param end
     * @param interval  秒为单位
     * @return com.greattimes.ev.indicator.param.req.IndicatorIntervalParam
     */
    public static IndicatorIntervalParam getIndicatorIntervalByInterVal(Long start, Long end, Integer interval) {
        if(start == null || end == null){
            throw new RuntimeException("传入的开始时间和结束时间不能为空！");
        }
        if(start > end){
            throw new RuntimeException("开始时间不能大于结束时间！");
        }
        if(interval == null || interval <= 0){
            throw new RuntimeException("参数interval值非法！");
        }
        IndicatorIntervalParam result = new IndicatorIntervalParam();
        List<Long> timeList = new ArrayList<>();
        if(interval == INTERVAL_FIVE_SECONDS || interval == INTERVAL_MINUTE){
        	start=DateUtils.longToFiveSecondTimeBehind(start);
            end=DateUtils.longToFiveSecondTime(end);
            result.setType(0);
        }else if(interval == INTERVAL_FIVE_MINUTE){
        	start=DateUtils.longToFiveMinuteTimeBehind(start);
            end=DateUtils.longToFiveMinuteTime(end);
            result.setType(2);
        }else if(interval == INTERVAL_HOUR){
        	start=DateUtils.longToHourTimeBehind(start);
            end=DateUtils.longToHourTime(end);
            result.setType(1);
        }else if(interval == INTERVAL_DAY){
        	start = DateUtils.longToDayTimeBehind(start);
            end = DateUtils.longToDayTime(end);
            result.setType(3);
        }else{
            throw new RuntimeException("时间颗粒度非法!");
        }
        if(start > end){
            throw new RuntimeException("开始时间和结束时间不符合规范！");
        }
        if(start.equals(end)) {
        	timeList.add(start);
        }else {
	        for (Long i = start; i < end; i = i + interval * 1000) {
	            timeList.add(i);
	        }
        }
        result.setInterval(interval);
        result.setStart(start);
        result.setEnd(end);
        result.setTimeList(timeList);
        return result;
    }

    /**
     * 根据传入的Interval进行处理查询的表的类型
     * @author NJ
     * @date 2019/4/1 20:50
     * @param interval  秒为单位
     * @return com.greattimes.ev.indicator.param.req.IndicatorIntervalParam
     */
    public static IndicatorIntervalParam getIndicatorIntervalByInterVal(Integer interval)  {
        if(interval == null){
            throw new RuntimeException("参数interval值为空！");
        }
        IndicatorIntervalParam result = new IndicatorIntervalParam();
        if(interval == INTERVAL_FIVE_SECONDS || interval == INTERVAL_MINUTE){
            result.setType(0);
        }else if(interval == INTERVAL_FIVE_MINUTE){
            result.setType(2);
        }else if(interval == INTERVAL_HOUR){
            result.setType(1);
        }else if(interval == INTERVAL_DAY){
            result.setType(3);
        }
        return result;
    }


    /**
     * 如果obj为null, 则返回null, 不返回"null"空字符串
     * @author NJ
     * @date 2018/12/29 11:52
     * @param obj
     * @return java.lang.String
     */
    public static String valueOf(Object obj) {
        return (obj == null) ? null : obj.toString();
    }

    /**
     * 根据某一时间点进行数据的查找并进行补充
     * @param data
     * @param time
     * @param key
     * @return
     */
    public static Long getFormatValueByTimeAndKey(List<Map<String, Object>> data, long time, String key){
        Long result = 0L;
        if(data.size() > 0){
            for (Map<String, Object> map3 : data) {
                long timeStamp = (long)map3.get("time");
                if(time == timeStamp){
                    result = evUtil.getMapLongValue(map3, key);
                    break;
                }
            }
        }
        return result;
    }

    /**
     * 根据传入的type类型选择相应的表
     * @author NJ
     * @date 2019/4/9 16:15
     * @param tableType
     *      1、应用指标bpmTran...
     *      2、事件指标bpmExtendTran..
     *      3、应用组件维度bpmPre...
     *      4、事件维度extendPre...
     *      5、网络指标netWork...
     * @param map
     * @return void
     */
    public static void dealIndicatorTable(int tableType, Map<String, Object> map, int granularity)  {
        String table;
        switch(tableType){
            case 1:
                if(granularity == 1) {
                    table = VM_TRANSACTION_INDICATOR;
                }else if(granularity == 0){
                    table = T_BPMTRAN;
                }else if(granularity == 2){
                    table = VM_TRANSACTION_INDICATOR+"_5";
                }else if(granularity == 3){
                    table = VM_TRANSACTION_INDICATOR+"_d";
                }else{
                    throw new RuntimeException("根据开始时间和结束时间确定表类型值非法!");
                }
                break;
            case 2:
                if(granularity == 1) {
                    table = VM_EXTEND_INDICATOR;
                }else if(granularity == 0){
                    table = T_BPM_EXTEND_TRAN;
                }else if(granularity == 2){
                    table = VM_EXTEND_INDICATOR+"_5";
                }else if(granularity == 3){
                    table = VM_EXTEND_INDICATOR+"_d";
                }else{
                    throw new RuntimeException("根据开始时间和结束时间确定表类型值非法!");
                }
                break;
            case 3:
                if( granularity == 0 ) {
                    table = TABLE_FIX_BPM;
                }else if(granularity==2) {
                    table = VIEW_VM_BPM +"_5";
                }else if(granularity == 1){
                    table = VIEW_VM_BPM;
                }else if(granularity == 3){
                    table = VIEW_VM_BPM+"_d";
                }else{
                    throw new RuntimeException("根据开始时间和结束时间确定表类型值非法!");
                }
                break;
            case 4:
                if(granularity == 0) {
                    table = TABLE_FIX_EXTEND;
                }else if(granularity == 2) {
                    table = VIEW_VM_EXTEND+"_5";
                }else if(granularity == 1){
                    table = VIEW_VM_EXTEND;
                }else if(granularity == 3){
                    table = VIEW_VM_EXTEND+"_d";
                }else{
                    throw new RuntimeException("根据开始时间和结束时间确定表类型值非法!");
                }
                break;
            case 5:
                if(granularity == 0) {
                    table = TABLE_FIX_NETWORK;
                }else if(granularity == 2) {
                    table = VIEW_VM_NETWORK+"_5";
                }else if(granularity == 1){
                    table = VIEW_VM_NETWORK;
                }else if(granularity == 3){
                    table = VIEW_VM_NETWORK+"_d";
                }else{
                    throw new RuntimeException("根据开始时间和结束时间确定表类型值非法!");
                }
                break;
            default:
                throw new RuntimeException("处理指标查询表参数错误！参数tableType:"+tableType);
        }
        map.put("type", granularity);
        map.put("table", table);
    }


    /**
     * 处理分段时间
     * @param timePart
     * @return
     */
    public static String generateTimeStr(List<Map<String, Object>> timePart) {
        String timeStr = "";
        List<String> timeList=new ArrayList<>();
        for (Map<String, Object> timeMap : timePart) {
            String t="";
            List<Long> time=(List<Long>) timeMap.get("time");
            if (!evUtil.listIsNullOrZero(time)) {
            	if(time.get(0).equals(time.get(1))) {
            		t = "(time =" + time.get(0)+")";
            	}else {
            		t = "(time >=" + time.get(0) + " and time<" + time.get(1) + ")";
            	}
                timeList.add(t);
            }
        }
        if (!evUtil.listIsNullOrZero(timeList)) {
            timeStr = StringUtils.join(timeList, " OR ");
        }
        return timeStr;
    }
   

    /***
     * 根据开始时间和结束时间生成sql语句
     * @param start
     * @param end
     * @return
     */
    public static String generateTimeStrByStartAndEnd(long start, long end){
        String str;
        if(start == end){
            str =  "(time =" +start+")";
        }else{
            str = "(time >=" + start + " and time<" + end + ")";
        }
        return str;
    }

    /**
     * 根据指标筛选条件生成sql语句
     * @author NJ
     * @date 2019/5/23 11:03
     * @param filterList
     * @return java.lang.String
     */
    public static String generateReportIndicatorFilterSqlStr(List<JSONObject> filterList){
        String filterStr = "";
        if(evUtil.listIsNullOrZero(filterList)){
        	return null;
        }
        String ename, ruleStr;
        List<String> sqlStrList = new ArrayList<>();
        for(JSONObject filter : filterList){
            ename = filter.getString("ename");
            int rule=filter.getIntValue("rule");
            ruleStr = translateIndicatorRule(filter.getIntValue("rule"));
            List<Double> value=(List<Double>) filter.get("value");
			if (!evUtil.listIsNullOrZero(value)) {
				if (rule == 1 || rule == 2) {
					sqlStrList.add("(" + ename + ruleStr + value.get(0) + ") ");
				}
				if (rule == 3 || rule == 4) {
					String str = StringUtils.join(value, " , ");
					str = ename + ruleStr + "(" + str + ")";
					sqlStrList.add(str);
				}
			}
        }
        if (!evUtil.listIsNullOrZero(sqlStrList)) {
            filterStr = StringUtils.join(sqlStrList, " AND ");
        }
        return filterStr;
    }
    
    
    /**
     * 多段对比时间间隔(不取绝对值)
     * @param timePart
     * @param compareTimePart
     * @return
     */
    public static Long getCompareInterval(List<Map<String, Object>> timePart, List<Map<String, Object>> compareTimePart)  {
        if(evUtil.listIsNullOrZero(timePart) || evUtil.listIsNullOrZero(compareTimePart)){
            throw new RuntimeException("传入的时间参数错误！");
        }
        long startTime =((List<Long>)timePart.get(0).get("time")).get(0);
        long compareStartTime =((List<Long>)compareTimePart.get(0).get("time")).get(0);
        return compareStartTime - startTime;
    }


    /**
     * 生成指标列
     * @param tableType
     *      1、应用指标bpmTran...
     *      2、事件指标bpmExtendTran..
     *      3、应用组件维度bpmPre...
     *      4、事件维度extendPre...
     *      5、网络指标 netWork...
     * @param columns
     * @param type
     * @param interval
     * @return
     */
    public static String generateIndicatorStrSql(int tableType, List<String> columns, int type, int interval)  {
        StringBuilder sb = new StringBuilder("");
        if(evUtil.listIsNullOrZero(columns)){
            return sb.toString();
        }
        Map<String, String> indicatorMap = getIndicatorMap(tableType,type);
        // day:3 hour:1 5min:2 second:0
        for(String str : columns){
            if(StringUtils.isNotBlank(indicatorMap.get(str))){
                //网络指标特殊逻辑
                if(tableType == 5){
                    sb.append(",").append(indicatorMap.get(str).replaceAll("\\$", String.valueOf(interval)));
                }else{
                    sb.append(",").append(indicatorMap.get(str));
                }
            }else{
                throw new RuntimeException("参数中无指标查询列！");
            }
        }
        return sb.toString();
    }

    /**
     * 获取列中的map
     * @param tableType
     *      1、应用指标bpmTran...
     *      2、事件指标bpmExtendTran..
     *      3、应用组件维度bpmPre...
     *      4、事件维度extendPre...
     *      5、网络指标 netWork...
     * @param type
     *       day:3 hour:1 5min:2 second:0
     * @return
     */
    private static Map getIndicatorMap(int tableType, int type){
        Map<String, String> result = new HashMap<>();
        if(tableType == 5) {
            if (type == 0) {
                result = IndicatorColumnMap.NETWORK_COLUMNS_MAP_SUM;
            } else {
                result = IndicatorColumnMap.NETWORK_COLUMNS_MAP_SUM_MERGE;
            }
        }
        return result;
    }
    /**
     * 交易指标SUM  5s 添加指标需修改
     * @author CGC
     * @param columns 
     * @return
     */
    public static String generateBpmIndicatorStrSum(List<String> columns){
    	String allTransCount=null;
    	if(columns.contains("allTransCount")) {
    		allTransCount="allTransCount";
    	}else {
    		allTransCount="SUM(allTransCount)";
    	}
        StringBuilder sb = new StringBuilder("");
        if(!evUtil.listIsNullOrZero(columns)){
        	for (String string : columns) {
				switch (string) {
				case "allTransCount":
					sb.append(",SUM(allTransCount) AS allTransCount");
					break;
				case "responseTime":
					sb.append(",round(if(isNaN(SUM(allStart2ends) / SUM(responseTransCount)), null, SUM(allStart2ends) / SUM(responseTransCount)),2) AS responseTime");
					break;
				case "responseRate":
					sb.append(",round(if(isNaN(SUM(responseTransCount) / "+allTransCount+"), null, SUM(responseTransCount) / "+allTransCount+") *100, 2) AS responseRate");
					break;	
				case "successRate":
					sb.append(",round(if(isNaN(SUM(successTransCount) / SUM(responseTransCount)),null,SUM(successTransCount) / SUM(responseTransCount)) * 100, 2) AS successRate");
					break;	
				case "money":
					sb.append(",round(SUM(money), 2) AS money");
					break;	
				case "businessSuccessRate":
					sb.append(",round(if(isNaN(SUM(businessSuccessCount) / "+allTransCount+"),null, SUM(businessSuccessCount) / "+allTransCount+")*100, 2) AS businessSuccessRate");
					break;
				default:
					break;
				}
			}
        }
        return sb.toString();
    }

    /**
     * @author NJ
     * @date 2019/7/12 10:07
     * @param type 0 : 5s  !0: 其它颗粒度
     * @param map  需要配置的参数
     * @param key  参数的key
     * @param columnNames  列名
     * @return void
     */
    public static void handleIndicatorStrParamByType(int type, Map<String, Object> map, String key, List<String> columnNames){
        if(type == 0){
            map.put(key, generateBpmIndicatorStrSum(columnNames));
        }else{
            map.put(key, generateBpmIndicatorStrSumMerge(columnNames));
        }
    }

    /**
     * @author NJ
     * @date 2019/7/12 10:07
     * @param type 0 : 5s  !0: 其它颗粒度
     * @param map  需要配置的参数
     * @param key  参数的key
     * @param columnNames  列名
     * @return void
     */
    public static void handleIndicatorStrParamByTypeNoPrefix(int type, Map<String, Object> map, String key, List<String> columnNames){
        String str;
        if(type == 0){
            str = generateBpmIndicatorStrSum(columnNames);
            if(str.startsWith(PREFIX)){
                str = str.replaceFirst(PREFIX,"");
            }
            map.put(key, str);
        }else{
            str = generateBpmIndicatorStrSumMerge(columnNames);
            if(str.startsWith(PREFIX)){
                str = str.replaceFirst(PREFIX, "");
            }
            map.put(key, str);
        }
    }


    /**
     * 交易指标SUMMerge  5min 1h 1d 添加指标需修改
     * @author CGC
     * @param columns
     * @return
     */
    public static String generateBpmIndicatorStrSumMerge(List<String> columns){
    	String allTransCount=null;
    	if(columns.contains("allTransCount")) {
    		allTransCount="allTransCount";
    	}else {
    		allTransCount="sumMerge(allTransCount)";
    	}
        StringBuilder sb = new StringBuilder("");
        if(!evUtil.listIsNullOrZero(columns)){
        	for (String string : columns) {
				switch (string) {
				case "allTransCount":
					sb.append(",sumMerge(allTransCount) AS allTransCount");
					break;
				case "responseTime":
					sb.append(",round(if(isNaN(sumMerge(allStart2ends) / sumMerge(responseTransCount)), null, sumMerge(allStart2ends) / sumMerge(responseTransCount)),2) AS responseTime");
					break;
				case "responseRate":
					sb.append(",round(if(isNaN(sumMerge(responseTransCount) / "+allTransCount+"), null, sumMerge(responseTransCount) / "+allTransCount+") *100, 2) AS responseRate");
					break;	
				case "successRate":
					sb.append(",round(if(isNaN(sumMerge(successTransCount) / sumMerge(responseTransCount)),null,sumMerge(successTransCount) / sumMerge(responseTransCount)) * 100, 2) AS successRate");
					break;	
				case "money":
					sb.append(",round(sumMerge(money), 2) AS money");
					break;	
				case "businessSuccessRate":
					sb.append(",round(if(isNaN(sumMerge(businessSuccessCount) / "+allTransCount+"),null, sumMerge(businessSuccessCount) / "+allTransCount+")*100, 2) AS businessSuccessRate");
					break;
				default:
					break;
				}
			}
        }
        return sb.toString();
    }

    public static String generateNetWorkSumStr(List<String> columns, int interval) {
        StringBuilder sb = new StringBuilder(" ");
        for (String column : columns) {
            switch (column) {
                case "InboundThroughput":
                    sb.append(", round(sum(tcp_in_bytes) / (").append(interval).append(" * 1024), 2) AS InboundThroughput ");
                    break;
                case "OutboundThroughput":
                    sb.append(", round(sum(tcp_out_bytes) / (").append(interval).append(" * 1024), 2) AS OutboundThroughput");
                    break;
                case "InboundGoodput":
                    sb.append(", round(sum(tcp_in_good_bytes) / (").append(interval).append(" * 1024), 2) AS InboundGoodput");
                    break;
                case "OutboundGoodput":
                    sb.append(", round(sum(tcp_out_good_bytes) / (").append(interval).append(" * 1024), 2) AS OutboundGoodput");
                    break;
                case "InboundTraffic":
                    sb.append(", round(sum(tcp_in_bytes) / 1024, 2) AS InboundTraffic");
                    break;
                case "OutboundTraffic":
                    sb.append(", round(sum(tcp_out_bytes) / 1024, 2) AS OutboundTraffic");
                    break;
                case "OutPayload":
                    sb.append(",round(sum(tcp_reassemble_out_bytes) / 1024, 2) AS OutPayload ");
                    break;
                case "InPayload":
                    sb.append(", round(sum(tcp_reassemble_in_bytes) / 1024, 2) AS InPayload");
                    break;
                case "PacketInboundThroughput":
                    sb.append(", round(sum(tcp_in_packet_count) / ").append(interval).append(", 2) AS PacketInboundThroughput");
                    break;
                case "PacketOutboundThroughput":
                    sb.append(", round(sum(tcp_out_packet_count) /").append(interval).append(" , 2) AS PacketOutboundThroughput");
                    break;
                case "PacketInboundTraffic":
                    sb.append(", sum(tcp_in_packet_count) AS PacketInboundTraffic");
                    break;
                case "PacketOutboundTraffic":
                    sb.append(", sum(tcp_out_packet_count) AS PacketOutboundTraffic");
                    break;
                case "PacketInboundPayload":
                    sb.append(", sum(tcp_in_good_packet_count) AS PacketInboundPayload");
                    break;
                case "PacketOutboundPayload":
                    sb.append(", sum(tcp_out_good_packet_count) AS PacketOutboundPayload");
                    break;
                case "PacketInboundRetransmissionRate":
                    sb.append(", round(sum(tcp_retrans_in_packet_count) / ").append(interval).append(", 2) AS PacketInboundRetransmissionRate");
                    break;
                case "PacketOutboundRetransmissionRate":
                    sb.append(", round(sum(tcp_retrans_out_packet_count) / ").append(interval).append(", 2) AS PacketOutboundRetransmissionRate");
                    break;
                case "InboundRetransmissionRate":
                    sb.append(", round(sum(tcp_retrans_in_bytes) / (").append(interval).append(" * 1024), 2) AS InboundRetransmissionRate");
                    break;
                case "OutboundRetransmissionRate":
                    sb.append(", round(sum(tcp_retrans_out_bytes) / (").append(interval).append(" * 1024), 2) AS OutboundRetransmissionRate");
                    break;
                case "InboundPacketLoss":
                    sb.append(", round(if(isNaN(sum(tcp_retrans_in_packet_count) / sum(tcp_in_packet_count)), 0,sum(tcp_retrans_in_packet_count) / sum(tcp_in_packet_count)) * 100,2) AS InboundPacketLoss");
                    break;
                case "OutboundPacketLoss":
                    sb.append(", round(if(isNaN(sum(tcp_retrans_out_packet_count) / sum(tcp_out_packet_count)), 0,sum(tcp_retrans_out_packet_count) / sum(tcp_out_packet_count)) * 100,2) AS OutboundPacketLoss");
                    break;
                case "InboundZeroPacket":
                    sb.append(", sum(tcp_zero_window_packet_in_count)  AS InboundZeroPacket");
                    break;
                case "OutboundZeroPacket":
                    sb.append(",sum(tcp_zero_window_packet_out_count) AS OutboundZeroPacket");
                    break;
                case "InboundRTT":
                    sb.append(", sum(tcp_syn_in_delay_microsecond) AS InboundRTT");
                    break;
                case "OutboundRTT":
                    sb.append(", sum(tcp_syn_out_delay_microsecond) AS OutboundRTT");
                    break;


                case "ConnectionRequest":
                    sb.append(", sum(tcp_try_connect_total_count) AS ConnectionRequest");
                    break;
                case "ConnectionOK":
                    sb.append(", sum(tcp_connect_ok_total_count) AS ConnectionOK");
                    break;
                case "ConnectionClose":
                    sb.append(", sum(tcp_connect_finclose_total_count) AS ConnectionClose");
                    break;
                case "ConnectionRestClose":
                    sb.append(",sum(tcp_connect_rest_total_count) AS ConnectionRestClose");
                    break;
                case "ConnectionTimeoutClose":
                    sb.append(", sum(tcp_connect_timeout_total_count) AS ConnectionTimeoutClose");
                    break;
                case "Connections":
                    sb.append(", sum(tcp_max_active_count) AS Connections");
                    break;
                default:
                    break;
            }
        }
        return sb.toString();
    }

    public static String generateNetWorkSumMergeStr(List<String> columns, int interval) {
        StringBuilder sb = new StringBuilder(" ");
        for (String column : columns) {
            switch (column) {
                case "InboundThroughput":
                    sb.append(", round(sumMerge(tcp_in_bytes) / (").append(interval).append(" * 1024), 2) AS InboundThroughput ");
                    break;
                case "OutboundThroughput":
                    sb.append(", round(sumMerge(tcp_out_bytes) / (").append(interval).append(" * 1024), 2) AS OutboundThroughput");
                    break;
                case "InboundGoodput":
                    sb.append(", round(sumMerge(tcp_in_good_bytes) / (").append(interval).append(" * 1024), 2) AS InboundGoodput");
                    break;
                case "OutboundGoodput":
                    sb.append(", round(sumMerge(tcp_out_good_bytes) / (").append(interval).append(" * 1024), 2) AS OutboundGoodput");
                    break;
                case "InboundTraffic":
                    sb.append(", round(sumMerge(tcp_in_bytes) / 1024, 2) AS InboundTraffic");
                    break;
                case "OutboundTraffic":
                    sb.append(", round(sumMerge(tcp_out_bytes) / 1024, 2) AS OutboundTraffic");
                    break;
                case "OutPayload":
                    sb.append(", round(sumMerge(tcp_reassemble_out_bytes) / 1024, 2) AS OutPayload ");
                    break;
                case "InPayload":
                    sb.append(", round(sumMerge(tcp_reassemble_in_bytes) / 1024, 2) AS InPayload");
                    break;
                case "PacketInboundThroughput":
                    sb.append(", round(sumMerge(tcp_in_packet_count) / ").append(interval).append(", 2) AS PacketInboundThroughput");
                    break;
                case "PacketOutboundThroughput":
                    sb.append(", round(sumMerge(tcp_out_packet_count) /").append(interval).append(" , 2) AS PacketOutboundThroughput");
                    break;
                case "PacketInboundTraffic":
                    sb.append(", sumMerge(tcp_in_packet_count) AS PacketInboundTraffic");
                    break;
                case "PacketOutboundTraffic":
                    sb.append(", sumMerge(tcp_out_packet_count) AS PacketOutboundTraffic");
                    break;
                case "PacketInboundPayload":
                    sb.append(", sumMerge(tcp_in_good_packet_count) AS PacketInboundPayload");
                    break;
                case "PacketOutboundPayload":
                    sb.append(", sumMerge(tcp_out_good_packet_count) AS PacketOutboundPayload");
                    break;
                case "PacketInboundRetransmissionRate":
                    sb.append(", round(sumMerge(tcp_retrans_in_packet_count) / ").append(interval).append(", 2) AS PacketInboundRetransmissionRate");
                    break;
                case "PacketOutboundRetransmissionRate":
                    sb.append(", round(sumMerge(tcp_retrans_out_packet_count) / ").append(interval).append(", 2) AS PacketOutboundRetransmissionRate");
                    break;
                case "InboundRetransmissionRate":
                    sb.append(", round(sumMerge(tcp_retrans_in_bytes) / (").append(interval).append(" * 1024), 2) AS InboundRetransmissionRate");
                    break;
                case "OutboundRetransmissionRate":
                    sb.append(", round(sumMerge(tcp_retrans_out_bytes) / (").append(interval).append(" * 1024), 2) AS OutboundRetransmissionRate");
                    break;
                case "InboundPacketLoss":
                    sb.append(", round(if(isNaN(sumMerge(tcp_retrans_in_packet_count) / sumMerge(tcp_in_packet_count)), 0,sumMerge(tcp_retrans_in_packet_count) / sumMerge(tcp_in_packet_count)) * 100,2) AS InboundPacketLoss");
                    break;
                case "OutboundPacketLoss":
                    sb.append(", round(if(isNaN(sumMerge(tcp_retrans_out_packet_count) / sumMerge(tcp_out_packet_count)), 0,sumMerge(tcp_retrans_out_packet_count) / sumMerge(tcp_out_packet_count)) * 100,2) AS OutboundPacketLoss");
                    break;
                case "InboundZeroPacket":
                    sb.append(", sumMerge(tcp_zero_window_packet_in_count)  AS InboundZeroPacket");
                    break;
                case "OutboundZeroPacket":
                    sb.append(",sumMerge(tcp_zero_window_packet_out_count) AS OutboundZeroPacket");
                    break;
                case "InboundRTT":
                    sb.append(", sumMerge(tcp_syn_in_delay_microsecond) AS InboundRTT");
                    break;
                case "OutboundRTT":
                    sb.append(", sumMerge(tcp_syn_out_delay_microsecond) AS OutboundRTT");
                    break;


                case "ConnectionRequest":
                    sb.append(", sumMerge(tcp_try_connect_total_count) AS ConnectionRequest");
                    break;
                case "ConnectionOK":
                    sb.append(", sumMerge(tcp_connect_ok_total_count) AS ConnectionOK");
                    break;
                case "ConnectionClose":
                    sb.append(", sumMerge(tcp_connect_finclose_total_count) AS ConnectionClose");
                    break;
                case "ConnectionRestClose":
                    sb.append(",sumMerge(tcp_connect_rest_total_count) AS ConnectionRestClose");
                    break;
                case "ConnectionTimeoutClose":
                    sb.append(", sumMerge(tcp_connect_timeout_total_count) AS ConnectionTimeoutClose");
                    break;
                case "Connections":
                    sb.append(", sumMerge(tcp_max_active_count) AS Connections");
                    break;
                default:
                    break;
            }
        }
        return sb.toString();
    }

	/**生成指标sql
	 * @author CGC
	 * @param paramMap
	 * @param enameList
	 * @param tableType
	 */
	public static void generateIndicatorColumnStr(Map<String, Object> paramMap,List<String> enameList, int tableType) {
		if(tableType==0) {
			paramMap.put("indicatorColumnStr", IndicatorSupportUtil.generateBpmIndicatorStrSum(enameList));
		}else {
			paramMap.put("indicatorColumnStr", IndicatorSupportUtil.generateBpmIndicatorStrSumMerge(enameList));
		}
	}

    /**
     * 网络指标sql
     * @author NJ
     * @date 2019/7/25 16:58
     * @param paramMap
     * @param columns
     * @param key
     * @param tableType
     * @param interval
     * @return void
     */
    public static void generateNetIndicatorColumnStr(Map<String, Object> paramMap,List<String> columns, String key, int tableType, int interval) {
        if(tableType == 0){
            paramMap.put(key,  IndicatorSupportUtil.generateNetWorkSumStr(columns,interval));
        }else{
            paramMap.put(key,  IndicatorSupportUtil.generateNetWorkSumMergeStr(columns,interval));
        }

    }



    /**
     *
     * @author NJ
     * @date 2019/8/9 15:29
     * @param timeParts
     * @param interval
     * @param intervalType
     * @return java.util.List<java.lang.Long>
     */
    public static List<Long> generateTimeList(List<Map<String, Object>> timeParts, int interval, int intervalType){
        if(evUtil.listIsNullOrZero(timeParts)){
            throw new RuntimeException("时间范围有误！");
        }
        if(intervalType == 1){
            Set<Long> dateTimeSet = new HashSet<>();
            for (Map<String, Object> timeMap : timeParts) {
                List<Long> time = (List<Long>) timeMap.get("time");
                if (evUtil.listIsNullOrZero(time)) {
                    throw new RuntimeException("时间范围有误！");
                }
                dateTimeSet.add(DateUtils.longToDayTime(time.get(0)));
            }
            List<Long> dateTimes = new ArrayList<>(dateTimeSet);
            Collections.sort(dateTimes);
            return dateTimes;
        }else{
            List<Long> dateTimeList = new ArrayList<>();
            Long start,end;
            for (Map<String, Object> timeMap : timeParts) {
                List<Long> time = (List<Long>) timeMap.get("time");
                if (evUtil.listIsNullOrZero(time)) {
                    throw new RuntimeException("时间范围有误！");
                }
                start = time.get(0);
                end = time.get(1);
                dateTimeList.addAll(IndicatorSupportUtil.getFormatTimeList(start, end, interval*1000));
            }
            return dateTimeList;

//            List<Long> time = (List<Long>) timeParts.get(0).get("time");
//            if (evUtil.listIsNullOrZero(time)) {
//                throw new RuntimeException("时间范围有误！");
//            }
//            Long start = time.get(0);
//            Long end = time.get(1);
//            return IndicatorSupportUtil.getFormatTimeList(start, end, interval*1000);
        }
    }




}
