package com.greattimes.ev.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/7/8 9:46
 */
public  class IndicatorColumnMap {


    /**
     * SUM 5s
     */
    public static final Map<String, String> NETWORK_COLUMNS_MAP_SUM;

    static {
        NETWORK_COLUMNS_MAP_SUM = new HashMap();
        NETWORK_COLUMNS_MAP_SUM.put("InboundThroughput", "round(sum(tcp_in_bytes) / ($ * 1024), 2) AS InboundThroughput");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundThroughput", "round(sum(tcp_out_bytes) / ($ * 1024), 2) AS OutboundThroughput");
        NETWORK_COLUMNS_MAP_SUM.put("InboundGoodput", "round(sum(tcp_in_good_bytes) / ($ * 1024), 2) AS InboundGoodput");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundGoodput", "round(sum(tcp_out_good_bytes) / ($ * 1024), 2) AS OutboundGoodput");
        NETWORK_COLUMNS_MAP_SUM.put("InboundTraffic", "round(sum(tcp_in_bytes) / 1024, 2) AS InboundTraffic");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundTraffic", "round(sum(tcp_out_bytes) / 1024, 2) AS OutboundTraffic");
        NETWORK_COLUMNS_MAP_SUM.put("OutPayload", "round(sum(tcp_reassemble_out_bytes) / 1024, 2) AS OutPayload");
        NETWORK_COLUMNS_MAP_SUM.put("InPayload", "round(sum(tcp_reassemble_in_bytes) / 1024, 2) AS InPayload");
        NETWORK_COLUMNS_MAP_SUM.put("PacketInboundThroughput", "round(sum(tcp_in_packet_count) / $, 2) AS PacketInboundThroughput");
        NETWORK_COLUMNS_MAP_SUM.put("PacketOutboundThroughput", "round(sum(tcp_out_packet_count) / $, 2) AS PacketOutboundThroughput");
        NETWORK_COLUMNS_MAP_SUM.put("PacketInboundTraffic", "sum(tcp_in_packet_count) AS PacketInboundTraffic");
        NETWORK_COLUMNS_MAP_SUM.put("PacketOutboundTraffic", "sum(tcp_out_packet_count) AS PacketOutboundTraffic");
        NETWORK_COLUMNS_MAP_SUM.put("PacketInboundPayload", "sum(tcp_in_good_packet_count) AS PacketInboundPayload");
        NETWORK_COLUMNS_MAP_SUM.put("PacketOutboundPayload", "sum(tcp_out_good_packet_count) AS PacketOutboundPayload");
        NETWORK_COLUMNS_MAP_SUM.put("PacketInboundRetransmissionRate", "round(sum(tcp_retrans_in_packet_count) / $, 2) AS PacketInboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM.put("PacketOutboundRetransmissionRate", "round(sum(tcp_retrans_out_packet_count) / $, 2) AS PacketOutboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM.put("InboundRetransmissionRate", "round(sum(tcp_retrans_in_bytes) / ($ * 1024), 2) AS InboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundRetransmissionRate", "round(sum(tcp_retrans_out_bytes) / ($ * 1024), 2) AS OutboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM.put("InboundPacketLoss", "round(if(isNaN(sum(tcp_retrans_in_packet_count) / sum(tcp_in_packet_count)), 0,sum(tcp_retrans_in_packet_count) / sum(tcp_in_packet_count)) * 100,2) AS InboundPacketLoss");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundPacketLoss", "round(if(isNaN(sum(tcp_retrans_out_packet_count) / sum(tcp_out_packet_count)), 0,sum(tcp_retrans_out_packet_count) / sum(tcp_out_packet_count)) * 100,2) AS OutboundPacketLoss");
        NETWORK_COLUMNS_MAP_SUM.put("InboundZeroPacket", "sum(tcp_zero_window_packet_in_count)  AS InboundZeroPacket");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundZeroPacket", "sum(tcp_zero_window_packet_out_count) AS OutboundZeroPacket");
        NETWORK_COLUMNS_MAP_SUM.put("InboundRTT", "sum(tcp_syn_in_delay_microsecond) AS InboundRTT");
        NETWORK_COLUMNS_MAP_SUM.put("OutboundRTT", "sum(tcp_syn_out_delay_microsecond) AS OutboundRTT");

        NETWORK_COLUMNS_MAP_SUM.put("ConnectionRequest", "sum(tcp_try_connect_total_count) AS ConnectionRequest");
        NETWORK_COLUMNS_MAP_SUM.put("ConnectionOK", "sum(tcp_connect_ok_total_count) AS ConnectionOK");
        NETWORK_COLUMNS_MAP_SUM.put("ConnectionClose", "sum(tcp_connect_finclose_total_count) AS ConnectionClose");
        NETWORK_COLUMNS_MAP_SUM.put("ConnectionRestClose", "sum(tcp_connect_rest_total_count) AS ConnectionRestClose");
        NETWORK_COLUMNS_MAP_SUM.put("ConnectionTimeoutClose", "sum(tcp_connect_timeout_total_count) AS ConnectionTimeoutClose");
        NETWORK_COLUMNS_MAP_SUM.put("Connections", "sum(tcp_max_active_count) AS Connections");
    }

    /**
     * SUM_MERGE  5min、60min、day
     */
    public static final Map<String, String> NETWORK_COLUMNS_MAP_SUM_MERGE;
    
    static {
        NETWORK_COLUMNS_MAP_SUM_MERGE = new HashMap();
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundThroughput", "round(sumMerge(tcp_in_bytes) / ($ * 1024), 2) AS InboundThroughput");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundThroughput", "round(sumMerge(tcp_out_bytes) / ($ * 1024), 2) AS OutboundThroughput");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundGoodput", "round(sumMerge(tcp_in_good_bytes) / ($ * 1024), 2) AS InboundGoodput");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundGoodput", "round(sumMerge(tcp_out_good_bytes) / ($ * 1024), 2) AS OutboundGoodput");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundTraffic", "round(sumMerge(tcp_in_bytes) / 1024, 2) AS InboundTraffic");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundTraffic", "round(sumMerge(tcp_out_bytes) / 1024, 2) AS OutboundTraffic");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutPayload", "round(sumMerge(tcp_reassemble_out_bytes) / 1024, 2) AS OutPayload");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InPayload", "round(sumMerge(tcp_reassemble_in_bytes) / 1024, 2) AS InPayload");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketInboundThroughput", "round(sumMerge(tcp_in_packet_count) / $, 2) AS PacketInboundThroughput");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketOutboundThroughput", "round(sumMerge(tcp_out_packet_count) / $, 2) AS PacketOutboundThroughput");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketInboundTraffic", "sumMerge(tcp_in_packet_count) AS PacketInboundTraffic");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketOutboundTraffic", "sumMerge(tcp_out_packet_count) AS PacketOutboundTraffic");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketInboundPayload", "sumMerge(tcp_in_good_packet_count) AS PacketInboundPayload");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketOutboundPayload", "sumMerge(tcp_out_good_packet_count) AS PacketOutboundPayload");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketInboundRetransmissionRate", "round(sumMerge(tcp_retrans_in_packet_count) / $, 2) AS PacketInboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("PacketOutboundRetransmissionRate", "round(sumMerge(tcp_retrans_out_packet_count) / $, 2) AS PacketOutboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundRetransmissionRate", "round(sumMerge(tcp_retrans_in_bytes) / ($ * 1024), 2) AS InboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundRetransmissionRate", "round(sumMerge(tcp_retrans_out_bytes) / ($ * 1024), 2) AS OutboundRetransmissionRate");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundPacketLoss", "round(if(isNaN(sumMerge(tcp_retrans_in_packet_count) / sumMerge(tcp_in_packet_count)), 0,sumMerge(tcp_retrans_in_packet_count) / sumMerge(tcp_in_packet_count)) * 100,2) AS InboundPacketLoss");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundPacketLoss", "round(if(isNaN(sumMerge(tcp_retrans_out_packet_count) / sumMerge(tcp_out_packet_count)), 0,sumMerge(tcp_retrans_out_packet_count) / sumMerge(tcp_out_packet_count)) * 100,2) AS OutboundPacketLoss");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundZeroPacket", "sumMerge(tcp_zero_window_packet_in_count)  AS InboundZeroPacket");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundZeroPacket", "sumMerge(tcp_zero_window_packet_out_count) AS OutboundZeroPacket");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("InboundRTT", "sumMerge(tcp_syn_in_delay_microsecond) AS InboundRTT");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("OutboundRTT", "sumMerge(tcp_syn_out_delay_microsecond) AS OutboundRTT");

        NETWORK_COLUMNS_MAP_SUM_MERGE.put("ConnectionRequest", "sumMerge(tcp_try_connect_total_count) AS ConnectionRequest");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("ConnectionOK", "sumMerge(tcp_connect_ok_total_count) AS ConnectionOK");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("ConnectionClose", "sumMerge(tcp_connect_finclose_total_count) AS ConnectionClose");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("ConnectionRestClose", "sumMerge(tcp_connect_rest_total_count) AS ConnectionRestClose");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("ConnectionTimeoutClose", "sumMerge(tcp_connect_timeout_total_count) AS ConnectionTimeoutClose");
        NETWORK_COLUMNS_MAP_SUM_MERGE.put("Connections", "sumMerge(tcp_max_active_count) AS Connections");
    }




}
