package com.greattimes.ev.utils;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/12/21 17:26
 */
public class ClickHouseEntity implements Serializable{
    private static final long serialVersionUID = 3744782845012843525L;

    private  String clickHouseAddress;
    private  String clickHouseUserName;
    private  String clickHousePassword;
    private  String clickHouseDB;
    private  Integer clickHouseSocketTimeout;

    public ClickHouseEntity(String clickHouseAddress, String clickHouseUserName, String clickHousePassword, String clickHouseDB, Integer clickHouseSocketTimeout) {
        this.clickHouseAddress = clickHouseAddress;
        this.clickHouseUserName = clickHouseUserName;
        this.clickHousePassword = clickHousePassword;
        this.clickHouseDB = clickHouseDB;
        this.clickHouseSocketTimeout = clickHouseSocketTimeout;
    }

    public String getClickHouseAddress() {
        return clickHouseAddress;
    }

    public void setClickHouseAddress(String clickHouseAddress) {
        this.clickHouseAddress = clickHouseAddress;
    }

    public String getClickHouseUserName() {
        return clickHouseUserName;
    }

    public void setClickHouseUserName(String clickHouseUserName) {
        this.clickHouseUserName = clickHouseUserName;
    }

    public String getClickHousePassword() {
        return clickHousePassword;
    }

    public void setClickHousePassword(String clickHousePassword) {
        this.clickHousePassword = clickHousePassword;
    }

    public String getClickHouseDB() {
        return clickHouseDB;
    }

    public void setClickHouseDB(String clickHouseDB) {
        this.clickHouseDB = clickHouseDB;
    }

    public Integer getClickHouseSocketTimeout() {
        return clickHouseSocketTimeout;
    }

    public void setClickHouseSocketTimeout(Integer clickHouseSocketTimeout) {
        this.clickHouseSocketTimeout = clickHouseSocketTimeout;
    }

    @Override
    public String toString() {
        return "ClickHouseEntity{" +
                "clickHouseAddress='" + clickHouseAddress + '\'' +
                ", clickHouseUserName='" + clickHouseUserName + '\'' +
                ", clickHousePassword='" + clickHousePassword + '\'' +
                ", clickHouseDB='" + clickHouseDB + '\'' +
                ", clickHouseSocketTimeout=" + clickHouseSocketTimeout +
                '}';
    }
}
