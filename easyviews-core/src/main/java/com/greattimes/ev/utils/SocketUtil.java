package com.greattimes.ev.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;

/** 
 *  <请求工具类>
 * @author XuYJ
 * 2017-05 
 */  
public class SocketUtil {

    static Logger logger = LoggerFactory.getLogger(SocketUtil.class);
	
	 /**
     * 发送socket请求
     * @param clientIp
     * @param clientPort
     * @param msg
     * @return
     */
	 public static synchronized String sendAlarmMsg(String clientIp,String clientPort,String msg){
	    	
	        String rs = "";
	        
	        if(clientIp==null||"".equals(clientIp)||clientPort==null||"".equals(clientPort)){
	            logger.error("Ip或端口不存在...");
	            return null;
	        }
	        
	        int clientPortInt = Integer.parseInt(clientPort);
	        
	        logger.info("socket请求Ip："+clientIp+" 端口："+clientPort);

	        Socket s = null;
	        DataInputStream dataIS = null;
	        InputStreamReader inSR = null;
	        BufferedReader br = null;
	        DataOutputStream dataOS = null;
	        OutputStreamWriter outSW = null;
	        BufferedWriter bw = null;
	        try {
		        s = new Socket(clientIp, clientPortInt);  
		  
	            dataIS = new DataInputStream(s.getInputStream());  
	            inSR = new InputStreamReader(dataIS, "GBK");  
	            br = new BufferedReader(inSR);  
	              
	            dataOS = new DataOutputStream(s.getOutputStream());  
	            outSW = new OutputStreamWriter(dataOS, "GBK");  
	            bw = new BufferedWriter(outSW);  
	  
                //发送数据  
                bw.write(msg + " \r\n");
                bw.flush();

				char[] arr =new char[8];
				//接收数据
				br.read(arr);
				for (int i = 0; i < arr.length; i++) {
					rs=rs +(char)arr[i];
				}
	              
	        } catch (Exception e) {
//	            logger.error("sendAlarmMsg发送请求异常："+e.getMessage());
	            logger.info("sendAlarmMsg发送请求异常："+e.getMessage());
	        }finally{
	            logger.info("sendAlarmMsg服务器回复："+rs);
	            try {
	                if(inSR!=null){
	                	inSR.close();
	                	inSR = null;
	                }
	                if(dataIS!=null){
	                	dataIS.close();
	                	dataIS = null;
	                }
	                if(dataOS!=null){
	                	dataOS.close();
	                	dataOS = null;
	                }
	                if(s!=null){
	                	s.close();
	                	s = null;
	                }
	            } catch (IOException e) {
	                e.printStackTrace();
	                logger.info("关闭流异常："+e.getMessage());
	            }
	        }
	        return rs;
	    }
}  