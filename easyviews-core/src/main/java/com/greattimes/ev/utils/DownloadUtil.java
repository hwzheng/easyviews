package com.greattimes.ev.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
/**
 * @author NJ
 * @date 2019/6/10 10:09
 */
public class DownloadUtil<T> {

    private List<T> list;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    private static Logger logger = LoggerFactory.getLogger(DownloadUtil.class);

    private DownloadUtil() {
    }

    /**
     * 根据数据创建工作簿
     * @author NJ
     * @date 2019/6/10 10:57
     * @param sheetName
     * @param headers
     * @param data
     * @return org.apache.poi.xssf.usermodel.XSSFWorkbook
     */
    public static <T> XSSFWorkbook downloadExcel(String sheetName, List<Map<String, String>> headers,
                                             List<T> data) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet(sheetName);
        XSSFRow row = sheet.createRow(0);
        // 第四步，创建单元格，并设置值表头 设置表头居中
        XSSFCellStyle style = wb.createCellStyle();
        // 创建一个居中格式
        style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        for (int i = 0; i < headers.size(); i++) {
            XSSFCell cell = row.createCell((short) i);
            cell.setCellValue(headers.get(i).values().iterator().next());
            cell.setCellStyle(style);
        }
        // 第五步，写入实体数据
        T t;
        String key;
        Object property;
        Map map;
        for (int i = 0; i < data.size(); i++) {
            row = sheet.createRow(i + 1);
            t = data.get(i);
            for (int j = 0; j < headers.size(); j++) {
                key = headers.get(j).keySet().iterator().next();
                if(t instanceof  Map){
                    map = (Map)t;
                    if("time".equals(key) || "timeCP".equals(key)){
                        row.createCell((short) j).setCellValue( DateUtils.longToDefaultDateTimeStr(Long.parseLong(evUtil.getMapStrValue(map, key))));
                    }else{
                        row.createCell((short) j).setCellValue(evUtil.getMapStrValue(map, key));
                    }
                }else{
                    property = PropertyUtils.describe(t).get(key);
                    if("time".equals(key) || "timeCP".equals(key)){
                        row.createCell((short) j).setCellValue( DateUtils.longToDefaultDateTimeStr(Long.parseLong(StringUtils.valueOfNullStr(property))));
                    }else{
                        row.createCell((short) j).setCellValue(StringUtils.valueOfNullStr(property));
                    }
                }

            }
        }
        return wb;
    }


    /**
     * 文件流写进前台
     *
     * @param response
     * @param wb
     * @param charCode
     * @param contentType
     * @param headerName
     * @param headerValue
     * @return void
     * @author NJ
     * @date 2019/6/10 10:55
     */
    public static void downloadFileByResp(HttpServletResponse response, XSSFWorkbook wb, String charCode,
                                          String contentType, String headerName, String headerValue) {
        response.setCharacterEncoding(charCode);
        response.setContentType(contentType);
        response.setHeader(headerName, headerValue);
        OutputStream os = null;
        try {
            os = response.getOutputStream();
            wb.write(os);
            if (os != null) {
                os.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("文件流写入前台异常，异常原因：{}", e.getMessage());
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (wb != null) {
                    wb.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                logger.info("关闭流出现异常，异常原因：{}", e.getMessage());
            }
        }
    }
    /**
     * 分多种格式解析单元格的值
     *
     * @param cell  单元格
     * @return  单元格的值
     */
    public static String convertCellToString(Cell cell){
        //如果为null会抛出异常，应当返回空字符串
        if (cell == null)
            return "";

        //POI对单元格日期处理很弱，没有针对的类型，日期类型取出来的也是一个double值，所以同样作为数值类型
        //解决日期2006/11/02格式读入后出错的问题，POI读取后变成“02-十一月-2006”格式
        if(cell.toString().contains("-") && checkDate(cell.toString())){
            String ans = "";
            try {
                ans = DateUtils.getDateStr(cell.getDateCellValue());
            } catch (Exception e) {
                ans = cell.toString();
            }
            return ans;
        }
        return cell.getStringCellValue();
    }

    /**
     * 判断是否是“02-十一月-2006”格式的日期类型
     */
    private static boolean checkDate(String str){
        String[] dataArr =str.split("-");
        try {
            if(dataArr.length == 3){
                int x = Integer.parseInt(dataArr[0]);
                String y =  dataArr[1];
                int z = Integer.parseInt(dataArr[2]);
                if(x>0 && x<32 && z>0 && z< 10000 && y.endsWith("月")){
                    return true;
                }
            }
        } catch (Exception e) {
            return false;
        }
        return false;
    }
}