package com.greattimes.ev.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;

import ru.yandex.clickhouse.ClickHouseConnection;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.settings.ClickHouseProperties;


/**
 * clickhouse连接工具类
 * @author NJ
 * @date 2018/12/21 10:11
 */
public class ClickHouseUtil {


    static Logger logger = LoggerFactory.getLogger(ClickHouseUtil.class);

//    private static String clickHouseAddress;
//    private static String clickHouseUserName;
//    private static String clickHousePassword;
//    private static String clickHouseDB;
//    private static Integer clickHouseSocketTimeout;

    private static String CLICKHOUSE_HOST = "clickhouse_host";
    private static String CLUSTER_URL="clickhouse_cluster_url";
    private static String CLUSTER_USERNAME = "jdbc.username";
    private static String CLUSTER_PASSWORD = "jdbc.password";

//    @Autowired
//    private  ConfigurationCache configurationCache;
//
//    @PostConstruct
//    public void init() {
//        clickHouseUtil = this;
//        clickHouseUtil.configurationCache = this.configurationCache;
//    }

//    private ClickHouseUtil(){}

    /**
     * 保存用户名和密码的map
     */
    private static Map<String, String> map = new HashMap<>();

    static {
        //加载文件
        Properties properties = new Properties();
        InputStream inputStream;
        try {
            inputStream = ClickHouseUtil.class.getClassLoader().getResourceAsStream("config.properties");
            properties.load(inputStream);
            String userName = properties.getProperty(CLUSTER_USERNAME);
            String passWord = properties.getProperty(CLUSTER_PASSWORD);
            map.put(CLUSTER_USERNAME, userName);
            map.put(CLUSTER_PASSWORD, passWord);
            inputStream.close();
        } catch (IOException e) {
            logger.error("获取properties文件clickhouse用户名和密码失败！");
        }
    }





    /**
     * 根据节点的主机名获取连接
     * @author NJ
     * @date 2018/12/26 13:32
     * @param host
     * @return java.sql.Connection
     */
    public static Connection getConn(String host, String url) {

//        String url = prop.getProperty(CLUSTER_URL);
//        url = url.replaceAll("\\{host}", host);

        String userName;
        String passWord;
        if(map == null || map.isEmpty()){
            InputStream inStream = ClickHouseUtil.class.getClassLoader().getResourceAsStream("config.properties");
            Properties prop = new Properties();
            try {
                prop.load(inStream);
                userName = prop.getProperty(CLUSTER_USERNAME);
                passWord = prop.getProperty(CLUSTER_PASSWORD);
                map.put(CLUSTER_USERNAME, userName);
                map.put(CLUSTER_PASSWORD, passWord);
                inStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                logger.error("获取properties文件clickhouse用户名和密码失败！");
                return null;
            }
        }else{
            userName = map.get(CLUSTER_USERNAME);
            passWord = map.get(CLUSTER_PASSWORD);
        }
        ClickHouseConnection conn;
        ClickHouseProperties properties = new ClickHouseProperties();
        properties.setUser(userName);
        properties.setPassword(passWord);

        //jdbc.cluster_url=jdbc:clickhouse://{host}:8123/default
//        String url = clickHouseUtil.configurationCache.getValue(CLUSTER_URL);
        //String url = cache.getValue(CLUSTER_URL);
        logger.info("clickhouse连接的url为:{},host:{}", url,host);
        url = url.replaceAll("\\{host}", host);
        ClickHouseDataSource clickHouseDataSource = new ClickHouseDataSource(url, properties);
        try {
            conn = clickHouseDataSource.getConnection();
            return conn;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.info("clickHouse建立连接失败！连接字符串：{}", url);
        }
        return null;
    }


    /**
     * 执行DDL语句方法
     * @author NJ
     * @date 2018/12/26 12:45
     * @param sql
     * @param host
     * @return boolean
     */
    public static boolean execDDL(String sql, String host,String url) {
        Connection connection = getConn(host, url);
        Statement statement = null;
        boolean result = false;
        try {
            statement = connection.createStatement();
            int affectRow = statement.executeUpdate(sql);
            if (affectRow != -1) {
                result = true;
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("clickHouse执行DDL语句失败,节点主机名称:{},！失败语句：{}", host, sql);
            return result;
        } finally {
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 执行查询语句
     * @author NJ
     * @date 2018/12/26 12:52
     * @param sql
     * @param host
     * @return java.util.List<com.alibaba.fastjson.JSONObject>
     */
    public static List<JSONObject> execQuery(String sql, String host, String url) {
        List<JSONObject> list = new ArrayList();
        Connection connection = getConn(host, url);
        Statement statement = null;
        try {
            statement = connection.createStatement();
            ResultSet results = statement.executeQuery(sql);
            ResultSetMetaData metaData = results.getMetaData();
            while(results.next()){
                JSONObject row = new JSONObject();
                for(int i = 1;i <= metaData.getColumnCount();i++){
                    row.put(metaData.getColumnName(i),results.getString(metaData.getColumnName(i)));
                }
                list.add(row);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("clickHouse执行QUERY语句失败,节点主机名:{},！失败语句：{}", host, sql);
            return list;
        } finally {
            //关闭连接
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 为集群创建每个节点创建本地表
     * @author NJ
     * @date 2018/12/21 18:27
     * @param cache
     * @param insertSql
     * @return boolean
     */
    public static boolean executeCreateTableByCache(ConfigurationCache cache, String insertSql,String url){
        boolean result = false;
        if(cache == null){
            throw new IllegalArgumentException("参数错误！缓存ConfigurationCache未注入!");
        }
        if(StringUtils.isBlank(insertSql)){
            return result;
        }
        String hosts = cache.getValueFromRedisThenDb(CLICKHOUSE_HOST);
        if(StringUtils.isBlank(hosts)){
            throw new IllegalArgumentException("参数错误！集群节点配置非法！无clickhouse_host参数");
        }
        boolean execResult = true;
        boolean connResult;
        String[] hostArr = hosts.split("\\,");
        for(int i = 0; i < hostArr.length; i++){
            connResult = execDDL(insertSql,hostArr[i], url);
            if(!connResult){
                execResult = false;
            }
        }
        return execResult;
    }

    /**
     * 判断表是否存在
     * @author NJ
     * @param sql
     * @param host
     * @param cache
     * @return
     */
    public static boolean isExistTable(String sql, String host, String url){
        List<JSONObject> list = execQuery(sql, host,url);
        boolean flag = false;
        if (!evUtil.listIsNullOrZero(list)) {
            if("1".equals(list.get(0).getString("result"))){
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 从缓存中获取集群的各个节点host
     * @author NJ
     * @date 2018/12/24 10:21
     * @param cache
     * @return java.util.List<com.greattimes.ev.utils.ClickHouseEntity>
     */
    public static List<String> getClickHouseEntityFromCache(ConfigurationCache cache){
        if(cache == null){
            throw new IllegalArgumentException("参数错误！缓存ConfigurationCache未注入!");
        }
        List<String> result = new ArrayList<>();
        String hosts = cache.getValueFromRedisThenDb(CLICKHOUSE_HOST);
        if(StringUtils.isNotBlank(hosts)){
            result =  Arrays.asList(hosts.split(","));
        }
        return result;
    }

    public static void main(String[] args) {
        String url = "jdbc:clickhouse://{host}:8123/default{xxx}fwefw{";
        url = url.replaceAll("\\{host}", "TTT");
        System.out.println(url);

//        //"jdbc:clickhouse://192.168.0.161:8123/default?option1=one%20two&option2=y");
//        String clickHouseAddress = "jdbc:clickhouse://192.168.0.161:8123";
//        String clickHouseUserName = "default";
//        String clickHousePassword = "qwer$#@!1234" ;
//        String clickHouseDB = "default";
//        Integer clickHouseSocketTimeout = 10000;
//        ClickHouseEntity entity = new ClickHouseEntity(clickHouseAddress, clickHouseUserName,clickHousePassword,  clickHouseDB, clickHouseSocketTimeout);
//        String sql  = "EXISTS TABLE aa";
//        String url = "jdbc:clickhouse://192.168.0.161:8123/default";
//        List<JSONObject> jsonObjects  = execQuery(sql, url);
//        System.out.println(jsonObjects);
//        String sql = "create table bpmTran_test1\n" +
//                "(\n" +
//                "  timeHour           Int64,\n" +
//                "  allStart2ends      Float64,\n" +
//                "  allTransCount      Int64,\n" +
//                "  applicationId      Int32,\n" +
//                "  bGroupId           Int32,\n" +
//                "  componentId        Int32,\n" +
//                "  date               Date,\n" +
//                "  level              Int32,\n" +
//                "  responseTransCount Int64,\n" +
//                "  successTransCount  Int64,\n" +
//                "  time               Int64,\n" +
//                "  uuid               Int32,\n" +
//                "  allTransCountBak   Int64 alias allTransCount\n" +
//                ")\n" +
//                "  engine = MergeTree\n" +
//                "    PARTITION BY date ORDER BY (time, date, uuid, bGroupId, level, applicationId, componentId)\n" +
//                "    SETTINGS index_granularity = 8192;";
//        ClickHouseUtil.execDDL(sql, clickHouseAddress, clickHouseUserName,clickHousePassword,clickHouseDB,clickHouseSocketTimeout);

    }



}
