package com.greattimes.ev.utils;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/**
 * 手动获取spring容器中bean
 * @author NJ
 * @date 2018/8/23 14:18
 */
@Component
@Lazy(false)
public class SpringUtils implements ApplicationContextAware,DisposableBean {

    private static ApplicationContext applicationContext = null;
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringUtils.applicationContext = applicationContext;
    }

    @Override
    public void destroy() throws Exception {
        SpringUtils.applicationContext = null;
    }

    public static void isInjected(){
        if(SpringUtils.applicationContext == null){
            throw new RuntimeException("springUtils applicationContext is not injected!");
        }
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String beanName){
        isInjected();
        return (T) applicationContext.getBean(beanName);
    }
    @SuppressWarnings("unchecked")
    public static <T> T getBean(Class<T> requiredType){
        isInjected();
        return applicationContext.getBean(requiredType);
    }
}
