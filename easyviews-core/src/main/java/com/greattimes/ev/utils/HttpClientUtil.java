package com.greattimes.ev.utils;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.message.PAB.Message;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.misc.BASE64Encoder;

import com.alibaba.fastjson.JSON;

/** 
 *  <HTTP请求工具类>
 * @author 
 * 2015-11 
 */  
public class HttpClientUtil {

    private static Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);

    private static Pattern p = Pattern.compile("\\s*|\t|\r|\n");

    /**
     * 新增连接超时参数，单位毫秒
     */
//    static final int CONNECT_REQUEST_TIMEOUT = 500;
    static final int CONNECT_TIMEOUT = 30000;
    static final int SOCKET_TIMEOUT = 30000;

    private static PoolingHttpClientConnectionManager cm;
    private static String EMPTY_STR = "";  
    private static String UTF_8 = "UTF-8";  
    private static void init(){  
        if(cm == null){  
            cm = new PoolingHttpClientConnectionManager();
            //整个连接池最大连接数
            cm.setMaxTotal(50);
            //每路由最大连接数，默认值是2
            cm.setDefaultMaxPerRoute(5);
        }
    }

    /**
     * 通过连接池获取HttpClient 
     * @return 
     */  
    private static CloseableHttpClient getHttpClient(){
        int timeOut;
        try {
            ConfigurationCache cache = SpringUtils.getBean(ConfigurationCache.class);
            if(cache == null){
                logger.info("从缓存中获取参数：{}失败！原因：{}","socket_timeout","未获取缓存实例");
                timeOut = SOCKET_TIMEOUT;
            }else{
                timeOut = Integer.parseInt(cache.getValue("socket_timeout")) * 1000;
            }
        }catch (Exception e){
            logger.info(e.getMessage());
            timeOut = SOCKET_TIMEOUT;
        }
        init();
        RequestConfig requestConfig = RequestConfig.custom()
//                .setConnectionRequestTimeout(CONNECT_REQUEST_TIMEOUT)
//                .setConnectTimeout(CONNECT_TIMEOUT)
                .setSocketTimeout(timeOut)
                .build();
        return HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(requestConfig).build();
    }


    /** 
     *  
     * @param url 
     * @return 
     */  
    public static String httpGetRequest(String url){  
        HttpGet httpGet = new HttpGet(url);  
        return getResult(httpGet);
    }  
      
    public static String httpGetRequest(String url, Map<String, Object> params) throws URISyntaxException{  
        URIBuilder ub = new URIBuilder();  
        ub.setPath(url);  
          
        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);  
        ub.setParameters(pairs);  
          
        HttpGet httpGet = new HttpGet(ub.build());  
        return getResult(httpGet);  
    }  
      
    public static String httpGetRequest(String url, Map<String, Object> headers,   
            Map<String, Object> params) throws URISyntaxException{  
        URIBuilder ub = new URIBuilder();  
        ub.setPath(url);  
          
        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);  
        ub.setParameters(pairs);  
          
        HttpGet httpGet = new HttpGet(ub.build());  
        for (Map.Entry<String, Object> param: headers.entrySet()) {  
            httpGet.addHeader(param.getKey(), param.getValue()+"");
        }  
        return getResult(httpGet);  
    }  
      
    public static String httpPostRequest(String url){  
        HttpPost httpPost = new HttpPost(url);  
        return getResult(httpPost);  
    }  
      
    public static String httpPostRequest(String url, Map<String, Object> params) throws UnsupportedEncodingException{  
        HttpPost httpPost = new HttpPost(url);  
        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);  
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, UTF_8));  
        return getResult(httpPost);  
    }  
      
    public static String httpPostRequest(String url, Map<String, Object> headers,   
            Map<String, Object> params) throws UnsupportedEncodingException{  
        HttpPost httpPost = new HttpPost(url);  
          
        for (Map.Entry<String, Object> param: headers.entrySet()) {  
            httpPost.addHeader(param.getKey(), param.getValue()+"");  
        }  
          
        ArrayList<NameValuePair> pairs = covertParams2NVPS(params);  
        httpPost.setEntity(new UrlEncodedFormEntity(pairs, UTF_8));  
          
        return getResult(httpPost);  
    }


    public static String sendMsg(String url,Message msg) throws ConnectTimeoutException, SocketTimeoutException,ClientProtocolException, IOException{
        HttpClient client = getHttpClient();
        HttpPost post = new HttpPost(url);
        logger.info("请求地址 :>>"+url);
        logger.info("请求原始参数："+msg.toString());
        // 创建表单参数列表
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        qparams.add(new BasicNameValuePair("mobiles", msg.getMobiles()));
        qparams.add(new BasicNameValuePair("content",msg.getContent()));
        post.setEntity(new UrlEncodedFormEntity(qparams,"GBK"));
        //提交请求
        long starttime=System.currentTimeMillis();
        String result = null;
        logger.info("请求开始时间：【"+starttime+"】");
        try{
            HttpResponse response = client.execute(post);
            long endtime=System.currentTimeMillis();
            logger.info("请求响应用时：【"+(endtime-starttime)+"】毫秒 ,响应码【"+response.getStatusLine().getStatusCode()+"】");
            //响应处理
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                entity = new BufferedHttpEntity(entity);
                InputStream in = entity.getContent();
                byte[] read = new byte[1024];
                byte[] all = new byte[0];
                int num;
                while ((num = in.read(read)) > 0) {
                    byte[] temp = new byte[all.length + num];
                    System.arraycopy(all, 0, temp, 0, all.length);
                    System.arraycopy(read, 0, temp, all.length, num);
                    all = temp;
                }
                result = new String(all,"UTF-8");
                logger.info("响应输出："+result);
                if (null != in) {
                    in.close();
                }
            }
            post.abort();
        }catch(Exception e){
            logger.error("请求异常："+e);
            return null;
        }
        return result;
    }


    /**
     *map参数转换成 NameValuePair集合
     * @param params
     * @return
     * ***/
    private static ArrayList<NameValuePair> covertParams2NVPS(Map<String, Object> params){  
        ArrayList<NameValuePair> pairs = new ArrayList<NameValuePair>();  
        for (Map.Entry<String, Object> param: params.entrySet()) {  
            pairs.add(new BasicNameValuePair(param.getKey(), param.getValue()+""));  
        }  
        return pairs;  
    }  
      
      
    /** 
     * 处理Http请求 
     * @param request
     * @return
     */  
    private static String getResult(HttpRequestBase request){
        //CloseableHttpClient httpClient = HttpClients.createDefault();  
        CloseableHttpClient httpClient = getHttpClient();  
        try{  
            CloseableHttpResponse response = httpClient.execute(request);  
            //response.getStatusLine().getStatusCode();  
            HttpEntity entity = response.getEntity();  
            if(entity!=null){  
                //long len = entity.getContentLength();// -1 表示长度未知  
                String result = EntityUtils.toString(entity);
                response.close();  
                //httpClient.close();  
                return result;  
            }  
        }catch(ClientProtocolException e){
            e.printStackTrace();
            logger.info("");
        }catch(IOException e){
            e.printStackTrace();
        }finally{

        }
        return EMPTY_STR;
    }  
    
    /***
     * 带消息头的普通参数psot请求
     * @param url
     * @param headers
     * @param params
     * @return
     */
    public static String posts(String url, Map<String, Object> headers,Map<String, Object> params){
    	logger.info("请求头:" + JSON.toJSONString(headers));  
    	logger.info("请求体:" + JSON.toJSONString(params));  
        logger.info("请求地址:" + url);  
        String body = null;  
        HttpClient client = getHttpClient();  
        HttpPost method = new HttpPost(url);  
        if (method != null){
        	try {
        		for (Map.Entry<String, Object> param: headers.entrySet()) {  
        			method.addHeader(param.getKey(), param.getValue()+"");
        		}  
        		ArrayList<NameValuePair> pairs=covertParams2NVPS(params);
				method.setEntity(new UrlEncodedFormEntity(pairs, UTF_8));
				long  startTime = System.currentTimeMillis();  
				HttpResponse response = client.execute(method);
				long  endTime = System.currentTimeMillis();  
				int statusCode = response.getStatusLine().getStatusCode();  
				logger.info("statusCode:" + statusCode);  
	            logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));  
	        	HttpEntity resEntity = response.getEntity();
	        	if (resEntity != null) {
	        		logger.info("Response content length: " + resEntity.getContentLength());
	        	}
	        	body = EntityUtils.toString(resEntity, "UTF-8");
	            logger.info(" 响应输出:>>"+body);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info(" 转码异常:>>"+e.getMessage());
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info(" 请求协议异常:>>"+e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				logger.info(" 网络错误:>>"+url);
			}
        }
    	return body;
    }
    
    /** 
     * json格式请求
     * @param parameters 
     * @return 
     */  
    public static String posts(String url, String parameters) throws IOException{
        String body = null;  
        logger.info("输入参数:" + parameters);  
        logger.info("请求地址:" + url);  
        HttpClient client = getHttpClient();  
        HttpPost method = new HttpPost(url);  
        if (method != null & parameters != null  
                && !"".equals(parameters.trim())) {  
            // 建立一个NameValuePair数组，用于存储欲传送的参数
            method.addHeader("Content-type","application/json; charset=utf-8");
            method.setHeader("Accept", "application/json;charset=utf-8");
            method.setEntity(new StringEntity(parameters, Charset.forName("UTF-8")));
            long startTime = System.currentTimeMillis();
            HttpResponse response = client.execute(method);
            long  endTime = System.currentTimeMillis();
            int statusCode = response.getStatusLine().getStatusCode();
            logger.info("statusCode:" + statusCode);
            logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));
            body = EntityUtils.toString(response.getEntity());
            logger.info(" 响应输出:>>"+body);
        }
        return body;  
    }

    /**
     * json格式请求
     * @param parameters
     * @return
     */
    public static String postsHeader(String url, String parameters) throws IOException{
        String body = null;
        logger.info("输入参数:" + parameters);
        logger.info("请求地址:" + url);
        HttpClient client = getHttpClient();
        HttpPost method = new HttpPost(url);
        if (method != null & parameters != null
                && !"".equals(parameters.trim())) {
            // 建立一个NameValuePair数组，用于存储欲传送的参数
            method.addHeader("Content-type","application/json; charset=utf-8");
            method.setEntity(new StringEntity(parameters, Charset.forName("UTF-8")));
            long startTime = System.currentTimeMillis();
            HttpResponse response = client.execute(method);
            long  endTime = System.currentTimeMillis();
            int statusCode = response.getStatusLine().getStatusCode();
            logger.info("statusCode:" + statusCode);
            logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));
            body = EntityUtils.toString(response.getEntity());
            logger.info(" 响应输出:>>"+body);
        }
        return body;
    }
    
    /** 
     * json格式请求
     * @param url
     * @param parameters 
     * @param charset 接收字符集
     * @return 
     */  
    public static String posts(String url, String parameters,String charset) throws Exception{
        String body = null;  
        logger.info("输入参数:" + parameters);  
        logger.info("请求地址:" + url);  
        HttpClient client = getHttpClient();  
        HttpPost method = new HttpPost(url);  
        if (method != null & parameters != null  
                && !"".equals(parameters.trim())) {  
            try {  
                // 建立一个NameValuePair数组，用于存储欲传送的参数  
                method.addHeader("Content-type","application/json; charset=utf-8");  
                method.setHeader("Accept", "application/json;charset=utf-8");  
                method.setEntity(new StringEntity(parameters, Charset.forName("UTF-8")));  
                long startTime = System.currentTimeMillis();  
                HttpResponse response = client.execute(method);  
                long  endTime = System.currentTimeMillis();  
                int statusCode = response.getStatusLine().getStatusCode();  
                logger.info("statusCode:" + statusCode);  
                logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));  
                body = EntityUtils.toString(response.getEntity(),charset);  
                logger.info(" 响应输出:>>"+body);
            } catch (IOException e) {  
            	 logger.info(" 网络错误:>>"+url);
            } finally {  
               // logger.info("调用接口状态：" + status);  
            }  
        }  
        return body;  
    }  
    
    /** 
     * Luoxiao 2016-8-19
     * json格式请求
     * @param parameters 
     * @return map格式.带status状态码，便于获取post请求正常("200")和非正常状态 
     */  
    public static Map<String,Object> postsWithMapReturn(String url, String parameters) throws IOException{
        String body = null;
        Map<String,Object>  map=new HashMap<String,Object>();
        logger.info("输入参数:" + parameters);  
        logger.info("请求地址:" + url);  
        HttpClient client = getHttpClient();  
        HttpPost method = new HttpPost(url);
        
        if (method != null & parameters != null  
                && !"".equals(parameters.trim())) {  
                // 建立一个NameValuePair数组，用于存储欲传送的参数
            method.addHeader("Content-type","application/json; charset=utf-8");
            method.setHeader("Accept", "application/json");
            method.setEntity(new StringEntity(parameters, Charset.forName("UTF-8")));
            long startTime = System.currentTimeMillis();
            HttpResponse response = client.execute(method);
            long  endTime = System.currentTimeMillis();
            int statusCode = response.getStatusLine().getStatusCode();
            logger.info("statusCode:" + statusCode);
            logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));
            body = EntityUtils.toString(response.getEntity());
            map.put("body", body);
            map.put("statusCode", String.valueOf(statusCode));
            logger.info(" 响应输出:>>"+body);
        }
        return map;  
    }
    
    
    /**
     * <带有64位加密的post>
     * */
    public static String postBase64(String url,String json) throws ClientProtocolException, IOException{
    	  HttpClient client = getHttpClient();   
    	  HttpPost post = new HttpPost(url);  
    	  logger.info("请求地址 :>>"+url);
    	  logger.info("请求原始参数："+json); 
    	  String param=changeToBase64(json);
    	  logger.info("请求加密后参数："+param);
    	  // 创建表单参数列表    
          List<NameValuePair> qparams = new ArrayList<NameValuePair>();   
          qparams.add(new BasicNameValuePair("input.string",param));  
          post.setEntity(new UrlEncodedFormEntity(qparams,"UTF-8")); 
          //提交请求
          long starttime=System.currentTimeMillis();
          String result = null;
          logger.info("请求开始时间：【"+starttime+"】");
          try{
        	  HttpResponse response = client.execute(post);
             
	          long endtime=System.currentTimeMillis();
	          logger.info("请求statusCode：【"+response.getStatusLine().getStatusCode()+"】");
	          logger.info("请求响应用时：【"+(endtime-starttime)+"】毫秒");
	          //响应处理
	          HttpEntity entity = response.getEntity();  
	          if (entity != null) {  
	              entity = new BufferedHttpEntity(entity);  
	              InputStream in = entity.getContent();  
	              byte[] read = new byte[1024];  
	              byte[] all = new byte[0];  
	              int num;  
	              while ((num = in.read(read)) > 0) {  
	                  byte[] temp = new byte[all.length + num];  
	                  System.arraycopy(all, 0, temp, 0, all.length);  
	                  System.arraycopy(read, 0, temp, all.length, num);  
	                  all = temp;  
	              }  
	              result = new String(all,"UTF-8");  
	              logger.info("响应输出："+result);
	              if (null != in) {  
	                  in.close();  
	              }  
	          }  
	          post.abort();  
          }catch(Exception e){
        	  logger.error("请求异常："+e);
          }
          return result;  
      }  
    
    
    /**
     * 外呼发送
     * @param url
     * @param password
     * @param message
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    public static String sendOutbound(String url,String password, Message message) throws ClientProtocolException, IOException{
        HttpClient client = getHttpClient();
        HttpPost post = new HttpPost(url);
        logger.info("请求地址 :>>"+url);
        // 创建表单参数列表
        List<NameValuePair> qparams = new ArrayList<NameValuePair>();
        qparams.add(new BasicNameValuePair("passwd",password));
        qparams.add(new BasicNameValuePair("issendmail","0"));
        qparams.add(new BasicNameValuePair("issendsms","0"));
        qparams.add(new BasicNameValuePair("iscall","1"));
        qparams.add(new BasicNameValuePair("callmobileno",message.getMobiles()));
        qparams.add(new BasicNameValuePair("callcontent",message.getContent()));
        qparams.add(new BasicNameValuePair("monitorsys","Easyview监控系统"));
        qparams.add(new BasicNameValuePair("systemname","Easyview监控系统"));
        post.setEntity(new UrlEncodedFormEntity(qparams,"GBK"));
        logger.info("请求原始参数："+qparams);
        //提交请求
        long starttime=System.currentTimeMillis();
        String result = null;
        logger.info("请求开始时间：【"+starttime+"】");
        HttpResponse response = client.execute(post);
        long endtime=System.currentTimeMillis();
        logger.info("请求响应用时：【"+(endtime-starttime)+"】毫秒 ,响应码【"+response.getStatusLine().getStatusCode()+"】");
        //响应处理
        HttpEntity entity = response.getEntity();
        if (entity != null) {
            entity = new BufferedHttpEntity(entity);
            InputStream in = entity.getContent();
            byte[] read = new byte[1024];
            byte[] all = new byte[0];
            int num;
            while ((num = in.read(read)) > 0) {
                byte[] temp = new byte[all.length + num];
                System.arraycopy(all, 0, temp, 0, all.length);
                System.arraycopy(read, 0, temp, all.length, num);
                all = temp;
            }
            result = new String(all,"UTF-8");
            logger.info("响应输出："+result);
            if (null != in) {
                in.close();
            }
        }
        post.abort();
        return result;
    }


    /**
     * <加密请求 并将"="替换成"()">
     * @param str
     * @return
     */
 	public static String changeToBase64(String str) {
 		byte[] b = null;
 		String s = null;
 		try {
 			b = str.getBytes("utf-8");
 		} catch (UnsupportedEncodingException e) {
 			e.printStackTrace();
 		}
 		if (b != null) {
 			s = new BASE64Encoder().encode(b);
            Matcher m = p.matcher(s);
            s = m.replaceAll("");
 			s=s.replaceAll("=", "\\(\\)");
 		}
 		return s;
 	}
 	 /**
     * 64位加密
     * */
    public static String changeToBase64ChangeStr(String str) {
		byte[] b = null;
		String s = null;
		try {
			b = str.getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (b != null) {
			s = new BASE64Encoder().encode(b);
			if(s.length()>2&&s.substring(s.length()-2,s.length()).equals("==")){
				s=s.substring(0,s.length()-2)+"()";
			}
			if(s.length()>1&&s.substring(s.length()-1,s.length()).equals("=")){
				s=s.substring(0,s.length()-1)+"()";
			}
            Matcher m = p.matcher(s);
            s = m.replaceAll("");
		}
		return s;
	}
 	
    /**
     * 64位加密
     * */
    public static String changeToBase64SubStr(String str) {
		byte[] b = null;
		String s = null;
		try {
			b = str.getBytes("utf-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		if (b != null) {
			s = new BASE64Encoder().encode(b);
			if(s.length()>2&&s.substring(s.length()-2,s.length()).equals("==")){
				s=s.substring(0,s.length()-2);
			}
			if(s.length()>1&&s.substring(s.length()-1,s.length()).equals("=")){
				s=s.substring(0,s.length()-1);
			}
            Matcher m = p.matcher(s);
            s = m.replaceAll("");
		}
		return s;
	}
    /**post请求返回流形式的处理
     * @param url
     * @param parameters
     * @return
     */
    public static Map<String,Object> getPostStream(String url, String parameters) throws IOException{
        String body = null;
        Map<String,Object>  map=new HashMap<String,Object>();
        logger.info("输入参数:" + parameters);  
        logger.info("请求地址:" + url);  
        HttpClient client = getHttpClient();  
        HttpPost method = new HttpPost(url);
        
        if (method != null & parameters != null  
                && !"".equals(parameters.trim())) {  
                // 建立一个NameValuePair数组，用于存储欲传送的参数
            method.setEntity(new StringEntity(parameters, Charset.forName("UTF-8")));
            long startTime = System.currentTimeMillis();
            HttpResponse response = client.execute(method);
            long  endTime = System.currentTimeMillis();
            int statusCode = response.getStatusLine().getStatusCode();
            logger.info("statusCode:" + statusCode);
            logger.info("调用API 花费时间(单位：毫秒)：" + (endTime - startTime));
            HttpEntity entity = response.getEntity();

            logger.info("1.Get Response Status: " + response.getStatusLine());
            InputStream in = null;
            if (entity != null) {
                logger.info("  Get ResponseContentEncoding():"+entity.getContentEncoding());
                logger.info("  Content Length():"+entity.getContentLength());
                //getResponse
                in=entity.getContent();
            }
            map.put("body", in);
            map.put("statusCode", String.valueOf(statusCode));
            logger.info(" 响应输出:>>"+body);
        }
        return map;  
    }
}  