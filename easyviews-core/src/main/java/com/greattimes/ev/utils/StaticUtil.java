package com.greattimes.ev.utils;
import com.greattimes.ev.Thread.TaskLogsInsert;
import com.greattimes.ev.system.entity.TaskLogs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * 队列静态缓存
 * @author LH
 *
 */
public class StaticUtil {

	static Logger log = LoggerFactory.getLogger(StaticUtil.class);
	
	/**定时任务日志记录队列*/
	public static BlockingQueue<TaskLogs> taskLogsQueue = new ArrayBlockingQueue<>(3000);

	/**告警提示短信监控队列*/
	public static BlockingQueue<String> alarmMsgQueue=new ArrayBlockingQueue<>(3000);

	/**外呼告警提示监控队列*/
	public static BlockingQueue<String> outAlarmMsgQueue=new ArrayBlockingQueue<>(3000);

	@PostConstruct
	public void init(){
		Thread t = new Thread(new TaskLogsInsert());
		t.start();
	}
}
