package com.greattimes.ev.email;

import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.MailUtils;
import com.greattimes.ev.utils.StaticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.Session;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2018/10/12 19:24
 */

//@Component(value="mailSend")
public class MailSend {
    /**
     * 告警邮件发送主机
     */
    public static final String ALARM_MAIL_HOST = "alarm_mail_host";
    /**
     * 告警邮件发送人
     */
    public static final String ALARM_MAIL_FROM_NAME = "alarm_mail_from_name";

    Logger log = LoggerFactory.getLogger(this.getClass());

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMailSendJob";

    @Autowired
    private ConfigurationCache configurationCache;
    @Autowired
    private IMessageService messageService;



    public void sendMail(){
        Date start = new Date();
        /**
         * 发送类型 （同告警发送方式一致） type 1 邮件 ，2 短信 ，3app ，4外呼
         * 状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
         */
        //查询待发送数据
        List<com.greattimes.ev.system.entity.Message> msgs = messageService.selectAlarmMessageByParam(1, 0);
        if(evUtil.listIsNullOrZero(msgs)){
            log.info("无相应的告警邮件信息有待发送！");
            return ;
        }
        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        msgs.stream().forEach(msg->{
            msg.setState(1);
        });

        messageService.updateBatchById(msgs);
        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("邮件发送-无相应的发送用户!定时任务执行结束");
            return ;
        }
        //转化为map
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        int successNum = 0,messageId;
        //邮件发送主题
        String title,fromName,host;
        List<Integer>  errorIds = new ArrayList<>();
        Integer groupId;
        for (com.greattimes.ev.system.entity.Message message : msgs) {
            messageId =  message.getId();
            Date execTime = new Date();
            TaskLogs taskLogs1 = new TaskLogs();
            taskLogs1.setStat(2);
            taskLogs1.setStarttimes(execTime);
            taskLogs1.setClassName(className);
            try {
                groupId = Integer.parseInt(message.getAlarmGroupId());
                //告警组对应的用户列表
                if(userAlarmMap.get(groupId) == null){
                    continue;
                }
                List<UserParam> userList = userAlarmMap.get(groupId).getUsers();
                //告警组对应的用户列表
                if(evUtil.listIsNullOrZero(userList)){
                    continue;
                }
                //邮件发送人名
                fromName = configurationCache.getValue(ALARM_MAIL_FROM_NAME);
                log.info("告警邮件发送人名称：  {}",fromName);
                //邮件发送主机
                host = configurationCache.getValue(ALARM_MAIL_HOST);
                log.info("邮件发送主机是：  {}",host);
                title = message.getMessage();
                Mail mail = new Mail(fromName, "", title,message.getMessage());

                for (UserParam userParam : userList) {
                    mail.addToAddress(userParam.getEmail());
                }
                Session session = MailUtils.createSessionWithoutValidation(host);
                MailUtils.send(session, mail);
                //更新告警信息状态
                //更新状态(状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败)
                messageService.updateAlarmMessageState(messageId,2);
                log.info("告警信息表id为:{}的告警邮件发送成功！", messageId);
                taskLogs1.setDesc("数据时间："+ DateUtils.format(new Date(message.getTimestamp()), "yyyy-MM-dd HH:mm:ss")
                        +"处理数据条数：1条");
                successNum ++ ;
            } catch (Exception e) {
                e.printStackTrace();
                errorIds.add(messageId);
                messageService.updateAlarmMessageState(messageId,3);
                log.info("发生异常,告警信息表id为:{}的告警邮件发送失败！",messageId);
                taskLogs1.setStat(3);
                taskLogs1.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
                taskLogs1.setDesc("发生异常,告警信息表id为"+messageId+"的告警短信发送失败！");
                taskLogs1.setDuration(((Long)(System.currentTimeMillis()- execTime.getTime())).intValue());
                StaticUtil.taskLogsQueue.add(taskLogs1);
            }
        }
        Date end = new Date();
        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(start);
        taskLogs.setClassName(className);
        //记录日志
        if(msgs.size() > 0){
            if(msgs.size() == successNum){
                //成功
                taskLogs.setStat(0);
                taskLogs.setDetail("邮件告警任务执行完成");
                taskLogs.setDesc("邮件发送成功！成功条数：" + msgs.size());
            }else if(successNum > 0 ){
                //部分成功
                taskLogs.setStat(1);
                taskLogs.setDetail("邮件告警任务执行完成");
                taskLogs.setDesc("部分邮件发送成功！成功条数：" + successNum + " 失败条数：" + errorIds.size()
                    + "发送失败告警信息id：" + errorIds.toString());
            }else{
                //失败(所有都失败)
                taskLogs.setStat(3);
                taskLogs.setDetail("邮件告警任务执行完成");
                taskLogs.setDesc("邮件发送失败！失败条数："+ errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
            }
            taskLogs.setDuration(((Long)(end.getTime()- start.getTime())).intValue());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }
        log.info("邮件告警任务执行完成，花费时间："+(end.getTime()-start.getTime())+"ms");
    }
}
