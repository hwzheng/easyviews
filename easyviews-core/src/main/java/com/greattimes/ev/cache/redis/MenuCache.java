package com.greattimes.ev.cache.redis;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.fastjson.JSON;
import com.greattimes.ev.cache.Cache;
import com.greattimes.ev.cache.IMenu;
import com.greattimes.ev.common.redis.RedisConstant;
import com.greattimes.ev.common.redis.RedisDao;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.service.IMenuService;
import com.greattimes.ev.system.service.IUserService;

public class MenuCache implements Cache ,IMenu {
	@Autowired
	private IMenuService menuService;
	
	@Autowired
	public RedisDao redis;
	Logger log=LoggerFactory.getLogger(this.getClass()); 
	private String key(String key){
		return RedisConstant.USER_MENU.concat(key);
	}
	
	@Override
	public void initCache() {
		// TODO Auto-generated method stub
		//a.id as uid, d.id,d.parent_id AS parentId ,d.ename,d.href ,d.sort ,d.icon,d.cname
		log.info(INIT_START);
		try {
			clearCache();
			List<Map<String, Object>> menus=menuService.getRoleMenus();
			menus.stream().forEach(menu->{
				String key=key(evUtil.getMapStrValue(menu, "roleId").concat(".").concat( evUtil.getMapStrValue(menu, "ename")));
				redis.add(key, menu);
			});
		} catch (Exception e) {
			// TODO: handle exception
			log.error(INIT_ERROR);
			log.error(e.toString());
		}
		log.info(INIT_END);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		Set<String> keys=redis.keys(RedisConstant.USER_MENU+"*");
		redis.delete(keys);
	}

	@Override
	public String getEname(String key) {
		// TODO Auto-generated method stub
		String menu=redis.get(key(key));
		if(menu==null){
			return null;
		}
		return  JSON.parseObject(menu).getString("ename");
	}

	@Override
	public String getUri(String key) {
		// TODO Auto-generated method stub
		Map<String, Object> menus=redis.get(key(key), Map.class);
		return evUtil.getMapStrValue(menus, "href");
	}
}
