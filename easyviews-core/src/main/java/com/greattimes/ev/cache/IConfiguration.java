package com.greattimes.ev.cache;

import com.greattimes.ev.system.entity.Configuration;

/**
 * 系统配置缓存
 * @author LiHua
 * @since 2018-03-22
 */
public interface IConfiguration {
	
	String INIT_ERROR="配置缓存初始报错！";
	String INIT_START="配置缓存初始化开始！";
	String INIT_END="配置缓存初始成功！";
	
	
	/**
	 * 根据key获取系统参数配置值
	 * @param key
	 * @return
	 */
	String getValue(String key);

	/**
	 * 根据key获取系统参数配置值
	 * 如果key不存在则返回null
	 * @param key
	 * @return
	 */
	String getDefaultValue(String key);
	/**
	 * 根据key获取系统参数配置
	 * @param key
	 * @return
	 */
	Configuration get(String key);
	
	/**
	 * 删除
	 * @param key
	 * @return
	 */
	void delete(String key);
	
	/**
	 * 更新值
	 * @param key
	 * @param value
	 */
	void update(String key,String value);
	
	/***
	 * 更新整个对象
	 * @param key
	 * @param config
	 */
	void update(String key, Configuration config);
	
	/**
	 * 新增
	 * @param key
	 * @param config
	 */
	void add(String key,Configuration config);


	/**
	 *
	 * @param key
	 * @return
	 */
	String getValueFromRedisThenDb(String key);

}
