package com.greattimes.ev.cache;

public interface IMenu {
	String INIT_ERROR="配置菜单缓存初始报错！";
	String INIT_START="配置菜单缓存初始化开始！";
	String INIT_END="配置菜单缓存初始成功！";
	/**
	 * 获取权限名称
	 * @param key
	 * @return
	 */
	String getEname(String key);
	
	/**
	 * 获取路径
	 * @param key
	 * @return
	 */
	String getUri(String key);
	
}
