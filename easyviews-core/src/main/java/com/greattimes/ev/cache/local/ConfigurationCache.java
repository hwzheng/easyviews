package com.greattimes.ev.cache.local;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.greattimes.ev.common.exception.RedisKeyGetException;
import com.greattimes.ev.common.exception.RedisKeyNotFoundException;
import com.greattimes.ev.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.greattimes.ev.cache.Cache;
import com.greattimes.ev.cache.IConfiguration;
import com.greattimes.ev.system.entity.Configuration;
import com.greattimes.ev.system.service.IConfigurationService;

public class ConfigurationCache implements Cache ,IConfiguration{
	
	Logger log=LoggerFactory.getLogger(this.getClass()); 
	
	@Autowired
	public IConfigurationService config;
	
	private static Map<String, Configuration> cache=new HashMap<>();
	
	@Override
	public void initCache(){
		// TODO Auto-generated method stub
		log.info(INIT_START);
		try {
			List<Configuration> sysConfigs=config.selectList(null);
			sysConfigs.stream().forEach(cnf->{
				String key =cnf.getKey();
				cache.put(key, cnf);
			});
		} catch (Exception e) {
			// TODO: handle exception
			log.error(INIT_ERROR);
			log.error(e.toString());
		}
		log.info(INIT_END);
	}
	
	@Override
	public void clearCache(){
		// TODO Auto-generated method stub
		cache.clear();
	}

	@Override
	public String getDefaultValue(String key) {
		// TODO Auto-generated method stub
		Configuration cnf;
		try {
			cnf = cache.get(key);
		} catch (Exception e) {
			log.info("从缓存中获取redis-key异常,异常key:{},异常信息:{}",key,e.getMessage());
			e.printStackTrace();
			throw new RedisKeyGetException("从缓存中获取redis-key异常,异常key:["+key+"] 异常信息:"+e.getMessage());
		}
		return cnf == null ? null : cnf.getValue();
	}

	@Override
	public String getValue(String key) {
		// TODO Auto-generated method stub
		return cache.get(key).getValue();
	}

	@Override
	public Configuration get(String key) {
		// TODO Auto-generated method stub
		return cache.get(key);
	}

	@Override
	public void delete(String key) {
		// TODO Auto-generated method stub
		cache.remove(key);
	}

	@Override
	public void update(String key, String value) {
		// TODO Auto-generated method stub
		Configuration cnf=cache.get(key);
		cnf.setValue(value);
	}
	
	@Override
	public void update(String key, Configuration config) {
		// TODO Auto-generated method stub
		cache.put(key, config);
	}

	@Override
	public void add(String key, Configuration config) {
		// TODO Auto-generated method stub
		cache.put(key, config);
	}

	@Override
	public String getValueFromRedisThenDb(String key) {
		String str =  cache.get(key).getValue();
		if(StringUtils.isBlank(str)){
			log.info("从redis缓存中获取不到数据，key：{}，线程id:{}", key, Thread.currentThread().getId());
			//get info from db
			Configuration dbConfig = config.findConfigurationByKey(key);
			if(dbConfig == null){
				log.info("从db获取不到数据，key：{}，线程id:{}", key, Thread.currentThread().getId());
				return null;
			}else{
				return dbConfig.getValue();
			}
		}
		return str;
	}

}
