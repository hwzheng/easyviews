package com.greattimes.ev.cache.redis;

import java.util.List;
import java.util.Set;

import com.greattimes.ev.common.exception.RedisKeyGetException;
import com.greattimes.ev.common.exception.RedisKeyNotFoundException;
import com.greattimes.ev.common.redis.RedisDaoImpl;
import com.greattimes.ev.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.greattimes.ev.cache.Cache;
import com.greattimes.ev.cache.IConfiguration;
import com.greattimes.ev.common.redis.RedisConstant;
import com.greattimes.ev.common.redis.RedisDao;
import com.greattimes.ev.system.entity.Configuration;
import com.greattimes.ev.system.service.IConfigurationService;

public class ConfigurationCache implements Cache ,IConfiguration{
	
	Logger log=LoggerFactory.getLogger(this.getClass()); 
	
	@Autowired
	public IConfigurationService config;
	
	@Autowired
	public RedisDao redis;
	
	
	private String key(String key){
		return RedisConstant.SYS_CONFIGURATION.concat(key);
	}

	public boolean add(String key, Object value){
		return redis.add(key, value);
	}
	
	@Override
	public void initCache(){
		// TODO Auto-generated method stub
		log.info(INIT_START);
		try {
			clearCache();
			List<Configuration> sysConfigs=config.selectList(null);
			sysConfigs.stream().forEach(cnf->{
				String key =key(cnf.getKey()) ;
				redis.add(key, cnf);
			});
		} catch (Exception e) {
			// TODO: handle exception
			log.error(INIT_ERROR);
			log.error(e.toString());
		}
		log.info(INIT_END);
	}
	
	@Override
	public void clearCache(){
		// TODO Auto-generated method stub
		Set<String> keys=redis.keys(RedisConstant.SYS_CONFIGURATION+"*");
		redis.delete(keys);
	}

	@Override
	public String getValue(String key) {
		// TODO Auto-generated method stub
        Configuration cnf;
        try {
            cnf = redis.get(key(key),Configuration.class);
        } catch (Exception e) {
            e.printStackTrace();
            log.info("从缓存中获取redis-key异常,异常key:{},异常信息:{}",key,e.getMessage());
            throw new RedisKeyGetException("从缓存中获取redis-key异常,异常key:["+key+"] 异常信息:"+e.getMessage());
        }
        if(cnf == null){
        	log.info("从缓存中未找到相应的key:{}",key);
            throw new RedisKeyNotFoundException("从缓存中未找到相应的key:"+key);
        }
        return cnf.getValue();
	}

	@Override
	public String getDefaultValue(String key) {
		Configuration cnf;
		try {
			cnf = redis.get(key(key),Configuration.class);
		} catch (Exception e) {
			log.info("从缓存中获取redis-key异常,异常key:{},异常信息:{}",key,e.getMessage());
			e.printStackTrace();
			throw new RedisKeyGetException("从缓存中获取redis-key异常,异常key:["+key+"] 异常信息:"+e.getMessage());
		}
		return cnf == null ? null : cnf.getValue();
	}

	@Override
	public Configuration get(String key) {
		// TODO Auto-generated method stub
		return redis.get(key(key),Configuration.class);
	}

	@Override
	public void delete(String key) {
		// TODO Auto-generated method stub
		 redis.delete(key(key));
	}

	@Override
	public void update(String key, String value) {
		// TODO Auto-generated method stub
		Configuration cnf=redis.get(key(key),Configuration.class);
		cnf.setValue(value);
		redis.update(key(key), cnf);
	}
	
	@Override
	public void update(String key, Configuration config) {
		// TODO Auto-generated method stub
		redis.update(key(key), config);
	}

	@Override
	public void add(String key, Configuration config) {
		// TODO Auto-generated method stub
		redis.add(key(key), config);
	}


	@Override
	public String getValueFromRedisThenDb(String key) {
		Configuration cnf =  redis.get(key(key),Configuration.class);
		if(cnf == null || StringUtils.isBlank(cnf.getValue())){
			log.info("从redis缓存中获取不到数据，key:{}，线程id:{}", key, Thread.currentThread().getId());
			//get info from db
			Configuration dbConfig = config.findConfigurationByKey(key);
			if(dbConfig == null){
				log.info("从db获取不到数据，key:{}，线程id:{}", key, Thread.currentThread().getId());
				return null;
			}else{
				return dbConfig.getValue();
			}
		}
		return cnf.getValue();
	}
}
