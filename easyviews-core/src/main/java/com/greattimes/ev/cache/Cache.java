package com.greattimes.ev.cache;

/**
 * 
 * @author LiHua
 *
 */
public interface Cache {
	
	/**
	 * 缓存
	 */
	void initCache();
	
	/**
	 * 清空
	 */
	void  clearCache();
	
}
