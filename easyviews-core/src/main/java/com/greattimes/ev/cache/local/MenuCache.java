package com.greattimes.ev.cache.local;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;

import com.greattimes.ev.cache.Cache;
import com.greattimes.ev.cache.IMenu;
import com.greattimes.ev.common.redis.RedisConstant;
import com.greattimes.ev.common.redis.RedisDao;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.service.IMenuService;
import com.greattimes.ev.system.service.IUserService;

public class MenuCache implements Cache ,IMenu {
	
	@Autowired
	private IMenuService menuService;
	
	private static List<Map<String, Object>> menus;
	
	private String key(String key){
		return RedisConstant.USER_MENU.concat(key);
	}
	
	@Override
	public void initCache() {
		// TODO Auto-generated method stub
		//a.id as uid, d.id,d.parent_id AS parentId ,d.ename,d.href ,d.sort ,d.icon,d.cname
		menuService.getRoleMenus();
		
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		menus.clear();
	}

	@Override
	public String getEname(String key) {
		// TODO Auto-generated method stub
		
		return null;
	}

	@Override
	public String getUri(String key) {
		// TODO Auto-generated method stub
		
		return null;
	}

}
