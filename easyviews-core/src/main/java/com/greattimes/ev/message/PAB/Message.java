package com.greattimes.ev.message.PAB;

/**
 * <p>
 *     短信实体
 * </p>
 */
public class Message {
    private String mobiles;

    private String content;

    public String getMobiles() {
        return mobiles;
    }

    public void setMobiles(String mobiles) {
        this.mobiles = mobiles;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "mobiles='" + mobiles + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
