package com.greattimes.ev.message.guangfa;

import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.message.ShortMessage;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.HttpClientUtil;
import com.greattimes.ev.utils.StaticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URLEncoder;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 *
 * @author NJ
 * @date 2019/3/24 21:05
 */
@Service
public class WeChatMessageOfGF implements ShortMessage<Message> {

    Logger log = LoggerFactory.getLogger(this.getClass());

    /** 微信告警的url*/
    public static final String ALARM_WECHAT_URL = "alarm_wechat_url";

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageSendJob";

    @Autowired
    private IMessageService messageService;

    @Autowired
    private ConfigurationCache configurationCache;

    @Override
    public void build() {
        Date start = new Date();
        /**
         * 待发送类型：  1 邮件 ，2 短信 ，3app ，4外呼 5微信
         */
        List<com.greattimes.ev.system.entity.Message> msgs = messageService.selectAlarmMessageByParam(5, 0);
        if(evUtil.listIsNullOrZero(msgs)){
            log.info("广发无相应的告警短信有待发送！");
            return ;
        }
        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        msgs.stream().forEach(msg->{
            msg.setState(1);
        });
        messageService.updateBatchById(msgs);
        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("广发模式告警短信-无相应的发送用户!");
            return ;
        }
        int successNum = 0,messageId, groupId;
        String alarmMsg;
        List<Integer> errorIds = new ArrayList<>();
        //转化为map
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        String url = configurationCache.getValue(ALARM_WECHAT_URL)+"/alarm/send?";
        for (com.greattimes.ev.system.entity.Message message : msgs) {
            messageId =  message.getId();
            Date execTime = new Date();
            TaskLogs taskLogs1 = new TaskLogs();
            taskLogs1.setStat(2);
            taskLogs1.setClassName(className);
            taskLogs1.setStarttimes(execTime);
            try {
                groupId = Integer.parseInt(message.getAlarmGroupId());
                //告警组对应的用户列表
                if(userAlarmMap.get(groupId) == null){
                    continue;
                }
                List<UserParam> userList = userAlarmMap.get(groupId).getUsers();
                if(evUtil.listIsNullOrZero(userList)){
                    continue;
                }
                //主题
                String subject = "EasyViews告警";
                alarmMsg = message.getMessage();
                String urlMsg;
                //主题
                StringBuilder sb = new StringBuilder("&subject=").append(URLEncoder.encode(subject,"utf-8"));
                //内容
                sb.append("&content=").append(URLEncoder.encode(alarmMsg,"utf-8"));
                int userNameNum = 0;
                List<String> noReceiverNameList = new ArrayList<>();
                for (UserParam userParam : userList) {
                    if(StringUtils.isBlank(userParam.getName())){
                        log.info("微信告警发送用户的姓名为空！");
                        continue;
                    }
                    StringBuilder allUrl = new StringBuilder(url);
                    String userName = URLEncoder.encode(userParam.getName(),"utf-8");
                    allUrl = allUrl.append("username=").append(userName);
                    allUrl.append(sb);
                    urlMsg = allUrl.toString();
                    String result = HttpClientUtil.httpGetRequest(urlMsg);
                    log.info(">>>>>>>>调用微信接口返回值："+result+"用户名："+userName+"告警id为："+ message.getId());
                    //发送成功
                    if("ok".equals(result)){
                        userNameNum ++;
                    }else{ //失败
                        noReceiverNameList.add(userParam.getName());
                    }
                }

                //默认如果有一个用户推送成功则认为告警信息发送成功
                if(userNameNum > 0){
                    //更新状态(状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败)
                    messageService.updateAlarmMessageState(messageId,2);
                }else{
                    log.info("调用微信发送接口返回值有误,告警信息表id为{}的告警短信发送失败！",messageId);
                    errorIds.add(messageId);
                    messageService.updateAlarmMessageState(messageId,3);
                    taskLogs1.setStat(3);
                    taskLogs1.setClassName(className);
                    taskLogs1.setDetail("调用微信发送接口返回值有误!");
                    taskLogs1.setDesc("告警信息表id为"+message.getId()+"的微信告警发送失败！");
                    taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                    StaticUtil.taskLogsQueue.add(taskLogs1);
                }
                if(noReceiverNameList.size() > 0){
                    log.info("微信用户:"+noReceiverNameList.toString()+"推送信息失败！告警id为"+ messageId);
                }
            } catch (Exception e) {
                errorIds.add(messageId);
                e.printStackTrace();
                log.info("发生异常,微信告警信息表id:{}的告警短信发送失败！",messageId);
                messageService.updateAlarmMessageState(messageId,3);
                taskLogs1.setStat(3);
                taskLogs1.setClassName(className);
                taskLogs1.setDesc("发生异常,微信告警信息表id为"+messageId+"的告警短信发送失败！");
                taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                StaticUtil.taskLogsQueue.add(taskLogs1);
            }
        }

        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(start);
        taskLogs.setClassName(className);
        Date end = new Date();
        //记录日志
        if(msgs.size() > 0) {
            if (msgs.size() == successNum) {
                taskLogs.setStat(0);
                taskLogs.setDetail("微信告警短信任务执行完成");
                taskLogs.setDesc("微信告警短信发送成功！成功条数：" + msgs.size());
            } else if (successNum > 0) {
                taskLogs.setStat(1);
                taskLogs.setDetail("微信告警短信任务执行完成");
                taskLogs.setDesc("部分微信告警短信发送成功！成功条数：" + successNum + " 失败条数：" + errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
            } else {
                taskLogs.setStat(3);
                taskLogs.setDetail("微信告警短信任务执行失败");
                taskLogs.setDesc("微信告警短信发送失败！失败条数：" + errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
            }
            taskLogs.setDuration(((Long) (end.getTime() - start.getTime())).intValue());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }

    }

    @Override
    public void send(Message parm) {
    }

    @Override
    public String sendAndReturn(Message parm) {
        return null;
    }
}
