package com.greattimes.ev.message.liaoning;


/**
 * @author NJ
 * @date 2018/10/12 16:11
 */
public class Message{

    private String ip;

    private String port;

    private String content;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
