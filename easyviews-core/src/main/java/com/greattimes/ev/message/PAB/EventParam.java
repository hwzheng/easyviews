package com.greattimes.ev.message.PAB;


import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/9/23 14:20
 */
public class EventParam implements Serializable{

    private static final long serialVersionUID = 4264296074118331859L;
    /**
     * 子系统id
     */
    private String subsysid;
    /**
     * appid
     */
    private String appid;
    /**
     * 子系统中文名
     */
    private String subsystemName;
    /**
     * 应用中文名
     */
    private String appName;
    /**
     * 领域
     */
    @JSONField(serialize=false)
    private String domain;
    /**
     * 业务
     */
    @JSONField(serialize=false)
    private String business;
    /**
     * 系统来源
     */
    private String source;
    /**
     * 	告警标识（0：告警 1：告警恢复）
     */
    private Integer flag;
    /**
     * 告警唯一标识
     */
    private String key;
    /**
     * 告警类型（tel,sms,email, paic,webchat,app）
     */
    @JSONField(serialize=false)
    private String type;
    /**
     * 告警级别 （INFO,NOTICE,WARNING,CRITICAL,DISASATER）
     */
    private String level;
    /**
     * 告警内容
     */
    private Map<String, Object> contents;
     /**
     * 告警时间
     */
    private Long time;

    public String getSubsysid() {
        return subsysid;
    }

    public void setSubsysid(String subsysid) {
        this.subsysid = subsysid;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getSubsystemName() {
        return subsystemName;
    }

    public void setSubsystemName(String subsystemName) {
        this.subsystemName = subsystemName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public Map<String, Object> getContents() {
        return contents;
    }

    public void setContents(Map<String, Object> contents) {
        this.contents = contents;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "EventParam{" +
                "subsysid='" + subsysid + '\'' +
                ", appid='" + appid + '\'' +
                ", subsystemName='" + subsystemName + '\'' +
                ", appName='" + appName + '\'' +
                ", domain='" + domain + '\'' +
                ", business='" + business + '\'' +
                ", source='" + source + '\'' +
                ", flag=" + flag +
                ", key='" + key + '\'' +
                ", type='" + type + '\'' +
                ", level='" + level + '\'' +
                ", contents=" + contents +
                ", time=" + time +
                '}';
    }
}
