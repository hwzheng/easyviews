package com.greattimes.ev.message.CMBC;


import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.exception.ShortMessageException;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.message.ShortMessage;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IDataManageService;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.SocketUtil;
import com.greattimes.ev.utils.SpringUtils;
import com.greattimes.ev.utils.StaticUtil;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
//@Component(value = "shortMessageOfCMBC")
public class ShortMessageOfCMBC implements ShortMessage<Message> {

   Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * 民生模式告警短信发送ip
     */
    private static final String MS_ALARM_MESSAGE_SEND_IP = "ms_alarm_message_send_ip";
    /**
     * 民生模式告警短信发送端口
     */
    private static final String MS_ALARM_MESSAGE_SEND_PORT = "ms_alarm_message_send_port";
    /**
     * 民生模式报文类型  固定不变
     */
    private static final String MSGTYPE = "6101";
    /**
     * 民生模式所属银行号 固定不变
     */
    private static final String BANK = "0305";
    /**
     * 民生模式所属传输流水号 自设  自定义
     */
    private static final String SYSTRACE = "000001";
    /**
     * 民生模式所属总线码 自设 固定不变
     */
    private static final String BUSNUM = "000001";
    /**
     * 民生模式所属银行号 自设  固定不变
     */
    private static final String SOURCEFLAG = "JYJK00";


    @Autowired
    private IMessageService messageService;

    @Autowired
    private ConfigurationCache configurationCache;

    public static Map<String,Integer> taskIdMap = null;

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageSendJob";

    @Override
    public void build() {
        Date start = new Date();
        /**
         * 待发送类型：  1 邮件 ，2 短信 ，3app ，4外呼 5微信
         */
        List<com.greattimes.ev.system.entity.Message> msgs = messageService.selectAlarmMessageByParam(2, 0);
        if(evUtil.listIsNullOrZero(msgs)){
            log.info("无相应的告警短信有待发送！");
            return ;
        }
        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        msgs.stream().forEach(msg->{
            msg.setState(1);
        });
        messageService.updateBatchById(msgs);
        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("民生模式告警短信-无相应的发送用户!");
            return ;
        }
        //转化为map
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        int successNum = 0;
        int messageId;
        String ip;
        String port;
        SimpleDateFormat sf = new SimpleDateFormat("MMddHHmmss");
        Integer groupId;
        List<Integer> errorIds = new ArrayList<>();
        for (com.greattimes.ev.system.entity.Message message : msgs) {
            messageId =  message.getId();
            Date execTime = new Date();
            TaskLogs taskLogs1 = new TaskLogs();
            taskLogs1.setStat(2);
            taskLogs1.setStarttimes(execTime);
            taskLogs1.setClassName(className);
            try {
                groupId = Integer.parseInt(message.getAlarmGroupId());
                //告警组对应的用户列表
                if(userAlarmMap.get(groupId) == null){
                    continue;
                }
                List<UserParam> userList = userAlarmMap.get(groupId).getUsers();
                if(evUtil.listIsNullOrZero(userList)){
                    continue;
                }
                List<String> mobiles = new ArrayList<>();
                for (UserParam userParam : userList) {
                    Long phone = userParam.getPhone();
                    if(phone != null){
                        mobiles.add(phone.toString());
                    }
                }
                if(mobiles.size() == 0){
                    log.info("告警信息表id为:{}的告警短信用户无电话号码", messageId);
                    continue;
                }
                //短信内容 512字符 GBK编码 不足右补空格
                String sMsg = message.getMessage();
                sMsg = formatStrTo512Byte(sMsg);
                boolean successFlag = false;
                Message param = new Message();
                //参数获取(及时获取缓存缓存中最新数据,缓存没有则查询数据库中数据)
                ip =  configurationCache.getValue(MS_ALARM_MESSAGE_SEND_IP);
                port =  configurationCache.getValue(MS_ALARM_MESSAGE_SEND_PORT);
                param.setIp(ip);
                param.setPort(port);
                String msgBody;
                for (String mobile : mobiles) {
                    try{
                        String transTm = sf.format(new Date());
                        msgBody = MSGTYPE + BANK + SYSTRACE +transTm+mobile+BUSNUM+SOURCEFLAG+sMsg;
                        //报文头 从Msgtype开始计算，单位为char，如“0123”
                        String MSGLEN = "0"+msgBody.getBytes("gbk").length;
                        String msgSend = MSGLEN + msgBody;
                        log.info("短信告警报文信息:{}", msgSend);
                        param.setContent(msgSend);
                        String reply = this.sendAndReturn(param);
                        if("00040000".equals(reply)){
                            successFlag = true;
                        }else if("00040001".equals(reply)){
                            log.info("长度错误,返回值：{}", reply);
                        }else  if("00040002".equals(reply) || "00040003".equals(reply)){
                            log.info("消息队列错误,返回值：{}", reply);
                        }else{
                            log.info("其他异常,返回值：{}", reply);
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                        log.info("电话号码：{}的告警信息表id为{} 的告警短信发送失败！{}", mobile, message.getId(),e.getMessage());
                    }
                }
                if(successFlag){
                    //只要某个号码的告警短信有发送成功的，就认为本次告警短信发送成功 避免重复发送
                    successNum ++;
                    messageService.updateAlarmMessageState(message.getId(),1);
                    //更新告警信息状态
                    log.info("告警信息表id为{}的告警短信发送完成！",message.getId());
                }else{
                    errorIds.add(messageId);
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.info("发生异常,告警信息表id为{}的告警短信发送失败！", messageId);
                errorIds.add(messageId);
                messageService.updateAlarmMessageState(messageId,3);
                taskLogs1.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
                taskLogs1.setStat(3);
                taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                taskLogs1.setDesc("发生异常,告警信息表id为"+messageId+"的告警短信发送失败！");
                StaticUtil.taskLogsQueue.add(taskLogs1);
            }
        }

        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(start);
        taskLogs.setClassName(className);
        Date end = new Date();
        //记录日志
        if(msgs.size() > 0){
            if(msgs.size() == successNum){
                //成功
                taskLogs.setStat(0);
                taskLogs.setDetail("告警短信任务执行完成");
                taskLogs.setDesc("告警短信发送成功！成功条数：" + msgs.size());
            }else if(successNum > 0 ){
                //部分成功
                taskLogs.setStat(1);
                taskLogs.setDetail("告警短信任务执行完成");
                taskLogs.setDesc("部分告警短信发送成功！成功条数：" + successNum + " 失败条数：" + errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
            }else{
                //失败(所有都失败)
                taskLogs.setStat(3);
                taskLogs.setDesc("告警短信发送失败！失败条数："+ errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
                taskLogs.setDetail("告警短信任务执行失败");
            }
            taskLogs.setDuration(((Long)(end.getTime()- start.getTime())).intValue());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }

    }

    @Override
    public void send(Message param) {
    }

    @Override
    public String sendAndReturn(Message parm) {
        String reply;
        try {
            reply = SocketUtil.sendAlarmMsg(parm.getIp(), parm.getPort(), parm.getContent());
            log.info("告警短信发送返回信息："+reply);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShortMessageException(e.getMessage());
        }
        return reply;
    }

    /**
     * 格式化字符串为512字节长度（gbk编码）  不足右边补空格
     * @param str gbk编码的字符串
     * @return
     */
    public String formatStrTo512Byte(String str){
        int strByteLength = getStrByteLength(str);
        if(strByteLength == 512){
            return str;
        }else if(strByteLength > 512){
            //截取并补数据
            char[] charArray = str.toCharArray();
            List<Character> cList = new ArrayList<Character>();
            int length = 0;
            for (char c : charArray) {
                if(length < 511){
                    if(isLetter(c)){
                        //英文字符  长度+1
                        length++;
                        cList.add(c);
                    }else{
                        length = length+2;
                        //中文字符 长度+2
                        cList.add(c);
                    }
                }else if(length==511){
                    if(isLetter(c)){
                        //英文字符  长度+1
                        length++;
                        cList.add(c);
                    }else{
                        //中文字符  添加一个空格
                        length++;
                        cList.add(' ');
                    }
                }else{
                    //长度达到512 跳出循环
                    break;
                }
            }
            char [] resultArray = new char[cList.size()];
            for (int i = 0; i < cList.size(); i++) {
                resultArray[i] = cList.get(i);
            }
            str = new String(resultArray);
        }else{
            //直接补空格
            int spaceNum = 512 - strByteLength;
            str = str+getSpaceStr(spaceNum);
        }
        return str;
    }

    /**
     * 获得空格字符串
     * @param spaceNum 空格数
     * @return
     */
    private String getSpaceStr(int spaceNum) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < spaceNum; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    /**
     * 获得字符串的字节数
     * @param s
     * @return
     */
    public int getStrByteLength(String s) {
        if (s == null){
            return 0;
        }
        char[] c = s.toCharArray();
        int len = 0;
        for (int i = 0; i < c.length; i++) {
            len++;
            if (!isLetter(c[i])) {
                len++;
            }
        }
        return len;
    }

    /**
     * 判断字符是否是英文字符
     * @param c
     * @return
     */
    public boolean isLetter(char c) {
        int k = 0x80;
        return c / k == 0 ? true : false;
    }
}
