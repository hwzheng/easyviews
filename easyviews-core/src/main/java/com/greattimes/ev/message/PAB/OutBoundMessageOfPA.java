package com.greattimes.ev.message.PAB;

import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.message.ShortMessage;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.HttpClientUtil;
import com.greattimes.ev.utils.StaticUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2019/3/25 15:07
 */
@Service
public class OutBoundMessageOfPA implements ShortMessage<Message> {

    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 告警外呼发送url
     */
    public static final String ALARM_OUTBOUND_URL = "alarm_outbound_url";
    /**
     * 外呼告警服务器端验证口令
     */
    public static final String OUTBOUND_SERVER_VALIDATE_PASSWD = "outbound_server_validate_passwd";

    @Autowired
    private IMessageService messageService;

    @Autowired
    private ConfigurationCache configurationCache;

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageSendJob";


    @Override
    public void build() {
        Date start = new Date();
        /**
         * 待发送类型：  1 邮件 ，2 短信 ，3app ，4外呼 5微信
         */
        List<com.greattimes.ev.system.entity.Message> msgs = messageService.selectAlarmMessageByParam(4, 0);
        if(evUtil.listIsNullOrZero(msgs)){
            log.info("无相应的告警短信有待发送！");
            return ;
        }
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        msgs.stream().forEach(msg->{
            msg.setState(1);
        });
        messageService.updateBatchById(msgs);
        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("平安模式告警短信-无相应的发送用户!");
            return ;
        }
        //转化为map
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        Integer groupId;
        int successNum = 0;
        int messageId;
        List<Integer> errorIds = new ArrayList<>();

        String url = configurationCache.getValue(ALARM_OUTBOUND_URL);
        String passwd = configurationCache.getValue(OUTBOUND_SERVER_VALIDATE_PASSWD);
        for (com.greattimes.ev.system.entity.Message message : msgs) {
            Date execTime = new Date();
            messageId =  message.getId();
            TaskLogs taskLogs1 = new TaskLogs();
            taskLogs1.setStat(2);
            taskLogs1.setClassName(className);
            taskLogs1.setStarttimes(execTime);
            try {
                groupId = Integer.parseInt(message.getAlarmGroupId());
                if(userAlarmMap.get(groupId) == null){
                    continue;
                }
                List<UserParam> userList = userAlarmMap.get(groupId).getUsers();
                if(evUtil.listIsNullOrZero(userList)){
                    continue;
                }
                List<String> mobiles = new ArrayList<>();
                /**
                 * 多个电话用英文逗号隔开，非上海号码前面加0（平安接口）
                 */
                String phone;
                for (UserParam uParam : userList) {
                    if(uParam.getPhone() == null){
                        continue;
                    }
                    phone = uParam.getPhone().toString();
                    if(phone != null && phone.length() > 0){
                        if(!(0==phone.indexOf(0))){
                            phone = "0" + phone;
                        }
                        mobiles.add(phone);
                    }
                }
                if(mobiles.size() == 0){
                    log.info("告警信息表id为{}的告警短信用户无电话号码", messageId);
                    continue;
                }

                Message msg = new Message();
                msg.setMobiles(StringUtils.join(mobiles,","));
                msg.setContent(message.getMessage());
                String result = HttpClientUtil.sendOutbound(url, passwd, msg);
                log.info("发送外呼告警返回结果：{}",result);
                if(result != null){
                    successNum ++ ;
                    log.info("外呼告警信息表id为{}的告警短信发送成功！",messageId);
                    messageService.updateAlarmMessageState(messageId,2);
                }else{
                    log.info("外呼告警信息表id为{}的告警短信发送失败！返回信息:{}",messageId,result);
                    messageService.updateAlarmMessageState(messageId,3);
                    taskLogs1.setStat(3);
                    errorIds.add(messageId);
                    taskLogs1.setClassName(className);
                    taskLogs1.setDetail("外呼告警发送返回信息为空！");
                    taskLogs1.setDesc("外呼告警信息表id为"+messageId+"的告警发送失败！");
                    taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                    StaticUtil.taskLogsQueue.add(taskLogs1);
                }
            } catch (Exception e) {
                errorIds.add(messageId);
                e.printStackTrace();
                log.info("发生异常,外呼告警信息表id为{}的告警短信发送失败！",messageId);
                messageService.updateAlarmMessageState(messageId,3);
                taskLogs1.setStat(3);
                taskLogs1.setClassName(className);
                taskLogs1.setDesc("发生异常,外呼告警信息表id为"+messageId+"的告警短信发送失败！");
                taskLogs1.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
                taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                StaticUtil.taskLogsQueue.add(taskLogs1);
            }
        }

        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(start);
        taskLogs.setClassName(className);
        Date end = new Date();
        //记录日志
        if(msgs.size() > 0) {
            if (msgs.size() == successNum) {
                //成功
                taskLogs.setStat(0);
                taskLogs.setDetail("外呼告警短信任务执行完成！");
                taskLogs.setDesc("外呼告警短信发送成功！成功条数：" + msgs.size());
            } else if (successNum > 0) {
                //部分成功
                taskLogs.setStat(1);
                taskLogs.setDetail("外呼告警短信任务执行完成");
                taskLogs.setDesc("部分外呼告警短信发送成功！成功条数：" + successNum + " 失败条数：" + errorIds.size()
                        + " 发送失败告警信息id：" + errorIds.toString());
            } else {
                //失败(所有都失败)
                taskLogs.setStat(3);
                taskLogs.setDetail("外呼告警短信任务执行失败！");
                taskLogs.setDesc("外呼告警短信发送失败！失败条数：" + errorIds.size()
                        + " 发送失败告警信息id：" + errorIds.toString());
            }
            taskLogs.setDuration(((Long) (end.getTime() - start.getTime())).intValue());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }
    }

    @Override
    public void send(Message parm) {
    }

    @Override
    public String sendAndReturn(Message parm) {
        return null;
    }
}
