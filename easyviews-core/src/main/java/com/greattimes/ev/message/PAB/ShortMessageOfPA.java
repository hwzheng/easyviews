package com.greattimes.ev.message.PAB;


import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.exception.ShortMessageException;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.message.ShortMessage;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.HttpClientUtil;
import com.greattimes.ev.utils.StaticUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class ShortMessageOfPA implements ShortMessage<Message> {

   Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 平安模式告警短信发送url
     */
    private static final String ALARM_MESSAGE_SEND_URL = "alarm_message_send_url";

    @Autowired
    private IMessageService messageService;

    @Autowired
    private ConfigurationCache configurationCache;

    private String sendUrl;

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageSendJob";

    @Override
    public void build() {
        Date start = new Date();
        //获取sendUrl
        sendUrl =  configurationCache.getValue(ALARM_MESSAGE_SEND_URL);
        log.info("告警短信发送url：{}", sendUrl);

        //查询待发送数据
        List<com.greattimes.ev.system.entity.Message> msgs = messageService.selectAlarmMessageByParam(2, 0);
        if(evUtil.listIsNullOrZero(msgs)){
            log.info("无相应的告警短信有待发送！");
            return ;
        }
        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        msgs.stream().forEach(msg->{
            msg.setState(1);
        });
        messageService.updateBatchById(msgs);
        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("平安模式告警短信-无相应的发送用户!");
            return ;
        }
        //转化为map
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        Integer groupId;
        int successNum = 0;
        int messageId;
        List<Integer> errorIds = new ArrayList<>();
        for (com.greattimes.ev.system.entity.Message message : msgs) {
            messageId =  message.getId();
            Date execTime = new Date();
            TaskLogs taskLogs1 = new TaskLogs();
            taskLogs1.setStat(2);
            taskLogs1.setClassName(className);
            taskLogs1.setStarttimes(execTime);
            try {
                groupId = Integer.parseInt(message.getAlarmGroupId());
                //告警组对应的用户列表
                if(userAlarmMap.get(groupId) == null){
                    continue;
                }
                List<UserParam> userList = userAlarmMap.get(groupId).getUsers();
                if(evUtil.listIsNullOrZero(userList)){
                    continue;
                }
                List<String> mobiles = new ArrayList<>();
                for (UserParam uParam : userList) {
                    Long phone = uParam.getPhone();
                    if(phone != null){
                        mobiles.add(phone.toString());
                    }
                }
                if(mobiles.size() == 0){
                    log.info("告警信息表id为{}的告警短信用户无电话号码", messageId);
                    continue;
                }
                Message msg = new Message();
                msg.setMobiles(StringUtils.join(mobiles,","));
                msg.setContent(message.getMessage());

                //调用send发送
                String result = this.sendAndReturn(msg);
                log.info("发送短信告警返回结果："+result);
                if(result != null){
                    successNum ++ ;
                    log.info("告警信息表id为{}的告警短信发送成功！",messageId);
                    //更新状态(状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败)
                    messageService.updateAlarmMessageState(messageId,2);
                }else{
                    log.info("发生异常,告警信息表id为{}的告警短信发送失败！返回信息:{}",messageId,result);
                    messageService.updateAlarmMessageState(messageId,3);
                    taskLogs1.setStat(3);
                    errorIds.add(messageId);
                    taskLogs1.setClassName(className);
                    taskLogs1.setDetail("发送返回信息为空！");
                    taskLogs1.setDesc("发生异常,告警信息表id为"+messageId+"的告警短信发送失败！");
                    taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                    StaticUtil.taskLogsQueue.add(taskLogs1);
                }
            } catch (Exception e) {
                errorIds.add(messageId);
                e.printStackTrace();
                log.info("发生异常,告警信息表id为{}的告警短信发送失败！",messageId);
                messageService.updateAlarmMessageState(messageId,3);
                taskLogs1.setStat(3);
                taskLogs1.setClassName(className);
                taskLogs1.setDesc("发生异常,告警信息表id为"+messageId+"的告警短信发送失败！");
                taskLogs1.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
                taskLogs1.setDuration(((Long)(System.currentTimeMillis()- start.getTime())).intValue());
                StaticUtil.taskLogsQueue.add(taskLogs1);
            }
        }

        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(start);
        taskLogs.setClassName(className);
        Date end = new Date();
        //记录日志
        if(msgs.size() > 0) {
            if (msgs.size() == successNum) {
                //成功
                taskLogs.setStat(0);
                taskLogs.setDetail("告警短信任务执行完成！");
                taskLogs.setDesc("告警短信发送成功！成功条数：" + msgs.size());
            } else if (successNum > 0) {
                //部分成功
                taskLogs.setStat(1);
                taskLogs.setDetail("告警短信任务执行完成");
                taskLogs.setDesc("部分告警短信发送成功！成功条数：" + successNum + " 失败条数：" + errorIds.size()
                        + " 发送失败告警信息id：" + errorIds.toString());
            } else {
                //失败(所有都失败)
                taskLogs.setStat(3);
                taskLogs.setDetail("告警短信任务执行失败！");
                taskLogs.setDesc("告警短信发送失败！失败条数：" + errorIds.size()
                        + " 发送失败告警信息id：" + errorIds.toString());
            }
            taskLogs.setDuration(((Long) (end.getTime() - start.getTime())).intValue());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }
    }

    @Override
    public void send(Message param) {
    }

    @Override
    public String sendAndReturn(Message param) {
        try {
            String result = HttpClientUtil.sendMsg(sendUrl, param);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShortMessageException(e.getMessage());
        }
    }
}
