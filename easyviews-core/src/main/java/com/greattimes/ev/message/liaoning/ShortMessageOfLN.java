package com.greattimes.ev.message.liaoning;


import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.exception.ShortMessageException;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.message.ShortMessage;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.SocketUtil;
import com.greattimes.ev.utils.StaticUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
//@Component(value = "shortMessageOfLN")
public class ShortMessageOfLN implements ShortMessage<Message> {

    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 辽宁模式渠道代码
     */
    private static final String ALARM_CHANNELID = "alarm_channelid";
    /**
     * 辽宁模式信息业务代码
     */
    private static final String ALARM_MSGCLASSID = "alarm_msgclassid";
    /**
     * 辽宁模式机构应用编号
     */
    private static final String ALARM_APPID = "alarm_appid";
    /**
     * 辽宁模式开户机构号
     */
    private static final String ALARM_OPENBRC = "alarm_openbrc";
    /**
     * 辽宁模式告警短信发送ip
     */
    private static final String ALARM_IP = "alarm_ip";
    /**
     * 辽宁模式告警短信发送端口
     */
    private static final String ALARM_PORT = "alarm_port";

    private String channelid;
    private String msgclassid;
    private String appid;
    private String openbrc;
    private String ip;
    private String port;

    @Autowired
    private IMessageService messageService;

    @Autowired
    private ConfigurationCache configurationCache;

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageSendJob";
    @Override
    public void build() {
        //参数获取(及时获取缓存缓存中最新数据)
        channelid = configurationCache.getValue(ALARM_CHANNELID);
        msgclassid =  configurationCache.getValue(ALARM_MSGCLASSID);
        appid =  configurationCache.getValue(ALARM_APPID);
        openbrc = configurationCache.getValue(ALARM_OPENBRC);
        ip =  configurationCache.getValue(ALARM_IP);
        port =  configurationCache.getValue(ALARM_PORT);
        log.info("辽宁模式告警短信发送参数:channelid【{}】,msgclassid:【{}】," +
                "appid:【{}】,openbrc:【{}】,ip:【{}】,port:【{}】",channelid, msgclassid,appid,
                openbrc,ip,port);
        Date start = new Date();
        //查询待发送数据
        List<com.greattimes.ev.system.entity.Message> msgs = messageService.selectAlarmMessageByParam(2, 0);
        if(evUtil.listIsNullOrZero(msgs)){
            log.info("无相应的告警短信有待发送！");
            return ;
        }
        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        msgs.stream().forEach(msg->{
            msg.setState(1);
        });
        messageService.updateBatchById(msgs);
        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("辽宁模式告警短信-无相应的发送用户!");
            return ;
        }
        //转化为map
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        int successNum = 0;
        int messageId;
        Message param = new Message();
        param.setIp(ip);
        param.setPort(port);
        List<Integer> errorIds = new ArrayList<>();
        Integer groupId;
        for (com.greattimes.ev.system.entity.Message message : msgs) {
            messageId =  message.getId();
            Date execTime = new Date();
            TaskLogs tlogs = new TaskLogs();
            tlogs.setStarttimes(execTime);
            tlogs.setStat(2);
            tlogs.setClassName(className);
            try {
                groupId = Integer.parseInt(message.getAlarmGroupId());
                //告警组对应的用户列表
                if(userAlarmMap.get(groupId) == null){
                    continue;
                }
                List<UserParam> userList = userAlarmMap.get(groupId).getUsers();
                if(evUtil.listIsNullOrZero(userList)){
                    continue;
                }
                List<String> mobiles = new ArrayList<>();
                for (UserParam uParam : userList) {
                    Long phone = uParam.getPhone();
                    if(phone != null){
                        mobiles.add(phone.toString());
                    }
                }
                if(mobiles.size() == 0){
                    log.info("告警信息表id为{}的告警短信用户无电话号码", messageId);
                    continue;
                }
                param.setContent(this.buildContent(start, message.getMessage(),mobiles));
                //调用send发送
                this.send(param);
                successNum ++ ;
                log.info("告警信息表id为{}的告警短信发送成功！",messageId);
                //更新状态(状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败)
                messageService.updateAlarmMessageState(messageId,2);
            } catch (Exception e) {
                errorIds.add(messageId);
                e.printStackTrace();
                log.info("发生异常,告警信息表id为{}的告警短信发送失败！",messageId);
                messageService.updateAlarmMessageState(messageId,3);
                tlogs.setStat(3);
                tlogs.setDuration(((Long)(System.currentTimeMillis()- execTime.getTime())).intValue());
                tlogs.setDesc("发生异常,告警信息表id为"+messageId+"的告警短信发送失败！");
                tlogs.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
                StaticUtil.taskLogsQueue.add(tlogs);
            }
        }

        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(start);
        taskLogs.setClassName(className);
        Date end = new Date();
        if(msgs.size() > 0){
            if(msgs.size() == successNum){
                //成功
                taskLogs.setDetail("告警短信任务执行完成");
                taskLogs.setStat(0);
                taskLogs.setDesc("告警短信发送成功！成功条数：" + msgs.size());
            }else if(successNum > 0 ){
                //部分成功
                taskLogs.setStat(1);
                taskLogs.setDesc("部分告警短信发送成功！成功条数：" + successNum + " 失败条数：" + errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
                taskLogs.setDetail("告警短信任务执行完成");
            }else{
                //失败(所有都失败)
                taskLogs.setStat(3);
                taskLogs.setDetail("告警短信任务执行失败");
                taskLogs.setDesc("告警短信发送失败！失败条数："+ errorIds.size()
                        + "发送失败告警信息id：" + errorIds.toString());
            }
            taskLogs.setDuration(((Long)(end.getTime()- start.getTime())).intValue());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }

      }

    @Override
    public void send(Message parm) {
        try {
            String reply = SocketUtil.sendAlarmMsg(parm.getIp(), parm.getPort(), parm.getContent());
            log.info("告警短信发送返回信息：{}",reply);
//            String RespCode = reply.substring(reply.indexOf("<RespCode>")+10, reply.indexOf("</RespCode>"));
//            String RespMsg = reply.substring(reply.indexOf("<RespMsg>")+9, reply.indexOf("</RespMsg>"));
//            if("000000".equals(RespCode)){
//                messageService.updateAlarmMessageState(alarmMessage.getId(),1);//更新告警信息状态
//                log.info("告警信息表id为"+alarmMessage.getId()+"的告警短信发送成功！");
//            }else{
//                alarmNotifyJobService.updateAlarmMessageState(alarmMessage.getId(),2);//更新告警信息状态
//                log.info("告警信息表id为"+alarmMessage.getId()+"的告警短信发送失败！具体信息"+ RespMsg);
//                tl.setStatus(1);
//                tel.setErrDesc("告警信息表id为"+alarmMessage.getId()+"的告警短信发送失败！具体信息"+ RespMsg);
//                tel.setTimeCost(new Date().getTime()-start);
//                StaticUtil.timerErrLogsQueue.add(tel);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShortMessageException(e.getMessage());
        }
    }


    public String buildContentByParams(Date execTime, String alarmMessage, List<String> mobiles
        , String channelidStr, String msgclassidStr, String appidStr, String openbrcStr) throws UnsupportedEncodingException {
        String content = "<root><head>"
                + "<WorkDate>"+ new SimpleDateFormat("yyyy-MM-dd").format(execTime) +"</WorkDate>"
                + "<WorkTime>"+ new SimpleDateFormat("HHmmss").format(execTime) +"</WorkTime>"
                + "<MsgId>00000000</MsgId>"
                + "<Flag>0</Flag>"
                + "<SvrType>JKPT</SvrType>"
                + "<SvrCode>C00200</SvrCode>"
                + "<RespCode></RespCode>"
                + "<RespMsg></RespMsg>"
                + "<Remark></Remark>"
                + "</head>"
                + "<body>"
                + "<ChannelId>"+ channelidStr +"</ChannelId>"
                + "<MsgClassId>"+ msgclassidStr +"</MsgClassId>"
                + "<AppId>"+ appidStr +"</AppId>"
                + "<Address>" + StringUtils.join(mobiles, ";") + "</Address>"
                + "<TemplateFlag>1</TemplateFlag>"
                + "<MsgContent>" + alarmMessage + "</MsgContent>"
                + "<PlanTime></PlanTime>"
                + "<Fname></Fname>"
                + "<Fvalue></Fvalue>"
                + "<OpenBrc>"+ openbrcStr +"</OpenBrc>"
                + "</body>"
                + "</root>";
        //报文头的8位数字
        String len = String.format("%08d", content.getBytes("gbk").length);
        //完整的报文信息
        String msgSend = len + content;
        log.info("短信告警报文信息:{}",msgSend);
        return msgSend;
    }

    public String buildContent(Date execTime, String alarmMessage, List<String> mobiles) throws UnsupportedEncodingException {
        String content = "<root><head>"
                + "<WorkDate>"+ new SimpleDateFormat("yyyy-MM-dd").format(execTime) +"</WorkDate>"
                + "<WorkTime>"+ new SimpleDateFormat("HHmmss").format(execTime) +"</WorkTime>"
                + "<MsgId>00000000</MsgId>"
                + "<Flag>0</Flag>"
                + "<SvrType>JKPT</SvrType>"
                + "<SvrCode>C00200</SvrCode>"
                + "<RespCode></RespCode>"
                + "<RespMsg></RespMsg>"
                + "<Remark></Remark>"
                + "</head>"
                + "<body>"
                + "<ChannelId>"+ channelid +"</ChannelId>"
                + "<MsgClassId>"+ msgclassid +"</MsgClassId>"
                + "<AppId>"+ appid +"</AppId>"
                + "<Address>" + StringUtils.join(mobiles, ";") + "</Address>"
                + "<TemplateFlag>1</TemplateFlag>"
                + "<MsgContent>" + alarmMessage + "</MsgContent>"
                + "<PlanTime></PlanTime>"
                + "<Fname></Fname>"
                + "<Fvalue></Fvalue>"
                + "<OpenBrc>"+ openbrc +"</OpenBrc>"
                + "</body>"
                + "</root>";
        //报文头的8位数字
        String len = String.format("%08d", content.getBytes("gbk").length);
        //完整的报文信息
        String msgSend = len + content;
        log.info("短信告警报文信息:{}",msgSend);
        return msgSend;
    }

    @Override
    public String sendAndReturn(Message parm) {
        return null;
    }
}
