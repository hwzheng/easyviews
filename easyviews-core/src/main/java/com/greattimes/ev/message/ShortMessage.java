package com.greattimes.ev.message;

/**
 * <p>
 *     短信接口
 * </p>
 * @author LiHua
 * @since 2018-10-10
 */
public interface ShortMessage<T> {

    /**
     * 短消息构建
     */
    void build();

    /***
     * 短消息发送
     */
    void send(T parm);

    /**
     * 短信发送返回值
     * @param parm
     * @return
     */
    String sendAndReturn(T parm);

}
