package com.greattimes.ev.message.PAB;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.entity.Application;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.JsonConfig;
import com.greattimes.ev.system.entity.Message;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IJsonConfigService;
import com.greattimes.ev.system.service.IMessageService;
import com.greattimes.ev.utils.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2019/9/23 14:18
 */
@Service
public class EventMessageOfPA {

    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 发送的url
     */
    private static  String ALARM_MESSAGE_EVENT_URL = "alarm_message_event_url";
    private static  String  EVENT_JSON_CODE = "EVENT_ALARM_PARAM";
    private static String APPIDUSEFORKEY = "appIdUseForKey";
    private static String APPNAMEUSEFORKEY = "appNameUseForKey";
    private static String TYPE = "type";
    private static String APPIDS = "appIds";
    private static String SOURCE = "source";
    private static String APPNAME = "appName";

    @Autowired
    private ConfigurationCache configurationCache;
    @Autowired
    private IMessageService messageService;
    @Autowired
    private IJsonConfigService jsonConfigService;
    @Autowired
    private IApplicationService applicationService;

    /**
     * 发送信息
     */
    public void sendMessage(){
        long start = System.currentTimeMillis();
        String sendUrl = configurationCache.getValue(ALARM_MESSAGE_EVENT_URL);
        //查询待发送数据
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("eventState", 0);
        paramMap.put("sendType", 0);
        EntityWrapper<Message> entityWrapper = new EntityWrapper<>();
        entityWrapper.where("eventState={0}", 0).le("sendType", 2);
        List<Message> messages = messageService.selectList(entityWrapper);
        if(evUtil.listIsNullOrZero(messages)){
            log.info("无相应的平安事件告警推送有待发送！");
            return ;
        }

        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        messages.stream().forEach(msg -> msg.setEventState(1));
        messageService.updateBatchById(messages);

        //查询相应的告警组人员(所有)
        Map<String, Object> groupMap = new HashMap<>();
        groupMap.put("groupActive", 1);
        groupMap.put("userActive", 1);
        List<AlarmGroupUserParam> alarmUser = messageService.selectAlarmUserList(groupMap);
        if(evUtil.listIsNullOrZero(alarmUser)){
            log.info("平安事件告警推送-无相应的发送用户!");
            return ;
        }
        Map<String,Object> jsonMap = new HashMap<>(3);
        jsonMap.put("code", EVENT_JSON_CODE);
        List<JsonConfig> configs = jsonConfigService.selectByMap(jsonMap);
        JSONObject eventJsonObj;
        Map<String, String> typeMap = new HashMap<>();
        Map<String, String> appIdsMap = new HashMap<>();
        Map<String, String> appNameMap = new HashMap<>();
        /**
         *  0 使用Key subsysId subsystemName
         *  1 使用Key appid appName
         */
        int appIdUseForKey = 0, appNameUseForKey = 0;
        String source;
        if(!evUtil.listIsNullOrZero(configs)){
            String jsonParam = configs.get(0).getParam();
            if(StringUtils.isNotBlank(jsonParam)){
                eventJsonObj = JSON.parseObject(jsonParam);
                typeMap = getKeyFromJSONObjectArr(eventJsonObj, TYPE);
                appIdsMap = getKeyFromJSONObjectArr(eventJsonObj, APPIDS);
                appNameMap = getKeyFromJSONObjectArr(eventJsonObj, APPNAME);
                appIdUseForKey = eventJsonObj.getIntValue(APPIDUSEFORKEY);
                appNameUseForKey = eventJsonObj.getIntValue(APPNAMEUSEFORKEY);
                source = eventJsonObj.getString(SOURCE);
            }else{
                log.info("平安事件告警推送缺少json配置!");
                return ;
            }
        }else{
            log.info("平安事件告警推送缺少json配置!");
            return;
        }

        //用户信息
        Map<Integer, AlarmGroupUserParam> userAlarmMap = alarmUser.stream().collect(Collectors.toMap(AlarmGroupUserParam::getId, Function.identity()));
        Set<Integer> applicationIds = messages.stream().collect(Collectors.mapping(Message::getApplicationId,Collectors.toSet()));
        EntityWrapper<Application> appWrapper = new EntityWrapper<>();
        appWrapper.in("id", applicationIds);
        List<Application> applications = applicationService.selectList(appWrapper);
        Map<Integer, String> appMap = applications.stream().collect(Collectors.toMap(Application::getId, Application::getName));
        Map<String, List<Message>> messagesMap = messages.stream().collect(Collectors.groupingBy(Message::getAlarmKey, Collectors.toList()));
        Message tempMessage;
        Integer applicationId;
        String applicationName;
        for(Map.Entry<String, List<Message>> entry : messagesMap.entrySet()) {
            List<Message> entryValue = entry.getValue();
            tempMessage = entryValue.get(0);
            applicationId = tempMessage.getApplicationId();
            applicationName = appMap.get(applicationId);
            EventParam param = new EventParam();
            //二选一的参数
            if(appIdUseForKey == 0){
                param.setSubsysid(appIdsMap.getOrDefault(applicationId.toString(), applicationId.toString()));
                param.setAppid("");
            }else{
                param.setAppid(appIdsMap.getOrDefault(applicationId.toString(), applicationId.toString()));
                param.setSubsysid("");
            }
            if(appNameUseForKey == 0){
                param.setSubsystemName(appNameMap.getOrDefault(applicationName, applicationName));
                param.setAppName("");
            }else{
                param.setAppName(appNameMap.getOrDefault(applicationName, applicationName));
                param.setSubsystemName("");
            }
            param.setSource(source);
            param.setFlag(tempMessage.getAlarmType());
            param.setLevel(tempMessage.getAlarmLevel().toString());
            param.setKey(tempMessage.getAlarmKey());
            /**
             *  处理contents里面的内容
             *
             *  "contents": {
             *       "sms": {
             *           "receivers": "张三,李四,王二",
             *           "title": "发送短信",
             *           "content": "发送短信告警"
             *      },
             *      "email": {
             *          "receivers": "张三",
             *          "title": "发送邮件",
             *          "content": "发送邮件告警"
             *       }
             *  },
             *
             */
            AlarmGroupUserParam  groupUserParam = userAlarmMap.get(Integer.parseInt(tempMessage.getAlarmGroupId()));
            HashMap<String, String> receiverMap = new HashMap<>();
            List<UserParam> userParams = groupUserParam.getUsers();
            if(evUtil.listIsNullOrZero(userParams)){
                continue;
            }
            //receiverMap.put("receivers", groupUserParam.getUsers().stream().collect(Collectors.mapping(UserParam::getName,Collectors.joining(","))));
            receiverMap.put("content", tempMessage.getMessage());
            String type,receiverCode;
            List<Integer> ids = new ArrayList<>();
            Map<String, Object> conMap = new HashMap<>();
            //排除告警类型为: 4外呼 5微信 类型
            String receivers;
            for(Message content : entryValue){
                type = content.getType();
                if(type.equals("4") || type.equals("5")){
                    continue;
                }
                receiverCode = typeMap.get(type);
                if( receiverCode != null){
                    HashMap<String, String> cloneMap = (HashMap<String, String>)receiverMap.clone();
                    //根据告警类型确定receivers,title
                    cloneMap.put("title", this.getTitleByMessageType(Integer.parseInt(type)));
                    receivers =  this.getReceiverByMessageType(Integer.parseInt(type), groupUserParam.getUsers());
                    if(StringUtils.isBlank(receivers)){
                        log.info("平安告警接收人信息非法！");
                        continue;
                    }
                    cloneMap.put("receivers", receivers);
                    conMap.put(receiverCode , cloneMap);
                    ids.add(content.getId());
                }
            }
            if(conMap.isEmpty()){
                continue;
            }
            param.setContents(conMap);
            param.setTime(tempMessage.getTimestamp()/1000);
            //发送信息
            String result;
            try {
                log.info("平安事件告警推送通知url：{}", sendUrl);
                String jsonStr = JSON.toJSONString(param, SerializerFeature.WriteMapNullValue);
                log.info("平安事件告警推送通知发送参数json:{}", jsonStr);
                result = HttpClientUtil.posts(sendUrl, jsonStr);
                log.info("平安事件告警推送通知原始返回值:{}", result);
                JSONObject jb = JSON.parseObject(result);
                Object code = jb.get("code");
                //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
                if( code != null && "200".equals(code.toString())){
                    messageService.updateAlarmMessageEventState(ids,2);
                }else{
                    messageService.updateAlarmMessageEventState(ids,3);
                }
            } catch (Exception e) {
                log.info("平安事件告警推送通知：" + e.getMessage());
                messageService.updateAlarmMessageEventState(ids,3);
            }
        }
        log.info("平安事件告警推送通知发送耗时：{}ms", (System.currentTimeMillis()-start));
    }


    /**
     * 将json配置中数组转换为map
     * @param jsonObject
     * @param key
     * @return
     */
    private Map<String, String> getKeyFromJSONObjectArr(JSONObject jsonObject, String key){
        Map<String, String> result = new HashMap<>();
        JSONArray jsonArray = jsonObject.getJSONArray(key);
        if(jsonArray == null || jsonArray.isEmpty()){
            return result;
        }
        JSONObject obj;
        for(int i = 0; i < jsonArray.size(); i++){
            obj = jsonArray.getJSONObject(i);
            result.put(obj.getString("easyviews"), obj.getString("thirdParty"));
        }
        return result;
    }

    /**
     * 根据不同发送方式，拼接事件告警title告警描述
     * @param type
     * @return
     */
    private String getTitleByMessageType(int type){
        /**
         *  发送类型 （同告警发送方式一致） 1 邮件 ，2 短信 ，3app ，4外呼 5微信
         */
        String str;
        switch (type){
            case 1 :
                str = "邮件告警";
                break;
            case 2 :
                str = "短信告警";
                break;
            case 3 :
                str = "APP告警";
                break;
            case 4 :
                str = "外呼告警";
                break;
            default:
                str = "其它告警";
                break;
        }
        return str;
    }

    /**
     * 根据短信的类型返回不同receiver
     * 规则：短信告警和APP填写电话号码，邮件告警填写邮件地址
     * 发送类型 （同告警发送方式一致） 1 邮件 ，2 短信 ，3app ，4外呼 5微信
     * 平安只处理发送类型1,2,3
     * @param type
     * @return
     */
    private String getReceiverByMessageType(int type, List<UserParam> userParamList){
        String receiver = null;
        if(evUtil.listIsNullOrZero(userParamList)){
            log.info("平安事件台通知的用户为空！");
            return receiver;
        }
        if(type == 1){
            receiver = userParamList.stream().filter(userParam -> StringUtils.isNotBlank(userParam.getEmail()))
                    .collect(Collectors.mapping(UserParam::getEmail, Collectors.joining(",")));
        }else if(type == 2 || type == 3){
            StringBuilder sb = new StringBuilder("");
            userParamList.stream().forEach(x->{
                if(x.getPhone() != null){
                    sb.append(x.getPhone().toString()).append(",");
                }
            });
            receiver = sb.toString();
            if(StringUtils.isNotBlank(receiver)){
                receiver = receiver.substring(0, receiver.lastIndexOf(","));
            }
        }else{
            log.info("平安事件台通知告警发送类型非法！告警发送类型：{}",type);
        }
        return receiver;
    }



}
