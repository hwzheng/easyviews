package com.greattimes.ev.liaoning.message.thread;

import com.greattimes.ev.utils.SocketUtil;
import com.greattimes.ev.utils.StaticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author NJ
 * @date 2019/3/1 18:25
 */
public class AlarmMsgSend implements Runnable {

    Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * 告警ip
     */
    private String alarmIp;
    /**
     * 告警的端口
     */
    private String port;

    public String getAlarmIp() {
        return alarmIp;
    }

    public void setAlarmIp(String alarmIp) {
        this.alarmIp = alarmIp;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    @Override
    public void run() {
        log.info("事件台告警报文信息发送线程启动....");
        boolean running=true;
        while(running){
            try {
                log.info("事件台短信告警报文信息准备发送....");
                String msg= StaticUtil.alarmMsgQueue.take();
                String reply = SocketUtil.sendAlarmMsg(alarmIp,port,msg);
                log.info("事件台告警短信返回结果:{}", reply);

//                String RespCode = reply.substring(reply.indexOf("<RespCode>")+10, reply.indexOf("</RespCode>"));
//                String RespMsg = reply.substring(reply.indexOf("<RespMsg>")+9, reply.indexOf("</RespMsg>"));
//                if("000000".equals(RespCode)){
//                    log.info("告警报文信息发送成功....");
//                }else{
//                    log.error("告警报文信息发送失败，具体信息：" + RespMsg);
//                }

            } catch (Exception e) {
                log.error("告警报文信息发送失败:>>"+ e +"/n" +e.getMessage());
                e.printStackTrace();
                continue;
            }


        }
        log.info("告警报文信息发送线程停止....");
    }
}
