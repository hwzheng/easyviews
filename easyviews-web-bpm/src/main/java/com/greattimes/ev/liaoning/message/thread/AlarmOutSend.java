package com.greattimes.ev.liaoning.message.thread;

import com.greattimes.ev.utils.HttpClientUtil;
import com.greattimes.ev.utils.StaticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author NJ
 * @date 2019/3/1 18:40
 */
public class AlarmOutSend implements Runnable{

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void run() {
        log.info("事件台外呼告警报文信息发送线程启动....");
        boolean running=true;
        while(running){
            try {
                log.info("事件台外呼告警报文信息准备发送....");
                String msg = StaticUtil.outAlarmMsgQueue.take();
                HttpClientUtil.httpGetRequest(msg);
                log.info("事件台外呼告警报文信息发送成功....报文信息为：{}",msg);
            } catch (Exception e) {
                log.info("事件台外呼告警报文信息发送失败:>>"+ e +"/n" +e.getMessage());
                e.printStackTrace();
                continue;
            }
        }
        log.info("事件台外呼告警报文信息发送线程停止....");
    }
}
