package com.greattimes.ev.liaoning.controller;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.liaoning.entity.JDetailData;
import com.greattimes.ev.liaoning.message.thread.AlarmMsgSend;
import com.greattimes.ev.liaoning.message.thread.AlarmOutSend;
import com.greattimes.ev.liaoning.service.ILiaoNingEventService;
import com.greattimes.ev.system.service.IDeptService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @author NJ
 * @date 2019/3/1 17:38
 */
//@RestController
//@Api(value = "EventController", tags = "事件监控台")
//@RequestMapping("/event")
public class EventController  extends BaseController {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ILiaoNingEventService iLiaoNingEventService;

    @Autowired
    private ConfigurationCache cache;
    @Autowired
    private IDeptService deptService;

//    @PostConstruct
    private void init(){
        //告警短信发送线程启动
        AlarmMsgSend msgSend = new AlarmMsgSend();
        try {
//            String alarmIp = cache.getValue("alarm_ip");
//            String alarmPort = cache.getValue("alarm_port");
            String alarmIp = cache.getDefaultValue("alarm_ip");
            String alarmPort = cache.getDefaultValue("alarm_port");
            if(alarmIp == null || alarmPort == null){
                log.info("参数中不包含alarm_ip或者alarm_port");
                return ;
            }
            msgSend.setAlarmIp(alarmIp);
            msgSend.setPort(alarmPort);
            log.info("事件台告警发送任务参数:alarm_ip:{},alarm_port:{}",alarmIp, alarmPort);
            Thread t1 = new Thread(msgSend);
            t1.start();
            //外呼告警发送线程启动
            AlarmOutSend outSend = new AlarmOutSend();
            Thread t2=new Thread(outSend);
            t2.start();
        } catch (Exception e) {
            e.printStackTrace();
            log.info("事件台告警发送任务线程未启动,异常信息:{}", e.getMessage());
        }
    }

    /**
     * 时间戳的获取
     * @author NJ
     * @date 2019/3/1 17:53
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequestMapping(value="/time",method= RequestMethod.GET)
    public JsonResult<Object> getEventTime(){
        Long time = System.currentTimeMillis() - Long.parseLong(cache.getValue("ln_event_delay")) * 60 * 1000;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(time));
        c.set(Calendar.MILLISECOND, 0);
        c.set(Calendar.SECOND,0);
        Map<String, Object> result = new HashMap<>();
        result.put("currTime", c.getTimeInMillis());
        return execSuccess(result);
    }

    /**
     * 事件台初始化
     * @author NJ
     * @date 2019/3/1 17:54
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequestMapping(value="/init",method=RequestMethod.POST)
    public JsonResult<Object> initChart(@RequestBody JSONObject param){
        Map<String,Object> m = iLiaoNingEventService.initQuery(param.getLongValue("time"));
        return execSuccess(m);
    }

    /**
     * 事件台详情
     * @param param
     * @return
     */
    @RequestMapping(value="/detail",method=RequestMethod.POST)
    public JsonResult<Object> queryDetail(@RequestBody JSONObject param){
        List<JDetailData> m = iLiaoNingEventService.detailQuery(param.getLongValue("time"));
        return execSuccess(m);
    }


    /**
     * 事件台某一指标的历史数据
     * @author NJ
     * @date 2019/3/1 18:05
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequestMapping(value="/history",method=RequestMethod.POST)
    public JsonResult<Object> queryHistory(@RequestBody JSONObject param){
        long time = param.getLongValue("time");
        String name = param.getString("name");
        Integer type = param.getInteger("type");
        Map<String, Object> m = iLiaoNingEventService.historyQuery(time, name, type);
        return execSuccess(m);
    }

    /***
     * 事件台中间服务数查询
     * @author NJ
     * @date 2019/3/1 18:09
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequestMapping(value="/service",method=RequestMethod.POST)
    public JsonResult<Object> getServiceInfo(@RequestBody JSONObject param){
        Map<String, Object> map = new HashMap<>();
        long time = param.getLongValue("time");
        map.put("time", time);
        map.put("dateStr", DateUtils.generateDateSqlStr(time, time, "date"));
        Integer ServiceCountNum = iLiaoNingEventService.getServiceCountByTime(map);
        List<Map<String,Object>> top5ServiceInfo = iLiaoNingEventService.getServiceTopN(time, 5);

//        Integer totalServiceNum = iLiaoNingEventService.getAllServiceNum();
        Integer totalServiceNum = deptService.getAllServiceNum();

        List<String> list = iLiaoNingEventService.getTuxTqueueRqAddr(time);
        List<String> serviceInfo = deptService.selectMissService(list);
        Map<String,Object> rtMap = new HashMap<>();
        rtMap.put("serviceCountNum",ServiceCountNum);
        rtMap.put("top5ServiceInfo", top5ServiceInfo);
        rtMap.put("totalServiceNum", totalServiceNum);
        rtMap.put("missServiceInfo",serviceInfo);
        return execSuccess(rtMap);
    }

}
