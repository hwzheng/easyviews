package com.greattimes.ev.bpm.controller.analysis;

import java.util.List;

import com.greattimes.ev.common.annotation.MethodCost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.indicator.param.resp.DashBoardParam;
import com.greattimes.ev.indicator.service.IDashBoardService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


/**
 * @author cgc
 * @date 2018/9/11 19:56
 */
@RestController
@RequestMapping(value = "/dashBoard/mainmonitor")
@Api(value="DashBoardController",tags="主监控台")
public class DashBoardController  extends BaseController {

    @Autowired
    private IDashBoardService dashBoardService;
    
    
    @ApiOperation(value="查询", notes="查询")
    @ApiImplicitParams({
        @ApiImplicitParam(name="ids",value="应用id",dataType="int", paramType = "query",allowMultiple=true),
        @ApiImplicitParam(name="start",value="开始时间戳",dataType="long", paramType = "query"),
        @ApiImplicitParam(name="end",value="结束时间戳",dataType="long", paramType = "query")
    })
    @MethodCost(desc = "主监控台查询")
    @RequestMapping(value = "/chart", method = RequestMethod.POST)
    public JsonResult<Object> select(@RequestBody JSONObject param) {
    	List<Integer> ids=(List<Integer>) param.get("ids");
    	Long start=param.getLong("start");
    	Long end=param.getLong("end");
    	//parameter check
        if(ids== null ||start == null || end==null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	DashBoardParam alarmlist= dashBoardService.getDashBoardData(ids, start, end);
        return execSuccess(alarmlist);
    }
}
