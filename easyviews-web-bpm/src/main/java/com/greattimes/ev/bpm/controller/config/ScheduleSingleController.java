package com.greattimes.ev.bpm.controller.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.entity.ScheduleSingle;
import com.greattimes.ev.bpm.service.config.IScheduleService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 单次排期表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@RestController
@Api(value="ScheduleSingleController",tags="单次排期管理模块")
@RequestMapping("/bpm/application/schedule/single")
@RequirePermission("alarmSetting")
public class ScheduleSingleController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IScheduleService scheduleService;
	@Autowired
	private INoticeService noticeService;

	/**
	 * 单次排期列表查询
	 * @param req 
	 * @param map
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value="单次排期列表查询", notes="单次排期列表查询" , response =ScheduleSingle.class ,responseContainer="List")
	@ApiImplicitParams({
		@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> select(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		List<ScheduleSingle> list = scheduleService.selectSingleSchedule(applicationId);
		return execSuccess(list);
	}

	/**
	 * 单次排期新增
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="单次排期新增", notes="单次排期新增")
	@ApiImplicitParams({
		@ApiImplicitParam(name="start",value="开始时间",dataType="Long", paramType = "query"),
		@ApiImplicitParam(name="end",value="结束时间",dataType="Long", paramType = "query"),
		@ApiImplicitParam(name="remarks",value="描述",dataType="String", paramType = "query"),
		@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query")
	})
	@OperateLog(type = OperaterType.INSERT, desc = "单次排期新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> add(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		scheduleService.addSingleSchedule(map);
		//sys_notice
        noticeService.insertNoticeByParam(2201,"单次排期新增",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 单次排期编辑
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="单次排期编辑", notes="单次排期编辑")
	@ApiImplicitParams({
		@ApiImplicitParam(name="start",value="开始时间",dataType="Long", paramType = "query"),
		@ApiImplicitParam(name="end",value="结束时间",dataType="Long", paramType = "query"),
		@ApiImplicitParam(name="remarks",value="描述",dataType="String", paramType = "query"),
		@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query"),
		@ApiImplicitParam(name="id",value="id",dataType="int", paramType = "query")
	})
	@OperateLog(type = OperaterType.UPDATE, desc = "单次排期编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		scheduleService.editSingleSchedule(map);
		//sys_notice
        noticeService.insertNoticeByParam(2202,"单次排期修改",0,0);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}
	
	/**
	 * 单次排期删除
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="单次排期删除", notes="单次排期删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name="id",value="id",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "单次排期删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		scheduleService.deleteSingleSchedule(id);
		//sys_notice
        noticeService.insertNoticeByParam(2203,"单次排期删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
}
