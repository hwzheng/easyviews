package com.greattimes.ev.bpm.controller.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.req.AlarmSendListParam;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.greattimes.ev.bpm.entity.AlarmSend;
import com.greattimes.ev.bpm.service.config.IAlarmSendService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 告警信息发送配置表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@Deprecated
@RestController
@Api(value="AlarmSendController",tags="告警发送模块")
@RequestMapping("/bpm/application/alarm/send")
public class AlarmSendController extends BaseController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IAlarmSendService alarmSendService;

	/**
	 * 告警发送查询
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警发送查询", notes="告警发送查询" , response =AlarmSend.class ,responseContainer="List")
	@ApiImplicitParams({
		@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> select(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		List<AlarmSend> list = alarmSendService.selectAlarmSend(map);
		return execSuccess(list);
	}

	/**
	 * 告警发送新增
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警发送新增", notes="告警发送新增")
	@OperateLog(type = OperaterType.INSERT, desc = "告警发送新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> add(HttpServletRequest req, @RequestBody AlarmSend alarmSend) {
		alarmSendService.addAlarmSend(alarmSend);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 告警发送编辑
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警发送编辑", notes="告警发送编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "告警发送编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(HttpServletRequest req, @RequestBody AlarmSend alarmSend) {
		alarmSendService.editAlarmSend(alarmSend);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 告警组查询
	 * @param req
	 * @return
	 */
	@ApiOperation(value="告警组查询", notes="告警组查询" , response =AlarmGroup.class ,responseContainer="List")
	@RequestMapping(value = "/alarmgroup", method = RequestMethod.GET)
	public JsonResult<Object> selectAlarmGroup(HttpServletRequest req) {
		return execSuccess(alarmSendService.selectAlarmGroup());
	}

	/**
	 * 告警发送删除
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警发送删除", notes="告警发送删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name="id",value="告警发送id",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "告警发送删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		alarmSendService.deleteAlarmSend(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 告警发送状态
	 * @param req
	 * @param list
	 * @return
	 */
	@ApiOperation(value="告警发送状态更改", notes="告警发送状态更改")
	@OperateLog(type = OperaterType.UPDATE, desc = "告警发送状态更改")
	@RequestMapping(value = "/active", method = RequestMethod.POST)
	public JsonResult<Object> active(HttpServletRequest req, @RequestBody AlarmSendListParam param) {
		alarmSendService.updateAlarmSendActive(param);
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}
}
