package com.greattimes.ev.bpm.controller.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.decode.param.req.JHttp;
import com.greattimes.ev.bpm.decode.param.req.JParams;
import com.greattimes.ev.bpm.decode.param.req.JParseGroup;
import com.greattimes.ev.bpm.decode.param.req.JProtocolDetail;
import com.greattimes.ev.bpm.decode.param.req.JProtocolRecognition;
import com.greattimes.ev.bpm.decode.param.req.JReqProtocolRecognition;
import com.greattimes.ev.bpm.entity.Protocol;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.HexUtils;
import com.greattimes.ev.common.utils.JsonUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;

import cn.com.greattimes.npds.ext.wrapper.SmartWrapper;
import cn.com.greattimes.npds.psa.analysis.MessageGroupAnalyst;
import cn.com.greattimes.npds.psa.analysis.PcapSessionAnalyst;
import cn.com.greattimes.npds.psa.exception.NoSpecifiedPacketException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 协议表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@RestController
@Api(value = "ProtocolController", tags = "协议管理模块")
@RequestMapping("/bpm/protocol")
@RequirePermission("decode")
public class ProtocolController extends BaseController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IProtocolService protocolService;

	@Autowired
	private INoticeService noticeService;

	/**
	 * 协议查询
	 * @param param
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "协议查询", notes = "协议查询", response = Protocol.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "flag", value = "1模版协议 0应用协议", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	public JsonResult<Object> queryCustomProtocol(HttpServletRequest req,@RequestBody JSONObject param ) {
		int flag=param.getIntValue("flag");
		if(1==flag||2==flag) {
			EntityWrapper<Protocol> wrapper=new EntityWrapper<>();
			// int pageNumber = jsonObject.getIntValue("pageNumber");
			// int pageSize= jsonObject.getIntValue("pageSize");
			// String name = jsonObject.getString("name");
			// JsonResult<Page<Protocol>> ret = new JsonResult<>();
			// Page<Protocol> page = new Page<>(pageNumber, pageSize);
			// page = protocolService.queryCustomProtocol(page,name);
			// ret.setData(page);
			wrapper.where("flag={0}", param.getIntValue("flag")).ne("state", -1);
			List<Protocol> list=protocolService.selectList(wrapper);
			return execSuccess(list);
		}else if(0==flag) {
			Map<String, Object> map=(Map<String, Object>) param.get("filter");
			User user = super.getSessionUser(req);
			Integer userId = user.getId();
			return execSuccess(protocolService.selectAppProtocol(userId,map));
		}else {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
	}

	/**
	 * 修改协议状态并通知大数据
	 * 
	 * @author NJ
	 * @date 2018/8/21 15:39
	 * @param req
	 * @param id
	 * @param state
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "修改协议状态并通知大数据", notes = "修改协议状态并通知大数据")
	@OperateLog(type = OperaterType.UPDATE, desc = "修改协议状态并通知大数据")
	@RequestMapping(value = "/{id}/{state}", method = RequestMethod.GET)
	public JsonResult<Object> updateProtocolState(HttpServletRequest req, @PathVariable int id,
			@PathVariable int state) {
		User user = getSessionUser(req);
		Protocol protocol = new Protocol();
		protocol.setId(id);
		protocol.setState(state);
		protocol.setUpdateBy(user.getName());
		protocol.setUpdateDate(new Date());
		// 获得通知解码url
		//String url =configurationCache.getValue(ConfigConstants.DECODE_HOST)+configurationCache.getValue(ConfigConstants.DECODE_STATE_CHANGE_URL);
		String retMessage = protocolService.updateProtocolState(protocol, null);
		return execSuccess(retMessage);
	}

	/**
	 * pacp文件上传解析
	 * 
	 * @author NJ
	 * @date 2018/8/21 15:43
	 * @param file
	 * @param req
	 * @param serverAddress
	 * @param serverPort
	 * @param protocolMode
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "pacp文件上传解析", notes = "pacp文件上传解析")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<Object> pacpFileUpload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) CommonsMultipartFile file,
			HttpServletRequest req, @RequestParam String serverAddress, @RequestParam String serverPort,
			@RequestParam String protocolMode) throws IllegalStateException, IOException{
		// 处理传过来的ip和port
		List<Integer> ports = getPortsList(serverPort);
		if (StringUtils.isBlank(serverAddress) || ports.isEmpty() || file.isEmpty()) {
			return returnError("参数错误！");
		}
		// 获得原始文件名
		String fileRealName = file.getOriginalFilename();
		// 点号的位置
		int pointIndex = fileRealName.indexOf(".");
		// 截取文件后缀
		String fileSuffix = fileRealName.substring(pointIndex);
		// 生成文件的前缀包含连字符
		UUID FileId = UUID.randomUUID();
		// 文件存取名
		String savedFileName = FileId.toString().replace("-", "").concat(fileSuffix);
		// 获取服务器指定文件存取路径
		String savedDir = req.getSession().getServletContext().getRealPath("upload");
		File savedFile = new File(savedDir, savedFileName);
		if (!savedFile.getParentFile().exists()) {
			savedFile.getParentFile().mkdirs();
		}
		file.transferTo(savedFile);
		String resultStr;
		try {
			// 每次调用都要新建对象
			log.info("pacp文件上传解析请求参数serverAddress：" +serverAddress+" ports:"+ports );
			PcapSessionAnalyst pa = new PcapSessionAnalyst(serverAddress, ports);
			resultStr = pa.analysePcap(savedFile.toPath());
		} catch (NoSpecifiedPacketException e) {
			log.info("无对应ip端口的业务数据包！NoSpecifiedPacketException");
			e.printStackTrace();
			return execError("无对应ip端口的业务数据包！", null);
		} catch (Exception e) {
			log.info("解析异常：" + e.getMessage());
			e.printStackTrace();
			return execError("解析异常" + e.getMessage(), e.getMessage());
		} finally {
			if (savedFile.exists()) {
				savedFile.delete();
			}
		}
		log.info("pacp文件上传解析结果：" + resultStr);
		List<JProtocolRecognition> result = JSON.parseArray(resultStr, JProtocolRecognition.class);
		return execSuccess(result);
	}

	/**
	 * 根据协议id,查询协议详情，获取大json
	 * 
	 * @author NJ
	 * @date 2018/8/21 16:13
	 * @param id
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value = "查询协议详情", notes = "查询协议详情")
	@RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
	public JsonResult<Object> selectProtocolDetailById(
			@ApiParam(required = false, value = "协议id") @PathVariable int id) throws UnsupportedEncodingException {
		/*Map<String, Object> param = new HashMap<String, Object>();
		// 获得通知解码url
		String url = configurationCache.getValue(ConfigConstants.DECODE_DETAIL_URL);
		param.put("protocolId", id);
		String ret = HttpClientUtil.posts(url, JSON.toJSONString(param));*/

//		String ret =new String(protocolService.selectById(id).getContent());
		String ret =new String(protocolService.selectById(id).getContent(),"UTF-8");

		JSONObject jb = JSON.parseObject(ret);
		JProtocolDetail jp = JSON.toJavaObject(jb, JProtocolDetail.class);
		return execSuccess(jp);
		/*String retCode = jb.get("retCode").toString();
		String retMessage = jb.get("retMessage").toString();
		if ("000000".equals(retCode)) {
			JSONObject jbt = jb.getJSONObject("retData");
			JProtocolDetail jp = JSON.toJavaObject(jbt, JProtocolDetail.class);
			// System.out.println(jp);
			return execSuccess(jp);
		} else {
			return execError(retMessage, "");
		}*/
	}

	/**
	 * 解析生成协议详情
	 * 
	 * @author NJ
	 * @date 2018/8/21 17:30
	 * @param jReqProtolRec
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value = "解析生成协议详情", notes = "解析生成协议详情")
	@RequestMapping(value = "/parse/detail", method = RequestMethod.POST)
	public JsonResult<Object> getProtocolDetail(@RequestBody JReqProtocolRecognition jReqProtolRec) {
		// 参数校验
		if (StringUtils.isBlank(jReqProtolRec.getProtocolType()) || StringUtils.isBlank(jReqProtolRec.getMessageType())
				|| StringUtils.isBlank(jReqProtolRec.getClientSendBytes())) {
			return returnError("参数错误！");
		}
		MessageGroupAnalyst mga;
		String jsonStr;
		// clientSendBytes 请求不能为空 为空报异常，serverSendBytes 响应可为空
		try {
			log.info("解析生成协议详情请求参数ProtocolType：" +jReqProtolRec.getProtocolType()+" MessageType:"+jReqProtolRec.getMessageType() );
			mga = new MessageGroupAnalyst(jReqProtolRec.getProtocolType(), jReqProtolRec.getMessageType(), null);
			jsonStr = mga.getMessageGroup(jReqProtolRec.getClientSendBytes(), jReqProtolRec.getServerSendBytes());
			log.info("解析生成协议详情结果：{}" ,jsonStr);
		} catch (Exception e) {
			log.info("解析异常:" + e.getMessage());
			e.printStackTrace();
			return execError("解析异常:" + e.getMessage(), e.getMessage());
		}
		JProtocolDetail jpd = JSON.parseObject(jsonStr, JProtocolDetail.class);
		initDefaultValue(jpd);
		return execSuccess(jpd);
	}

	/**
	 * 分组message获取
	 * 
	 * @author NJ
	 * @date 2018/8/22 17:52
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "分组message获取", notes = "分组message获取")
	@RequestMapping(value = "/querymessage", method = RequestMethod.POST)
	public JsonResult<Object> queryGroupMessage(@RequestBody JSONObject jsonObject) {
		Map<String, Object> messageMap = new HashMap<>(2);
		messageMap.put("protocolMessage", protocolService.queryProtocolMessage(jsonObject.getIntValue("protocolId")));
		messageMap.put("groupMessage",
				protocolService.queryProtocolPacketMessage(jsonObject.getIntValue("protocolId")));
		return execSuccess(messageMap);
	}

	/**
	 * Hex字符串处理,目前支持html,xml,json
	 * 
	 * @author NJ
	 * @date 2018/8/22 14:02
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = " Hex字符串处理", notes = " Hex字符串处理")
	@ApiImplicitParams({ @ApiImplicitParam(name = "hex", value = "16进制字节内容", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "charset", value = "字符编码", dataType = "String", paramType = "query") })
	@RequestMapping(value = "/hextostr", method = RequestMethod.POST)
	public JsonResult<Object> ajaxHexToStr(@RequestBody JSONObject jsonObject) {
		String result = null;
		try {
			byte[] bytes = HexUtils.toBytes(jsonObject.getString("hex"));
			result = new String(bytes, jsonObject.getString("charset"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return execSuccess(result);
	}

	/**
	 * 支持xml、html、json 转化xpath
	 * 
	 * @author NJ
	 * @date 2018/8/22 14:14
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = " 字符串转换为xpath", notes = "字符串转换为xpath")
	@RequestMapping(value = "/fieldselection", method = RequestMethod.POST)
	public JsonResult<Object> toXpath(@RequestBody JSONObject jsonObject) {
		MessageGroupAnalyst mga;
		String jsonStr;
		String protocolType = jsonObject.getString("protocolCode");
		String messageType = jsonObject.getString("messageType");
		String clientSendBytes = jsonObject.getString("clientSendBytes");
		String serverSendBytes = jsonObject.getString("serverSendBytes");
		// clientSendBytes 请求不能为空 为空报异常，serverSendBytes 响应可为空
		try {
			log.info("字符串转换为xpath请求参数ProtocolType：" +protocolType+" MessageType:"+messageType);
			mga = new MessageGroupAnalyst(protocolType, messageType, jsonObject.toJavaObject(Map.class));
			jsonStr = mga.getMessageGroup(clientSendBytes, serverSendBytes);
			log.info("字符串转换为xpath结果：{}" ,jsonStr);
		} catch (Exception e) {
			log.info("解析异常:" + e.getMessage() == null ? "" : e.getMessage());
			e.printStackTrace();
			return execError("解析异常{}", e.getMessage() == null ? "!" : (":" + e.getMessage()));
		}
		JProtocolDetail jpd = JSON.parseObject(jsonStr, JProtocolDetail.class);
		if (jpd.getError() == 1) {
			return execError("参数配置错误！", "参数配置错误！");
		}
		if (jpd.getError() == 2) {
			return execError("分组配置错误！", "分组配置错误！");
		}
		List<JParseGroup> groups = jpd.getParseGroup();
		Map<String, String> resultMap = new HashMap<String, String>();
		try {
			for (JParseGroup group : groups) {
				String message = group.getMessage();
				// HexToStr
				String charSet = group.getCharset() == 1 ? "UTF-8" : "GB18030";
				byte[] bytes = HexUtils.toBytes(message);
				resultMap.put(group.getId().toString(), SmartWrapper.wrap(bytes, charSet));
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return execError("解析异常!", e.getMessage());
		} catch (DocumentException e) {
			e.printStackTrace();
			return execError("解析异常!", e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			return execError("解析异常!", e.getMessage());
		} catch (Exception e) {
			return execError("解析异常!", e.getMessage());
		}
		return execSuccess(resultMap);
	}

	/**
	 * 保存协议
	 * 
	 * @author NJ
	 * @date 2018/8/22 15:30
	 * @param req
	 * @param jProtocolDetail
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "保存协议", notes = "保存协议")
	@OperateLog(type=OperaterType.INSERT ,desc="保存协议")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> save(HttpServletRequest req, @RequestBody @Validated JProtocolDetail jProtocolDetail)
			throws Exception {
		User user = getSessionUser(req);
		// 获得解码url
		//String url =configurationCache.getValue(ConfigConstants.DECODE_HOST)+configurationCache.getValue(ConfigConstants.DECODE_SAVE_UPDATE_URL);
		Integer protocolId = jProtocolDetail.getProtocolId();
		String name=jProtocolDetail.getDesc();
		String key=jProtocolDetail.getProtocolKey();
		int flag=1;
		if(protocolService.checkProtocolName(protocolId, name, flag)) {
			if(protocolService.checkProtocolKey(protocolId, key, flag)) {
				String message = protocolService.saveOrUpdateProtocolDetail(null, user.getName(), jProtocolDetail,flag);
				return execSuccess(message);
			}else {
				return execCheckError("协议Key重复！");
			}
		}else {
			return execCheckError("协议名重复！");
		}
	}
	
	/**
	 * 编辑协议
	 * 
	 * @author NJ
	 * @date 2018/8/22 15:30
	 * @param req
	 * @param jProtocolDetail
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "编辑协议", notes = "编辑协议")
	@OperateLog(type=OperaterType.UPDATE ,desc="编辑协议")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> saveOrUpdate(HttpServletRequest req, @RequestBody @Validated JProtocolDetail jProtocolDetail)
			throws Exception {
		User user = getSessionUser(req);
		// 获得解码url
		//String url = configurationCache.getValue(ConfigConstants.DECODE_HOST)+configurationCache.getValue(ConfigConstants.DECODE_SAVE_UPDATE_URL);
		Integer protocolId = jProtocolDetail.getProtocolId();
		String name=jProtocolDetail.getDesc();
		String key=jProtocolDetail.getProtocolKey();
		int flag=0;
		if(protocolService.checkProtocolName(protocolId, name, flag)) {
			if(protocolService.checkProtocolKey(protocolId, key, flag)) {
				String message = protocolService.saveOrUpdateProtocolDetail(null, user.getName(), jProtocolDetail,flag);
				return execSuccess(message);
			}else {
				return execCheckError("协议Key重复！");
			}
		}else {
			return execCheckError("协议名重复！");
		}
	}
	/**
	 * 编辑应用协议
	 * 
	 * @author NJ
	 * @date 2018/8/22 15:30
	 * @param req
	 * @param jProtocolDetail
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "编辑应用协议", notes = "编辑应用协议")
	@OperateLog(type=OperaterType.UPDATE ,desc="编辑应用协议")
	@RequestMapping(value = "/application/edit", method = RequestMethod.POST)
	public JsonResult<Object> applicationEdit(HttpServletRequest req, @RequestBody @Validated JProtocolDetail jProtocolDetail)
			throws Exception {
		User user = getSessionUser(req);
		// 获得解码url
		//String url = configurationCache.getValue(ConfigConstants.DECODE_HOST)+configurationCache.getValue(ConfigConstants.DECODE_SAVE_UPDATE_URL);
		String message = protocolService.saveOrUpdateProtocolDetail(null, user.getName(), jProtocolDetail,0);
		//sys_notice
        noticeService.insertNoticeByParam(2401,"编辑应用协议",0,0);
        noticeService.insertNoticeByParam(2401,"编辑应用协议",1,0);
		return execSuccess(message);
	}

	/**
	 * 获取端口列表 ，以换行形式分组 端口范围以-分隔 形如：80-90\n8080-8090
	 * 
	 * @param: serverPort
	 * @return List<Integer>
	 */
	private List<Integer> getPortsList(String serverPort) {
		List<Integer> result = new ArrayList<Integer>();
		Set<Integer> ports = new HashSet<>();
		if (StringUtils.isBlank(serverPort)) {
			return result;
		}
		String regx = "^[0-9\\-\\s]+$";
		if (!Pattern.matches(regx, serverPort)) {
			return result;
		}
		serverPort = serverPort.replaceAll(" ", "");
		String[] portsArr = serverPort.split("\n|\r");
		String[] portRangeArr = null;
		Integer begin, end, single;
		String beginStr, endStr;
		for (int i = 0; i < portsArr.length; i++) {
			String ranStr = portsArr[i];
			portRangeArr = ranStr.split("-");
			if (portRangeArr.length == 0) {
				continue;
			} else if (portRangeArr.length == 1) {
				// 单个端口
				String str = portRangeArr[0];
				if (StringUtils.isNotBlank(str) && StringUtils.isNumeric(str)) {
					single = Integer.parseInt(str);
					if (single >= 0 && single <= 65535) {
						ports.add(Integer.parseInt(str));
					} else {
						return result;
					}
				}
			} else if (portRangeArr.length == 2) {
				// 端口范围
				beginStr = portRangeArr[0];
				endStr = portRangeArr[1];
				if (StringUtils.isBlank(beginStr) || StringUtils.isBlank(endStr) || !StringUtils.isNumeric(beginStr)
						|| !StringUtils.isNumeric(endStr)) {
					return result;
				}
				begin = Integer.parseInt(beginStr);
				end = Integer.parseInt(endStr);
				if (begin > end || begin < 0 || end > 65535) {
					return result;
				}
				while (begin <= end) {
					ports.add(begin++);
				}
			} else {
				return result;
			}
		}
		result.addAll(ports);
		return result;
	}

	/**
	 * 默认值赋值
	 * 
	 * @param jpd
	 */
	private void initDefaultValue(JProtocolDetail jpd) {
		String protocolCode = jpd.getProtocolCode();
		// 默认同步 0：同步、1：异步
		jpd.setTransportMode(0);
		// 内置协议不支持启用、停用和删除，仅允许使用或作为模板目前阶段始终为false
		jpd.setIsBuiltin(false);
		// -1：删除、0：停用、1：启用
		jpd.setState(1);

		if ("110".equals(protocolCode)) {
			// HTTP
			JParams params = new JParams();
			JHttp http = new JHttp();
			http.setUriAsServiceCode(false);
			http.setThrowStaticResource(true);
			http.setReqSendMsgBody(false);
			http.setRespSendMsgBody(false);
			params.setHttp(http);
			jpd.setParams(params);
		} /*
			 * else if("100".equals(protocolCode)){//TCP JTcp tcp = new JTcp();
			 * tcp.setUseEBCDID(false); //tcp.set JTcpReqRespMsg reqMsg = new
			 * JTcpReqRespMsg(); reqMsg.setType(0); JTcpReqRespMsg respMsg = new
			 * JTcpReqRespMsg(); respMsg.setType(0); tcp.setReqMsg(reqMsg);
			 * tcp.setRespMsg(respMsg); params.setTcp(tcp); }
			 */
	}

	/**
	 * json文件上传
	 * @param file
	 * @param req
	 * @param protocolId
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@ApiOperation(value = "json文件上传", notes = "json文件上传")
	@RequestMapping(value = "/jsonupload", method = RequestMethod.POST)
	public JsonResult<Object> jsonFileUpload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req, @RequestParam int protocolId) throws UnsupportedEncodingException {
		File jsonfile = null;
		String fileName = file.getOriginalFilename();
		long len = file.getSize();//上传文件的大小, 单位为字节
		double fileSize = (double) len / 1024;//转换为K
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		if (!"json".equals(fieldType)) {
			return execCheckError("文件上传失败！文件类型错误");
		}
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}if(fileSize>65) {
			return execCheckError("文件上传失败!文件过大");
		}
		
		else{
			InputStream ins = null;
			try {
				ins = file.getInputStream();
			} catch (IOException e) {
				log.info("json文件上传异常！IOException");
				e.printStackTrace();
				return execError("json文件上传异常！", e.getMessage());
			}
			String savedDir = req.getSession().getServletContext().getRealPath("upload");
			jsonfile = new File(savedDir, file.getOriginalFilename());
			if (!jsonfile.getParentFile().exists()) {
				jsonfile.getParentFile().mkdirs();
			}
		    inputStreamToFile(ins, jsonfile);
		}

		StringBuilder sb = new StringBuilder();// 定义一个字符串缓存，将字符串存放缓存中
		try {
			FileReader reader = new FileReader(jsonfile);// 定义一个fileReader对象，用来初始化BufferedReader
			BufferedReader bReader = new BufferedReader(reader);// new一个BufferedReader对象，将文件内容读取到缓存
			String s = "";
			while ((s = bReader.readLine()) != null) {// 逐行读取文件内容，不读取换行符和末尾的空格
				sb.append(s + "\n");// 将读取的字符串添加换行符后累加存放在缓存中
			}
			bReader.close();
			//新建一个file，然后获取之前的地址使用file自带的delete方法
			File del = new File(jsonfile.toURI());
	   		del.delete();
		} catch (IOException e) {
			log.info("json文件上传异常！IOException");
			e.printStackTrace();
			return execError("json文件上传异常！", e.getMessage());
		}
		String str =new String(sb.toString().getBytes(), "UTF-8") ;
		User user = getSessionUser(req);
		String name=user.getName();
		/*byte[] arr=str.getBytes();
		Protocol entity=new Protocol();
		entity.setId(protocolId);
		entity.setContent(arr);
		User user = getSessionUser(req);
		String name=user.getName();
		entity.setUpdateBy(user.getName());
		entity.setUpdateDate(new Date());
		Boolean flag=protocolService.updateById(entity);*/
		JSONObject jb = null;
		try {
			jb = JSON.parseObject(str);
		} catch (Exception e) {
			return execCheckError("上传文件的json格式有误！");
		}
		Protocol p=protocolService.selectById(protocolId);
		Integer protocolFlag=p.getFlag();
		//json中的flag
		Integer protocolFlag2=jb.getIntValue("flag");
		//模版协议要校验协议名
		if(null!=protocolFlag&&(1==protocolFlag||2==protocolFlag)) {
			if (protocolService.checkProtocolName(protocolId, jb.getString("desc"), 2)) {
				if (protocolService.checkProtocolKey(protocolId, jb.getString("protocolKey"), 2)) {
					Boolean flag = null;
					if(1==protocolFlag) {
						flag = protocolService.updateByUploadJson(name, str, protocolId);
					}else {
						flag = protocolService.updateLogsByUploadJson(name, str, protocolId);
					}
					if (flag) {
						// sys_notice
						noticeService.insertNoticeByParam(2402, "上传json文件", 0, 0);
						noticeService.insertNoticeByParam(2402, "上传json文件", 1, 0);
						return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
					}
					return execCheckError(Constant.UPLOAD_ERROR_MSG);
				}
				else {
					return execCheckError("协议Key重复！");
				}
			}
			else {
				return execCheckError("协议名重复！");
			}
		}
		//应用协议不校验协议名
		if (null != protocolFlag && 0 == protocolFlag) {
			// 校验一下protocolId
			Integer jsonProtocolId = jb.getIntValue("protocolId");
			if (jsonProtocolId.equals(protocolId)) {
				Boolean flag = null;
				// 判断是否是日志协议
				if (2 == protocolFlag2) {
					flag = protocolService.updateLogsByUploadJson(name, str, protocolId);
				} else {
					flag = protocolService.updateByUploadJson(name, str, protocolId);
				}
				if (flag) {
					// sys_notice
					noticeService.insertNoticeByParam(2402, "上传json文件", 0, 0);
					noticeService.insertNoticeByParam(2402, "上传json文件", 1, 0);
					return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
				}
				return execCheckError(Constant.UPLOAD_ERROR_MSG);
			} else {
				return execCheckError("上传文件的protocolId是否为["+protocolId+"]!");
			}
		}else {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
	}
	
	/**
	 * 上传不能识别协议json
	 * @param file
	 * @param req
	 * @return
	 * @throws Exception 
	 */
	@RequirePermission("modelAddUpload")
	@ApiOperation(value = "上传不能识别协议json", notes = "上传不能识别协议json")
	@OperateLog(type=OperaterType.INSERT,desc="上传不能识别协议")
	@RequestMapping(value = "/jsonadd", method = RequestMethod.POST)
	public JsonResult<Object> addJson(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req) throws Exception {
		File jsonfile = null;
		String fileName = file.getOriginalFilename();
		long len = file.getSize();//上传文件的大小, 单位为字节
		double fileSize = (double) len / 1024;//转换为K
		
		if (file.isEmpty() || fileName.lastIndexOf(".json") == -1) {
			return execCheckError("文件上传失败！请确认文件类型是否正确");
		}if(fileSize>65) {
			return execCheckError("文件上传失败!文件过大");
		}
		else{
			InputStream ins = null;
			try {
				ins = file.getInputStream();
			} catch (IOException e) {
				log.info("json文件上传异常！IOException");
				e.printStackTrace();
				return execError("json文件上传异常！", e.getMessage());
			}
			String savedDir = req.getSession().getServletContext().getRealPath("upload");
			jsonfile = new File(savedDir, file.getOriginalFilename());
			if (!jsonfile.getParentFile().exists()) {
				jsonfile.getParentFile().mkdirs();
			}
		    inputStreamToFile(ins, jsonfile);
		}

		StringBuilder sb = new StringBuilder();// 定义一个字符串缓存，将字符串存放缓存中
		try {
			FileReader reader = new FileReader(jsonfile);// 定义一个fileReader对象，用来初始化BufferedReader
			BufferedReader bReader = new BufferedReader(reader);// new一个BufferedReader对象，将文件内容读取到缓存
			String s = "";
			while ((s = bReader.readLine()) != null) {// 逐行读取文件内容，不读取换行符和末尾的空格
				sb.append(s + "\n");// 将读取的字符串添加换行符后累加存放在缓存中
			}
			bReader.close();
			//新建一个file，然后获取之前的地址使用file自带的delete方法
			File del = new File(jsonfile.toURI());
	   		del.delete();
		} catch (IOException e) {
			log.info("json文件上传异常！IOException");
			e.printStackTrace();
			return execError("json文件上传异常！", e.getMessage());
		}
		String name = getSessionUser(req).getName();
		String str =new String(sb.toString().getBytes(), "UTF-8") ;
		JSONObject jb = null;
		try {
			jb = JSON.parseObject(str);
		} catch (Exception e) {
			return execCheckError("上传文件json格式有误！");
		}
		String pname=jb.getString("desc");
		String key=jb.getString("protocolKey");
		Integer analyzeflag=jb.getIntValue("analyzeflag");
		if (protocolService.checkProtocolName(0, pname, 1)) {
			if (protocolService.checkProtocolKey(0, key, 1)) {
				//若是不能解析的协议
				if(analyzeflag.equals(0)) {
				boolean flag = protocolService.addJson(name, str);
				if (flag) {
					return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
				}
				return execCheckError(Constant.UPLOAD_ERROR_MSG);
				}else {//可解析协议
					return execSuccess(protocolService.saveOrUpdateProtocolDetail(null, name, jb.toJavaObject(JProtocolDetail.class), 1));
				}
			} else {
				return execCheckError("协议Key重复！");
			}
		}else {
			return execCheckError("协议名重复！");
		}
	}


	/**
	 * 将流写入文件
	 * @param ins
	 * @param file
	 */
	private void inputStreamToFile(InputStream ins, File file) {
		 try {
			   OutputStream os = new FileOutputStream(file);
			   int bytesRead = 0;
			   byte[] buffer = new byte[8192];
			   while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
			    os.write(buffer, 0, bytesRead);
			   }
			   os.close();
			   ins.close();
			  } catch (Exception e) {
			   e.printStackTrace();
			  }
	}

	/**
	 * json下载
	 * @param req
	 * @param resp
	 * @param protocolId
	 * @return
	 */
	@ApiOperation(value = "json下载", notes = "json下载")
	@RequestMapping(value = "/jsondownload/{protocolId}", method = RequestMethod.GET)
	public JsonResult<Object> download(HttpServletRequest req, HttpServletResponse resp,
			@ApiParam(required = true, name = "protocolId", value = "协议id") @PathVariable int protocolId){
		// 查出需要下载的真实数据
		Protocol p=protocolService.selectById(protocolId);
		String str=new String(p.getContent());
		// 格式化json字符串
        str = JsonUtil.formatJson(str);
		// 将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=content.json");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			os.write(str.getBytes());
			os.close();
		} catch (IOException e) {
			log.info("json文件下载异常！IOException");
			e.printStackTrace();
			return execError("json文件下载异常！", e.getMessage());
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}
	
	/**
	 * js脚本上传
	 * @param file
	 * @param req
	 * @param protocolId
	 * @return
	 */
	@RequirePermission("appJsUpload")
	@ApiOperation(value = "js文件上传", notes = "js文件上传")
	@OperateLog(type=OperaterType.UPDATE,desc="js文件上传")
	@RequestMapping(value = "/jarupload", method = RequestMethod.POST)
	public JsonResult<Object> jarFileUpload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req, @RequestParam int protocolId) {
		long len = file.getSize();//上传文件的大小, 单位为字节
		double fileSize = (double)len/(1024*1024);//转换为M
		
		String fileName = file.getOriginalFilename();
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		if (!"js".equals(fieldType)) {
			return execCheckError("文件上传失败！文件类型错误");
		}
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}if(fileSize>16) {
			return execCheckError("文件上传失败！文件过大");
		}
		byte[] b =null;
		try {
			InputStream jar = file.getInputStream();
			b = IOUtils.toByteArray(jar);
		} catch (IOException e) {
			log.info("js文件上传异常！IOException");
			e.printStackTrace();
			return execError("js文件上传异常！", e.getMessage());
		}
		Protocol entity=new Protocol();
		entity.setId(protocolId);
		entity.setPatch(b);
		if(protocolService.updateById(entity)) {
			//sys_notice
	        noticeService.insertNoticeByParam(2403,"js文件上传",1,0);
			return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
		}
		return execCheckError(Constant.UPLOAD_ERROR_MSG);
	}
	
	/**
	 * 上传日志协议
	 * @param file
	 * @param req
	 * @return
	 * @throws Exception 
	 */
	@RequirePermission("modelUpload")
	@ApiOperation(value = "上传日志协议", notes = "上传日志协议")
	@OperateLog(type=OperaterType.INSERT,desc="上传日志协议")
	@RequestMapping(value = "/logs/add", method = RequestMethod.POST)
	public JsonResult<Object> addLogs(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req) throws Exception {
		File jsonfile = null;
		String fileName = file.getOriginalFilename();
		long len = file.getSize();//上传文件的大小, 单位为字节
		double fileSize = (double) len / 1024;//转换为K
		
		if (file.isEmpty() || fileName.lastIndexOf(".json") == -1) {
			return execCheckError("文件上传失败！请确认文件类型是否正确");
		}if(fileSize>65) {
			return execCheckError("文件上传失败!文件过大");
		}
		else{
			InputStream ins = null;
			try {
				ins = file.getInputStream();
			} catch (IOException e) {
				log.info("json文件上传异常！IOException");
				e.printStackTrace();
				return execError("json文件上传异常！", e.getMessage());
			}
			String savedDir = req.getSession().getServletContext().getRealPath("upload");
			jsonfile = new File(savedDir, file.getOriginalFilename());
			if (!jsonfile.getParentFile().exists()) {
				jsonfile.getParentFile().mkdirs();
			}
		    inputStreamToFile(ins, jsonfile);
		}

		StringBuilder sb = new StringBuilder();// 定义一个字符串缓存，将字符串存放缓存中
		try {
			FileReader reader = new FileReader(jsonfile);// 定义一个fileReader对象，用来初始化BufferedReader
			BufferedReader bReader = new BufferedReader(reader);// new一个BufferedReader对象，将文件内容读取到缓存
			String s = "";
			while ((s = bReader.readLine()) != null) {// 逐行读取文件内容，不读取换行符和末尾的空格
				sb.append(s + "\n");// 将读取的字符串添加换行符后累加存放在缓存中
			}
			bReader.close();
			//新建一个file，然后获取之前的地址使用file自带的delete方法
			File del = new File(jsonfile.toURI());
	   		del.delete();
		} catch (IOException e) {
			log.info("json文件上传异常！IOException");
			e.printStackTrace();
			return execError("json文件上传异常！", e.getMessage());
		}
		String name = getSessionUser(req).getName();
		String str =new String(sb.toString().getBytes(), "UTF-8") ;
		JSONObject jb = null;
		try {
			jb = JSON.parseObject(str);
		} catch (Exception e) {
			return execCheckError("上传文件json格式有误！");
		}
		String pname=jb.getString("desc");
		String key=jb.getString("key");
		if (protocolService.checkProtocolName(0, pname, 1)) {
			if (protocolService.checkProtocolKey(0, key, 1)) {
				boolean flag = protocolService.addLogsJson(name, str);
				if (flag) {
					return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
				}else {
					return execCheckError(Constant.UPLOAD_ERROR_MSG);
				}
			} else {
				return execCheckError("协议Key重复！");
			}
		}else {
			return execCheckError("协议名重复！");
		}
	}
	
	/**
	 * @param req
	 * @param resp
	 * @param protocolId
	 * @return
	 */
	@ApiOperation(value = "jar下载", notes = "jar下载")
	@RequestMapping(value = "/jardownload/{protocolId}", method = RequestMethod.GET)
	@Deprecated
	public JsonResult<Object> download1(HttpServletRequest req, HttpServletResponse resp,
			@ApiParam(required = true, name = "protocolId", value = "协议id") @PathVariable int protocolId){
		// 查出需要下载的真实数据
		Protocol p=protocolService.selectById(protocolId);
		// 将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=content.js");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			os.write(p.getPatch());
			os.close();
		} catch (IOException e) {
			log.info("js文件下载异常！IOException");
			e.printStackTrace();
			return execError("js文件下载异常！", e.getMessage());
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}
	
	
}
