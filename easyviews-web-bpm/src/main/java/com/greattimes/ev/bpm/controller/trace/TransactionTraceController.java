package com.greattimes.ev.bpm.controller.trace;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.spring.PropertyPreFilters;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.resp.TranslateDataParam;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.Protocol;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.bpm.trace.param.req.HttpMessageParam;
import com.greattimes.ev.bpm.trace.param.req.IntelligentTraceParam;
import com.greattimes.ev.bpm.trace.param.req.SingleTraceParam;
import com.greattimes.ev.bpm.trace.param.req.TraceParam;
import com.greattimes.ev.bpm.trace.param.resp.IntelligentComponentResParam;
import com.greattimes.ev.bpm.trace.param.resp.TraceListResParam;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.HexUtils;
import com.greattimes.ev.common.utils.ProtocolUtil;
import com.greattimes.ev.common.utils.ReportUtil;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.OriginalParam;
import com.greattimes.ev.indicator.service.IOriginalService;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.utils.HttpClientUtil;

import cn.com.greattimes.npds.psa.analysis.MessageGroupAnalyst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author NJ
 * @date 2018/10/24 14:20
 */
@RequirePermission("tradeTrack")
@RestController
@RequestMapping(value = "/trace")
@Api(value = "TransactionTraceController", tags = "交易追踪")
public class TransactionTraceController extends BaseController {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ICommonService commonService;
	@Autowired
	private IComponentDimensionService componentDimensionService;
	@Autowired
	private ICustomService customService;
	@Autowired
	private ConfigurationCache configurationCache;
	@Autowired
	private IComponentService componentService;
	@Autowired
	private IProtocolService protocolService;
	@Autowired
	private IOriginalService originalService;

	/**
	 * 测试接口
	 *
	 * String url = "http://192.168.0.158:7080/api/tradechase/query";
	 *
	 * String url = "http://192.168.0.158:7080/api/tradechase/refresh";
	 *
	 * String url = "http://192.168.0.158:7080/api/tradechase/order";
	 *
	 * @param param
	 * @return
	 */

	@ApiOperation(value = "交易追踪初始化", notes = "交易追踪初始化")
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	public JsonResult<Object> query(@RequestBody TraceParam param) {
		logger.info("交易追踪初始化请求参数:{}", param.toString());
		StopWatch sw = new StopWatch();
		sw.start();

		// post请求
//		String url = "http://192.168.0.231:7080/api/tradechase/query";

		String url = "http://" + componentService.getCenterIpBycomponentId(param.getCompontentId()) +":"
				+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT) + configurationCache.getValue(Constant.TRANSACTION_TRACE_INIT);

		String ret = null;
		try {
			logger.info("交易追踪初始化URL:{}",url);
			String jsonStr = JSON.toJSONString(param, SerializerFeature.WriteMapNullValue);
			logger.info("交易追踪初始化发送参数:{}", jsonStr);
			ret = HttpClientUtil.posts(url, jsonStr);
		} catch (Exception e) {
			logger.info("交易追踪初始化发生异常：" + e.getMessage());
			sw.stop();
			logger.info("》》》》当前线程：【{}】,交易追踪初始化发生异常：{}ms",Thread.currentThread().getId(), sw.getLastTaskTimeMillis());
			sw.start();
			throw new RuntimeException("交易追踪初始化发生异常!"+e.getMessage());
		}

		logger.debug("交易追踪列表查询接口原始返回值:{}", ret);
		JSONObject jb = JSON.parseObject(ret);
		Integer code = jb.getInteger("code");
		logger.debug("交易追踪列表查询接口返回值:{}", jb.toJSONString());
		/**
		 * 错误代码，0-成功 其他-错误（具体参见错误码定义）
		 */
		if (code != null && code.equals(0)) {
			TraceListResParam traceListResParam = JSON.parseObject(jb.getString("data"), TraceListResParam.class);
			List<Map<String, Object>> tmList = traceListResParam.getDetail();
			logger.debug("查询到的交易列表数据：{}", JSON.toJSONString(tmList));
			if (evUtil.listIsNullOrZero(tmList)) {
				return execSuccess(traceListResParam);
			}
			// 翻译值查询
			Set<TranslateDataParam> translate = new HashSet<>();
			List<Map<String, Object>> dimensions = new ArrayList<>();
			Long uuid = param.getUuid();
			if (param.getType().intValue() == 1) {
				translate.addAll(commonService.getComponentTranslate(uuid));
				// dimension
				dimensions = componentDimensionService.selectDimensionNameBycomponentId(uuid.intValue());
			}
			if (param.getType().intValue() == 2) {
				translate.addAll(commonService.getCustomTranslate(uuid));
				//分析维度翻译
				Component c = commonService.selectComponentByCustomId(uuid.intValue());
				if(null!=c) {
					translate.addAll(commonService.getComponentTranslate(Long.parseLong(c.getId().toString())));
				}
				// dimension(查询统计和分析维度)
				dimensions = customService.selectDimensionByCustomIdAndType(uuid.intValue(), -1);
			}
			logger.debug("业务对应的翻译值列表:{}", translate);
			boolean flag;
			String dimensionStr;
			for (Map<String, Object> map : tmList) {
				for (Map<String, Object> dimensionMap : dimensions) {
					flag = false;
					dimensionStr = evUtil.getMapStrValue(dimensionMap, "ename");
					for (TranslateDataParam map2 : translate) {
						if (dimensionStr.equals(map2.getEname()) && map2.getValue().equals(map.get(dimensionStr))) {
							map.put(dimensionStr + "Translate", map2.getMapping());
							flag = true;
							break;
						}
					}
					if (!flag) {
						map.put(dimensionStr + "Translate", null);
					}
				}
			}
			sw.stop();
			logger.info("交易追踪列表查询接口，花费时间：{}ms", sw.getTotalTimeMillis());
			return execSuccess(traceListResParam);
		} else {
			String exception = jb.getString("exception");
			String msg = jb.getString("msg");
			logger.info("交易追踪列表查询接口调用失败！错误返回码：{} 错误信息：{} 异常信息", code, msg, exception);
			return execError("交易追踪列表查询接口调用失败！" + "错误返回码:" + code + " 错误信息:" + msg, " 异常信息:" + exception);
		}
	}

	@ApiOperation(value = "csv下载", notes = "csv下载")
	@RequestMapping(value = "csv/download", method = RequestMethod.POST)
	public void downLoadCsv(HttpServletResponse resp, @RequestBody TraceParam param) {
		logger.info("csv下载请求参数:{}", param.toString());
		List<Map<String, String>> headers = new ArrayList<>();
		List<JSONObject> headersParam=param.getHeaders();
		if(!evUtil.listIsNullOrZero(headersParam)){
			headersParam.stream().forEach(X->{
				Map<String, String> map = new HashMap<>();
				map.put(X.getString("name"), X.getString("value"));
				headers.add(map);
			});
		}
		if(evUtil.listIsNullOrZero(headers)) {
			throw new RuntimeException("未选择数据列！");
		}
		String url = "http://" + componentService.getCenterIpBycomponentId(param.getCompontentId()) +":"
				+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT) + configurationCache.getValue(Constant.TRANSACTION_TRACE_INIT);
		String ret = null;
		try {
			logger.info("csv下载请求数据URL:{}",url);
			String jsonStr = JSON.toJSONString(param, SerializerFeature.WriteMapNullValue);
			logger.info("csv下载请求数据发送参数:{}", jsonStr);
			ret = HttpClientUtil.posts(url, jsonStr);
		} catch (Exception e) {
			logger.info("csv下载请求数据发生异常：" + e.getMessage());
			throw new RuntimeException("csv下载请求数据异常!"+e.getMessage());
		}

		//logger.info("csv下载请求数据原始返回值:{}", ret);
		JSONObject jb = JSON.parseObject(ret);
		Integer code = jb.getInteger("code");
		//logger.info("csv下载请求数据接口返回值:{}", jb.toJSONString());
		if (code != null && code.equals(0)) {
			TraceListResParam traceListResParam = JSON.parseObject(jb.getString("data"), TraceListResParam.class);
			List<Map<String, Object>> tmList = traceListResParam.getDetail();
			//logger.info("查询到的交易列表数据：{}", JSON.toJSONString(tmList));
			if (!evUtil.listIsNullOrZero(tmList)) {
				// 翻译值查询
				Set<TranslateDataParam> translate = new HashSet<>();
				List<Map<String, Object>> dimensions = new ArrayList<>();
				Long uuid = param.getUuid();
				if (param.getType().intValue() == 1) {
					translate.addAll(commonService.getComponentTranslate(uuid));
					// dimension
					dimensions = componentDimensionService.selectDimensionNameBycomponentId(uuid.intValue());
				}
				if (param.getType().intValue() == 2) {
					translate.addAll(commonService.getCustomTranslate(uuid));
					//分析维度翻译
					Component c = commonService.selectComponentByCustomId(uuid.intValue());
					if(null!=c) {
						translate.addAll(commonService.getComponentTranslate(Long.parseLong(c.getId().toString())));
					}
					// dimension(查询统计和分析维度)
					dimensions = customService.selectDimensionByCustomIdAndType(uuid.intValue(), -1);
				}
				//logger.info("业务对应的翻译值列表:{}", translate);
				boolean flag;
				String dimensionStr;
				for (Map<String, Object> map : tmList) {
					for (Map<String, Object> dimensionMap : dimensions) {
						flag = false;
						dimensionStr = evUtil.getMapStrValue(dimensionMap, "ename");
						for (TranslateDataParam map2 : translate) {
							if (dimensionStr.equals(map2.getEname()) && map2.getValue().equals(map.get(dimensionStr))) {
								map.put(dimensionStr + "Translate", map2.getMapping());
								flag = true;
								break;
							}
						}
						if (!flag) {
							map.put(dimensionStr + "Translate", null);
						}
					}
				}
			}
			ReportUtil cw = null;
			try {
				cw = new ReportUtil(resp.getOutputStream());
				ReportUtil.downLoadCsv(tmList,headers,"trance.csv",cw,resp);
				cw.flush();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (cw != null) {
						cw.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			String exception = jb.getString("exception");
			String msg = jb.getString("msg");
			logger.info("交易追踪列表查询接口调用失败！错误返回码：{} 错误信息：{} 异常信息", code, msg, exception);
			throw new RuntimeException("交易追踪列表查询接口调用失败！" + "错误返回码:" + code + " 错误信息:" + msg);
		}
	}
	
	


	@ApiOperation(value = "交易追踪初刷新停止", notes = "交易追踪初刷新停止")
	@RequestMapping(value = "/refresh", method = RequestMethod.POST)
	public JsonResult<Object> refresh(@RequestBody JSONObject param) {
		logger.info("交易追踪初刷新停止请求参数:{}", param.toString());
		StopWatch sw = new StopWatch();
		sw.start();
		String sessionId = param.getString("sessionId");
		int action = param.getIntValue("action");
		Map<String, Object> map = new HashMap<>();
		map.put("sessionId", sessionId);
		map.put("action", action);

		// post请求
//		String url = "http://192.168.0.231:7080/api/tradechase/refresh";

		String url = "http://" + componentService.getCenterIpBycomponentId(param.getInteger("componentId"))
				+":"+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT)
				+ configurationCache.getValue(Constant.TRANSACTION_TRACE_REFRESH);


		String ret = null;
		try {
			logger.info("交易追踪初刷新停止URL:{}",url);
			ret = HttpClientUtil.posts(url, JSON.toJSONString(map, SerializerFeature.WriteMapNullValue));
		} catch (Exception e) {
			logger.info("交易追踪初刷新停止发生异常：" + e.getMessage());
			throw new RuntimeException("交易追踪初刷新停止发生异常："+e.getMessage());
		}
		logger.info("交易追踪初刷新停止返回原始结果:{}",ret);
		JSONObject jb = JSON.parseObject(ret);
		logger.info("交易追踪初刷新停止返回结果:{}",jb.toJSONString());
		Integer code = jb.getInteger("code");
		if (code != null && code.equals(0)) {
			sw.stop();
			JSONObject dataJSON = jb.getJSONObject("data");
			Map<String, Object> result = new HashMap<>();
			result.put("total", dataJSON.getInteger("total"));
			result.put("state", dataJSON.getInteger("state"));
			logger.info("交易追踪刷新停止，花费时间：{}ms", sw.getTotalTimeMillis());
			return execSuccess(result);
		} else {
			String exception = jb.getString("exception");
			String msg = jb.getString("msg");
			logger.info("交易追踪刷新停止接口调用失败！错误返回码：{} 错误信息：{} 异常信息", code, msg, exception);
			return execError("交易追踪列表查询接口调用失败！" + "错误返回码:" + code + " 错误信息:" + msg, " 异常信息:" + exception);
		}
	}

	@Deprecated
	@ApiOperation(value = "交易追踪分页排序", notes = "交易追踪分页排序")
	@RequestMapping(value = "/page/order", method = RequestMethod.POST)
	public JsonResult<Object> pageOrder(@RequestBody TraceParam param) {
		logger.info("交易追踪分页排序请求参数:{}", param.toString());
		/**
		 * sessionId String 是 回话ID pageSize Int 是 每页条数 默认10 pageNum int 是 查询页数 默认1 order
		 * Object 否 排序 rule int 是 排序规则1 升序 2降序 dimension string 是 排序字段
		 */
		StopWatch sw = new StopWatch();
		sw.start();

		// post请求
//		String url = "http://192.168.0.159:7080/api/tradechase/order";


//		String url = "http://192.168.0.163:7080/api/tradechase/order";

		String url = "http://" + componentService.getCenterIpBycomponentId(param.getCompontentId()) +
				":"+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT)
				+ configurationCache.getValue(Constant.TRANSACTION_TRACE_ORDER);

//		String url = configurationCache.getValue(Constant.TRANSACTION_TRACE_HOST) + configurationCache.getValue(Constant.TRANSACTION_TRACE_ORDER);

		logger.info("交易追踪分页排序URL:{}",url);

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("sessionId", param.getSessionId());
		paramMap.put("pageSize", param.getPageSize());
		paramMap.put("pageNum", param.getPageNum());

		List<JSONObject> orders = param.getOrder();
		List<Map<String, Object>> orderList = new ArrayList<>();
		if(!evUtil.listIsNullOrZero(orders)){
			orders.forEach(x -> {
				Map<String, Object> map = new HashMap<>();
				map.put("rule", x.getInteger("rule"));
				map.put("dimension", x.getString("dimension"));
				orderList.add(map);
			});
			paramMap.put("order", orderList);
		}
		logger.info("交易追踪分页排序参数:{}", JSON.toJSONString(paramMap));
		String ret = null;
		try {
			ret = HttpClientUtil.posts(url, JSON.toJSONString(paramMap, SerializerFeature.WriteMapNullValue));
		} catch (Exception e) {
			logger.info("交易追踪分页排序发生异常：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}

		logger.debug("交易追踪分页排序原始返回值:{}", ret);
		JSONObject jb = JSON.parseObject(ret);
		logger.debug("交易追踪分页排序返回值:{}", jb.toJSONString());
		Integer code = jb.getInteger("code");
		if (code != null && code.equals(0)) {
			TraceListResParam traceListResParam = JSON.parseObject(jb.getString("data"), TraceListResParam.class);
			 List<Map<String,Object>> tmList = traceListResParam.getDetail();
			 if(evUtil.listIsNullOrZero(tmList)){
			 return execSuccess(new TraceListResParam());
			 }
			 //翻译值查询
			 List<TranslateDataParam> translate = new ArrayList<>();
			 List<Map<String, Object>> dimensions = new ArrayList<>();
			 Long uuid = param.getUuid();
			 if (param.getType().intValue() == 1) {
				 translate = commonService.getComponentTranslate(uuid);
				 //dimension
				 dimensions = componentDimensionService.selectDimensionNameBycomponentId(uuid.intValue());
			 }
			 if (param.getType().intValue() == 2) {
				 translate=commonService.getCustomTranslate(uuid);
				 //dimension(查询统计和分析维度)
				 dimensions = customService.selectDimensionByCustomIdAndType(uuid.intValue(),-1);
			 }
			logger.debug("业务对应的翻译值列表:{}",translate);
			 boolean flag;
			 String dimensionStr;
			 for (Map<String, Object> map : tmList) {
				 for(Map<String, Object> dimesionMap : dimensions){
					 dimensionStr = evUtil.getMapStrValue(dimesionMap, "ename");
					 flag = false;
					 for (TranslateDataParam map2 : translate) {
						 if (dimensionStr.equals(map2.getEname()) && map2.getValue().equals(map.get(dimensionStr))) {
							 map.put(dimensionStr+"Translate", map2.getMapping());
							 flag=true;
							 break;
						}
			 		}
			 		if(!flag) {
						map.put(dimensionStr+"Translate", null);
					 }
			 	}
			 }

			 sw.stop();
			 logger.info("交易追踪分页排序查询接口，花费时间：{}ms",sw.getTotalTimeMillis());
			return execSuccess(traceListResParam);
		} else {
			String exception = jb.getString("exception");
			String msg = jb.getString("msg");
			logger.info("交易追踪分页排序查询接口调用失败！错误返回码：{} 错误信息：{} 异常信息", code, msg, exception);
			return execError("交易追踪分页排序查询接口调用失败！" + "错误返回码:" + code + " 错误信息:" + msg, " 异常信息:" + exception);
		}
	}

	@Deprecated
	@ApiOperation(value = "单笔交易追踪", notes = "单笔交易追踪")
	@RequestMapping(value = "/single/query", method = RequestMethod.POST)
	public JsonResult<Object> singleQuery(@RequestBody SingleTraceParam param) {

		logger.info("单笔交易追踪请求参数:{}", param.toString());

//		String url = "http://192.168.0.26:7080/api/transactionChainTracking";

		String url = "http://" + componentService.getCenterIpBycomponentId(param.getComponentId())+":"+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT) + configurationCache.getValue(Constant.TRANSACTION_TRACE_SINGLE);

//		String url = configurationCache.getValue(Constant.TRANSACTION_TRACE_HOST) + configurationCache.getValue(Constant.TRANSACTION_TRACE_SINGLE);

		//查询某一个业务下所有组件的前后追踪字段置入参数对象中



//		EntityWrapper<Component> componentEntityWrapper = new EntityWrapper<>();
//		componentEntityWrapper.where("applicationId={}", param.getApplicationId());
//		List<Component> components = componentService.selectList(componentEntityWrapper);
//		List<JSONObject> traceChain = new ArrayList<>();
//		if(!evUtil.listIsNullOrZero(components)){
//			for(Component component : components){
//				if(component.getPa
// tternFieldUp() == null && component.getPatternFieldDown() == null){
//					continue;
//				}else{
//					JSONObject jsonObject = new JSONObject();
//					jsonObject.put("componentId", component.getId());
//					jsonObject.put("leftField", component.getPatternFieldUp() == null ? "" : component.getPatternFieldUp());
//					jsonObject.put("rightField", component.getPatternFieldDown() == null ? "" : component.getPatternFieldDown());
//					traceChain.add(jsonObject);
//				}
//			}
//		}
//
//		param.setTraceChain(traceChain);
		String ret = null;
		try {
			logger.info("单笔交易追踪URL:{}",url);
			ret = HttpClientUtil.posts(url, JSON.toJSONString(param, SerializerFeature.WriteMapNullValue));
		} catch (Exception e) {
			logger.info("单笔交易追踪发生异常：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		logger.debug("单笔交易追踪原始返回值:{}", ret);
		JSONObject jb = JSON.parseObject(ret);
		Integer code = jb.getInteger("code");
		if (code != null && code.equals(0)) {
			List<Map<String, Object>> listData = JSON.parseObject(jb.getString("data"), List.class);
			List<Integer> componentIds = new ArrayList<>();
			if(!evUtil.listIsNullOrZero(listData)){
				for(Map<String, Object> map : listData){
					componentIds.add(evUtil.getMapIntValue(map, "componentId"));
				}
			}
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("ids", componentIds);
			Map<Integer, Map<String, Object>> nameMap = componentService.getComponentNameByMap(paramMap);
//			Map<Integer, List<Map<String, Object>>> dimensionsMap = componentDimensionService.selectDimensionNameByIds(componentIds);
			Map<Integer, Map<String, Map<String, Object>>> dimensionsMap = componentDimensionService.selectDimensionNameMapByIds(componentIds);
			Integer componentId;
			Map<String, Object> dMap;
			boolean flag;
			for(Map<String, Object> map : listData){
				componentId = evUtil.getMapIntegerValue(map,"componentId");
				dMap = nameMap.get(componentId);
				map.put("componentName", dMap.get("componentName"));
				map.put("applicationName", dMap.get("applicationName"));
				map.put("monitorName", dMap.get("monitorName"));
				Map<String, Object> extendFields = (Map<String, Object>)map.get("extendFields");
				Map<String, Object> dimensionResult = new HashMap<>();
				flag = false;
				for(Map.Entry<String, Object> entry : extendFields.entrySet()){
					Map<String, Map<String, Object>> componentMap = dimensionsMap.get(componentId);
					Map<String, Object> dataMap;
					if(componentMap != null ){
						dataMap = componentMap.get(entry.getKey());
						if(dataMap != null){
							dimensionResult.put(dataMap.get("trueName").toString(), entry.getValue());
							flag = true;
						}
					}
					if(!flag){
						dimensionResult.put(entry.getKey(),entry.getValue());
					}
				}
				map.put("extendFields", dimensionResult);
				map.put("location", "");
			}
			return execSuccess(listData);
		} else {
			String exception = jb.getString("exception");
			String msg = jb.getString("msg");
			logger.info("单笔交易追踪查询接口调用失败！错误返回码：{} 错误信息：{} 异常信息", code, msg, exception);
			return execError("单笔交易追踪查询接口调用失败！" + "错误返回码:" + code + " 错误信息:" + msg, " 异常信息:" + exception);
		}
	}

	@Deprecated
	@ApiOperation(value = "追踪链条查询", notes = "追踪链条查询")
	@RequestMapping(value = "/chain/query", method = RequestMethod.POST)
	public JsonResult<Object> originQuery(@RequestBody JSONObject param) {
		logger.info("追踪链条查询请求参数:{}", param.toString());
		return execSuccess(componentService.getComponentChainParamByAppId(param.getInteger("applicationId")));
	}

	@RequirePermission("traceRequestAndResponse")
	@ApiOperation(value = "原始报文追踪", notes = "原始报文追踪")
	@RequestMapping(value = "/origin/query", method = RequestMethod.POST)
	public JsonResult<Object> originQuery(@RequestBody HttpMessageParam param) throws Exception {
		MessageGroupAnalyst mga;
		String charset=param.getCharset();
		//获取中心id
		Component component=componentService.selectById(param.getComponentId());
		param.setCenterId(component.getCenterId().toString());
		String msg = null;
		Map<String, Object> map = new HashMap<>();
		logger.info("原始报文追踪请求参数:{}", param.toString());
		String url = configurationCache.getValue(ConfigConstants.DECODE_HOST)
				+ configurationCache.getValue(ConfigConstants.MESSAGE_DOWNLOAD_URL);
		String result = null;
		try {
			logger.info("原始报文追踪url:{}", url);
			result = HttpClientUtil.posts(url, JSON.toJSONString(param));
		} catch (Exception e) {
			logger.info("原始报文追踪--调用远程接口发生异常：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		if (!StringUtils.isBlank(result)) {
			logger.info("原始报文追踪响应参数:{}", result.toString());
			JSONObject jb = null;
			try {
				jb = JSON.parseObject(result);
			} catch (Exception e) {
				logger.info("原始报文追踪--返回结果转换异常：" + e.getMessage());
				throw new RuntimeException(e.getMessage());
			}
			String returnCode = jb.getString("responseCode");
			String returnMessage = jb.getString("responseDesc");
			if (!"200".equals(returnCode)) {
				return execCheckError(returnMessage);
			} else {
				String reqMsg=null;
				String resMsg=null;
				Protocol p=protocolService.selectById(component.getPrototcolId());
				try {
					mga = new MessageGroupAnalyst(ProtocolUtil.getProtocolCodeByProtocolType(p.getType()),ProtocolUtil.getMessageTypeByMessageFormat(p.getMessageFormat()) , null);
					reqMsg=mga.getMessage(0, jb.getString("reqMsg"));
					if (null != jb.getString("resMsg")) {
						resMsg=mga.getMessage(1, jb.getString("resMsg"));
					}
				} catch (Exception e) {
					logger.info("解析异常:" + e.getMessage());
					e.printStackTrace();
					return execError("解析异常:" + e.getMessage(), e.getMessage());
				}
				if(StringUtils.isBlank(charset)) {
					charset=jb.getString("charset");
				}
				byte[] bytes = HexUtils.toBytes(reqMsg);
				String msgStr = new String(bytes, charset);
				map.put("reqMsg", msgStr);
				map.put("clientSendBytes", jb.getString("reqMsg"));
				if (null != resMsg) {
					byte[] bytes2 = HexUtils.toBytes(resMsg);
					String msgStr2 = new String(bytes2, charset);
					map.put("resMsg", msgStr2);
				} else {
					map.put("resMsg", null);
				}
				map.put("serverSendBytes", jb.getString("resMsg"));
				map.put("charset", jb.getString("charset"));
				msg = jb.getString("responseDesc");
				return execSuccess(map, msg);
			}
		} else {
			msg = "调用远程接口无返回结果！";
			logger.error("调用" + url + "无返回结果！");
			return execCheckError(msg);
		}
	}

	@RequirePermission("traceRequestAndResponse")
	@ApiOperation(value = "新原始报文追踪", notes = "新原始报文追踪")
	@RequestMapping(value = "/origin/select", method = RequestMethod.POST)
	public JsonResult<Object> originSelect(@RequestBody OriginalParam param) throws Exception {
		MessageGroupAnalyst mga;
		String charset=param.getCharset();
		Integer type=param.getType();
		Integer asyn=param.getAsyn()==null?0:param.getAsyn();
		String msg = null;
		Map<String, Object> map = new HashMap<>();
		Component component=componentService.selectById(param.getComponentId());
		//若是异步 查询时不用组件条件
		if(asyn.equals(1)) {
			param.setComponentId(null);
		}
		String result = originalService.originSelect(param);
		if (!StringUtils.isBlank(result)) {
			String reqMsg = null;
			String resMsg = null;
			Protocol p = protocolService.selectById(component.getPrototcolId());
			if (StringUtils.isBlank(charset)) {
				charset = "UTF-8";
			}
			//日志协议直接返回
			if(p.getType().equals("日志")) {
				if (type.equals(1)) {
					map.put("reqMsg", null);
					map.put("clientSendBytes", null);
					map.put("resMsg", result);
					map.put("serverSendBytes", result);
				} else {
					map.put("reqMsg", result);
					map.put("clientSendBytes", result);
					map.put("resMsg", null);
					map.put("serverSendBytes", null);
				}
				map.put("charset", charset);
				return execSuccess(map);
			
			}
			// http xml协议处理
			else if (p.getType().equals("HTTP") && p.getMessageFormat().equals("XML")) {
				try {
					mga = new MessageGroupAnalyst(ProtocolUtil.getProtocolCodeByProtocolType(p.getType()),
							ProtocolUtil.getMessageTypeByMessageFormat(p.getMessageFormat()), null);
					if (type.equals(1)) {
						resMsg = mga.getMessage(1, result);
						byte[] bytes = HexUtils.toBytes(resMsg);
						String msgStr = new String(bytes, charset);
						map.put("reqMsg", null);
						map.put("clientSendBytes", null);
						map.put("resMsg", msgStr);
						map.put("serverSendBytes", result);
					} else {
						reqMsg = mga.getMessage(0, result);
						byte[] bytes = HexUtils.toBytes(reqMsg);
						String msgStr = new String(bytes, charset);
						map.put("reqMsg", msgStr);
						map.put("clientSendBytes", result);
						map.put("resMsg", null);
						map.put("serverSendBytes", null);
					}
					map.put("charset", charset);
				} catch (Exception e) {
					logger.info("解析异常:" + e.getMessage());
					e.printStackTrace();
					return execError("解析异常:" + e.getMessage(), e.getMessage());
				}
				return execSuccess(map);
			} else {
				if (type.equals(1)) {
					byte[] bytes = HexUtils.toBytes(result);
					String msgStr = new String(bytes, charset);
					map.put("reqMsg", null);
					map.put("clientSendBytes", null);
					map.put("resMsg", msgStr);
					map.put("serverSendBytes", result);
				} else {
					byte[] bytes = HexUtils.toBytes(result);
					String msgStr = new String(bytes, charset);
					map.put("reqMsg", msgStr);
					map.put("clientSendBytes", result);
					map.put("resMsg", null);
					map.put("serverSendBytes", null);
				}
				map.put("charset", charset);
				return execSuccess(map);
			}
		}else {
			msg = "未获取到报文！";
			return execCheckError(msg);
		}
	}

	@RequirePermission("pcapdown")
	@ApiOperation(value = "pcap下载", notes = "pcap下载")
	@RequestMapping(value = "/pcap/download", method = RequestMethod.POST)
	public JsonResult<Object> pcapDownload(HttpServletResponse resp, @RequestBody HttpMessageParam param) {
		//获取中心id
		Component component=componentService.selectById(param.getComponentId());
		param.setCenterId(component.getCenterId().toString());
		String msg = null;
		Map<String, Object> map = new HashMap<>();
		logger.info("pcap下载请求参数:{}", param.toString());
		String url = configurationCache.getValue(ConfigConstants.DECODE_HOST)
				+ configurationCache.getValue(ConfigConstants.SESSION_DOWNLOAD_URL);
		Map<String, Object> result = new HashMap<>();
		try {
			logger.info("pcap下载url:{}", url);
			result = HttpClientUtil.getPostStream(url, JSON.toJSONString(param));
		} catch (Exception e) {
			logger.error("pcap下载--调用远程接口发生异常：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		if(null!=result) {
			logger.info("pcap下载响应参数:{}", result.toString());
		}else {
			logger.info("pcap下载响应参数为空！");
		}
		if (!result.isEmpty()) {
			String statusCode = evUtil.getMapStrValue(result, "statusCode");
			InputStream body = (InputStream) result.get("body");
			if ("200".equals(statusCode)) {
				// 将文件存到指定位置
				resp.setCharacterEncoding("utf-8");
				resp.setContentType("multipart/form-data");
				resp.setHeader("Content-Disposition", "attachment;fileName=task["+param.getTaskId()+"].cap");
				OutputStream os = null;
				try {
					byte[] data=input2byte(body);
					os = resp.getOutputStream();
					os.write(data);
					os.close();
					body.close();
				} catch (IOException e) {
					logger.info("文件下载异常！IOException");
					e.printStackTrace();
					return execError("文件下载异常！", e.getMessage());
				}
				return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
			} else if ("550".equals(statusCode)) {
				return execCheckError("没有服务探针");
			} else if ("551".equals(statusCode)) {
				return execCheckError("参数不正确");
			} else if ("552".equals(statusCode)) {
				return execCheckError("轮询探针超时");
			} else if ("553".equals(statusCode)) {
				return execCheckError("轮询探针失败");
			} else if ("554".equals(statusCode)) {
				return execCheckError("下载数据超时");
			} else if ("555".equals(statusCode)) {
				return execCheckError("下载数据失败");
			} else if ("556".equals(statusCode)) {
				return execCheckError("无数据");
			} else if ("557".equals(statusCode)) {
				return execCheckError("参数错误");
			} else if ("558".equals(statusCode)) {
				return execCheckError("等待后台程序处理");
			} else if ("559".equals(statusCode)) {
				return execCheckError("数据包被清理无法进行交易追踪");
			} else {
				return execCheckError("未知错误");
			}
		} else {
			msg = "调用远程接口无返回结果！";
			logger.error("调用" + url + "无返回结果！");
			return execCheckError(msg);
		}
	}

	@ApiOperation(value = "智能交易追踪", notes = "智能交易追踪")
	@RequestMapping(value = "/intelligent/query", method = RequestMethod.POST)
	public JsonResult<Object> intelligentQuery( @RequestBody IntelligentTraceParam param) {
		logger.info("智能交易追踪请求参数:{}", JSON.toJSONString(param));
		StopWatch sw = new StopWatch();
		sw.start();

//		String url = "http://192.168.0.231:7080/api/intelligent/query";

		String url = "http://" + componentService.getCenterIpBycomponentId(param.getComponentId()) +":"
		+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT) + configurationCache.getValue(Constant.TRANSACTION_TRACE_INTELLIGENT);

		logger.info("智能交易追踪URL:{}", url);
		String ret = null;
		try {
			String[] excludeProperties = {"componentId"};
			PropertyPreFilters filters = new PropertyPreFilters();
			PropertyPreFilters.MySimplePropertyPreFilter excludefilter = filters.addFilter();
			excludefilter.addExcludes(excludeProperties);
			String jsonStr = JSON.toJSONString(param,excludefilter, SerializerFeature.WriteMapNullValue);
//			String jsonStr = JSON.toJSONString(param, SerializerFeature.WriteMapNullValue);
			logger.info("智能交易追踪调用远程接口参数：{}",jsonStr);
			ret = HttpClientUtil.posts(url, jsonStr);
		} catch (Exception e) {
			logger.info("智能交易追踪发生异常：" + e.getMessage());
			sw.stop();
			logger.info("》》》》当前线程：【{}】,智能交易追踪查询异常：{}ms",Thread.currentThread().getId(), sw.getLastTaskTimeMillis());
			sw.start();
			throw new RuntimeException(e.getMessage());
		}
		logger.debug("智能交易追踪原始返回值:{}", ret);
		JSONObject jb = JSON.parseObject(ret);
		Integer code = jb.getInteger("code");
		/**
		 * 错误代码，0-成功 其他-错误（具体参见错误码定义）
		 */
		List<IntelligentComponentResParam> intelligentResParams = new ArrayList<>();
		if (code != null && code.equals(0)) {
			intelligentResParams = JSON.parseArray(jb.getString("data"), IntelligentComponentResParam.class);
			if (!evUtil.listIsNullOrZero(intelligentResParams)) {
				Integer componentId;
				for (IntelligentComponentResParam intelligentResParam : intelligentResParams) {
					List<Map<String, Object>> tmList = intelligentResParam.getComponent().getData().getDetail();
					if (!evUtil.listIsNullOrZero(tmList)) {
						// 翻译值查询
						componentId = intelligentResParam.getComponent().getId();
						List<TranslateDataParam> translate = commonService.getComponentTranslate(componentId.longValue());
						List<Map<String, Object>> dimensions = componentDimensionService.selectDimensionNameBycomponentId(componentId);
						logger.debug("业务对应的翻译值列表:{}", translate);
						boolean flag;
						String dimensionStr;
						for (Map<String, Object> map : tmList) {
							for (Map<String, Object> dimensionMap : dimensions) {
								flag = false;
								dimensionStr = evUtil.getMapStrValue(dimensionMap, "ename");
								for (TranslateDataParam map2 : translate) {
									if (dimensionStr.equals(map2.getEname()) && map2.getValue().equals(map.get(dimensionStr))) {
										map.put(dimensionStr + "Translate", map2.getMapping());
										flag = true;
										break;
									}
								}
								if (!flag) {
									map.put(dimensionStr + "Translate", null);
								}
							}
						}
					}
				}
				sw.stop();
				logger.info("智能交易追踪查询接口，花费时间：{}ms", sw.getTotalTimeMillis());
				return execSuccess(intelligentResParams);
			}
		} else {
			String exception = jb.getString("exception");
			String msg = jb.getString("msg");
			logger.info("智能交易追踪查询接口调用失败！错误返回码：{} 错误信息：{} 异常信息", code, msg, exception);
			return execError("智能交易追踪查询接口调用失败！" + "错误返回码:" + code + " 错误信息:" + msg, " 异常信息:" + exception);
		}
		return execSuccess(intelligentResParams);
	}


	/**inputStream 轉byte[]
	 * @param inStream
	 * @return
	 * @throws IOException
	 */
	public static final byte[] input2byte(InputStream inStream) throws IOException {
		ByteArrayOutputStream swapStream = new ByteArrayOutputStream();
		byte[] buff = new byte[100];
		int rc = 0;
		while ((rc = inStream.read(buff, 0, 100)) > 0) {
			swapStream.write(buff, 0, rc);
		}
		byte[] in2b = swapStream.toByteArray();
		return in2b;
	}

}
