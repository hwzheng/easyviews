package com.greattimes.ev.bpm.controller.indicator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.common.annotation.MethodCost;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.TransactionExtendTableParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;
import com.greattimes.ev.indicator.service.ITransactionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author NJ
 * @date 2018/9/19 16:17
 */
@RestController
@Api(value = "CustomTransactionController", tags = "自定义业务指标查询")
@RequestMapping("/indicator/extend")
public class CustomTransactionController extends BaseController {
	@Autowired
	private ITransactionService transactionService;
	
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @ApiOperation(value = "自定义业务指标查询图表类", notes = "自定义业务指标查询图表类")
    @MethodCost(desc = "自定义业务指标查询图表类")
    @RequestMapping(value = "/chart", method = RequestMethod.POST)
    public JsonResult<Object> selectChart(@RequestBody IndicatorParam param) {
    	if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	//int interval=Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
    	IndicatorDataParam para= transactionService.extendChart(param);
        return execSuccess(para);
    }
    
    @ApiOperation(value = "事件聚合图表类", notes = "事件聚合图表类")
    @MethodCost(desc = "事件聚合图表类")
    @RequestMapping(value = "/combine/chart", method = RequestMethod.POST)
    public JsonResult<Object> selectCombineChart(@RequestBody IndicatorParam param) {
    	if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	IndicatorDataParam para= transactionService.extendCombineChart(param);
        return execSuccess(para);
    }
    
    @ApiOperation(value="表查询", notes="表查询",response = TransactionTableParam.class,responseContainer="list")
    @MethodCost(desc = "自定义业务指标查询表查询")
    @RequestMapping(value = "/table", method = RequestMethod.POST)
    public JsonResult<Object> table(HttpServletRequest request, @RequestBody IndicatorParam param) {
    	//parameter check
        if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	//int interval=Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
    	List<TransactionExtendTableParam> para= transactionService.extendTable(param);
        return execSuccess(para);
    }
    
    @ApiOperation(value="事件聚合指标接口", notes="事件聚合指标接口",response = TransactionTableParam.class,responseContainer="list")
    @MethodCost(desc = "事件聚合指标接口")
    @RequestMapping(value = "/combine/table", method = RequestMethod.POST)
    public JsonResult<Object> selectCombineTable(HttpServletRequest request, @RequestBody IndicatorParam param) {
    	//parameter check
        if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	TransactionExtendTableParam para= transactionService.extendCombineTable(param);
        return execSuccess(para);
    }

    @ApiOperation(value="事件指标接口", notes="事件指标接口",response = TransactionTableParam.class,responseContainer="list")
    @MethodCost(desc = "事件指标接口")
    @RequestMapping(value = "/instant", method = RequestMethod.POST)
    public JsonResult<Object> selectInstant(HttpServletRequest request, @RequestBody IndicatorParam param) {
    	//parameter check
        if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	List<TransactionExtendTableParam> para= transactionService.extendTableGroupByUuid(param);
        return execSuccess(para);
    }
}
