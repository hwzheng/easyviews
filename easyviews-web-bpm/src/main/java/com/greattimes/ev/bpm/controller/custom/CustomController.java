package com.greattimes.ev.bpm.controller.custom;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.AlarmParam;
import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.service.common.ISequenceService;
import com.greattimes.ev.bpm.service.config.IAlarmRuleService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.config.IProtocolFieldService;

import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.constants.ConfigConstants;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.custom.param.req.CustomDownloadParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomBasicParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomFieldParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomParam;
import com.greattimes.ev.bpm.service.config.IRegioninfoService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.ReportUtil;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.service.IBpmPreService;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;

import cn.com.greattimes.npds.common.util.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 自定义分析配置 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
@RestController
@Api(value = "CustomController", tags = "自定义配置")
@RequestMapping("/bpm/custom")
@RequirePermission("custom")
public class CustomController extends BaseController {

	@Autowired
	private ICustomService customService;
	@Autowired
	private IRegioninfoService regioninfoService;
	@Autowired
	private IAlarmRuleService alarmRuleService;
	@Autowired
	private ISequenceService sequenceService;
	@Autowired
	private IBpmPreService bpmPreService;
	@Autowired
	private IProtocolFieldService protocolFieldService;
	@Autowired
	private INoticeService noticeService;
	@Autowired
	private IComponentService componentService;
	@Autowired
	private ConfigurationCache configurationCache;

    private static final String TABLE_FIX_EXTEND = "extendPre_";
    private static final String VIEW_FIX_EXTEND = "v_extend_pre_";
    private static final String VIEW_VM_EXTEND = "vm_extend_pre_";
    Logger log = LoggerFactory.getLogger(this.getClass());
	/**
	 * 自定义配置列表查询
	 * 
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "自定义配置列表查询", notes = "自定义配置列表查询", response = CustomParam.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	@RequirePermission("common")
	public JsonResult<Object> select(HttpServletRequest req) {
		User user = super.getSessionUser(req);
		Integer userId = user.getId();
		return execSuccess(customService.selectCustom(userId));
	}

	/**
	 * 自定义配置排序
	 * 
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "自定义配置排序", notes = "自定义配置排序")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "ids", value = "自定义配置id和sort数组", allowMultiple = true, paramType = "query") })
	@OperateLog(type = OperaterType.UPDATE, desc = "自定义配置排序")
	@RequestMapping(value = "/sort", method = RequestMethod.POST)
	public JsonResult<Object> updateSort(@RequestBody JSONObject param) {
		JSONArray array = param.getJSONArray("ids");
		customService.updateSort(array.toJavaList(CustomParam.class));
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 自定义配置批量激活修改
	 * 
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "自定义配置批量激活修改", notes = "自定义配置批量激活修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "custom", value = "自定义配置id和active数组", allowMultiple = true, paramType = "query") })
	@OperateLog(type = OperaterType.UPDATE, desc = "自定义配置批量激活修改")
	@RequestMapping(value = "/active/batch", method = RequestMethod.POST)
	public JsonResult<Object> updateActive(@RequestBody JSONObject param) {
		JSONArray array = param.getJSONArray("custom");
		customService.updateActive(array.toJavaList(CustomParam.class));
		//sys_notice
        noticeService.insertNoticeByParam(2309,"事件激活状态更新",0,0);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 自定义配置批量告警状态修改
	 * 
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "自定义配置批量告警状态修改", notes = "自定义配置批量告警状态修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "custom", value = "自定义配置id和state数组", allowMultiple = true, paramType = "query") })
	@OperateLog(type = OperaterType.UPDATE, desc = "自定义配置批量告警修改")
	@RequestMapping(value = "/state/batch", method = RequestMethod.POST)
	public JsonResult<Object> updateState(@RequestBody JSONObject param) {
		JSONArray array = param.getJSONArray("custom");
		customService.updateState(array.toJavaList(CustomParam.class));
		//sys_notice
        noticeService.insertNoticeByParam(2310,"事件告警状态更新",0,0);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 自定义配置删除
	 * 
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "自定义配置删除", notes = "自定义配置删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.DELETE, desc = "自定义配置删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody JSONObject param) {
		customService.deleteCustom(param.getIntValue("id"), 1);
		//sys_notice
        noticeService.insertNoticeByParam(2302,"事件删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 统计维度列表查询
	 * 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "统计维度列表查询", notes = "统计维度列表查询", response = StatisticsDimension.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "customId", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/basic/dimension/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> selectStatisticsDimension(@RequestBody Map<String, Object> map) {
		int customId = evUtil.getMapIntValue(map, "customId");
		List<StatisticsDimension> list = customService.updateStepAndselectStatistics(customId);
		return execSuccess(list);
	}

	/**
	 * 维度信息列表查询
	 * 
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "维度信息列表查询", notes = "维度信息列表查询", response = AnalyzeDimension.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "customId", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/analyze/dimension/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> selectDimension(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int customId = evUtil.getMapIntValue(map, "customId");
		List<CustomFieldParam> list = customService.selectDimension(customId);
		return execSuccess(list);
	}

	/**
	 * 指标信息列表查询
	 * 
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "指标信息列表查询", notes = "指标信息列表查询", response = CustomFieldParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "customId", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/analyze/indicator/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> selectIndicator(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int customId = evUtil.getMapIntValue(map, "customId");
		List<Map<String, Object>> list = customService.selectIndicator(customId);
		return execSuccess(list);
	}

	/**
	 * 分析信息保存
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "分析信息保存", notes = "分析信息保存")
	@ApiImplicitParams({ @ApiImplicitParam(name = "customId", value = "配置id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "flag", value = "0 维度保存  1指标保存", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "dimension", value = "维度数组", allowMultiple = true, paramType = "query"),
			@ApiImplicitParam(name = "indicator", value = "指标数组", allowMultiple = true, paramType = "query") })
	@OperateLog(type = OperaterType.INSERT, desc = "自定义配置分析信息保存")
	@RequestMapping(value = "/analyze/save", method = RequestMethod.POST)
	public JsonResult<Object> saveAnalyze(@RequestBody JSONObject param) {
		int customId = param.getIntValue("customId");
		int flag = param.getIntValue("flag");
		if (flag == 1) {
			JSONArray indicator = param.getJSONArray("indicator");
			if (null != indicator) {
				customService.addIndicator(customId, indicator.toJavaList(CustomFieldParam.class));
			} else {
				customService.addIndicator(customId, null);
			}
			//sys_notice
	        noticeService.insertNoticeByParam(2312,"事件指标保存",0,0);
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		}
		if (flag == 0) {
			List<CustomFieldParam> list = param.getJSONArray("dimension").toJavaList(CustomFieldParam.class);
			if (!evUtil.listIsNullOrZero(list)) {
				customService.addDimension(customId,list);
			} else {
				customService.addDimension(customId, null);
			}
			//sys_notice
	        noticeService.insertNoticeByParam(2311,"事件分析维度保存",0,0);
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		}else {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
	}


	/**
	 * 别名查询
	 * 
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "别名查询", notes = "别名查询", response = DimensionParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "customId", value = "自定义配置id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "menuId", value = "列表id (0代表维度,1代表指标)", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/othername/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> select(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		Integer customId = evUtil.getMapIntValue(map, "customId");
		Integer menuId = evUtil.getMapIntValue(map, "menuId");
		List<DimensionParam> list = customService.selectOtherName(customId, menuId);
		return execSuccess(list);
	}

	/**
	 * 别名保存
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "别名保存", notes = "别名保存")
	@OperateLog(type = OperaterType.INSERT, desc = "别名保存")
	@RequestMapping(value = "/othername/save", method = RequestMethod.POST)
	public JsonResult<Object> save(@RequestBody DimensionParam param) {
		String otherName = param.getOtherName();
		if (StringUtils.isNotBlank(otherName)) {
			if (otherName.length() > 10) {
				return execCheckError("别名长度过长，请重新输入");
			}
		}
		customService.saveOtherName(param);
		int menuId = param.getMenuId();
		switch (menuId) {
		case 0:
			//sys_notice
	        noticeService.insertNoticeByParam(2313,"事件维度别名保存",0,0);
			break;
		case 1:
			//sys_notice
	        noticeService.insertNoticeByParam(2314,"事件指标别名保存",0,0);
			break;
		default:
			break;
		}
		
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 基本信息新增
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "基本信息新增", notes = "基本信息新增")
	@ApiImplicitParams({ @ApiImplicitParam(name = "name", value = "配置名称", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.INSERT, desc = "自定义配置-基本信息新增")
	@RequestMapping(value = "/basic/msg/add", method = RequestMethod.POST)
	public JsonResult<Object> addBasicMsg(@RequestBody Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		int componentId = evUtil.getMapIntValue(map, "componentId");
		String name = evUtil.getMapStrValue(map, "name");
		CustomParam cs = new CustomParam();
		cs.setName(name);
		// 校验用户名
		Boolean flag = customService.checkCustomName(cs, componentId,1);
		if (flag) {
			int customId = customService.addBasicMsg(applicationId, componentId, name);
			//sys_notice
	        noticeService.insertNoticeByParam(2301,"事件新增",0,0);
			//指标默认选中 交易量 响应时间 响应率 成功率
	        JSONObject param=new JSONObject();
	        List<Integer> type=new ArrayList<>(2);
	        type.add(1);
	        //type.add(2);
	        List<Map<String, Object>> indicator=componentService.selectIndicator(type);
	        param.put("flag", 1);
	        List<CustomFieldParam> field=new ArrayList<>();
	        for (Map<String, Object> ma : indicator) {
	        	CustomFieldParam p=new CustomFieldParam();
	        	p.setFieldId(Integer.parseInt(ma.get("value").toString()));
	        	field.add(p);
			}
	        param.put("indicator", field);
	        param.put("customId", customId);
	        saveAnalyze(param);
			return execSuccess(customId);
		} else {
			return execCheckError("名称不能重复！");
		}
	}

	/**
	 * 基本信息编辑
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "基本信息编辑", notes = "基本信息编辑")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "name", value = "配置名称", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "change", value = "组件是否更改", dataType = "Boolean", paramType = "query") })
	@OperateLog(type = OperaterType.UPDATE, desc = "自定义配置-基本信息编辑")
	@RequestMapping(value = "/basic/msg/edit", method = RequestMethod.POST)
	public JsonResult<Object> editBasicMsg(@RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		int componentId = evUtil.getMapIntValue(map, "componentId");
		String name = evUtil.getMapStrValue(map, "name");
		Boolean change = (Boolean) map.get("change");
		CustomParam cs = new CustomParam();
		cs.setName(name);
		cs.setId(id);
		// 校验用户名
		Boolean flag = customService.checkCustomName(cs,componentId, 0);
		if (flag) {
			customService.editBasicMsg(id, applicationId, componentId, name, change);
			if(change) {
				//sys_notice
		        noticeService.insertNoticeByParam(2302,"事件编辑",0,0);
			}
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		} else {
			return execCheckError("名称不能重复！");
		}
	}

	/**
	 * 自定义配置基本信息查询
	 * 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "自定义配置基本信息查询", notes = "自定义配置基本信息查询", response = Custom.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/basic/msg/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> selectBasicMsg(@RequestBody Map<String, Object> map) {
		return execSuccess(customService.selectBasicMsg(evUtil.getMapIntValue(map, "id")));
	}

	/**
	 * 上传信息查看
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "上传信息查看", notes = "上传信息查看", response = DimensionValue.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "统计维度表id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/basic/dimension/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectDimensionDetail(@RequestBody Map<String, Object> map) {
		return execSuccess(customService.selectDimensionDetail(evUtil.getMapIntValue(map, "id")));
	}

	/**
	 * 统计维度编辑
	 * @param statisticsDimension
	 * @return
	 */
	@ApiOperation(value = "统计维度编辑", notes = "统计维度编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "自定义配置-统计维度编辑")
	@RequestMapping(value = "/basic/dimension/edit", method = RequestMethod.POST)
	public JsonResult<Object> editDimension(@RequestBody @Validated StatisticsDimension statisticsDimension) {
		// 校验用户名
		Boolean flag = customService.checkName(statisticsDimension, 0);
		if (flag) {
			customService.editDimension(statisticsDimension.getId(), statisticsDimension.getDimensionId(),
					statisticsDimension.getName(),statisticsDimension.getUseDictionary());
			noticeService.insertNoticeByParam(2315,"统计维度编辑",0,0);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		} else {
			return execCheckError("翻译别名不能重复！");
		}
	}

	/**
	 * 统计维度新增
	 * @param file
	 * @param dimension
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	@ApiOperation(value = "统计维度新增", notes = "统计维度新增")
	@OperateLog(type = OperaterType.INSERT, desc = "自定义配置-统计维度新增")
	@RequestMapping(value = "/basic/dimension/add", method = RequestMethod.POST)
	public JsonResult<Object> addDimension(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			@Validated StatisticsDimension dimension) throws IOException, EncryptedDocumentException, InvalidFormatException {
		int customId=dimension.getCustomId();
		// 校验用户名
		Boolean flag = customService.checkName(dimension, 1);
		if (flag) {
			boolean fileFlag=file==null?true:false;
			Integer statisticsDimensionId = customService.addstatisticsDimension(dimension);
			//默认录入
			Integer type=dimension.getType();
			if(null!=type&&!type.equals(0)&&fileFlag) {
				List<DimensionValue> details=new ArrayList<>();
				switch (type) {
				// 省份
				case 1:
					List<DictionaryParam> list = regioninfoService.selectProvince();
					for (DictionaryParam dictionaryParam : list) {
						DimensionValue detail=new DimensionValue();
						detail.setStatisticsDimensionId(statisticsDimensionId);
						detail.setName(dictionaryParam.getLabel());
						detail.setValue(dictionaryParam.getValue().toString());
						int sid=sequenceService.sequenceNextVal("seq_uuid");
						detail.setId(sid);
						details.add(detail);
					}
					break;
				// 地市
				case 2:
					Integer provinceId = dimension.getProvinceId();
					List<DictionaryParam> lists = regioninfoService.selectCity(provinceId);
					for (DictionaryParam dictionaryParam : lists) {
						DimensionValue detail=new DimensionValue();
						detail.setStatisticsDimensionId(statisticsDimensionId);
						detail.setName(dictionaryParam.getLabel());
						detail.setValue(dictionaryParam.getValue().toString());
						detail.setId(sequenceService.sequenceNextVal("seq_uuid"));
						details.add(detail);
					}
					break;
				default:
					break;
				}
				if (details.size() > 0) {
					customService.insertDimensionValue(details, statisticsDimensionId);
				}
				// uuid添加
				customService.saveUuid(statisticsDimensionId);
			}
			//上传
			DimensionValue dimensionvalue = new DimensionValue();
			dimensionvalue.setStatisticsDimensionId(statisticsDimensionId);
			if (!fileFlag) {
				upload(file, dimensionvalue);
			}
			//sys_notice
	        noticeService.insertNoticeByParam(2304,"事件统计维度新增",0,0);
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		} else {
			return execCheckError("翻译别名不能重复！");
		}
	}


	/**
	 * 统计维度删除
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "统计维度删除", notes = "统计维度删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "统计维度表id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.DELETE, desc = "自定义配置-统计维度删除")
	@RequestMapping(value = "/basic/dimension/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteDimension(@RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		customService.deleteDimension(id);
		//sys_notice
        noticeService.insertNoticeByParam(2305,"事件统计维度删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 过滤条件查询
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "过滤条件查询", notes = "过滤条件查询", response = CustomBasicParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/basic/filter/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> selectFilter(@RequestBody Map<String, Object> map) {
		return execSuccess(customService.updateStepAndselectFilter(evUtil.getMapIntValue(map, "id")));
	}

	/**
	 * 成功条件查询
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "成功条件查询", notes = "成功条件查询", response = CustomBasicParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/basic/success/select", method = RequestMethod.POST)
	@RequirePermission("common")
	public JsonResult<Object> selectSuccess(@RequestBody Map<String, Object> map) {
		return execSuccess(customService.updateStepAndselectSuccess(evUtil.getMapIntValue(map, "id")));
	}

	/**
	 * 过滤条件保存
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "过滤条件保存", notes = "过滤条件保存")
	@OperateLog(type = OperaterType.INSERT, desc = "自定义配置-过滤条件保存")
	@RequestMapping(value = "/basic/filter/save", method = RequestMethod.POST)
	public JsonResult<Object> saveFilter(@RequestBody CustomBasicParam param) {
		customService.saveFilter(param);
		//sys_notice
        noticeService.insertNoticeByParam(2307,"事件筛选条件保存",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 *  成功条件保存
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "成功条件保存", notes = "成功条件保存")
	@OperateLog(type = OperaterType.INSERT, desc = "自定义配置-成功条件保存")
	@RequestMapping(value = "/basic/success/save", method = RequestMethod.POST)
	public JsonResult<Object> saveSuccess(@RequestBody CustomBasicParam param) {
		customService.saveSuccess(param);
		//sys_notice
        noticeService.insertNoticeByParam(2308,"事件成功值条件保存",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 统计维度上传
	 * @param file
	 * @param dimensionvalue
	 * @return
	 * @throws IOException
	 * @throws EncryptedDocumentException
	 * @throws InvalidFormatException
	 */
	@ApiOperation(value = "统计维度上传", notes = "统计维度上传")
	@RequestMapping(value = "/basic/dimension/upload", method = RequestMethod.POST)
	public JsonResult<Object> upload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			DimensionValue dimensionvalue) throws IOException, EncryptedDocumentException, InvalidFormatException {
		List<DimensionValue> details = new ArrayList<>();
		String fileName = file.getOriginalFilename();
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		if (!"xls".equals(fieldType)) {
			return execCheckError("文件上传失败！文件类型错误");
		}
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}
		InputStream in = file.getInputStream();
		Workbook xssfWorkbook = WorkbookFactory.create(in);
		Sheet xssfSheet = xssfWorkbook.getSheetAt(0);
		if (xssfSheet == null) {
			xssfWorkbook.close();
			return execCheckError("文件解析失败！请确认是否是下载的文件类型");
		}
		int statisticsDimensionId = dimensionvalue.getStatisticsDimensionId();
		for (int i = 3; i <= xssfSheet.getLastRowNum(); i++) {
			Row xssfRow = xssfSheet.getRow(i);
			if (xssfRow != null) {
				DimensionValue detail = new DimensionValue();
				Cell value = xssfRow.getCell(0);
				Cell name = xssfRow.getCell(1);
				String valueStr = "";
				String nameStr = "";

				if (value == null && name == null) {
					continue;
				} else if (name == null) {
					xssfWorkbook.close();
					int row = i - 2;
					return execCheckError("第" + row + "行翻译为空");
				} else if (value == null) {
					xssfWorkbook.close();
					int row = i - 2;
					return execCheckError("第" + row + "行数据值为空");
				}

				if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					long n = (long) value.getNumericCellValue();
					valueStr = n + "";
				} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					valueStr = value.getStringCellValue();
				}

				if (name.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					long n = (long) name.getNumericCellValue();
					nameStr = n + "";
				} else if (name.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					nameStr = name.getStringCellValue();
				}
				detail.setStatisticsDimensionId(statisticsDimensionId);
				detail.setName(nameStr);
				detail.setValue(valueStr);
				detail.setId(sequenceService.sequenceNextVal("seq_uuid"));
				details.add(detail);
			}
		}
		if (details.size() > 0) {
			customService.insertDimensionValue(details, statisticsDimensionId);
		}
		xssfWorkbook.close();
		// uuid添加
		customService.saveUuid(statisticsDimensionId);
		//sys_notice
        noticeService.insertNoticeByParam(2306,"事件统计维度值上传",0,0);
		return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
	}

	/**
	 * 统计维度下载
	 * @param req
	 * @param resp
	 * @param param
	 * @throws IOException
	 */
	@ApiOperation(value = "统计维度下载", notes = "统计维度下载")
	@RequestMapping(value = "/basic/dimension/download", method = RequestMethod.POST)
	public void download(HttpServletRequest req, HttpServletResponse resp, @RequestBody CustomDownloadParam param)
			throws IOException {
		String fileName = "dimensionData.xls";
		String firstTitle = "维度数据";
		String secondTitle = "维度数据";
		List<String> tableTitleLine = new ArrayList<>();
		tableTitleLine.add("数据值");
		tableTitleLine.add("翻译");
		List<List<String>> resultData = new ArrayList<>();
		if (null != param.getId()) {
			List<DimensionValue> list = customService.selectDimensionDetail(param.getId());
			for (DimensionValue dimensionValue : list) {
				List<String> rData = new ArrayList<>();
				rData.add(dimensionValue.getValue());
				rData.add(dimensionValue.getName());
				resultData.add(rData);
			}
		} else {
			Integer type = param.getType();
			switch (type) {
			// 普通
			case 0:
				break;
			// 省份
			case 1:
				List<DictionaryParam> list = regioninfoService.selectProvince();
				for (DictionaryParam dictionaryParam : list) {
					List<String> rData = new ArrayList<>();
					rData.add(dictionaryParam.getValue().toString());
					rData.add(dictionaryParam.getLabel());
					resultData.add(rData);
				}
				break;
			// 地市
			case 2:
				Integer provinceId = param.getProvinceId();
				List<DictionaryParam> lists = regioninfoService.selectCity(provinceId);
				for (DictionaryParam dictionaryParam : lists) {
					List<String> rData = new ArrayList<>();
					rData.add(dictionaryParam.getValue().toString());
					rData.add(dictionaryParam.getLabel());
					resultData.add(rData);
				}
				break;
			case 3:
				Integer province = param.getProvinceId();
				Integer cityId = param.getCityId();
				if (province != -1 && cityId != -1) {
					List<Regioninfo> city = regioninfoService.getCity(province, cityId);
					for (Regioninfo regioninfo : city) {
						List<String> rData = new ArrayList<>();
						rData.add(regioninfo.getCityId().toString());
						rData.add(regioninfo.getCityName());
						resultData.add(rData);
					}
				} else {
					List<DictionaryParam> citys = regioninfoService.selectCity(province);
					for (DictionaryParam dictionaryParam : citys) {
						List<String> rData = new ArrayList<>();
						rData.add(dictionaryParam.getValue().toString());
						rData.add(dictionaryParam.getLabel());
						resultData.add(rData);
					}
				}
				break;
			default:
				break;
			}
		}
		ReportUtil.reportEXCEL(fileName, firstTitle, secondTitle, tableTitleLine, resultData, resp);
	}

	/**
	 * 步骤查询
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "步骤查询", notes = "步骤查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "配置id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/basic/step", method = RequestMethod.POST)
	public JsonResult<Object> selectStep(@RequestBody Map<String, Object> map) {
		Object step = customService.selectStep(evUtil.getMapIntValue(map, "id"));
		return execSuccess(step);
	}

	/**
	 * 告警查询
	 * @author NJ
	 * @date 2018/8/28 13:52
	 * @param map
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="告警查询", notes="告警查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name="customId",value="应用id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="type",value="告警类型",dataType="int", paramType = "query")})
	@RequestMapping(value = "/alarm/query", method = RequestMethod.POST)
	public JsonResult<Object> alarmSelect(@RequestBody Map<String, Object> map) {
		return execSuccess( alarmRuleService.selectCustomAlarmRule(map));
	}

	/**
	 * 告警删除
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警删除", notes="告警删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "告警删除")
	@RequestMapping(value = "/alarm/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		AlarmRule alarmRule = alarmRuleService.selectById(id);
		alarmRuleService.deleteAlarmRule(id);
		//sys_notice
		insertNoticeCustomByRuleType(alarmRule.getType(), 3);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 激活状态更新
	 * @author NJ
	 * @date 2018/6/7 14:36
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="激活状态更新", notes="激活状态更新")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "激活状态更新")
	@RequestMapping(value = "/alarm/active", method = RequestMethod.POST)
	public JsonResult<Object> active( @RequestBody JSONObject param) {
		int id = param.getIntValue("id");
		alarmRuleService.updateActive(id,param.getIntValue("active"));
		//sys_notice
		AlarmRule alarmRule = alarmRuleService.selectById(id);
		insertNoticeCustomByRuleType(alarmRule.getType(), 4);
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}

	/**
	 * 激活状态批量更新
	 * @author NJ
	 * @date 2018/6/7 14:41
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="激活状态批量更新", notes="激活状态批量更新")
	@ApiImplicitParams({
			@ApiImplicitParam(name="ids",value="告警id集合",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "激活状态批量更新")
	@RequestMapping(value = "/alarm/active/batch", method = RequestMethod.POST)
	public JsonResult<Object> batchActive(@RequestBody JSONObject param) {
		int type = param.getIntValue("type");
		alarmRuleService.updateBatchActiveCustom(param.getIntValue("customId"), param.getIntValue("active"),
				type);
		//sys_notice
		insertNoticeCustomByRuleType(type, 4);
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}

	/**
	 * 自定义业务告警详情查询
	 * @author NJ
	 * @date 2018/8/28 14:49
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="自定义业务告警详情查询", notes="自定义业务告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")
	})
	@RequestMapping(value="/alarm/business/detail" , method = RequestMethod.POST)
	public JsonResult<Object> getbusinessAlarmDetail(@RequestBody JSONObject param) {
		/**
		 * 自定义业务告警详情查询与应用查询一致
		 */
		return execSuccess(alarmRuleService.getApplicationAlarmDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}
	/**
	 * 业务告警保存
	 * @author NJ
	 * @date 2018/8/28 14:19
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="业务告警保存", notes="业务告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "业务告警保存")
	@RequestMapping(value = "/alarm/business/save", method = RequestMethod.POST)
	public JsonResult<Object> addBusinessAlarm(@RequestBody AlarmParam param) {
		param.setType(Constant.ALARMTYPE_CUSTOM_BUSINESS);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 统计维度告警详情查询
	 * @author NJ
	 * @date 2018/8/28 14:49
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="统计维度告警详情查询", notes="统计维度告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")
	})
	@RequestMapping(value="/alarm/statistics/detail" , method = RequestMethod.POST)
	public JsonResult<Object> getStatisticsAlarmDetail(@RequestBody JSONObject param) {
		/**
		 * 调用单维度方法
		 */
		return execSuccess(alarmRuleService.getSingleDimensionDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 统计维度告警保存
	 * @author NJ
	 * @date 2018/8/28 14:52
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="统计维度告警保存", notes="统计维度告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "统计维度告警保存")
	@RequestMapping(value = "/alarm/statistics/save", method = RequestMethod.POST)
	public JsonResult<Object> addStatisticsAlarm(@RequestBody AlarmParam param) {
		param.setType(Constant.ALARMTYPE_CUSTOM_STATISTICS_DIMENSION);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 普通维度告警详情查询
	 * @author NJ
	 * @date 2018/8/28 14:49
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="普通维度告警详情查询", notes="普通维度告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")
	})
	@RequestMapping(value="/alarm/normal/detail" , method = RequestMethod.POST)
	public JsonResult<Object> getNormalAlarmDetail(@RequestBody JSONObject param) {
		//普通维度查询与单维度查询逻辑一致，调用原来的方法
		return execSuccess(alarmRuleService.getSingleDimensionDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 普通维度告警保存,普通维度保存逻辑与单维度一致
	 * @author NJ
	 * @date 2018/8/28 14:52
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="普通维度告警保存", notes="普通维度告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "普通维度告警保存")
	@RequestMapping(value = "/alarm/normal/save", method = RequestMethod.POST)
	public JsonResult<Object> addNormalAlarm(@RequestBody AlarmParam param) {
		param.setType(Constant.ALARMTYPE_CUSTOM_NORMAL_DIMENSION);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 多维度告警详情查询
	 * @author NJ
	 * @date 2018/8/28 14:49
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="多维度告警详情查询", notes="多维度告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")
	})
	@RequestMapping(value="/alarm/multi/detail" , method = RequestMethod.POST)
	public JsonResult<Object> getMultiAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getMultiDimensionDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 多维度告警保存
	 * @author NJ
	 * @date 2018/8/28 14:52
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="多维度告警保存", notes="多维度告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "多维度告警保存")
	@RequestMapping(value = "/alarm/multi/save", method = RequestMethod.POST)
	public JsonResult<Object> addMultiAlarm(@RequestBody AlarmParam param) {
		param.setType(Constant.ALARMTYPE_CUSTOM__MULTI_DIMENSION);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 分别调用新增和修改接口
	 * @param param
	 */
	private void saveOrUpdateAlarm(AlarmParam param){
		List<String> indicatorIds  =  Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","));
		if(param.getId() == null || param.getId().intValue() <= 0){
			alarmRuleService.addAlarm(param,indicatorIds );
			insertNoticeCustomByRuleType(param.getType(), 1);
		}else{
			alarmRuleService.updateAlarm(param,indicatorIds);
			insertNoticeCustomByRuleType(param.getType(), 2);
		}
	}


	/**
	 * 根据告警的类型调用不同的通知
	 * @param ruleType
	 * @param type l 新增  2 编辑 3 删除 4 激活
	 */
	private void insertNoticeCustomByRuleType(int ruleType, int type){
		//告警类型 1 应用 ，2 组件， 3ip， 4port，5单位维度 ，6 多维度 ，7 监控点告警 ,
		//8 数据源告警；9自定义-- 业务 ，10自定义-- 统计维度， 11自定义-- 普通维度， 12自定义--  多维度',
		if(type == 1){
			//新增
			switch (ruleType){
				case 9:
					noticeService.insertNoticeByParam(1901,"事件告警创建",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3101,"事件统计维度告警创建",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3201,"事件普通维度告警创建",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3301,"事件多级维度告警创建",0,0);
					break;
				default:
					break;
			}
		}else if(type == 2){
			// 编辑
			switch (ruleType){
				case 9:
					noticeService.insertNoticeByParam(1902,"事件告警编辑",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3102,"事件统计维度告警编辑",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3202,"事件普通维度告警编辑",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3302,"事件多级维度告警编辑",0,0);
					break;
				default:
					break;
			}
		}
		if(type == 3){
			switch (ruleType) {
				case 9:
					noticeService.insertNoticeByParam(1903,"事件告警删除",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3103,"事件统计维度告警删除",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3203,"事件普通维度告警删除",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3303,"事件多级维度告警删除",0,0);
					break;
				default:
					break;
			}
		}else if(type == 4){
			switch (ruleType) {
				case 9:
					noticeService.insertNoticeByParam(1904,"事件告警激活",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3104,"事件统计维度告警激活",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3204,"事件普通维度告警激活",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3304,"事件多级维度告警激活",0,0);
					break;
				default:
					break;
			}
		}
	}

}
