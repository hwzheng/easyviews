package com.greattimes.ev.bpm.controller.alarm;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.alarm.param.req.AlarmInfoParam;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.entity.AlarmConditionTemplateParam;
import com.greattimes.ev.bpm.entity.AlarmInfo;
import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.service.alarm.IAlarmService;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.bpm.service.config.IAlarmRuleService;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.IndicatorIntervalParam;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.service.ITransactionService;
import com.greattimes.ev.system.service.IDictionaryService;
import com.greattimes.ev.utils.IndicatorSupportUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2018/9/19 19:07
 */
@RestController
@Api(value = "AlarmController", tags = "告警查询")
@RequestMapping("/alarm")
public class AlarmController extends BaseController{

    @Autowired
    private IAlarmService alarmService;
    @Autowired
    private IAlarmRuleService alarmRuleService;
    @Autowired
    private ITransactionService transactionService;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private ICustomService customService;
    @Autowired
    private IComponentDimensionService componentDimensionService;
    @Autowired
    private IApplicationService applicationService;

    @Autowired
    private ConfigurationCache configurationCache;

    @ApiOperation(value = "应用告警数查询", notes = "应用告警数查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name="uuid",value="uuid",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="start",value="开始时间戳",dataType="long", paramType = "query"),
            @ApiImplicitParam(name="end",value="结束时间戳",dataType="long", paramType = "query")
    })
    @RequestMapping(value = "/application", method = RequestMethod.POST)
    public JsonResult<Object> selectApplicationAlarmAmount(@RequestBody JSONObject jsonObject) {
        JSONArray jsonArray = jsonObject.getJSONArray("applicationId");
        Long start = jsonObject.getLong("start");
        Long end = jsonObject.getLong("end");
        if(jsonArray == null || start == null || end == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        List<Integer> applicationIds = jsonArray.toJavaList(Integer.class);
        int interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL)) * 1000;
        return execSuccess(alarmService.selectApplicationAlarm(applicationIds,start,end, interval));
    }


    @ApiOperation(value = "其它告警数查询", notes = "其它告警数查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name="uuid",value="uuid",dataType="array", paramType = "query"),
            @ApiImplicitParam(name="start",value="开始时间戳",dataType="long", paramType = "query"),
            @ApiImplicitParam(name="end",value="结束时间戳",dataType="long", paramType = "query")
    })
    @RequestMapping(value = "/other/amount", method = RequestMethod.POST)
    public JsonResult<Object> selectOtherAlarmAmount(@RequestBody JSONObject jsonObject) {
        long start = jsonObject.getLongValue("start");
        long end = jsonObject.getLongValue("end");
        List<Map<String, Object>>  uuidArray =  (List)jsonObject.getJSONArray("uuid");
        List<Map<String, Object>> uuidsCon = new ArrayList<>();
        uuidArray.forEach(x->{
            Map<String, Object> map = new HashMap<>();
            map.put("value", (List<Integer>)x.get("value"));
            map.put("type", x.get("type"));
            uuidsCon.add(map);
        });
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("start",start);
        paramMap.put("end", end);
        paramMap.put("uuids", uuidsCon);
        paramMap.put("interval", Integer.parseInt(configurationCache.getValue(Constant.BPM_INTERVAL)) * 1000);
        return execSuccess(alarmService.selectOtherAlarmAmount(paramMap));
    }



    @ApiOperation(value = "告警列表查询", notes = "告警列表查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name="uuid",value="uuid",dataType="array", paramType = "query"),
            @ApiImplicitParam(name="start",value="开始时间戳",dataType="long", paramType = "query"),
            @ApiImplicitParam(name="end",value="结束时间戳",dataType="long", paramType = "query")
    })
    @RequestMapping(value = "/list", method = RequestMethod.POST)
    @Deprecated
    public JsonResult<Object> selectAlarmList(@RequestBody JSONObject jsonObject) {
        int pageSize = jsonObject.getIntValue("pageSize");
        int currentPage = jsonObject.getIntValue("currentPage");
        int alarmlevel = jsonObject.getIntValue("alarmlevel");
        int productType = jsonObject.getIntValue("productType");
        int applicationId = jsonObject.getIntValue("applicationId");
        String constraintDesc = jsonObject.getString("constraintDesc");
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("alarmlevel", alarmlevel);
        paramMap.put("productType", productType);
        paramMap.put("applicationId", applicationId);
        paramMap.put("constraintDesc", constraintDesc);
        paramMap.put("start", jsonObject.getLongValue("start"));
        paramMap.put("end", jsonObject.getLongValue("end"));
        return execSuccess(alarmService.selectPageByIdAndType(new Page<>(currentPage,pageSize),paramMap));
    }

    @ApiOperation(value = "告警保存", notes = "告警保存")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="id",dataType="告警id", paramType = "query"),
            @ApiImplicitParam(name="manageState",value="是否已经处理",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="managerdsec",value="告警详情",dataType="string", paramType = "query")
    })
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public JsonResult<Object> updateAlarmOpinion(@RequestBody JSONObject jsonObject) {

        List<Integer> ids = jsonObject.getJSONArray("id").toJavaList(Integer.class);

        alarmService.updateAlarmInfoOpinion(ids,
                jsonObject.getIntValue("manageState"),jsonObject.getString("managerdsec"));

        return execSuccess(Constant.SAVE_SUCCESS_MSG);
    }


    @ApiOperation(value = "告警详情", notes = "告警详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="id",dataType="告警id", paramType = "query")
    })
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public JsonResult<Object> selectAlarmDetail(@RequestBody JSONObject jsonObject) {
        return execSuccess(alarmService.selectAlarmInfoById(jsonObject.getIntValue("id")));
    }


    @ApiOperation(value = "告警浏览器", notes = "告警浏览器")
    @RequestMapping(value = "/browser", method = RequestMethod.POST)
    public JsonResult<Object> selectAlarmBrowser(HttpServletRequest request, @RequestBody AlarmInfoParam alarmInfoParam) {
        int interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL)) * 1000;
        alarmInfoParam.setBpmInterval(interval);
        //加入权限控制
        List<ApplicationParam> applicationParams = applicationService.selectActiveApplication(getSessionUser(request).getId());
        if(evUtil.listIsNullOrZero(applicationParams)){
            return execSuccess(Collections.EMPTY_LIST);
        }
        List<Integer> appIds = applicationParams.stream().map(ApplicationParam::getId).collect(Collectors.toList());
        Integer applicationId = alarmInfoParam.getApplicationId();

        List<Integer> authIds = new ArrayList<>();
        //如果传入appId不在该用户的权限下则返回空列表
        if(applicationId != null ){
            if(!appIds.contains(applicationId)){
                return execSuccess(Collections.EMPTY_LIST);
            }else{
                authIds.add(applicationId);
                alarmInfoParam.setAuthAppIds(authIds);
            }
        }else{
            alarmInfoParam.setAuthAppIds(appIds);
        }


        return execSuccess(alarmService.selectAlarmBrowserByParams(alarmInfoParam));
    }

    /**
     * 大屏接口去掉权限控制
     * @param request
     * @param alarmInfoParam
     * @return
     */
    @ApiOperation(value = "告警浏览器", notes = "告警浏览器")
    @RequestMapping(value = "/browser/page", method = RequestMethod.POST)
    public JsonResult<Object> selectAlarmBrowserPage(HttpServletRequest request, @RequestBody AlarmInfoParam alarmInfoParam) {
        int interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL)) * 1000;
        alarmInfoParam.setBpmInterval(interval);

        //加入权限控制
        List<ApplicationParam> applicationParams = applicationService.selectActiveApplication(getSessionUser(request).getId());
        if(evUtil.listIsNullOrZero(applicationParams)){
            return execSuccess(Collections.EMPTY_LIST);
        }
        List<Integer> appIds = applicationParams.stream().map(ApplicationParam::getId).collect(Collectors.toList());
        Integer applicationId = alarmInfoParam.getApplicationId();

        List<Integer> authIds = new ArrayList<>();
        //如果传入appId不在该用户的权限下则返回空列表
        if(applicationId != null ){
            if(!appIds.contains(applicationId)){
                return execSuccess(Collections.EMPTY_LIST);
            }else{
                authIds.add(applicationId);
                alarmInfoParam.setAuthAppIds(authIds);
            }
        }else{
            alarmInfoParam.setAuthAppIds(appIds);
        }


        Page<AlarmInfo> page = new Page<>();
        Integer pageSize = alarmInfoParam.getPageSize();
        Integer pageNum = alarmInfoParam.getPageNum();
        if(pageSize != null && pageNum != null){
            page = new Page<>(pageNum, pageSize);
        }
        return execSuccess(alarmService.selectAlarmBrowserPageByParams(page, alarmInfoParam));
    }


    @ApiOperation(value = "根据告警id查询告警", notes = "根据告警id查询告警")
    @RequestMapping(value = "/info", method = RequestMethod.POST)
    public JsonResult<Object> selectAlarmBrowserByAlarmIds(@RequestBody JSONObject param) {

        List<Integer> ids = param.getJSONArray("ids").toJavaList(Integer.class);
        if(evUtil.listIsNullOrZero(ids)){
            return execSuccess(new ArrayList<>());
        }else{
            Map<String, Object> map = new HashMap<>();
            int interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL)) * 1000;
            map.put("interval", interval);
            map.put("ids", ids);
            return execSuccess(alarmService.selectAlarmInfoByIds(map));
        }
    }


    @ApiOperation(value = "告警浏览器指标查询", notes = "告警浏览器指标查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="id",dataType="告警id", paramType = "query")
    })
    @RequestMapping(value = "/browser/indicator", method = RequestMethod.POST)
    public JsonResult<Object> selectAlarmBrowserIndicator(@RequestBody JSONObject jsonObject) {
        /**
         * jumpType 1 跳转kpi  2 跳转多维
         */
        Map<String, Object> result = new HashMap<>();
        //告警配置( alarminfo 与 alarmdimension 一对多关系)
        List<AlarmInfo> alarms = alarmService.selectAlarmInfoAndDimensionById(jsonObject.getIntValue("id"));
        AlarmInfo alarmInfo;
        if(evUtil.listIsNullOrZero(alarms)){
            return execSuccess(new IndicatorDataParam());
        }else{
            alarmInfo = alarms.get(0);
        }

        IndicatorParam  indicatorParam = new IndicatorParam();

       //获取告警配置模板中的指标
        List<AlarmConditionTemplateParam> templates =  alarmRuleService.selectTemplateParamByRuleId(alarmInfo.getRuleId());
        List<Integer> indicatorIds = templates.stream().map(AlarmConditionTemplateParam::getIndicatorId).collect(Collectors.toList());
        //bpm-indicator
        Map<Integer, Indicator> indicatorMap = commonService.findIndicatorMapByIds(indicatorIds);

        /**
         * 统计模板(包含重复的指标)
         * 判断是否包含除了大于或者小于颗粒度模板的其它指标
         * 如果有判断是否含有 如果其它模板仅有基线模板(7) 并且是清华基线则结果时间数组则不进行交集操作
         */
        boolean needMerge = true;
        List<com.greattimes.ev.indicator.param.req.Indicator > indicatorList = new ArrayList<>();
        Set<Integer> templateCodeSet = new HashSet<>();
        for(AlarmConditionTemplateParam templateParam : templates){
            Integer templateCode = templateParam.getTemplateCode();
            Indicator indicator = indicatorMap.get(templateParam.getIndicatorId());
            indicator.setTemplateCode(templateCode);
            indicator.setThreshlod(templateParam.getThreshold());
            //bpm->indicator
            com.greattimes.ev.indicator.param.req.Indicator target = new com.greattimes.ev.indicator.param.req.Indicator();
            BeanUtils.copyProperties(indicator, target);
            indicatorList.add(target);
            //大于颗粒度和小于颗粒度模板
            if(templateCode != 2 && templateCode != 4){
                templateCodeSet.add(templateCode);
            }
        }

        if(templateCodeSet.isEmpty()){
            needMerge = false;
        }else{
            //基线模板
            if(templateCodeSet.size() == 1 && templateCodeSet.iterator().next().equals(7)){
                String qingHua  = configurationCache.getValue(Constant.USE_QINGHUA);
                if("1".equals(qingHua)){
                    needMerge = false;
                }
            }
        }
        indicatorParam.setNeedMerge(needMerge);
        indicatorParam.setIndicatorList(indicatorList);

        /**
         *  1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度 ， 7 监控点告警 , 8 数据源告警；
         *  9自定义-- 业务 ，10自定义-- 统计维度， 11自定义-- 普通维度， 12自定义--  多维度'
         */
        Map<String, Object> otherAlarmMap = new HashMap<>();
        otherAlarmMap.put("ruleId", alarmInfo.getRuleId());
        otherAlarmMap.put("start", jsonObject.getLongValue("start"));
        otherAlarmMap.put("end", jsonObject.getLongValue("end"));
        //查询其它基线模板告警数据
        List<Map<String, Object>> otherLineAlarms = new ArrayList<>();
        int ruleType = alarmInfo.getRuleType();
        indicatorParam.setIndicator(indicatorIds);
        List<Integer> uuidList;
        result.put("ruleType", ruleType);
        result.put("applicationId", alarmInfo.getApplicationId());

        if( ruleType == 1 ){
            indicatorParam.setUuid(Arrays.asList(alarmInfo.getApplicationId()));
            otherAlarmMap.put("applicationId", alarmInfo.getApplicationId());
            otherLineAlarms = alarmService.selectAbnormalinfo(otherAlarmMap);
            result.put("jumpType", 1);
            result.put("uuid", alarmInfo.getApplicationId());
        }else if( ruleType == 2 ){
            indicatorParam.setUuid(Arrays.asList(alarmInfo.getComponentId()));
            otherAlarmMap.put("componentId", alarmInfo.getComponentId());
            otherLineAlarms = alarmService.selectAbnormalinfo(otherAlarmMap);
            result.put("jumpType", 1);
            result.put("uuid",alarmInfo.getComponentId());
        }else if( ruleType == 3 || ruleType == 4 ){
            //ip && ip/port
            indicatorParam.setUuid(Arrays.asList(Integer.parseInt(alarmInfo.getDimensionValue())));
            otherAlarmMap.put("dimensionValue", alarmInfo.getDimensionValue());
            otherAlarmMap.put("dimensionId", -1);
            otherLineAlarms = alarmService.selectAbnormalinfo(otherAlarmMap);

            result.put("jumpType", 1);
            result.put("uuid",Integer.parseInt(alarmInfo.getDimensionValue()));

        }else if(ruleType == 5 || ruleType == 6  || ruleType == 11 || ruleType == 12 ){
            //[5,6]bpmPre_3896  [11,12] extendPre_3914
            /**
             * 5单维度 ，6 多维度  11自定义-- 普通维度， 12自定义--  多维度'
             */
            //组件id
            uuidList = Arrays.asList(alarmInfo.getComponentId());
            indicatorParam.setUuid(uuidList);
            otherAlarmMap.put("dimensionValue", alarmInfo.getDimensionValue());
            otherAlarmMap.put("dimensionId", alarmInfo.getDimensionId());
            otherLineAlarms = alarmService.selectAbnormalinfo(otherAlarmMap);

            if(ruleType == 5 || ruleType == 6){
                //组件维度[区分表名]
                indicatorParam.setType(1);
                //组件维度
                result.put("componentId", alarmInfo.getComponentId());
                List<Map<String, Object>> fields =  componentDimensionService.selectDimension(alarmInfo.getComponentId());
                List<JSONObject> jumpFilterDimension = new ArrayList<>();
                if(!evUtil.listIsNullOrZero(fields)){
                    for (AlarmInfo info : alarms) {
                        JSONObject jsonObj = new JSONObject();
                        for (Map<String, Object> map : fields) {
                            if (info.getDimensionId() == evUtil.getMapIntValue(map, "fieldId")) {
                                jsonObj.put("ename", evUtil.getMapStrValue(map, "ename"));
                                jsonObj.put("value", info.getDimensionValue());
                                jsonObj.put("dimensionId", info.getDimensionId());
                                jsonObj.put("rule", 3);
                                jumpFilterDimension.add(jsonObj);
                                break;
                            }
                        }
                    }
                    indicatorParam.setFilterDimension(jumpFilterDimension);
                }
                result.put("filterDimension", jumpFilterDimension);
            }else{
                //拓展维度[区分表名]
                indicatorParam.setType(2);
                //业务维度
                result.put("customId", alarmInfo.getExtendId());
                List<Map<String, Object>> list = customService.selectCustomDimensionByCustomIdAndType(alarmInfo.getExtendId(), -1);
                List<JSONObject> jumpFilterDimension = new ArrayList<>();
                if (!evUtil.listIsNullOrZero(list)) {
                    for (AlarmInfo info : alarms) {
                        JSONObject jsonObj = new JSONObject();
                        for (Map<String, Object> map : list) {
                            if (info.getDimensionId() == evUtil.getMapIntValue(map, "id")) {
                                jsonObj.put("ename", evUtil.getMapStrValue(map, "ename"));
                                jsonObj.put("value", info.getDimensionValue());
                                jumpFilterDimension.add(jsonObj);
                                break;
                            }
                        }
                    }
                }
                result.put("filterDimension", jumpFilterDimension);
                indicatorParam.setApplicationId(alarmInfo.getApplicationId());
                indicatorParam.setCustomId(alarmInfo.getExtendId());
            }
            result.put("jumpType", 2);
        }else if(ruleType == 9){
            //自定义业务
            indicatorParam.setUuid(Arrays.asList(alarmInfo.getExtendId()));
            otherAlarmMap.put("extendId", alarmInfo.getExtendId());
            otherLineAlarms = alarmService.selectAbnormalinfo(otherAlarmMap);
            result.put("jumpType", 1);
            result.put("uuid",alarmInfo.getExtendId());
        }else if(ruleType == 10){
            //统计维度
            indicatorParam.setUuid(Arrays.asList(Integer.parseInt(alarmInfo.getDimensionValue())));
            otherAlarmMap.put("dimensionValue", alarmInfo.getDimensionValue());
            otherAlarmMap.put("dimensionId", alarmInfo.getDimensionId());
            otherLineAlarms = alarmService.selectAbnormalinfo(otherAlarmMap);
            result.put("jumpType", 1);
            result.put("uuid",alarmInfo.getDimensionValue());
        }
        //告警类型
        indicatorParam.setRuleType(ruleType);
        //权限检查
        indicatorParam.setCheckRule(true);
        //告警条件关系 1 任一条件 ，2 全部条件
        indicatorParam.setRelation(templates.get(0).getRelation());
        result.put("desc", alarmInfo.getRuledesc());
        result.put("managerdsec", alarmInfo.getManagerdsec());
        result.put("dimensiondesc", alarmInfo.getConstraintDesc());

        indicatorParam.setStart(jsonObject.getLong("start"));
        indicatorParam.setEnd(jsonObject.getLong("end"));

        /**
         * 需要换算的指标[比率] id:3,4,21,22,27,28
         * 响应率:responseRate,成功率:successRate,包进重传率:PacketInboundRetransmissionRate,
         * 包出重传率:PacketOutboundRetransmissionRate,进丢包率:InboundPacketLoss,出丢包率:OutboundPacketLoss
         */
        List<Integer> specialIndicatorIds = new ArrayList<>();
        List<String> specialList = Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","));
        if(!evUtil.listIsNullOrZero(specialList)){
            specialList.forEach(x->{
                specialIndicatorIds.add(Integer.parseInt(x));
            });
        }

        indicatorParam.setSpecialIndicators(specialIndicatorIds);


        //其它模板的告警
        List<Long> abnormalTime = new ArrayList<>();
        //key:模板的指标id value:告警的异常点
        Map<Integer, List<Long>> otherAlarmResult = new HashMap<>();

        if(!evUtil.listIsNullOrZero(otherLineAlarms)){
            for(Map<String, Object> map : otherLineAlarms){
                otherAlarmResult.computeIfAbsent(evUtil.getMapIntegerValue(map,"indicatorId"),
                        ArrayList :: new).add(evUtil.getMapLongValue(map, "time"));
            }
            abnormalTime = otherAlarmResult.values().iterator().next();
        }
        indicatorParam.setAbnormalTime(abnormalTime);
        IndicatorDataParam indicatorDataParam = transactionService.selectIndicatorDataByAlarmRule(indicatorParam);

        /**
         * 告警条件关系 1 任一条件 ，2 全部条件
         */
        int relation = indicatorParam.getRelation();
        //其他模板进行告警时间列表进行组装(逻辑and和or)
        List<IndicatorDataDetailParam> indicatorData = indicatorDataParam.getIndicator();
        if(!evUtil.listIsNullOrZero(indicatorData)){
            for(IndicatorDataDetailParam inparam : indicatorData){
                List<Long> otherAlarmTimes = new ArrayList<>();
                if(!otherAlarmResult.isEmpty() && !evUtil.listIsNullOrZero(otherAlarmResult.get(inparam.getId()))){
                    otherAlarmTimes = otherAlarmResult.get(inparam.getId());
                }
                /**
                 * 相同指标时间数组合并
                 * 规则1:and 取交集 or 取并集
                 *    2:排序
                 */
                List<Long> abnormalList = inparam.getAbnormalTime();
                if(relation == 1){
                    //or
                    if(!evUtil.listIsNullOrZero(abnormalList)){
                        otherAlarmTimes.removeAll(abnormalList);
                        abnormalList.addAll(otherAlarmTimes);
                    }else{
                        abnormalList = otherAlarmTimes;
                    }
                }else{
                    //and
//                    if(needMerge){
//                        if(!evUtil.listIsNullOrZero(abnormalList) && !evUtil.listIsNullOrZero(otherAlarmTimes)){
//                            abnormalList.retainAll(otherAlarmTimes);
//                        }else{
//                            abnormalList = new ArrayList<>();
//                        }
//                    }
                }
                Collections.sort(abnormalList);
                inparam.setAbnormalTime(abnormalList);
            }
        }

        result.put("time", indicatorDataParam.getTime());
        result.put("indicator", indicatorData);
        return  execSuccess(result);
    }


    @ApiOperation(value = "清华基线告警", notes = "清华基线告警")
    @RequestMapping(value = "/baseline/alarm", method = RequestMethod.POST)
    public JsonResult<Object> getBaseLineAlarm(@RequestBody JSONObject jsonObject) {

        /**
         * 是否采用清华接口
         */
        String qingHua  = configurationCache.getValue(Constant.USE_QINGHUA);
        if(qingHua == null || ! "1".equals(qingHua)) {
            return  execSuccess(Collections.EMPTY_LIST);
        }

        //告警配置( alarminfo 与 alarmdimension 一对多关系)
        List<AlarmInfo> alarms = alarmService.selectAlarmInfoAndDimensionById(jsonObject.getIntValue("id"));
        if(evUtil.listIsNullOrZero(alarms)){
            return  execSuccess(Collections.EMPTY_LIST);
        }

        AlarmInfo info = alarms.get(0);
        //获取告警配置模板中的指标
        List<AlarmConditionTemplateParam> templates =  alarmRuleService.selectTemplateParamByRuleId(info.getRuleId());

        if(evUtil.listIsNullOrZero(templates)){
            return  execSuccess(Collections.EMPTY_LIST);
        }


        /**
         * 1 应用，9 业务
         * 2 组件，7 监控点，8 数据源，10 统计维度
         * 3 ip， 4 ip/port
         * templateCode 7
         *
         */
        String url = configurationCache.getValue(Constant.QINGHUA_BASELINE_HOST) +
                configurationCache.getValue(Constant.QINGHUA_BASELINE_SENSITIVITY);
        List<Map<String, Object>> result = new ArrayList<>();

        for(AlarmInfo alarmInfo : alarms){
            /**
             *  kpikey 生成规则
             *
             *  指标
             *  kpi.数据层级_组件id或事件id_uuid.指标id
             *
             *  单维度
             *  kpi.数据层级_组件id或事件id_维度id_维度值.指标id
             *
             *  数据层级为告警的类型
             *
             */
            Integer ruleType = alarmInfo.getRuleType();
            if(ruleType == 9){
                //业务
                for(AlarmConditionTemplateParam conditionTemplateParam : templates){
                    if(conditionTemplateParam.getTemplateCode() == 7){
                        Map<String, Object> map = new HashMap<>();
//                        map.put("alarmlevel", alarmInfo.getAlarmlevel());
                        url += "level="+alarmInfo.getAlarmlevel()+"&title=true";
                        map.put("url", url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getExtendId()+"_"+alarmInfo.getExtendId()
                                +"."+conditionTemplateParam.getIndicatorId()));
                        result.add(map);

//                        kpiKeys.add(url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getExtendId()+"_"+alarmInfo.getExtendId()
//                                +"."+conditionTemplateParam.getIndicatorId()));
                    }
                }
            }else if(ruleType == 2){
                //组件
                for(AlarmConditionTemplateParam conditionTemplateParam : templates){
                    if(conditionTemplateParam.getTemplateCode() == 7){
                        Map<String, Object> map = new HashMap<>();
//                        map.put("alarmlevel", alarmInfo.getAlarmlevel());
                        url += "level="+alarmInfo.getAlarmlevel()+"&title=true";
                        map.put("url", url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getComponentId()+"_"+alarmInfo.getComponentId()
                                +"."+conditionTemplateParam.getIndicatorId()));
                        result.add(map);

//                        kpiKeys.add(url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getComponentId()+"_"+alarmInfo.getComponentId()
//                                +"."+conditionTemplateParam.getIndicatorId()));
                    }
                }
//            } else if(ruleType == 8){
//                //数据源
//                for(AlarmConditionTemplateParam conditionTemplateParam : templates){
//                    if(conditionTemplateParam.getTemplateCode() == 7){
//                        Map<String, Object> map = new HashMap<>();
//                        map.put("alarmlevel", alarmInfo.getAlarmlevel());
//                        map.put("url", url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getExtendId()+"_"+alarmInfo.getSourceId()
//                                +"."+conditionTemplateParam.getIndicatorId()));
//                        result.add(map);
//                    }
//                }
            }else if(ruleType == 10){
                //统计维度
                if(alarmInfo.getDimensionType() == 1){
                    for(AlarmConditionTemplateParam conditionTemplateParam : templates){
                        if(conditionTemplateParam.getTemplateCode() == 7){
                            Map<String, Object> map = new HashMap<>();
//                            map.put("alarmlevel", alarmInfo.getAlarmlevel());
                            url += "level="+alarmInfo.getAlarmlevel()+"&title=true";
                            // kpi.数据层级_组件id或事件id_维度id_维度值.指标id
                            map.put("url", url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getExtendId()+"_"+alarmInfo.getDimensionId()
                                    +"_"+alarmInfo.getDimensionValue()+"."+conditionTemplateParam.getIndicatorId()));
                            result.add(map);
//                            kpiKeys.add(url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getExtendId()+"_"+alarmInfo.getDimensionId()
//                                    +"_"+alarmInfo.getDimensionValue()+"."+conditionTemplateParam.getIndicatorId()));

                        }
                    }
                }
            }else if(ruleType == 3 || ruleType == 4) {
                //ip或者port
                if(alarmInfo.getDimensionType() == -1){
                    for(AlarmConditionTemplateParam conditionTemplateParam : templates){
                        if(conditionTemplateParam.getTemplateCode() == 7){
                            Map<String, Object> map = new HashMap<>();
//                            map.put("alarmlevel", alarmInfo.getAlarmlevel());
                            url += "level="+alarmInfo.getAlarmlevel()+"&title=true";
                            map.put("url", url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getComponentId()+"_"+alarmInfo.getDimensionValue()
                                    +"."+conditionTemplateParam.getIndicatorId()));
                            result.add(map);

//                            kpiKeys.add(url.replaceFirst("\\*", ruleType.toString()+"_"+alarmInfo.getComponentId()+"_"+alarmInfo.getDimensionValue()
//                                    +"."+conditionTemplateParam.getIndicatorId()));

                        }
                    }
                }
            }
        }
        return execSuccess(result);
    }


    @ApiOperation(value = "组件层级以下告警或者事件告警数量", notes = "组件层级以下告警或者事件告警数量")
    @RequestMapping(value = "/component/amount", method = RequestMethod.POST)
    public JsonResult<Object> getComponentAlarm(@RequestBody JSONObject jsonObject) {
        Map<String, Object> param = new HashMap<>();
        Integer type = jsonObject.getInteger("type");
        Integer uuid = jsonObject.getInteger("uuid");
        Long start = jsonObject.getLong("start");
        Long end = jsonObject.getLong("end");
        Integer intervalType;
        IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start,end, configurationCache);
        //0 seconds 1 hour  2 min   3 day
        Integer indicatorType = indicatorParam.getType();
        if(indicatorType == 0){
            int bpmInterval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
            if(bpmInterval == 60) {
                intervalType = -1;
            }else{
                intervalType = indicatorType;
            }
        }else{
            intervalType = indicatorType;
        }
        param.put("start",start);
        param.put("end",end);
        param.put("type", type);
        param.put("id", uuid);
        param.put("intervalType", intervalType);
        List<Map<String, Object>> data = alarmService.selectAlarmNumByInterval(param);
        List<Long> timeList = indicatorParam.getTimeList();
        Map<String, Object> result = new HashMap<>();
        List<Integer> amountList = new ArrayList<>();
        boolean flag;
        for(long time : timeList){
            flag = false;
            for(Map<String, Object> map : data){
                if(evUtil.getMapLongValue(map, "atime") == time){
                    amountList.add(evUtil.getMapIntValue(map, "amount"));
                    flag = true;
                    break;
                }
            }
            if(!flag){
                amountList.add(0);
            }
        }
        result.put("time", timeList);
        result.put("amount", amountList);
        return execSuccess(result);
    }







    }
