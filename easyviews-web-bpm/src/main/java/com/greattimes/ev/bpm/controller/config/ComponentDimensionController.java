package com.greattimes.ev.bpm.controller.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.ComponentDimensionParam;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.config.IProtocolFieldService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.service.IBpmPreService;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 组件分析维度 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
@RestController
@Api(value="ComponentDimensionController",tags="维度配置模块")
@RequestMapping("/bpm/component/dimension")
@RequirePermission("analyze")
public class ComponentDimensionController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IComponentDimensionService componentDimensionService;
	@Autowired
	private IBpmPreService bpmPreService;
	@Autowired
	private IProtocolFieldService protocolFieldService;
	@Autowired
	private INoticeService noticeService;
	@Autowired
	private ConfigurationCache configurationCache;

	private static final String TABLE_FIX_BPM = "bpmPre_";
	private static final String VIEW_FIX_BPM = "v_transaction_pre_";
	private static final String VIEW_VM_BPM = "vm_transaction_pre_";
	private static String CLICKHOUSE_HOST = "clickhouse_host";
	/**
	 * 分析维度信息查询
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="分析维度信息查询", notes="分析维度信息查询" , response =ProtocolField.class ,responseContainer="List")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="componentId",value="组件id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> selectDimension(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int componentId = evUtil.getMapIntValue(map, "componentId");
		List<Map<String, Object>> list = componentDimensionService.selectDimension(componentId);
		return execSuccess(list);
	}

	/**
	 * 分析维度名称查询(正名和异名)
	 * @author NJ
	 * @date 2018/10/18 10:00
	 * @param map
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="分析维度信息查询", notes="分析维度信息查询" , response =ProtocolField.class ,responseContainer="List")
	@ApiImplicitParams({
			@ApiImplicitParam(name="componentId",value="组件id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/othername", method = RequestMethod.POST)
	public JsonResult<Object> selectDimensionNameByComponentId(@RequestBody Map<String, Object> map) {
		int componentId = evUtil.getMapIntValue(map, "componentId");
		return execSuccess(componentDimensionService.selectDimensionNameBycomponentId(componentId));
	}


	/**
	 * 分析维度信息保存
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="分析维度信息保存", notes="分析维度信息保存")
	@OperateLog(type = OperaterType.INSERT, desc = "分析维度信息保存")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> saveRelation(HttpServletRequest req, @RequestBody ComponentDimensionParam param) {
		componentDimensionService.saveComponentDimens(param);
		/*String hosts = configurationCache.getValueFromRedisThenDb(CLICKHOUSE_HOST);
		String url=configurationCache.getValueFromRedisThenDb("clickhouse_cluster_url");
		log.info("clickhouse连接的url为:{},host:{}", url,hosts);
        if(StringUtils.isBlank(hosts)){
            throw new IllegalArgumentException("参数错误！集群节点配置非法！无clickhouse_host参数");
        }
        String[] hostArr = hosts.split("\\,");
        for(int i = 0; i < hostArr.length; i++){
			creatTable(param,hostArr[i],url);
		}*/
		//sys_notice
        noticeService.insertNoticeByParam(2006,"组件分析维度保存",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	private void creatTable(ComponentDimensionParam param, String host, String url) {
		List<Map<String, Object>> fieldList=componentDimensionService.selectDimensionNameBycomponentId(param.getComponentId());
		List<String> enameList = new ArrayList<>();
		fieldList.stream().forEach(x -> {
			enameList.add(x.get("ename").toString());
		});
		//创建bpmPre_表
		int componentId = param.getComponentId();
		List<DimensionParam> dimensionParam = null;
		dimensionParam = param.getDimension();
		String table = TABLE_FIX_BPM + componentId;
		List<Integer> columnsId = dimensionParam.stream().map(DimensionParam::getFieldId).collect(Collectors.toList());
		List<ProtocolField> p=new ArrayList<>();
		if(!evUtil.listIsNullOrZero(columnsId)) {
			p = protocolFieldService.selectBatchIds(columnsId);
		}
		List<String> columns = p.stream().map(ProtocolField::getEname).collect(Collectors.toList());
		String localTable=table+"_local";
		//本地表
		boolean isExist_l = bpmPreService.isExist(localTable,host,url);
		List<String> newList=new ArrayList<>();
		if (isExist_l) {
			if (bpmPreService.compareWithTable(localTable, columns,host,url)) {
					/*try {
						// 删除小时视图
						if (bpmPreService.isExist(VIEW_FIX_BPM + componentId + "_60",host) ){
							bpmPreService.dropTable(VIEW_FIX_BPM + componentId + "_60",host);
						}*/
						try {
							// 删除视图表
							if (bpmPreService.isExist(VIEW_VM_BPM + componentId,host,url)) {
								bpmPreService.dropTable(VIEW_VM_BPM + componentId,host,url);
							}
							if (bpmPreService.isExist(VIEW_VM_BPM + componentId+"_5",host,url)) {
								bpmPreService.dropTable(VIEW_VM_BPM + componentId+"_5",host,url);
							}
							try {
								// 分离物化视图
								if (bpmPreService.isExist(VIEW_VM_BPM + componentId+"_local",host,url)) {
									newList=bpmPreService.addPerTableColumns(componentId, enameList,host,url);
									bpmPreService.detachTable(VIEW_VM_BPM + componentId+"_local",host,url);
								}
								// 分离物化视图
								if (bpmPreService.isExist(VIEW_VM_BPM + componentId+"_local_5",host,url)) {
									newList=bpmPreService.addPerTableColumns(componentId, enameList,host,url);
									bpmPreService.detachTable(VIEW_VM_BPM + componentId+"_local_5",host,url);
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								log.info("分离视图失败！ --》【" + VIEW_VM_BPM + componentId+"_local" + "】");
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							log.info("删除视图表失败！ --》【" + VIEW_VM_BPM + componentId + "】");
						}
						
					/*} catch (Exception e) {
						// TODO Auto-generated catch block
						log.info("删除视图失败！ --》【" + VIEW_FIX_BPM + componentId + "_60】");
					}*/
				
				
				bpmPreService.updatePerTable(localTable, columns, host,url);
				try {
					// 修改物化视图
					String innerTable = "`.inner."+VIEW_VM_BPM+ componentId+"_local"+"`";
					bpmPreService.updateView(innerTable, columns, host,url);
					String innerTable_5 = "`.inner."+VIEW_VM_BPM+ componentId+"_local_5"+"`";
					bpmPreService.updateView(innerTable_5, columns, host,url);
					try {
						// 重新绑定视图
						bpmPreService.AttachView(newList, host,url);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						log.info("绑定视图失败！ --》【" + VIEW_VM_BPM + componentId+"_local" + "】");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.info("修改视图失败！ --》【" + VIEW_VM_BPM + componentId +"_local"+ "】");
				}
				//创建时图表
				bpmPreService.createVmTable(componentId, columns,host,url);
				//创建小时视图
				//bpmPreService.createPerHouer(componentId, columns, 1);
			}
		} else {
			bpmPreService.addPerTable(componentId, columns, configurationCache.getValueFromRedisThenDb("clickhouse_cluster_url"));
			//创建物化视图
			bpmPreService.createPerMaterializedView(componentId, columns, url);
			//创建小时视图
			//bpmPreService.createPerHouer(componentId, columns, 1);
		}
		//集群表
		boolean isExist = bpmPreService.isExist(table,host,url);
		if (isExist) {
			if (bpmPreService.compareWithTable(table, columns,host,url)) {
				bpmPreService.updatePerTable(table, columns, host,url);
			}
		} else {
			bpmPreService.addPerTable(componentId, columns,url);
		}
	}
	
}
