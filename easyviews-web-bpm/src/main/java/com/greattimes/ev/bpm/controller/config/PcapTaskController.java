package com.greattimes.ev.bpm.controller.config;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.PcapTask;
import com.greattimes.ev.bpm.service.config.IPcapTaskService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.utils.HttpClientUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * pca下载任务 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-11-07
 */
@RestController
@RequestMapping("/bpm/pcap/task")
@Api(value="PcapTaskController",tags="pacp下载")
@RequirePermission("pcapdown")
public class PcapTaskController extends BaseController{
	@Autowired
    private IPcapTaskService pcapTaskService;
	@Autowired
	private ConfigurationCache configurationCache;
	Logger log = LoggerFactory.getLogger(this.getClass());
	@ApiOperation(value = "pacp下载列表", notes = "pacp下载列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dIp", value = "目的ip", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "size", value = "每页数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "当前页码", dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/query", method = RequestMethod.POST)
    public JsonResult<Object> selectLogs(@RequestBody JSONObject param){
        Integer pageNumber = param.getInteger("page");
        Integer pageSize= param.getInteger("size");
        Page<Map<String,Object>> page = new Page<>(pageNumber,pageSize);
        Map<String,Object> map = new HashMap<>(1);
        map.put("dIp", param.getString("dIp"));
        return execSuccess(pcapTaskService.selectPcapTaskByMap(page, map));
    }
	
	/**
	 * 新增
	 * @param req
	 * @param center
	 * @return
	 */
	@ApiOperation(value="pcap任务新建", notes="pcap任务新建")
	@OperateLog(type=OperaterType.INSERT,desc="pcap任务新建")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> save(HttpServletRequest req, @RequestBody JSONObject jsonObject) {
		User user=getSessionUser(req);
		int id=pcapTaskService.insertTask(user.getId(),jsonObject);
		//Pcap数据下载--调用远程接口
		Map<String, Object> param = new HashMap<String, Object>();
		String url = configurationCache.getValue(ConfigConstants.DECODE_HOST)+configurationCache.getValue(ConfigConstants.PCAP_DOWNLOAD_URL);
		param.put("taskId", id+"");
		param.put("centerId", jsonObject.getString("centerId"));
		param.put("startTime", jsonObject.getLongValue("startTime"));
		param.put("endTime", jsonObject.getLongValue("endTime"));
		param.put("type", jsonObject.getInteger("downType")-1);
		param.put("srcIp", jsonObject.getString("sip"));
		param.put("dstIp", jsonObject.getString("dip"));
		param.put("dstPort", jsonObject.getInteger("port"));
		Integer portType=jsonObject.getInteger("portType");
		if(portType.equals(2)) {
			portType=0;
		}
		param.put("portType", portType);
		log.info("pcap任务新建请求参数:{}",param.toString());
		String result = null;
		try {
			log.info("pcap任务新建url:{}",url);
			result = HttpClientUtil.posts(url, JSON.toJSONString(param));
		} catch (Exception e) {
			PcapTask entity=new PcapTask();
			entity.setId(id);
			entity.setState(3);
			entity.setError("Pcap数据下载--调用远程接口发生异常!");
			pcapTaskService.updateById(entity);
			log.error("Pcap数据下载--调用远程接口发生异常：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		log.info("pcap任务新建响应参数:{}",result.toString());
		if (!StringUtils.isBlank(result)) {
			JSONObject jb = null;
			try {
				jb = JSON.parseObject(result);
			} catch (Exception e) {
				PcapTask entity=new PcapTask();
				entity.setId(id);
				entity.setState(3);
				entity.setError("Pcap数据下载--调用远程接口发生异常!返回结果转换异常");
				pcapTaskService.updateById(entity);
				log.error("Pcap数据下载--调用远程接口发生异常：" + e.getMessage());
				throw new RuntimeException(e.getMessage());
			}
			String returnCode = jb.getString("responseCode");
			String returnMessage = jb.getString("responseDesc");
			if (!"200".equals(returnCode)) {
				PcapTask entity = new PcapTask();
				entity.setId(id);
				entity.setState(3);
				entity.setError(returnMessage);
				pcapTaskService.updateById(entity);
			} else {
				PcapTask entity = new PcapTask();
				entity.setId(id);
				entity.setState(1);
				pcapTaskService.updateById(entity);
			}
		}else {
			PcapTask entity = new PcapTask();
			entity.setId(id);
			entity.setState(3);
			entity.setError("调用"+url+"无返回结果！");
			pcapTaskService.updateById(entity);
		}
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	
	/**
	 * 删除
	 */
	@ApiOperation(value="删除", notes="删除")
	@OperateLog(type=OperaterType.DELETE,desc="pcap任务删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody PcapTask pcapTask) {
		Integer id = pcapTask.getId();
		pcapTaskService.deleteById(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
	
	/**
	 * 取消
	 */
	@ApiOperation(value="取消", notes="取消")
	@OperateLog(type=OperaterType.UPDATE ,desc="pcap任务取消")
	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	public JsonResult<Object> cancel(@RequestBody PcapTask pcapTask) {
		int id=pcapTask.getId();
		Map<String, Object> params=new HashMap<>();
		params.put("taskId", id+"");
		log.info("请求参数:{}",params.toString());
		String url = configurationCache.getValue(ConfigConstants.DECODE_HOST)+configurationCache.getValue(ConfigConstants.PCAP_DOWNLOAD_CANCEL_URL);
		String result = null;
		try {
			Map<String, Object> headers=new HashMap<>();
			headers.put("Content-type", "application/json; charset=utf-8");
			headers.put("Accept", "application/json;charset=utf-8");
			//result = HttpClientUtil.posts(url, JSON.toJSONString(param));
			log.info("pcap任务取消url:{}",url);
			result = HttpClientUtil.httpGetRequest(url, headers, params);
		} catch (Exception e) {
			PcapTask entity=new PcapTask();
			entity.setId(id);
			entity.setState(3);
			entity.setError("Pcap数据下载取消--调用远程接口发生异常");
			pcapTaskService.updateById(entity);
			log.error("Pcap数据下载取消--调用远程接口发生异常：" + e.getMessage());
			throw new RuntimeException(e.getMessage());
		}
		log.info("响应参数:{}",result);
		if (!StringUtils.isBlank(result)) {
			JSONObject jb = null;
			try {
				jb = JSON.parseObject(result);
			} catch (Exception e) {
				PcapTask entity=new PcapTask();
				entity.setId(id);
				entity.setState(3);
				entity.setError("Pcap数据下载取消--调用远程接口发生异常！返回结果转换异常");
				pcapTaskService.updateById(entity);
				log.error("Pcap数据下载取消--调用远程接口发生异常：" + e.getMessage());
				throw new RuntimeException(e.getMessage());
			}
			String returnCode = jb.getString("responseCode");
			String returnMessage = jb.getString("responseDesc");
			if (!"200".equals(returnCode)) {
				PcapTask entity = new PcapTask();
				entity.setId(id);
				entity.setState(3);
				entity.setError(returnMessage);
				pcapTaskService.updateById(entity);
			} else {
				PcapTask entity = new PcapTask();
				entity.setId(id);
				entity.setState(4);
				pcapTaskService.updateById(entity);
			}
		}else {
			PcapTask entity = new PcapTask();
			entity.setId(id);
			entity.setState(3);
			entity.setError("调用"+url+"无返回结果！");
			pcapTaskService.updateById(entity);
		}
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}
	
	@ApiOperation(value="查询未完成任务", notes="查询未完成任务")
	@RequestMapping(value = "/result", method = RequestMethod.POST)
	public JsonResult<Object> selectCenter() {
		List<Integer> states=new ArrayList<>(2);
		states.add(1);
		states.add(0);
		EntityWrapper<PcapTask> wrapper=new EntityWrapper<>();
		wrapper.in("state", states);
		List<PcapTask> pcapList=pcapTaskService.selectList(wrapper);
		List<Integer> ids = pcapList.stream().map(PcapTask::getId).collect(Collectors.toList());
		return execSuccess(ids);
	}
	
	@ApiOperation(value="dIP校验", notes="dIP校验")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "ip", value = "目的ip", dataType = "String", paramType = "query"),
        @ApiImplicitParam(name = "port", value = "目的port", dataType = "String", paramType = "query")
	})
	@RequestMapping(value = "/ip/check", method = RequestMethod.POST)
	public JsonResult<Object> checkIp(@RequestBody JSONObject jsonObject) {
		String ip=jsonObject.getString("ip");
		String port=jsonObject.getString("port");
		Integer centerId=jsonObject.getInteger("centerId");
		if(null==ip||null==centerId) {
			return execCheckError("错误的参数类型：ip["+ip+"],centerId["+centerId+"]");
		}
		boolean flag=pcapTaskService.checkIp(ip,centerId,port);
		return execSuccess(flag);
	}

	@ApiOperation(value="错误信息查询", notes="错误信息查询")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "任务id", dataType = "int", paramType = "query")
	})
	@RequestMapping(value = "/fail/reason", method = RequestMethod.POST)
	public JsonResult<Object> getMessage(@RequestBody JSONObject jsonObject) {
		int id=jsonObject.getIntValue("id");
		String message=pcapTaskService.selectById(id).getError();
		if(StringUtils.isBlank(message)) {
			return execCheckError("当前任务无失败信息！");
		}else
			return execSuccess(message,"SELECT SUCCESS");
	}
	
	@ApiOperation(value="下载", notes="下载")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "任务id", dataType = "int", paramType = "query")
	})
	@RequestMapping(value = "/download", method = RequestMethod.POST)
	public JsonResult<Object> download(HttpServletResponse resp,@RequestBody JSONObject jsonObject) {
		int id=jsonObject.getIntValue("id");
		String href=pcapTaskService.selectById(id).getHref();
		
		// 将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName="+id+".zip");
		OutputStream os = null;
		
		try {
			// 查出需要下载的真实数据
			FileInputStream in =new FileInputStream(new File(href));
			//当文件没有结束时，每次读取一个字节显示
            byte[] data=new byte[in.available()];
            in.read(data);
			os = resp.getOutputStream();
			os.write(data);
			os.close();
			in.close();
		} catch (IOException e) {
			log.info("文件下载异常！IOException");
			e.printStackTrace();
			return execError("文件下载异常！", e.getMessage());
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}
	/**下载完成回调（解码请求web）
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "下载完成回调（解码请求web）", notes = "下载完成回调（解码请求web）")
	@OperateLog(type=OperaterType.UPDATE,desc="下载完成回调")
    @RequestMapping(value = "/callback", method = RequestMethod.POST)
    public Map<String, Object> callBack(@RequestBody JSONObject param){
		log.info("开始回调---参数:{}",param.toString());
        Integer responseCode = param.getInteger("responseCode");
        Integer taskid= param.getInteger("taskId");
        String responseDesc= param.getString("responseDesc");
        String fileName= param.getString("fileName");
        PcapTask task=new PcapTask();
        task.setId(taskid);
        
        if(responseCode.equals(200)) {
        	task.setState(2);
        	task.setHref(fileName);
        }
        else {
        	task.setState(3);
        	task.setError(responseDesc);
        }
        pcapTaskService.updateById(task);
        Map<String, Object> result = new HashMap<>();
        result.put("responseCode", 200);
        return result;
    }
}

