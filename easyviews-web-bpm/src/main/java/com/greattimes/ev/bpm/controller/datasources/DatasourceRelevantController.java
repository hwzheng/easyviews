package com.greattimes.ev.bpm.controller.datasources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.datasources.param.req.DatasourceRelevantParam;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;
import com.greattimes.ev.bpm.entity.IndicatorContraint;
import com.greattimes.ev.bpm.entity.RelevantContraint;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.datasources.IDatasourceRelevantService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 数据源配置表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
@RestController
@Api(value = "DatasourceRelevantController", tags = "外部数据配置")
@RequestMapping("/bpm/relevantdata")
@RequirePermission("externalData")
public class DatasourceRelevantController extends BaseController {
	@Autowired
	private IDatasourceRelevantService datasourceRelevantService;
	@Autowired
	private IComponentService componentService;
	@Autowired
	private INoticeService noticeService;
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	/**
	 * 数据源配置新增
	 * 
	 * @param req
	 * @param relevant
	 * @return
	 */
	@ApiOperation(value = "新增", notes = "新增")
	@OperateLog(type = OperaterType.INSERT, desc = "外部数据配置新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> add(HttpServletRequest req, @RequestBody @Validated DatasourceRelevantParam relevant) {
		User user = super.getSessionUser(req);
		int userId = user.getId();
		datasourceRelevantService.add(userId, relevant);
		//sys_notice
        noticeService.insertNoticeByParam(2101,"外部数据源新建",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 外部数据配置查询
	 * 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "外部数据配置查询", notes = "外部数据配置查询", response = DatasourceRelevantParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> select(@RequestBody Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		List<DatasourceRelevantParam> list = datasourceRelevantService.select(applicationId);
		return execSuccess(list);
	}

	/**
	 * 外部数据配置删除
	 * 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "外部数据配置删除", notes = "外部数据配置删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.DELETE, desc = "外部数据配置删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		datasourceRelevantService.delete(id);
		//sys_notice
        noticeService.insertNoticeByParam(2103,"外部数据源删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 外部数据配置修改
	 * 
	 * @param req
	 * @param relevant
	 * @return
	 */
	@ApiOperation(value = "外部数据配置编辑", notes = "外部数据配置编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "外部数据配置修改")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(HttpServletRequest req, @RequestBody @Validated DatasourceRelevantParam relevant) {
		if (null == relevant.getId()) {
			return execCheckError("外部数据配置id为空！");
		}
		User user = super.getSessionUser(req);
		int userId = user.getId();
		datasourceRelevantService.edit(relevant, userId);
		//sys_notice
        noticeService.insertNoticeByParam(2102,"外部数据源编辑",0,0);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 外部数据配置下载
	 * 
	 * @param req
	 * @param resp
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	@ApiOperation(value = "已关联下载", notes = "已关联下载")
	@RequestMapping(value = "/downloadrelevant/{id}", method = RequestMethod.GET)
	public JsonResult<Object> download(HttpServletRequest req, HttpServletResponse resp,
			@ApiParam(required = true, name = "id", value = "配置id") @PathVariable int id) {
		DatasourceRelevant datasourceRelevant = datasourceRelevantService.getDatasource(id);
		// 获取全部关联约束
		List<RelevantContraint> alllist = datasourceRelevantService.slectRelevant(datasourceRelevant.getComponentId(),
				datasourceRelevant.getIndicatorGroupId());
		// 获取为关联数据
		List<Map<String, Object>> list1 = new ArrayList<>();
		List<DataSourcesRelationParam> ips = componentService.selectByComponentId(datasourceRelevant.getComponentId());
		List<DataSourcesRelationParam> ipAndPort = componentService
				.selectIpAndport(datasourceRelevant.getComponentId());
		// 第一步，创建一个webbook，对应一个Excel文件
		XSSFWorkbook wb = new XSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		XSSFSheet sheet = wb.createSheet("已关联数据");
		XSSFSheet sheet2 = wb.createSheet("未关联数据");
		// 第三步，在sheet中添加表头第0行
		XSSFRow row = sheet.createRow((int) 0);
		XSSFRow row2 = sheet2.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		Integer field = datasourceRelevant.getRelevantField();
		Integer mount = datasourceRelevant.getMount();
		String name;
		XSSFCell cell, cell2;
		if (null == field) {
			name = setTitle(mount);
			switch (mount) {
			case 0:
				// 第一个sheet页
				cell = row.createCell((short) 0);
				cell.setCellValue(name);
				cell.setCellStyle(style);
				cell = row.createCell((short) 1);
				cell.setCellValue("value");
				cell.setCellStyle(style);
				// 第五步，写入实体数据
				for (int i = 0; i < alllist.size(); i++) {
					row = sheet.createRow((int) i + 1);
					RelevantContraint detail = alllist.get(i);
					// 创建单元格，并设置值
					row.createCell((short) 0).setCellValue(detail.getComponentId());
					row.createCell((short) 1).setCellValue(detail.getValue());
				}
				// 第二个sheet页
				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);

				if (evUtil.listIsNullOrZero(alllist)) {
					row2 = sheet2.createRow((short) 1);
					row2.createCell((short) 0).setCellValue(datasourceRelevant.getComponentId());
				}
				break;
			case 1:
				// 第一个sheet页
				cell = row.createCell((short) 0);
				cell.setCellValue(name);
				cell.setCellStyle(style);
				cell = row.createCell((short) 1);
				cell.setCellValue("value");
				cell.setCellStyle(style);
				// 第五步，写入实体数据
				for (int i = 0; i < alllist.size(); i++) {
					row = sheet.createRow((int) i + 1);
					RelevantContraint detail = alllist.get(i);
					// 创建单元格，并设置值
					row.createCell((short) 0).setCellValue(detail.getIp());
					row.createCell((short) 1).setCellValue(detail.getValue());
				}
				// 第二个sheet页
				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, alllist, ips, datasourceRelevant.getComponentId(),1);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
				}
				break;
			case 2:
				// 第一个sheet页
				cell = row.createCell((short) 0);
				cell.setCellValue(name);
				cell.setCellStyle(style);
				cell = row.createCell((short) 1);
				cell.setCellValue("value");
				cell.setCellStyle(style);
				// 第五步，写入实体数据
				for (int i = 0; i < alllist.size(); i++) {
					row = sheet.createRow((int) i + 1);
					RelevantContraint detail = alllist.get(i);
					// 创建单元格，并设置值
					row.createCell((short) 0).setCellValue(detail.getIp() + ":" + detail.getPort());
					row.createCell((short) 1).setCellValue(detail.getValue());
				}

				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 2);
				cell2.setCellValue("port");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, alllist, ipAndPort, datasourceRelevant.getComponentId(),2);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
					row2.createCell((short) 2).setCellValue(detail.get("port").toString());
				}
				break;
			default:
				break;
			}

		} else {
			name = setTitle(field);
			switch (field) {
			case 1:
				// 第一个sheet页
				cell = row.createCell((short) 0);
				cell.setCellValue(name);
				cell.setCellStyle(style);
				cell = row.createCell((short) 1);
				cell.setCellValue("value");
				cell.setCellStyle(style);
				// 第五步，写入实体数据
				for (int i = 0; i < alllist.size(); i++) {
					row = sheet.createRow((int) i + 1);
					RelevantContraint detail = alllist.get(i);
					// 创建单元格，并设置值
					row.createCell((short) 0).setCellValue(detail.getIp());
					row.createCell((short) 1).setCellValue(detail.getValue());
				}
				// 第二个sheet页
				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, alllist, ips, datasourceRelevant.getComponentId(),1);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
				}
				break;
			case 2:
				// 第一个sheet页
				cell = row.createCell((short) 0);
				cell.setCellValue(name);
				cell.setCellStyle(style);
				cell = row.createCell((short) 1);
				cell.setCellValue("value");
				cell.setCellStyle(style);
				// 第五步，写入实体数据
				for (int i = 0; i < alllist.size(); i++) {
					row = sheet.createRow((int) i + 1);
					RelevantContraint detail = alllist.get(i);
					// 创建单元格，并设置值
					row.createCell((short) 0).setCellValue(detail.getIp() + ":" + detail.getPort());
					row.createCell((short) 1).setCellValue(detail.getValue());
				}

				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 2);
				cell2.setCellValue("port");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, alllist, ipAndPort, datasourceRelevant.getComponentId(),2);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
					row2.createCell((short) 2).setCellValue(detail.get("port").toString());
				}
				break;
			default:
				break;
			}
		}
		// 第六步，将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=relevant.xlsx");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			wb.write(os);
			// 这里主要关闭。
			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}

	/**
	 * 设置表格头值
	 * 
	 * @param mount
	 * @return
	 */
	private String setTitle(Integer value) {
		String name = null;
		switch (value) {
		case 0:
			name = "组件";
			break;
		case 1:
			name = "ip";
			break;
		case 2:
			name = "ip:port";
			break;
		default:
			break;
		}
		return name;

	}

	/**
	 * 获取未关联数据
	 * 
	 * @param list1
	 *            未关联
	 * @param alllist
	 *            关联
	 * @param ips
	 *            全部ip或IP：port
	 * @param component
	 *            组件id
	 */
	public void handelList(List<Map<String, Object>> list1, List<RelevantContraint> alllist,
			List<DataSourcesRelationParam> ips, Integer component, int field) {
		Boolean flag = false;
		if (field == 1) {
			for (DataSourcesRelationParam ip : ips) {
				for (RelevantContraint re : alllist) {
					flag = false;
					if (ip.getIp().equals(re.getIp())) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					Map<String, Object> col = new HashMap<>();
					col.put("ip", ip.getIp());
					col.put("port", ip.getPort());
					col.put("component", component);
					list1.add(col);
				}
			}
		} else {
			for (DataSourcesRelationParam ip : ips) {
				for (RelevantContraint re : alllist) {
					flag = false;
					if (ip.getIp().equals(re.getIp())&&ip.getPort().intValue()==re.getPort().intValue()) {
						flag = true;
						break;
					}
				}
				if (!flag) {
					Map<String, Object> col = new HashMap<>();
					col.put("ip", ip.getIp());
					col.put("port", ip.getPort());
					col.put("component", component);
					list1.add(col);
				}
			}
		}
	}

	/**
	 * 未关联下载
	 * 
	 * @param req
	 * @param resp
	 * @param id
	 * @return
	 */
	@ApiOperation(value = "未关联下载", notes = "未关联下载")
	@RequestMapping(value = "/downloadindicator/{id}", method = RequestMethod.GET)
	public JsonResult<Object> downloadAll(HttpServletRequest req, HttpServletResponse resp,
			@ApiParam(required = true, name = "id", value = "配置id") @PathVariable int id) {
		DatasourceRelevant datasourceRelevant = datasourceRelevantService.getDatasource(id);
		// 获取未关联约束
		List<IndicatorContraint> alllist = datasourceRelevantService
				.selectIndicator(datasourceRelevant.getIndicatorGroupId(), 0);
		// 获取全部关联约束
		List<RelevantContraint> all = datasourceRelevantService.slectRelevant(datasourceRelevant.getComponentId(),
				datasourceRelevant.getIndicatorGroupId());
		// 获取为关联数据
		List<Map<String, Object>> list1 = new ArrayList<>();
		List<DataSourcesRelationParam> ips = componentService.selectByComponentId(datasourceRelevant.getComponentId());
		List<DataSourcesRelationParam> ipAndPort = componentService
				.selectIpAndport(datasourceRelevant.getComponentId());
		// 第一步，创建一个webbook，对应一个Excel文件
		XSSFWorkbook wb = new XSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		XSSFSheet sheet = wb.createSheet("未关联数据");
		XSSFSheet sheet2 = wb.createSheet("组件(ip、port)");
		// 第三步，在sheet中添加表头第0行
		XSSFRow row = sheet.createRow((int) 0);
		XSSFRow row2 = sheet2.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		Integer field = datasourceRelevant.getRelevantField();
		Integer mount = datasourceRelevant.getMount();
		String name;
		XSSFCell cell, cell2;
		if (null == field) {
			name = setTitle(mount);
		} else {
			name = setTitle(field);
		}
		// 第一个sheet页
		cell = row.createCell((short) 0);
		cell.setCellValue(name);
		cell.setCellStyle(style);
		cell = row.createCell((short) 1);
		cell.setCellValue("value");
		cell.setCellStyle(style);
		// 第五步，写入实体数据
		for (int i = 0; i < alllist.size(); i++) {
			row = sheet.createRow((int) i + 1);
			IndicatorContraint detail = alllist.get(i);
			// 创建单元格，并设置值
			row.createCell((short) 0).setCellValue("");
			row.createCell((short) 1).setCellValue(detail.getValue());
		}
		if (null == field) {
			switch (mount) {
			case 0:
				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				row2 = sheet2.createRow((short) 1);
				if(!evUtil.listIsNullOrZero(alllist)) {
					row2.createCell((short) 0).setCellValue(datasourceRelevant.getComponentId());
				}
				break;
			case 1:
				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, all, ips, datasourceRelevant.getComponentId(),1);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
				}
				break;
			case 2:

				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 2);
				cell2.setCellValue("port");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, all, ipAndPort, datasourceRelevant.getComponentId(),2);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
					row2.createCell((short) 2).setCellValue(detail.get("port").toString());
				}
				break;
			default:
				break;
			}

		} else {
			name = setTitle(field);
			switch (field) {
			case 1:
				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, all, ips, datasourceRelevant.getComponentId(),1);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
				}
				break;
			case 2:

				cell2 = row2.createCell((short) 0);
				cell2.setCellValue("组件");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 1);
				cell2.setCellValue("ip");
				cell2.setCellStyle(style);
				cell2 = row2.createCell((short) 2);
				cell2.setCellValue("port");
				cell2.setCellStyle(style);

				// 第五步，写入实体数据
				handelList(list1, all, ipAndPort, datasourceRelevant.getComponentId(),2);
				for (int i = 0; i < list1.size(); i++) {
					row2 = sheet2.createRow((int) i + 1);
					Map<String, Object> detail = list1.get(i);
					// 创建单元格，并设置值
					row2.createCell((short) 0).setCellValue(detail.get("component").toString());
					row2.createCell((short) 1).setCellValue(detail.get("ip").toString());
					row2.createCell((short) 2).setCellValue(detail.get("port").toString());
				}
				break;
			default:
				break;
			}
		}
		// 第六步，将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=relevant.xlsx");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			wb.write(os);
			// 这里主要关闭。
			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}

	/**
	 * 外部数据配置上传
	 * 
	 * @param file
	 * @param req
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "外部数据配置上传", notes = "外部数据配置上传")
	@OperateLog(type = OperaterType.INSERT, desc = "外部数据配置上传")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<Object> upload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req, DatasourceRelevant relevant) throws IOException {
		int id = relevant.getId();
		DatasourceRelevant re = datasourceRelevantService.getDatasource(id);
		List<RelevantContraint> details = new ArrayList<>();
		String fileName = file.getOriginalFilename();
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		if (!"xlsx".equals(fieldType)) {
			return execCheckError("文件上传失败！文件类型错误");
		}
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}
		InputStream in = file.getInputStream();
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(in);
		XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
		if (xssfSheet == null) {
			xssfWorkbook.close();
			return execCheckError("文件解析失败！请确认是否是下载的文件类型");
		}
		int componentId = re.getComponentId();
		int indicatorGroupId = re.getIndicatorGroupId();
		Integer filed = re.getRelevantField();
		int datasourseId = re.getDataSourceId();
		if (filed != null) {
			switch (filed) {
			case 1:
				for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
					XSSFRow xssfRow = xssfSheet.getRow(i);
					if (xssfRow != null) {
						RelevantContraint detail = new RelevantContraint();
						XSSFCell relate = xssfRow.getCell(0);
						XSSFCell value = xssfRow.getCell(1);
						String valueStr = "";
						String relateStr = "";

						if (value == null && relate == null) {
							continue;
						} else if (relate == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行关联关系为空");
						} else if (value == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行value为空");
						}

						if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) value.getNumericCellValue();
							valueStr = n + "";
						} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							valueStr = value.getStringCellValue();
						}

						if (relate.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) relate.getNumericCellValue();
							relateStr = n + "";
						} else if (relate.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							relateStr = relate.getStringCellValue();
						}
						detail.setComponentId(componentId);
						detail.setIndicatorGroupId(indicatorGroupId);
						detail.setValue(valueStr);
						detail.setIp(relateStr);
						detail.setPort(-1);
						details.add(detail);
					}
				}
				break;
			case 2:
				for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
					XSSFRow xssfRow = xssfSheet.getRow(i);
					if (xssfRow != null) {
						RelevantContraint detail = new RelevantContraint();
						XSSFCell relate = xssfRow.getCell(0);
						XSSFCell value = xssfRow.getCell(1);
						String valueStr = "";
						String relateStr = "";

						if (value == null && relate == null) {
							continue;
						} else if (relate == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行关联关系为空");
						} else if (value == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行value为空");
						}

						if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) value.getNumericCellValue();
							valueStr = n + "";
						} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							valueStr = value.getStringCellValue();
						}

						if (relate.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) relate.getNumericCellValue();
							relateStr = n + "";
						} else if (relate.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							relateStr = relate.getStringCellValue();
						}
						detail.setComponentId(componentId);
						detail.setIndicatorGroupId(indicatorGroupId);
						detail.setValue(valueStr);
						String arr[] = relateStr.trim().split(":");
						if(arr.length<2) {
							logger.debug("上传文件ip port未使用':'隔开");
						}
						detail.setIp(arr[0]);
						detail.setPort(Integer.parseInt(arr[1]));
						details.add(detail);
					}
				}
				break;
			default:
				break;
			}

		} else {
			int mount = re.getMount();
			switch (mount) {
			case 0:
				for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
					XSSFRow xssfRow = xssfSheet.getRow(i);
					if (xssfRow != null) {
						RelevantContraint detail = new RelevantContraint();
						XSSFCell relate = xssfRow.getCell(0);
						XSSFCell value = xssfRow.getCell(1);
						String valueStr = "";
						String relateStr = "";

						if (value == null && relate == null) {
							continue;
						} else if (relate == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行关联关系为空");
						} else if (value == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行value为空");
						}

						if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) value.getNumericCellValue();
							valueStr = n + "";
						} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							valueStr = value.getStringCellValue();
						}
						if (relate.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) relate.getNumericCellValue();
							relateStr = n + "";
						} else if (relate.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							relateStr = relate.getStringCellValue();
						}
						detail.setComponentId(Integer.parseInt(relateStr));
						detail.setIndicatorGroupId(indicatorGroupId);
						detail.setValue(valueStr);
						detail.setIp("-1");
						detail.setPort(-1);
						details.add(detail);
					}
				}
				break;
			case 1:
				for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
					XSSFRow xssfRow = xssfSheet.getRow(i);
					if (xssfRow != null) {
						RelevantContraint detail = new RelevantContraint();
						XSSFCell relate = xssfRow.getCell(0);
						XSSFCell value = xssfRow.getCell(1);
						String valueStr = "";
						String relateStr = "";

						if (value == null && relate == null) {
							continue;
						} else if (relate == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行关联关系为空");
						} else if (value == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行value为空");
						}

						if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) value.getNumericCellValue();
							valueStr = n + "";
						} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							valueStr = value.getStringCellValue();
						}

						if (relate.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) relate.getNumericCellValue();
							relateStr = n + "";
						} else if (relate.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							relateStr = relate.getStringCellValue();
						}
						detail.setComponentId(componentId);
						detail.setIndicatorGroupId(indicatorGroupId);
						detail.setValue(valueStr);
						detail.setIp(relateStr);
						detail.setPort(-1);
						details.add(detail);
					}
				}
				break;
			case 2:
				for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
					XSSFRow xssfRow = xssfSheet.getRow(i);
					if (xssfRow != null) {
						RelevantContraint detail = new RelevantContraint();
						XSSFCell relate = xssfRow.getCell(0);
						XSSFCell value = xssfRow.getCell(1);
						String valueStr = "";
						String relateStr = "";

						if (value == null && relate == null) {
							continue;
						} else if (relate == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行关联关系为空");
						} else if (value == null) {
							xssfWorkbook.close();
							return execCheckError("第" + i + "行value为空");
						}

						if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) value.getNumericCellValue();
							valueStr = n + "";
						} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							valueStr = value.getStringCellValue();
						}

						if (relate.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							long n = (long) relate.getNumericCellValue();
							relateStr = n + "";
						} else if (relate.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							relateStr = relate.getStringCellValue();
						}
						detail.setComponentId(componentId);
						detail.setIndicatorGroupId(indicatorGroupId);
						detail.setValue(valueStr);
						String arr[] = relateStr.trim().split(":");
						if(arr.length<2) {
							logger.debug("上传文件ip port未使用':'隔开");
						}
						detail.setIp(arr[0]);
						detail.setPort(Integer.parseInt(arr[1]));
						details.add(detail);
					}
				}
				break;
			default:
				break;
			}
		}
		if (details.size() > 0) {
			datasourceRelevantService.insert(details, datasourseId);
			//sys_notice
	        noticeService.insertNoticeByParam(2104,"外部数据源上传",0,0);
		}
		xssfWorkbook.close();
		return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
	}

	/**
	 * 是否展示维度
	 * 
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "是否展示维度", notes = "是否展示维度")
	@ApiImplicitParams({ @ApiImplicitParam(name = "showDimension", value = "激活状态", dataType = "int"),
			@ApiImplicitParam(name = "id", value = "应用id", dataType = "int") })
	@OperateLog(type = OperaterType.UPDATE, desc = "是否展示维度")
	@RequestMapping(value = "/showdimension", method = RequestMethod.POST)
	public JsonResult<Object> active(@RequestBody JSONObject param) {
		datasourceRelevantService.updateActive(param.getIntValue("id"), param.getIntValue("showDimension"));
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}

}
