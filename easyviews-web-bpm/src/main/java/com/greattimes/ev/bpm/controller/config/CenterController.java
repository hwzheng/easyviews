package com.greattimes.ev.bpm.controller.config;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.Center;
import com.greattimes.ev.bpm.service.config.ICenterService;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 中心表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-22
 */
@RestController
@Api(value="CenterController",tags="中心管理模块")
@RequestMapping("/bpm/center")
@RequirePermission("center")
public class CenterController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ICenterService iCenterService;


	/**
	 * 查询
	 * @author NJ
	 * @date 2018/9/10 16:08
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@ApiOperation(value="查询", notes="查询", response = Center.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> selectCenter(@RequestBody JSONObject jsonObject) {
		return execSuccess(iCenterService.getCenters(jsonObject.getIntValue("active")));
	}
	
	/**
	 * 详情
	 * @return
	 */
	@ApiOperation(value="详情", notes="详情", response = Center.class, responseContainer = "List")
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectCenterDetail(@RequestBody Center center) {
		int id=center.getId();
		return execSuccess(iCenterService.selectById(id));
	}

	/**
	 * 新增
	 * @param req
	 * @param center
	 * @return
	 */
	/*@ApiOperation(value="新增", notes="新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> saveCenter(HttpServletRequest req, @RequestBody Center center) {
		Boolean flag = iCenterService.checkCenterName(center, 1);
		if (flag) {
			iCenterService.save(center);
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		} else {
			log.error("中心名重复");
			return execCheckError("中心名不能重复");
		}
	}*/

	/**
	 * 编辑
	 */
	@ApiOperation(value="编辑", notes="编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> editCenter(HttpServletRequest req, @RequestBody @Validated Center center) {
		if(null==center.getId()) {
			return execCheckError("中心id为空！");
		}
		Boolean flag = iCenterService.checkCenterName(center, 0);
		if (flag) {
			iCenterService.update(center);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		} else {
			log.error("中心名重复");
			return execCheckError("中心名不能重复");
		}
	}

	/**
	 * 删除
	 */
	@ApiOperation(value="删除", notes="删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteCenter(HttpServletRequest req, @RequestBody Center center) {
		if(null==center.getId()) {
			return execCheckError("id为空！");
		}
		Integer id = center.getId();
		iCenterService.delete(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 激活状态改变
	 * @author NJ
	 * @date 2018/9/10 15:51
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="激活状态改变", notes="激活状态改变")
	@RequestMapping(value = "/active", method = RequestMethod.POST)
	public JsonResult<Object> activeCenter(@RequestBody JSONObject jsonObject) {
		iCenterService.updateCenterActive(jsonObject.getIntValue("id"),jsonObject.getIntValue("active"));
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}

	/**
	 * 组件下拉-采集口信息查询
	 */
	@RequirePermission("common")
	@ApiOperation(value="组件下拉-采集口信息查询", notes="组件下拉-采集口信息查询")
	@RequestMapping(value = "/probeinfo", method = RequestMethod.POST)
	public JsonResult<Object> getProbeInfo(@RequestBody Center center) {
		Integer centerId = center.getId();
		return execSuccess(iCenterService.getProbeInfo(centerId));
	}
}
