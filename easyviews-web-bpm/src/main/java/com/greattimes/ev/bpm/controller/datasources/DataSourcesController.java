package com.greattimes.ev.bpm.controller.datasources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.IndicatorContraint;
import com.greattimes.ev.bpm.entity.Indicators;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import io.swagger.annotations.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.datasources.param.req.ComponentIndicatorsParam;
import com.greattimes.ev.bpm.datasources.param.req.IndicatorGroupParam;
import com.greattimes.ev.bpm.entity.DataSources;
import com.greattimes.ev.bpm.entity.Dscomponent;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.datasources.IDataSourcesService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 * 数据源配置表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@RestController
@Api(value = "DataSourcesController", tags = "数据源配置")
@RequestMapping("/bpm/datasource")
@RequirePermission("datasource")
public class DataSourcesController extends BaseController {
	@Autowired
	private IDataSourcesService dataSourcesService;
	@Autowired
	private IDeleteRuleService deleteRuleService;
	@Autowired
	private INoticeService noticeService;

	@RequirePermission("common")
	@ApiOperation(value="列表查询", notes="列表查询")
	@RequestMapping(value = "/select", method =  RequestMethod.GET)
	public JsonResult<Object> select(){
		return execSuccess(dataSourcesService.select());
	}

	@ApiOperation(value="激活状态修改", notes="激活状态修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "激活状态修改")
	@RequestMapping(value = "/active", method = RequestMethod.POST)
	public JsonResult<Object> active(@RequestBody JSONObject param){
		DataSources dataSources = new DataSources();
		dataSources.setActive(param.getIntValue("active"));
		dataSources.setId(param.getIntValue("id"));
		if(dataSourcesService.updateById(dataSources)){
			//sys_notice
			noticeService.insertNoticeByParam(3405,"数据源激活状态",0,0);
			return execSuccess(Constant.STATE_SUCCESS_MSG);
		}else{
			return execCheckError(Constant.STATE_ERROR_MSG);
		}
	}

	@ApiOperation(value="同步状态修改", notes="同步状态修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="syncType",value="同步状态",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "同步状态修改")
	@RequestMapping(value = "/sync", method = RequestMethod.POST)
	public JsonResult<Object> sync(@RequestBody JSONObject param){
		DataSources dataSources = new DataSources();
		dataSources.setSyncType(param.getIntValue("syncType"));
		dataSources.setId(param.getIntValue("id"));
		if(dataSourcesService.updateById(dataSources)){
			//sys_notice
			noticeService.insertNoticeByParam(3404,"数据源同步状态",0,0);
			return execSuccess(Constant.STATE_SUCCESS_MSG);
		}else{
			return execCheckError(Constant.STATE_ERROR_MSG);
		}
	}

	@ApiOperation(value="数据源删除", notes="数据源删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "数据源删除")
	@RequestMapping(value="/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody JSONObject param){
		int id=param.getIntValue("id");
		dataSourcesService.deleteDataSourceById(id);
		//删除外部数据源配置
		deleteRuleService.deleteDataSource(id);

		//sys_notice
		noticeService.insertNoticeByParam(3403,"数据源删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	@ApiOperation(value="数据源增加", notes="数据源增加")
	@ApiImplicitParams({
			@ApiImplicitParam(name="name",value="数据源名称",dataType="string", paramType = "query"),
			@ApiImplicitParam(name="type",value="数据源类型",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="desc",value="描述",dataType="string", paramType = "query")
	})
	@OperateLog(type = OperaterType.INSERT, desc = "数据源增加")
	@RequestMapping(value="/add", method = RequestMethod.POST)
	public JsonResult<Object> save(HttpServletRequest request, @RequestBody JSONObject param){
		String name = param.getString("name");
		Integer type = param.getInteger("type");
		String desc = param.getString("desc");
		if(StringUtils.isBlank(name) || type == null ){
			return execCheckError(Constant.SAVE_ERROR_MSG);
		}
		if(dataSourcesService.isHasSameDataSourceName(name, 0)){
			return execCheckError("数据源名称不能重复！");
		}
		User user = super.getSessionUser(request);
		DataSources  dataSources = new DataSources();
		dataSources.setName(name);
		dataSources.setType(type);
		dataSources.setDesc(desc);
		DataSources result = dataSourcesService.saveOrUpdate(dataSources, user.getId());
		if(result.getId() == null){
			return execCheckError(Constant.SAVE_ERROR_MSG) ;
		}else{
			//sys_notice
			noticeService.insertNoticeByParam(3401,"数据源新建",0,0);
			Map<String, Object> map = new HashMap<>(1);
			map.put("id", result.getId());
			return execSuccess(map, Constant.SAVE_SUCCESS_MSG);
		}
	}

	@ApiOperation(value="数据源修改", notes="数据源修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="name",value="数据源名称",dataType="string", paramType = "query"),
			@ApiImplicitParam(name="type",value="数据源类型",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="desc",value="描述",dataType="string", paramType = "query")
	})
	@OperateLog(type = OperaterType.UPDATE, desc = "数据源修改")
	@RequestMapping(value="/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(HttpServletRequest request, @RequestBody JSONObject param){
		String name = param.getString("name");
		Integer type = param.getInteger("type");
		String desc = param.getString("desc");
		Integer id = param.getIntValue("id");
		if(StringUtils.isBlank(name) || type == null ){
			return execCheckError(Constant.SAVE_ERROR_MSG);
		}
		if(dataSourcesService.isHasSameDataSourceName(name, id)){
			return execCheckError("数据源名称不能重复！");
		}
		User user = super.getSessionUser(request);
		DataSources  dataSources = new DataSources();
		dataSources.setName(name);
		dataSources.setType(type);
		dataSources.setDesc(desc);
		dataSources.setId(id);
		DataSources result = dataSourcesService.saveOrUpdate(dataSources, user.getId());
		if(result.getId() == null){
			return execCheckError(Constant.EDIT_ERROR_MSG);
		}else{
			//sys_notice
			noticeService.insertNoticeByParam(3402,"数据源编辑",0,0);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		}
	}
	@RequirePermission("common")
	@ApiOperation(value="数据源详情", notes="数据源详情", response = DataSources.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query")})
	@RequestMapping(value="/detail", method = RequestMethod.POST)
	public JsonResult<Object> detail(@RequestBody JSONObject param){
		return execSuccess(dataSourcesService.selectById(param.getIntValue("id")));
	}

	@RequirePermission("common")
	@ApiOperation(value="指标组列表", notes="指标组列表")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query")})
	@RequestMapping(value="/indicator/group/select", method = RequestMethod.POST)
	public JsonResult<Object> indicatorList(@RequestBody JSONObject param){
		return execSuccess(dataSourcesService.selectIndicatorGroupBySourcesId(param.getIntValue("id")));
	}


    @ApiOperation(value="指标组保存", notes="指标组保存")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query")})
    @RequestMapping(value="/indicator/group/add", method = RequestMethod.POST)
	@OperateLog(type = OperaterType.INSERT, desc = "指标组保存")
    public JsonResult<Object> indicatorSave(@RequestBody IndicatorGroupParam indicatorGroupParam){
		if(dataSourcesService.isHasSameIndicatorGroupName(indicatorGroupParam.getName(), 0, indicatorGroupParam.getSourcesId())){
			return execCheckError("指标组名称不能重复！");
		}
		int id = dataSourcesService.saveOrUpdateIndicator(indicatorGroupParam);
		//sys_notice
		noticeService.insertNoticeByParam(3406,"数据源指标组配置新建",0,0);
		return execSuccess(id);
    }

	@ApiOperation(value="指标组编辑", notes="指标组编辑")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="数据源id",dataType="int", paramType = "query")})
	@RequestMapping(value="/indicator/group/edit", method = RequestMethod.POST)
	@OperateLog(type = OperaterType.UPDATE, desc = "指标组编辑")
	public JsonResult<Object> indicatorEdit(@RequestBody IndicatorGroupParam indicatorGroupParam){
		if(dataSourcesService.isHasSameIndicatorGroupName(indicatorGroupParam.getName(), indicatorGroupParam.getId(),indicatorGroupParam.getSourcesId())){
			return execCheckError("指标组名称不能重复！");
		}
		dataSourcesService.saveOrUpdateIndicator(indicatorGroupParam);
		//删除外部数据配置
		deleteRuleService.deleteIndicatorGroup(indicatorGroupParam.getId());
		//sys_notice
		noticeService.insertNoticeByParam(3407,"数据源指标组配置编辑",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	@RequirePermission("common")
	@ApiOperation(value="指标组详情查询", notes="指标组详情查询", response =IndicatorGroupParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="指标组id",dataType="int", paramType = "query")})
	@RequestMapping(value="/indicator/group/detail", method = RequestMethod.POST)
	public JsonResult<Object> indicatorDetail(@RequestBody JSONObject param){
		return execSuccess(dataSourcesService.indicatorDetail(param.getIntValue("id")));
	}

	@ApiOperation(value="指标组删除", notes="指标组删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="指标组id",dataType="int", paramType = "query")})
	@RequestMapping(value="/indicator/group/delete", method = RequestMethod.POST)
	@OperateLog(type = OperaterType.DELETE, desc = "指标组删除")
	public JsonResult<Object> deleteGroup(@RequestBody JSONObject param){
		dataSourcesService.deleteGroupById(param.getIntValue("id"));
		//删除关联数据
		deleteRuleService.deleteIndicatorGroup(param.getIntValue("id"));
		//sys_notice
		noticeService.insertNoticeByParam(3408,"数据源指标组配置删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 数据源组件查询
	 * @param param
	 * @return
	 */
	@Deprecated
	@ApiOperation(value = "数据源组件查询", notes = "数据源组件查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "数据源id", value = "sourcesId", dataType = "int",paramType = "query") })
	@RequestMapping(value = "/data/tree/select", method = RequestMethod.POST)
	public JsonResult<Object> selectComponent(@RequestBody JSONObject param) {
		return execSuccess(dataSourcesService.selectComponent(param.getIntValue("sourcesId")));
	}

	/**
	 * 数据源组件添加
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "数据源组件添加", notes = "数据源组件添加")
	@ApiImplicitParams({ @ApiImplicitParam(name = "数据源id", value = "sourcesId", dataType = "int",paramType = "query"),
		@ApiImplicitParam(name = "同级树的id", value = "treeId", dataType = "int",paramType = "query"),
		@ApiImplicitParam(name = "节点名称", value = "title", dataType = "string",paramType = "query")
	})
	@OperateLog(type=OperaterType.INSERT,desc="数据源组件添加")
	@RequestMapping(value = "/data/tree/add", method = RequestMethod.POST)
	public JsonResult<Object> addComponent(@RequestBody JSONObject param) {
		Integer sourcesId=param.getInteger("sourcesId");
		Integer treeId=param.getInteger("treeId");
		String title=param.getString("title");
		Dscomponent ds=new Dscomponent();
		ds.setSourcesId(sourcesId);
		ds.setId(treeId);
		ds.setName(title);
		//判断菜单有没有数据配置
		if(!evUtil.listIsNullOrZero(dataSourcesService.selectContraint(treeId))) {
			return execCheckError("不允许创建子菜单！");
		}
		// 校验菜单名
		Boolean flag = dataSourcesService.checkName(ds, 1);
		if(flag) {
			int id=dataSourcesService.addComponent(sourcesId,treeId,title);
			return execSuccess(id, Constant.SAVE_SUCCESS_MSG);
		}else {
			return execCheckError("组名称重复！");
		}
	}

	/**
	 * 数据源组件删除
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "数据源组件删除", notes = "数据源组件删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "树的id", value = "treeId", dataType = "int",paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "数据源组件删除")
	@RequestMapping(value = "/data/tree/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteComponent(@RequestBody JSONObject param) {
		Integer treeId=param.getInteger("treeId");
		dataSourcesService.deleteComponent(treeId);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 *  数据源组件编辑
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "数据源组件编辑", notes = "数据源组件编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "数据源组件编辑")
	@ApiImplicitParams({ @ApiImplicitParam(name = "数据源id", value = "sourcesId", dataType = "int",paramType = "query"),
		@ApiImplicitParam(name = "同级树的id", value = "treeId", dataType = "int",paramType = "query"),
		@ApiImplicitParam(name = "节点名称", value = "title", dataType = "string",paramType = "query")
	})
	@RequestMapping(value = "/data/tree/edit", method = RequestMethod.POST)
	public JsonResult<Object> editComponent(@RequestBody JSONObject param) {
		Integer sourcesId=param.getInteger("sourcesId");
		Integer treeId=param.getInteger("treeId");
		String title=param.getString("title");
		Dscomponent ds=new Dscomponent();
		ds.setSourcesId(sourcesId);
		ds.setId(treeId);
		ds.setName(title);
		// 校验
		Boolean flag = dataSourcesService.checkName(ds, 0);
		if (flag) {
			dataSourcesService.editComponent(sourcesId,treeId,title);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		} else {
			return execCheckError("组名重复！");
		}
	}

	/**
	 * 指标组下拉列表查询
	 * @param param
	 * @return
	 */
	@Deprecated
	@ApiOperation(value = "指标组下拉列表查询", notes = "指标组下拉列表查询",response = ComponentIndicatorsParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "数据源id", value = "sourcesId", dataType = "int",paramType = "query") })
	@RequestMapping(value = "/data/indicator/list/select", method = RequestMethod.POST)
	public JsonResult<Object> selectIndicatorGroup(@RequestBody JSONObject param) {
		return execSuccess(dataSourcesService.selectIndicatorGroup(param.getIntValue("sourcesId")));
	}

	/**
	 * 指标组列表查询
	 * @param param
	 * @return
	 */
	@Deprecated
	@ApiOperation(value = "指标组列表查询", notes = "指标组列表查询",response = ComponentIndicatorsParam.class, responseContainer = "List")
	@ApiImplicitParams({@ApiImplicitParam(name = "同级树的id", value = "componentId", dataType = "int",paramType = "query"),
		@ApiImplicitParam(name = "数据源id", value = "sourcesId", dataType = "int",paramType = "query")
	})
	@RequestMapping(value = "/data/indicator/select", method = RequestMethod.POST)
	public JsonResult<Object> selectContraint(@RequestBody JSONObject param) {
		Integer componentId=param.getIntValue("componentId");
		Integer sourcesId=param.getIntValue("sourcesId");
		boolean flag=dataSourcesService.checkChildren(componentId,sourcesId);
		if(flag) {
			return execCheckError("该节点不是叶子节点！");
		}else {
			return execSuccess(dataSourcesService.selectContraint(componentId));
		}
	}

	/**
	 * 数据配置添加
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "数据配置添加", notes = "数据配置添加")
	@OperateLog(type=OperaterType.INSERT,desc="数据配置添加")
	@RequestMapping(value = "/data/indicator/add", method = RequestMethod.POST)
	public JsonResult<Object> addContraint(@RequestBody ComponentIndicatorsParam param) {
		dataSourcesService.addContraint(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 数据配置编辑
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "数据配置编辑", notes = "数据配置编辑")
	@OperateLog(type=OperaterType.UPDATE,desc="数据配置编辑")
	@RequestMapping(value = "/data/indicator/edit", method = RequestMethod.POST)
	public JsonResult<Object> editContraint(@RequestBody ComponentIndicatorsParam param) {
		dataSourcesService.editContraint(param);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 数据配置删除
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "数据配置删除", notes = "数据配置删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "组件指标组id", dataType = "int", paramType = "query")
	})
	@OperateLog(type = OperaterType.DELETE, desc = "数据配置删除")
	@RequestMapping(value = "/data/indicator/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteContraint(@RequestBody JSONObject param) {
		dataSourcesService.deleteContraint(param.getIntValue("id"));
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 指标同步状态更改
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "指标同步状态更改", notes = "指标同步状态更改")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "组件指标组id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "active", value = "同步状态", dataType = "int", paramType = "query")
	})
	@OperateLog(type = OperaterType.UPDATE, desc = "指标同步状态更改")
	@RequestMapping(value = "/data/indicator/active", method = RequestMethod.POST)
	public JsonResult<Object> updateActive(@RequestBody JSONObject param) {
		dataSourcesService.updateActive(param.getIntValue("id"),param.getIntValue("active"));
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}


	/**
	 * 指标组下载
	 * @author NJ
	 * @date 2018/8/16 18:45
	 * @param req
	 * @param resp
	 * @param id
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "指标组下载", notes = "指标组上传")
	@RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
	public JsonResult<Object> download(HttpServletRequest req, HttpServletResponse resp, @ApiParam(required = true, name = "id", value = "指标组id") @PathVariable int id) {
		List<Indicators> indicators = dataSourcesService.selectDsIndicatorsByGroupId(id);
		List<IndicatorContraint> indicatorContraintList = dataSourcesService.selectIndicatorContraintByGroupId(id);
		if(!evUtil.listIsNullOrZero(indicators)) {
			// 第一步，创建一个webbook，对应一个Excel文件
			XSSFWorkbook wb = new XSSFWorkbook();
			for (Indicators indicator : indicators) {
				// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
				XSSFSheet sheet = wb.createSheet(indicator.getName());
				// 第三步，在sheet中添加表头第0行
				XSSFRow row = sheet.createRow((int) 0);
				// 第四步，创建单元格，并设置值表头 设置表头居中
				XSSFCellStyle style = wb.createCellStyle();
				// 创建一个居中格式
				style.setAlignment(XSSFCellStyle.ALIGN_CENTER);
				XSSFCell cell = row.createCell((short) 0);
				cell.setCellValue("id");
				cell.setCellStyle(style);
				cell = row.createCell((short) 1);
				cell.setCellValue("约束");
				cell.setCellStyle(style);
				//写入数据
				int j = 0;
				for(int i = 0; i < indicatorContraintList.size(); i++){
					IndicatorContraint indicatorContraint = indicatorContraintList.get(i);
					if(indicator.getId().intValue() == indicatorContraint.getIndicatorId().intValue()){
						row = sheet.createRow((int) ++j);
						// 第四步，创建单元格，并设置值
						row.createCell((short) 0).setCellValue(indicatorContraint.getOuterUuid());
						row.createCell((short) 1).setCellValue(indicatorContraint.getValue());
					}
				}
			}
			// 第六步，将文件存到指定位置
			resp.setCharacterEncoding("utf-8");
			resp.setContentType("multipart/form-data");
			resp.setHeader("Content-Disposition", "attachment;fileName=indicatorDetail.xlsx");
			OutputStream os = null;
			try {
				os = resp.getOutputStream();
				wb.write(os);
				// 这里主要关闭。
				if (os != null) {
					os.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				try {
					if (os != null) {
						os.close();
					}
					if (wb != null) {
						wb.close();
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return execCheckError("指标组没有相应指标");
	}

	/**
	 * 指标组上传
	 * @author NJ
	 * @date 2018/8/16 18:46
	 * @param file
	 * @param req
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "指标组上传", notes = "指标组上传")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<Object> upload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req,@RequestParam("id") int id) throws IOException {
		String fileName = file.getOriginalFilename();
		if (file.isEmpty() || fileName.lastIndexOf(".xlsx") == -1) {
			return execCheckError("文件解析失败！请确认是否是下载的文件类型");
		}
		//先删除表中数据后上传
		Map<String, Object> map = new HashMap<>(1);
		map.put("indicatorGroupId",id);
		dataSourcesService.deleteIndicatorContraintByMap(map);
		Map<String, Integer> indicatorMap = dataSourcesService.selectDsIndicatorsMapByGroupId(id);
		InputStream in = file.getInputStream();
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(in);
		Iterator<XSSFSheet> iterator = xssfWorkbook.iterator();
		List<IndicatorContraint> indicatorContraintList = new ArrayList<>();
		String uuidStr ;
		String valueStr;
		while(iterator.hasNext()){
			XSSFSheet xssfSheet = iterator.next();
			String sheetName = xssfSheet.getSheetName();
			if(indicatorMap.get(sheetName) != null){
				for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
					XSSFRow xssfRow = xssfSheet.getRow(i);
					if (xssfRow != null) {
						XSSFCell uuid = xssfRow.getCell(0);
						XSSFCell value = xssfRow.getCell(1);
						if (uuid.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							uuidStr = String.valueOf((long)uuid.getNumericCellValue());
						} else if (uuid.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							uuidStr = uuid.getStringCellValue();
						}else{
							continue;
						}
						if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
							valueStr = String.valueOf((long)value.getNumericCellValue());
						} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							valueStr = value.getStringCellValue();
						}else{
							continue;
						}
						IndicatorContraint indicatorContraint = new IndicatorContraint();
						indicatorContraint.setIndicatorId(indicatorMap.get(sheetName));
						indicatorContraint.setOuterUuid(uuidStr);
						indicatorContraint.setValue(valueStr);
						indicatorContraint.setIndicatorGroupId(id);
						indicatorContraint.setMark(0);
						indicatorContraintList.add(indicatorContraint);
					}
				}
			}
		}
		xssfWorkbook.close();
		if(indicatorContraintList.size() > 0){
			dataSourcesService.insertBatchDsIndicatorContraint(indicatorContraintList);
			//sys_notice
			noticeService.insertNoticeByParam(3409,"数据源指标组excel上传",0,0);
			return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
		}else{
			return execCheckError("所上传文件没有匹配数据，请检查之后再上传！");
		}
	}
}
