package com.greattimes.ev.bpm.controller.config;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.utils.IndicatorSupportUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.entity.Dimension;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.service.common.ISequenceService;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.bpm.service.config.IRegioninfoService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.bpm.service.datasources.IDataSourcesService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.system.service.IDictionaryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 业务通用查询
 * @author NJ
 * @date 2018/5/24 18:23
 */
@RestController
@RequestMapping("/common")
@Api(value = "CommonController", tags = "业务通用查询")
public class CommonController extends BaseController {

	@Autowired
	private IDictionaryService iDictionaryService;
	@Autowired
	private IComponentService iComponentService;
	@Autowired
	private IApplicationService iApplicationService;
	@Autowired
	private IComponentDimensionService componentDimensionService;
	@Autowired
	private IProtocolService protocolService;
	@Autowired
	private IRegioninfoService regioninfoService;
	@Autowired
	private IDataSourcesService dataSourcesService;
	@Autowired
	private ICustomService CustomService;
	@Autowired
	private ISequenceService sequenceService;
	@Autowired
	private ConfigurationCache configurationCache;

	/**    
	 * 查询字典表的name和value
	 * @author NJ  
	 * @date 2018/5/25 9:30  
	 * @param param  
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>  
	 */
	@ApiOperation(value = "查询字典表", notes = "查询字典表")
	@ApiImplicitParams({ @ApiImplicitParam(name = "type", value = "字典表类型", dataType = "string", paramType = "query") })
	@RequestMapping(value = "/dictionary/select", method = RequestMethod.POST)
	public JsonResult<Object> findDictionaryByDictTypeCode(@RequestBody JSONObject param) {
		List<Map<String, Object>> result = new ArrayList<>();
		String type = param.getString("type");
		if (StringUtils.isNotEmpty(type)) {
			result = iDictionaryService.selectDicNameAndValueByDictTypeCode(type);
		}
		return execSuccess(result);
	}

	/**
	 * 根据业务id查询组件name和id
	 * @author NJ
	 * @date 2018/5/25 10:02
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据业务id查询组件", notes = "根据业务id查询组件", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "businessId", value = "业务id", dataType = "string", paramType = "query") })
	@RequestMapping(value = "/components", method = RequestMethod.POST)
	public JsonResult<Object> findComponentByBusinessId(@RequestBody JSONObject param) {
		int businessId = param.getIntValue("businessId");
		return execSuccess(iComponentService.selectComponentNameAndIdByBusinessId(businessId));
	}

	/**
	 * 根据组件id查询组件ip
	 * @author NJ
	 * @date 2018/5/25 10:14
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据组件id查询组件ip", notes = "根据组件id查询组件ip", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/ip", method = RequestMethod.POST)
	public JsonResult<Object> findComponentIpByComponentId(@RequestBody JSONObject param) {
		return execSuccess(iComponentService.selectComponentIpById(param.getIntValue("componentId")));
	}

	/**
	 * 根据ip查询端口
	 * @author NJ
	 * @date 2018/5/25 11:42
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据ip查询端口", notes = "根据ip查询端口", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "ip", value = "ip", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/port", method = RequestMethod.POST)
	public JsonResult<Object> findComponentPortByIp(@RequestBody JSONObject param) {
		return execSuccess(iComponentService.selectComponentPortByIp(param.getIntValue("ip")));
	}

	/**
	 * 根据userId 查询相关应用
	 * @author NJ
	 * @date 2018/5/25 13:08
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据userId 查询相关应用", notes = "根据userId 查询相关应用", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/application", method = RequestMethod.POST)
	public JsonResult<Object> findAppliactionByUserId(@RequestBody JSONObject param) {
		return execSuccess(iApplicationService.selectAppAsDictonaryByUserId(param.getIntValue("userId")));
	}

	/**
	 * 根据componentId 查询对应维度
	 * @author cgc
	 * @date 2018/5/29 10:08
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据组件id查询对应维度", notes = "根据组件id查询对应维度", response = ProtocolField.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/dimension", method = RequestMethod.POST)
	public JsonResult<Object> findDimensionByComponentId(@RequestBody JSONObject param) {
		return execSuccess(componentDimensionService.selectDimensionByComponentId(param.getIntValue("componentId")));
	}

	@ApiOperation(value = "根据组件id查找相应ip和端口", notes = "根据组件id查找相应ip和端口", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/ipports", method = RequestMethod.POST)
	public JsonResult<Object> findIpPortByComponentId(@RequestBody JSONObject param) {
		return execSuccess(iComponentService.selectIpPortsByComponentId(param.getIntValue("componentId")));
	}

	@ApiOperation(value = "查找指标", notes = "查找指标", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type", value = "指标类型", dataType = "int", allowMultiple = true, paramType = "query") })
	@RequestMapping(value = "/indicator", method = RequestMethod.POST)
	public JsonResult<Object> findIndicator(@RequestBody Map<String, Object> map) {
		List<Integer> type=(List<Integer>) map.get("type");
		return execSuccess(iComponentService.selectIndicator(type));
	}

	/**
	 * 省份查询 
	 * @return
	 */
	@ApiOperation(value = "省份查询", notes = "省份查询", response = DictionaryParam.class, responseContainer = "List")
	@RequestMapping(value = "/province", method = RequestMethod.GET)
	public JsonResult<Object> selectProvince() {
		return execSuccess(regioninfoService.selectProvince());
	}

	/**
	 * 地市查询 
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "地市查询 ", notes = "地市查询 ", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "省份id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/city", method = RequestMethod.POST)
	public JsonResult<Object> selectCity(@RequestBody JSONObject param) {
		int provinceId = param.getIntValue("id");
		return execSuccess(regioninfoService.selectCity(provinceId));
	}

	/**    
	 * 协议输出字段查询
	 * @author cgc
	 * @date 2018/5/25 9:30  
	 * @param param  
	 * @return   
	 */
	@ApiOperation(value = "协议输出字段查询", notes = "协议输出字段查询", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type", value = "查询类型 1 协议 2 组件 3 自定义id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "id", value = "id", dataType = "int", paramType = "query") ,
			@ApiImplicitParam(name = "flag", value = "1 分析配置 0 二级维度", dataType = "int", paramType = "query") 
	})
	@RequestMapping(value = "/protocol/field", method = RequestMethod.POST)
	public JsonResult<Object> selectProtocolField(@RequestBody JSONObject param) {
		int id = param.getIntValue("id");
		int type = param.getIntValue("type");
		int flag = param.getIntValue("flag");
		return execSuccess(protocolService.selectProtocolField(id,type,flag));
	}
	
	/**
	 * 协议可用维度字段查询
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "协议可用维度字段查询", notes = "协议可用维度字段查询", response = Dimension.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "protocolId", value = "协议id", dataType = "int", paramType = "query")
	})
	@RequestMapping(value = "/protocol/dimension", method = RequestMethod.POST)
	public JsonResult<Object> selectProtocolDimension(@RequestBody(required = false) JSONObject param) {
		Integer protocolId = (Integer) param.get("protocolId");
		return execSuccess(protocolService.selectProtocolDimension(protocolId));
	}

	/**
	 * 根据应用applicationId查询所属监控点
	 * @author NJ
	 * @date 2018/8/8 18:22
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据应用id查询所属监控点", notes = "根据应用id查询所属监控点", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "监控点id", dataType = "int", paramType = "query"),
	})
	@RequestMapping(value = "/application/monitor", method = RequestMethod.POST)
	public JsonResult<Object> selectMonitorByAppId(@RequestBody JSONObject param) {
		return execSuccess(iApplicationService.selectMonitorDictionaryByAppId(param.getIntValue("applicationId")));
	}

	/**
	 * 根据监控点查询组件
	 * @author NJ
	 * @date 2018/8/8 18:22
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据监控点查询组件", notes = "根据监控点查询组件", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "monitorId", value = "监控点id", dataType = "int", paramType = "query"),
	})
	@RequestMapping(value = "/monitor/component", method = RequestMethod.POST)
	public JsonResult<Object> selectComponentByMonitorId(@RequestBody JSONObject param) {
		return execSuccess(iApplicationService.selectComponentDictionaryByMonitorId(param.getIntValue("monitorId")));
	}


	/**
	 * 查询数据源关联约束
	 * @author NJ
	 * @date 2018/8/17 18:33
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
    @ApiOperation(value = "查询数据源关联约束", notes = "查询数据源关联约束", response = DictionaryParam.class, responseContainer = "List")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "dataSourceId", value = "数据源id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "groupId", value = "指标组id", dataType = "int", paramType = "query")
    })
	@RequestMapping(value = "/alarm/relevantContraint", method = RequestMethod.POST)
	public JsonResult<Object> selectIndicatorByParam(@RequestBody JSONObject param) {
		return execSuccess(dataSourcesService.selectRelevantContraintDict(param.getIntValue("componentId"),
				param.getIntValue("dataSourceId"), param.getIntValue("groupId")));
	}

	/**    
	 * 自定义分析配置id查询分析统计维度
	 * @author NJ  
	 * @date 2018/8/29 14:31
	 * @param param  
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>  
	 */
	@ApiOperation(value = "自定义分析配置id查询分析统计维度", notes = "自定义分析配置id查询分析统计维度")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "customId", value = "自定义分析配置id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型", dataType = "int", paramType = "query")
	})
	@RequestMapping(value="/alarm/custom/dimension", method = RequestMethod.POST)
	public JsonResult<Object> selectStatisticDimensionByParam(@RequestBody JSONObject param) {
		return execSuccess(CustomService.selectDimensionByCustomIdAndType(param.getIntValue("customId"),param.getIntValue("type")));
	}

	/**
	 * 自定义分析配置id查询分析统计维度带有别名和翻译
	 * @author NJ
	 * @date 2018/8/29 14:31
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "自定义分析配置id查询分析统计维度", notes = "自定义分析配置id查询分析统计维度")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "customId", value = "自定义分析配置id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型", dataType = "int", paramType = "query")
	})
	@RequestMapping(value="/custom/dimension/othername" , method = RequestMethod.POST)
	public JsonResult<Object> selectStatisticDimensionOtherNameByParam(@RequestBody JSONObject param) {
		return execSuccess(CustomService.selectDimensionOtherNameByCustomIdAndType(param.getIntValue("customId"),param.getIntValue("type")));
	}

	@ApiOperation(value = "交易追踪字段", notes = "交易追踪字段")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "customId", value = "自定义分析配置id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型 0 组件普通维度 1 业务维度", dataType = "int", paramType = "query")
	})
	@RequestMapping(value="/trace/field" , method = RequestMethod.POST)
	public JsonResult<Object> selectTraceDimensionByParam(@RequestBody JSONObject param) {
		Integer componentId = param.getInteger("componentId");
		Integer customId = param.getInteger("customId");
		Map<String, Object> map = new HashMap<>(3);
		Integer type = param.getInteger("type");
		map.put("type", type);
		map.put("componentId", componentId);
		map.put("customId", customId);
		return execSuccess(CustomService.selectTraceDimensionByParam(map));
	}


	/**
	 * 根据统计分析维度查询维度值
	 * @author NJ
	 * @date 2018/8/29 18:09
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "根据统计分析维度查询维度值", notes = "根据统计分析维度查询维度值",response = DictionaryParam.class, responseContainer = "List")
	@RequestMapping(value="/alarm/custom/dimension/value" , method = RequestMethod.POST)
	public JsonResult<Object> selectStatisticDimensionValueByParam(@RequestBody JSONObject param) {
		return execSuccess(CustomService.selectStatisticsDimensionValueDict(param.getIntValue("id")));
	}

	/**
	 * 获取自增序列
	 * @author NJ
	 * @date 2018/9/7 15:47
	 * @param req
	 * @param sequenceName
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequestMapping(value="/nextval/{sequenceName}" ,method=RequestMethod.GET)
	public JsonResult<Object> sysConfigPage(HttpServletRequest req, @PathVariable String sequenceName){
		Map<String,Integer> result = new HashMap<>(1);
		result.put("seq",sequenceService.sequenceNextVal(sequenceName));
		return  execSuccess(result);
	}
	
	/**
	 * kpi分析左侧菜单
	 * @author cgc  
	 * @date 2018年9月17日  
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "kpi分析左侧菜单", notes = "kpi分析左侧菜单")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "type", value = "0 普通进入（新建）  1 其他页面进入（编辑）", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "id", value = "要选中的id", dataType = "int", paramType = "query",allowMultiple=true)
})
	@RequestMapping(value = "/menu", method = RequestMethod.POST)
	public JsonResult<Object> selectKpiMenu(HttpServletRequest req,@RequestBody JSONObject param) {
		int userId=getSessionUser(req).getId();
		int type=param.getIntValue("type");
		int applicationId=param.getIntValue("applicationId");
		Integer level=param.getInteger("level");
		List<Integer> checkedId= param.getJSONArray("id").toJavaList(Integer.class);
		return execSuccess(iApplicationService.selectMenu(userId,type,checkedId,level,null,0,applicationId));
	}
	
	/**
	 * 多维分析左侧菜单
	 * @author cgc  
	 * @date 2018年9月17日  
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "多维分析左侧菜单", notes = "多维分析左侧菜单")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "type", value = "0 普通进入（新建）  1 其他页面进入（编辑）", dataType = "int", paramType = "query"),
        @ApiImplicitParam(name = "id", value = "要选中的id", dataType = "int", paramType = "query",allowMultiple=true)
})
	@RequestMapping(value = "/multidimension/menu", method = RequestMethod.POST)
	public JsonResult<Object> selectMultidimensionMenu(HttpServletRequest req,@RequestBody JSONObject param) {
		int userId=getSessionUser(req).getId();
		int type=param.getIntValue("type");
		List<Integer> checkedId= param.getJSONArray("id").toJavaList(Integer.class);
		int applicationId=param.getIntValue("applicationId");
		Integer level=param.getInteger("level");
		return execSuccess(iApplicationService.selectMenu(userId,type,checkedId,level,null,1,applicationId));
	}
	/**
	 * 告警组组件树
	 * @author cgc  
	 * @date 2018年12月3日  
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "告警组组件树", notes = "告警组组件树")
	@RequestMapping(value = "/components/tree", method = RequestMethod.POST)
	public JsonResult<Object> getAlarmGrouopComponentTree(HttpServletRequest req) {
		int userId=getSessionUser(req).getId();
		return execSuccess(iApplicationService.getAlarmGrouopComponentTree(userId));
	}
	
	/**
	 * 告警组事件树
	 * @author cgc  
	 * @date 2018年12月3日  
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "告警组事件树", notes = "告警组事件树")
	@RequestMapping(value = "/events/tree", method = RequestMethod.POST)
	public JsonResult<Object> getAlarmGrouopEventTree(HttpServletRequest req) {
		int userId=getSessionUser(req).getId();
		return execSuccess(iApplicationService.getAlarmGrouopEventTree(userId));
	}

	/**
	 * 根据参数key查询系统参数配置
	 * @author NJ
	 * @date 2018/9/19 10:50
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "查询系统参数配置", notes = "查询系统参数配置")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "key", value = "参数key", dataType = "string", paramType = "query")})
	@RequestMapping(value="/constant/query", method = RequestMethod.POST)
	public JsonResult<Object> findSysConfigConstant(@RequestBody JSONObject jsonObject){
		Map<String, Object> map = new HashMap<>(1);
		map.put("systemparam", configurationCache.getValue(jsonObject.getString("key")));
		return execSuccess(map);
	}

	/**
	 * 根据类型获取相应的时间戳
	 * @author NJ
	 * @date 2018/9/19 10:56
	 * @param jsonObject   type:1、系统默认 2、应用 3、告警 (时间延迟从参数配置中获取)
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "获取相应的时间戳", notes = "获取相应的时间戳")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "type", value = "1、系统默认 2、应用 3、告警 ", dataType = "string", paramType = "query"),
			@ApiImplicitParam(name = "interval", value = "-1 毫秒级 0、秒级 1、分钟级 2、小时 3、根据参数管理bpm_interval颗粒度处理(<=60s) ", dataType = "string", paramType = "query")
	})
	@RequestMapping(value="/timestamp", method = RequestMethod.POST)
	public JsonResult<Object> getTimeStamp(@RequestBody JSONObject jsonObject){
		int type = jsonObject.getIntValue("type");
		int interval = jsonObject.getIntValue("interval");

//		if(interval == -1){
//		}else if(interval == 0){
//			c.set(Calendar.MILLISECOND, 0);
//		}else if(interval == 1){
//			c.set(Calendar.MILLISECOND, 0);
//			c.set(Calendar.SECOND,0);
//		}else if(interval == 2){
//			c.set(Calendar.MINUTE, 0);
//			c.set(Calendar.MILLISECOND, 0);
//			c.set(Calendar.SECOND,0);
//		}
		long time = System.currentTimeMillis();
		if(type == 2){
			time = time - Integer.parseInt(configurationCache.getValue(Constant.APPLICATION_DELAY))*1000;
		}else if(type == 3){
			time = time - Integer.parseInt(configurationCache.getValue(Constant.ALARM_DELAY))*1000;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(new Date(time));
		if(interval == -1){
		}else if(interval == 0){
			c.set(Calendar.MILLISECOND, 0);
		}else if(interval == 1){
			c.set(Calendar.MILLISECOND, 0);
			c.set(Calendar.SECOND,0);
		}else if(interval == 2){
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.MILLISECOND, 0);
			c.set(Calendar.SECOND,0);
		}else if(interval == 3){
			int bpmInterval = Integer.parseInt(configurationCache.getValue(Constant.BPM_INTERVAL));
			c.set(Calendar.MILLISECOND, 0);
			c.set(Calendar.SECOND, (c.get(Calendar.SECOND) / bpmInterval) * bpmInterval);
		}
		Map<String, Object> result = new HashMap<>(1);
		result.put("timestamp",c.getTimeInMillis());
		return execSuccess(result);
	}

	@ApiOperation(value = "根据时间段获取颗粒度", notes = "根据时间段获取颗粒度")
	@RequestMapping(value="/interval", method = RequestMethod.POST)
	public JsonResult<Object> getInterval(@RequestBody JSONObject jsonObject){
		Long duration = jsonObject.getLong("duration");
		Map<String, Object> result = new HashMap<>();
		result.put("interval", IndicatorSupportUtil.getIntervalByDuration(configurationCache,duration));
		return execSuccess(result);
	}

	/**
	 * 路径图组件树
	 * @author NJ
	 * @date 2018/9/26 13:10
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "组件树", notes = "组件树")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query",allowMultiple=true)
	})
	@RequestMapping(value = "/component/tree", method = RequestMethod.POST)
	public JsonResult<Object> selectComponentTree(@RequestBody JSONObject param) {
		return execSuccess(iApplicationService.selectComponentTree(param.getIntValue("componentId")));
	}

	@ApiOperation(value = "查找自定义业务指标", notes = "查找自定义业务指标", response = DictionaryParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", allowMultiple = true, paramType = "query") })
	@RequestMapping(value = "/custom/indicator", method = RequestMethod.POST)
	public JsonResult<Object> findCustomIndicator(@RequestBody JSONObject param) {
        return execSuccess(CustomService.findCustomIndicatorByComponentId(param.getIntValue("componentId")));
	}

	/**
	 * 自定义业务组件树
	 * @author NJ
	 * @date 2018/9/26 13:10
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "自定义业务组件树", notes = "自定义业务组件树")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query",allowMultiple=true)
	})
	@RequestMapping(value = "/custom/tree", method = RequestMethod.POST)
	public JsonResult<Object> selectCustomTree(@RequestBody JSONObject param) {
		return execSuccess(CustomService.selectCustomTreeByAppId(param.getIntValue("applicationId")));
	}

	@ApiOperation(value = "自定义统计维度树", notes = "自定义统计维度树")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "customId", value = "事件id", dataType = "int", paramType = "query", allowMultiple=true)
	})
	@PostMapping("/custom/statistics/tree")
	public JsonResult<Object> selectCustomStaticDimensionTree(@RequestBody JSONObject param) {
		Integer customId = param.getInteger("customId");
		if(customId == null){
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
		Map<String, Object> paramMap = new HashMap<>(3);
		paramMap.put("customId",customId);
		return execSuccess(CustomService.selectStatisticsDimensionForTree(paramMap));
	}
	
	@ApiOperation(value = "查询组件下的事件", notes = "查询组件下的事件")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "componentId", value = "组件id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "active", value = "状态  1 on 0 off 2 全部", dataType = "int", paramType = "query")
			
	})
	@RequestMapping(value = "/custom/select", method = RequestMethod.POST)
	public JsonResult<Object> selectCustomBycomponentId(@RequestBody JSONObject param) {
		int componentId=param.getIntValue("componentId");
		int active=param.getIntValue("active");
		Map<String, Object> map=new HashMap<>();
		map.put("componentId", componentId);
		map.put("active", active);
		return execSuccess(CustomService.selectCustomBycomponentId(map));
	}
	
	@ApiOperation(value = "查询组件", notes = "查询组件")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "active", value = "状态  1 on 0 off 2 全部", dataType = "int", paramType = "query")
			
	})
	@RequestMapping(value = "/component/select/active", method = RequestMethod.POST)
	public JsonResult<Object> selectComponent(@RequestBody JSONObject param) {
		int applicationId=param.getIntValue("applicationId");
		int active=param.getIntValue("active");
		Map<String, Object> map=new HashMap<>();
		map.put("applicationId", applicationId);
		map.put("active", active);
		return execSuccess(iComponentService.selectComponentByType(map));
	}


}
