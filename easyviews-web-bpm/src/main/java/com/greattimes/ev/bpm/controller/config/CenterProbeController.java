package com.greattimes.ev.bpm.controller.config;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.resp.CenterProbeParam;
import com.greattimes.ev.bpm.entity.CenterProbe;
import com.greattimes.ev.bpm.service.config.ICenterService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 探针表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-22
 */
@RestController
@Api(value="CenterProbeController",tags="探针管理模块")
@RequestMapping("/bpm/centerprobe")
@RequirePermission("center")
public class CenterProbeController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ICenterService iCenterService;

	/**
	 * 详情
	 * @param req
	 * @return
	 */
	@ApiOperation(value="详情", notes="详情", response = CenterProbe.class, responseContainer = "List")
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectProbe(@RequestBody CenterProbe probe) {
		return execSuccess(iCenterService.selectProbe(probe.getId()));
	}
	
	/**
	 * 查询
	 * @param req
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value="查询", notes="查询", response = CenterProbeParam.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public JsonResult<Object> selectAll(HttpServletRequest req) {
		return execSuccess(iCenterService.selectProbe());
	}

	/**
	 * 新增
	 * @param req
	 * @param center
	 * @return
	 */
	@ApiOperation(value="新增", notes="新增")
	@OperateLog(type=OperaterType.INSERT,desc="探针新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> saveProbe(HttpServletRequest req, @RequestBody @Validated CenterProbe centerprobe) {
		Boolean flag = iCenterService.checkProbeName(centerprobe, 1);
		if (flag) {
			Boolean ipCheck = iCenterService.checkProbeIp(centerprobe, 1);
			if(ipCheck) {
				iCenterService.save(centerprobe);
				return execSuccess(Constant.SAVE_SUCCESS_MSG);
			}else {
				log.error("探针ip重复");
				return execCheckError("探针IP不能重复");
			}
		} else {
			log.error("探针名重复");
			return execCheckError("探针名不能重复");
		}
	}

	/**
	 * 编辑
	 */
	@ApiOperation(value="编辑", notes="编辑")
	@OperateLog(type=OperaterType.UPDATE,desc="探针编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> editProbe(HttpServletRequest req, @RequestBody @Validated CenterProbe centerprobe) {
		Boolean flag = iCenterService.checkProbeName(centerprobe, 0);
		if (flag) {//
			Boolean ipCheck = iCenterService.checkProbeIp(centerprobe, 2);
			if(ipCheck) {
				iCenterService.update(centerprobe);
				return execSuccess(Constant.EDIT_SUCCESS_MSG);
			}else {
				log.error("探针ip重复");
				return execCheckError("探针IP不能重复");
			}
		} else {
			log.error("探针名重复");
			return execCheckError("探针名不能重复");
		}
	}

	/**
	 * 删除
	 */
	@ApiOperation(value="删除", notes="删除")
	@OperateLog(type=OperaterType.DELETE,desc="探针删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteProbe(HttpServletRequest req, @RequestBody CenterProbe centerprobe) {
		Integer id = centerprobe.getId();
		iCenterService.deleteProbe(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
}
