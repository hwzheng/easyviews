package com.greattimes.ev.bpm.controller.analysis.bottleneck;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.common.annotation.MethodCost;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.BottleneckTask;
import com.greattimes.ev.bpm.service.analysis.IBottleneckTaskService;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.service.IBpmPreService;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.service.IDictionaryService;
import com.greattimes.ev.utils.HttpClientUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 智能瓶颈报表任务表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@RestController
@RequestMapping("/bottleneck")
@Api(value = "BottleneckTaskController", tags = "瓶颈分析配置")
@RequirePermission("bottleneck")
public class BottleneckTaskAnalyseController extends BaseController {
	@Autowired
	private IBottleneckTaskService bottleneckTaskService;
	@Autowired
	private IBpmPreService bpmPreService;
	@Autowired
	private IComponentDimensionService componentDimensionService;
	@Autowired
	private ICustomService customService;
	@Autowired
	private IComponentService componentService;
	@Autowired
	private IDictionaryService dictionaryService;
	@Autowired
	private ConfigurationCache configurationCache;
	Logger log = LoggerFactory.getLogger(this.getClass());
	private static final String TABLE_FIX_BPM = "bpmPre";
	private static final String TABLE_FIX_EXTEND = "extendPre";

	@ApiOperation(value = "分析数据集添加", notes = "分析数据集添加")
	@OperateLog(type = OperaterType.INSERT, desc = "分析数据集添加")
	@RequestMapping(value = "/datalist/config/add", method = RequestMethod.POST)
	public JsonResult<Object> addTask(HttpServletRequest request, @RequestBody JSONObject jsonObject)
			throws ParseException {
		User user = getSessionUser(request);
		String name = jsonObject.getString("name");
		if (bottleneckTaskService.checkTaskName(null, name, 1)) {
			Integer id = bottleneckTaskService.addTask(jsonObject, user.getId());
			Map<String, Object> map = new HashMap<>();
			map.put("dataListId", id);
			return execSuccess(map, Constant.SAVE_SUCCESS_MSG);
		} else {
			return execCheckError("任务名重复！");
		}

	}

	@ApiOperation(value = "数据集列表查询", notes = "数据集列表查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "type", value = "数据集类型 0定时 1 单次", dataType = "int") })
	@MethodCost(desc = "数据集列表查询")
	@RequestMapping(value = "/datalist/select", method = RequestMethod.POST)
	public JsonResult<Object> selectTask(HttpServletRequest request, @RequestBody JSONObject jsonObject) {
		User user = getSessionUser(request);
		int type = jsonObject.getIntValue("type");
		Map<String, Object> map = new HashMap<>(2);
		map.put("type", type);
		map.put("userId", user.getId());
		return execSuccess(bottleneckTaskService.selectTask(map));
	}

	@ApiOperation(value = "数据集列表删除", notes = "数据集列表删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "数据集id", dataType = "int") })
	@OperateLog(type = OperaterType.DELETE, desc = "数据集列表删除")
	@RequestMapping(value = "/datalist/config/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteTask(@RequestBody JSONObject jsonObject) {
		bottleneckTaskService.deleteTask(jsonObject.getIntValue("id"));
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	@ApiOperation(value = "数据集列表编辑", notes = "数据集列表编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "数据集列表编辑")
	@RequestMapping(value = "/datalist/config/edit", method = RequestMethod.POST)
	public JsonResult<Object> updateTask(HttpServletRequest request, @RequestBody JSONObject jsonObject) {
		User user = getSessionUser(request);
		int id = jsonObject.getIntValue("id");
		String name = jsonObject.getString("name");
		if (bottleneckTaskService.checkTaskName(id, name, 2)) {
			bottleneckTaskService.updateTask(jsonObject, user.getId());
			Map<String, Object> map = new HashMap<>();
			map.put("dataListId", id);
			return execSuccess(map, Constant.EDIT_SUCCESS_MSG);
		} else {
			return execCheckError("任务名重复！");
		}
	}

	@ApiOperation(value = "数据集详情表头查询", notes = "数据集详情表头查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "数据集id", dataType = "int") })
	@RequestMapping(value = "/datalist/columns/select", method = RequestMethod.POST)
	public JsonResult<Object> selectColumns(@RequestBody JSONObject jsonObject) {
		int id = jsonObject.getIntValue("id");
		return execSuccess(bottleneckTaskService.selectColumns(id));
	}

	@ApiOperation(value = "任务新建", notes = "任务新建")
	@OperateLog(type = OperaterType.INSERT, desc = "任务新建")
	@RequestMapping(value = "/task/add", method = RequestMethod.POST)
	public JsonResult<Object> addItem(@RequestBody JSONObject jsonObject) throws ParseException {
		String name = jsonObject.getString("name");
		if (bottleneckTaskService.checkItemName(null, name, 1)) {
			Integer id = bottleneckTaskService.addItem(jsonObject);
			Map<String, Object> map = new HashMap<>();
			map.put("taskId", id);
			//瓶颈分析单次分析请求接口
			int taskId=jsonObject.getIntValue("datalistId");
			BottleneckTask task=bottleneckTaskService.selectById(taskId);
			if (task.getType().equals(0)) {
				bottleneckTaskService.updateStep(String.valueOf(taskId));
				String ip = componentService.getCenterIpBycomponentId(task.getComponentId());
				if (null != ip) {
					Map<String, Object> param = new HashMap<String, Object>();
					String url = "http://"+ip+":"+configurationCache.getValue(ConfigConstants.TRANSACTION_TRACE_PORT)+configurationCache.getValue(ConfigConstants.AI_ETL_URL);
					param.put("taskId", taskId);
					param.put("taskItemId", id);
					log.info("请求参数:{}",param.toString());
					Map<String, Object> result = new HashMap<>();
					try {
						result = HttpClientUtil.postsWithMapReturn(url, JSON.toJSONString(param));
					} catch (Exception e) {
						log.error("瓶颈分析单次分析请求接口--调用远程接口发生异常：" + e.getMessage());
						throw new RuntimeException(e.getMessage());
					}
					log.info("响应参数:{}",result.toString());
					if (!result.isEmpty()) {
						String statusCode = evUtil.getMapStrValue(result, "statusCode");
						String body = evUtil.getMapStrValue(result, "body");
						if ("202".equals(statusCode)) {
							log.info("瓶颈分析单次分析请求接口成功！");
						}else {
							log.info("瓶颈分析单次分析请求接口失败！");
						}
					} else {
						log.error("调用" + url + "无返回结果！");
					}
				} else {
					log.error("瓶颈分析开始分析未获取到组件对应中心ip!组件id:[" + task.getComponentId() + "]");
				}
			}
			return execSuccess(map);
		}else {
			return execCheckError("子任务名重复！");
		}
	}

	@ApiOperation(value = "任务编辑", notes = "任务编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "任务编辑")
	@RequestMapping(value = "/task/edit", method = RequestMethod.POST)
	public JsonResult<Object> updateItem(@RequestBody JSONObject jsonObject) {
		int id=jsonObject.getInteger("id");
		String name=jsonObject.getString("name");
		if (bottleneckTaskService.checkItemName(id, name, 2)) {
		bottleneckTaskService.updateItem(jsonObject);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
		}else {
			return execCheckError("子任务名重复！");
		}
	}

	@ApiOperation(value = "任务详情", notes = "任务详情")
	@ApiImplicitParams({ @ApiImplicitParam(name = "taskId", value = "任务id", dataType = "int") })
	@RequestMapping(value = "/task/detail", method = RequestMethod.POST)
	public JsonResult<Object> getItemDetail(@RequestBody JSONObject jsonObject) {
		int id = jsonObject.getIntValue("taskId");
		return execSuccess(bottleneckTaskService.getItemDetail(id));
	}

	@ApiOperation(value = "任务刪除", notes = "任务刪除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "taskId", value = "任务id", dataType = "int") })
	@RequestMapping(value = "/task/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteItem(@RequestBody JSONObject jsonObject) {
		int id;
		List<Integer> ids=new ArrayList<>();
		if(null!=jsonObject.get("taskId")) {
			id=jsonObject.getIntValue("taskId");
			ids.add(id);
		}
		bottleneckTaskService.deleteItem(ids);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 分析初始化
	 * 
	 * @param taskId
	 * @return
	 * @throws ParseException
	 */
	@ApiOperation(value = "分析初始化", notes = "分析初始化")
	@OperateLog(type = OperaterType.UPDATE, desc = "分析初始化")
	@RequestMapping(value = "/analysis/query/{taskId}", method = RequestMethod.POST)
	public JsonResult<Object> initStep(@PathVariable String taskId) throws ParseException {
		bottleneckTaskService.updateStep(taskId);
		return execSuccess(Constant.INIT_SUCCESS_MSG);
	}

	@ApiOperation(value = "定时任务日期列表查询", notes = "定时任务日期列表查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "taskId", value = "任务id", dataType = "int") })
	@RequestMapping(value = "/analysis/timed/date/select", method = RequestMethod.POST)
	public JsonResult<Object> selectDate(@RequestBody JSONObject jsonObject) {
		int id = jsonObject.getIntValue("taskId");
		return execSuccess(bottleneckTaskService.selectDate(id));
	}

	@ApiOperation(value = "分析进度", notes = "分析进度")
	@RequestMapping(value = "/analysis/step/{taskId}", method = RequestMethod.POST)
	public JsonResult<Object> getStep(@PathVariable String taskId) throws ParseException {
		return execSuccess(bottleneckTaskService.getStep(taskId));
	}

	@ApiOperation(value = "数据集详情数据查询", notes = "数据集详情数据查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "数据集id", dataType = "int"),
			@ApiImplicitParam(name = "time", value = "查询时间  单次不传  定时yy-mm-dd", dataType = "String") })
	@MethodCost(desc="数据集详情数据查询")
	@RequestMapping(value = "/datalist/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectDatalist(@RequestBody JSONObject jsonObject) {
		int id = jsonObject.getIntValue("id");
		String time = jsonObject.getString("time");
		Map<String, Object> map = new HashMap<>(2);
		map.put("id", id);
		map.put("time", time);
		Map<String, Object> result = selectDatalist(map);
		return execSuccess(result);
	}

	/**
	 * 数据集详情查询
	 * 
	 * @param map
	 * @return
	 */
	private Map<String, Object> selectDatalist(Map<String, Object> map) {
		//查询事件的固定维度
		/*List<Map<String, Object>> dictionnaryList= dictionaryService.selectDicNameAndValueByDictTypeCode("FIXED_DIMENSION");
		List<String> dictionnary = new ArrayList<>();
		dictionnaryList.stream().forEach(x -> {
			dictionnary.add(x.get("value").toString());
		});*/
		//返回结果map
		Map<String, Object> result = new HashMap<>();
		//用于画图的数据list
		List<Map<String, Object>> graphData = new ArrayList<>();
		StopWatch sw = new StopWatch();
		sw.start();
		Map<String, Object> task = bottleneckTaskService.selectTaskDetail(map);
		sw.stop();
		log.debug("数据集配置详情查询，花费时间：{}ms", sw.getLastTaskTimeMillis());
		//数据类型 1：BPM, 2:事件
		Integer dateType = (int) task.get("dataType");
		//报表类型 0单次 1 日报(定时)  2 周报 3月报
		Integer type = (int) task.get("type");
		Integer applicationId = (int) task.get("applicationId");
		//查询时间list
		List<Map<String, Object>> datelist = new ArrayList<>();
		// 单次
		if (type.equals(0)) {
			List<Long> date = (List<Long>) task.get("singleTime");
			Map<String, Object> dateMap = new HashMap<>();
			if (!evUtil.collectionIsNullOrZero(date)) {
				dateMap.put("start", date.get(0));
				dateMap.put("end", date.get(1));
				datelist.add(dateMap);
			}
		} else {//定时，需要将日期拼上时间
			String timingTime = (String) task.get("timingTime");
			List<List<String>> date = (List<List<String>>) task.get("timePart");
			if (!evUtil.listIsNullOrZero(date)) {
				for (List<String> list : date) {
					Map<String, Object> dateMap = new HashMap<>();
					if (!evUtil.listIsNullOrZero(list)) {
						long start = Long.parseLong(
								DateUtils.date2TimeStamp(timingTime + " " + list.get(0), "yyyy-MM-dd HH:mm"));
						long end = Long.parseLong(
								DateUtils.date2TimeStamp(timingTime + " " + list.get(1), "yyyy-MM-dd HH:mm"));
						dateMap.put("start", start);
						dateMap.put("end", end);
						datelist.add(dateMap);
					}
				}
			}
		}

		String table = null;
		// 数值类型最大最小平均值
		List<Map<String, Object>> numdata = new ArrayList<>();
		// 查询条件map
		Map<String, Object> selectMap = new HashMap<>();
		selectMap.put("datelist", datelist);
		//日期
		List<String> dateStr=DateUtils.getDateStr(datelist);
		selectMap.put("dateStr", dateStr);
		sw.start();
		//bpm
		if (dateType.equals(1)) {
			// 组件
			table = TABLE_FIX_BPM ;
			selectMap.put("table", table);
			selectMap.put("componentId", task.get("componentId"));
			selectMap.put("applicationId", applicationId);
			// 添加是类别且是指标 应用成功 retStatus 事件成功 businessStatus有无响应 existResponse
			// List<Map<String, Object>>
			// list=bpmPreService.selectBpmSuccessAndfailPercent(datelist,table);
			//List<Map<String, Object>> list = bpmPreService.selectBpmSuccessAndfailPercent(selectMap);
			selectMap.put("type", 1);
			//numdata = bpmPreService.getIndicatorMaxMinAvg(selectMap);
			//数字类型的最大值、最小值、平均值（金额  请求字节 响应字节 响应时间 ）
			numdata = bpmPreService.getIndicatorMaxMinAvgPercent(selectMap);
			if(evUtil.listIsNullOrZero(numdata)) {
				return null;
			}
			Map<String, Object> graphDataMap = new HashMap<>();
			//应用成功 
			graphDataMap.put("key", "retStatus");
			List<Map<String, Object>> categoryList = new ArrayList<>();
			Map<String, Object> categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "success");
			categoryListMap.put("count", numdata.get(0).get("successCount"));
			categoryListMap.put("percent", numdata.get(0).get("successPercent"));
			categoryList.add(categoryListMap);
			categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "fail");
			categoryListMap.put("count", numdata.get(0).get("failCount"));
			categoryListMap.put("percent", numdata.get(0).get("failPercent"));
			categoryList.add(categoryListMap);
			graphDataMap.put("categoryList", categoryList);
			graphData.add(graphDataMap);
			graphDataMap = new HashMap<>();
			//有无响应
			graphDataMap.put("key", "existResponse");
			categoryList = new ArrayList<>();
			categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "success");
			categoryListMap.put("count", numdata.get(0).get("responseCount"));
			categoryListMap.put("percent", numdata.get(0).get("responsePercent"));
			categoryList.add(categoryListMap);
			categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "fail");
			categoryListMap.put("count", numdata.get(0).get("unresponseCount"));
			categoryListMap.put("percent", numdata.get(0).get("unresponsePercent"));
			categoryList.add(categoryListMap);
			graphDataMap.put("categoryList", categoryList);
			graphData.add(graphDataMap);
		} else {//事件
			table = TABLE_FIX_EXTEND;
			selectMap.put("customId", task.get("customId"));
			selectMap.put("applicationId", applicationId);
			selectMap.put("table", table);
			// 添加是类别且是指标 应用成功 retStatus 事件成功 businessStatus有无响应 existResponse
			//List<Map<String, Object>> list = bpmPreService.selectExtendSuccessAndfailPercent(selectMap);
			selectMap.put("type", 2);
			numdata = bpmPreService.getIndicatorMaxMinAvgPercent(selectMap);
			if(evUtil.listIsNullOrZero(numdata)) {
				return null;
			}
			Map<String, Object> graphDataMap = new HashMap<>();
			//事件成功 
			graphDataMap.put("key", "businessStatus");
			List<Map<String, Object>> categoryList = new ArrayList<>();
			Map<String, Object> categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "success");
			categoryListMap.put("count", numdata.get(0).get("successCount"));
			categoryListMap.put("percent", numdata.get(0).get("successPercent"));
			categoryList.add(categoryListMap);
			categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "fail");
			categoryListMap.put("count", numdata.get(0).get("failCount"));
			categoryListMap.put("percent", numdata.get(0).get("failPercent"));
			categoryList.add(categoryListMap);
			graphDataMap.put("categoryList", categoryList);
			graphData.add(graphDataMap);
			graphDataMap = new HashMap<>();
			//有无响应
			graphDataMap.put("key", "existResponse");
			categoryList = new ArrayList<>();
			categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "success");
			categoryListMap.put("count", numdata.get(0).get("responseCount"));
			categoryListMap.put("percent", numdata.get(0).get("responsePercent"));
			categoryList.add(categoryListMap);
			categoryListMap = new HashMap<>(3);
			categoryListMap.put("value", "fail");
			categoryListMap.put("count", numdata.get(0).get("unresponseCount"));
			categoryListMap.put("percent", numdata.get(0).get("unresponsePercent"));
			categoryList.add(categoryListMap);
			graphDataMap.put("categoryList", categoryList);
			graphData.add(graphDataMap);
		}

		// 获取表头
		List<Map<String, Object>> col = bottleneckTaskService.selectColumns((int) map.get("id"));
		for (Map<String, Object> map2 : col) {
			Map<String, Object> graphDataMap = new HashMap<>();
			// 是类别 且是维度
			if (map2.get("type").equals(2) && map2.get("property").equals(1)) {
				List<Map<String, Object>> list=new ArrayList<>();
				if (dateType.equals(1)) {
					selectMap.put("colm", map2.get("key"));
					list = bpmPreService.selectAllTransCountPercent(selectMap);
				}else {
					//if(dictionnary.contains(evUtil.getMapStrValue(map2, "key"))) {
					selectMap.put("colm", map2.get("key"));
					/*}else {
						selectMap.put("colm","visitParamExtractString(extendFields,'"+map2.get("key")+"') AS "+map2.get("key"));
					}*/
					selectMap.put("groupcolm", map2.get("key"));
					list = bpmPreService.selectCustomAllTransCountPercent(selectMap);
				}
				//List<Map<String, Object>> num=bpmPreService.getDifTradeType(selectMap);
				graphDataMap.put("key", map2.get("key"));
				//graphDataMap.put("difTradeType", num.size());
				List<Map<String, Object>> categoryList = new ArrayList<>();
				for (Map<String, Object> map3 : list) {
					Map<String, Object> categoryListMap = new HashMap<>(3);
					categoryListMap.put("value", map3.get(map2.get("key").toString()));
					categoryListMap.put("count", map3.get("count"));
					categoryListMap.put("percent", map3.get("percent"));
					categoryList.add(categoryListMap);
				}
				graphDataMap.put("categoryList", categoryList);
				graphData.add(graphDataMap);
			}
			// 数值
			if (map2.get("type").equals(1)) {
				graphDataMap.put("key", map2.get("key"));
				List<Map<String, Object>> valueList = new ArrayList<>();
				Map<String, Object> valueListMap = new HashMap<>(3);
				valueListMap.put("min", numdata.get(0).get("min" + map2.get("key")));
				valueListMap.put("average", numdata.get(0).get("avg" + map2.get("key")));
				valueListMap.put("max", numdata.get(0).get("max" + map2.get("key")));
				valueList.add(valueListMap);
				graphDataMap.put("valueList", valueList);
				graphData.add(graphDataMap);
			}
		}
		result.put("graphData", graphData);
		sw.stop();
		log.debug("查询graphData，花费时间：{}ms", sw.getLastTaskTimeMillis());
		// ---table部分----
		int rowNum=0;
		List<Map<String, Object>> tableData = new ArrayList<>();
		Map<String, Object> tableMap = new HashMap<>();
		tableMap.put("datelist", datelist);
		tableMap.put("dateStr", dateStr);
		if (dateType.equals(1)) {
			tableMap.put("table", TABLE_FIX_BPM);
			tableMap.put("componentId", task.get("componentId"));
			List<Map<String, Object>> dimension = componentDimensionService
					.selectDimensionNameBycomponentId((int) task.get("componentId"));
			List<String> dimensionColumnsList = new ArrayList<>();
			for (Map<String, Object> map2 : dimension) {
				dimensionColumnsList.add(map2.get("ename").toString());
			}
			String dimensionColumns = StringUtils.join(dimensionColumnsList, " , ");
			tableMap.put("dimensionColumns", dimensionColumns);
			sw.start();
			tableData = bpmPreService.getBpmTableData(tableMap);
			sw.stop();
			log.debug("查询瓶颈表格数据 ，花费时间：{}ms", sw.getLastTaskTimeMillis());
			sw.start();
			rowNum=bpmPreService.getTableDataCount(tableMap);
			sw.stop();
			log.debug("查询瓶颈表格数据行数 ，花费时间：{}ms", sw.getLastTaskTimeMillis());
			for (Map<String, Object> m : tableData) {
				m.put("retStatus", m.get("successCount") + "/" + m.get("failCount"));
				m.remove("successCount");
				m.remove("failCount");
				m.put("existResponse", m.get("responseCount") + "/" + m.get("unresponseCount"));
				m.remove("responseCount");
				m.remove("unresponseCount");
			}
		} else {
			tableMap.put("table", TABLE_FIX_EXTEND );
			tableMap.put("applicationId", applicationId);
			tableMap.put("customId", task.get("customId"));
			List<Map<String, Object>> dimension = customService.selectCommonDimension((int) task.get("customId"));
			List<String> dimensionColumnsList = new ArrayList<>();
			List<String> selectColumnsList = new ArrayList<>();
			List<String> rowColumnsList = new ArrayList<>();
			for (Map<String, Object> map2 : dimension) {
				dimensionColumnsList.add(map2.get("ename").toString());
				//if(dictionnary.contains(evUtil.getMapStrValue(map2, "ename"))) {
					selectColumnsList.add( map2.get("ename").toString());
					rowColumnsList.add( map2.get("ename").toString());
				/*}else {
					selectColumnsList.add("visitParamExtractString(extendFields,'"+map2.get("ename")+"') AS "+map2.get("ename"));
					rowColumnsList.add("visitParamExtractString(extendFields,'"+map2.get("ename")+"')");
				}*/
			}
			String dimensionColumns = StringUtils.join(dimensionColumnsList, " , ");
			String selectColumns = StringUtils.join(selectColumnsList, " , ");
			String rowColumns = StringUtils.join(rowColumnsList, " , ");
			tableMap.put("dimensionColumns", dimensionColumns);
			tableMap.put("selectColumns", selectColumns);
			sw.start();
			tableData = bpmPreService.getExtendTableData(tableMap);
			sw.stop();
			log.debug("查询瓶颈表格数据，花费时间：{}ms", sw.getLastTaskTimeMillis());
			tableMap.put("rowColumns", rowColumns);
			sw.start();
			rowNum=bpmPreService.getExtendTableDataCount(tableMap);
			sw.stop();
			log.debug("查询表格数据行数，花费时间：{}ms", sw.getLastTaskTimeMillis());
			for (Map<String, Object> m : tableData) {
				m.put("businessStatus", m.get("successCount") + "/" + m.get("failCount"));
				m.remove("successCount");
				m.remove("failCount");
				m.put("existResponse", m.get("responseCount") + "/" + m.get("unresponseCount"));
				m.remove("responseCount");
				m.remove("unresponseCount");
			}
		}
		result.put("rowNum", rowNum);
		result.put("tableData", tableData);
		return result;
	}

	@ApiOperation(value = "数据集配置详情查询", notes = "数据集配置详情查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "数据集id", dataType = "int"),
			@ApiImplicitParam(name = "time", value = "查询时间  单次不传  定时yy-mm-dd", dataType = "String") })
	@RequestMapping(value = "/config/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectTaskDetail(@RequestBody JSONObject jsonObject) {
		int id = jsonObject.getIntValue("id");
		String time = jsonObject.getString("time");
		Map<String, Object> map = new HashMap<>(2);
		map.put("id", id);
		map.put("time", time);
		return execSuccess(bottleneckTaskService.selectTaskDetail(map));
	}
}
