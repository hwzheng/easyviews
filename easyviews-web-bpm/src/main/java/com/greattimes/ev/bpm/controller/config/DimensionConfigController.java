package com.greattimes.ev.bpm.controller.config;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.Dimension;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.service.IBpmPreService;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


/**
 * @author NJ
 * @date 2019/2/15 16:22
 */
@Deprecated
@RestController
@Api(value="DimensionConfigController",tags="维度配置管理模块")
@RequestMapping("/bpm/dimensionconfig")
@RequirePermission("dimensionconfig")
public class DimensionConfigController extends BaseController {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IProtocolService protocolService;
    @Autowired
	private IBpmPreService bpmPreService;
    @Autowired
	private ConfigurationCache configurationCache;
    
    private static String CLICKHOUSE_HOST = "clickhouse_host";
    private static final String BPMPRE = "bpmPre";
	private static final String VM_BPM = "vm_transaction_pre";
	private static final String VIEW_VM_BPM = "vm_transaction_pre_local";

    @ApiOperation(value = "维度新增", notes = "维度新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "字段id", dataType = "int", paramType = "query") ,
            @ApiImplicitParam(name = "name", value = "字段名称", dataType = "string", paramType = "query")
    })
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JsonResult<Object> select(@RequestBody @Validated Dimension dimension) {
        Dimension dimen  = protocolService.selectProDimensionById(dimension.getId());
        if(dimen != null){
            return execCheckError("编码重复！");
        }
        List<Dimension> dimensions = protocolService.selectProDimensionByName(dimension.getName());
        if(!evUtil.listIsNullOrZero(dimensions)){
            return execCheckError("字段名称不能重复！");
        }
        boolean result = protocolService.saveOrUpdateProDimension(dimension);
        //修改clicHouse表
        String hosts = configurationCache.getValueFromRedisThenDb(CLICKHOUSE_HOST);
		String url=configurationCache.getValueFromRedisThenDb("clickhouse_cluster_url");
		log.info("clickhouse连接的url为:{},host:{}", url,hosts);
        if(StringUtils.isBlank(hosts)){
            throw new IllegalArgumentException("参数错误！集群节点配置非法！无clickhouse_host参数");
        }
        String[] hostArr = hosts.split("\\,");
        for(int i = 0; i < hostArr.length; i++){
			updateTable(dimension.getName(),hostArr[i],url);
		}
        if(result){
            return execSuccess(Constant.SAVE_SUCCESS_MSG);
        }else {
            return execCheckError(Constant.SAVE_ERROR_MSG);
        }
    }

    private void updateTable(String name, String host, String url) {
    	List<String> newList=protocolService.selectProDimension().stream().map(Dimension::getName).collect(Collectors.toList());
    	String localTable=BPMPRE+"_local";
		List<String> columns=new ArrayList<>();
		columns.add(name);
		if (bpmPreService.compareWithTable(localTable, columns,host,url)) {
			try {
				// 删除集群表
				if (bpmPreService.isExist(BPMPRE, host, url)) {
					bpmPreService.dropTable(BPMPRE, host, url);
				}
				try {
					// 删除视图表
					if (bpmPreService.isExist(VM_BPM, host, url)) {
						bpmPreService.dropTable(VM_BPM, host, url);
					}
					if (bpmPreService.isExist(VM_BPM + "_5", host, url)) {
						bpmPreService.dropTable(VM_BPM + "_5", host, url);
					}
					try {
						// 分离物化视图
						if (bpmPreService.isExist(VIEW_VM_BPM, host, url)) {
							bpmPreService.detachTable(VIEW_VM_BPM, host, url);
						}
						// 分离物化视图
						if (bpmPreService.isExist(VIEW_VM_BPM + "_5", host, url)) {
							bpmPreService.detachTable(VIEW_VM_BPM + "_5", host, url);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						log.info("分离视图失败！ --》【" + VIEW_VM_BPM + "】");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.info("删除视图表失败！ --》【" + VIEW_VM_BPM + "】");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info("删除集群表失败！ --》【" + BPMPRE + "】");
			}
		//修改本地表
		bpmPreService.updatePerTable(localTable, columns, host,url);
		try {
			// 修改物化视图
			String innerTable = "`.inner."+VIEW_VM_BPM+"`";
			bpmPreService.updateView(innerTable, columns, host,url);
			String innerTable_5 = "`.inner."+VIEW_VM_BPM+"_5"+"`";
			bpmPreService.updateView(innerTable_5, columns, host,url);
			try {
				// 重新绑定视图
				bpmPreService.AttachView(newList, host,url);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				log.info("绑定视图失败！ --》【" + VIEW_VM_BPM  + "】");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("修改视图失败！ --》【" + VIEW_VM_BPM + "】");
		}
		//创建视图表
		bpmPreService.createVmTable(0, columns,host,url);
		//创建集群表
		bpmPreService.CreateClusterTable(newList, url);
	}
		
	}
	@ApiOperation(value = "协议维度查询", notes = "协议维度查询")
    @RequestMapping(value = "/query", method = RequestMethod.GET)
    public JsonResult<Object> select() {
        return execSuccess(protocolService.selectProDimension());
    }

}
