package com.greattimes.ev.bpm.controller.indicator;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.common.annotation.MethodCost;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.service.INetPerformanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.HashSet;
import java.util.Set;


/**
 * @author cgc
 * @date 2018/10/08 18:06
 */
@RestController
@Api(value = "ApplicationIndicatorController", tags = "应用指标查询")
@RequestMapping("/indicator/application")
public class ApplicationIndicatorController extends BaseController{

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private INetPerformanceService performanceService;
    @Autowired
    private IApplicationService applicationService;

    @ApiOperation(value = "应用指标查询图表类", notes = "应用指标查询图表类")
    @RequestMapping(value = "/chart", method = RequestMethod.POST)
    @MethodCost(desc = "应用指标查询图表类")
    public JsonResult<Object> chart(@RequestBody IndicatorParam param) {
        //parameter check
        if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        return execSuccess(performanceService.selectAppNetPerformanceListForChart(param));
    }

    @ApiOperation(value = "应用指标查询表格类", notes = "应用指标查询表格类")
    @MethodCost(desc = "应用指标查询表格类")
    @RequestMapping(value = "/table", method = RequestMethod.POST)
    public JsonResult<Object> table(HttpServletRequest request, @RequestBody IndicatorParam param) throws Exception {
        //parameter check
        if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
//        if(param.getIsAuthority() != null && param.getIsAuthority().equals(1)){
//            Set<Integer> sets = new HashSet<>();
//            Set<Long> uuids = applicationService.selectAllUuidByUserId(this.getSessionUser(request).getId());
//            uuids.forEach(x->{
//                sets.add(x.intValue());
//            });
//            param.getUuid().retainAll(sets);
//            param.setUuid(param.getUuid());
//        }
        return execSuccess(performanceService.selectAppNetPerformanceListForTable(param));
    }

}
