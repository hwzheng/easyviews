package com.greattimes.ev.bpm.controller.config;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.resp.IpPortParam;
import com.greattimes.ev.bpm.entity.AlarmRule;
import com.greattimes.ev.bpm.entity.ServerIp;
import com.greattimes.ev.bpm.service.config.IAlarmRuleService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.UserLogs;
import com.greattimes.ev.system.service.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 下线通知接口
 * @author: nj
 * @date: 2020-03-12 10:19
 * @version: 0.0.1
 */
@RestController
@Api(value="NotificationController",tags="下线通知接口(平安)")
@RequestMapping("/bpm/notification")
public class NotificationController extends BaseController {

    @Autowired
    private IComponentService componentService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IAlarmRuleService alarmRuleService;

    @Autowired
    private INoticeService noticeService;

    /**
     * 检查接口
     * @param map
     * @author: nj
     * @date: 2020-03-12 10:28
     * @version: 0.0.1
     * @return: com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="检查接口", notes="检查接口" , response = Map.class ,responseContainer="List")
    @ApiImplicitParams({
            @ApiImplicitParam(name="ip",value="ip",dataType="list", paramType = "query")})
    @RequestMapping(value = "/check", method = RequestMethod.POST)
    public JsonResult<Object> select(@RequestBody JSONObject map) {
        List<String> ipList = map.getJSONArray("ip").toJavaList(String.class);
        if(evUtil.listIsNullOrZero(ipList)){
            return execCheckError("输入参数为空!");
        }
        if(ipList.size() > 100){
            return execCheckError("单次只能检测100个ip!");
        }
        List<ServerIp> serverIps = componentService.selectIpByIpStr(ipList);
        int size = serverIps.size();
        HashSet<String> set = new HashSet((int)(size / 0.75f + 1.0f));
        serverIps.stream().forEach(x-> set.add(x.getIp()));
        Map<String, Object> result = new HashMap<>(3);
        List<Map<String, Object>> list = new ArrayList<>(ipList.size());
        for(String str : ipList){
            Map<String, Object> temp = new HashMap<>(4);
            temp.put("name", str);
            temp.put("state", set.contains(str));
            list.add(temp);
        }
        result.put("ip", list);
        return execSuccess(result);
    }



    @ApiOperation(value="检查下线", notes="检查下线")
    @RequestMapping(value = "/down", method = RequestMethod.POST)
    public JsonResult<Object> save(@RequestBody JSONObject map) {
        List<String> ipList = map.getJSONArray("ip").toJavaList(String.class);
        if(evUtil.listIsNullOrZero(ipList)){
            return execCheckError("输入参数为空!");
        }
        if(ipList.size() > 30){
            return execCheckError("单次只能下线30个ip!");
        }
        Map<String, Object> result = new HashMap<>(3);
        List<Map<String, Object>> list = new ArrayList<>(ipList.size());
        List<IpPortParam> ipPortParams = componentService.selectIpPortParamByPortStr(ipList);
        if(evUtil.listIsNullOrZero(ipPortParams)){
            ipList.stream().forEach(x->{
                Map<String, Object> temp = new HashMap<>(4);
                temp.put("name", x);
                temp.put("state", false);
                list.add(temp);
            });
            result.put("ip", list);
            return execSuccess(result);
        }
        Set<Integer> componentIdSet = new HashSet<>();
        StringBuilder sb = new StringBuilder("下线通知:");
        Set<String> descSet = new HashSet<>((int)(ipPortParams.size()/0.75f+1));
        Set<String> portStrSet = new HashSet<>((int)(ipPortParams.size()/0.75f+1));
        for(IpPortParam portParam : ipPortParams){
            portStrSet.add(portParam.getPortStr());
            componentIdSet.add(portParam.getComponentId());
            descSet.add("应用:"+portParam.getAppName()+",组件:"+portParam.getCompName()+",端口:"+portParam.getPortStr()+ " ");
        }
        List<AlarmRule> alarmRules = alarmRuleService.selectAlarmRuleByComponentIds(new ArrayList<>(componentIdSet));
        if(!evUtil.listIsNullOrZero(alarmRules)){
            Set<Integer> types = new HashSet<>();
            List<Integer> alarmIds = new ArrayList<>();
            for(AlarmRule alarmRule : alarmRules){
                types.add(alarmRule.getType());
                alarmIds.add(alarmRule.getId());
            }
            //批量更新状态
            alarmRuleService.updateBatchActiveByIds(alarmIds,0);
            //通知大数据
            types.stream().forEach(x-> notifyAlarmActive(x));
        }
        descSet.stream().forEach(x-> sb.append(x));
        //(Date time, Integer type, Integer state, String user, String desc, String module, Integer roleType
        UserLogs userLog = new UserLogs(new Date(),8,0,"admin", sb.toString(), "配置下线", 0);
        userService.insertUserLogs(userLog);
        //返回结果
        ipList.stream().forEach(x->{
            Map<String, Object> temp = new HashMap<>(4);
            temp.put("name", x);
            temp.put("state", portStrSet.contains(x));
            list.add(temp);
        });
        result.put("ip", list);
        return execSuccess(result);
    }


    /**
     * 通知大数据
     * @param type
     */
    private  void notifyAlarmActive(int type){
        switch (type) {
            case 9:
                noticeService.insertNoticeByParam(1904,"事件告警激活",0,0);
                break;
            case 2:
                noticeService.insertNoticeByParam(1304,"组件告警激活",0,0);
                break;
            case 3:
                noticeService.insertNoticeByParam(1404,"组件ip告警激活",0,0);
                break;
            case 4:
                noticeService.insertNoticeByParam(1504,"组件port告警激活",0,0);
                break;
            case 5:
                noticeService.insertNoticeByParam(1604,"组件单维度告警激活",0,0);
                break;
            case 6:
                noticeService.insertNoticeByParam(1704,"组件多维度告警激活",0,0);
                break;
            case 8:
                noticeService.insertNoticeByParam(1804,"数据源告警激活",0,0);
                break;
            case 10:
                noticeService.insertNoticeByParam(3104,"事件统计维度告警激活",0,0);
                break;
            case 11:
                noticeService.insertNoticeByParam(3204,"事件普通维度告警激活",0,0);
                break;
            case 12:
                noticeService.insertNoticeByParam(3304,"事件多级维度告警激活",0,0);
                break;
            default:
                break;
        }

    }
}