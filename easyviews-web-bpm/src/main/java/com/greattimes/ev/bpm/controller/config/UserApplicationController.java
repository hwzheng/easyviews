package com.greattimes.ev.bpm.controller.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.entity.Application;
import com.greattimes.ev.bpm.entity.UserApplication;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 应用表 前端控制器
 * 
 * @author Cgc
 * @since 2018-05-18
 */
@RestController
@Api(value="UserApplicationController",tags="用户应用管理模块")
@RequestMapping("/bpm/userapp")
@RequirePermission("userApplication")
public class UserApplicationController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IApplicationService applicationService;

	/**
	 * 查询应用
	 * @param req
	 * @param userApplication
	 * @return
	 */
	@ApiOperation(value="查询应用", notes="查询应用", response =ApplicationParam.class ,responseContainer="List")
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> selectApplications(HttpServletRequest req, @RequestBody UserApplication userApplication) {
		int userId = userApplication.getUserId();
		List<ApplicationParam> list = applicationService.selectApplications(userId);
		return execSuccess(list);
	}

	/**
	 * 查询全部应用
	 * @return
	 */
	@ApiOperation(value="查询全部应用", notes="查询全部应用", response =Application.class ,responseContainer="List")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public JsonResult<Object> selectApplications() {
		List<Application> list = applicationService.selectApplications();
		return execSuccess(list);
	}

	/**
	 * 保存用户应用关系
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="保存用户应用关系", notes="保存用户应用关系")
	@ApiImplicitParams({
		@ApiImplicitParam(name="userId",value="用户id",dataType="int", paramType = "query"),
		@ApiImplicitParam(name="userAppIds",value="勾选的应用id",dataType="int", allowMultiple = true, paramType = "query")
	})
	@OperateLog(type = OperaterType.INSERT, desc = "保存用户应用关系")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> saveRelation(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int userId = evUtil.getMapIntValue(map, "userId");
		List<Integer> userAppIds = (List<Integer>) map.get("userAppIds");
		applicationService.saveUserApplication(userId, userAppIds);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	
	/**
	 * 用户应用关系删除
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "用户应用关系删除", notes = "用户应用关系删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", paramType = "query"), 
	@ApiImplicitParam(name="applicationId",value="应用id",dataType = "int", paramType = "query")
	})
	@OperateLog(type = OperaterType.DELETE, desc = "用户应用关系删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteRelation(@RequestBody Map<String,Object> map) {
		Integer userId = evUtil.getMapIntValue(map, "userId");
		Integer applicationId = evUtil.getMapIntValue(map, "applicationId");
		applicationService.deleteRelation(userId,applicationId);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
}
