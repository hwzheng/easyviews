package com.greattimes.ev.bpm.controller.config;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.decode.param.req.JProtocolDetail;
import com.greattimes.ev.bpm.entity.ComponentDimension;
import com.greattimes.ev.bpm.entity.Protocol;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.config.IProtocolFieldService;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.permission.RequirePermission;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.ComponentParam;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/5/28 09:50
 */
@RequirePermission("basicSetting")
@RestController
@RequestMapping("/bpm/component")
@Api(value="ComponentController",tags="组件配置")
public class ComponentController extends BaseController{

    @Autowired
    private IComponentService iComponentService;
    @Autowired
    private IDeleteRuleService deleteRuleService;
    @Autowired
    private ConfigurationCache configurationCache;
    @Autowired
    private INoticeService noticeService;
    @Autowired
    private IComponentDimensionService componentDimensionService;
    @Autowired
    private IProtocolFieldService protocolFieldService;
    @Autowired
    private IProtocolService protocolService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * 新增组件
     * @author NJ
     * @DATE 2018/5/28 9:36
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="新增组件", notes="新增组件")
    @OperateLog(type = OperaterType.INSERT, desc = "新增组件")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JsonResult<Object> add(HttpServletRequest req,@Validated @RequestBody ComponentParam param) throws InvocationTargetException, IllegalAccessException {

        int topicNum = Integer.parseInt(configurationCache.getValue(Constant.TOPIC_NUM));
        int portNum = Integer.parseInt(configurationCache.getValue(Constant.PORT_MAX_NUM));
        String name = getSessionUser(req).getName();

    	if(iComponentService.isHasSameName(-1,param.getMonitorId(),param.getName())){
            return execCheckError("组件名称不能重复！");
        }

        //校验端口的最大数量并且模式为port级别
        if(param.getStatisticalMode().intValue() == 3 && !checkPortNum(param, portNum)){
            return execCheckError("组件的端口数量过大！");
        }

        Component componentResult = iComponentService.addOrUpdate(param,name, topicNum);

    	int componentId = componentResult.getId();

        //sys_notice
        noticeService.insertNoticeByParam(1201,"组件创建",0,0);
        noticeService.insertNoticeByParam(1201,"组件创建",1,0);

        //分析维度默认选中某些维度id
        EntityWrapper<ProtocolField> entityWrapper = new EntityWrapper<>();
        entityWrapper.where("protocolId={0}", componentResult.getPrototcolId());
        List<ProtocolField> protocolFields = protocolFieldService.selectList(entityWrapper);

        List<String> columns = new ArrayList<>();
        List<Integer> dimensionIds = new ArrayList<>();
        String enName;
        int statisticalMode = componentResult.getStatisticalMode();
        for(ProtocolField field : protocolFields){
            enName = field.getEname();
            //客户端
            if(Constant.CLIENT_IP.equals(enName)){
                columns.add(Constant.CLIENT_IP);
                dimensionIds.add(field.getId());
            }

            //组件的统计模式  `statisticalMode` int(11) NOT NULL COMMENT '组件数据统计模式1 组件 2 ip 3端口 ',
            if(statisticalMode == 2){
                //服务端
                if(Constant.SERVER_IP.equals(enName)){
                    columns.add(Constant.SERVER_IP);
                    dimensionIds.add(field.getId());
                }
            }else if(statisticalMode == 3){
                //服务端
                if(Constant.SERVER_IP.equals(enName)){
                    columns.add(Constant.SERVER_IP);
                    dimensionIds.add(field.getId());
                }
                //服务端
                if(Constant.SERVER_PORT.equals(enName)){
                    columns.add(Constant.SERVER_PORT);
                    dimensionIds.add(field.getId());
                }
            }
            //交易类型
            if(Constant.TRANSACTION_TYPE.equals(enName)){
                columns.add(enName);
                dimensionIds.add(field.getId());
            }
            //交易渠道
            if(Constant.TRANSACTION_CHANNEL.equals(enName)){
                columns.add(enName);
                dimensionIds.add(field.getId());
            }
            //返回码
            if(Constant.RET_CODE.equals(enName)){
                columns.add(enName);
                dimensionIds.add(field.getId());
            }
        }

        //组件分析维度的保存
        int k = 0;
        for(Integer dimensionId : dimensionIds){
            ComponentDimension componentDimension = new ComponentDimension();
            componentDimension.setComponentId(componentId);
            componentDimension.setSort(++k);
            componentDimension.setDimensionId(dimensionId);
            componentDimensionService.insert(componentDimension);
        }

        List<Map<String, Object>> fieldList = componentDimensionService.selectDimensionNameBycomponentId(componentResult.getId());

        List<String> enameList = new ArrayList<>();
        fieldList.stream().forEach(x -> {
            enameList.add(x.get("ename").toString());
        });


        //创建表
        //bpmPreService.addPerTable(componentId, columns, configurationCache.getValueFromRedisThenDb("clickhouse_cluster_url"));
        //bpmPreService.createPerView(componentId, columns, 1);
        //创建物化视图
        //bpmPreService.createPerMaterializedView(componentId, columns, configurationCache.getValueFromRedisThenDb("clickhouse_cluster_url"));
        //创建小时视图
        //bpmPreService.createPerHouer(componentId, columns, 1);

        return execSuccess(Constant.SAVE_SUCCESS_MSG);
    }

    /**
     * 修改组件
     * @author NJ
     * @date 2018/5/28 10:11
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="修改组件", notes="修改组件")
    @OperateLog(type = OperaterType.UPDATE, desc = "修改组件")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public JsonResult<Object> edit(HttpServletRequest req, @Validated @RequestBody  ComponentParam param) throws InvocationTargetException, IllegalAccessException{
        int topicNum = Integer.parseInt(configurationCache.getValue(Constant.TOPIC_NUM));
        int portNum = Integer.parseInt(configurationCache.getValue(Constant.PORT_MAX_NUM));
        String name =getSessionUser(req).getName();
    	Integer id = param.getId();
        if(id == null || id.intValue() <= 0){
            return execCheckError("参数错误！");
        }
        if(iComponentService.isHasSameName(id,param.getMonitorId(),param.getName())){
            return execCheckError("组件名称不能重复！");
        }

        //校验端口的最大数量并且模式为port级别
        if(param.getStatisticalMode().intValue() == 3 && !checkPortNum(param, portNum)){
            return execCheckError("组件的端口数量过大！");
        }

        Component componentResult = iComponentService.addOrUpdate(param,name, topicNum);

        /**
         * 组件统计模式的修改，更新组件分析维度表
         * 只进行正向的增加，反向不做减少字段
         */
        // '组件数据统计模式1 组件 2 ip 3端口 ',
        int statisticalMode = componentResult.getStatisticalMode();
        int componentId = componentResult.getId();
        if(statisticalMode == 2 || statisticalMode == 3){
            EntityWrapper<ProtocolField> entityWrapper = new EntityWrapper<>();
            entityWrapper.where("protocolId={0}", componentResult.getPrototcolId());
            List<ProtocolField> protocolFields = protocolFieldService.selectList(entityWrapper);
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("componentId", componentId);
            List<ComponentDimension> componentDimensions = componentDimensionService.selectByMap(paramMap);
            String enName;
            Integer fieldId;
            ComponentDimension serverIpDimension = new ComponentDimension();
            ComponentDimension serverPortDimension = new ComponentDimension();
            boolean hasServerIp = false, hasSeverPort = false;
            for(ProtocolField field : protocolFields) {
                enName = field.getEname();
                fieldId = field.getId();
                if (Constant.SERVER_IP.equals(enName)) {
                    serverIpDimension.setComponentId(componentId);
                    serverIpDimension.setDimensionId(fieldId);
                    for(ComponentDimension d : componentDimensions){
                        if(d.getDimensionId().equals(fieldId) ){
                            hasServerIp = true;
                            break;
                        }
                    }
                }
                if (Constant.SERVER_PORT.equals(enName)) {
                    serverPortDimension.setComponentId(componentId);
                    serverPortDimension.setDimensionId(field.getId());
                    for(ComponentDimension d : componentDimensions){
                        if(d.getDimensionId().equals(fieldId) ){
                            hasSeverPort = true;
                            break;
                        }
                    }
                }
            }
            if(statisticalMode == 2){
                if(!hasServerIp){
                    int sort = componentDimensionService.selectMaxSortByComponentId(componentId);
                    serverIpDimension.setSort(++sort);
                    componentDimensionService.insert(serverIpDimension);
                }
            }else if(statisticalMode == 3){
                int sort = componentDimensionService.selectMaxSortByComponentId(componentId);
                if(!hasServerIp){
                    serverIpDimension.setSort(++sort);
                    componentDimensionService.insert(serverIpDimension);
                }
                if(!hasSeverPort){
                    serverPortDimension.setSort(++sort);
                    componentDimensionService.insert(serverPortDimension);
                }
            }
        }

        //sys_notice
        noticeService.insertNoticeByParam(1202,"组件编辑",0,0);
        noticeService.insertNoticeByParam(1202,"组件编辑",1,0);
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }

    /**
     * 查询组件列表
     * @author NJ
     * @date 2018/5/28 10:30
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("common")
    @ApiOperation(value="查询组件列表", notes="查询组件列表", response = Component.class, responseContainer = "List")
    @ApiImplicitParams({
            @ApiImplicitParam(name="businessId",value="应用id",dataType="int", paramType = "query")})
    @RequestMapping(value = "/select", method = RequestMethod.POST)
    public JsonResult<Object> select(@RequestBody JSONObject param){
        return execSuccess(iComponentService.selectComponentParamListByAppId(param.getIntValue("businessId")));
    }

    /**
     * 组件删除
     * @author NJ
     * @date 2018/5/28 17:39
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="组件删除", notes="组件删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="组件id",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.DELETE, desc = "组件删除")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public JsonResult<Object> delete(@RequestBody JSONObject param) {
        List<Integer> list = new ArrayList<>();
        list.add(param.getInteger("id"));
        deleteRuleService.deleteComponent(list);
        //sys_notice
        noticeService.insertNoticeByParam(1203,"组件删除",0,0);
        noticeService.insertNoticeByParam(1203,"组件删除",1,0);
        return execSuccess(Constant.DELETE_SUCCESS_MSG);
    }


    /**
     * 组件排序
     * @author NJ
     * @date 2018/5/28 17:41
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="组件排序", notes="组件排序")
    @ApiImplicitParams({
            @ApiImplicitParam(name="ids",value="组件id数组",dataType = "int", allowMultiple = true, paramType = "query")})
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public JsonResult<Object> sort(@RequestBody JSONObject param) {
        JSONArray idsArray = param.getJSONArray("ids");
        List<ComponentParam> componentParams = new ArrayList<>();
        for(int i = 0; i < idsArray.size(); i++){
            ComponentParam componentParam = new ComponentParam();
            componentParam.setId(idsArray.getJSONObject(i).getInteger("id"));
            componentParam.setSort(idsArray.getJSONObject(i).getInteger("sort"));
            componentParams.add(componentParam);
        }
//        iComponentService.updateBatchSort(idsArray.toJavaList(ComponentParam.class));
        iComponentService.updateBatchSort(idsArray.toJavaList(ComponentParam.class));
        return execSuccess("组件排序成功！");
    }

    /**
     * 组件激活
     * @author NJ
     * @date 2018/5/28 18:04
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="组件激活", notes="组件激活")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="组件id",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.UPDATE, desc = "组件激活")
    @RequestMapping(value = "/active", method = RequestMethod.POST)
    public JsonResult<Object> active(@RequestBody JSONObject param) {
        iComponentService.updateActive(param.getIntValue("id"), param.getIntValue("active"));
        //sys_notice
        noticeService.insertNoticeByParam(1204,"组件业务统计状态修改",0,0);
        noticeService.insertNoticeByParam(1204,"组件业务统计状态修改",1,0);
        return execSuccess(Constant.STATE_SUCCESS_MSG);
    }

    /**
     * 组件详情
     * @author NJ
     * @date 2018/5/29 9:36
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="组件详情", notes="组件详情", response = ComponentParam.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="组件id",dataType="int", paramType = "query")})
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public JsonResult<Object> detail(@RequestBody JSONObject param) throws InvocationTargetException, IllegalAccessException {
        return execSuccess(iComponentService.getComponentDetail(param.getIntValue("id")));
    }
    /**根据组件id查询协议详情
     * @param param
     * @return
     * @throws UnsupportedEncodingException
     */
    @ApiOperation(value = "查询协议详情", notes = "查询协议详情")
    @ApiImplicitParams({@ApiImplicitParam(name="id",value="组件id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/protocol", method = RequestMethod.POST)
    public JsonResult<Object> getProtocol(@RequestBody JSONObject param) throws UnsupportedEncodingException {
    	int id=iComponentService.selectById(param.getIntValue("id")).getPrototcolId();
    	Protocol p=protocolService.selectById(id);
    	String ret =new String(p.getContent(),"UTF-8");
		JSONObject jb = JSON.parseObject(ret);
		JProtocolDetail jp = JSON.toJavaObject(jb, JProtocolDetail.class);
		Map<String, Object> map=new HashMap<>();
		map.put("protocolType", jp.getProtocolCode());
		map.put("messageType", jp.getMessageType());
		map.put("protocolId", jp.getProtocolId());
		map.put("analyzeflag", p.getAnalyzeflag());
        return execSuccess(map);
    }

    /**
     * 校验端口的最大数量
     * @param param
     * @param portNum
     * @return
     */
    private boolean checkPortNum(ComponentParam param, int portNum){
        List<JSONObject> serverList = param.getServer();
        int num = 0, lastPointIndex, fristNum,lastNum, portStart, portEnd;
        String [] portStrArr, ipStrArr, ips, ports;
        String ipStr, portStr,lastIpNumStr;
        for (JSONObject jsonObject : serverList) {
            ipStr = this.formatIpPortStr(jsonObject.getString("ip"));
            portStr = this.formatIpPortStr(jsonObject.getString("port"));
            if (StringUtils.isBlank(ipStr) || StringUtils.isBlank(portStr)) {
                break;
            }
            portStrArr = portStr.trim().split("\n|\r");
            ipStrArr = ipStr.trim().split("\n|\r");
            for(String ip : ipStrArr){
                if(StringUtils.isBlank(ip)){
                    continue;
                }
                ips =  ip.trim().split("-");
                if(ips.length > 1) {
                    //处理该种形式ip:192.168.0.1-80
                    lastPointIndex = ips[0].lastIndexOf(".");
                    lastIpNumStr = ips[0].substring(lastPointIndex + 1);
                    fristNum = Integer.parseInt(lastIpNumStr);
                    lastNum = Integer.parseInt(ips[1]);
                    while (fristNum <= lastNum) {
                        for(String port : portStrArr){
                            if(StringUtils.isBlank(port)){
                                continue;
                            }
                            ports =  port.trim().split("-");
                            if(ports.length > 1){
                                portStart = Integer.parseInt(ports[0]);
                                portEnd = Integer.parseInt(ports[1]);
                                while(portStart <= portEnd){
                                    portStart ++;
                                    num++;
                                }
                            }else{
                                num ++;
                            }
                        }
                        fristNum ++;
                    }
                }else{
                    for(String port : portStrArr){
                        if(StringUtils.isBlank(port)){
                            continue;
                        }
                        ports =  port.trim().split("-");
                        if(ports.length > 1){
                            portStart = Integer.parseInt(ports[0]);
                            portEnd = Integer.parseInt(ports[1]);
                            while(portStart <= portEnd){
                                portStart ++;
                                num++;
                            }
                        }else{
                            num ++;
                        }
                    }
                }
            }
        }
        log.info("port端口数量："+num);
        return num <= portNum;
    }

    /**
     * 去除多余的回车
     * @author NJ
     * @date 2018/6/15 15:51
     * @param
     * @return java.lang.String
     */
    private String formatIpPortStr(String ipPortStr){
        if(StringUtils.isNotBlank(ipPortStr)){
            ipPortStr = ipPortStr.replaceAll(" ","");
            StringBuilder sb = new StringBuilder("");
            String [] array = ipPortStr.split("\n|\r");
            for(String str : array){
                if(StringUtils.isNotBlank(str)){
                    sb.append(str).append("\n");
                }
            }
            return sb.substring(0,sb.lastIndexOf("\n"));
        }
        return "";
    }
}
