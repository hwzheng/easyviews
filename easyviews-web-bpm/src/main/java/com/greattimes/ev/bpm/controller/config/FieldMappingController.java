package com.greattimes.ev.bpm.controller.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.greattimes.ev.base.Constant;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.resp.DimensionLableParam;
import com.greattimes.ev.bpm.config.param.resp.FieldMappingParam;
import com.greattimes.ev.bpm.entity.FieldMapping;
import com.greattimes.ev.bpm.entity.FieldMappingCnf;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * 维度字段映射表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
@RestController
@Api(value = "FieldMappingController", tags = "映射配置模块")
@RequestMapping("/bpm/component/mapping")
@RequirePermission("analyze")
public class FieldMappingController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IComponentDimensionService componentDimensionService;
	@Autowired
	private INoticeService noticeService;

	/**
	 * 映射配置查询
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "映射配置查询", notes = "映射配置查询", response = FieldMappingParam.class, responseContainer = "List")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> select(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		List<FieldMappingParam> list = componentDimensionService.selectFieldMapping(applicationId);
		return execSuccess(list);
	}

	/**
	 * 映射配置新增
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "映射配置新增", notes = "映射配置新增")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "componentIds", value = "组件ids", dataType = "int", allowMultiple = true, paramType = "query"),
			@ApiImplicitParam(name = "dimensionIds", value = "维度ids", dataType = "int", allowMultiple = true, paramType = "query") })
	@OperateLog(type = OperaterType.INSERT, desc = "映射配置新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> add(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		User user = super.getSessionUser(req);
		String loginName = user.getLoginName();
		componentDimensionService.addFieldMapping(map, loginName);
		//sys_notice
        noticeService.insertNoticeByParam(2001,"组件映射新建",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 映射配置修改
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "映射配置编辑", notes = "映射配置编辑")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "componentIds", value = "组件ids", dataType = "int", allowMultiple = true, paramType = "query"),
			@ApiImplicitParam(name = "dimensionIds", value = "维度ids", dataType = "int", allowMultiple = true, paramType = "query"),
			@ApiImplicitParam(name = "id", value = "id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.UPDATE, desc = "映射配置修改")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		User user = super.getSessionUser(req);
		String loginName = user.getLoginName();
		componentDimensionService.editFieldMapping(map, loginName);
		//sys_notice
        noticeService.insertNoticeByParam(2002,"组件映射编辑",0,0);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 映射配置删除
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "映射配置删除", notes = "映射配置删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.DELETE, desc = "映射配置删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		Integer id = evUtil.getMapIntValue(map, "id");
		componentDimensionService.deleteFieldMapping(id);
		//sys_notice
        noticeService.insertNoticeByParam(2003,"组件映射删除",0,0);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 加载组件和维度下拉框
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "加载组件和维度下拉框", notes = "加载组件和维度下拉框")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "applicationId", value = "应用id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/loadselect", method = RequestMethod.POST)
	public JsonResult<Object> loadSelect(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		List<DimensionLableParam> list = componentDimensionService.loadSelect(applicationId);
		return execSuccess(list);
	}

	/**
	 * 映射配置下载
	 * @param req
	 * @param resp
	 * @param map
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(value = "映射配置下载", notes = "映射配置下载")
	@RequestMapping(value = "/download/{configId}", method = RequestMethod.GET)
	public JsonResult<Object> download(HttpServletRequest req, HttpServletResponse resp,
			@ApiParam(required = true, name = "configId", value = "配置id") @PathVariable int configId)
			throws IOException {
		// 查出需要下载的真实数据
		List<FieldMapping> FieldMapping = componentDimensionService.selectMapping(configId);
		// 第一步，创建一个webbook，对应一个Excel文件
		XSSFWorkbook wb = new XSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		XSSFSheet sheet = wb.createSheet("翻译明细");
		// 第三步，在sheet中添加表头第0行
		XSSFRow row = sheet.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		XSSFCell cell = row.createCell((short) 0);
		cell.setCellValue("待翻译值");
		cell.setCellStyle(style);
		cell = row.createCell((short) 1);
		cell.setCellValue("翻译");
		cell.setCellStyle(style);

		// 第五步，写入实体数据
		for (int i = 0; i < FieldMapping.size(); i++) {
			row = sheet.createRow((int) i + 1);
			FieldMapping detail = FieldMapping.get(i);
			// 第四步，创建单元格，并设置值
			row.createCell((short) 0).setCellValue(detail.getValue());
			row.createCell((short) 1).setCellValue(detail.getMapping());
		}
		// 第六步，将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=translateDetail.xlsx");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			wb.write(os);
			// 这里主要关闭。
			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}

	/**
	 * 映射配置上传
	 * @param file
	 * @param req
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "映射配置上传", notes = "映射配置上传")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<Object> upload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req, FieldMappingCnf fieldmappingcnf) throws IOException {
		List<FieldMapping> details = new ArrayList<>();
		String fileName = file.getOriginalFilename();
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		if (!"xlsx".equals(fieldType)) {
			return execCheckError("文件上传失败！文件类型错误");
		}
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}
		InputStream in = file.getInputStream();
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(in);
		XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
		if (xssfSheet == null) {
			xssfWorkbook.close();
			return execCheckError("文件解析失败！请确认是否是下载的文件类型");
		}
		int configId = fieldmappingcnf.getId();
		int applictionId = fieldmappingcnf.getApplicationId();
		for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
			XSSFRow xssfRow = xssfSheet.getRow(i);
			if (xssfRow != null) {
				FieldMapping detail = new FieldMapping();
				XSSFCell value = xssfRow.getCell(0);
				XSSFCell mapping = xssfRow.getCell(1);
				String valueStr = "";
				String mappingStr = "";

				if (value == null && mapping == null) {
					continue;
				} else if (mapping == null) {
					xssfWorkbook.close();
					return execCheckError("第" + i + "行翻译为空");
				} else if (value == null) {
					xssfWorkbook.close();
					return execCheckError("第" + i + "行待翻译值为空");
				}

				if (value.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					long n = (long) value.getNumericCellValue();
					valueStr = n + "";
				} else if (value.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					valueStr = value.getStringCellValue();
				}

				if (mapping.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					long n = (long) mapping.getNumericCellValue();
					mappingStr = n + "";
				} else if (mapping.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					mappingStr = mapping.getStringCellValue();
				}
				detail.setConfigId(configId);
				detail.setMapping(mappingStr);
				detail.setValue(valueStr);
				details.add(detail);
			}
		}
		if (details.size() > 0) {
			User sessionUser = getSessionUser(req);
			componentDimensionService.insertFieldMapping(details, sessionUser.getLoginName(), configId, applictionId);
			//sys_notice
	        noticeService.insertNoticeByParam(2004,"组件映射上传",0,0);
		}
		xssfWorkbook.close();
		return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
	}
}
