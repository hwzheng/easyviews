package com.greattimes.ev.bpm.controller.config;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.AlarmParam;
import com.greattimes.ev.bpm.entity.AlarmRule;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.permission.RequirePermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.service.config.IAlarmRuleService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 告警规则表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */


@RestController
@RequestMapping("/bpm/alarm/config")
@Api(value="AlarmRuleController",tags="告警配置")
@RequirePermission("alarmSetting")
public class AlarmRuleController extends BaseController {


	@Autowired
	private IAlarmRuleService alarmRuleService;
	@Autowired
	private INoticeService noticeService;
	@Autowired
	private ConfigurationCache configurationCache;

	/**
	 * 告警查询
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警查询", notes="告警查询")
	@ApiImplicitParams({
			@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="type",value="告警类型",dataType="int", paramType = "query")})
	@RequestMapping(value = "/application/query", method = RequestMethod.POST)
	public JsonResult<Object> select(@RequestBody Map<String, Object> map) {
		List<Map<String, Object>> list = alarmRuleService.selectAlarmRule(map);
		return execSuccess(list);
	}

	/**
	 * 告警删除
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警删除", notes="告警删除")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "告警删除")
	@RequestMapping(value = "/application/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		AlarmRule alarmRule = alarmRuleService.selectById(id);

		alarmRuleService.deleteAlarmRule(id);
		// sys_notice
		insertNoticeByRuleType(alarmRule.getType(), 3);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 激活状态更新
	 * @author NJ
	 * @date 2018/6/7 14:36
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="激活状态更新", notes="激活状态更新")
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "激活状态更新")
	@RequestMapping(value = "/application/active", method = RequestMethod.POST)
	public JsonResult<Object> active(@RequestBody JSONObject param) {
		alarmRuleService.updateActive(param.getIntValue("id"),param.getIntValue("active"));
		AlarmRule alarmRule = alarmRuleService.selectById(param.getIntValue("id"));
		insertNoticeByRuleType(alarmRule.getType(), 4);
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}

	/**
	 * 激活状态批量更新
	 * @author NJ
	 * @date 2018/6/7 14:41
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="激活状态批量更新", notes="激活状态批量更新")
	@ApiImplicitParams({
			@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query"),
			@ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "激活状态批量更新")
	@RequestMapping(value = "/application/active/batch", method = RequestMethod.POST)
	public JsonResult<Object> batchActive(@RequestBody JSONObject param) {
		alarmRuleService.updateBatchActive(param.getIntValue("applicationId"),param.getIntValue("active"),
				param.getIntValue("type"));
		insertNoticeByRuleType(param.getIntValue("type"), 4);
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}



	/**
	 * 应用告警详情查询
	 * @author nj
	 * @date 2018/6/4 14:28
	 * @param param
	 * @return com.greattimes.ev.common.model.jsonresult<java.lang.object>
	 */
	@ApiOperation(value="应用告警详情查询", notes="应用告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value="/application/detail" , method = RequestMethod.POST)
	public JsonResult<Object> getApplicationAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getApplicationAlarmDetail(param.getIntValue("id"),Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}
	/**
	 * 组件告警详情查询
	 * @author NJ  
	 * @date 2018/6/4 14:28  
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>  
	 */
	@ApiOperation(value="组件告警详情查询", notes="组件告警详情查询", response = AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value="/component/detail", method = RequestMethod.POST)
	public JsonResult<Object> getComponentAlarmDetail(@RequestBody JSONObject param) {
		AlarmParam result = alarmRuleService.getComponentAlarmDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(",")));
		if(result == null){
			return execCheckError(Constant.HAS_NO_RECORDS);
		}
		return execSuccess(result);
	}

	/**
	 * ip告警详情查询
	 * @author NJ
	 * @date 2018/6/4 18:52
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="ip告警详情查询", notes="ip告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value="/ip/detail",method = RequestMethod.POST )
	public JsonResult<Object> getIpAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getIpAlarmDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/***
	 * ip/port告警详情查询
	 * @author NJ
	 * @date 2018/6/4 18:53
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="ip/port告警详情查询", notes="ip/port告警详情查询", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value="/port/detail", method = RequestMethod.POST)
	public JsonResult<Object> getIpPortAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getIpPortAlarmDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 单维度告警详情
	 * @author NJ
	 * @date 2018/6/4 18:53
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="单维度告警详情", notes="单维度告警详情", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/dimension/single/detail", method = RequestMethod.POST)
	public JsonResult<Object> getSingleDimensionAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getSingleDimensionDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 多维度告警详情
	 * @param param
	 * @return
	 */
	@ApiOperation(value="多维度告警详情", notes="多维度告警详情", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/dimension/mulit/detail", method = RequestMethod.POST)
	public JsonResult<Object> getMultiDimensionAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getMultiDimensionDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 应用告警保存
	 * @author NJ
	 * @date 2018/6/4 9:59
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@Deprecated
	@ApiOperation(value="应用告警保存", notes="应用告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "应用告警保存")
	@RequestMapping(value = "/application/save", method = RequestMethod.POST)
	public JsonResult<Object> addAppAlarm(@RequestBody @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_APPLICATION);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	/**
	 *  组件告警保存
	 * @author NJ
	 * @date 2018/6/11 15:38
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="组件告警保存", notes="组件告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "组件告警保存")
	@RequestMapping(value = "/component/save", method = RequestMethod.POST)
	public JsonResult<Object> addComponentAlarm(@RequestBody @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_COMPONENT);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	/**
	 * ip告警保存
	 * @author NJ
	 * @date 2018/6/11 15:38
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="ip告警保存", notes="ip告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "ip告警保存")
	@RequestMapping(value = "/ip/save", method = RequestMethod.POST)
	public JsonResult<Object> addIpAlarm(@RequestBody  @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_IP);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	/**
	 * ip/port告警保存
	 * @author NJ
	 * @date 2018/6/11 15:38
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="ip/port告警保存", notes="ip/port告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "ip/port告警保存")
	@RequestMapping(value = "/port/save", method = RequestMethod.POST)
	public JsonResult<Object> addPortAlarm(@RequestBody @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_IPPORT);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	/**
	 * 单维度告警保存
	 * @author NJ
	 * @date 2018/6/11 15:38
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="单维度告警保存", notes="单维度告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "单维度告警保存")
	@RequestMapping(value = "/dimension/single/save", method = RequestMethod.POST)
	public JsonResult<Object> addSingleDimensionAlarm(@RequestBody @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_SINGLEDIMENSION);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	/**
	 * 多维度告警保存
	 * @author NJ
	 * @date 2018/6/11 15:39
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="多维度告警保存", notes="多维度告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "多维度告警保存")
	@RequestMapping(value = "/dimension/mulit/save", method = RequestMethod.POST)
	public JsonResult<Object> addMulitDimensionAlarm(@RequestBody @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_MULTIEDIMENSION);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 监控点告警保存
	 * @author NJ
	 * @date 2018/8/15 14:20
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@Deprecated
	@ApiOperation(value="监控点告警保存", notes="监控点告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "监控点告警保存")
	@RequestMapping(value = "/monitor/save", method = RequestMethod.POST)
	public JsonResult<Object> addMonitorAlarm( @RequestBody @Validated AlarmParam param) {
		param.setType(Constant.ALARMTYPE_MONITOR);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 数据源告警保存
	 * @author NJ
	 * @date 2018/8/15 14:22
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="数据源告警保存", notes="数据源告警保存")
	@OperateLog(type = OperaterType.INSERT, desc = "数据源告警保存")
	@RequestMapping(value = "/datasoruce/save", method = RequestMethod.POST)
	public JsonResult<Object> addDataSourceAlarm(@Validated @RequestBody AlarmParam param) {
		param.setType(Constant.ALARMTYPE_DATASOURCE);
		saveOrUpdateAlarm(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 数据源告警详情
	 * @param param
	 * @return
	 */
	@ApiOperation(value="数据源告警详情", notes="数据源告警详情", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/datasoruce/detail", method = RequestMethod.POST)
	public JsonResult<Object> getDataSourceAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getDataSourceAlarmDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}

	/**
	 * 监控点告警详情
	 * @author NJ
	 * @date 2018/8/15 14:20
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value="监控点告警详情", notes="监控点告警详情", response =AlarmParam.class)
	@ApiImplicitParams({
			@ApiImplicitParam(name="id",value="告警id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/monitor/detail", method = RequestMethod.POST)
	public JsonResult<Object> getMonitorAlarmDetail(@RequestBody JSONObject param) {
		return execSuccess(alarmRuleService.getMonitorDimensionDetail(param.getIntValue("id"),
				Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","))));
	}


	/**
	 * 分别调用新增和修改接口
	 * @param param
	 */
	private void saveOrUpdateAlarm(AlarmParam param){
		List<String> indicatorList  =  Arrays.asList(configurationCache.getValue(ConfigConstants.INDICATOR_RATE_IDS).split(","));
		if(param.getId() == null || param.getId().intValue() <= 0){
			alarmRuleService.addAlarm(param, indicatorList);
			insertNoticeByRuleType(param.getType(), 1);
		}else{
			alarmRuleService.updateAlarm(param, indicatorList);
			insertNoticeByRuleType(param.getType(), 2);
		}
	}

	/**
	 * 根据告警的类型调用不同的通知
	 * @param ruleType
	 * @param type l 新增  2 编辑 3 删除 4 激活
	 */
	private void insertNoticeByRuleType(int ruleType, int type){
		//告警类型 1 应用 ，2 组件， 3ip， 4port，5单位维度 ，6 多维度 ，7 监控点告警 ,
		//8 数据源告警；9自定义-- 业务 ，10自定义-- 统计维度， 11自定义-- 普通维度， 12自定义--  多维度',
		if(type == 1){
			//新增
			switch (ruleType){
				case 2:
					noticeService.insertNoticeByParam(1301,"组件告警创建",0,0);
					break;
				case 3:
					noticeService.insertNoticeByParam(1401,"组件ip告警创建",0,0);
					break;
				case 4:
					noticeService.insertNoticeByParam(1501,"组件port告警创建",0,0);
					break;
				case 5:
					noticeService.insertNoticeByParam(1601,"组件单维度告警创建",0,0);
					break;
				case 6:
					noticeService.insertNoticeByParam(1701,"组件多维度告警创建",0,0);
					break;
				case 8:
					noticeService.insertNoticeByParam(1801,"数据源告警创建",0,0);
					break;
				case 9:
					noticeService.insertNoticeByParam(1901,"事件告警创建",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3101,"事件统计维度告警创建",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3201,"事件普通维度告警创建",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3301,"事件多级维度告警创建",0,0);
					break;
				default:
					break;
			}
		}else if(type == 2){
			// 编辑
			switch (ruleType){
				case 2:
					noticeService.insertNoticeByParam(1302,"组件告警编辑",0,0);
					break;
				case 3:
					noticeService.insertNoticeByParam(1402,"组件ip告警编辑",0,0);
					break;
				case 4:
					noticeService.insertNoticeByParam(1502,"组件port告警编辑",0,0);
					break;
				case 5:
					noticeService.insertNoticeByParam(1602,"组件单维度告警编辑",0,0);
					break;
				case 6:
					noticeService.insertNoticeByParam(1702,"组件多维度告警编辑",0,0);
					break;
				case 8:
					noticeService.insertNoticeByParam(1802,"数据源告警编辑",0,0);
					break;
				case 9:
					noticeService.insertNoticeByParam(1902,"事件告警编辑",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3102,"事件统计维度告警编辑",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3202,"事件普通维度告警编辑",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3302,"事件多级维度告警编辑",0,0);
					break;
				default:
					break;
			}
		}
		if(type == 3){
			switch (ruleType) {
				case 2:
					noticeService.insertNoticeByParam(1303,"组件告警删除",0,0);
					break;
				case 3:
					noticeService.insertNoticeByParam(1403,"组件ip告警删除",0,0);
					break;
				case 4:
					noticeService.insertNoticeByParam(1503,"组件port告警删除",0,0);
					break;
				case 5:
					noticeService.insertNoticeByParam(1603,"组件单维度告警删除",0,0);
					break;
				case 6:
					noticeService.insertNoticeByParam(1703,"组件多维度告警删除",0,0);
					break;
				case 8:
					noticeService.insertNoticeByParam(1803,"数据源告警删除",0,0);
					break;
				case 9:
					noticeService.insertNoticeByParam(1903,"事件告警删除",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3103,"事件统计维度告警删除",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3203,"事件普通维度告警删除",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3303,"事件多级维度告警删除",0,0);
					break;
				default:
					break;
			}
		}else if(type == 4){
			switch (ruleType) {
				case 2:
					noticeService.insertNoticeByParam(1304,"组件告警激活",0,0);
					break;
				case 3:
					noticeService.insertNoticeByParam(1404,"组件ip告警激活",0,0);
					break;
				case 4:
					noticeService.insertNoticeByParam(1504,"组件port告警激活",0,0);
					break;
				case 5:
					noticeService.insertNoticeByParam(1604,"组件单维度告警激活",0,0);
					break;
				case 6:
					noticeService.insertNoticeByParam(1704,"组件多维度告警激活",0,0);
					break;
				case 8:
					noticeService.insertNoticeByParam(1804,"数据源告警激活",0,0);
					break;
				case 9:
					noticeService.insertNoticeByParam(1904,"事件告警激活",0,0);
					break;
				case 10:
					noticeService.insertNoticeByParam(3104,"事件统计维度告警激活",0,0);
					break;
				case 11:
					noticeService.insertNoticeByParam(3204,"事件普通维度告警激活",0,0);
					break;
				case 12:
					noticeService.insertNoticeByParam(3304,"事件多级维度告警激活",0,0);
					break;
				default:
					break;
			}
		}
	}

}
