package com.greattimes.ev.bpm.controller.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.req.MultiScheduleDetailParam;
import com.greattimes.ev.bpm.config.param.req.MultiScheduleParam;
import com.greattimes.ev.bpm.service.config.IScheduleService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 重复排期表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
@RestController
@Api(value="ScheduleRepeatController",tags="重复排期管理模块")
@RequestMapping("/bpm/application/schedule/multi")
public class ScheduleRepeatController extends BaseController{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IScheduleService scheduleService;
	@Autowired
	private INoticeService noticeService;

	/**
	 * 重复排期列表查询
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="重复排期列表查询", notes="重复排期列表查询" , response =MultiScheduleDetailParam.class ,responseContainer="List")
	@ApiImplicitParams({
		@ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> select(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		List<MultiScheduleDetailParam> list = scheduleService.selectMultiSchedule(applicationId);
		return execSuccess(list);
	}

	/**
	 * 重复排期保存
	 * @param req
	 * @param param
	 * @return
	 */
	@RequirePermission("alarmSetting")
	@ApiOperation(value="重复排期保存", notes="重复排期保存")
	@OperateLog(type = OperaterType.INSERT, desc = "重复排期保存")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> save(HttpServletRequest req, @RequestBody MultiScheduleParam param) {
		scheduleService.saveMultiSchedule(param);
		//sys_notice
        noticeService.insertNoticeByParam(2204,"重复排期保存",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
}
