package com.greattimes.ev.bpm.controller.analysis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.req.ExtendChooseParam;
import com.greattimes.ev.bpm.service.analysis.IKpiService;
import com.greattimes.ev.common.model.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author cgc
 *
 */
@RestController
@RequestMapping(value = "/bpm/kpi")
@Api(value="KpiController",tags="kpi分析")
public class KpiController  extends BaseController {

    @Autowired
    private IKpiService kpiService;
    
    
    /**  
     * 指标类型查询 
     * @author cgc  
     * @date 2018年9月20日  
     * @return
     */
    @ApiOperation(value="指标类型查询", notes="指标类型查询")
    @RequestMapping(value = "/indicator/type", method = RequestMethod.GET)
    public JsonResult<Object> getIndicatorGroup() {
        return execSuccess(kpiService.getIndicatorGroup());
    }
    
    /**  
     * 第三方指标下拉列表查询
     * @author CGC
     * @return
     */
    @ApiOperation(value="第三方指标下拉列表查询", notes="第三方指标下拉列表查询")
    @RequestMapping(value = "/extend/choose", method = RequestMethod.POST)
    public JsonResult<Object> getExtendChoose(@RequestBody ExtendChooseParam param) {
        return execSuccess(kpiService.getExtendChoose(param));
    }

}
