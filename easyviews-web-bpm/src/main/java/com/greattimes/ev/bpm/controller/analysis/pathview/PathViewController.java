package com.greattimes.ev.bpm.controller.analysis.pathview;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.permission.RequirePermission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.PathArea;
import com.greattimes.ev.bpm.entity.PathLine;
import com.greattimes.ev.bpm.entity.PathNode;
import com.greattimes.ev.bpm.service.analysis.IPathViewConfigService;
import com.greattimes.ev.common.model.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;


/**
 * @author NJ
 * @date 2018/9/11 19:56
 */
@RestController
@RequestMapping(value = "/bpm/analysis/pathview/config")
@Api(value="PathViewController",tags="路径图配置")
public class PathViewController  extends BaseController {

    @Autowired
    private IPathViewConfigService pathViewConfigService;
    
    /**    
     *  路径图详情
     * @author NJ  
     * @date 2018/9/11 20:02
     * @param jp  
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>  
     */
    @RequirePermission("baseline")
    @ApiOperation(value="路径图详情", notes="路径图详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name="applicationId",value="应用id",dataType="int", paramType = "query")})
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public JsonResult<Object> findPathDetail(@RequestBody JSONObject jp) {
        return execSuccess(pathViewConfigService.findPathViewDetail(jp.getIntValue("applicationId")));
    }

    /**
     * 路径图保存
     * @author NJ
     * @date 2018/9/11 20:05
     * @param jp
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("pathviewoption")
    @ApiOperation(value="路径图保存", notes="路径图保存")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public JsonResult<Object> savePathView(@RequestBody JSONObject jp) {
        JSONArray lineArray = jp.getJSONArray("pathLine");
        JSONArray nodeArray = jp.getJSONArray("pathNode");
        JSONArray pathArray = jp.getJSONArray("pathArea");
        Integer applicationId = jp.getInteger("applicationId");
        if(lineArray == null || nodeArray == null || pathArray == null
                || applicationId == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        Map<String,Object> param = new HashMap<>();
        param.put("pathLine", lineArray.toJavaList(PathLine.class));
        param.put("pathNode", nodeArray.toJavaList(PathNode.class));
        param.put("pathArea", pathArray.toJavaList(PathArea.class));
        param.put("applicationId",applicationId);
        param.put("type", 1);
        pathViewConfigService.savePathViewConfig(param);
        return execSuccess(Constant.SAVE_SUCCESS_MSG);
    }

    /**
     * 应用树查询
     * @author NJ
     * @date 2018/9/13 11:34
     * @param request
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("pathviewoption")
    @ApiOperation(value="应用树查询", notes="应用树查询")
    @RequestMapping(value = "/tree", method = RequestMethod.POST)
    public JsonResult<Object> findApplicationTree(HttpServletRequest request, @RequestBody JSONObject jp) {
        JSONArray jsonArray = jp.getJSONArray("applicationIds");
        List<Integer> applicationIds = new ArrayList<>();
        if(jsonArray != null){
            applicationIds = jp.getJSONArray("applicationIds").toJavaList(Integer.class);
        }
        return execSuccess(pathViewConfigService.findApplicationTree(getSessionUser(request).getId(),applicationIds));
    }

}
