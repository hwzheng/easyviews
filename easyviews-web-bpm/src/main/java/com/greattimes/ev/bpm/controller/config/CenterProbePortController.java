package com.greattimes.ev.bpm.controller.config;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.bpm.config.param.resp.CenterProbePortParam;
import com.greattimes.ev.bpm.entity.CenterProbePort;
import com.greattimes.ev.bpm.service.config.ICenterService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 探针口表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-22
 */
@Deprecated
@RestController
@Api(value="CenterProbePortController",tags="探针口管理模块")
@RequestMapping("/bpm/centerprobeport")
@RequirePermission("center")
public class CenterProbePortController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ICenterService iCenterService;

	
	/**
	 * 查询
	 * @param req
	 * @return
	 */
	@ApiOperation(value="查询", notes="查询", response = CenterProbePortParam.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public JsonResult<Object> selectAll(HttpServletRequest req) {
		return execSuccess(iCenterService.selectProbePort(null));
	}

	/**
	 * 详情
	 * @param req
	 * @return
	 */
	@ApiOperation(value="详情", notes="详情", response = CenterProbePortParam.class, responseContainer = "List")
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectDetail(@RequestBody CenterProbePortParam param) {
		return execSuccess(iCenterService.selectProbePort(param.getId()));
	}
	
	/**
	 * 新增
	 * @param req
	 * @param center
	 * @return
	 */
	@ApiOperation(value="新增", notes="新增")
	@OperateLog(type=OperaterType.INSERT,desc="探针口新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> savePort(HttpServletRequest req, @RequestBody CenterProbePort port) {
		iCenterService.save(port);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 编辑
	 */
	@ApiOperation(value="编辑", notes="编辑")
	@OperateLog(type=OperaterType.UPDATE,desc="探针口编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> editPort(HttpServletRequest req, @RequestBody CenterProbePort port) {
		iCenterService.update(port);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 删除
	 */
	@ApiOperation(value="删除", notes="删除")
	@OperateLog(type=OperaterType.DELETE,desc="探针口删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deletePort(HttpServletRequest req, @RequestBody CenterProbePort port) {
		Integer id = port.getId();
		iCenterService.deletePort(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
}
