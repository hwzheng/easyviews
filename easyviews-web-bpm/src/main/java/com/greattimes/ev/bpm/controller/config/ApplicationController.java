package com.greattimes.ev.bpm.controller.config;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.config.param.resp.MonitorParam;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


/**
 * @author NJ
 * @date 2018/5/25 14:41
 */
@RestController
@Api(value="ApplicationController",tags="应用管理")
@RequestMapping("/bpm/application")
public class ApplicationController extends BaseController{

    @Autowired
    private IApplicationService iApplicationService;
    @Autowired
    private IDeleteRuleService deleteRuleService;
    @Autowired
    private INoticeService noticeService;
    @Autowired
    private ConfigurationCache configurationCache;

    /**
     *  查询用户应用列表
     * @author NJ
     * @date 2018/5/25 16:27
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("common")
    @ApiOperation(value="查询用户应用列表", notes="查询用户应用列表" , response =ApplicationParam.class, responseContainer="List")
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户id",dataType="int", paramType = "query")})
    @RequestMapping(value = "/select", method = RequestMethod.POST)
    public JsonResult<Object> findList(@RequestBody JSONObject param){
        return execSuccess(iApplicationService.selectApplicationByUserId(param.getIntValue("userId")));
    }
    
    /**
     *  查询监控状态on的应用
     * @author cgc
     * @date 2018/5/25 16:27
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("common")
    @ApiOperation(value="查询监控状态on的应用", notes="查询监控状态on的应用" , response =ApplicationParam.class, responseContainer="List")
    @ApiImplicitParams({
            @ApiImplicitParam(name="userId",value="用户id",dataType="int", paramType = "query")})
    @RequestMapping(value = "/active/select", method = RequestMethod.POST)
    public JsonResult<Object> findActive(@RequestBody JSONObject param){
        return execSuccess(iApplicationService.selectActiveApplication(param.getIntValue("userId")));
    }
    
    /**
     * 更新应用激活状态
     * @author NJ
     * @date 2018/5/25 16:27
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("appSetting")
    @ApiOperation(value="更新应用激活状态", notes="更新应用激活状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name="active",value="激活状态",dataType="int"),
            @ApiImplicitParam(name="id",value="应用id",dataType="int")})
    @OperateLog(type = OperaterType.UPDATE, desc = "更新应用激活状态")
    @RequestMapping(value = "/active", method = RequestMethod.POST)
    public JsonResult<Object> active(@RequestBody JSONObject param) {
        iApplicationService.updateActive(param.getIntValue("id"), param.getIntValue("active"));
        //sys_notice
        noticeService.insertNoticeByParam(1004,"应用激活状态修改",0,0);
        noticeService.insertNoticeByParam(1004,"应用激活状态修改",1,0);
        return execSuccess(Constant.STATE_SUCCESS_MSG);
    }
    /**
     * 批量更新应用激活状态
     * @author NJ
     * @date 2018/6/7 13:59
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("appSetting")
    @ApiOperation(value="批量更新应用激活状态", notes="批量更新应用激活状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name="active",value="激活状态",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="userId",value="用户id",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.UPDATE, desc = "批量更新应用激活状态")
    @RequestMapping(value = "/active/batch", method = RequestMethod.POST)
    public JsonResult<Object> batchActive(@RequestBody JSONObject param){
        iApplicationService.updateBatchActive(param.getIntValue("userId"),param.getIntValue("active"));
        //sys_notice
        noticeService.insertNoticeByParam(1004,"应用激活状态修改",0,0);
        noticeService.insertNoticeByParam(1004,"应用激活状态修改",1,0);
        return execSuccess(Constant.STATE_SUCCESS_MSG);
    }

    /**
     * 更新应用告警状态
     * @author NJ
     * @date 2018/5/25 16:42
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("appSetting")
    @ApiOperation(value="更新应用告警状态", notes="更新应用告警状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmState",value="告警状态",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="id",value="应用id",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.UPDATE, desc = "更新应用告警状态")
    @RequestMapping(value = "/alarm/state", method = RequestMethod.POST)
    public JsonResult<Object> updateAlarmStatus(@RequestBody JSONObject param){
        iApplicationService.updateAlarmStatus(param.getIntValue("id"),param.getIntValue("alarmState"));
        //sys_notice
        noticeService.insertNoticeByParam(1005,"应用告警状态修改",0,0);
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }

    /**
     * 批量更新应用告警状态
     * @author NJ
     * @date 2018/6/11 15:45
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("appSetting")
    @ApiOperation(value="批量更新应用告警状态", notes="批量更新应用告警状态")
    @ApiImplicitParams({
            @ApiImplicitParam(name="alarmState",value="告警状态",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="userId",value="用户id",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.UPDATE, desc = "批量更新应用告警状态")
    @RequestMapping(value = "/alarm/state/batch", method = RequestMethod.POST)
    public JsonResult<Object> batchState(@RequestBody JSONObject param){
        iApplicationService.updateBatchAlarmStatus(param.getIntValue("userId"),param.getIntValue("alarmState"));
        return execSuccess(Constant.STATE_SUCCESS_MSG);
    }

    /**
     * 批量更新排序值
     * @author NJ
     * @date 2018/5/25 17:43
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("appSetting")
    @ApiOperation(value="批量更新排序值", notes="批量更新排序值")
    @ApiImplicitParams({
            @ApiImplicitParam(name="ids",value="应用id数组",dataType = "int", allowMultiple = true, paramType = "query")})
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public JsonResult<Object> updateSort(@RequestBody JSONObject param){
        JSONArray array = param.getJSONArray("ids");
        iApplicationService.updateSort(array.toJavaList(ApplicationParam.class));
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }

    /**
     * 应用删除
     * @author NJ
     * @date 2018/5/25 17:48
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("appSetting")
    @ApiOperation(value="应用删除", notes="应用删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="应用id数组",dataType="int")})
    @OperateLog(type = OperaterType.DELETE, desc = "应用删除")
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public JsonResult<Object> delete(@RequestBody JSONObject param){
        iApplicationService.delete(param.getIntValue("id"));
        //sys_notice
        noticeService.insertNoticeByParam(1003,"应用删除(逻辑)",0,0);
        noticeService.insertNoticeByParam(1003,"应用删除(逻辑)",1,0);
        return execSuccess(Constant.DELETE_SUCCESS_MSG);
    }


    /**
     * 应用新增
     * @author NJ
     * @date 2018/5/25 17:52
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="应用新增", notes="应用新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="应用名称",dataType="string"),
            @ApiImplicitParam(name="centerIds",value="中心id",dataType = "int", allowMultiple = true, paramType = "query"),
            @ApiImplicitParam(name="centerIds",value="中心id",dataType = "int", allowMultiple = true, paramType = "query"),
    })
    @OperateLog(type = OperaterType.INSERT, desc = "应用新增")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JsonResult<Object> add(HttpServletRequest request, @Validated @RequestBody ApplicationParam param){
        String name = param.getName();
        if(iApplicationService.isHasSameAppName(-1,name)){
            return execCheckError("应用名称不能重复！");
        }
        Integer active = param.getActive();
        Integer alarmState = param.getAlarmState();
        if(active == null || alarmState == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        User user = getSessionUser(request);
        int id = iApplicationService.add(user.getId(), param);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        return execSuccess(jsonObject);
    }

    /**
     * 应用修改
     * @author NJ
     * @date 2018/5/28 18:05
     * @param request
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="应用修改", notes="应用修改")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="应用名称",dataType="string"),
            @ApiImplicitParam(name="centerIds",value="中心id",dataType = "int", allowMultiple = true, paramType = "query"),
            @ApiImplicitParam(name="id",value="应用id",dataType="int")
    })
    @OperateLog(type = OperaterType.UPDATE, desc = "应用修改")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public JsonResult<Object> edit(HttpServletRequest request, @Validated @RequestBody ApplicationParam param){
        String name = param.getName();
        int id = param.getId();
        if(iApplicationService.isHasSameAppName(id,name)){
            return execCheckError("应用名称不能重复！");
        }
        User user = getSessionUser(request);
        Integer active = param.getActive();
        Integer alarmState = param.getAlarmState();
        if(active == null || alarmState == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        iApplicationService.edit(user.getId(),param);
        //sys_notice
        noticeService.insertNoticeByParam(1002,"应用编辑",0,0);
        return  execSuccess(Constant.EDIT_SUCCESS_MSG);
    }

    /**
     * 根据id获取应用详情
     * @author NJ
     * @date 2018/5/29 10:32
     * @param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="获取应用详情", notes="获取应用详情",  response =ApplicationParam.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="应用id",dataType="int")
    })
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public JsonResult<Object> getDetail(@RequestBody JSONObject param){
        return execSuccess(iApplicationService.getDetail(param.getIntValue("id")));
    }

    /**
     * 监控点列表查询
     * @author NJ
     * @date 2018/8/8 10:56
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="监控点列表", notes="监控点列表",  response = MonitorParam.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="应用id",dataType="int")
    })
    @RequestMapping(value = "/monitor/list")
    public JsonResult<Object> getMonitorList(@RequestBody JSONObject param){
        return execSuccess(iApplicationService.selectMonitorList(param.getIntValue("id")));
    }

    /**
     * 监控点新增
     * @author NJ
     * @date 2018/8/8 10:56
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="监控点新增", notes="监控点新增")
    @ApiImplicitParams({
            @ApiImplicitParam(name="applicationId",value="应用id",dataType="int"),
            @ApiImplicitParam(name="name", value = "监控名称", dataType = "string"),
            @ApiImplicitParam(name="componentNum", value = "组件数量", dataType = "int")
    })
    @RequestMapping(value = "/monitor/add")
    public JsonResult<Object> addMonitor(@RequestBody JSONObject param){
        Integer appId =  param.getInteger("applicationId");
        String name = param.getString("name");
        Integer componentNum =  param.getInteger("componentNum");
        if(iApplicationService.isHasSameMonitorName(appId,-1, name, true)){
            return execCheckError("监控点名称不能重复！");
        }
        boolean result = iApplicationService.saveOrUpdateMonitor(name, componentNum, appId, null);
        if(result){
            return execSuccess(Constant.SAVE_SUCCESS_MSG);
        }else{
            return execCheckError(Constant.SAVE_ERROR_MSG);
        }
    }

    /**
     * 监控点编辑
     * @author NJ
     * @date 2018/8/8 10:56
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="监控点编辑", notes="监控点编辑")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="监控点id",dataType="int"),
            @ApiImplicitParam(name="name", value = "监控名称", dataType = "string"),
            @ApiImplicitParam(name="componentNum", value = "组件数量", dataType = "int")
    })
    @RequestMapping(value = "/monitor/edit")
    public JsonResult<Object> editMonitor(@RequestBody JSONObject param){
        Integer id = param.getIntValue("id");
        String name = param.getString("name");
        Integer componentNum =  param.getInteger("componentNum");
        if(iApplicationService.isHasSameMonitorName(-1, id,  name, false)){
            return execCheckError("监控点名称不能重复！");
        }

        boolean result = iApplicationService.saveOrUpdateMonitor(name, componentNum,null, id);
        if(result){
            //sys_notice
            noticeService.insertNoticeByParam(1102,"监控点编辑",0,0);
            return execSuccess(Constant.EDIT_SUCCESS_MSG);
        }else{
            return execCheckError(Constant.EDIT_ERROR_MSG);
        }
    }

    /**
     *  监控点删除
     * @author NJ
     * @date 2018/8/8 10:56
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="监控点删除", notes="监控点删除")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="监控点id",dataType="int")
    })
    @RequestMapping(value = "/monitor/delete")
    public JsonResult<Object> deleteMonitor(@RequestBody JSONObject param){
        deleteRuleService.deleteMonitor(param.getIntValue("id"));
        //sys_notice
        noticeService.insertNoticeByParam(1103,"监控点删除",0,0);
        noticeService.insertNoticeByParam(1103,"监控点删除",1,0);
        return execSuccess(Constant.DELETE_SUCCESS_MSG);
    }

    /**
     * 更新监控点排序
     * @author NJ
     * @date 2018/5/25 17:43
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="更新监控点排序", notes="更新监控点排序")
    @ApiImplicitParams({
            @ApiImplicitParam(name="ids",value="监控点id",dataType = "int", allowMultiple = true, paramType = "query")})
    @RequestMapping(value = "/monitor/sort", method = RequestMethod.POST)
    public JsonResult<Object> updateMonitorSort(@RequestBody JSONObject param){
        JSONArray array = param.getJSONArray("ids");
        iApplicationService.updateMonitorSort(array.toJavaList(MonitorParam.class));
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }

    /**
     * 监控点详情
     * @author NJ
     * @date 2018/8/8 17:30
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("basicSetting")
    @ApiOperation(value="监控点详情", notes="监控点详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id", value="监控点id",dataType = "int", allowMultiple = true, paramType = "query")})
    @RequestMapping(value = "/monitor/detail", method = RequestMethod.POST)
    public JsonResult<Object> monitorDetail(@RequestBody JSONObject param){
        return execSuccess(iApplicationService.selectMonitorDetail(param.getIntValue("id")));
    }


    @RequestMapping(value = "/find/alarm/id", method = RequestMethod.POST)
    public JsonResult<Object> findAppIdByApplicationAlarm(HttpServletRequest req, @RequestBody JSONObject param){
        Long start = param.getLong("start");
        Long end = param.getLong("end");
        Integer limit = param.getIntValue("limit");
        if(start == null || end == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        User user = super.getSessionUser(req);
        Integer id = user.getId();
        return execSuccess(iApplicationService.findAppIdByAlarmInfo(id,start,end,limit));
    }



}
