package com.greattimes.ev.bpm.controller.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.bpm.service.alarm.IAlarmMassageProducerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.resp.AlarmGroupParam;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.greattimes.ev.bpm.service.config.IAlarmGroupService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 告警信息发送配置表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@RestController
@Api(value="AlarmGroupController",tags="告警组管理模块")
@RequestMapping("/bpm/alarm/group")
@RequirePermission("alarmGroup")
public class AlarmGroupController extends BaseController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IAlarmGroupService alarmGroupService;
	@Autowired
	private IAlarmMassageProducerService iAlarmMassageProducerService;

	/**
	 * 告警组查询
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警组查询", notes="告警组查询" , response =AlarmGroup.class ,responseContainer="List")
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	public JsonResult<Object> select() {
		List<AlarmGroup> list = alarmGroupService.selectList(null);
		return execSuccess(list);
	}

	/**
	 * 告警组新增
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警组新增", notes="告警发送新增")
	@OperateLog(type = OperaterType.INSERT, desc = "告警发送新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> add(HttpServletRequest req, @RequestBody @Validated AlarmGroup alarmGroup) {
		if(alarmGroupService.checkName(alarmGroup,1)) {
			alarmGroupService.insert(alarmGroup);
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		}else {
			return execCheckError("告警组名重复！");
		}
		
	}

	/**
	 * 告警发送编辑
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警组编辑", notes="告警组编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "告警组编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(HttpServletRequest req, @RequestBody @Validated AlarmGroup alarmGroup) {
		if(null==alarmGroup.getId()) {
			return execCheckError("告警组id为空！");
		}
		if(alarmGroupService.checkName(alarmGroup,0)) {
			alarmGroupService.updateById(alarmGroup);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		}else {
			return execCheckError("告警组名重复！");
		}
	}

	/**
	 * 告警组详情
	 * @param req
	 * @return
	 */
	@ApiOperation(value="告警组详情查询", notes="告警组详情查询" , response =AlarmGroupParam.class)
	@ApiImplicitParams({@ApiImplicitParam(name="id",value="告警发送id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	public JsonResult<Object> selectAlarmGroup(HttpServletRequest req, @RequestBody AlarmGroup alarmGroup) {
		return execSuccess(alarmGroupService.selectAlarmGroup(alarmGroup.getId()));
	}

	/**
	 * 告警组删除
	 * @param req 
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警组删除", notes="告警组删除")
	@ApiImplicitParams({
		@ApiImplicitParam(name="id",value="告警发送id",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.DELETE, desc = "告警发送删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int id = evUtil.getMapIntValue(map, "id");
		alarmGroupService.deleteAlarmGroup(id);
		iAlarmMassageProducerService.clearMessageMap(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 告警组状态
	 * @param map
	 * @return
	 */
	@ApiOperation(value="告警组状态", notes="告警组状态")
	@ApiImplicitParams({
		@ApiImplicitParam(name="id",value="告警组id",dataType="int", paramType = "query"),
		@ApiImplicitParam(name="active",value="告警组状态1激活0关闭",dataType="int", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "告警组状态")
	@RequestMapping(value = "/state", method = RequestMethod.POST)
	public JsonResult<Object> updateState(@RequestBody AlarmGroup alarmGroup) {
		alarmGroupService.updateById(alarmGroup);
		iAlarmMassageProducerService.clearMessageMap(alarmGroup.getId());
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}
	
	/**
	 * 告警组详情保存
	 * @param req
	 * @param list
	 * @return
	 */
	@ApiOperation(value="告警组详情保存", notes="告警组详情保存")
	@OperateLog(type = OperaterType.INSERT, desc = "告警组详情保存")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> saveGroup(@RequestBody @Validated AlarmGroupParam param) {
		alarmGroupService.saveGroup(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
}
