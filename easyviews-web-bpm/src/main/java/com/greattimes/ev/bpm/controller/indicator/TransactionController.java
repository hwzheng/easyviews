package com.greattimes.ev.bpm.controller.indicator;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.datasources.param.req.IndicatorGroupParam;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.bpm.service.datasources.IDataSourcesService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.MethodCost;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.ExtendIndicatorParam;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.req.TpsParam;
import com.greattimes.ev.indicator.param.resp.ExtendIndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;
import com.greattimes.ev.indicator.param.resp.TransactionTpsParam;
import com.greattimes.ev.indicator.service.ITransactionService;
import com.greattimes.ev.utils.HttpClientUtil;
import com.greattimes.ev.utils.IndicatorSupportUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * @author cgc
 * @date 2018/9/11 19:56
 */
@RestController
@RequestMapping(value = "/indicator/transaction")
@Api(value="TransactionController",tags="交易指标查询")
public class TransactionController  extends BaseController {

    @Autowired
    private ITransactionService transactionService;

    @Autowired
	private ConfigurationCache configurationCache;

    @Autowired
    private IApplicationService applicationService;
    
    @Autowired
    private ICommonService commonService;
    
    @Autowired
	private IDataSourcesService dataSourcesService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @ApiOperation(value="图查询", notes="图查询",response = IndicatorDataParam.class)
	@MethodCost(desc = "交易指标查询图查询")
    @RequestMapping(value = "/chart", method = RequestMethod.POST)
    public JsonResult<Object> chart(@RequestBody IndicatorParam param) {
    	if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	//int interval=Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
    	IndicatorDataParam para= transactionService.chart(param);
        return execSuccess(para);
    }
    
    @ApiOperation(value="第三方指标图查询", notes="第三方指标图查询",response = ExtendIndicatorDataParam.class)
   	@MethodCost(desc = "第三方指标图查询")
       @RequestMapping(value = "/extend/chart", method = RequestMethod.POST)
       public JsonResult<Object> extendChart(@RequestBody ExtendIndicatorParam param) {
       	if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getInnerUUID())){
               return execCheckError(Constant.PARAMETER_ERROR_MSG);
           }
       	//int interval=Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
        IndicatorGroupParam group=dataSourcesService.indicatorDetail(param.getGroupId());
        Long start = param.getStart();
		Long end = param.getEnd();
        int groupInterval=group.getInterval();
        int unit=group.getUnit();
        int num=1;
        switch (unit) {
		case 1://毫秒
			num=1;
			break;
		case 2://秒
			num=1000;
			break;
		case 3://分钟
			num=1000*60;
			start=DateUtils.longToMinute(start, groupInterval);
			end=DateUtils.longToMinute(end, groupInterval);
			break;
		case 4://小时
			num=1000*3600;
			start=DateUtils.longToHour(start, groupInterval);
			end=DateUtils.longToHour(end, groupInterval);
			break;
		default:
			break;
		}
        param.setInterval(groupInterval*num);
        param.setStart(start);
        param.setEnd(end);
       	ExtendIndicatorDataParam para= transactionService.extendIndicatorChart(param);
        return execSuccess(para);
       }
    
    @ApiOperation(value="基线查询", notes="基线查询",response = IndicatorDataParam.class)
	@MethodCost(desc = "交易指标基线查询")
    @RequestMapping(value = "/baseline", method = RequestMethod.POST)
    public JsonResult<Object> baseline(@RequestBody IndicatorParam param) {
    	if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
    	int timeType=param.getTimeType()==null?0:param.getTimeType();
    	int interval;
    	if(timeType==1) {
    		interval=60000;
    	}else{
    		interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL)) * 1000;
    	}
    	IndicatorDataParam para= transactionService.baseline(param,interval);
        return execSuccess(para);
    }
    
    @ApiOperation(value="清华基线查询", notes="清华基线查询")
	@MethodCost(desc = "清华基线查询")
    @RequestMapping(value = "/baseband", method = RequestMethod.POST)
    public JsonResult<Object> baseband(@RequestBody JSONObject param) {
    	Map<String,Object> result=new HashMap<>();
    	int interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL)) * 1000;
    	List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(DateUtils.longToFiveSecondTimeBehind(param.getLong("start")), DateUtils.longToFiveSecondTime(param.getLong("end")), interval);
    	List<Map<String,Object>> indicatorList=new ArrayList<>();
    	result.put("time", timeList);
    	Map<String,Object> key=(Map<String, Object>) param.get("key");
    	String start=param.getLong("start").toString().substring(0, 10);
    	String end=param.getLong("end").toString().substring(0, 10);
    	List<Integer> indicator=(List<Integer>) param.get("indicator");
    	DecimalFormat df = new DecimalFormat("#.00");
    	if(null==key.get("type")) {
    		 return execSuccess(result);
    	}else {
    		if(evUtil.listIsNullOrZero(indicator)) {
    			return execSuccess(result);
    		}else {
    			for (Integer integer : indicator) {
    				Map<String,Object> indicatorMap=new HashMap<>();
    				indicatorMap.put("id", integer);
    				indicatorMap.put("uuid", key.get("uuid"));
    				key.put("indicatorId", integer);
    				String kpikey=commonService.getKpiKey(1, key);
    				String url=configurationCache.getValue(ConfigConstants.KPI_HOST)+kpikey+"/"+configurationCache.getValue(ConfigConstants.KPI_BASEBAND)+start+","+end;
    				String data = null;
    				try {
    					Map<String, Object> headers=new HashMap<>();
    					headers.put("Content-type", "application/json; charset=utf-8");
    					headers.put("Accept", "application/json;charset=utf-8");
    					logger.info("清华基线查询url:{}",url);
    					data=HttpClientUtil.httpGetRequest(url, headers, null);
    				} catch (Exception e) {
    					//logger.error("清华基线查询--调用远程接口发生异常：" + e.getMessage());
    					return execCheckError("清华基线查询--调用远程接口发生异常");
    					//throw new RuntimeException(e.getMessage());
    				}
    				logger.info("清华基线查询响应:{}",result);
					//String data="{\"code\":200,\"msg\":\"OK\",\"data\":{\"header\":[\"time\",\"value\",\"anomaly\",\"DClassifier:505e3604-d022-4d68-8772-8b271ec42ed0:lower\",\"DClassifier:505e3604-d022-4d68-8772-8b271ec42ed0:upper\"],\"values\":[[1542160800,1502,false,1296.685,1866.685],[1542160860,1601,false,1493.685,1866.685],[1542160920,1601,false,1493.685,1866.685],[1542160980,1601,false,1393.685,1966.685],[1542161040,1601,false,1493.685,1866.685],[1542161100,1601,false,1493.685,1866.685],[1542161160,1601,false,1693.685,1766.685],[1542161220,1601,false,1493.685,1866.685],[1542161280,1601,false,1493.685,1866.685],[1542161340,1601,false,1493.685,1866.685],[1542161400,1601,false,1493.685,1866.685]]}}";
					JSONObject jb;
					try {
						jb = JSON.parseObject(data);
					} catch (Exception e) {
						//logger.error("基带获取解析返回结果发生异常：" + e.getMessage());
						//throw new RuntimeException(e.getMessage());
						return execCheckError("基带获取解析返回结果发生异常");
					}
					
					Map<String, Object> jsonData=(Map<String, Object>) jb.get("data");
					if(MapUtils.isEmpty(jsonData)) {
						return execCheckError("无返回结果！");
					}
					List<String> header=(List<String>) jsonData.get("header");
					List<List<Object>> values=(List<List<Object>>) jsonData.get("values");
					int upperIndex=findIndex("upper",header);
					int lowerIndex=findIndex("lower",header);
					List<String> upper=new ArrayList<>(timeList.size());
					List<String> lower=new ArrayList<>(timeList.size());
					for (List<Object> v : values) {
						String up = null,low = null;
						if(null!=v.get(upperIndex)) {
							double upValue=Double.parseDouble(v.get(upperIndex).toString());
							up=df.format(upValue);
						}
						if(null!=v.get(lowerIndex)) {
							double lowValue=Double.parseDouble(v.get(lowerIndex).toString());
							low=df.format(lowValue);
						}
						upper.add(up);
						lower.add(low);
					}
					indicatorMap.put("upper", upper);
					indicatorMap.put("lower", lower);
					indicatorList.add(indicatorMap);
				}
    			result.put("indicator", indicatorList);
    			return execSuccess(result);
    		}
    	}
        
    }
    
    /**在list中模糊查找string 返回出现的索引
     * @param string
     * @param header
     * @return
     */
    private int findIndex(String string, List<String> header) {
		int index=0;
		for (int i = 0; i < header.size(); i++) {
			if(header.get(i).indexOf(string)!=-1) {
				index=i;
			}
		}
		return index;
	}

	@ApiOperation(value="表查询", notes="表查询",response = TransactionTableParam.class,responseContainer="list")
	@MethodCost(desc = "交易指标查询表查询")
    @RequestMapping(value = "/table", method = RequestMethod.POST)
    public JsonResult<Object> table(HttpServletRequest request, @RequestBody IndicatorParam param) {
    	//parameter check
        if(param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
//        if(param.getIsAuthority() != null && param.getIsAuthority().equals(1)){
//            //是否带有权限查询
//            Set<Long> uuids = applicationService.selectAllUuidByUserId(this.getSessionUser(request).getId());
//            Set<Integer> sets = new HashSet<>();
//            uuids.forEach(x->{
//                sets.add(x.intValue());
//            });
//            param.getUuid().retainAll(sets);
//            param.setUuid(param.getUuid());
//        }

    	//int interval=Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
    	List<TransactionTableParam> para= transactionService.table(param);
        return execSuccess(para);
    }

    /**
     * 应用交易量查询
     * @author NJ
     * @date 2019/7/25 17:10
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="应用交易量查询", notes="应用交易量查询")
	@MethodCost(desc = "应用交易量查询")
    @RequestMapping(value = "/transcount", method = RequestMethod.POST)
    public JsonResult<Object> selectAppTranCount(@RequestBody Map<String, Object> param) {
        return execSuccess(transactionService.selectAppTransByMap(param));
    }

	/**
	 * 只支持最小颗粒度
	 * @param param
	 * @return
	 */
	@ApiOperation(value="组件层级以及以下指标数据", notes="组件层级以及以下指标数据")
	@MethodCost(desc = "组件层级以及以下指标数据")
    @RequestMapping(value = "/component", method = RequestMethod.POST)
    public JsonResult<Object> selectComponentData(@RequestBody Map<String, Object> param) {
        return execSuccess(transactionService.selectTransByMap(param));
    }


	@ApiOperation(value="多维交易追踪钢琴键", notes="多维交易追踪钢琴键")
	@MethodCost(desc = "多维交易追踪钢琴键")
	@RequestMapping(value = "/component/piano", method = RequestMethod.POST)
	public JsonResult<Object> selectComponentPiano(@RequestBody Map<String, Object> param) {
		int type = evUtil.getMapIntValue(param, "type");
		int uuid = evUtil.getMapIntValue(param, "uuid");
		long start = evUtil.getMapLongValue(param, "start");
		long end = evUtil.getMapLongValue(param, "end");
		return execSuccess(transactionService.transCountPiano(type, uuid, start, end));
	}


	@ApiOperation(value="组件聚合指标接口", notes="组件聚合指标接口")
	@MethodCost(desc = "组件聚合指标接口")
	@RequestMapping(value = "/combine/table", method = RequestMethod.POST)
	public JsonResult<Object> selectCombineComponent(@RequestBody IndicatorParam param){
		return execSuccess(transactionService.selectCombineBpmTranTable(param));
	}

	@ApiOperation(value = "bpm指标接口", notes = "bpm指标接口", response = TransactionTableParam.class, responseContainer = "list")
	@MethodCost(desc = "bpm指标接口")
	@RequestMapping(value = "/instant", method = RequestMethod.POST)
	public JsonResult<Object> selectInstant(HttpServletRequest request, @RequestBody IndicatorParam param) {
		// parameter check
		if (param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())) {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
		List<TransactionTableParam> para = transactionService.tableGroupByUuid(param);
		return execSuccess(para);
	}
	
	@ApiOperation(value = "Tps峰值", notes = "Tps峰值")
	@MethodCost(desc = "Tps峰值查询")
	@RequestMapping(value = "/tps", method = RequestMethod.POST)
	public JsonResult<Object> getTpsMax(@RequestBody TpsParam param) {
		if (param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())) {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
		List<TransactionTpsParam> result = transactionService.getTpsMax(param);
		return execSuccess(result);
	}
	
	@ApiOperation(value = "Tps峰值", notes = "Tps峰值")
	@MethodCost(desc = "Tps峰值(聚合)查询")
	@RequestMapping(value = "/combine/tps", method = RequestMethod.POST)
	public JsonResult<Object> getCombineTpsMax(@RequestBody TpsParam param) {
		if (param.getStart() == null || param.getEnd() == null || evUtil.listIsNullOrZero(param.getUuid())) {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
		TransactionTpsParam result = transactionService.getCombineTpsMax(param);
		return execSuccess(result);
	}

}
