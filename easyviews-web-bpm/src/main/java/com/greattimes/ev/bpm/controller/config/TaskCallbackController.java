package com.greattimes.ev.bpm.controller.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.PcapTask;
import com.greattimes.ev.bpm.service.config.IPcapTaskService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * pca下载任务 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-11-07
 */
@RestController
@RequestMapping("/logs")
@Api(value="TaskCallbackController",tags="回调接口")
public class TaskCallbackController extends BaseController{
	@Autowired
    private IPcapTaskService pcapTaskService;
	Logger log = LoggerFactory.getLogger(this.getClass());
	/**下载完成回调（解码请求web）
	 * @param param
	 * @return
	 */
	@ApiOperation(value = "下载完成回调（解码请求web）", notes = "下载完成回调（解码请求web）")
    @RequestMapping(value = "/taskCallBack", method = RequestMethod.POST)
    public JsonResult<Object> callBack(@RequestBody JSONObject param){
		log.info("开始回调---参数:{}",param.toString());
        Integer responseCode = param.getInteger("responseCode");
        Integer taskid= param.getInteger("taskId");
        String responseDesc= param.getString("responseDesc");
        String fileName= param.getString("fileName");
        PcapTask task=new PcapTask();
        task.setId(taskid);
        
        if(responseCode.equals(200)) {
        	task.setState(2);
        	task.setHref(fileName);
        }
        else {
        	task.setState(3);
        	task.setError(responseDesc);
        }
        pcapTaskService.updateById(task);
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }
}

