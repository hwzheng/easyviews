package com.greattimes.ev.bpm.controller.indicator;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.entity.FieldMapping;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.MethodCost;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.DimensionChartParam;
import com.greattimes.ev.indicator.param.req.DimensionPageParam;
import com.greattimes.ev.indicator.param.req.DimensionParam;
import com.greattimes.ev.indicator.service.IBpmPreService;
import com.greattimes.ev.system.service.IDictionaryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author cgc
 * @date 2018/9/26 18:26
 */
@RestController
@Api(value = "MultiimensionController", tags = "多维分析")
@RequestMapping("/multidimension")
public class MultidimensionController extends BaseController{

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IBpmPreService bpmPreService;
    @Autowired
    private ICustomService customService;
    @Autowired
	private IDictionaryService dictionaryService;
    @Autowired
	private ConfigurationCache configurationCache;

    @ApiOperation(value = "多维分析查询表格", notes = "多维分析查询表格")
	@MethodCost(desc = "多维分析查询表格")
    @RequestMapping(value = "/select", method = RequestMethod.POST)
    public JsonResult<Object> selectTable(@RequestBody DimensionParam param) {
        return execSuccess(bpmPreService.selectListForTable(param,null));
    }
    
    @ApiOperation(value = "多维分析查询表格(分页)", notes = "多维分析查询表格(分页)")
    @MethodCost(desc = "多维分析查询表格(分页)")
    @RequestMapping(value = "/table", method = RequestMethod.POST)
    public JsonResult<Object> selectTablePage(@RequestBody DimensionPageParam param){
        Integer pageNumber = param.getPage();
        Integer pageSize= param.getSize();
        Page<Map<String,Object>> page = new Page<>(pageNumber,pageSize);
        return execSuccess(bpmPreService.selectTablePage(page, param,null));
    }

    @ApiOperation(value = "多维分析查询图表类", notes = "多维分析查询图表类")
	@MethodCost(desc = "多维分析查询图表类")
    @RequestMapping(value = "/chart", method = RequestMethod.POST)
    public JsonResult<Object> selectChart(@RequestBody DimensionParam param) {
        return execSuccess(bpmPreService.selectListForChart(param));
    }
    
    @ApiOperation(value = "多维趋势分析图", notes = "多维趋势分析图")
	@MethodCost(desc = "多维趋势分析图")
    @RequestMapping(value = "/analyze/chart", method = RequestMethod.POST)
    public JsonResult<Object> getAnalyzeChart(@RequestBody DimensionChartParam param) {
    	Map<String, Object> result=new HashMap<>();
    	List<Map<String, Object>> indicatorResList=new ArrayList<>();
    	Map<String, Object> map=new HashMap<>(); 
    	DimensionParam p=new DimensionParam();
    	p.setUuid(param.getUuid());
    	p.setType(param.getType());
    	p.setApplicationId(param.getApplicationId());
    	p.setStartTime(param.getStartTime());
    	p.setEndTime(param.getEndTime());
    	p.setCompareStartTime(param.getCompareStartTime());
    	p.setCompareEndTime(param.getCompareEndTime());
    	p.setIndicatorIds(param.getIndicatorIds());
    	List<JSONObject> choose=param.getChoose();
    	if(!evUtil.listIsNullOrZero(choose)) {
    		for (JSONObject jsonObject : choose) {
    			Integer chooseId=jsonObject.getInteger("chooseId");
    			p.setFilterDimension((List<JSONObject>) jsonObject.get("filterDimension"));
    			map=bpmPreService.selectAnalyzeListForChart(p);
    			List<Map<String, Object>> list=(List<Map<String, Object>>) map.get("indicator");
    			for (Map<String, Object> map2 : list) {
					map2.put("chooseId", chooseId);
				}
    			indicatorResList.addAll(list);
			}
    	}
    	result.put("time", map.get("time"));
    	result.put("indicator", indicatorResList);
        return execSuccess(result);
    }
    
    /**
	 * excel下载
	 * @param req
	 * @param resp
	 * @param map
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(value = "excel下载", notes = "excel下载")
	@RequestMapping(value = "/download", method = RequestMethod.POST)
	public JsonResult<Object> download(HttpServletRequest req, HttpServletResponse resp,@RequestBody DimensionPageParam param)
			throws IOException {
		Integer pageNumber = param.getPage();
		Integer pageSize = param.getSize();
		Page<Map<String, Object>> page = new Page<>(pageNumber, pageSize);

		List<String> fixed_dimension = new ArrayList<>();
		// 获取applicationId
		if (param.getType().equals(2)) {
			param.setApplicationId(customService.selectById(param.getUuid()).getApplicationId());
			// 预处理维度的非json字段
			List<Map<String, Object>> result = dictionaryService.selectDicNameAndValueByDictTypeCode("FIXED_DIMENSION");
			if (!evUtil.listIsNullOrZero(result)) {
				for (Map<String, Object> map2 : result) {
					fixed_dimension.add(evUtil.getMapStrValue(map2, "value"));
				}
			}
		}
		Long time=System.currentTimeMillis();
		//获取表头
		List<JSONObject> header=param.getHeader();
		// 查出需要下载的真实数据
		List<Map<String, Object>> list = bpmPreService.selectTablePage(page, param,fixed_dimension).getRecords();
		// 第一步，创建一个webbook，对应一个Excel文件
		XSSFWorkbook wb = new XSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		XSSFSheet sheet = wb.createSheet("维度分析表格");
		// 第三步，在sheet中添加表头第0行
		XSSFRow row = sheet.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
		for (int i = 0; i < header.size(); i++) {
			XSSFCell cell = row.createCell((short) i);
			cell.setCellValue(header.get(i).getString("name"));
			cell.setCellStyle(style);
		}
		

		// 第五步，写入实体数据
		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			Map<String, Object> detail = list.get(i);
			// 第四步，创建单元格，并设置值
			for (int j = 0; j < header.size(); j++) {
				row.createCell((short) j).setCellValue(evUtil.getMapStrValue(detail, header.get(j).getString("ename")));
			}
		}
		// 第六步，将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=Detail.xlsx");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			wb.write(os);
			// 这里主要关闭。
			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}


}
