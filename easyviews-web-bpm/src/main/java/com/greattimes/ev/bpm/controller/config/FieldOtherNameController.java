package com.greattimes.ev.bpm.controller.config;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 别名表 前端控制器
 * </p>
 *
 * @author Cgc
 * @since 2018-05-24
 */
@RestController
@Api(value="FieldOtherNameController",tags="别名配置模块")
@RequestMapping("/bpm/component/othername")
@RequirePermission("analyze")
public class FieldOtherNameController extends BaseController {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IComponentDimensionService componentDimensionService;
	@Autowired
	private INoticeService noticeService;

	/**
	 * 别名查询
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="别名查询", notes="别名查询" , response =DimensionParam.class ,responseContainer="List")
	@ApiImplicitParams({
		  @ApiImplicitParam(name="componentId",value="组件id",dataType="int", paramType = "query")})
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> select(HttpServletRequest req, @RequestBody Map<String, Object> map) {
		int componentId = evUtil.getMapIntValue(map, "componentId");
		List<DimensionParam> list = componentDimensionService.selectOtherName(componentId);
		return execSuccess(list);
	}

	/**
	 *	别名保存
	 * @param req
	 * @param map
	 * @return
	 */
	@ApiOperation(value="别名保存", notes="别名保存")
	@OperateLog(type = OperaterType.INSERT, desc = "别名保存")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> save(HttpServletRequest req, @RequestBody DimensionParam param) {
		String otherName=param.getOtherName();
		if(otherName.length()>10) {
			return execCheckError("别名长度过长，请重新输入");
		}
		componentDimensionService.saveOtherName(param);
		//sys_notice
        noticeService.insertNoticeByParam(2005,"组件别名管理保存",0,0);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	 /**
     * 批量更新排序值
     * @param param
     * @return
     */
    @ApiOperation(value="批量更新排序值", notes="批量更新排序值")
    @ApiImplicitParams({
            @ApiImplicitParam(name="ids",value="维度id数组",dataType = "int", allowMultiple = true, paramType = "query")})
    @RequestMapping(value = "/sort", method = RequestMethod.POST)
    public JsonResult<Object> updateSort(@RequestBody JSONObject param){
        JSONArray array = param.getJSONArray("ids");
        componentDimensionService.updateSort(array.toJavaList(DimensionParam.class));
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }
	
}
