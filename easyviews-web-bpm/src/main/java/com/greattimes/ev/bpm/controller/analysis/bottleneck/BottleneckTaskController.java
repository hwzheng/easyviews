package com.greattimes.ev.bpm.controller.analysis.bottleneck;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.common.logger.Logger;
import com.alibaba.dubbo.common.logger.LoggerFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.analysis.param.resp.MetaParam;
import com.greattimes.ev.bpm.service.analysis.IBottleneckTaskService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.service.IBpmPreService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


/**
 * <p>
 * 智能瓶颈报表任务表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@RestController
@RequestMapping("/api/bottleneck")
@Api(value = "BottleneckTaskController", tags = "瓶颈分析配置")
public class BottleneckTaskController extends BaseController{
	@Autowired
	private IBottleneckTaskService bottleneckTaskService;
	@Autowired
	private IBpmPreService bpmPreService;
	Logger log = LoggerFactory.getLogger(this.getClass());

	/**
	 * @param taskId
	 * @return
	 */
	@RequestMapping(value = "/meta/{taskId}", method = RequestMethod.GET)
	public JsonResult<Object> getMeta(@PathVariable String taskId) {
		List<Map<String, Object>> maxmin=new ArrayList<>();
		// 如果是定时
		if (taskId.length() > 8) {
			taskId = taskId.substring(8, taskId.length());
			List<Map<String, Object>> detail = bottleneckTaskService.getTimeTaskDetail(Integer.parseInt(taskId));
			if(evUtil.listIsNullOrZero(detail)) {
				return execCheckError("未查询到相应配置信息！");
			}
			int customId = evUtil.getMapIntValue(detail.get(0), "customId");
			int componentId = evUtil.getMapIntValue(detail.get(0), "componentId");
			int dataType= evUtil.getMapIntValue(detail.get(0), "dataType");
			DateFormat dateFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//格式化一下时间
			Date dNow = new Date(); //当前时间
			Date dBefore = new Date();
			Calendar calendar = Calendar.getInstance(); //得到日历
			calendar.setTime(dNow);//把当前时间赋给日历
			calendar.add(Calendar.DAY_OF_MONTH, -1); //设置为前一天
			dBefore = calendar.getTime(); //得到前一天的时间
			String defaultStartDate = dateFmt.format(dBefore); //格式化前一天
			defaultStartDate = defaultStartDate.substring(0,10)+" 00:00:00";
			String defaultEndDate = defaultStartDate.substring(0,10)+" 23:59:59";
			
			long startDate = Long.parseLong(DateUtils.date2TimeStamp(defaultStartDate, "yyyy-MM-dd HH:mm:ss"));
			long endDate = Long.parseLong(DateUtils.date2TimeStamp(defaultEndDate, "yyyy-MM-dd HH:mm:ss"));
			if (dataType == 1) {
				maxmin = bpmPreService.getMaxAndMinData(componentId, startDate, endDate, 1);
			} else {
				maxmin = bpmPreService.getMaxAndMinData(customId, startDate, endDate, 2);
			}
		} else {
			List<Map<String, Object>> detail = bottleneckTaskService.getSingleTaskDetail(Integer.parseInt(taskId));
			if(evUtil.listIsNullOrZero(detail)) {
				return execCheckError("未查询到相应配置信息！");
			}
			int customId = evUtil.getMapIntValue(detail.get(0), "customId");
			int componentId = evUtil.getMapIntValue(detail.get(0), "componentId");
			int dataType= evUtil.getMapIntValue(detail.get(0), "dataType");
			long startDate = Long.valueOf(DateUtils.date2TimeStamp(detail.get(0).get("startDate").toString(),"yyyy-MM-dd HH:mm:ss"));
			long endDate = Long.valueOf(DateUtils.date2TimeStamp(detail.get(0).get("endDate").toString(),"yyyy-MM-dd HH:mm:ss"));
			if (dataType == 1) {
				maxmin = bpmPreService.getMaxAndMinData(componentId, startDate, endDate, 1);
			} else {
				maxmin = bpmPreService.getMaxAndMinData(customId, startDate, endDate, 2);
			}
		}
		Map<String, Object> map=new HashMap<>();
		if(!evUtil.listIsNullOrZero(maxmin)) {
			 map=maxmin.get(0);
		}
		else {
			log.error("瓶颈分析配置getMeta无数据");
		}
		MetaParam meta = bottleneckTaskService.getMeta(taskId);
		List<List<Object>> numericCols=meta.getNumericCols();
		if(!evUtil.listIsNullOrZero(numericCols)) {
			for (List<Object> numericColDetailParam : numericCols) {
				String key=numericColDetailParam.get(0).toString();
				numericColDetailParam.remove(1);
				numericColDetailParam.add((float)evUtil.getMapDoubleValue(map, "min"+key));
				numericColDetailParam.add((float)evUtil.getMapDoubleValue(map, "max"+key));
			}
		}
		meta.setNumericCols(numericCols);
		return execSuccess(meta);
	}

//    @OperateLog(type = OperaterType.SELECT, desc = "获取分析结果")
    @ApiOperation(value="获取分析结果", notes="获取分析结果")
    @RequestMapping(value = "/detail/{taskItemId}", method = RequestMethod.GET)
    public Map<String, Object> getMetaUrlResult(@PathVariable String taskItemId){
		Map<String, Object> map = bottleneckTaskService.selectTaskDataByItemDayId(taskItemId);
		Map<String, Object> result = new HashMap<>();
		if(map == null || map.isEmpty()){
			result.put("code", 1);
			result.put("msg", "未查询到数据！");
			return result;
		}
		result.put("code", 0);
		result.put("data", JSON.parseObject(evUtil.getMapStrValue(map,"data")));
        return result;
    }
//    @OperateLog(type = OperaterType.INSERT, desc = "保存本次分析json")
    @ApiOperation(value="保存本次分析json", notes="保存本次分析json")
    @RequestMapping(value = "/detail/save/{taskItemId}", method = RequestMethod.POST)
    public JsonResult<Object> getSaveJson(@PathVariable String taskItemId, @RequestBody JSONObject jsonObject) throws ParseException {
        bottleneckTaskService.insertTaskData(taskItemId, jsonObject);
        return execSuccess(Constant.SAVE_SUCCESS_MSG);
    }
}
