package com.greattimes.ev.bpm.controller.analysis.report;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartDataParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportChartLocationParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportDescriptionLocationParam;
import com.greattimes.ev.bpm.entity.ReportChart;
import com.greattimes.ev.bpm.entity.ReportDescription;
import com.greattimes.ev.bpm.entity.ReportDescriptionLocation;
import com.greattimes.ev.bpm.service.analysis.IReportService;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.MethodCost;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.entity.BpmExtendTran;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.param.req.ReportChartParam;
import com.greattimes.ev.indicator.param.req.ReportIndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportTableParam;
import com.greattimes.ev.indicator.param.resp.ReportDataChartParam;
import com.greattimes.ev.indicator.service.IBpmPreService;
import com.greattimes.ev.indicator.service.ITransactionService;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.service.IDictionaryService;
import com.greattimes.ev.system.service.IUserService;
import com.greattimes.ev.utils.DownloadUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 智能报表表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@RestController
@RequestMapping("/ai/report")
@Api(value = "ReportController", tags = "智能报表")
@RequirePermission("aireport")
public class ReportController extends BaseController{
	@Autowired
	private IReportService reportService;
	@Autowired
	private IComponentService componentService;
	@Autowired
	private ICustomService customService;
	@Autowired
	private IUserService userService;
	@Autowired
	private ConfigurationCache configurationCache;
	@Autowired
	private IBpmPreService bpmPreService;
	@Autowired
    private ITransactionService transactionService;
	@Autowired
	private IDictionaryService dictionaryService;
	@Autowired
	private IApplicationService applicationService;

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@ApiOperation(value = "报表添加", notes = "报表添加")
	@OperateLog(type = OperaterType.INSERT, desc = "报表添加")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> addReport(HttpServletRequest request, @RequestBody JSONObject jsonObject){
		User user = getSessionUser(request);
		String name = jsonObject.getString("name");
		if (reportService.checkReportName(null, name, 1)) {
			Integer id = reportService.addReport(jsonObject, user.getId());
			Map<String, Object> map = new HashMap<>();
			map.put("reportId", id);
			return execSuccess(map, Constant.SAVE_SUCCESS_MSG);
		} else {
			return execCheckError("报表名重复！");
		}

	}
	
	@ApiOperation(value = "报表编辑", notes = "报表编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "报表编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> editReport(HttpServletRequest request, @RequestBody JSONObject jsonObject) {
		User user = getSessionUser(request);
		String name = jsonObject.getString("name");
		if (reportService.checkReportName(jsonObject.getInteger("id"), name, 0)) {
			reportService.editReport(jsonObject, user.getId());
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		} else {
			return execCheckError("报表名重复！");
		}

	}

	@ApiOperation(value = "报表列表查看", notes = "报表列表查看")
	@RequestMapping(value = "/query/list", method = RequestMethod.POST)
	public JsonResult<Object> queryReportList(HttpServletRequest request,@RequestBody JSONObject jsonObject) {
		User user = getSessionUser(request);
		int type = jsonObject.getIntValue("type");
		int orderBy = jsonObject.getIntValue("orderBy");
		Map<String, Object> map = new HashMap<>(3);
		map.put("type", type);
		map.put("orderBy", orderBy);
		map.put("userId", user.getId());
		List<Map<String, Object>> list=reportService.queryReportList(map);
		return execSuccess(list);
	}

	@ApiOperation(value = "报表查看", notes = "报表查看")
	@RequestMapping(value = "/query", method = RequestMethod.POST)
	public JsonResult<Object> queryReport(@RequestBody JSONObject jsonObject) {
		Map<String, Object> map=reportService.queryReport(jsonObject.getInteger("id"));
		return execSuccess(map);
	}
	
	@ApiOperation(value = "报表删除", notes = "报表删除")
	@OperateLog(type = OperaterType.DELETE, desc = "报表删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteReport(@RequestBody JSONObject jsonObject) {
		reportService.deleteReport(jsonObject.getInteger("id"));
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
	
	@ApiOperation(value = "描述添加", notes = "描述添加")
	@OperateLog(type = OperaterType.INSERT, desc = "描述添加")
	@RequestMapping(value = "/description/add", method = RequestMethod.POST)
	public JsonResult<Object> addDescription(@RequestBody JSONObject jsonObject) {
		String name=jsonObject.getString("name");
		String detail=jsonObject.getString("detail");
		Integer reportId=jsonObject.getInteger("reportId");
		ReportDescription description=new ReportDescription(name, detail, reportId);
		Integer id = reportService.addDescription(description);
		ReportDescriptionLocation lo=new ReportDescriptionLocation();
		lo.setDescriptionId(id);
		lo.setHeight(jsonObject.getDouble("h"));
		lo.setLeft(jsonObject.getDouble("x"));
		lo.setTop(jsonObject.getDouble("y"));
		lo.setWidth(jsonObject.getDouble("w"));
		reportService.saveDescriptionLocation(lo);
		Map<String, Object> map = new HashMap<>();
		map.put("reportId", id);
		return execSuccess(map, Constant.SAVE_SUCCESS_MSG);

	}
	@ApiOperation(value = "描述编辑", notes = "描述编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "描述编辑")
	@RequestMapping(value = "/description/edit", method = RequestMethod.POST)
	public JsonResult<Object> editDescription(@RequestBody JSONObject jsonObject) {
		ReportDescription description=new ReportDescription();
		description.setId(jsonObject.getInteger("id"));
		description.setDetail(jsonObject.getString("detail"));
		description.setName(jsonObject.getString("name"));
		reportService.editDescription(description);
		ReportDescriptionLocation lo=new ReportDescriptionLocation();
		lo.setDescriptionId(jsonObject.getInteger("id"));
		lo.setHeight(jsonObject.getDouble("h"));
		lo.setLeft(jsonObject.getDouble("x"));
		lo.setTop(jsonObject.getDouble("y"));
		lo.setWidth(jsonObject.getDouble("w"));
		reportService.updateDescriptionLocation(lo);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}
	@ApiOperation(value = "描述删除", notes = "描述删除")
	@OperateLog(type = OperaterType.DELETE, desc = "描述删除")
	@RequestMapping(value = "/description/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteDescription(@RequestBody ReportDescription description) {
		reportService.deleteDescription(description.getId());
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
	
	@RequirePermission("common")
	@ApiOperation(value = "描述查看", notes = "描述查看")
	@RequestMapping(value = "/description/query", method = RequestMethod.POST)
	public JsonResult<Object> queryDescriptionList(@RequestBody ReportDescription description) {
		List<Map<String, Object>> list=reportService.queryDescriptionList(description.getReportId());
		return execSuccess(list);
	}
	
	@ApiOperation(value = "图表添加", notes = "图表添加")
	@OperateLog(type = OperaterType.INSERT, desc = "图表添加")
	@RequestMapping(value = "/chart/append", method = RequestMethod.POST)
	public JsonResult<Object> insertChartRelation(@RequestBody JSONObject jsonObject) {
		Integer reportId = jsonObject.getInteger("reportId");
		List<Map<String, Object>> charts=(List) jsonObject.getJSONArray("chart");
		reportService.insertChartRelation(reportId,charts);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);

	}
	
	@ApiOperation(value = "图表重新添加", notes = "图表重新添加")
	@OperateLog(type = OperaterType.INSERT, desc = "图表重新添加")
	@RequestMapping(value = "/chart/readd", method = RequestMethod.POST)
	public JsonResult<Object> editChartRelation(@RequestBody JSONObject jsonObject) {
		Integer reportId = jsonObject.getInteger("reportId");
		Integer chartId = jsonObject.getInteger("chartId");
		Integer chartRelationId = jsonObject.getInteger("chartRelationId");
		reportService.editChartRelation(reportId,chartId,chartRelationId);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);

	}
	
	@ApiOperation(value = "图表移除", notes = "图表移除")
	@OperateLog(type = OperaterType.DELETE, desc = "图表移除")
	@RequestMapping(value = "/chart/remove", method = RequestMethod.POST)
	public JsonResult<Object> deleteChartRelation(@RequestBody JSONObject jsonObject) {
		Integer reportId = jsonObject.getInteger("reportId");
		Integer id=jsonObject.getInteger("id");
		reportService.deleteChartRelation(reportId,id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	@ApiOperation(value = "更新位置", notes = "更新位置")
	@RequestMapping(value = "/location", method = RequestMethod.POST)
	public JsonResult<Object> saveChartLocation(HttpServletRequest request,@RequestBody JSONObject jsonObject) {
		List<ReportDescriptionLocationParam> description=new ArrayList<>();
		List<ReportChartLocationParam> chart=new ArrayList<>();
		if(null!=jsonObject.getJSONArray("description")) {
			description=jsonObject.getJSONArray("description").toJavaList(ReportDescriptionLocationParam.class);
		}
		if(null!=jsonObject.getJSONArray("chart")) {
			chart=jsonObject.getJSONArray("chart").toJavaList(ReportChartLocationParam.class);
		}
		reportService.saveLocation(description,chart);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	@ApiOperation(value = "可选图表列表查询", notes = "可选图表列表查询")
	@RequestMapping(value = "/chart/available/query", method = RequestMethod.POST)
	public JsonResult<Object> queryAvailableChart(HttpServletRequest request,@RequestBody JSONObject jsonObject) {
		User user = getSessionUser(request);
		int type = jsonObject.getIntValue("type");
		int orderBy = jsonObject.getIntValue("orderBy");
		int dataType = jsonObject.getIntValue("dataType");
		Map<String, Object> map = new HashMap<>(3);
		map.put("type", type);
		map.put("orderBy", orderBy);
		map.put("userId", user.getId());
		map.put("dataType", dataType);
		List<Map<String, Object>> list=reportService.queryAvailableChart(map);
		return execSuccess(list);
	}

	@RequirePermission("common")
	@ApiOperation(value = "展示图表", notes = "展示图表")
	@RequestMapping(value = "/chart/show", method = RequestMethod.POST)
	public JsonResult<Object> queryChartList(HttpServletRequest request,@RequestBody JSONObject jsonObject) {
		User user = getSessionUser(request);
		List<Map<String, Object>> list=reportService.queryChartList(jsonObject.getInteger("reportId"),user.getId());
		return execSuccess(list);
	}

	@RequirePermission("common")
	@ApiOperation(value = "智能报表[top图,饼状图]", notes = "智能报表[top图,饼状图]")
	@MethodCost(desc="智能报表[top图,饼状图]")
	@RequestMapping(value = "/chart/indicator/pie", method = RequestMethod.POST)
	public JsonResult<Object> queryChartpie(@RequestBody ReportChartParam re) {
		List<Map<String, Object>> result=new ArrayList<>();
		if(!evUtil.listIsNullOrZero(re.getDimensions())) {
			result=bpmPreService.getTopChartData(re);
		}else {
			result=transactionService.getPieChartData(re);
		}
		return execSuccess(result);
	}

	@RequirePermission("common")
	@ApiOperation(value = "智能报表[数值图]", notes = "智能报表[数值图]")
	@MethodCost(desc="智能报表[数值图]")
	@RequestMapping(value = "/chart/indicator/datachart", method = RequestMethod.POST)
	public JsonResult<Object> queryDataChart(@RequestBody ReportChartParam re) {
		ReportDataChartParam result=new ReportDataChartParam();
		Integer isDimension=re.getIsDimension();
		//未勾选维度
		if(isDimension.equals(0)) {
			result=transactionService.getDataChart(re);
		}else {
			result=bpmPreService.getDataChart(re);
		}
		return execSuccess(result);
	}

	/**
	 * 图表新增
	 * @author NJ
	 * @date 2019/3/29 11:05
	 * @param request
	 * @param reportChartParam
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "图表新增", notes = "图表新增")
	@OperateLog(type = OperaterType.INSERT, desc = "图表新增")
	@PostMapping(value="/chart/application/add")
	public JsonResult<Object> saveChart(HttpServletRequest request, @RequestBody com.greattimes.ev.bpm.analysis.param.req.ReportChartParam reportChartParam) {
		Integer id = getSessionUser(request).getId();
		handUserIds(reportChartParam, id);
		if(reportService.checkReportCharName(null, id, reportChartParam.getName())){
			return execCheckError("图表名称重复！");
		}
		ReportChart result = reportService.saveOrUpdateReportChart(reportChartParam);
		Map<String, Object> idMap = new HashMap<>();
		idMap.put("id", result.getId());
		return execSuccess(idMap);
	}

	/**
	 * 图表更新
	 * @author NJ
	 * @date 2019/3/29 11:07
	 * @param request
	 * @param reportChartParam
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "图表编辑", notes = "图表编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "图表编辑")
	@PostMapping(value="/chart/application/edit")
	public JsonResult<Object> editChart(HttpServletRequest request, @RequestBody com.greattimes.ev.bpm.analysis.param.req.ReportChartParam reportChartParam) {
		Integer id = getSessionUser(request).getId();
		handUserIds(reportChartParam, id);
		if(reportService.checkReportCharName(reportChartParam.getId(), id, reportChartParam.getName())){
			return execCheckError("图表名称重复！");
		}
		ReportChart result = reportService.saveOrUpdateReportChart(reportChartParam);
		Map<String, Object> idMap = new HashMap<>();
		idMap.put("id", result.getId());
		return execSuccess(idMap, Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 图表删除
	 * @author NJ
	 * @date 2019/3/29 15:15
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "图表删除", notes = "图表删除")
	@OperateLog(type = OperaterType.DELETE, desc = "图表删除")
	@PostMapping(value="/chart/application/delete")
	public JsonResult<Object> deleteChart(@RequestBody JSONObject jsonObject) {
		List<Integer> ids = new ArrayList<>();
		ids.add(jsonObject.getInteger("id"));
		boolean result = reportService.deleteReportChartByIds(ids);
		if(result){
			return execSuccess(Constant.DELETE_SUCCESS_MSG);
		}else{
			return execSuccess(Constant.DELETE_ERROR_MSG);
		}
	}

	/**
	 * 图表查询
	 * @author NJ
	 * @date 2019/3/29 11:09
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "图表查询", notes = "图表查询")
	@PostMapping(value="/chart/application/query")
	public JsonResult<Object> findChartById(@RequestBody JSONObject jsonObject) {
		Map<String, Object> map = new HashMap<>(3);
		map.put("id", jsonObject.getInteger("id"));
		return execSuccess(reportService.findReportChartByMap(map));
	}

	/**
	 * 事件图表新增
	 * @author NJ
	 * @date 2019/3/29 11:05
	 * @param request
	 * @param reportChartParam
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "事件图表新增", notes = "事件图表新增")
	@OperateLog(type = OperaterType.INSERT, desc = "事件图表新增")
	@PostMapping(value="/chart/event/add")
	public JsonResult<Object> saveCustomChart(HttpServletRequest request, @RequestBody com.greattimes.ev.bpm.analysis.param.req.ReportChartParam reportChartParam) {
		Integer id = getSessionUser(request).getId();
		handUserIds(reportChartParam, id);
		if(reportService.checkReportCharName(null, id, reportChartParam.getName())){
			return execCheckError("图表名称重复！");
		}
		ReportChart result = reportService.saveOrUpdateReportChart(reportChartParam);
		Map<String, Object> idMap = new HashMap<>();
		idMap.put("id", result.getId());
		return execSuccess(idMap);
	}

	/**
	 * 事件图表更新
	 * @author NJ
	 * @date 2019/3/29 11:07
	 * @param request
	 * @param reportChartParam
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "事件图表更新", notes = "事件图表更新")
	@OperateLog(type = OperaterType.UPDATE, desc = "事件图表更新")
	@PostMapping(value="/chart/event/edit")
	public JsonResult<Object> editCustomChart(HttpServletRequest request, @RequestBody com.greattimes.ev.bpm.analysis.param.req.ReportChartParam reportChartParam) {
		Integer id = getSessionUser(request).getId();
		handUserIds(reportChartParam, id);
		if(reportService.checkReportCharName(reportChartParam.getId(), id, reportChartParam.getName())){
			return execCheckError("图表名称重复！");
		}
		ReportChart result = reportService.saveOrUpdateReportChart(reportChartParam);
		Map<String, Object> idMap = new HashMap<>();
		idMap.put("id", result.getId());
		return execSuccess(idMap, Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 事件图表删除
	 * @author NJ
	 * @date 2019/3/29 15:15
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "事件图表删除", notes = "事件图表删除")
	@OperateLog(type = OperaterType.DELETE, desc = "事件图表删除")
	@PostMapping(value="/chart/event/delete")
	public JsonResult<Object> deleteCustomChart(@RequestBody JSONObject jsonObject) {
		List<Integer> ids = new ArrayList<>();
		ids.add(jsonObject.getInteger("id"));
		boolean result = reportService.deleteReportChartByIds(ids);
		if(result){
			return execSuccess(Constant.DELETE_SUCCESS_MSG);
		}else{
			return execSuccess(Constant.DELETE_ERROR_MSG);
		}
	}

	/**
	 * 事件图表查询
	 * @author NJ
	 * @date 2019/3/29 11:09
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@ApiOperation(value = "事件图表查询", notes = "事件图表查询")
	@PostMapping(value="/chart/event/query")
	public JsonResult<Object> findCustomChartById(@RequestBody JSONObject jsonObject) {
		Map<String, Object> map = new HashMap<>();
		map.put("id", jsonObject.getInteger("id"));
		return execSuccess(reportService.findEventReportChartByMap(map));
	}

	/**
	 * 应用树
	 * @author NJ
	 * @date 2019/3/31 18:09
	 * @param request
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@PostMapping(value = "/chart/application/tree")
	public JsonResult<Object> selectApplicationTreeForAi(HttpServletRequest request){
		User user = getSessionUser(request);
		Map<String, Object> map = new HashMap<>(3);
		map.put("userId", user.getId());
		return execSuccess(componentService.selectAiApplicationTree(map));
	}

	/**
	 * 事件树
	 * @author NJ
	 * @date 2019/3/31 18:09
	 * @param request
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@PostMapping(value = "/chart/event/tree")
	public JsonResult<Object> selectComponentTreeForAi(HttpServletRequest request){
		User user = getSessionUser(request);
		Map<String, Object> map = new HashMap<>(3);
		map.put("userId", user.getId());
		return execSuccess(customService.selectCustomTreeForAi(map));
	}

	/**
	 * 根据报表查询报表相关人员，不包括报表本身的创建人员
	 * @author NJ
	 * @date 2019/4/1 19:01
	 * @param jsonObject
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@PostMapping(value = "/chart/group/user")
	public JsonResult<Object> selectReportUser(@RequestBody JSONObject jsonObject){
//		return execSuccess(userService.selectUserByReportId(jsonObject.getInteger("id")));
		return execSuccess(userService.selectUsersExcludeSelf(jsonObject.getInteger("id"),1));
	}


	@RequirePermission("common")
	@PostMapping(value = "/chart/indicator/trans/line")
	public JsonResult<Object> selectReportLineData(@RequestBody ReportIndicatorParam param){
		return execSuccess(transactionService.getReportLineIndicator(param));
	}

	/**
	 * AI报表事件数据折线图
	 * @param param
	 * @return
	 */
	@RequirePermission("common")
	@PostMapping(value = "/chart/indicator/event/line")
	public JsonResult<Object> selectEventReportLineData(@RequestBody ReportIndicatorParam param){
		return execSuccess(transactionService.getExtendReportLineIndicator(param));
	}

	/**
	 * AI报表维度数据折线图
	 * @author NJ
	 * @date 2019/4/15 9:37
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@PostMapping(value = "/chart/indicator/dimension/line")
	public JsonResult<Object> selectDimensionReportLineData(@RequestBody ReportIndicatorParam param){
		Integer dataType = param.getDataType();
		if(dataType == 1){
			return execSuccess(bpmPreService.getReportPreTranIndicator(param));
		}else if(dataType == 2){
			return execSuccess(bpmPreService.getReportExtendPreTranIndicator(param));
		}else {
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
	}

	/**
	 * AI报表根据chartIds返回数据配置
	 * @author NJ
	 * @date 2019/4/15 9:37
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@Deprecated
	@PostMapping(value = "/chart/series/name")
	public JsonResult<Object> selectReportConfigByChartId(@RequestBody JSONObject param){
		List<Integer> chartIds = param.getJSONArray("chartIds").toJavaList(Integer.class);
		return execSuccess(reportService.selectReportChartDataByChartIds(chartIds));
	}

	/**
	 * AI报表根据uuid返回数据配置
	 * @author NJ
	 * @date 2019/4/15 9:38
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
    @PostMapping(value = "/chart/series/name/uuid")
    public JsonResult<Object> selectReportConfigByUuid(@RequestBody JSONObject param){
        JSONArray jsonArray = param.getJSONArray("reportChartData");
        if(jsonArray == null || jsonArray.isEmpty()){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        List<ReportChartDataParam> reportChartDataParams = jsonArray.toJavaList(ReportChartDataParam.class);
		Map<String, Object> result = reportService.selectReportChartDataByUuid(reportChartDataParams);
        return execSuccess(result);
    }


	/**
	 * 1.3.19智能报表[表格]
	 * @author NJ
	 * @date 2019/4/16 10:08
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@PostMapping(value = "/chart/indicator/table")
	@MethodCost(desc="表格查询")
	public JsonResult<Object> selectReportTable(@RequestBody ReportTableParam param){
		Integer dataType = param.getDataType();
		if(dataType == 1){
			param.setNameMap(applicationService.namesMap(param.getUuids()));
			Page<BpmTran> page = new Page<>(param.getPage(),param.getSize());
			return execSuccess(transactionService.selectBpmTablePage(page, param));
		}else if(dataType == 3){
			param.setNameMap(applicationService.customNameMap(param.getUuids()));
			Page<BpmExtendTran> page = new Page<>(param.getPage(),param.getSize());
			return execSuccess(transactionService.selectExtendBpmTablePage(page, param));
		} else if (dataType == 4||dataType == 2) {
			Page<Map<String, Object>> page = new Page<>(param.getPage(), param.getSize());
			return execSuccess(bpmPreService.selectReportTablePage(page, param));
		}else{
		    return null;
		}
    }

	/**
	 * 报表top表
	 * @author NJ
	 * @date 2019/5/24 16:48
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@PostMapping(value = "/chart/indicator/table/top")
	@MethodCost(desc="报表top表")
	public JsonResult<Object> selectTopReportTable(@RequestBody ReportTableParam param){
		Integer dataType = param.getDataType();
		if(dataType == 1){
			param.setNameMap(applicationService.namesMap(param.getUuids()));
			return execSuccess(transactionService.selectBpmTable(param));
		}else if(dataType == 3){
			param.setNameMap(applicationService.customNameMap(param.getUuids()));
			return execSuccess(transactionService.selectExtendBpmTable(param));
		} else if (dataType == 4||dataType == 2) {
			return execSuccess(bpmPreService.selectReportTable(param));
		} else{
			return null;
		}
	}
	/**
	 * 下载数据table
	 * @author NJ
	 * @date 2019/6/10 14:31
	 * @param param
	 * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
	 */
	@RequirePermission("common")
	@PostMapping(value = "/chart/indicator/table/download")
	public void selectDownloadReportTable(HttpServletResponse resp, @RequestBody ReportTableParam param) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
		Integer dataType = param.getDataType();
		List<JSONObject> headersParam = param.getHeaders();
		List<Map<String, String>> headers = new ArrayList<>();
		if(!evUtil.listIsNullOrZero(headersParam)){
			headersParam.stream().forEach(X->{
				Map<String, String> map = new HashMap<>();
				map.put(X.getString("name"), X.getString("value"));
				headers.add(map);
			});
		}
		if(dataType == 1){
			param.setNameMap(applicationService.namesMap(param.getUuids()));
			List<BpmTran> bpmTrans = transactionService.selectBpmTable(param);
			XSSFWorkbook wb = DownloadUtil.downloadExcel("报表", headers, bpmTrans);
			DownloadUtil.downloadFileByResp(resp,wb,"utf-8","multipart/form-data",
					"Content-Disposition","attachment;fileName=table.xlsx"
			);
		}else if(dataType == 3){
			param.setNameMap(applicationService.customNameMap(param.getUuids()));
			List<BpmExtendTran> extendTrans = transactionService.selectExtendBpmTable(param);
			XSSFWorkbook wb = DownloadUtil.downloadExcel("报表", headers, extendTrans);
			DownloadUtil.downloadFileByResp(resp,wb,"utf-8","multipart/form-data",
					"Content-Disposition","attachment;fileName=table.xlsx");
		} else if (dataType == 4) {
			List<Map<String, Object>> extendDimen = bpmPreService.selectReportTable(param);
			XSSFWorkbook wb = DownloadUtil.downloadExcel("报表", headers, extendDimen);
			DownloadUtil.downloadFileByResp(resp,wb,"utf-8","multipart/form-data",
					"Content-Disposition","attachment;fileName=table.xlsx");
		} else if (dataType == 2) {
			List<Map<String, Object>> bpmDimen = bpmPreService.selectReportTable(param);
			XSSFWorkbook wb = DownloadUtil.downloadExcel("报表", headers, bpmDimen);
			DownloadUtil.downloadFileByResp(resp,wb,"utf-8","multipart/form-data",
					"Content-Disposition","attachment;fileName=table.xlsx");
		}else{
			throw new RuntimeException("非法参数dataType！");
		}
	}



	/**
     * 根据图表id删除图表
	 * 自己创建的图表删除图表相关表结构，分享的图表删除关系
     * @param param
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     * @throw
     * @author NJ
     * @date 2019-04-29 11:34
    */
	@PostMapping(value = "/chart/delete")
	public JsonResult<Object> deleteReportChartByIds(@RequestBody JSONObject param,HttpServletRequest request){
		JSONArray jsonArray = param.getJSONArray("ids");
		if(jsonArray == null || jsonArray.isEmpty()){
			return execCheckError(Constant.PARAMETER_ERROR_MSG);
		}
		Set<Integer> ids = new HashSet<>(jsonArray.toJavaList(Integer.class));
		boolean result = reportService.deleteChartByIds(ids,getSessionUser(request).getId());
		if(result){
			return execSuccess(Constant.DELETE_SUCCESS_MSG);
		}else{
			return execSuccess(Constant.DELETE_ERROR_MSG);
		}
	}





	private void handUserIds(com.greattimes.ev.bpm.analysis.param.req.ReportChartParam param, int userId){
		param.setCreateBy(userId);
		List<Integer> userIdList = param.getUserIds();
		if(userIdList == null){
			userIdList = new ArrayList<>();
		}
		HashSet<Integer> userIdSet = new HashSet<>(userIdList);
		userIdSet.add(userId);
		param.setUserIds(new ArrayList<>(userIdSet));
	}

}

