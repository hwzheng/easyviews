package com.greattimes.ev.indicator.mapper;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.indicator.entity.SourceTran;

public interface SourceTranMapper {

	List<SourceTran> selectSourceTranByUuid(Map<String, Object> map);

}
