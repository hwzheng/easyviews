package com.greattimes.ev.indicator.mybatisinterceptor;

public abstract class  SqlHandler {

    public abstract String viewType();

    public abstract String handleSql(String sql);


}
