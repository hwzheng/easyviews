package com.greattimes.ev.indicator.mybatisinterceptor;

import java.util.Arrays;

public class TransactionIndicatorSqlHandler extends SqlHandler {


    @Override
    public String viewType() {
        return "v_transaction_indicator";
    }

    @Override
    public String handleSql(String sql) {

        if (sql.indexOf("WHERE")>=-1){
           sql=sql.replaceAll("WHERE" ,"where");
        }
        if(sql.indexOf("GROUP ")>=-1){
            sql=sql.replaceAll("GROUP " ,"group ");
        }

        String where="";
        if(!sql.contains("group ")){
           where= sql.substring(sql.indexOf("where"),sql.length());
        }else {
           where=sql.substring(sql.indexOf("where"),sql.indexOf("group "));
        }

        StringBuilder sb=new StringBuilder("( SELECT ");
        sb.append(" applicationId, componentId, time, uuid, date, level,")
                .append(" round(if(isNaN(SUM(allStart2ends) / SUM(responseTransCount)), 0, " +
                        "SUM(allStart2ends) / SUM(responseTransCount)),2) AS responseTime,")
                .append(" round(if(isNaN(SUM(responseTransCount) / SUM(allTransCountBak)), 0,\n" +
                        "SUM(responseTransCount) / SUM(allTransCountBak)) * 100, 2)  AS responseRate,")
                .append(" round(if(isNaN(SUM(successTransCount) / SUM(responseTransCount)), 0,\n" +
                        "SUM(successTransCount) / SUM(responseTransCount)) * 100, 2) AS successRate,")
                .append("sum(allTransCount)       AS allTransCount")
                .append(" FROM default.bpmTran ")
                .append(where)
                .append(" GROUP BY applicationId, componentId, time, uuid, date, level )");

        sql=sql.replace(this.viewType(),sb.toString());
        System.out.println(sql);
        return sql;
    }


    public static void main(String[] args) {
        String tsql="SELECT\n" +
                "\t\t\tuuid as applicationId,\n" +
                "\t\t\tsum(allTransCount) allTransCount\n" +
                "\t\tFROM\n" +
                "         v_transaction_indicator\n" +
                "    where time >= 1545717000000\n" +
                "\t\tAND time <=1545717000000+1*60*1000\n" +
                "\t\tAND\tuuid in  (2,3,4,7,12,13,14,17,20,33)\n" +
                "    and level=1\n" +
                "\t\tGROUP BY\n" +
                "\t\t\tuuid";
        TransactionIndicatorSqlHandler t=new TransactionIndicatorSqlHandler();
        t.handleSql(tsql);

    }


}
