package com.greattimes.ev.indicator.mapper;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.indicator.entity.BpmFCS;

public interface BpmFCSMapper {

	/**
	 * 基线查询
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmFCS> selectBpmFCSByUuid(Map<String, Object> map);

	/**
	 * 基线分钟级
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmFCS> selectMinuteBpmFCSByUuid(Map<String, Object> map);
	
}
