package com.greattimes.ev.indicator.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.bpm.entity.Custom;
import com.greattimes.ev.bpm.entity.DimensionValue;
import com.greattimes.ev.bpm.entity.Server;
import com.greattimes.ev.bpm.entity.ServerIp;
import com.greattimes.ev.bpm.service.alarm.IAlarmService;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.bpm.service.common.IIndicatorService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.entity.BpmExtendTran;
import com.greattimes.ev.indicator.entity.BpmFCS;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.entity.SourceTran;
import com.greattimes.ev.indicator.mapper.BpmFCSMapper;
import com.greattimes.ev.indicator.mapper.BpmPreMapper;
import com.greattimes.ev.indicator.mapper.BpmTranMapper;
import com.greattimes.ev.indicator.mapper.NetPerformanceMapper;
import com.greattimes.ev.indicator.mapper.SourceTranMapper;
import com.greattimes.ev.indicator.param.req.ExtendIndicatorParam;
import com.greattimes.ev.indicator.param.req.Indicator;
import com.greattimes.ev.indicator.param.req.IndicatorIntervalParam;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportChartParam;
import com.greattimes.ev.indicator.param.req.ReportIndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportTableParam;
import com.greattimes.ev.indicator.param.req.TpsParam;
import com.greattimes.ev.indicator.param.resp.ExtendIndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.ExtendIndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.ReportDataChartParam;
import com.greattimes.ev.indicator.param.resp.ReportIndicatorDetailParam;
import com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam;
import com.greattimes.ev.indicator.param.resp.TransactionExtendTableParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;
import com.greattimes.ev.indicator.param.resp.TransactionTpsParam;
import com.greattimes.ev.indicator.service.ITransactionService;
import com.greattimes.ev.utils.IndicatorSupportUtil;

/**
 * @author cgc
 *
 */
@Service
public class TransationServiceImpl implements ITransactionService {
	@Autowired
	private BpmTranMapper bpmTranMapper;
	@Autowired
	private SourceTranMapper sourceTranMapper;
	@Autowired
	private BpmFCSMapper bpmFCSMapper;
	@Autowired
	private IIndicatorService indicatorService;
	@Autowired
	private ICommonService commonService;
	@Autowired
	private NetPerformanceMapper netPerformanceMapper;
	@Autowired
	private IAlarmService alarmService;
	@Autowired
	private BpmPreMapper bpmPreMapper;
	@Autowired
	private ConfigurationCache configurationCache;
	
	
	private static int INTERVAL_MINUTE = 60;
	private static DecimalFormat df = new DecimalFormat("0.00");

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	private static final String T_BPMFCS = "bpmFCS";
	private static final String T_BPMTRAN = "bpmTran";
	private static final String T_BPMEXTENDTRAN = "bpmExtendTran";
	// private static final String T_BPMNET = "netPerformance";

	private static final String PER_TABLE_FIX_BPM = "bpmPre";
	private static final String PER_TABLE_FIX_EXTEND = "extendPre";
	private static final String VM_TRANSACTION_INDICATOR = "vm_transaction_indicator";
	private static final String VM_EXTEND_INDICATOR = "vm_extend_indicator";
	private static final String VM_EXTENDPRE = "vm_extendPre";

	@Override
	public IndicatorDataParam chart(IndicatorParam param) {
		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Integer> indicator = param.getIndicator();
		if (evUtil.listIsNullOrZero(indicator)) {
			return result;
		}
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		
		IndicatorIntervalParam indicatorParam =null;
		Integer interval=param.getInterval();
		if (interval == null) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}

		List<Long> timeList = indicatorParam.getTimeList();
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(1, map, indicatorParam.getType());
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		List<Integer> type=new ArrayList<>();
		type.add(1);
		//全部交易指标
		List<String> indicatorList=commonService.selectIndicator(type);
		//筛选指标
		enameList.retainAll(indicatorList);
		// 拼接指标sql
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.debug("交易指标数据：{}", JSON.toJSONString(map));
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(map);

		// group by uuid
		Map<Integer, List<BpmTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();
		uuid.forEach(x -> resultGroup.put(x, initDefault(grouplist.computeIfAbsent(x, ArrayList :: new), timeList)));

		// assembly data
		List<IndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
			for (Integer integer : indicator) {
				IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
				detail.setId(integer);
				detail.setUuid(entry.getKey());
				List<BpmTran> bpm = entry.getValue();
				// 置为map
				List<Map<String, Object>> mapbpm = new ArrayList<>();
				bpm.stream().forEach(x -> {
					Map<String, Object> dataMap = new HashMap<>(16);
					// 添加指标需修改
					dataMap.put("allTransCount", x.getAllTransCount());
					dataMap.put("responseTime", x.getResponseTime());
					dataMap.put("responseRate", x.getResponseRate());
					dataMap.put("successRate", x.getSuccessRate());
					mapbpm.add(dataMap);
				});
				// 获取指标名
				String colname = colnameList.get(integer);
				// 根据指标名取值
				List<String> invalue = new ArrayList<>();
				mapbpm.stream().forEach(x -> {
					String str = null;
					if (null != x.get(colname)) {
						str = x.get(colname).toString();
					}
					invalue.add(str);
				});

				detail.setValue(invalue);
				value.add(detail);
			}
			result.setIndicator(value);
		}
		return result;
	}

	/**
	 * init default list
	 *
	 * @param data
	 * @param timeList
	 * @return
	 */
	private List<BpmTran> initDefault(List<BpmTran> data, List<Long> timeList) {
		List<BpmTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : data) {
				if (time.equals(trans.getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmTran(time));
			}
		}
		return result;
	}

	private List<BpmTran> initDateDefault(List<BpmTran> data, List<Long> timeList) {
		List<BpmTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : data) {
				if (time.equals(trans.getDate().getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmTran(time));
			}
		}
		return result;
	}

	/**
	 * 第三方指标补数据 init default list
	 *
	 * @param data
	 * @param timeList
	 * @return
	 */
	private List<SourceTran> initExtendDefault(List<SourceTran> data, List<Long> timeList, List<Integer> indicator) {
		List<SourceTran> result = new ArrayList<>();
		boolean flag;
		for (Integer indicatorId : indicator) {
			for (Long time : timeList) {
				flag = false;
				for (SourceTran trans : data) {
					if (trans.getIndicatorid().equals(indicatorId) && time.equals(trans.getTime())) {
						result.add(trans);
						flag = true;
						break;
					}
				}
				if (!flag) {
					result.add(new SourceTran(time, indicatorId));
				}
			}
		}
		return result;
	}

	@Override
	public IndicatorDataParam bpmAndExtendChart(IndicatorParam param) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuid = param.getUuid();

		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}

		Long start = param.getStart();
		Long end = param.getEnd();

		// 原始指标id(条件中包含重复的指标)
		List<Indicator> indicators = param.getIndicatorList();

		Set<Indicator> indicatorSet = new LinkedHashSet<>();

		/**
		 *
		 * 根据指标type 判断查询的表的类别, 只限于 2 组件, 3ip, 4port 1、BPM基础指标 2、扩展基础指标 3、网络指标 4、应用指标
		 *
		 */
		Map<Integer, Set<Indicator>> groupsIndicator = new HashMap<>();
		indicators.stream().forEach(x -> {
			indicatorSet.add(x);
			if (groupsIndicator.get(x.getType()) == null) {
				Set<Indicator> setList = new LinkedHashSet<>();
				setList.add(x);
				groupsIndicator.put(x.getType(), setList);
			} else {
				groupsIndicator.get(x.getType()).add(x);
			}
		});

		/**
		 * 去重之后的指标id
		 */
		// 按照指标id进行去重
		List<Indicator> indicatorList = new ArrayList<>(indicatorSet);

		Integer interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
		IndicatorIntervalParam intervalParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end,
				interval);
		start = intervalParam.getStart();
		end = intervalParam.getEnd();
		int granularity = intervalParam.getType();
		List<Long> timeList = intervalParam.getTimeList();

		int ruleType = param.getRuleType();
		Map<String, Object> paramMap = new HashMap<>();

		paramMap.put("start", start);
		paramMap.put("end", end);
		paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));

		/**
		 * 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度 ， 7 监控点告警 , 8 数据源告警； 9自定义-- 业务 ，10自定义--
		 * 统计维度， 11自定义-- 普通维度， 12自定义-- 多维度'
		 */
		boolean isCheckRule = param.isCheckRule();
		Integer relation = param.getRelation();
		List<IndicatorDataDetailParam> indicatorResult = new ArrayList<>();

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,参数准备耗时：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		// 告警配置页面暂时没有应用告警的配置,即不走 ruleType==1 的分支
		if (ruleType == 1) {
			IndicatorSupportUtil.dealIndicatorTable(1, paramMap, granularity);
			IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
					commonService.selectIndicator(Arrays.asList(1)));
			logger.info("bpmAndExtendChart查询参数为:{}", JSON.toJSONString(paramMap));
			List<Map<String, Object>> dataList = bpmTranMapper.selectBpmTranReturnMap(paramMap);

			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,获取指标数据：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			stopWatch.start();

			List<IndicatorDataDetailParam> valueParams = IndicatorSupportUtil
					.getIndicatorDataByIndicatorList(indicatorList, uuid, dataList, timeList);
			if (isCheckRule) {
				this.formatSpecialIndicator(indicators, param.getSpecialIndicators());
				indicatorResult.addAll(this.getIndicatorDataParamByAlarmRule(valueParams, timeList, indicators,
						relation, param.isNeedMerge(), param.getAbnormalTime()));
			} else {
				indicatorResult.addAll(valueParams);
			}

			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,根据告警规则查询：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			stopWatch.start();

		} else if (ruleType >= 2 && ruleType <= 4) {
			// 2 组件, 3ip, 4port
			if (groupsIndicator.containsKey(3) || groupsIndicator.containsKey(4)) {
				// 组件,ip,port 出网络数据
				// 1、BPM基础指标(V_TRANSACTION_INDICATOR) 3、BPM应用指标(Net_netPerformance)
				List<IndicatorDataDetailParam> merge = new ArrayList<>();
				for (Map.Entry<Integer, Set<Indicator>> entry : groupsIndicator.entrySet()) {
					if (entry.getKey().equals(1)) {
						paramMap.put("ids", uuid);
						IndicatorSupportUtil.dealIndicatorTable(1, paramMap, granularity);
						IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
								commonService.selectIndicator(Arrays.asList(1)));
						logger.debug("bpmAndExtendChart查询参数为:{}", JSON.toJSONString(paramMap));
						List<Map<String, Object>> list = bpmTranMapper.selectBpmTranReturnMap(paramMap);

						merge.addAll(IndicatorSupportUtil.getIndicatorDataByIndicatorList(
								new ArrayList<>(entry.getValue()), uuid, list, timeList));
					} else if (entry.getKey().equals(3)) {
						paramMap.put("ids", uuid);
						paramMap.put("interval", interval);
						IndicatorSupportUtil.dealIndicatorTable(5, paramMap, granularity);
						IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
								commonService.selectIndicator(Arrays.asList(3)), "indicatorColumnStr",
								intervalParam.getType(), intervalParam.getInterval());
						logger.debug("bpmAndExtendChart查询参数为:{}", JSON.toJSONString(paramMap));
						List<Map<String, Object>> list = netPerformanceMapper.selectListByColumnStr(paramMap);

						merge.addAll(IndicatorSupportUtil.getIndicatorDataByIndicatorList(
								new ArrayList<>(entry.getValue()), uuid, list, timeList));
					} else if (entry.getKey().equals(4)) {
						paramMap.put("ids", uuid);
						IndicatorSupportUtil.dealIndicatorTable(5, paramMap, granularity);
						IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
								commonService.selectIndicator(Arrays.asList(4)), "indicatorColumnStr",
								intervalParam.getType(), intervalParam.getInterval());
						logger.debug("bpmAndExtendChart查询参数为:{}", JSON.toJSONString(paramMap));
						List<Map<String, Object>> list = netPerformanceMapper.selectListByColumnStr(paramMap);
						merge.addAll(IndicatorSupportUtil.getIndicatorDataByIndicatorList(
								new ArrayList<>(entry.getValue()), uuid, list, timeList));
					}

					stopWatch.stop();
					logger.debug("》》》》当前线程：【{}】,获取指标数据：{}ms", Thread.currentThread().getId(),
							stopWatch.getLastTaskTimeMillis());
					stopWatch.start();

					if (isCheckRule) {
						this.formatSpecialIndicator(indicators, param.getSpecialIndicators());
						indicatorResult.addAll(this.getIndicatorDataParamByAlarmRule(merge, timeList, indicators,
								relation, param.isNeedMerge(), param.getAbnormalTime()));
					} else {
						indicatorResult.addAll(merge);
					}

					stopWatch.stop();
					logger.debug("》》》》当前线程：【{}】,根据告警规则查询：{}ms", Thread.currentThread().getId(),
							stopWatch.getLastTaskTimeMillis());
					stopWatch.start();
				}
			} else {
				// 指标类型只有一种 1、BPM基础指标
				paramMap.put("ids", uuid);
				IndicatorSupportUtil.dealIndicatorTable(1, paramMap, granularity);
				IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
						commonService.selectIndicator(Arrays.asList(1)));
				logger.debug("bpmAndExtendChart查询参数为:{}", JSON.toJSONString(paramMap));
				List<Map<String, Object>> lists = bpmTranMapper.selectBpmTranReturnMap(paramMap);
				stopWatch.stop();
				logger.debug("》》》》当前线程：【{}】,获取指标数据：{}ms", Thread.currentThread().getId(),
						stopWatch.getLastTaskTimeMillis());
				stopWatch.start();
				List<IndicatorDataDetailParam> valueParams = IndicatorSupportUtil
						.getIndicatorDataByIndicatorList(param.getIndicatorList(), uuid, lists, timeList);
				if (isCheckRule) {
					this.formatSpecialIndicator(indicators, param.getSpecialIndicators());
					indicatorResult.addAll(this.getIndicatorDataParamByAlarmRule(valueParams, timeList, indicators,
							relation, param.isNeedMerge(), param.getAbnormalTime()));
				} else {
					indicatorResult.addAll(valueParams);
				}
				stopWatch.stop();
				logger.debug("》》》》当前线程：【{}】,根据告警规则查询：{}ms", Thread.currentThread().getId(),
						stopWatch.getLastTaskTimeMillis());
				stopWatch.start();
			}
		} else if (ruleType == 9 || ruleType == 10) {
			// 自定义-- 业务 ，10自定义-- 统计维度
			paramMap.put("ids", uuid);
			IndicatorSupportUtil.dealIndicatorTable(2, paramMap, granularity);
			IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
					commonService.selectIndicator(Arrays.asList(1, 2)));
			logger.debug("bpmAndExtendChart查询参数为:{}", JSON.toJSONString(paramMap));
			List<Map<String, Object>> lists = bpmTranMapper.selectBpmExtendTranReturnMap(paramMap);

			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,获取指标数据：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			stopWatch.start();

			List<IndicatorDataDetailParam> valueParams = IndicatorSupportUtil
					.getIndicatorDataByIndicatorList(param.getIndicatorList(), uuid, lists, timeList);
			if (isCheckRule) {
				this.formatSpecialIndicator(indicators, param.getSpecialIndicators());
				indicatorResult.addAll(this.getIndicatorDataParamByAlarmRule(valueParams, timeList, indicators,
						relation, param.isNeedMerge(), param.getAbnormalTime()));
			} else {
				indicatorResult.addAll(valueParams);
			}

			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,根据告警规则查询：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			stopWatch.start();

		}
		result.setIndicator(indicatorResult);
		result.setTime(timeList);

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,接口总耗时：{}s", Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());
		return result;
	}

	@Override
	public List<TransactionTableParam> table(IndicatorParam param) {
		List<TransactionTableParam> table = new ArrayList<>();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return table;
		}
		Long start = param.getStart();
		Long end = param.getEnd();

		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end,
				configurationCache);
		List<Long> timeList = indicatorParam.getTimeList();
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(1, map, indicatorParam.getType());
		List<Integer> type = new ArrayList<>();
		type.add(1);
		List<String> enameList = commonService.selectIndicator(type);
		// 拼接指标sql
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.debug("》》》》请求参数：{}", JSON.toJSONString(map));
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(map);

		// group by uuid
		Map<Integer, List<BpmTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();

		uuid.forEach(x -> resultGroup.put(x, initDefault(grouplist.computeIfAbsent(x, ArrayList::new), timeList)));

		// assembly data
		for (int i = 0; i < timeList.size(); i++) {
			for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
				TransactionTableParam trans = new TransactionTableParam();
				trans.setTime(timeList.get(i));
				trans.setUuid(entry.getKey());
				// 添加指标需修改
				trans.setAllTransCount(entry.getValue().get(i).getAllTransCount());
				trans.setResponseRate(entry.getValue().get(i).getResponseRate());
				trans.setResponseTime(entry.getValue().get(i).getResponseTime());
				trans.setSuccessRate(entry.getValue().get(i).getSuccessRate());
				table.add(trans);
			}
		}
		return table;
	}

	@Override
	public Map<String, Object> selectAppTransByMap(Map<String, Object> param) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		long start = evUtil.getMapLongValue(param, "start");
		long end = evUtil.getMapLongValue(param, "end");
		int applicationId = evUtil.getMapIntValue(param, "applicationId");

		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end,
				configurationCache);
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		List<Long> timeList = indicatorParam.getTimeList();

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,redis获取参数：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		Map<String, Object> map = new HashMap<>(4);
		map.put("ids", Arrays.asList(applicationId));
		map.put("start", start);
		map.put("end", end);

		// 处理表
		int type = indicatorParam.getType();
		IndicatorSupportUtil.dealIndicatorTable(1, map, type);

		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));

		// deal with indicator str
		List<String> columnNames = commonService.selectIndicator(Arrays.asList(1));

		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), map, "indicatorColumnStr",
				columnNames);

		logger.debug("》》》》selectAppTransByMap:请求参数：{}", JSON.toJSONString(map));
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(map);

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,获取clickhouse指标库数据耗时：{}ms", Thread.currentThread().getId(),
				stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		List<Integer> dataList = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : bpmTranList) {
				if (time.equals(trans.getTime())) {
					dataList.add(trans.getAllTransCount());
					flag = true;
					break;
				}
			}
			if (!flag) {
				dataList.add(null);
			}
		}
		Map<String, Object> result = new HashMap<>();
		result.put("time", timeList);
		result.put("transCount", dataList);

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,计算结果耗时：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		logger.debug("》》》》当前线程：【{}】,接口总耗时：{}s", Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());
		return result;
	}

	@Override
	public IndicatorDataParam baseline(IndicatorParam param, int interval) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuidList = param.getUuid();
		if (evUtil.listIsNullOrZero(uuidList)) {
			return result;
		}
		List<Integer> indicatorList = param.getIndicator();
		if (evUtil.listIsNullOrZero(indicatorList)) {
			return result;
		}
		Integer uuid = uuidList.get(0);
		Long start,end;
		if(interval==60000) {
			start = DateUtils.longToMinute(param.getStart(),1);
			end = DateUtils.longToMinute(param.getEnd(),1);
		}else {
			start = DateUtils.longToFiveSecondTimeBehind(param.getStart());
			end = DateUtils.longToFiveSecondTime(param.getEnd());
		}
		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval);

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,redis获取参数：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		String table = T_BPMFCS;
		Map<String, Object> map = new HashMap<>(5);
		map.put("uuid", uuid);
		map.put("start", start);
		map.put("end", end);
		map.put("indicator", indicatorList);
		map.put("table", table);
		List<BpmFCS> bpmFCSList=new ArrayList<>();
		if(interval==60000) {
			bpmFCSList = bpmFCSMapper.selectMinuteBpmFCSByUuid(map);
		}else {
			bpmFCSList = bpmFCSMapper.selectBpmFCSByUuid(map);
		}

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,获取clickhouse指标库数据耗时：{}ms", Thread.currentThread().getId(),
				stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		// group by uuid
		Map<Integer, List<BpmFCS>> grouplist = bpmFCSList.stream()
				.collect(Collectors.groupingBy(BpmFCS::getIndicatorid, Collectors.toList()));
		Map<Integer, List<BpmFCS>> resultGroup = new LinkedHashMap<>();

		indicatorList.forEach(x -> resultGroup.put(x, initDefaultBpmFCS(grouplist.computeIfAbsent(x, ArrayList :: new), timeList, uuid, x)));

		// assembly data
		List<IndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		for (Map.Entry<Integer, List<BpmFCS>> entry : resultGroup.entrySet()) {
			IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
			detail.setId(entry.getKey());
			detail.setUuid(uuid);
			List<BpmFCS> fcs = entry.getValue();
			List<Double> valueDouble = fcs.stream().map(BpmFCS::getFcsdata).collect(Collectors.toList());
			List<String> valueStr = new ArrayList<>();
			// 转换成List<String>
			for (Double double1 : valueDouble) {
				if (null != double1) {
					valueStr.add(double1.toString());
				} else {
					valueStr.add(null);
				}
			}
			detail.setValue(valueStr);
			value.add(detail);
		}
		result.setIndicator(value);

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,计算结果耗时：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		logger.debug("》》》》当前线程：【{}】,接口总耗时：{}s", Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());

		return result;
		/*
		 * IndicatorIntervalParam
		 * indicatorParam=IndicatorSupportUtil.getIndicatorInterval(T_BPMFCS, start,
		 * end, configurationCache); List<Long> timeList = indicatorParam.getTimeList();
		 * String table=indicatorParam.getTable();
		 */
	}

	private List<BpmFCS> initDefaultBpmFCS(List<BpmFCS> data, List<Long> timeList, Integer uuid, Integer indicator) {
		List<BpmFCS> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmFCS trans : data) {
				if (time.equals(trans.getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmFCS(time, uuid, indicator));
			}
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> selectTransByMap(Map<String, Object> param) {
		int uuid = evUtil.getMapIntValue(param, "uuid");
		int type = evUtil.getMapIntValue(param, "type");
		Long start = evUtil.getMapLongValue(param, "start");
		Long end = evUtil.getMapLongValue(param, "end");

		// 1、交易性能 2、网络性能 3、应用性能
		int indicatorType = evUtil.getMapIntValue(param, "indicatorType");
		/**
		 * 组件1 全部ip 2 ip(port)3 业务 4 维度5
		 */
		List<Integer> ids = Arrays.asList(uuid);
		List<Integer> uuids = new ArrayList<>();
		Map<Integer, Object> nameMap = new HashMap<>();
		Map<Integer, Object> customMap = new HashMap<>();
		Map<Integer, DimensionValue> dimensionMap = new HashMap<>();
		// alarm type 1 应用 2 监控点 3 组件 4 业务 5 数据源 6 ip 7 port 8 业务维度
		Map<String, Object> alarmParamMap = new HashMap<>();
		alarmParamMap.put("start", start);
		alarmParamMap.put("end", end);

		StopWatch stopWatch = new StopWatch();
		stopWatch.start();

		// 告警数据的查询
		if (type == 1 || type == 2) {
			// 全部ip
			List<ServerIp> serverIps = commonService.findServerIpsByComponentIds(ids);
			for (ServerIp serverIp : serverIps) {
				uuids.add(serverIp.getId());
				nameMap.put(serverIp.getId(), serverIp.getIp());
			}
			// 查询ip告警参数
			alarmParamMap.put("ids", uuids);
			alarmParamMap.put("type", 6);

		} else if (type == 3) {
			List<Server> servers = commonService.selectPortByIpId(uuid);
			for (Server server : servers) {
				uuids.add(server.getId());
				nameMap.put(server.getId(), server.getPort());
			}
			// 端口告警参数
			alarmParamMap.put("ids", uuids);
			alarmParamMap.put("type", 7);

		} else if (type == 4) {
			List<Custom> customs = commonService.selectCustomByComponentId(uuid);
			for (Custom custom : customs) {
				uuids.add(custom.getId());
				nameMap.put(custom.getId(), custom.getName());
				customMap.put(custom.getId(), custom.getId());
			}
			// 业务告警参数
			alarmParamMap.put("ids", uuids);
			alarmParamMap.put("type", 4);
		} else if (type == 5) {
			List<DimensionValue> values = commonService.selectDimensionValueByStatisticsDimensionId(uuid);
			for (DimensionValue dimensionValue : values) {
				uuids.add(dimensionValue.getId());
				nameMap.put(dimensionValue.getId(), dimensionValue.getName());
				customMap.put(dimensionValue.getId(), dimensionValue.getCustomId());
				dimensionMap.put(dimensionValue.getId(), dimensionValue);
			}
			// 维度告警参数
			alarmParamMap.put("ids", uuids);
			alarmParamMap.put("type", 8);

		}

		if (evUtil.listIsNullOrZero(uuids)) {
			return Collections.emptyList();
		}

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,参数准备：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		// 查询告警数据
		List<Map<String, Object>> alarmData = alarmService.selectOtherSingleAlarmAmount(alarmParamMap);

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,查询告警数据：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		stopWatch.start();

		List<Map<String, Object>> dataList = new ArrayList<>();

		IndicatorIntervalParam interValParam = IndicatorSupportUtil.getIndicatorInterval(start, end,
				configurationCache);
		start = interValParam.getStart();
		end = interValParam.getEnd();

		// custom
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("ids", uuids);
		paramMap.put("start", start);
		paramMap.put("end", end);

		// 加入date分区条件
		paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.debug("》》》》查询参数：【{}】", JSON.toJSONString(paramMap));

		Integer interval = interValParam.getInterval();
		Integer granularity = interValParam.getType();

		// 业务
		if (type == 4) {
			if (indicatorType == 1) {
				// 交易
				IndicatorSupportUtil.dealIndicatorTable(2, paramMap, granularity);
				IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
						commonService.selectIndicator(Arrays.asList(1, 2)));
				logger.debug("》》selectTransByMap(组件层级以下参数):{}", JSON.toJSONString(paramMap));
				dataList = bpmTranMapper.selectBpmExtendTranReturnMap(paramMap);
				dataList.stream().forEach(x -> {
					x.put("name", nameMap.get(x.get("uuid")));
					x.put("customId", customMap.get(x.get("uuid")));
					IndicatorSupportUtil.getAlarmAmountMapByParams((Integer) x.get("uuid"), (Long) x.get("time"),
							alarmData, x);
				});
			} else {
				throw new RuntimeException("indicatorType参数非法！");
			}
			// else if(indicatorType == 2){
			// //网络
			// IndicatorSupportUtil.dealIndicatorTable(5, paramMap, granularity);
			// paramMap.put("interval", interval);
			// IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
			// commonService.selectIndicator(Arrays.asList(3)),
			// "indicatorColumnStr",interValParam.getType(),interval);
			// logger.info("》》selectTransByMap(组件层级以下参数):{}",JSON.toJSONString(paramMap));
			// dataList = netPerformanceMapper.selectListByColumnStr(paramMap);
			// dataList.stream().forEach(x->{
			// x.put("name", nameMap.get(x.get("uuid")));
			// x.put("customId",customMap.get(x.get("uuid")));
			// IndicatorSupportUtil.getAlarmAmountMapByParams((Integer)x.get("uuid"),
			// (Long)x.get("time"),alarmData,x);
			// });
			// }else if(indicatorType == 3){
			// //应用性能
			// IndicatorSupportUtil.dealIndicatorTable(5, paramMap, granularity);
			// IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
			// commonService.selectIndicator(Arrays.asList(4)),
			// "indicatorColumnStr",interValParam.getType(), interval);
			// logger.info("》》selectTransByMap(组件层级以下参数):{}",JSON.toJSONString(paramMap));
			// dataList = netPerformanceMapper.selectListByColumnStr(paramMap);
			// dataList.stream().forEach(x->{
			// x.put("name", nameMap.get(x.get("uuid")));
			// x.put("customId",customMap.get(x.get("uuid")));
			// IndicatorSupportUtil.getAlarmAmountMapByParams((Integer)x.get("uuid"),
			// (Long)x.get("time"),alarmData,x);
			// });
			// }
		} else if (type == 5) {
			// 维度
			if (indicatorType == 1) {
				// 交易
				IndicatorSupportUtil.dealIndicatorTable(2, paramMap, granularity);
				IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
						commonService.selectIndicator(Arrays.asList(1, 2)));
				logger.debug("》》selectTransByMap(组件层级以下参数):{}", JSON.toJSONString(paramMap));
				dataList = bpmTranMapper.selectBpmExtendTranReturnMap(paramMap);
				dataList.stream().forEach(x -> {
					x.put("name", nameMap.get(x.get("uuid")));
					x.put("customId", customMap.get(x.get("uuid")));
					IndicatorSupportUtil.getAlarmAmountMapByParams((Integer) x.get("uuid"), (Long) x.get("time"),
							alarmData, x);
					x.put("valueId", dimensionMap.get(x.get("uuid")).getValue());
				});
			} else {
				throw new RuntimeException("indicatorType参数非法！");
			}

			// else if(indicatorType == 2){
			// //网络
			// paramMap.put("interval", interval);
			// IndicatorSupportUtil.dealIndicatorTable(5, paramMap, granularity);
			// IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
			// commonService.selectIndicator(Arrays.asList(3)),
			// "indicatorColumnStr",interValParam.getType(), interValParam.getInterval());
			// logger.info("》》selectTransByMap(组件层级以下参数):{}",JSON.toJSONString(paramMap));
			// dataList = netPerformanceMapper.selectListByColumnStr(paramMap);
			// dataList.stream().forEach(x->{
			// x.put("valueId",dimensionMap.get(x.get("uuid")).getValue());
			// x.put("name", nameMap.get(x.get("uuid")));
			// x.put("customId",customMap.get(x.get("uuid")));
			// IndicatorSupportUtil.getAlarmAmountMapByParams((Integer)x.get("uuid"),
			// (Long)x.get("time"),alarmData,x);
			// });
			// }else if(indicatorType == 3){
			// //应用
			// IndicatorSupportUtil.dealIndicatorTable(5, paramMap, granularity);
			// IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
			// commonService.selectIndicator(Arrays.asList(4)),
			// "indicatorColumnStr",interValParam.getType(), interval);
			// logger.info("》》selectTransByMap(组件层级以下参数):{}",JSON.toJSONString(paramMap));
			// dataList = netPerformanceMapper.selectListByColumnStr(paramMap);
			// dataList.stream().forEach(x->{
			// x.put("name", nameMap.get(x.get("uuid")));
			// x.put("customId",customMap.get(x.get("uuid")));
			// x.put("valueId",dimensionMap.get(x.get("uuid")).getValue());
			// IndicatorSupportUtil.getAlarmAmountMapByParams((Integer)x.get("uuid"),
			// (Long)x.get("time"),alarmData,x);
			// });
			// }

		} else {
			// 1组件 2全部ip 3ip(port)
			if (indicatorType == 1) {
				IndicatorSupportUtil.dealIndicatorTable(1, paramMap, granularity);
				IndicatorSupportUtil.handleIndicatorStrParamByType(granularity, paramMap, "indicatorColumnStr",
						commonService.selectIndicator(Arrays.asList(1)));
				logger.debug("》》selectTransByMap(组件层级以下参数):{}", JSON.toJSONString(paramMap));
				dataList = bpmTranMapper.selectBpmTranReturnMap(paramMap);
				dataList.stream().forEach(x -> {
					x.put("name", nameMap.get(evUtil.getMapIntValue(x, "uuid")));
					IndicatorSupportUtil.getAlarmAmountMapByParams((Integer) x.get("uuid"), (Long) x.get("time"),
							alarmData, x);
				});
			} else if (indicatorType == 2) {
				paramMap.put("interval", interval);
				IndicatorSupportUtil.dealIndicatorTable(5, paramMap, 0);
				IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
						commonService.selectIndicator(Arrays.asList(3)), "indicatorColumnStr", interValParam.getType(),
						interValParam.getInterval());
				logger.debug("》》selectTransByMap(组件层级以下参数):{}", JSON.toJSONString(paramMap));
				dataList = netPerformanceMapper.selectListByColumnStr(paramMap);
				dataList.stream().forEach(x -> {
					x.put("name", nameMap.get(evUtil.getMapIntValue(x, "uuid")));
					IndicatorSupportUtil.getAlarmAmountMapByParams((Integer) x.get("uuid"), (Long) x.get("time"),
							alarmData, x);
					x.put("customId", customMap.get(x.get("uuid")));
				});
			} else if (indicatorType == 3) {
				IndicatorSupportUtil.dealIndicatorTable(5, paramMap, 0);
				IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap,
						commonService.selectIndicator(Arrays.asList(4)), "indicatorColumnStr", interValParam.getType(),
						interValParam.getInterval());
				logger.debug("》》selectTransByMap(组件层级以下参数):{}", JSON.toJSONString(paramMap));
				dataList = netPerformanceMapper.selectListByColumnStr(paramMap);
				dataList.stream().forEach(x -> {
					x.put("name", nameMap.get(x.get("uuid")));
					IndicatorSupportUtil.getAlarmAmountMapByParams((Integer) x.get("uuid"), (Long) x.get("time"),
							alarmData, x);
					x.put("customId", customMap.get(x.get("uuid")));
				});
			} else {
				throw new RuntimeException("indicatorType参数非法！");
			}
		}

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,指标数据获取：{}ms", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		logger.debug("》》》》当前线程：【{}】,接口总耗时：{}ms", Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());

		return dataList;
	}

	@Override
	public IndicatorDataParam extendChart(IndicatorParam param) {
		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end,
				configurationCache);
		Integer interval=param.getInterval();
		if (interval == null) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}
		List<Long> timeList = indicatorParam.getTimeList();
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(2, map, indicatorParam.getType());
		// 拼接指标sql
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<BpmExtendTran> bpmTranList = bpmTranMapper.selectBpmExtendTranByUuid(map);

		// group by uuid
		Map<Integer, List<BpmExtendTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmExtendTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmExtendTran>> resultGroup = new LinkedHashMap<>();

		uuid.forEach(x -> resultGroup.put(x, initExtendDefault(grouplist.computeIfAbsent(x, ArrayList :: new), timeList)));

		// assembly data
		List<IndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		for (Map.Entry<Integer, List<BpmExtendTran>> entry : resultGroup.entrySet()) {
			for (Integer integer : indicator) {
				IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
				detail.setId(integer);
				detail.setUuid(entry.getKey());
				List<BpmExtendTran> bpm = entry.getValue();
				// 置为map
				List<Map<String, Object>> mapbpm = new ArrayList<>();
				bpm.stream().forEach(x -> {
					Map<String, Object> dataMap = new HashMap<>(14);
					// 添加指标需修改
					dataMap.put("allTransCount", x.getAllTransCount());
					dataMap.put("responseTime", x.getResponseTime());
					dataMap.put("responseRate", x.getResponseRate());
					dataMap.put("successRate", x.getSuccessRate());
					dataMap.put("money", x.getMoney());
					dataMap.put("businessSuccessRate", x.getBusinessSuccessRate());
					mapbpm.add(dataMap);
				});
				// 获取指标名
				String colname = colnameList.get(integer);
				// 根据指标名取值
				List<String> invalue = new ArrayList<>();
				mapbpm.stream().forEach(x -> {
					String str = null;
					if (null != x.get(colname)) {
						str = x.get(colname).toString();
					}
					invalue.add(str);
				});

				detail.setValue(invalue);
				value.add(detail);
			}
			result.setIndicator(value);
		}
		return result;
	}
	
	@Override
	public IndicatorDataParam extendCombineChart(IndicatorParam param) {
		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		Integer interval=param.getInterval();
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		IndicatorIntervalParam indicatorParam =null;
		if (interval == null) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}
		List<Long> timeList = indicatorParam.getTimeList();
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(2, map, indicatorParam.getType());
		// 拼接指标sql
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<BpmExtendTran> bpmTranList = bpmTranMapper.selectCombineExtendTran(map);
		//补数据
		bpmTranList=initExtendDefault(bpmTranList,timeList);

		// assembly data
		List<IndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		
			for (Integer integer : indicator) {
				IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
				detail.setId(integer);
				// 置为map
				List<Map<String, Object>> mapbpm = new ArrayList<>();
				bpmTranList.stream().forEach(x -> {
					Map<String, Object> dataMap = new HashMap<>(14);
					// 添加指标需修改
					dataMap.put("allTransCount", x.getAllTransCount());
					dataMap.put("responseTime", x.getResponseTime());
					dataMap.put("responseRate", x.getResponseRate());
					dataMap.put("successRate", x.getSuccessRate());
					dataMap.put("money", x.getMoney());
					dataMap.put("businessSuccessRate", x.getBusinessSuccessRate());
					mapbpm.add(dataMap);
				});
				// 获取指标名
				String colname = colnameList.get(integer);
				// 根据指标名取值
				List<String> invalue = new ArrayList<>();
				mapbpm.stream().forEach(x -> {
					String str = null;
					if (null != x.get(colname)) {
						str = x.get(colname).toString();
					}
					invalue.add(str);
				});

				detail.setValue(invalue);
				value.add(detail);
			}
			result.setIndicator(value);
		
		return result;
	}


	/**
	 *
	 * @param data
	 * @param timeList
	 * @return
	 */
	private List<BpmExtendTran> initExtendDefault(List<BpmExtendTran> data, List<Long> timeList) {
		List<BpmExtendTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmExtendTran trans : data) {
				if (time.equals(trans.getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmExtendTran(time));
			}
		}
		return result;
	}

	private List<BpmExtendTran> initExtendDateDefault(List<BpmExtendTran> data, List<Long> timeList) {
		List<BpmExtendTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmExtendTran trans : data) {
				if (time.equals(trans.getDate().getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmExtendTran(time));
			}
		}
		return result;
	}

	@Override
	public List<TransactionExtendTableParam> extendTable(IndicatorParam param) {
		List<TransactionExtendTableParam> table = new ArrayList<>();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return table;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		Integer interval=param.getInterval();
		IndicatorIntervalParam indicatorParam = new IndicatorIntervalParam();
		if (null == interval) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}
		List<Long> timeList = indicatorParam.getTimeList();
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(2, map, indicatorParam.getType());
		// 拼接指标sql
		List<String> indicators = new ArrayList<>();
		List<Integer> indicator = param.getIndicator();
		if (null != indicator) {
			// 获取指标名
			Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
			for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
				indicators.add(ename.getValue());
			}
		} else {
			List<Integer> type = new ArrayList<>();
			type.add(1);
			type.add(2);
			indicators = commonService.selectIndicator(type);
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, indicators, evUtil.getMapIntValue(map, "type"));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<BpmExtendTran> bpmTranList = bpmTranMapper.selectBpmExtendTranByUuid(map);

		// group by uuid
		Map<Integer, List<BpmExtendTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmExtendTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmExtendTran>> resultGroup = new LinkedHashMap<>();

		uuid.forEach(x -> resultGroup.put(x, initExtendDefault(grouplist.computeIfAbsent(x, ArrayList :: new), timeList)));

		// assembly data
		for (int i = 0; i < timeList.size(); i++) {
			for (Map.Entry<Integer, List<BpmExtendTran>> entry : resultGroup.entrySet()) {
				TransactionExtendTableParam trans = new TransactionExtendTableParam();
				// 添加指标需修改
				trans.setTime(timeList.get(i));
				trans.setUuid(entry.getKey());
				trans.setAllTransCount(entry.getValue().get(i).getAllTransCount());
				trans.setResponseRate(entry.getValue().get(i).getResponseRate());
				trans.setResponseTime(entry.getValue().get(i).getResponseTime());
				trans.setSuccessRate(entry.getValue().get(i).getSuccessRate());
				trans.setMoney(entry.getValue().get(i).getMoney());
				trans.setBusinessSuccessRate(entry.getValue().get(i).getBusinessSuccessRate());
				table.add(trans);
			}
		}

		return table;
	}
	@Override
	public TransactionExtendTableParam extendCombineTable(IndicatorParam param) {
		TransactionExtendTableParam table = new TransactionExtendTableParam();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return table;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		Integer interval=param.getInterval();
		IndicatorIntervalParam indicatorParam = new IndicatorIntervalParam();
		if (null == interval) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(2, map, indicatorParam.getType());
		// 拼接指标sql
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		map.put("indicatorColumnStr", map.get("indicatorColumnStr").toString().substring(1));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<BpmExtendTran> bpmTranList = bpmTranMapper.selectCombineExtendTranTable(map);
		if(!evUtil.listIsNullOrZero(bpmTranList)) {
			table.setAllTransCount(bpmTranList.get(0).getAllTransCount());
			table.setResponseRate(bpmTranList.get(0).getResponseRate());
			table.setResponseTime(bpmTranList.get(0).getResponseTime());
			table.setSuccessRate(bpmTranList.get(0).getSuccessRate());
			table.setMoney(bpmTranList.get(0).getMoney());
			table.setBusinessSuccessRate(bpmTranList.get(0).getBusinessSuccessRate());
		}
		return table;
	}

	@Override
	public IndicatorDataParam selectIndicatorDataByAlarmRule(IndicatorParam param) {
		/**
		 * 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度 ， 7 监控点告警 , 8 数据源告警； 9自定义-- 业务 ，10自定义--
		 * 统计维度， 11自定义-- 普通维度， 12自定义-- 多维度'
		 */

		// todo 监控点和数据源告警
		int ruleType = param.getRuleType();
		// 1 应用 ，2 组件， 3ip， 4port，9自定义-- 业务 ，10自定义-- 统计维度，
		if (ruleType == 1 || ruleType == 2 || ruleType == 3 || ruleType == 4 || ruleType == 9 || ruleType == 10) {
			return this.bpmAndExtendChart(param);
		} else if (ruleType == 5 || ruleType == 6 || ruleType == 11 || ruleType == 12) {
			// 5单维度 ，6 多维度 ，11自定义-- 普通维度， 12自定义-- 多维度'
			return this.getPreIndicatorByDimension(param);
		} else {
			return new IndicatorDataParam();
		}

	}

	/**
	 * 根据告警规则查询指标数据
	 * 
	 * @param dataList
	 *            原数据
	 * @param timeList
	 *            时间数组
	 * @param indicators
	 *            实际是告警模板的数据(包含重复数据)
	 * @param relation
	 *            逻辑关系 1:or 2: and
	 * @param needMerge
	 *            是否合并其它模板的数据
	 * @param abnormalAlarmTime
	 *            其它告警模板所出的数据(忽略指标)
	 * @return
	 */
	private List<IndicatorDataDetailParam> getIndicatorDataParamByAlarmRule(List<IndicatorDataDetailParam> dataList,
			List<Long> timeList, List<Indicator> indicators, Integer relation, boolean needMerge,
			List<Long> abnormalAlarmTime) {

		/**
		 * 按照指标id进行分组(原因:配置相同的模板指标)
		 */
		Map<Integer, List<Indicator>> filterIndicatorMap = new HashMap<>();

		// 去重之后的大小颗粒度模板
		Set<Indicator> indicatorSet = new LinkedHashSet<>();

		// 大小颗粒度模板集合(包含重复)
		List<Indicator> originIndicators = new ArrayList<>();

		indicators.forEach(x -> {
			// 只进行大小颗粒度模板过滤 对应模板相应指标参考SVN:\easyviews4\需求文档\页面细节.docx
			if (x.getTemplateCode() == 2 || x.getTemplateCode() == 4) {
				originIndicators.add(x);
				indicatorSet.add(x);

				filterIndicatorMap.computeIfAbsent(x.getId(),  ArrayList :: new).add(x);

			}
		});

		if (filterIndicatorMap.isEmpty()) {
			return dataList;
		}

		List<Indicator> ruleIndicatorList;
		Integer templateCode;
		String columnType;
		Double threshold;

		if (relation == 1) {
			// or 关系 任意条件
			for (IndicatorDataDetailParam detailParam : dataList) {
				List<Long> filterTime = new ArrayList<>();
				ruleIndicatorList = filterIndicatorMap.get(detailParam.getId());
				if (!evUtil.listIsNullOrZero(ruleIndicatorList)) {
					for (Indicator indicator : ruleIndicatorList) {
						templateCode = indicator.getTemplateCode();
						columnType = indicator.getColumnType();
						threshold = indicator.getThreshlod();
						// 或的关系,贪心比较
						for (int i = 0; i < timeList.size(); i++) {
							if (filterTime.size() - 1 >= i) {
								// 已经包含 do nothing
							} else {
								if (this.compareObjByType(threshold, detailParam.getValue().get(i), columnType,
										templateCode)) {
									filterTime.add(timeList.get(i));
								}
							}
						}
						detailParam.setAbnormalTime(filterTime);
					}
				}
			}

		} else {
			/**
			 * and 条件 1、相同指标和不同指标严格遵循 and 逻辑 2、需要与其他模板(大数据出的告警)告警点比较 3、清华基线不在and逻辑内
			 *
			 * 程序期待结果： 所有指标告警时间点必须一致，不会出现不一致的情况，清华基线除外
			 */
			List<Long> filterTime = new ArrayList<>();
			boolean flag, abnormalFlag;
			Iterator<Long> abnormalIterator;
			Long abnormalTime;
			int indicatorId;
			for (int i = 0; i < timeList.size(); i++) {
				if (needMerge) {
					abnormalIterator = abnormalAlarmTime.iterator();
					abnormalFlag = false;
					while (abnormalIterator.hasNext()) {
						abnormalTime = abnormalIterator.next();
						if (abnormalTime.equals(timeList.get(i))) {
							abnormalIterator.remove();
							abnormalFlag = true;
							break;
						}
					}
					if (!abnormalFlag) {
						continue;
					}
				}
				flag = true;
				for (IndicatorDataDetailParam detailParam : dataList) {
					// 指标匹配规则
					indicatorId = detailParam.getId();
					ruleIndicatorList = filterIndicatorMap.get(indicatorId);
					if (!evUtil.listIsNullOrZero(ruleIndicatorList)) {
						for (Indicator indicator : ruleIndicatorList) {
							templateCode = indicator.getTemplateCode();
							threshold = indicator.getThreshlod();
							columnType = indicator.getColumnType();
							if (!this.compareObjByType(threshold, detailParam.getValue().get(i), columnType,
									templateCode)) {
								flag = false;
								break;
							}
						}
						if (!flag) {
							break;
						}
					}
				}
				if (flag) {
					filterTime.add(timeList.get(i));
				}
			}

			// 组装数据
			if (!evUtil.listIsNullOrZero(filterTime)) {
				for (IndicatorDataDetailParam detailParam : dataList) {
					detailParam.setAbnormalTime(filterTime);
				}
			}
		}
		return dataList;
	}

	/**
	 * 比较大小
	 * 
	 * @param target
	 * @param value
	 * @param columnType
	 * @param rule
	 * @return
	 */
	private boolean compareObjByType(Object target, String value, String columnType, int rule) {
		boolean result = false;
		if (value == null) {
			return result;
		}
		if (rule == 1 || rule == 2) {
			if (Double.parseDouble(target.toString()) < Double.parseDouble(value)) {
				result = true;
			}
		} else {

			if (Double.parseDouble(target.toString()) > Double.parseDouble(value)) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public IndicatorDataParam getPreIndicatorByDimension(IndicatorParam param) {

		Long start = param.getStart();
		Long end = param.getEnd();

		Integer interval = Integer.parseInt(configurationCache.getValue(ConfigConstants.BPM_INTERVAL));
		IndicatorIntervalParam intervalParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end,
				interval);
		start = intervalParam.getStart();
		end = intervalParam.getEnd();

		List<Long> timeList = intervalParam.getTimeList();

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("start", start);
		paramMap.put("end", end);

		// handle dimensionConditions
		List<String> columnStrList = new ArrayList<>();
		List<JSONObject> filterDimensionIds = param.getFilterDimension();
		int tableType = param.getType();
		if (!evUtil.listIsNullOrZero(filterDimensionIds)) {
			List<Integer> dimensionIds = new ArrayList<>();
			filterDimensionIds.forEach(x -> {
				dimensionIds.add(x.getIntValue("dimensionId"));
			});

			List<String> conditionStr = new ArrayList<>();
			if (!evUtil.listIsNullOrZero(filterDimensionIds)) {
				for (JSONObject jsonObject : filterDimensionIds) {
					columnStrList.add(jsonObject.getString("ename"));
				}
				String dimensionCondition;
				for (JSONObject jsonObject : filterDimensionIds) {
					conditionStr.add(jsonObject.getString("ename") + " = '" + jsonObject.getString("value") + "'");
				}
				if (!evUtil.listIsNullOrZero(conditionStr)) {
					dimensionCondition = StringUtils.join(conditionStr, " AND ");
					paramMap.put("dimensionCondition", " AND " + dimensionCondition);
				}
				String dimensionColumns;
				if (!evUtil.listIsNullOrZero(columnStrList)) {
					dimensionColumns = StringUtils.join(columnStrList, " , ");
					paramMap.put("dimensionColumns", " , " + dimensionColumns);
				}
			}
		}

		Set<String> columns = new HashSet<>();
		List<Indicator> indicators = param.getIndicatorList();
		indicators.stream().forEach(x -> {
			columns.add(x.getColumnName());
		});

		List<Map<String, Object>> data = new ArrayList<>();
		paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		// table
		if (tableType == 1) {
			String table = PER_TABLE_FIX_BPM;
			paramMap.put("table", table);
			paramMap.put("type", 0);
			paramMap.put("componentId", param.getUuid().get(0));
			IndicatorSupportUtil.handleIndicatorStrParamByType(0, paramMap, "indicatorColumnStr",
					new ArrayList<>(columns));
			logger.debug("getPreIndicatorByDimension查询参数:{}", JSON.toJSONString(paramMap));
			data = bpmPreMapper.getMultiChat(paramMap);
		}
		if (tableType == 2) {
			String table = PER_TABLE_FIX_EXTEND;
			paramMap.put("table", table);
			paramMap.put("applicationId", param.getApplicationId());
			paramMap.put("customId", param.getCustomId());
			paramMap.put("type", 0);
			IndicatorSupportUtil.handleIndicatorStrParamByType(0, paramMap, "indicatorColumnStr",
					new ArrayList<>(columns));
			logger.debug("getPreIndicatorByDimension查询参数:{}", JSON.toJSONString(paramMap));
			data = bpmPreMapper.getCustomMultiChat(paramMap);
		}

		data = IndicatorSupportUtil.getForMatList(data, timeList);
		IndicatorDataParam result = new IndicatorDataParam();
		result.setTime(timeList);
		/**
		 * 按照指标id进行分组(原因:配置相同的模板指标)
		 */
		Map<Integer, List<Indicator>> filterIndicatorMap = new HashMap<>();
		Set<Indicator> indicatorSet = new LinkedHashSet<>();
		List<Indicator> originIndicators = new ArrayList<>();

		List<Integer> specialIndicatorIds = param.getSpecialIndicators();

		indicators.forEach(x -> {
			indicatorSet.add(x);
			// 只进行大小模板过滤
			if (x.getTemplateCode() == 2 || x.getTemplateCode() == 4) {
				originIndicators.add(x);
				if (filterIndicatorMap.get(x.getId()) != null) {
					// 特殊指标*100
					if (!evUtil.listIsNullOrZero(specialIndicatorIds) && specialIndicatorIds.contains(x.getId())) {
						x.setThreshlod(x.getThreshlod() * 100);
					}
					filterIndicatorMap.get(x.getId()).add(x);
				} else {
					List<Indicator> indicatorList = new ArrayList<>();
					indicatorList.add(x);
					// 特殊指标*100
					if (!evUtil.listIsNullOrZero(specialIndicatorIds) && specialIndicatorIds.contains(x.getId())) {
						x.setThreshlod(x.getThreshlod() * 100);
					}
					filterIndicatorMap.put(x.getId(), indicatorList);
				}
			}
		});

		List<IndicatorDataDetailParam> indicatorResList = new ArrayList<>();
		// 组装数据
		for (Indicator indicator : indicatorSet) {
			IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
			List<String> value = new ArrayList<>();
			String name = indicator.getColumnName();
			for (Map<String, Object> map : data) {
				value.add(map.get(name) == null ? null : map.get(name).toString());
			}
			detail.setId(indicator.getId());
			detail.setValue(value);
			detail.setName(indicator.getName());
			detail.setUnit(indicator.getUnit());
			detail.setType(indicator.getType());
			indicatorResList.add(detail);
		}
		result.setIndicator(indicatorResList);
		if (filterIndicatorMap.isEmpty()) {
			return result;
		}
		// result.getIndicator().addAll(this.getIndicatorDataParamByAlarmRule(indicatorResList,timeList,
		// indicators,
		// param.getRelation(), param.isNeedMerge(),param.getAbnormalTime()));

		result.setIndicator(this.getIndicatorDataParamByAlarmRule(indicatorResList, timeList, indicators,
				param.getRelation(), param.isNeedMerge(), param.getAbnormalTime()));

		return result;
	}

	@Override
	public ReportLineIndicatorParam getReportLineIndicator(ReportIndicatorParam param) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		ReportLineIndicatorParam result = new ReportLineIndicatorParam();
		Integer interval = param.getInterval();
		// 0 正常 1 广发特有逻辑 天级数据分段查询数据
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}

		// Long start = param.getStartTime(), end = param.getEndTime();

		// get indicator columns
		List<Integer> indicators = param.getIndicators();
		Map<Integer, Map<String, Object>> columnsNamesMap = indicatorService.getIndicatorMapByIds(indicators);

		// get indicator columnName for generate indicator str sql
		List<Integer> types = new ArrayList<>();
		types.add(1);
		List<String> columnNames = commonService.selectIndicator(types);

		// get indicator columns
		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if (intervalType == 1) {
			dInterval = 5 * 60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		} else {
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval);
		}

		// result.setTime(indicatorParam.getTimeList());
		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,从缓存中获取参数耗时：{}ms", Thread.currentThread().getId(),
				stopWatch.getLastTaskTimeMillis());
		stopWatch.start();
		// List<Long> timeList = indicatorParam.getTimeList();

		Map<String, Object> map = new HashMap<>();
		// deal with table and tableType
		IndicatorSupportUtil.dealIndicatorTable(1, map, indicatorParam.getType());

		map.put("ids", uuid);

		// map.put("start", start);
		// map.put("end", end);
		// map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start,end));

		List<Map<String, Object>> timeParts = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timeParts, dInterval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		map.put("timeStr", " and (" + timeStr + ")");
		map.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		// deal with indicator str
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), map, "indicatorColumnStr",
				columnNames);

		List<Long> timeList = IndicatorSupportUtil.generateTimeList(timePart, interval, intervalType);
		if (intervalType == 1) {
			map.put("groupColumns", " date ");
		} else {
			map.put("groupColumns", " time ");
		}
		logger.info("折线图指标查询参数:{}", map);
		List<BpmTran> bpmTranList = new ArrayList<>();
		if(interval.equals(INTERVAL_MINUTE)){
			bpmTranList = bpmTranMapper.selectBpmTranMinuteByUuid(map);
		}else {
			bpmTranList = bpmTranMapper.selectBpmTranByUuid(map);
		}
		int compare = param.getCompare();
		if (compare == 1 || compare == 2) {
			// lastCycle
			// Long compareStartTime = param.getCompareStartTime();
			// Long compareEndTime = param.getCompareEndTime();
			// IndicatorIntervalParam indicatorCompareParam =
			// IndicatorSupportUtil.getIndicatorIntervalByInterVal(compareStartTime,
			// compareEndTime, param.getInterval());
			// map.put("start", compareStartTime);
			// map.put("end", compareEndTime);
			// map.put("dateStr",
			// DateUtils.getDateStrByStartAndEnd(compareStartTime,compareEndTime));

			List<Map<String, Object>> timePartsCompare = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(timePartsCompare, dInterval);
			String compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
			map.put("timeStr", " and (" + compareTimeStr + ")");
			map.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
			List<Long> compareTimeList = IndicatorSupportUtil.generateTimeList(compareTimePart, interval, intervalType);
			// deal with indicator str
			IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), map, "indicatorColumnStr",
					columnNames);
			logger.info("折线图对比指标查询参数:{}", map);
			List<BpmTran> lastCycleBpmTranList = new ArrayList<>();
			if(interval.equals(INTERVAL_MINUTE)) {
				lastCycleBpmTranList = bpmTranMapper.selectBpmTranMinuteByUuid(map);
			}else {
				lastCycleBpmTranList = bpmTranMapper.selectBpmTranByUuid(map);
			}
			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,查询数据库耗时：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			result = assembleLineCompareData(lastCycleBpmTranList, bpmTranList, uuid, timeList, compareTimeList,
					columnsNamesMap, true, intervalType);
		} else {
			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,查询数据库耗时：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			result = assembleLineCompareData(new ArrayList<>(), bpmTranList, uuid, timeList, null, columnsNamesMap,
					false, intervalType);
		}
		logger.debug("》》》》当前线程：【{}】,接口总耗时：{}s", Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());
		result.setTime(timeList);
		return result;
	}

	@Override
	public ReportLineIndicatorParam getExtendReportLineIndicator(ReportIndicatorParam param) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		Integer interval = param.getInterval();
		// 0 正常 1 广发特有逻辑 天级数据分段查询数据
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();

		ReportLineIndicatorParam result = new ReportLineIndicatorParam();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}

		// Long start = param.getStartTime(), end = param.getEndTime();

		List<JSONObject> customIndicators = param.getCustomIndicators();
		Set<Integer> indicatorSet = new HashSet<>();
		if (evUtil.listIsNullOrZero(customIndicators)) {
			throw new RuntimeException("传入的自定义事件指标参数为空！");
		}

		// handle duplicate indicatorIds
		List<Integer> customIds = new ArrayList<>();
		Map<Integer, List<Integer>> customIndicatorMap = new HashMap<>();
		for (JSONObject json : customIndicators) {
			List<Integer> indicators = json.getJSONArray("indicators").toJavaList(Integer.class);
			indicatorSet.addAll(indicators);
			customIndicatorMap.put(json.getInteger("customId"), indicators);
			customIds.add(json.getInteger("customId"));
		}
		Map<String, Object> paramMap = new HashMap<>(3);
		paramMap.put("ids", customIds);
		List<Map<String, Object>> indicators = commonService.selectCustomIndicatorNamesByMap(paramMap);
		Integer customIdKey;
		List<Integer> indicatorIds;
		int mCustomId, mIndicatorId;
		// key:customId value:list(indicator)
		Map<Integer, List<Map<String, Object>>> indicatorMap = new HashMap<>();
		for (Map.Entry<Integer, List<Integer>> entry : customIndicatorMap.entrySet()) {
			customIdKey = entry.getKey();
			indicatorIds = entry.getValue();
			for (Integer indicatorId : indicatorIds) {
				Iterator<Map<String, Object>> iterator = indicators.iterator();
				while (iterator.hasNext()) {
					Map<String, Object> map = iterator.next();
					mCustomId = evUtil.getMapIntegerValue(map, "customId");
					mIndicatorId = evUtil.getMapLong(map, "indicatorId").intValue();
					if (customIdKey.equals(mCustomId) && indicatorId.equals(mIndicatorId)) {
						indicatorMap.computeIfAbsent(mCustomId,  ArrayList :: new).add(map);
						iterator.remove();
						break;
					}
				}
			}
		}

		Map<String, Object> map = new HashMap<>(9);
		// get indicator columns
		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if (intervalType == 1) {
			dInterval = 5 * 60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
			map.put("groupColumns", " date ");
		} else {
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval);
			map.put("groupColumns", " time ");
		}

		// IndicatorIntervalParam indicatorParam =
		// IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end,
		// param.getInterval());

		stopWatch.stop();
		logger.debug("》》》》当前线程：【{}】,从缓存中获取参数耗时：{}ms", Thread.currentThread().getId(),
				stopWatch.getLastTaskTimeMillis());
		stopWatch.start();
		// List<Long> timeList = indicatorParam.getTimeList();

		IndicatorSupportUtil.dealIndicatorTable(2, map, indicatorParam.getType());
		map.put("ids", uuid);
		// map.put("start", start);
		// map.put("end", end);
		// map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start,end));

		List<Map<String, Object>> timeParts = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timeParts, dInterval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		map.put("timeStr", " and (" + timeStr + ")");
		map.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));

		List<Long> timeList = IndicatorSupportUtil.generateTimeList(timePart, interval, intervalType);

		// deal with indicator str
		List<Integer> types = new ArrayList<>();
		types.add(1);
		types.add(2);
		List<String> columnNames = commonService.selectIndicator(types);
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), map, "indicatorColumnStr",
				columnNames);
		logger.info("折线图事件指标查询参数:{}", map);
		List<BpmExtendTran> extendBpmTranList = new ArrayList<>();
		if(interval.equals(INTERVAL_MINUTE)){
			extendBpmTranList = bpmTranMapper.selectBpmExtendTranMinuteByUuid(map);
		}else {
			extendBpmTranList=bpmTranMapper.selectBpmExtendTranByUuid(map);
		}
		
		int compare = param.getCompare();
		if (compare == 1 || compare == 2) {
			// lastCycle
			// Long compareStartTime = param.getCompareStartTime();
			// Long compareEndTime = param.getCompareEndTime();
			// map.put("start", compareStartTime);
			// map.put("end", compareEndTime);
			// IndicatorIntervalParam indicatorCompareParam =
			// IndicatorSupportUtil.getIndicatorIntervalByInterVal(compareStartTime,
			// compareEndTime, param.getInterval());
			// map.put("dateStr",
			// DateUtils.getDateStrByStartAndEnd(compareStartTime,compareEndTime));

			List<Map<String, Object>> timePartsCompare = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(timePartsCompare, dInterval);
			String compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
			map.put("timeStr", " and (" + compareTimeStr + ")");
			map.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
			List<Long> compareTimeList = IndicatorSupportUtil.generateTimeList(compareTimePart, interval, intervalType);
			// deal with indicator str
			IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), map, "indicatorColumnStr",
					columnNames);
			List<BpmExtendTran> lastCycleExtendBpmTranList =new ArrayList<>();
			if(interval.equals(INTERVAL_MINUTE)){
				lastCycleExtendBpmTranList = bpmTranMapper.selectBpmExtendTranMinuteByUuid(map);
			}else {
				lastCycleExtendBpmTranList=bpmTranMapper.selectBpmExtendTranByUuid(map);
			}
			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,查询数据库耗时：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			result = assembleExtendLineCompareData(lastCycleExtendBpmTranList, extendBpmTranList, uuid, timeList,
					compareTimeList, indicatorMap, true, intervalType);
		} else {
			stopWatch.stop();
			logger.debug("》》》》当前线程：【{}】,查询数据库耗时：{}ms", Thread.currentThread().getId(),
					stopWatch.getLastTaskTimeMillis());
			result = assembleExtendLineCompareData(new ArrayList<>(), extendBpmTranList, uuid, timeList, null,
					indicatorMap, false, intervalType);
		}
		result.setTime(timeList);
		logger.debug("》》》》当前线程：【{}】,接口总耗时：{}s", Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());
		return result;
	}

	/**
	 * AI报表组装数据
	 * 
	 * @author NJ
	 * @date 2019/4/11 17:48
	 * @param lastCycleBpmTranList
	 * @param bpmTranList
	 * @param uuid
	 * @param timeList
	 * @param indicatorIdMap
	 * @param isCompare
	 * @return com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam
	 */
	public ReportLineIndicatorParam assembleLineCompareData(List<BpmTran> lastCycleBpmTranList,
			List<BpmTran> bpmTranList, List<Integer> uuid, List<Long> timeList, List<Long> compareTimeList,
			Map<Integer, Map<String, Object>> indicatorIdMap, boolean isCompare, int intervalType) {
		if (evUtil.listIsNullOrZero(lastCycleBpmTranList)) {
			lastCycleBpmTranList = new ArrayList<>();
		}

		ReportLineIndicatorParam result = new ReportLineIndicatorParam();
		// group by uuid
		Map<Integer, List<BpmTran>> groupUuid = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmTran>> lastCycleGroupUuid = lastCycleBpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getUuid, Collectors.toList()));

		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();
		Map<Integer, List<BpmTran>> lastCycleResultGroup = new LinkedHashMap<>();

		uuid.forEach(x -> {
			if (groupUuid.containsKey(x)) {
				if (intervalType == 0) {
					resultGroup.put(x, initDefault(groupUuid.get(x), timeList));
				} else {
					resultGroup.put(x, initDateDefault(groupUuid.get(x), timeList));
				}
			} else {
				// init default list
				if (intervalType == 0) {
					resultGroup.put(x, initDefault(new ArrayList<>(), timeList));
				} else {
					resultGroup.put(x, initDateDefault(new ArrayList<>(), timeList));
				}
			}
			if (isCompare) {
				if (lastCycleGroupUuid.containsKey(x)) {
					if (intervalType == 0) {
						lastCycleResultGroup.put(x, initDefault(lastCycleGroupUuid.get(x), compareTimeList));
					} else {
						lastCycleResultGroup.put(x, initDateDefault(lastCycleGroupUuid.get(x), compareTimeList));
					}
				} else {
					// init default list
					if (intervalType == 0) {
						lastCycleResultGroup.put(x, initDefault(new ArrayList<>(), compareTimeList));
					} else {
						lastCycleResultGroup.put(x, initDateDefault(new ArrayList<>(), compareTimeList));
					}
				}
			}
		});

		// assembly data
		List<ReportIndicatorDetailParam> detailParams = new ArrayList<>();
		Integer indicatorId;
		Map<String, Object> indicatorData;
		String indicatorColumnName;
		for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
			for (Map.Entry<Integer, Map<String, Object>> indicatorMap : indicatorIdMap.entrySet()) {
				indicatorData = indicatorMap.getValue();
				indicatorId = Integer.parseInt(String.valueOf(indicatorData.get("id")));
				List<BpmTran> bpmValues = entry.getValue();
				List<BpmTran> lastCycleBpmValues = lastCycleResultGroup.get(entry.getKey());
				ReportIndicatorDetailParam detailParam = new ReportIndicatorDetailParam();
				// get indicator columnName
				indicatorColumnName = String.valueOf(indicatorData.get("columnName"));
				List<String> values = new ArrayList<>();
				List<String> lastCycleValues = new ArrayList<>();
				for (int i = 0; i < bpmValues.size(); i++) {
					BpmTran bpmValue = bpmValues.get(i);
					Object bpmData = getIndicatorByName(indicatorColumnName, bpmValue);
					values.add(bpmData == null ? null : bpmData.toString());
					if (isCompare && i < lastCycleBpmValues.size()) {
						BpmTran lastCycleBpmValue = lastCycleBpmValues.get(i);
						Object lastCycleBpmData = getIndicatorByName(indicatorColumnName, lastCycleBpmValue);
						lastCycleValues.add(lastCycleBpmData == null ? null : lastCycleBpmData.toString());
					}
				}

				detailParam.setUuid(entry.getKey());
				detailParam.setValueCP(lastCycleValues);
				detailParam.setValue(values);

				detailParam.setId(indicatorId);
				detailParam.setName(evUtil.getMapStrValue(indicatorData, "name"));
				detailParam.setUnit(evUtil.getMapStrValue(indicatorData, "unit"));

				detailParams.add(detailParam);
			}
		}
		result.setIndicators(detailParams);
		return result;
	}

	/**
	 * 格式化特殊指标的阈值,一般为响应率成功率等,该指标配置在参数配置中去
	 * 
	 * @author NJ
	 * @date 2018/12/10 20:15
	 * @param indicators
	 * @param specialIds
	 * @return void
	 */
	private void formatSpecialIndicator(List<Indicator> indicators, List<Integer> specialIds) {
		List<Integer> specialList = specialIds;
		if (!evUtil.listIsNullOrZero(specialList)) {
			for (Indicator indicator : indicators) {
				if (specialList.contains(indicator.getId())) {
					indicator.setThreshlod(indicator.getThreshlod() * 100);
				}
			}
		}
	}

	/**
	 * 有对比日期的组装数据
	 * 
	 * @param lastCycleTranList
	 * @param bpmTranList
	 * @param uuid
	 * @param timeList
	 * @param customIndicatorIdMap
	 * @return
	 */
	public ReportLineIndicatorParam assembleExtendLineCompareData(List<BpmExtendTran> lastCycleTranList,
			List<BpmExtendTran> bpmTranList, List<Integer> uuid, List<Long> timeList, List<Long> compareTimeList,
			Map<Integer, List<Map<String, Object>>> customIndicatorIdMap, boolean isCompare, int intervalType) {
		ReportLineIndicatorParam result = new ReportLineIndicatorParam();
		// group by uuid
		Map<Integer, List<BpmExtendTran>> groupUuid = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmExtendTran::getUuid, Collectors.toList()));

		Map<Integer, List<BpmExtendTran>> lastCycleGroupUuid = lastCycleTranList.stream()
				.collect(Collectors.groupingBy(BpmExtendTran::getUuid, Collectors.toList()));

		Map<Integer, List<BpmExtendTran>> resultGroup = new LinkedHashMap<>();
		Map<Integer, List<BpmExtendTran>> lastCycleResultGroup = new LinkedHashMap<>();
		uuid.forEach(x -> {
			if (groupUuid.containsKey(x)) {
				if (intervalType == 0) {
					resultGroup.put(x, initExtendDefault(groupUuid.get(x), timeList));
				} else {
					resultGroup.put(x, initExtendDateDefault(groupUuid.get(x), timeList));
				}
			} else {
				// init default list
				if (intervalType == 0) {
					resultGroup.put(x, initExtendDefault(new ArrayList<>(), timeList));
				} else {
					resultGroup.put(x, initExtendDateDefault(new ArrayList<>(), timeList));
				}
			}
			if (isCompare) {
				if (lastCycleGroupUuid.containsKey(x)) {
					if (intervalType == 0) {
						lastCycleResultGroup.put(x, initExtendDefault(lastCycleGroupUuid.get(x), compareTimeList));
					} else {
						lastCycleResultGroup.put(x, initExtendDateDefault(lastCycleGroupUuid.get(x), compareTimeList));
					}
				} else {
					// init default list
					if (intervalType == 0) {
						lastCycleResultGroup.put(x, initExtendDefault(new ArrayList<>(), compareTimeList));
					} else {
						lastCycleResultGroup.put(x, initExtendDateDefault(new ArrayList<>(), compareTimeList));
					}
				}
			}
		});

		// assembly data
		Integer uuidKey;
		String colName;
		List<Map<String, Object>> indicators;
		List<ReportIndicatorDetailParam> detailParams = new ArrayList<>();
		for (Map.Entry<Integer, List<BpmExtendTran>> entry : resultGroup.entrySet()) {
			uuidKey = entry.getKey();
			indicators = customIndicatorIdMap.get(uuidKey);
			// todo 可以考虑将空间换时间
			for (Map<String, Object> indMap : indicators) {
				colName = evUtil.getMapStrValue(indMap, "columnName");
				List<BpmExtendTran> bpmValues = entry.getValue();
				List<BpmExtendTran> lastCycleBpmValues = lastCycleResultGroup.get(entry.getKey());
				ReportIndicatorDetailParam detailParam = new ReportIndicatorDetailParam();
				List<String> values = new ArrayList<>();
				List<String> lastCycleValues = new ArrayList<>();
				for (int i = 0; i < bpmValues.size(); i++) {
					BpmExtendTran bpmValue = bpmValues.get(i);
					Object bpmData = getExtendIndicatorByName(colName, bpmValue);
					values.add(bpmData == null ? null : bpmData.toString());
					if (isCompare && i < lastCycleBpmValues.size()) {
						BpmExtendTran lastCycleBpmValue = lastCycleBpmValues.get(i);
						Object lastCycleBpmData = getExtendIndicatorByName(colName, lastCycleBpmValue);
						lastCycleValues.add(lastCycleBpmData == null ? null : lastCycleBpmData.toString());
					}
				}
				detailParam.setValueCP(lastCycleValues);
				detailParam.setValue(values);
				detailParam.setUuid(uuidKey);
				detailParam.setName(evUtil.getMapStrValue(indMap, "indicatorName"));
				detailParam.setUnit(evUtil.getMapStrValue(indMap, "unit"));
				detailParam.setId(evUtil.getMapLong(indMap, "indicatorId").intValue());
				detailParams.add(detailParam);
			}
		}
		result.setIndicators(detailParams);
		return result;
	}

	/**
	 * 根据指标的名称映射获取对象中相应的值
	 * 
	 * @param columnName
	 * @param bpmTran
	 * @return
	 */
	public Object getIndicatorByName(String columnName, BpmTran bpmTran) {
		Object result;
		if ("responseTime".equals(columnName)) {
			result = bpmTran.getResponseTime();
		} else if ("allTransCount".equals(columnName)) {
			result = bpmTran.getAllTransCount();
		} else if ("responseRate".equals(columnName)) {
			result = bpmTran.getResponseRate();
		} else if ("successRate".equals(columnName)) {
			result = bpmTran.getSuccessRate();
		} else {
			throw new RuntimeException("传入的指标参数有误！");
		}
		return result;
	}

	/**
	 *
	 * @param columnName
	 * @param bpmTran
	 * @return
	 */
	public Object getExtendIndicatorByName(String columnName, BpmExtendTran bpmTran) {
		Object result;
		if ("responseTime".equals(columnName)) {
			result = bpmTran.getResponseTime();
		} else if ("allTransCount".equals(columnName)) {
			result = bpmTran.getAllTransCount();
		} else if ("responseRate".equals(columnName)) {
			result = bpmTran.getResponseRate();
		} else if ("successRate".equals(columnName)) {
			result = bpmTran.getSuccessRate();
		} else if ("money".equals(columnName)) {
			result = bpmTran.getMoney();
		} else if ("businessSuccessRate".equals(columnName)) {
			result = bpmTran.getBusinessSuccessRate();
		} else {
			throw new RuntimeException("传入的指标参数有误！");
		}
		return result;
	}

	@Override
	public Page<BpmTran> selectBpmTablePage(Page<BpmTran> page, ReportTableParam param) {
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();
		Integer interval = param.getInterval();
		Map<String, Object> paramMap = new HashMap<>();
		// 排序
		JSONObject orderBy = param.getOrder();
		boolean isTimeDimension = param.getIsTimeDimension() == 1 ? true : false;
		boolean isCompareSort = isCompareSort(intervalType, paramMap, orderBy, isTimeDimension);
		if (isTimeDimension) {
			if (intervalType == 1) {
				paramMap.put("groupColumns", " date ");
			} else {
				paramMap.put("groupColumns", " time ");
			}
		}
		// get indicator columns
		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if (intervalType == 1) {
			dInterval = 5 * 60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		} else {
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval);
		}

		IndicatorSupportUtil.dealIndicatorTable(1, paramMap, indicatorParam.getType());
		paramMap.put("ids", param.getUuids());

		List<Map<String, Object>> timePartParam = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timePartParam, dInterval);

		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		paramMap.put("timeStr", " and (" + timeStr + ")");
		paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));

		List<JSONObject> filterIndicator = param.getFilterIndicator();
		if (!evUtil.listIsNullOrZero(filterIndicator)) {
			paramMap.put("indicatorStr",
					" (" + IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterIndicator) + ") ");
		}

		// deal with indicator str
		List<Integer> types = new ArrayList<>();
		types.add(1);
		List<String> columnNames = commonService.selectIndicator(types);
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), paramMap, "indicatorColumnStr",
				columnNames);
		boolean isCompare = (param.getCompare() == 1 || param.getCompare() == 2) ? true : false;
		List<BpmTran> data;
		int total;
		if (isCompareSort) {
			// all data
			data = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
			total = data.size();
		} else {
			data = bpmTranMapper.selectBpmTranByUuidAndGroups(page, paramMap);
			IndicatorSupportUtil.handleIndicatorStrParamByTypeNoPrefix(indicatorParam.getType(), paramMap,
					"indicatorColumnStrNoPrefix", columnNames);
			total = bpmTranMapper.selectBpmTranCountByUuidAndGroups(paramMap);
		}
		List<BpmTran> compareData = new ArrayList<>();
		Map<Integer, String> nameMap = param.getNameMap();
		if (isCompare) {
			List<Map<String, Object>> compareTimePartParam = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(compareTimePartParam, dInterval);

			long compareInterval = IndicatorSupportUtil.getCompareInterval(timePart, compareTimePart);
			String compareTimeStr;
			if (isCompareSort || !isTimeDimension || intervalType == 1) {
				compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
				paramMap.put("timeStr", " and (" + compareTimeStr + ")");
				paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
				// 查询全部
				compareData = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
			} else {
				if (!evUtil.listIsNullOrZero(data)) {
					LongSummaryStatistics summaryStatistics = data.stream().mapToLong(x -> x.getTime())
							.summaryStatistics();
					long start = summaryStatistics.getMin() + compareInterval;
					long end = summaryStatistics.getMax() + compareInterval + interval * 1000;
					compareTimeStr = IndicatorSupportUtil.generateTimeStrByStartAndEnd(start, end);
					paramMap.put("timeStr", " and (" + compareTimeStr + ")");
					paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
					compareData = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
				}
			}
			assembleBpmTranData(intervalType, isTimeDimension, data, compareData, nameMap, compareInterval);
			// 对比字段排序
			if (isCompareSort) {
				// 根据相应维度排序
				SortBpmTran(orderBy.getInteger("sortType"), data, orderBy.getString("ename"));
				List<BpmTran> handCompareData = new ArrayList<>();
				Integer pageNumber = param.getPage();
				Integer pageSize = param.getSize();
				// 从第几条开始
				int offset = (pageNumber - 1) * pageSize;
				int size = 0;
				for (int i = offset; i < data.size(); i++) {
					handCompareData.add(data.get(i));
					size++;
					if (size == pageSize) {
						break;
					}
				}
				data = handCompareData;
			}
		} else {
			// data.forEach(x->x.setName(nameMap.get(x.getUuid())));
			handCommonBpmTranData(intervalType, isTimeDimension, data, nameMap);
		}
		page.setTotal(total);
		page.setRecords(data);
		return page;
	}

	/**
	 * 组装对比数据
	 * 
	 * @param intervalType
	 * @param isTimeDimension
	 * @param data
	 * @param compareData
	 * @param nameMap
	 * @param compareInterval
	 */
	private void assembleBpmTranData(Integer intervalType, boolean isTimeDimension, List<BpmTran> data,
			List<BpmTran> compareData, Map<Integer, String> nameMap, long compareInterval) {
		for (BpmTran bpmTran : data) {
			bpmTran.setName(nameMap.get(bpmTran.getUuid()));
			// 广发逻辑的特殊处理
			if (intervalType == 1 && isTimeDimension) {
				bpmTran.setTime(bpmTran.getDate().getTime());
			}
			if (isTimeDimension) {
				bpmTran.setTimeCP(compareInterval + bpmTran.getTime());
			}
			for (BpmTran compareTran : compareData) {
				if (intervalType == 1 && isTimeDimension) {
					compareTran.setTime(compareTran.getDate().getTime());
				}
				if (isTimeDimension) {
					if (bpmTran.getUuid().equals(compareTran.getUuid())
							&& (compareTran.getTime() - bpmTran.getTime()) == compareInterval) {
						assembleBpmTranCP(bpmTran, compareTran);
						break;
					}
				} else {
					if (bpmTran.getUuid().equals(compareTran.getUuid())) {
						assembleBpmTranCP(bpmTran, compareTran);
						break;
					}
				}
			}
		}
	}

	/**
	 * bpmTran排序
	 * 
	 * @param sortType
	 * @param data
	 * @param orderColumnName
	 */
	private void SortBpmTran(int sortType, List<BpmTran> data, String orderColumnName) {
		Collections.sort(data, (x, y) -> {
			Double value, valueCp;
			try {
				Object o2 = PropertyUtils.describe(y).get(orderColumnName);
				Object o1 = PropertyUtils.describe(x).get(orderColumnName);
				value = o1 == null ? -1 : Math.abs(Double.parseDouble(String.valueOf(o1)));
				valueCp = o2 == null ? -1 : Math.abs(Double.parseDouble(String.valueOf(o2)));
				if (sortType == 0) {
					return value.compareTo(valueCp);
				} else {
					return 0 - value.compareTo(valueCp);
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			return -1;
		});
	}

	@Override
	public Page<BpmExtendTran> selectExtendBpmTablePage(Page<BpmExtendTran> page, ReportTableParam param) {
		Integer interval = param.getInterval();
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();
		Map<String, Object> paramMap = new HashMap<>();
		// 排序
		JSONObject orderBy = param.getOrder();
		boolean isTimeDimension = param.getIsTimeDimension() == 1 ? true : false;
		boolean isCompareSort = isCompareSort(intervalType, paramMap, orderBy, isTimeDimension);

		// 是否勾选时间维度 是否时间维度 0 否 1是
		if (isTimeDimension) {
			if (intervalType == 1) {
				paramMap.put("groupColumns", " date ");
			} else {
				paramMap.put("groupColumns", " time ");
			}
		}

		// get indicator columns
		// IndicatorIntervalParam indicatorParam =
		// IndicatorSupportUtil.getIndicatorIntervalByInterVal(param.getInterval());
		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if (intervalType == 1) {
			dInterval = 5 * 60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		} else {
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		}

		IndicatorSupportUtil.dealIndicatorTable(2, paramMap, indicatorParam.getType());
		paramMap.put("ids", param.getUuids());

		List<Map<String, Object>> timePartParam = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timePartParam, dInterval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		paramMap.put("timeStr", " and (" + timeStr + ")");
		paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		List<JSONObject> filterIndicator = param.getFilterIndicator();
		if (!evUtil.listIsNullOrZero(filterIndicator)) {
			paramMap.put("indicatorStr",
					" (" + IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterIndicator) + ") ");
		}

		// deal with indicator str
		List<Integer> types = new ArrayList<>();
		types.add(1);
		types.add(2);
		List<String> columnNames = commonService.selectIndicator(types);
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), paramMap, "indicatorColumnStr",
				columnNames);
		boolean isCompare = (param.getCompare() == 1 || param.getCompare() == 2) ? true : false;
		List<BpmExtendTran> data;
		int total;
		if (isCompareSort) {
			// all data
			data = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
			total = data.size();
		} else {
			data = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(page, paramMap);
			IndicatorSupportUtil.handleIndicatorStrParamByTypeNoPrefix(indicatorParam.getType(), paramMap,
					"indicatorColumnStrNoPrefix", columnNames);
			total = bpmTranMapper.selectBpmExtendTranCountByUuidAndGroups(paramMap);
		}
		Map<Integer, String> customMap = param.getNameMap();
		List<BpmExtendTran> compareData = new ArrayList<>();
		if (isCompare) {
			// 对比字段排序
			String compareTimeStr;
			// long compareInterval =
			// IndicatorSupportUtil.getCompareInterval(param.getTimePart(),
			// param.getTimeCompare());

			List<Map<String, Object>> compareTimePartParam = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(compareTimePartParam, dInterval);
			long compareInterval = IndicatorSupportUtil.getCompareInterval(timePart, compareTimePart);
			if (isCompareSort || !isTimeDimension || intervalType == 1) {
				compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
				paramMap.put("timeStr", " and (" + compareTimeStr + ")");
				paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
				// 查询全部
				compareData = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
			} else {
				// 根据原始数据缩小时间范围
				if (!evUtil.listIsNullOrZero(data)) {
					LongSummaryStatistics summaryStatistics = data.stream().mapToLong(x -> x.getTime())
							.summaryStatistics();
					long start = summaryStatistics.getMin() + compareInterval;
					long end = summaryStatistics.getMax() + compareInterval + interval * 1000;
					compareTimeStr = IndicatorSupportUtil.generateTimeStrByStartAndEnd(start, end);
					paramMap.put("timeStr", " and (" + compareTimeStr + ")");
					paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
					compareData = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
				}
			}

			assembleBpmExtendTranData(intervalType, isTimeDimension, data, customMap, compareData, compareInterval);

			// 对比字段排序
			if (isCompareSort) {
				sortBpmExtendTran(orderBy.getIntValue("sortType"), data, orderBy.getString("ename"));
				List<BpmExtendTran> handCompareData = new ArrayList<>();
				Integer pageNumber = param.getPage();
				Integer pageSize = param.getSize();
				// 从第几条开始
				int offset = (pageNumber - 1) * pageSize;
				int size = 0;
				for (int i = offset; i < data.size(); i++) {
					handCompareData.add(data.get(i));
					size++;
					if (size == pageSize) {
						break;
					}
				}
				data = handCompareData;
			}
		} else {
			// data.forEach(x->x.setName(customMap.get(x.getUuid())));
			handCommonBpmExtendTranData(intervalType, isTimeDimension, data, customMap);
		}
		page.setTotal(total);
		page.setRecords(data);
		return page;
	}

	@Override
	public List<BpmTran> selectBpmTable(ReportTableParam param) {
		Integer interval = param.getInterval();
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();
		Map<String, Object> paramMap = new HashMap<>();
		JSONObject orderBy = param.getOrder();
		Integer orderType, sortType;
		String orderColumn;
		boolean isTimeDimension = param.getIsTimeDimension() == 1 ? true : false;
		boolean isCompareSort = isCompareSort(intervalType, paramMap, orderBy, isTimeDimension);
		if (null != orderBy) {
			orderColumn = orderBy.getString("ename");
			// sortType: 0 升序 1 降序 type: 1 原始排序 2 对比排序
			sortType = orderBy.getInteger("sortType");
			orderType = orderBy.getInteger("orderType");
			if (!isTimeDimension && orderType.equals(0)) {
				throw new RuntimeException("未勾选时间维度不能按时间排序！");
			}
			isCompareSort = generateOrderStrToMap(paramMap, orderType, sortType, orderColumn, isCompareSort,
					intervalType);
		} else {
			paramMap.put("orderStr", null);
		}

		if (isTimeDimension) {
			paramMap.put("groupColumns", " time ");
		}
		// 是否勾选时间维度 是否时间维度 0 否 1是
		if (isTimeDimension) {
			if (intervalType == 1) {
				paramMap.put("groupColumns", " date ");
			} else {
				paramMap.put("groupColumns", " time ");
			}
		}

		// get indicator columns
		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if (intervalType == 1) {
			dInterval = 5 * 60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		} else {
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval);
		}

		IndicatorSupportUtil.dealIndicatorTable(1, paramMap, indicatorParam.getType());
		paramMap.put("ids", param.getUuids());
		// 格式化time数组
		List<Map<String, Object>> timeParts = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timeParts, dInterval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		paramMap.put("timeStr", " and (" + timeStr + ")");
		paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		List<JSONObject> filterIndicator = param.getFilterIndicator();
		if (!evUtil.listIsNullOrZero(filterIndicator)) {
			paramMap.put("indicatorStr",
					" (" + IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterIndicator) + ") ");
		}

		// deal with indicator str
		List<Integer> types = new ArrayList<>();
		types.add(1);
		List<String> columnNames = commonService.selectIndicator(types);
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), paramMap, "indicatorColumnStr",
				columnNames);

		boolean isCompare = (param.getCompare() == 1 || param.getCompare() == 2) ? true : false;
		List<BpmTran> data;
		Integer limit = param.getLimit();
		if (isCompareSort) {
			// all data
			logger.info("selectBpmTable():{}", JSON.toJSONString(paramMap));
			data = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
		} else {
			paramMap.put("limit", limit);
			logger.info("selectBpmTable():{}", JSON.toJSONString(paramMap));
			data = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
		}
		List<BpmTran> compareData = new ArrayList<>();
		Map<Integer, String> nameMap = param.getNameMap();
		if (isCompare) {
			String compareTimeStr;
			List<Map<String, Object>> timePartsCompare = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(timePartsCompare, dInterval);
			long compareInterval = IndicatorSupportUtil.getCompareInterval(timePart, compareTimePart);
			if (isCompareSort || !isTimeDimension || intervalType == 1) {
				compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
				paramMap.put("timeStr", " and (" + compareTimeStr + ")");
				paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
				paramMap.remove("limit");
				logger.info("selectBpmTable():{}", JSON.toJSONString(paramMap));
				compareData = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
			} else {
				// 优化，在原始结果集查找开始和结束时间，生成对比的开始和结束时间，缩短查询时长
				if (!evUtil.listIsNullOrZero(data)) {
					LongSummaryStatistics summaryStatistics = data.stream().mapToLong(x -> x.getTime())
							.summaryStatistics();
					long start = summaryStatistics.getMin() + compareInterval;
					long end = summaryStatistics.getMax() + compareInterval + interval * 1000;
					compareTimeStr = IndicatorSupportUtil.generateTimeStrByStartAndEnd(start, end);
					paramMap.put("timeStr", " and (" + compareTimeStr + ")");
					paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
					paramMap.remove("limit");
					logger.info("selectBpmTable():{}", JSON.toJSONString(paramMap));
					compareData = bpmTranMapper.selectBpmTranByUuidAndGroups(paramMap);
				}
			}

			assembleBpmTranData(intervalType, isTimeDimension, data, compareData, nameMap, compareInterval);

			// 对比字段排序
			if (isCompareSort) {
				// 根据相应维度排序
				SortBpmTran(orderBy.getInteger("sortType"), data, orderBy.getString("ename"));
				data = getBpmTrans(data, limit);
			}
		} else {
			handCommonBpmTranData(intervalType, isTimeDimension, data, nameMap);
		}
		return data;
	}

	@Override
	public List<BpmExtendTran> selectExtendBpmTable(ReportTableParam param) {
		Integer interval = param.getInterval();
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();
		Map<String, Object> paramMap = new HashMap<>();
		JSONObject orderBy = param.getOrder();
		boolean isTimeDimension = param.getIsTimeDimension() == 1 ? true : false;
		boolean isCompareSort = isCompareSort(intervalType, paramMap, orderBy, isTimeDimension);
		if (isTimeDimension) {
			if (intervalType == 1) {
				paramMap.put("groupColumns", " date ");
			} else {
				paramMap.put("groupColumns", " time ");
			}
		}

		// get indicator columns
		int dInterval;
		IndicatorIntervalParam indicatorParam;
		if (intervalType == 1) {
			dInterval = 5 * 60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		} else {
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
		}

		IndicatorSupportUtil.dealIndicatorTable(2, paramMap, indicatorParam.getType());
		paramMap.put("ids", param.getUuids());
		List<Map<String, Object>> timePartParam = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timePartParam, dInterval);

		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		paramMap.put("timeStr", " and (" + timeStr + ")");
		paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		List<JSONObject> filterIndicator = param.getFilterIndicator();
		if (!evUtil.listIsNullOrZero(filterIndicator)) {
			paramMap.put("indicatorStr",
					" (" + IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterIndicator) + ") ");
		}

		// deal with indicator str
		List<Integer> types = new ArrayList<>();
		types.add(1);
		types.add(2);
		List<String> columnNames = commonService.selectIndicator(types);
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), paramMap, "indicatorColumnStr",
				columnNames);
		Integer limit = param.getLimit();
		boolean isCompare = (param.getCompare() == 1 || param.getCompare() == 2) ? true : false;
		List<BpmExtendTran> data;
		if (isCompareSort) {
			// all data
			logger.info("selectExtendBpmTable():{}", JSON.toJSONString(paramMap));
			data = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
		} else {
			paramMap.put("limit", limit);
			logger.info("selectExtendBpmTable():{}", JSON.toJSONString(paramMap));
			data = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
		}
		Map<Integer, String> customMap = param.getNameMap();
		List<BpmExtendTran> compareData = new ArrayList<>();
		if (isCompare) {

			List<Map<String, Object>> compareTimePartParam = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(compareTimePartParam, dInterval);
			long compareInterval = IndicatorSupportUtil.getCompareInterval(timePart, compareTimePart);
			String compareTimeStr;
			if (isCompareSort || !isTimeDimension || intervalType == 1) {
				compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
				paramMap.put("timeStr", " and (" + compareTimeStr + ")");
				paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
				paramMap.remove("limit");
				logger.info("selectExtendBpmTable():{}", JSON.toJSONString(paramMap));
				compareData = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
			} else {
				if (!evUtil.listIsNullOrZero(data)) {
					LongSummaryStatistics summaryStatistics = data.stream().mapToLong(x -> x.getTime())
							.summaryStatistics();
					long start = summaryStatistics.getMin() + compareInterval;
					long end = summaryStatistics.getMax() + compareInterval + interval * 1000;
					compareTimeStr = IndicatorSupportUtil.generateTimeStrByStartAndEnd(start, end);
					paramMap.put("timeStr", " and (" + compareTimeStr + ")");
					paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
					paramMap.remove("limit");
					logger.info("selectExtendBpmTable():{}", JSON.toJSONString(paramMap));
					compareData = bpmTranMapper.selectBpmExtendTranByUuidAndGroups(paramMap);
				}
			}

			assembleBpmExtendTranData(intervalType, isTimeDimension, data, customMap, compareData, compareInterval);

			// 对比字段排序
			if (isCompareSort) {
				sortBpmExtendTran(orderBy.getIntValue("sortType"), data, orderBy.getString("ename"));
				List<BpmExtendTran> handCompareData = new ArrayList<>();
				if (null != limit) {
					if (data.size() < limit) {
						limit = data.size();
					}
					for (int i = 0; i < limit; i++) {
						handCompareData.add(data.get(i));
					}
					data = handCompareData;
				}
			}
		} else {
			handCommonBpmExtendTranData(intervalType, isTimeDimension, data, customMap);
		}
		return data;
	}

	private void handCommonBpmExtendTranData(Integer intervalType, boolean isTimeDimension, List<BpmExtendTran> data,
			Map<Integer, String> customMap) {
		if (intervalType == 1 && isTimeDimension) {
			data.forEach(x -> {
				x.setName(customMap.get(x.getUuid()));
				x.setTime(x.getDate().getTime());
			});
		} else {
			data.forEach(x -> x.setName(customMap.get(x.getUuid())));
		}
	}

	private void handCommonBpmTranData(Integer intervalType, boolean isTimeDimension, List<BpmTran> data,
			Map<Integer, String> nameMap) {
		if (intervalType == 1 && isTimeDimension) {
			data.forEach(x -> {
				x.setName(nameMap.get(x.getUuid()));
				x.setTime(x.getDate().getTime());
			});
		} else {
			data.forEach(x -> x.setName(nameMap.get(x.getUuid())));
		}
	}

	/**
	 * 分页数据的组装
	 * 
	 * @param data
	 * @param limit
	 * @return
	 */
	private List<BpmTran> getBpmTrans(List<BpmTran> data, Integer limit) {
		List<BpmTran> handCompareData = new ArrayList<>();
		if (null != limit) {
			if (data.size() < limit) {
				limit = data.size();
			}
			for (int i = 0; i < limit; i++) {
				handCompareData.add(data.get(i));
			}
			data = handCompareData;
		}
		return data;
	}

	private boolean isCompareSort(Integer intervalType, Map<String, Object> paramMap, JSONObject orderBy,
			boolean isTimeDimension) {
		boolean isCompareSort = false;
		Integer sortType;
		String orderColumn;
		Integer orderType;
		if (null != orderBy) {
			// sortType: 0 升序 1 降序 type: 1 原始排序 2 对比排序
			sortType = orderBy.getInteger("sortType");
			orderColumn = orderBy.getString("ename");
			orderType = orderBy.getInteger("orderType");
			if (!isTimeDimension && orderType.equals(0)) {
				throw new RuntimeException("未勾选时间维度不能按时间排序！");
			}
			isCompareSort = generateOrderStrToMap(paramMap, orderType, sortType, orderColumn, isCompareSort,
					intervalType);
		} else {
			paramMap.put("orderStr", null);
		}
		return isCompareSort;
	}

	private void assembleBpmExtendTranData(Integer intervalType, boolean isTimeDimension, List<BpmExtendTran> data,
			Map<Integer, String> customMap, List<BpmExtendTran> compareData, long compareInterval) {
		for (BpmExtendTran extendTran : data) {
			if (intervalType == 1 && isTimeDimension) {
				extendTran.setTime(extendTran.getDate().getTime());
			}
			extendTran.setName(customMap.get(extendTran.getUuid()));
			if (isTimeDimension) {
				extendTran.setTimeCP(compareInterval + extendTran.getTime());
			}
			for (BpmExtendTran compareExtendTran : compareData) {
				if (intervalType == 1 && isTimeDimension) {
					compareExtendTran.setTime(compareExtendTran.getDate().getTime());
				}
				if (isTimeDimension) {
					if (extendTran.getUuid().equals(compareExtendTran.getUuid())
							&& (compareExtendTran.getTime() - extendTran.getTime()) == compareInterval) {
						assembleBpmExtendTranCP(extendTran, compareExtendTran);
						break;
					}
				} else {
					if (extendTran.getUuid().equals(compareExtendTran.getUuid())) {
						assembleBpmExtendTranCP(extendTran, compareExtendTran);
						break;
					}
				}
			}
		}
	}

	private boolean generateOrderStrToMap(Map<String, Object> paramMap, Integer orderType, Integer sortType,
			String orderColumn, boolean isCompareSort, int interval) {
		String orderStr;
		// 排序字段类型 0 时间戳 1 指标 2 维度 3 对比指标 4对比百分比 5 对比差值
		if (orderType >= 0 && orderType <= 2) {
			// 原始字段排序(原始分页,对比查询全部)
			String asc = sortType == 0 ? " ASC " : " DESC ";
			// 广发天级数据特殊逻辑处理
			if (orderType == 0 && interval == 1) {
				orderColumn = " date ";
			}
			orderStr = orderColumn + asc;
			paramMap.put("orderStr", orderStr);
		} else {
			// 对比字段排序(原始和对比查全部)
			isCompareSort = true;
		}
		return isCompareSort;
	}

	private void sortBpmExtendTran(int sortType, List<BpmExtendTran> data, String orderColumnName) {
		Collections.sort(data, (x, y) -> {
			Double value;
			Double valueCp;
			try {
				Object o2 = PropertyUtils.describe(y).get(orderColumnName);
				Object o1 = PropertyUtils.describe(x).get(orderColumnName);
				value = o1 == null ? -1 : Math.abs(Double.parseDouble(String.valueOf(o1)));
				valueCp = o2 == null ? -1 : Math.abs(Double.parseDouble(String.valueOf(o2)));
				if (sortType == 0) {
					return value.compareTo(valueCp);
				} else {
					return 0 - value.compareTo(valueCp);
				}
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			}
			return -1;
		});
	}

	/**
	 * set BpmTran CP
	 * 
	 * @param bpmTran
	 * @param compareTran
	 */
	public void assembleBpmTranCP(BpmTran bpmTran, BpmTran compareTran) {
		bpmTran.setResponseRateCP(compareTran.getResponseRate());
		bpmTran.setResponseRateDIFF(compareTran.getResponseRate() == null || bpmTran.getResponseRate() == null ? null
				: new Double(df.format(bpmTran.getResponseRate() - compareTran.getResponseRate()).toString()));
		bpmTran.setResponseRatePER(compareTran.getResponseRate() == null || bpmTran.getResponseRate() == null
				|| compareTran.getResponseRate().equals(0.0) ? null
						: new Double(df.format((bpmTran.getResponseRate() - compareTran.getResponseRate())
								/ compareTran.getResponseRate() * 100).toString()));

		bpmTran.setResponseTimeCP(compareTran.getResponseTime());
		bpmTran.setResponseTimeDIFF(compareTran.getResponseTime() == null || bpmTran.getResponseTime() == null ? null
				: new Double(df.format(bpmTran.getResponseTime() - compareTran.getResponseTime()).toString()));
		bpmTran.setResponseTimePER(compareTran.getResponseTime() == null || bpmTran.getResponseTime() == null
				|| compareTran.getResponseTime().equals(0.0) ? null
						: new Double(df.format((bpmTran.getResponseTime() - compareTran.getResponseTime())
								/ compareTran.getResponseTime() * 100).toString()));

		bpmTran.setSuccessRateCP(compareTran.getSuccessRate());
		bpmTran.setSuccessRateDIFF(compareTran.getSuccessRate() == null || bpmTran.getSuccessRate() == null ? null
				: new Double(df.format(bpmTran.getSuccessRate() - compareTran.getSuccessRate()).toString()));
		bpmTran.setSuccessRatePER(compareTran.getSuccessRate() == null || bpmTran.getSuccessRate() == null
				|| compareTran.getSuccessRate().equals(0.0) ? null
						: new Double(df.format((bpmTran.getSuccessRate() - compareTran.getSuccessRate())
								/ compareTran.getSuccessRate() * 100).toString()));

		bpmTran.setAllTransCountCP(compareTran.getAllTransCount());
		bpmTran.setAllTransCountDIFF(compareTran.getAllTransCount() == null ? null
				: new Double(df.format(bpmTran.getAllTransCount() - compareTran.getAllTransCount()).toString()));
		bpmTran.setAllTransCountPER(compareTran.getAllTransCount() == null || compareTran.getAllTransCount().equals(0)
				? null
				: new Double(
						df.format((bpmTran.getAllTransCount().longValue() - compareTran.getAllTransCount().longValue())
								/ (compareTran.getAllTransCount().longValue() * 1.0) * 100).toString()));

		// bpmTran.setResponseTransCountCP(compareTran.getResponseTransCount());
		// bpmTran.setResponseTransCountDIFF(compareTran.getResponseTransCount() == null
		// || bpmTran.getResponseTransCount() == null ? null :
		// compareTran.getResponseTransCount() - bpmTran.getResponseTransCount());
		// bpmTran.setResponseTransCountPER(compareTran.getResponseTransCount() == null
		// || bpmTran.getResponseTransCount() == null ? null :
		// (compareTran.getResponseTransCount() -
		// bpmTran.getResponseTransCount())/bpmTran.getResponseTransCount());

		// bpmTran.setAllStart2endsCP(compareTran.getAllStart2ends());
		// bpmTran.setSuccessRateDIFF(compareTran.getSuccessRate() == null ||
		// bpmTran.getSuccessRate() == null ? null : compareTran.getSuccessRate() -
		// bpmTran.getSuccessRate());
		// bpmTran.setSuccessRatePER(compareTran.getSuccessRate() == null ||
		// bpmTran.getSuccessRate() == null ? null : (compareTran.getSuccessRate() -
		// bpmTran.getSuccessRate())/bpmTran.getSuccessRate());

	}

	/**
	 * set BpmExtendTran CP
	 * 
	 * @param extendTran
	 * @param compareExtendTran
	 */
	public void assembleBpmExtendTranCP(BpmExtendTran extendTran, BpmExtendTran compareExtendTran) {
		extendTran.setResponseRateCP(compareExtendTran.getResponseRate());
		extendTran.setResponseRateDIFF(
				compareExtendTran.getResponseRate() == null || extendTran.getResponseRate() == null ? null
						: new Double(df.format(extendTran.getResponseRate() - compareExtendTran.getResponseRate())
								.toString()));
		extendTran.setResponseRatePER(compareExtendTran.getResponseRate() == null
				|| extendTran.getResponseRate() == null || compareExtendTran.getResponseRate().equals(0.0) ? null
						: new Double(df.format((extendTran.getResponseRate() - compareExtendTran.getResponseRate())
								/ compareExtendTran.getResponseRate() * 100).toString()));

		extendTran.setResponseTimeCP(compareExtendTran.getResponseTime());
		extendTran.setResponseTimeDIFF(
				compareExtendTran.getResponseTime() == null || extendTran.getResponseTime() == null ? null
						: new Double(df.format(extendTran.getResponseTime() - compareExtendTran.getResponseTime())
								.toString()));
		extendTran.setResponseTimePER(compareExtendTran.getResponseTime() == null
				|| extendTran.getResponseTime() == null || compareExtendTran.getResponseTime().equals(0.0) ? null
						: new Double(df.format((extendTran.getResponseTime() - compareExtendTran.getResponseTime())
								/ compareExtendTran.getResponseTime() * 100).toString()));

		extendTran.setSuccessRateCP(compareExtendTran.getSuccessRate());
		extendTran.setSuccessRateDIFF(compareExtendTran.getSuccessRate() == null || extendTran.getSuccessRate() == null
				? null
				: new Double(df.format(extendTran.getSuccessRate() - compareExtendTran.getSuccessRate()).toString()));
		extendTran.setSuccessRatePER(compareExtendTran.getSuccessRate() == null || extendTran.getSuccessRate() == null
				|| compareExtendTran.getSuccessRate().equals(0.0) ? null
						: new Double(df.format((extendTran.getSuccessRate() - compareExtendTran.getSuccessRate())
								/ compareExtendTran.getSuccessRate() * 100).toString()));

		extendTran.setAllTransCountCP(compareExtendTran.getAllTransCount());
		extendTran.setAllTransCountDIFF(
				compareExtendTran.getAllTransCount() == null || extendTran.getAllTransCount() == null ? null
						: extendTran.getAllTransCount() - compareExtendTran.getAllTransCount());
		extendTran.setAllTransCountPER(compareExtendTran.getAllTransCount() == null
				|| extendTran.getAllTransCount() == null || compareExtendTran.getAllTransCount().equals(0) ? null
						: new Double(df.format((extendTran.getAllTransCount() - compareExtendTran.getAllTransCount())
								/ (compareExtendTran.getAllTransCount() * 1.0) * 100).toString()));

		extendTran.setMoneyCP(compareExtendTran.getMoney());
		extendTran.setMoneyDIFF(compareExtendTran.getMoney() == null || extendTran.getMoney() == null ? null
				: new Double(df.format(compareExtendTran.getMoney() - extendTran.getMoney()).toString()));
		extendTran.setMoneyPER(compareExtendTran.getMoney() == null || extendTran.getMoney() == null
				|| compareExtendTran.getMoney().equals(0.0)
						? null
						: new Double(df.format(
								(compareExtendTran.getMoney() - extendTran.getMoney()) / compareExtendTran.getMoney())
								.toString()));

		extendTran.setBusinessSuccessRateCP(compareExtendTran.getBusinessSuccessRate());
		extendTran.setBusinessSuccessRateDIFF(
				compareExtendTran.getBusinessSuccessRate() == null || extendTran.getBusinessSuccessRate() == null ? null
						: new Double(df.format(
								compareExtendTran.getBusinessSuccessRate() - extendTran.getBusinessSuccessRate())
								.toString()));
		extendTran.setBusinessSuccessRatePER(compareExtendTran.getBusinessSuccessRate() == null
				|| extendTran.getBusinessSuccessRate() == null || compareExtendTran.getBusinessSuccessRate().equals(0.0)
						? null
						: new Double(df.format(
								(compareExtendTran.getBusinessSuccessRate() - extendTran.getBusinessSuccessRate())
										/ compareExtendTran.getBusinessSuccessRate())
								.toString()));

		// extendTran.setResponseTransCountCP(compareExtendTran.getResponseTransCount());
		// extendTran.setSuccessTransCountCP(compareExtendTran.getSuccessTransCount());
		// extendTran.setAllStart2endsCP(compareExtendTran.getAllStart2ends());

	}

	@Override
	public List<Map<String, Object>> getPieChartData(ReportChartParam re) {
		// 图标类型 1 折线 ，2 柱状 ，3 ，饼图 ，4 ，表格，5 柱状top
		Integer chartType = re.getType();
		Integer limit = re.getLimit();
		Integer sort = re.getSortType() == null ? 0 : re.getSortType();
		Integer interval = re.getInterval();
		Integer intervalType = re.getIntervalType() == null ? 0 : re.getIntervalType();
		if (intervalType == 1) {
			interval = 5 * 60;
		}
		// 数据类型 1 应用 2 事件 3 漏斗
		Integer dataType = re.getDataType();
		Map<String, Object> map = new HashMap<>(14);
		// 获取查询时间
		String timeStr = "";
		List<Map<String, Object>> timePart = re.getTimePart();
		if (!evUtil.listIsNullOrZero(timePart)) {
			timePart = DateUtils.generateTimeStr(timePart, interval);
			timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
			map.put("timeStr", "AND (" + timeStr + ")");
			map.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		} else {
			return null;
		}
		// 指标筛选
		List<JSONObject> filterIndicator = re.getFilterIndicator();
		if (!evUtil.listIsNullOrZero(filterIndicator)) {
			map.put("indicatorStr",
					" (" + IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterIndicator) + ") ");
		}
		// uuid
		List<Integer> uuids = new ArrayList<>();
		List<Map<String, Object>> uuid = re.getUuids();
		// 数据类型 1 应用 ，2 监控点 ，3 组件 ，4IP ，5 端口 ，6事件
		for (Map<String, Object> map2 : uuid) {
			uuids.add((int) map2.get("id"));
		}

		// 指标名(单指标)
		List<com.greattimes.ev.bpm.entity.Indicator> indicatorList = commonService
				.findIndicatorByIds(re.getIndicators());
		if (evUtil.listIsNullOrZero(indicatorList)) {
			return null;
		}
		String indicator = indicatorList.get(0).getColumnName();
		map.put("indicator", indicator);
		// 如果是top 加入limit 和sort
		if (null != chartType) {
			if (chartType.equals(5)) {
				// map.put("limit", limit);
				if (sort.equals(0)) {
					map.put("sort", "ASC");
				} else {
					map.put("sort", "DESC");
				}
			}
		}
		int granularity = 0;
		if (intervalType == 0) {
			granularity = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval).getType();
		} else if (intervalType == 1) {
			granularity = 2;
		}

		List<Map<String, Object>> list = new ArrayList<>();
		// 事件
		if (dataType.equals(3)) {
			// uuids.addAll(customId);
			map.put("ids", uuids);
			map.put("customType", 2);
			IndicatorSupportUtil.dealIndicatorTable(2, map, granularity);
			List<Integer> type = new ArrayList<>();
			type.add(1);
			type.add(2);
			List<String> indicators = commonService.selectIndicator(type);
			IndicatorSupportUtil.generateIndicatorColumnStr(map, indicators, evUtil.getMapIntValue(map, "type"));
			logger.info("事件饼图查询条件{}", map);
			list = bpmTranMapper.getPieChartData(map);
		} else if (dataType.equals(1)) {
			/*
			 * uuids.addAll(applicationIds); uuids.addAll(monitor);
			 * uuids.addAll(componentId); uuids.addAll(ip); uuids.addAll(port);
			 */
			map.put("ids", uuids);
			IndicatorSupportUtil.dealIndicatorTable(1, map, granularity);
			List<Integer> type = new ArrayList<>();
			type.add(1);
			List<String> indicators = commonService.selectIndicator(type);
			IndicatorSupportUtil.generateIndicatorColumnStr(map, indicators, evUtil.getMapIntValue(map, "type"));
			map.put("customType", 1);
			logger.info("应用饼图查询条件{}", map);
			list = bpmTranMapper.getPieChartData(map);
		}
		// 根据uuid补数据
		if (!evUtil.listIsNullOrZero(uuids)) {
			// 無序圖逻辑
			if (sort.equals(2)) {
				List<Map<String, Object>> newList = new ArrayList<>();
				boolean flag = false;
				for (Integer id : uuids) {
					flag = false;
					for (Map<String, Object> map2 : list) {
						if (map2.get("uuid").equals(id)) {
							flag = true;
							newList.add(map2);
							break;
						}
					}
					if (!flag) {
						Map<String, Object> e = new HashMap<>();
						e.put("uuid", id);
						e.put(indicator, null);
						newList.add(e);
					}

				}
				list.clear();
				list.addAll(newList);
			} else {
				boolean flag = false;
				for (Integer id : uuids) {
					flag = false;
					for (Map<String, Object> map2 : list) {
						if (map2.get("uuid").equals(id)) {
							flag = true;
							break;
						}
					}
					if (!flag) {
						Map<String, Object> e = new HashMap<>();
						e.put("uuid", id);
						e.put(indicator, null);
						// 若是升序
						if (sort.equals(0)) {
							list.add(0, e);
						} else {
							list.add(e);
						}
					}

				}
			}
		}
		// top图limit
		if (null != limit) {
			if (chartType.equals(5) && list.size() > limit) {
				// 取对应分页数据
				List<Map<String, Object>> subData = new ArrayList<>();
				for (int i = 0; i < limit; i++) {
					subData.add(list.get(i));
				}
				list.clear();
				list.addAll(subData);
				subData.clear();
			}
		}
		// compare data
		List<Map<String, Object>> compareData = new ArrayList<>();
		List<Map<String, Object>> timeComPare = re.getTimeCompare();
		if (!evUtil.listIsNullOrZero(timeComPare)) {
			timeComPare = DateUtils.generateTimeStr(timeComPare, interval);
			String timeCompareStr = IndicatorSupportUtil.generateTimeStr(timeComPare);
			map.put("timeStr", "AND (" + timeCompareStr + ")");
			map.put("dateStr", DateUtils.getDateStrbyTimePart(timeComPare));
			map.remove("indicatorStr");
			map.remove("limit");
			logger.info("智能报表top/饼状图对比查询请求参数{}", map);
			compareData = bpmTranMapper.getPieChartData(map);
		}
		// 生成返回结果
		List<Map<String, Object>> result = new ArrayList<>();
		for (Map<String, Object> map2 : list) {
			Map<String, Object> resultMap = new HashMap<>();
			if (!evUtil.listIsNullOrZero(timeComPare)) {
				boolean flag = false;
				for (Map<String, Object> map3 : compareData) {
					if (map2.get("uuid").equals(map3.get("uuid"))) {
						resultMap.put("valueCP", map3.get(indicator).toString());
						flag = true;
						break;
					}
				}
				if (!flag) {
					resultMap.put("valueCP", null);
				}
			}
			resultMap.put("id", map2.get("uuid"));
			// 加上dataType
			for (Map<String, Object> uuidMap : uuid) {
				if (uuidMap.get("id").toString().equals(resultMap.get("id").toString())) {
					resultMap.put("dataType", uuidMap.get("dataType"));
					break;
				}
			}
			if (null != map2.get(indicator)) {
				resultMap.put("value", map2.get(indicator).toString());
			} else {
				resultMap.put("value", map2.get(indicator));
			}
			result.add(resultMap);
		}
		return result;
	}

	@Override
	public ReportDataChartParam getDataChart(ReportChartParam re) {
		ReportDataChartParam chart = new ReportDataChartParam();
		IndicatorParam param = new IndicatorParam();
		List<Integer> uuid = new ArrayList<>();
		List<Long> timeList = new ArrayList<>();
		List<String> valueList = new ArrayList<>();
		String sumCompare = null;
		String sum = null;
		uuid.add((Integer) re.getUuids().get(0).get("id"));
		param.setUuid(uuid);
		param.setIndicator(re.getIndicators());
		List<Map<String, Object>> time = re.getTimePart();
		Integer interval = re.getInterval();
		Integer intervalType = re.getIntervalType() == null ? 0 : re.getIntervalType();
		for (Map<String, Object> map : time) {
			List<Long> timeP = (List<Long>) map.get("time");
			param.setStart(timeP.get(0));
			param.setEnd(timeP.get(1));
			ReportDataChartParam result = new ReportDataChartParam();
			if (!re.getUuids().get(0).get("dataType").equals(6)) {
				result = chart(param, interval, intervalType, 1);
			} else {
				result = chart(param, interval, intervalType, 2);
			}
			timeList.addAll(result.getTime());
			valueList.addAll(result.getValue());
		}
		// 计算当前值
		List<Map<String, Object>> list = getPieChartData(re);
		if (!evUtil.listIsNullOrZero(list)) {
			if (null != list.get(0).get("value")) {
				sum = list.get(0).get("value").toString();
			}
			List<Map<String, Object>> timeCompare = re.getTimeCompare();
			if (!evUtil.listIsNullOrZero(timeCompare)) {
				if (null != list.get(0).get("valueCP")) {
					sumCompare = list.get(0).get("valueCP").toString();
				}
			}
		}
		chart.setSum(sum);
		chart.setSumCompare(sumCompare);
		chart.setTime(timeList);
		chart.setValue(valueList);
		return chart;
	}

	/**
	 * @param param
	 * @param interval
	 * @param type
	 *            1 应用 2 事件
	 * @return
	 */
	private ReportDataChartParam chart(IndicatorParam param, Integer interval, Integer intervalType, Integer type) {
		ReportDataChartParam result = new ReportDataChartParam();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Long> timeList = new ArrayList<>();
		// 证券特殊逻辑
		if (intervalType == 1) {
			interval = 5 * 60;
		}
		IndicatorIntervalParam indicatorIntervalParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end,
				interval);
		if (intervalType == 1) {
			timeList.add(DateUtils.longToDayTime(start));
		} else {
			timeList = indicatorIntervalParam.getTimeList();
		}
		result.setTime(timeList);
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		start = indicatorIntervalParam.getStart();
		end = indicatorIntervalParam.getEnd();
		Integer granularity = indicatorIntervalParam.getType();
		Map<String, Object> map = new HashMap<>();
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		IndicatorSupportUtil.dealIndicatorTable(type, map, granularity);
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		if (intervalType == 1) {
			map.put("isSecurities", 1);
		}
		if (type.equals(1)) {
			logger.info("应用数值图查询参数:{}", map);
			List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(map);
			if (intervalType == 1) {
				if (!evUtil.listIsNullOrZero(bpmTranList)) {
					for (BpmTran bpmtran : bpmTranList) {
						Date date = bpmtran.getDate();
						bpmtran.setTime(DateUtils.getDateLong(date));
					}
				}
			}
			bpmTranList = initDefault(bpmTranList, timeList);
			// assembly data
			List<String> invalue = new ArrayList<>();
			for (Integer integer : indicator) {
				// 置为map
				List<Map<String, Object>> mapbpm = new ArrayList<>();
				bpmTranList.stream().forEach(x -> {
					// 添加指标需修改
					Map<String, Object> dataMap = new HashMap<>();
					dataMap.put("allTransCount", x.getAllTransCount());
					dataMap.put("responseTime", x.getResponseTime());
					dataMap.put("responseRate", x.getResponseRate());
					dataMap.put("successRate", x.getSuccessRate());
					mapbpm.add(dataMap);
				});
				// 获取指标名
				String colname = colnameList.get(integer);
				// 根据指标名取值
				mapbpm.stream().forEach(x -> {
					String str = null;
					if (null != x.get(colname)) {
						str = x.get(colname).toString();
					}
					invalue.add(str);
				});
			}
			result.setValue(invalue);
		} else {
			logger.info("事件数值图查询参数:{}", map);
			List<BpmExtendTran> bpmTranList = bpmTranMapper.selectBpmExtendTranByUuid(map);
			if (intervalType == 1) {
				if (!evUtil.listIsNullOrZero(bpmTranList)) {
					for (BpmExtendTran bpmtran : bpmTranList) {
						Date date = bpmtran.getDate();
						bpmtran.setTime(DateUtils.getDateLong(date));
					}
				}
			}
			bpmTranList = initExtendDefault(bpmTranList, timeList);
			// assembly data
			List<String> invalue = new ArrayList<>();
			for (Integer integer : indicator) {
				// 置为map
				List<Map<String, Object>> mapbpm = new ArrayList<>();
				bpmTranList.stream().forEach(x -> {
					// 添加指标需修改
					Map<String, Object> dataMap = new HashMap<>();
					dataMap.put("allTransCount", x.getAllTransCount());
					dataMap.put("money", x.getMoney());
					dataMap.put("responseTime", x.getResponseTime());
					dataMap.put("responseRate", x.getResponseRate());
					dataMap.put("successRate", x.getSuccessRate());
					dataMap.put("businessSuccessRate", x.getBusinessSuccessRate());
					mapbpm.add(dataMap);
				});
				// 获取指标名
				String colname = colnameList.get(integer);
				// 根据指标名取值
				mapbpm.stream().forEach(x -> {
					String str = null;
					if (null != x.get(colname)) {
						str = x.get(colname).toString();
					}
					invalue.add(str);
				});
			}
			result.setValue(invalue);
		}
		return result;
	}

	@Override
	public ExtendIndicatorDataParam extendIndicatorChart(ExtendIndicatorParam param) {
		ExtendIndicatorDataParam result = new ExtendIndicatorDataParam();
		List<String> uuid = param.getInnerUUID();
		if (evUtil.listIsNullOrZero(uuid)) {
			return result;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Integer> indicator = param.getIndicator();
		if (evUtil.listIsNullOrZero(indicator)) {
			return result;
		}

		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, param.getInterval());

		Map<String, Object> map = new HashMap<>();
		map.put("innerUUID", uuid);
		map.put("indicatorid", indicator);
		map.put("start", start);
		map.put("end", end);
		map.put("dataSourceId", param.getSourcesId());
		map.put("table", "sourceTran");
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.info("第三方指标请求参数{}", map);
		List<SourceTran> sourceTranList = sourceTranMapper.selectSourceTranByUuid(map);

		// group by uuid
		Map<String, List<SourceTran>> grouplist = sourceTranList.stream()
				.collect(Collectors.groupingBy(SourceTran::getInnerUUID, Collectors.toList()));
		Map<String, List<SourceTran>> resultGroup = new LinkedHashMap<>();

		uuid.forEach(x -> {
			if (grouplist.containsKey(x)) {
				resultGroup.put(x, initExtendDefault(grouplist.get(x), timeList, indicator));
			} else {
				// init default list
				resultGroup.put(x, initExtendDefault(new ArrayList<>(), timeList, indicator));
			}
		});

		// assembly data
		List<ExtendIndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		for (Map.Entry<String, List<SourceTran>> entry : resultGroup.entrySet()) {
			for (Integer integer : indicator) {
				ExtendIndicatorDataDetailParam detail = new ExtendIndicatorDataDetailParam();
				detail.setId(integer);
				detail.setInnerUUID(Integer.parseInt(entry.getKey()));
				List<SourceTran> source = entry.getValue();
				List<String> invalue = new ArrayList<>();
				for (SourceTran sourceTran : source) {
					if (sourceTran.getIndicatorid().equals(integer)) {
						if (null != sourceTran.getValue()) {
							String str = new DecimalFormat("0.####").format(sourceTran.getValue());// 这里得到的是用“，”隔开的数字串
							str = str.replace(",", "");
							invalue.add(str);
						} else {
							invalue.add(null);
						}
					}
				}

				detail.setValue(invalue);
				value.add(detail);
			}
			result.setIndicator(value);
		}
		return result;
	}

	@Override
	public Map<String, Object> transCountPiano(int type, int uuid, long start, long end) {
		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end,
				configurationCache);
		List<Long> timeList = indicatorParam.getTimeList();
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();
		Map<String, Object> map = new HashMap<>(4);
		List<Integer> uuids = new ArrayList<>();
		uuids.add(uuid);
		map.put("ids", uuids);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(type, map, indicatorParam.getType());
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<Integer> types = new ArrayList<>();
		types.add(1);
		List<String> columnNames = commonService.selectIndicator(types);
		// deal with indicator str
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(), map, "indicatorColumnStr",
				columnNames);
		logger.info("transCountPiano指标查询参数：{}", JSON.toJSONString(map));
		List<Integer> amountList = new ArrayList<>();
		if (type == 1) {
			List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(map);
			boolean flag;
			for (Long time : timeList) {
				flag = false;
				for (BpmTran trans : bpmTranList) {
					if (time.equals(trans.getTime())) {
						amountList.add(trans.getAllTransCount());
						flag = true;
						break;
					}
				}
				if (!flag) {
					amountList.add(null);
				}
			}
		} else {
			List<BpmExtendTran> bpmExtendTranList = bpmTranMapper.selectBpmExtendTranByUuid(map);
			boolean flag;
			for (Long time : timeList) {
				flag = false;
				for (BpmExtendTran trans : bpmExtendTranList) {
					if (time.equals(trans.getTime())) {
						amountList.add(trans.getAllTransCount());
						flag = true;
						break;
					}
				}
				if (!flag) {
					amountList.add(null);
				}
			}
		}
		Map<String, Object> result = new HashMap<>();
		result.put("time", indicatorParam.getTimeList());
		result.put("transCount", amountList);
		result.put("interval", indicatorParam.getInterval());
		return result;
	}

	@Override
	public Map<String, Object> selectCombineBpmTranTable(IndicatorParam param) {
		List<Integer> uuid = param.getUuid();
		Long start = param.getStart();
		Long end = param.getEnd();

		Integer interval = param.getInterval();
		IndicatorIntervalParam indicatorParam;
		if(interval == null){
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end,
					configurationCache);
		}else{
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start,end,interval);
		}

		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(1, map, indicatorParam.getType());
		List<Integer> indicators = param.getIndicator();
		Map<Integer,String> indicatorMap = indicatorService.getIndicatorName(indicators);

		List<String> columnNames = new ArrayList<>(indicatorMap.values());
		// 拼接指标sql
		IndicatorSupportUtil.generateIndicatorColumnStr(map, columnNames, evUtil.getMapIntValue(map, "type"));
		map.put("indicatorColumnStr", map.get("indicatorColumnStr").toString().substring(1));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.debug("》》》》请求参数：{}", JSON.toJSONString(map));
		List<Map<String,Object>> list = bpmTranMapper.selectCombineBpmTranTable(map);
		Map<String, Object> result = new HashMap<>();
 		if(!evUtil.listIsNullOrZero(list)){
			Map<String,Object> tempMap = list.get(0);
			for(String columnName : columnNames){
				result.put(columnName, tempMap.get(columnName));
			}
		}else{
			columnNames.stream().forEach(x-> result.put(x, null));
		}
		return result;
	}

	@Override
	public List<TransactionExtendTableParam> extendTableGroupByUuid(IndicatorParam param) {
		List<TransactionExtendTableParam> table = new ArrayList<>();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return table;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		Integer interval=param.getInterval();
		IndicatorIntervalParam indicatorParam = new IndicatorIntervalParam();
		if (null == interval) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(2, map, indicatorParam.getType());
		// 拼接指标sql
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<BpmExtendTran> bpmTranList = bpmTranMapper.selectExtendTranTable(map);
		//补数据
		for (Integer id : uuid) {
			boolean flag=false;
			for (BpmExtendTran bpmExtendTran : bpmTranList) {
				if(id.equals(bpmExtendTran.getUuid())) {
					flag=true;
					break;
				}
			}
			if(!flag) {
				bpmTranList.add(new BpmExtendTran(id));
			}
		}
		if(!evUtil.listIsNullOrZero(bpmTranList)) {
			for (BpmExtendTran bpmExtendTran : bpmTranList) {
				TransactionExtendTableParam detail=new TransactionExtendTableParam();
				detail.setUuid(bpmExtendTran.getUuid());
				detail.setAllTransCount(bpmExtendTran.getAllTransCount());
				detail.setResponseRate(bpmExtendTran.getResponseRate());
				detail.setResponseTime(bpmExtendTran.getResponseTime());
				detail.setSuccessRate(bpmExtendTran.getSuccessRate());
				detail.setMoney(bpmExtendTran.getMoney());
				detail.setBusinessSuccessRate(bpmExtendTran.getBusinessSuccessRate());
				table.add(detail);
			}
		}
		return table;
	}

	@Override
	public List<TransactionTableParam> tableGroupByUuid(IndicatorParam param) {
		List<TransactionTableParam> table = new ArrayList<>();
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return table;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		Integer interval=param.getInterval();
		IndicatorIntervalParam indicatorParam = new IndicatorIntervalParam();
		if (null == interval) {
			indicatorParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		}else {
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, interval);
		}
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(1, map, indicatorParam.getType());
		// 拼接指标sql
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		List<String> enameList = new ArrayList<>();
		for (Map.Entry<Integer, String> ename : colnameList.entrySet()) {
			enameList.add(ename.getValue());
		}
		IndicatorSupportUtil.generateIndicatorColumnStr(map, enameList, evUtil.getMapIntValue(map, "type"));
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		List<BpmTran> bpmTranList = bpmTranMapper.selectTranTable(map);
		//补数据
		for (Integer id : uuid) {
			boolean flag=false;
			for (BpmTran bpmTran : bpmTranList) {
				if(id.equals(bpmTran.getUuid())) {
					flag=true;
					break;
				}
			}
			if(!flag) {
				bpmTranList.add(new BpmTran(id));
			}
		}
		if(!evUtil.listIsNullOrZero(bpmTranList)) {
			for (BpmTran bpmTran : bpmTranList) {
				TransactionTableParam detail=new TransactionTableParam();
				detail.setUuid(bpmTran.getUuid());
				detail.setAllTransCount(bpmTran.getAllTransCount());
				detail.setResponseRate(bpmTran.getResponseRate());
				detail.setResponseTime(bpmTran.getResponseTime());
				detail.setSuccessRate(bpmTran.getSuccessRate());
				table.add(detail);
			}
		}
		return table;
	}

	@Override
	public List<TransactionTpsParam> getTpsMax(TpsParam param) {
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return null;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		// default interval 5s
		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, 5);
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		Integer type = param.getType();
		if (type.equals(1)) {// bpm
			map.put("table", T_BPMTRAN);
		} else {
			map.put("table", T_BPMEXTENDTRAN);
		}
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.info("tps峰值请求参数{}",map);
		List<TransactionTpsParam> result = bpmTranMapper.getTpsMax(map);
		// 补数据
		for (Integer id : uuid) {
			boolean flag = false;
			for (TransactionTpsParam tpsParam : result) {
				if (id.equals(tpsParam.getUuid())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new TransactionTpsParam(id));
			}
		}
		return result;
	}

	@Override
	public TransactionTpsParam getCombineTpsMax(TpsParam param) {
		List<Integer> uuid = param.getUuid();
		if (evUtil.listIsNullOrZero(uuid)) {
			return null;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		// default interval 5s
		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, 5);
		start = indicatorParam.getStart();
		end = indicatorParam.getEnd();

		Map<String, Object> map = new HashMap<>(14);
		map.put("ids", uuid);
		map.put("start", start);
		map.put("end", end);
		Integer type = param.getType();
		if (type.equals(1)) {// bpm
			map.put("table", T_BPMTRAN);
		} else {
			map.put("table", T_BPMEXTENDTRAN);
		}
		// 加入date分区条件
		map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
		logger.info("tps峰值请求参数{}",map);
		TransactionTpsParam result = bpmTranMapper.getCombineTpsMax(map);
		// 补数据
		return result;
	}
}
