package com.greattimes.ev.indicator.service.impl;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import com.greattimes.ev.bpm.service.common.IIndicatorService;
import com.greattimes.ev.indicator.param.req.*;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.ReportDataChartParam;
import com.greattimes.ev.indicator.param.resp.ReportIndicatorDetailParam;
import com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam;
import com.greattimes.ev.utils.ClickHouseUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.bpm.config.param.resp.TranslateDataParam;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.utils.IndicatorSupportUtil;


import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.mapper.BpmPreMapper;
import com.greattimes.ev.indicator.service.IBpmPreService;

/**
 * @author NJ
 * @date 2018/9/26 21:03
 */
@Service
public class BpmPreServiceImpl implements IBpmPreService{

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private BpmPreMapper bpmPreMapper;

    @Autowired
    private ICommonService commonService;

    @Autowired
	private ConfigurationCache configurationCache;

	@Autowired
	private IIndicatorService indicatorService;

    private static final String TABLE_FIX_BPM = "bpmPre";
    private static final String TABLE_FIX_EXTEND = "extendPre";
    private static final String VIEW_FIX_BPM = "v_transaction_pre_";
    private static final String VIEW_FIX_EXTEND = "v_extendPre_60";
    private static final String VIEW_VM_BPM = "vm_transaction_pre";
    private static final String VIEW_VM_EXTEND = "vm_extendPre";
    private static final String CLICKHOUSE_CLUSTER_NAME= "clickhouse_cluster_name";
    private static final String CLICKHOUSE_CLUSTER_DB_NAME= "clickhouse_cluster_db_name";





    @Override
	public Map<String, Object> selectListForChart(DimensionParam param) {
    	List<Integer> indicatorIds =param.getIndicatorIds();
        if(evUtil.listIsNullOrZero(indicatorIds)) {
        	return null;
        }
        Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream().collect(Collectors.toMap(Indicator::getId,indicator->indicator));
        List<String> enameList=new ArrayList<>();
		for(Entry<Integer, Indicator> ename:indicatorMap.entrySet()) {
			enameList.add(ename.getValue().getColumnName());
		}
        IndicatorIntervalParam indicatorParam=IndicatorSupportUtil.getIndicatorInterval(param.getStartTime(), param.getEndTime(), configurationCache);
    	Long start = indicatorParam.getStart();
		Long end = indicatorParam.getEnd();
		List<Long> timeList =indicatorParam.getTimeList();
		int type=indicatorParam.getType();

		Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("start", start);
        paramMap.put("end", end);
        //handle  dimensionConditions
        List<JSONObject> filterDimensionIds = param.getFilterDimension();
        if(!evUtil.listIsNullOrZero(filterDimensionIds)){
        	/*for (JSONObject jsonObject : filterDimensionIds) {
        		columnStrList.add(jsonObject.getString("ename"));
			}*/
            List<String> conditionStr = new ArrayList<>();
            String dimensionCondition;
            for (JSONObject jsonObject : filterDimensionIds) {
				String ename  = jsonObject.getString("ename");
				int rule = jsonObject.getIntValue("rule");
				List<String> value = (List<String>) jsonObject.get("value");
				handleFilterDimension(conditionStr, ename, rule, value);
    		}
            if(!evUtil.listIsNullOrZero(conditionStr)){
            	dimensionCondition = StringUtils.join(conditionStr, " AND ");
                paramMap.put("dimensionCondition", " AND " + dimensionCondition);
            }
        }


        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(param.getStartTime(), param.getEndTime());
        paramMap.put("dateStr", dateStrList);
        List<Map<String, Object>> data = new ArrayList<>();
        if (param.getType().intValue()==1) {
        	paramMap.put("componentId", param.getUuid());
            IndicatorSupportUtil.dealIndicatorTable(3, paramMap, type);
    		IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, enameList, evUtil.getMapIntValue(paramMap, "type"));
            data = bpmPreMapper.getMultiChat(paramMap);
		}
        if (param.getType().intValue()==2) {
        	paramMap.put("applicationId", param.getApplicationId());
        	paramMap.put("customId", param.getUuid());
            IndicatorSupportUtil.dealIndicatorTable(4, paramMap, type);
    		IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, enameList, evUtil.getMapIntValue(paramMap, "type"));
            data = bpmPreMapper.getCustomMultiChat(paramMap);
		}

        //compare data
        String date=param.getCompareData();
        List<Map<String, Object>> compareData=new ArrayList<>();
        List<Long> comparetimeList =new  ArrayList<>();
        if(StringUtils.isNotEmpty(date)) {
        	Long compareStart =initCompareTime(date,start);
        	Long compareEnd =initCompareTime(date,end);
        	//加入date分区条件
            List<String> compareDateStrList = DateUtils.getDateStrByStartAndEnd(compareStart, compareEnd);
            paramMap.put("dateStr", compareDateStrList);
        	paramMap.put("start", compareStart);
            paramMap.put("end", compareEnd);
            if (param.getType().intValue()==1) {
				compareData = bpmPreMapper.getMultiChat(paramMap);
			}
            if (param.getType().intValue()==2) {
            	compareData = bpmPreMapper.getCustomMultiChat(paramMap);
            }
            IndicatorIntervalParam compareParam=IndicatorSupportUtil.getIndicatorInterval(compareStart, compareEnd, configurationCache);
            comparetimeList = compareParam.getTimeList();
            compareData=IndicatorSupportUtil.getForMatList(compareData,comparetimeList);
        }

        data=IndicatorSupportUtil.getForMatList(data,timeList);
        Map<String, Object> resultMap=new HashMap<>();
        resultMap.put("time", timeList);
        if(!evUtil.listIsNullOrZero(indicatorIds)) {
        	List<Map<String, Object>> indicatorResList=new ArrayList<>();
        	for (Map.Entry<Integer, Indicator> entry : indicatorMap.entrySet()) {
        		Map<String, Object> indicatorResMap=new HashMap<>();
        		indicatorResMap.put("id", entry.getKey());
        		List<String> value=new ArrayList<>();
        		List<String> valueCP=new ArrayList<>();
        		String name=indicatorMap.get(entry.getKey()).getColumnName();
    			/*if(name.equals("allTransCount")) {
            		name="transCount";
            	}*/
        		for (Map<String, Object> map : data) {
        			value.add(map.get(name)==null?null:map.get(name).toString());
        		}
        		for (Map<String, Object> map : compareData) {
        			valueCP.add(map.get(name)==null?null:map.get(name).toString());
        		}
        		indicatorResMap.put("value", value);
        		indicatorResMap.put("valueCP", valueCP);
        		indicatorResList.add(indicatorResMap);
        	}
        	resultMap.put("indicator", indicatorResList);
        }
		return resultMap;
	}
    
    @Override
	public Map<String, Object> selectAnalyzeListForChart(DimensionParam param) {
		List<Integer> indicatorIds = param.getIndicatorIds();
		if (evUtil.listIsNullOrZero(indicatorIds)) {
			return null;
		}
		Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream()
				.collect(Collectors.toMap(Indicator::getId, indicator -> indicator));
		List<String> enameList=new ArrayList<>();
		for(Entry<Integer, Indicator> ename:indicatorMap.entrySet()) {
			enameList.add(ename.getValue().getColumnName());
		}
    	IndicatorIntervalParam indicatorParam=IndicatorSupportUtil.getIndicatorInterval(param.getStartTime(), param.getEndTime(), configurationCache);
    	Long start = indicatorParam.getStart();
		Long end = indicatorParam.getEnd();
		List<Long> timeList =indicatorParam.getTimeList();
		int type=indicatorParam.getType();

		Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("start", start);
        paramMap.put("end", end);
        //handle  dimensionConditions
        List<JSONObject> filterDimensionIds = param.getFilterDimension();
        if(!evUtil.listIsNullOrZero(filterDimensionIds)){
            List<String> conditionStr = new ArrayList<>();
            String dimensionCondition;
            for (JSONObject jsonObject : filterDimensionIds) {
				String ename  = jsonObject.getString("ename");
				int rule = jsonObject.getIntValue("rule");
				List<String> value = (List<String>) jsonObject.get("value");
				handleFilterDimension(conditionStr, ename, rule, value);
    		}
            if(!evUtil.listIsNullOrZero(conditionStr)){
            	dimensionCondition = StringUtils.join(conditionStr, " AND ");
                paramMap.put("dimensionCondition", " AND " + dimensionCondition);
            }
        }


        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(param.getStartTime(), param.getEndTime());
        paramMap.put("dateStr", dateStrList);
        List<Map<String, Object>> data = new ArrayList<>();
        if (param.getType().intValue()==1) {
        	paramMap.put("componentId", param.getUuid());
            IndicatorSupportUtil.dealIndicatorTable(3, paramMap, type);
            IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, enameList, evUtil.getMapIntValue(paramMap, "type"));
            data = bpmPreMapper.getMultiChat(paramMap);
		}
        if (param.getType().intValue()==2) {
        	paramMap.put("applicationId", param.getApplicationId());
        	paramMap.put("customId", param.getUuid());
            IndicatorSupportUtil.dealIndicatorTable(4, paramMap, type);
            IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, enameList, evUtil.getMapIntValue(paramMap, "type"));
            data = bpmPreMapper.getCustomMultiChat(paramMap);
		}

        //compare data
        Long compareStartTime=param.getCompareStartTime();
        Long compareEndTime=param.getCompareEndTime();
        List<Map<String, Object>> compareData=new ArrayList<>();
        List<Long> comparetimeList =new  ArrayList<>();
        if(null!=compareStartTime&&null!=compareEndTime) {
        	IndicatorIntervalParam compareParam=IndicatorSupportUtil.getIndicatorInterval(compareStartTime, compareEndTime, configurationCache);
        	Long compareStart = compareParam.getStart();
    		Long compareEnd = compareParam.getEnd();
        	//加入date分区条件
            List<String> compareDateStrList = DateUtils.getDateStrByStartAndEnd(compareStart, compareEnd);
            paramMap.put("dateStr", compareDateStrList);
        	paramMap.put("start", compareStart);
            paramMap.put("end", compareEnd);
            if (param.getType().intValue()==1) {
				compareData = bpmPreMapper.getMultiChat(paramMap);
			}
            if (param.getType().intValue()==2) {
            	compareData = bpmPreMapper.getCustomMultiChat(paramMap);
            }
            comparetimeList = compareParam.getTimeList();
            compareData=IndicatorSupportUtil.getForMatList(compareData,comparetimeList);
        }

        data=IndicatorSupportUtil.getForMatList(data,timeList);
        Map<String, Object> resultMap=new HashMap<>();
        resultMap.put("time", timeList);
        if(!evUtil.listIsNullOrZero(indicatorIds)) {
        	List<Map<String, Object>> indicatorResList=new ArrayList<>();
        	for (Map.Entry<Integer, Indicator> entry : indicatorMap.entrySet()) {
        		Map<String, Object> indicatorResMap=new HashMap<>();
        		indicatorResMap.put("id", entry.getKey());
        		List<String> value=new ArrayList<>();
        		List<String> valueCP=new ArrayList<>();
        		String name=indicatorMap.get(entry.getKey()).getColumnName();
    			/*if(name.equals("allTransCount")) {
            		name="transCount";
            	}*/
        		for (Map<String, Object> map : data) {
        			value.add(map.get(name)==null?null:map.get(name).toString());
        		}
        		for (Map<String, Object> map : compareData) {
        			valueCP.add(map.get(name)==null?null:map.get(name).toString());
        		}
        		indicatorResMap.put("value", value);
        		indicatorResMap.put("valueCP", valueCP);
        		indicatorResList.add(indicatorResMap);
        	}
        	resultMap.put("indicator", indicatorResList);
        }
		return resultMap;
	}


	@Override
    public List<Map<String, Object>> selectListForTable(DimensionParam param,List<String> fixed_dimension){

        Map<String, Object> paramMap = new HashMap<>(14);

        //handle indicatorConditions(indicators)
        List<JSONObject> filterIndicatorIds = param.getFilterIndicator();
        if(!evUtil.listIsNullOrZero(filterIndicatorIds)){
            List<Integer> indicatorIds = new ArrayList<>();
            for(JSONObject jsonObject : filterIndicatorIds){
                indicatorIds.add(jsonObject.getIntValue("indicatorId"));
            }
            Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream().collect(Collectors.toMap(Indicator::getId,indicator->indicator));
            //generating condition
            List<String> conditionStr = new ArrayList<>();
            String indicatorConditionStr;
            for(JSONObject jsonObject : filterIndicatorIds){

                if(indicatorMap.get(jsonObject.getInteger("indicatorId")) != null){
                	int rule=jsonObject.getIntValue("rule");
            		List<Long> value=(List<Long>) jsonObject.get("value");
                	//交易量allTransCount转换为transCount
                	String name=indicatorMap.get(jsonObject.getInteger("indicatorId")).getColumnName();
                	/*if(name.equals("allTransCount")) {
                		name="transCount";
                	}*/
                	if(rule==1||rule==2) {
                	String str = name
                				+ this.translateIndicatorRule(jsonObject.getIntValue("rule")) + value.get(0);
                	conditionStr.add(str);
                	}
                	if(rule==3||rule==4) {
                		String str=StringUtils.join(value, " , ");
                    	str = name+ this.translateIndicatorRule(jsonObject.getIntValue("rule")) +"("+str+")";
                    	conditionStr.add(str);
                    }
                }
            }
            if(!evUtil.listIsNullOrZero(conditionStr)){
                indicatorConditionStr = StringUtils.join(conditionStr, " AND ");
                paramMap.put("indicatorCondition", indicatorConditionStr);
            }
        }


      //handle  dimensionConditions (包含ename/dimensionType)
        List<JSONObject> filterDimensionIds = param.getFilterDimension();
        if(!evUtil.listIsNullOrZero(filterDimensionIds)){
            List<String> conditionStr = new ArrayList<>();
            String dimensionConditionStr;
            for(JSONObject jsonObject : filterDimensionIds){
				// 如果是事件 需要判断是否是事件固定维度
				String ename = jsonObject.getString("ename");
				/*if (param.getType().intValue() == 2) {
					if (fixed_dimension.contains(jsonObject.getString("ename"))) {
						ename = jsonObject.getString("ename");
					} else {
						ename = "visitParamExtractString(extendFields,'" + jsonObject.getString("ename") + "')";
					}
				}else {
					ename = jsonObject.getString("ename");
				}*/
				int rule = jsonObject.getIntValue("rule");
				List<String> value = (List<String>) jsonObject.get("value");
				handleFilterDimension(conditionStr, ename, rule, value);

			}
            if(!evUtil.listIsNullOrZero(conditionStr)){
            	dimensionConditionStr = StringUtils.join(conditionStr, " AND ");
                paramMap.put("dimensionCondition", " AND " + dimensionConditionStr);
            }
        }



        //handle dimension columns
        List<String> columnStrList = new ArrayList<>();
        List<String> groupColumnStrList = new ArrayList<>();
        List<JSONObject> columnsList= param.getDimensionColumns();
		if (param.getType().intValue() == 1) {
			if (!evUtil.listIsNullOrZero(columnsList)) {
				for (JSONObject jsonObject : columnsList) {
					columnStrList.add(jsonObject.getString("ename"));
					groupColumnStrList.add(jsonObject.getString("ename"));
				}
			}
		}
		//如果是事件 需要判断是否是事件固定维度
		if (param.getType().intValue() == 2) {
			if (!evUtil.listIsNullOrZero(columnsList)) {
				for (JSONObject jsonObject : columnsList) {
					/*if(fixed_dimension.contains(jsonObject.getString("ename"))) {
						columnStrList.add(jsonObject.getString("ename"));
					}else {
						columnStrList.add("visitParamExtractString(extendFields,'"+jsonObject.getString("ename")+"') AS "+jsonObject.getString("ename"));
					}*/
					columnStrList.add(jsonObject.getString("ename"));
					groupColumnStrList.add(jsonObject.getString("ename"));
				}
			}
		}
        if(!evUtil.listIsNullOrZero(columnStrList)){
            paramMap.put("dimensionColumns",StringUtils.join(columnStrList, " , "));
        }
        IndicatorIntervalParam indicatorParam=IndicatorSupportUtil.getIndicatorInterval(param.getStartTime(), param.getEndTime(), configurationCache);
        paramMap.put("start", indicatorParam.getStart());
        paramMap.put("end", indicatorParam.getEnd());
        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(indicatorParam.getStart(), indicatorParam.getEnd());
        paramMap.put("dateStr", dateStrList);
        int granularity=indicatorParam.getType();
        //table
        List<Map<String, Object>> data = new ArrayList<>();
        if (param.getType().intValue()==1) {
        	IndicatorSupportUtil.dealIndicatorTable(3, paramMap, granularity);
        	List<Integer> type = new ArrayList<>();
    		type.add(1);
    		List<String> indicators = commonService.selectIndicator(type);
    		IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
        	paramMap.put("componentId", param.getUuid());
            data = bpmPreMapper.selectListByIndicators(paramMap);
		}
        if (param.getType().intValue()==2) {
        	if(!evUtil.listIsNullOrZero(groupColumnStrList)){
                paramMap.put("groupDimensionColumns",StringUtils.join(groupColumnStrList, " , "));
            }
        	IndicatorSupportUtil.dealIndicatorTable(4, paramMap, granularity);
        	// 拼接指标sql
        	List<Integer> type = new ArrayList<>();
    		type.add(1);
    		type.add(2);
    		List<String> indicators = commonService.selectIndicator(type);
    		IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
        	paramMap.put("applicationId", param.getApplicationId());
        	paramMap.put("customId", param.getUuid());
            data = bpmPreMapper.selectCustomListByIndicators(paramMap);
		}




        //compare data
        String date=param.getCompareData();
        List<Map<String, Object>> compareData=new ArrayList<>();
        if(StringUtils.isNotEmpty(date)) {
        	Long compareStart =initCompareTime(date,param.getStartTime());
        	Long compareEnd =initCompareTime(date,param.getEndTime());
        	IndicatorIntervalParam compareParam=IndicatorSupportUtil.getIndicatorInterval(compareStart, compareEnd, configurationCache);
        	compareEnd=compareParam.getEnd();
        	compareStart=compareParam.getStart();
        	//加入date分区条件
            List<String> compareDateStrList = DateUtils.getDateStrByStartAndEnd(compareStart, compareEnd);
            paramMap.put("dateStr", compareDateStrList);
        	paramMap.put("start", compareStart);
            paramMap.put("end", compareEnd);
            paramMap.remove("indicatorCondition");
            if (param.getType().intValue()==1) {
            	compareData= bpmPreMapper.selectListByIndicators(paramMap);
            }else {
            	compareData= bpmPreMapper.selectCustomListByIndicators(paramMap);
            }

        }

        //查询条件有维度 根据维度匹配加入
		for (Map<String, Object> map : data) {
			boolean flag = false;

			for (Map<String, Object> compareMap : compareData) {
				boolean judge = true;
				//所有的查询维度一样 就匹配上
				for (String str : groupColumnStrList) {
					if (!map.get(str).equals(compareMap.get(str))) {
						judge = false;
						break;
					}
				}
				if (judge) {
					//添加指标需修改
					map.put("allTransCountCP", compareMap.get("allTransCount"));
					map.put("responseTimeCP", compareMap.get("responseTime"));
					map.put("responseRateCP", compareMap.get("responseRate"));
					map.put("successRateCP", compareMap.get("successRate"));
					if (param.getType().intValue()==2) {
						map.put("businessSuccessRateCP", compareMap.get("businessSuccessRate"));
						map.put("moneyCP", compareMap.get("money"));
					}
					flag = true;
					break;
				}

			}
			if (!flag) {
				//添加指标需修改
				map.put("allTransCountCP", null);
				map.put("responseTimeCP", null);
				map.put("responseRateCP", null);
				map.put("successRateCP", null);
				if (param.getType().intValue()==2) {
					map.put("businessSuccessRateCP", null);
					map.put("moneyCP", null);
				}
			}
		}
        // tranlate
        List<TranslateDataParam> translate=new ArrayList<>();
        if(!evUtil.listIsNullOrZero(param.getDimensionColumns())){
        	if (param.getType().intValue()==1) {
        		translate=commonService.getComponentTranslate(param.getUuid());
        	}
        	if (param.getType().intValue()==2) {
        		translate=commonService.getCustomTranslate(param.getUuid());
        	}
        	//加入翻译
        	for (Map<String, Object> map : data) {
        		for (String str : groupColumnStrList) {
        			boolean flag=false;
        			for (TranslateDataParam map2 : translate) {
    					if(str.equals(map2.getEname())&&map.get(str).equals(map2.getValue())) {
    						map.put(str+"Translate", map2.getMapping());
    						flag=true;
    						break;
    					}
    				}
        			if(!flag) {
        				map.put(str+"Translate", null);
        			}
				}
    		}


        }

        return data;
    }

    /**
     * 生成对比的起始时间戳
     * @author cgc
     * @date 2018年10月10日
     * @param date
     * @param time
     * @return
     */
    private static Long initCompareTime(String date, Long time) {

    	String timestr=DateUtils.timeStamp2Date(String.valueOf(time), "HH:mm:ss");
    	timestr=date+" "+timestr;
    	timestr= DateUtils.date2TimeStamp(timestr, "yyyyMMdd HH:mm:ss");
		return Long.valueOf(timestr);
	}
	/**
     * 指标筛选规则转化为sql语句条件
     * @param rule
     * @return
     * @throws RuntimeException
     */
    private String translateIndicatorRule(int rule) throws RuntimeException {
        String sql;
        switch (rule){
            case 1 :
                sql = " > ";
                break;
            case 2 :
                sql = " < ";
                break;
            case 3 :
                sql = " IN ";
                break;
            case 4 :
                sql = " NOT IN ";
                break;
            default:
                throw new RuntimeException("筛选规则参数有误，参数(rule):"+ rule);
        }
        return sql;
    }




    @Override
    public boolean addPerTable(int componentId, List<String> columns,String url) {
		Map<String, Object> param = new HashMap<>(3);
		String tableName = TABLE_FIX_BPM + componentId + "_local";
		param.put("table", tableName);
		// hand columns condition ddl
		String columnStr = "";
		String orderStr = "";
		List<String> cols = new ArrayList<>();
		String createSql = "create table if not exists  " + tableName + "\n" + "\t\t(\n"
				+ "\t\t\tdate               Date,\n" + "\t\t\ttime               Int64,\n"
				+ "\t\t\ttimeHour           Int64,\n" + "\t\t\ttimeFiveMinute     Int64,\n"
				+ "\t\t\tallTransCount      Int64,\n" + "\t\t\tallTransCountBak   Int64 alias allTransCount,\n"
				+ "\t\t\tresponseTransCount Int64,\n" + "\t\t\tsuccessTransCount  Int64,\n"
				+ "\t\t\tallStart2ends      Float64,\n" + "\t\t\tresponseTimeMax    Float64,\n"
				+ "\t\t\tresponseTimeMin    Float64,\n" + "\t\t\tbytesLength    Int64,\n"
				+ "\t\t\tbytesLengthMax Int64,\n" + "\t\t\tbytesLengthMin Int64,\n"
				+ "\t\t\trespBytesLength    Int64,\n" + "\t\t\trespBytesLengthMax Int64,\n"
				+ "\t\t\trespBytesLengthMin Int64\n" + "\n" + "$1" + "\n" + "\t\t)\n" + "\t\tengine = MergeTree\n"
				+ "\t\tPARTITION BY date ORDER BY (date, time\n" + "\n" + "$2" + "\n" + "\t\t)\n"
				+ "\t\tSETTINGS index_granularity = 8192;";

		String clusterName = configurationCache.getValue(CLICKHOUSE_CLUSTER_NAME);
		String clusterDbName = configurationCache.getValue(CLICKHOUSE_CLUSTER_DB_NAME);

		String clusterSql = "\t\tcreate table if not exists  " + TABLE_FIX_BPM + componentId + "\n" + "\t\t(\n"
				+ "\t\t\tdate               Date,\n" + "\t\t\ttime               Int64,\n"
				+ "\t\t\ttimeHour           Int64,\n" + "\t\t\ttimeFiveMinute     Int64,\n"
				+ "\t\t\tallTransCount      Int64,\n" + "\t\t\tallTransCountBak   Int64 alias allTransCount,\n"
				+ "\t\t\tresponseTransCount Int64,\n" + "\t\t\tsuccessTransCount  Int64,\n"
				+ "\t\t\tallStart2ends      Float64,\n" + "\t\t\tresponseTimeMax    Float64,\n"
				+ "\t\t\tresponseTimeMin    Float64,\n" + "\t\t\tbytesLength    Int64,\n"
				+ "\t\t\tbytesLengthMax Int64,\n" + "\t\t\tbytesLengthMin Int64,\n"
				+ "\t\t\trespBytesLength    Int64,\n" + "\t\t\trespBytesLengthMax Int64,\n"
				+ "\t\t\trespBytesLengthMin Int64\n" + "$" + "\n" + "\t\t)\n" + "\t\tengine = Distributed("
				+ clusterName + ", " + clusterDbName + ", " + tableName + ", rand());";
		if (!evUtil.listIsNullOrZero(columns)) {
			columns.forEach(x -> {
				cols.add(x + " String ");
			});
			columnStr = " , " + StringUtils.join(cols, " , ");
			orderStr = " , " + StringUtils.join(columns, " , ");
			param.put("columns", columnStr);
			param.put("orderCols", orderStr);
		}
		createSql = StringUtils.replaceEach(createSql, new String[] { "$1", "$2" },
				new String[] { columnStr, orderStr });
		clusterSql = clusterSql.replaceAll("\\$", columnStr);

		// bpmPreMapper.createPerTable(param);
		// create table
		ClickHouseUtil.executeCreateTableByCache(configurationCache, createSql, url);
		// create cluster table
		ClickHouseUtil.executeCreateTableByCache(configurationCache, clusterSql, url);
		logger.info("########createSql:" + createSql);
		logger.info("########clusterSql:" + clusterSql);

		return true;
	}

	@Override
	public boolean CreateClusterTable(List<String> columns,String url) {
		String tableName = TABLE_FIX_BPM+"_local";
		// hand columns condition ddl
		String columnStr = "";
		List<String> cols = new ArrayList<>();

		String clusterName = configurationCache.getValue(CLICKHOUSE_CLUSTER_NAME);
		String clusterDbName = configurationCache.getValue(CLICKHOUSE_CLUSTER_DB_NAME);

		/*String clusterSql = "\t\tcreate table if not exists  " + TABLE_FIX_BPM  + "\n" + "\t\t(\n"
				+ "\t\t\tdate               Date,\n" + "\t\t\ttime               Int64,\n"
				+ "\t\t\ttimeHour           Int64,\n" + "\t\t\ttimeFiveMinute     Int64,\n"
				+ "\t\t\tallTransCount      Int64,\n" + "\t\t\tallTransCountBak   Int64 alias allTransCount,\n"
				+ "\t\t\tresponseTransCount Int64,\n" + "\t\t\tsuccessTransCount  Int64,\n"
				+ "\t\t\tallStart2ends      Float64,\n" + "\t\t\tresponseTimeMax    Float64,\n"
				+ "\t\t\tresponseTimeMin    Float64,\n" + "\t\t\tbytesLength    Int64,\n"
				+ "\t\t\tbytesLengthMax Int64,\n" + "\t\t\tbytesLengthMin Int64,\n"
				+ "\t\t\trespBytesLength    Int64,\n" + "\t\t\trespBytesLengthMax Int64,\n"
				+ "\t\t\trespBytesLengthMin Int64\n" + "$" + "\n" + "\t\t)\n" + "\t\tengine = Distributed("
				+ clusterName + ", " + clusterDbName + ", " + tableName + ", rand());";*/

		String clusterSql = "create table if not exists "+TABLE_FIX_BPM+"\r\n" +
				"(\r\n" +
				"  date               Date,\r\n" +
				"  time               Int64,\r\n" +
				"  timeHour           Int64,\r\n" +
				"  timeFiveMinute     Int64,\r\n" +
				"  applicationId		 Int32,\r\n" +
				"  monitorId			 Int32,\r\n" +
				"  componentId    Int32,\r\n" +
				"  allTransCount      Int64,\r\n" +
				"  responseTransCount Int64,\r\n" +
				"  successTransCount  Int64,\r\n" +
				"  allStart2ends      Float64,\r\n" +
				"  responseTimeMax    Float64,\r\n" +
				"  responseTimeMin    Float64,\r\n" +
				"  bytesLength        Int64,\r\n" +
				"  bytesLengthMax     Int64,\r\n" +
				"  bytesLengthMin     Int64,\r\n" +
				"  respBytesLength    Int64,\r\n" +
				"  respBytesLengthMax Int64,\r\n" +
				"  respBytesLengthMin Int64,\r\n" +
				"  clientIp           String,\r\n" +
				"    serverIp           String,\r\n" +
				"  serverPort String,\r\n" +
				"  retCode            String,\r\n" +
				"  transactionType    String,\r\n" +
				"  transactionChannel String,\r\n" +
				"  allTransCountBak   Int64 alias allTransCount\r\n" + "$"+
				")\r\n" +
				"  engine = Distributed("+clusterName+", "+clusterDbName+", "+tableName+", rand());";
		if (!evUtil.listIsNullOrZero(columns)) {
			columns.forEach(x -> {
				cols.add(x + " String ");
			});
			columnStr = " , " + StringUtils.join(cols, " , ");
		}
		clusterSql = clusterSql.replaceAll("\\$", columnStr);

		// create cluster table
		ClickHouseUtil.executeCreateTableByCache(configurationCache, clusterSql, url);
		logger.info("########clusterSql:" + clusterSql);

		return true;
	}


	@Override
	public boolean updatePerTable(String table, List<String> columns, String host,String url) {
		//查询现有的列名
		if(!evUtil.listIsNullOrZero(columns)){
			String sql="DESCRIBE  TABLE  "+table;
			List<JSONObject> tableInfo =ClickHouseUtil.execQuery(sql, host,url);
			Set<String> colSet = new HashSet<>();
			tableInfo.forEach(x->{
				colSet.add(evUtil.getMapStrValue(x,"name"));
			});
			List<String> colList = new ArrayList<>();
			boolean flag;
			for(String dstcol : columns){
				flag = true;
				for(String srCol : colSet){
					if(dstcol.equals(srCol)){
						flag = false;
						break;
					}
				}
				if(flag){
					colList.add(" ADD COLUMN "+ dstcol+" String ");
				}
			}
			if(!evUtil.listIsNullOrZero(colList)){
				String modifyDdl =" ALTER TABLE " + table + " "+ StringUtils.join(colList," , ");
				logger.info("修改表结构的sql" + modifyDdl);
				ClickHouseUtil.execDDL(modifyDdl,host,url);
			}
		}
		return true;
	}

	@Override
	public boolean isExist(String table,String host,String url) {
		String sql="EXISTS TABLE "+table;
		boolean flag=ClickHouseUtil.isExistTable(sql, host, url);
		return flag;
	}

	@Override
	public IndicatorDataParam getIndicatorBydimension(IndicatorParam param, int interval) {
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval * 1000);

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("start", start);
		paramMap.put("end", end);
		//handle  dimensionConditions
		List<String> columnStrList = new ArrayList<>();
		List<JSONObject> filterDimensionIds = param.getFilterDimension();
		if(!evUtil.listIsNullOrZero(filterDimensionIds)){
			List<Integer> dimensionIds = new ArrayList<>();
			filterDimensionIds.forEach(x->{
				dimensionIds.add(x.getIntValue("dimensionId"));
			});
			//获取协议字段ename
			Map<Integer, ProtocolField> fieldMap = commonService.findFieldByIds(dimensionIds)
					.stream().collect(Collectors.toMap(ProtocolField::getId, field->field));
			dimensionIds.forEach(x->{
				columnStrList.add(fieldMap.get(x).getEname());
			});
			List<String> conditionStr = new ArrayList<>();
			String indicatorConditionStr;
			for(JSONObject jsonObject : filterDimensionIds){
				if(fieldMap.get(jsonObject.getInteger("dimensionId")) != null){
					if(jsonObject.getIntValue("rule")==3||jsonObject.getIntValue("rule")==4) {
						String str = fieldMap.get(jsonObject.getInteger("dimensionId")).getEname()
								+ IndicatorSupportUtil.translateDimensionRule(jsonObject.getIntValue("rule")) + "'"+jsonObject.get("value")+"'";
						conditionStr.add(str);
					}
					if(jsonObject.getIntValue("rule")==2||jsonObject.getIntValue("rule")==1) {
						String[] inArr=jsonObject.getString("value").split(",");
						String instr="";
						for (String string : inArr) {
							instr=instr+"'"+string+"'"+",";
						}
						instr=instr.substring(0, instr.length()-1);
						String str = fieldMap.get(jsonObject.getInteger("dimensionId")).getEname()
								+ IndicatorSupportUtil.translateDimensionRule(jsonObject.getIntValue("rule")) +"("+ instr+")";
						conditionStr.add(str);
					}
					if(jsonObject.getIntValue("rule")==5||jsonObject.getIntValue("rule")==6) {
						String str = fieldMap.get(jsonObject.getInteger("dimensionId")).getEname()
								+ IndicatorSupportUtil.translateDimensionRule(jsonObject.getIntValue("rule")) +"'"+ jsonObject.get("value")+"'";
						conditionStr.add(str);
					}
				}
			}
			if(!evUtil.listIsNullOrZero(conditionStr)){
				indicatorConditionStr = StringUtils.join(conditionStr, " AND ");
				paramMap.put("dimensionCondition", " AND " + indicatorConditionStr);
			}
		}
		//handle dimension columns
        /*if(!evUtil.listIsNullOrZero(columnStrList)){
            paramMap.put("dimensionColumns",StringUtils.join(columnStrList, " , "));
        }*/

		//table
		if (param.getType().intValue()==1) {
			String table = VIEW_FIX_BPM + param.getUuid();
			paramMap.put("table", table);
		}
		if (param.getType().intValue()==2) {
			String table = VIEW_FIX_EXTEND + param.getUuid();
			paramMap.put("table", table);
		}
		List<Map<String, Object>> data = bpmPreMapper.getMultiChat(paramMap);

		data=IndicatorSupportUtil.getForMatList(data,timeList);
		IndicatorDataParam result=new IndicatorDataParam();
		result.setTime(timeList);
		List<Integer> indicatorIds =param.getIndicator();
		if(!evUtil.listIsNullOrZero(indicatorIds)) {
			Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream().collect(Collectors.toMap(Indicator::getId,indicator->indicator));
			List<IndicatorDataDetailParam> indicatorResList=new ArrayList<>();
			for (Map.Entry<Integer, Indicator> entry : indicatorMap.entrySet()) {
				IndicatorDataDetailParam detail=new IndicatorDataDetailParam();
				detail.setId(entry.getKey());
				List<String> value=new ArrayList<>();
				String name=indicatorMap.get(entry.getKey()).getColumnName();
    			/*if(name.equals("allTransCount")) {
            		name="transCount";
            	}*/
				for (Map<String, Object> map : data) {
					value.add(map.get(name)==null?null:map.get(name).toString());
				}
				detail.setValue(value);
				indicatorResList.add(detail);
			}
			result.setIndicator(indicatorResList);
		}
		return result;
	}

	/*@Override
	public boolean createPerView(int id, List<String> columns, int type) {
        Map<String, Object> param = new HashMap<>(3);
        String viewName;
        String tableName;
        if(type == 1){
            viewName = VIEW_FIX_BPM + id;
            tableName = TABLE_FIX_BPM + id;
        }else if( type == 2){
            viewName = VIEW_FIX_EXTEND + id;
            tableName = TABLE_FIX_EXTEND + id;
        }else{
            return false;
        }
        param.put("view", viewName);
        param.put("table", tableName);

        String sql = "\t\tCREATE VIEW if not exists "+viewName+"\n" +
                "\t\tAS\n" +
                "\t\tSELECT date, time, SUM(allTransCount) AS allTransCount\n" +
                "\t\t\t, round(if(isNaN(SUM(allStart2ends) / SUM(responseTransCount)), 0, SUM(allStart2ends) / SUM(responseTransCount)), 2) AS responseTime\n" +
                "\t\t\t, round(if(isNaN(SUM(responseTransCount) / SUM(allTransCountBak)), 0, SUM(responseTransCount) / SUM(allTransCountBak))*100, 2) AS responseRate\n" +
                "\t\t\t, round(if(isNaN(SUM(successTransCount) / SUM(responseTransCount)), 0, SUM(successTransCount) / SUM(responseTransCount))*100, 2) AS successRate\n" +
                "\t\t\t, SUM(bytesLength) AS bytesLength\n" +
                "\t\t\t, Max(bytesLengthMax) AS bytesLengthMax\n" +
                "\t\t\t, Min(bytesLengthMin) AS bytesLengthMin\n" +
                "\t\t\t, SUM(respBytesLength) AS respBytesLength\n" +
                "\t\t\t, Max(respBytesLengthMax) AS respBytesLengthMax\n" +
                "\t\t\t, Min(respBytesLengthMin) AS respBytesLengthMin\n" +
                "\t\t\t, Max(responseTimeMax) AS responseTimeMax\n" +
                "\t\t\t, Min(responseTimeMin) AS responseTimeMin\n" +
                "$"+
                "\t\tFROM "+tableName+"\n" +
                "\t\tGROUP BY time, date\n" +
                "$";

        //hand columns condition ddl
        String columnStr = "";
        if(!evUtil.listIsNullOrZero(columns)){
            columnStr = " , "+StringUtils.join(columns," , ");
            param.put("columns",  columnStr);
        }
        sql = sql.replaceAll("\\$", columnStr);
        if(type == 1){
            ClickHouseUtil.executeCreateTableByCache(configurationCache, sql);
            logger.info("########createPerView:"+sql);
        }else{
            bpmPreMapper.createPerExtendView(param);
        }

        return true;
    }*/

	@Override
	public void dropTable(String tableName,String host,String url) {
		String sql="DROP TABLE "+tableName;
		ClickHouseUtil.execDDL(sql,host,url);

	}

	@Override
	public boolean createPerMaterializedView(int id, List<String> columns, String url) {
		String viewName = VIEW_VM_BPM + id+"_local";
		String viewName_5 = VIEW_VM_BPM + id+"_local_5";
		String tableName =  TABLE_FIX_BPM + id+"_local";
		String sql = "\t\tCREATE materialized VIEW if not exists  "+viewName+"\n" +
				"\t\tENGINE = AggregatingMergeTree(date, \n" +
				"\t\t(time\n" +
				"$"+
				"\t\t), 8192)\n" +
				"\t\tAS\n" +
				"\t\tselect date ,\n" +
				"\t\t     timeHour  as time\n" +
				"\t\t    ,sumState(allStart2ends) as allStart2ends\n" +
				"\t\t    ,sumState(responseTransCount) as responseTransCount\n" +
				"\t\t    ,sumState(successTransCount) as successTransCount\n" +
				"\t\t    ,sumState(allTransCount) as allTransCount\n" +
				"\t\t    ,sumState(bytesLength) as bytesLength\n" +
				"\t\t    ,sumState(respBytesLength) as respBytesLength\n" +
				"\t\t\t, Max(bytesLengthMax) AS bytesLengthMax\n" +
				"\t\t\t, Min(bytesLengthMin) AS bytesLengthMin\n" +
				"\t\t\t, Max(respBytesLengthMax) AS respBytesLengthMax\n" +
				"\t\t\t, Min(respBytesLengthMin) AS respBytesLengthMin\n" +
				"\t\t\t, Max(responseTimeMax) AS responseTimeMax\n" +
				"\t\t\t, Min(responseTimeMin) AS responseTimeMin\n" +
				"$"+
				"\t\tFROM "+tableName+"\n" +
				"\t\tGROUP BY timeHour, date\n" +
				"$;";

		String sql_5 = "\t\tCREATE materialized VIEW if not exists  "+viewName_5+"\n" +
				"\t\tENGINE = AggregatingMergeTree(date, \n" +
				"\t\t(time\n" +
				"$"+
				"\t\t), 8192)\n" +
				"\t\tAS\n" +
				"\t\tselect date ,\n" +
				"\t\t     timeFiveMinute  as time\n" +
				"\t\t    ,sumState(allStart2ends) as allStart2ends\n" +
				"\t\t    ,sumState(responseTransCount) as responseTransCount\n" +
				"\t\t    ,sumState(successTransCount) as successTransCount\n" +
				"\t\t    ,sumState(allTransCount) as allTransCount\n" +
				"\t\t    ,sumState(bytesLength) as bytesLength\n" +
				"\t\t    ,sumState(respBytesLength) as respBytesLength\n" +
				"\t\t\t, Max(bytesLengthMax) AS bytesLengthMax\n" +
				"\t\t\t, Min(bytesLengthMin) AS bytesLengthMin\n" +
				"\t\t\t, Max(respBytesLengthMax) AS respBytesLengthMax\n" +
				"\t\t\t, Min(respBytesLengthMin) AS respBytesLengthMin\n" +
				"\t\t\t, Max(responseTimeMax) AS responseTimeMax\n" +
				"\t\t\t, Min(responseTimeMin) AS responseTimeMin\n" +
				"$"+
				"\t\tFROM "+tableName+"\n" +
				"\t\tGROUP BY timeFiveMinute, date\n" +
				"$;";

		String clusterName = configurationCache.getValue(CLICKHOUSE_CLUSTER_NAME);
		String clusterDbName = configurationCache.getValue(CLICKHOUSE_CLUSTER_DB_NAME);

		String vmSql = "CREATE TABLE IF NOT EXISTS "+VIEW_VM_BPM + id+" AS "+viewName+"\n" +
				"        ENGINE = Distributed("+clusterName+", "+clusterDbName+", "+viewName+", rand());";

		String vmSql_5 = "CREATE TABLE IF NOT EXISTS "+VIEW_VM_BPM + id+"_5"+" AS "+viewName_5+"\n" +
				"        ENGINE = Distributed("+clusterName+", "+clusterDbName+", "+viewName_5+", rand());";

		String columnStr = "";
		//hand columns condition ddl
		if(!evUtil.listIsNullOrZero(columns)){
			columnStr =   " , "+StringUtils.join(columns," , ");
		}
		sql = sql.replaceAll("\\$", columnStr);
		sql_5 = sql_5.replaceAll("\\$", columnStr);
		// bpmPreMapper.createPerMaterializedView(param);
		logger.info("########createPerMaterializedView:" + sql);
		ClickHouseUtil.executeCreateTableByCache(configurationCache, sql,url);
		logger.info("########createPerMaterializedView--vmSql:" + sql);
		ClickHouseUtil.executeCreateTableByCache(configurationCache, vmSql,url);

		logger.info("########createPerMaterializedView_5:" + sql_5);
		ClickHouseUtil.executeCreateTableByCache(configurationCache, sql_5,url);
		logger.info("########createPerMaterializedView--vmSql_5:" + sql_5);
		ClickHouseUtil.executeCreateTableByCache(configurationCache, vmSql_5,url);
		return true;
	}

	/*@Override
	public boolean createPerHouer(int id, List<String> columns, int type) {
        Map<String, Object> param = new HashMap<>(3);
        String viewName = "";
        String tableName = "";
        if(type == 1){
            viewName = VIEW_FIX_BPM + id+ "_60";
            tableName = VIEW_VM_BPM + id;
            param.put("view", viewName);
            param.put("table", tableName);
        }else if( type == 2){
            viewName = VIEW_FIX_EXTEND + id+"_60";
            tableName = VIEW_VM_EXTEND + id;
            param.put("view", viewName);
            param.put("table", tableName);
        }else{
            return false;
        }

        //hand columns condition ddl
        String columnStr = "";
        if(!evUtil.listIsNullOrZero(columns)){
            columnStr =  " , "+StringUtils.join(columns," , ");
            param.put("columns", columnStr);
        }
        String sql = "CREATE  VIEW if not exists  "+viewName+"\n" +
                "\t\tAS\n" +
				"  SELECT date,\n" +
				"         time,\n" +
				"         sumMerge(allStart2ends)      AS allStart2ends,\n" +
				"         sumMerge(responseTransCount) AS responseTransCount,\n" +
				"         sumMerge(successTransCount)  AS successTransCount,\n" +
				"         sumMerge(allTransCount)      AS allTransCount,\n" +
				"         sumMerge(bytesLength)        AS bytesLength,\n" +
				"         sumMerge(respBytesLength)    AS respBytesLength,\n" +
				"         Max(bytesLengthMax)          AS bytesLengthMax,\n" +
				"         Min(bytesLengthMin)          AS bytesLengthMin,\n" +
				"         Max(respBytesLengthMax)      AS respBytesLengthMax,\n" +
				"         Min(respBytesLengthMin)      AS respBytesLengthMin,\n" +
				"         Max(responseTimeMax)         AS responseTimeMax,\n" +
				"         Min(responseTimeMin)         AS responseTimeMin\n" +
                "$"+
                "\t\tFROM "+tableName+"\n" +
                "\t\tGROUP BY time, date\n" +
                "$;" ;

        sql = sql.replaceAll("\\$", columnStr);

        if(type == 1){
//            bpmPreMapper.createPerViewHour(param);
            ClickHouseUtil.executeCreateTableByCache(configurationCache, sql);
            logger.info("########createPerHouer:"+sql);
        }else{
            bpmPreMapper.createPerExtendViewHour(param);
        }

        return true;
    }*/

	@Override
	public void detachTable(String table,String host,String url) {
		String sql="DETACH TABLE "+table;
		ClickHouseUtil.execDDL(sql, host,url);
	}

	@Override
	public boolean updateView(String table, List<String> columns, String host,String url) {
        //查询现有的列名
        if(!evUtil.listIsNullOrZero(columns)){
            String sql="DESCRIBE  TABLE  "+table;
			List<JSONObject> tableInfo= ClickHouseUtil.execQuery(sql, host, url);
            Set<String> colSet = new HashSet<>();
            tableInfo.forEach(x->{
                colSet.add(evUtil.getMapStrValue(x,"name"));
            });
            List<String> colList = new ArrayList<>();
            boolean flag;
            for(String dstcol : columns){
                flag = true;
                for(String srCol : colSet){
                    if(dstcol.equals(srCol)){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    colList.add(" ADD COLUMN "+ dstcol+" String ");
                }
            }
            if(!evUtil.listIsNullOrZero(colList)){
                String modifyDdl =" ALTER TABLE " + table + " "+ StringUtils.join(colList," , ");
                logger.info("修改表结构的sql" + modifyDdl);
                ClickHouseUtil.execDDL(modifyDdl, host,url);
            }
        }
        return true;
    }

	@Override
	public boolean AttachView(List<String> enameList, String host,String url) {
		String view=VIEW_VM_BPM+"_local";
		String view_5=VIEW_VM_BPM+"_local_5";
		String	table=TABLE_FIX_BPM+"_local";
		String ename;
		if(evUtil.listIsNullOrZero(enameList)) {
			ename="";
		}else {
			ename=", "+StringUtils.join(enameList," , ");
		}
        String sql=" ATTACH MATERIALIZED VIEW "+view+" ENGINE = AggregatingMergeTree(date,(time,componentId, serverIp, clientIp,serverPort, transactionType, transactionChannel, retCode),8192)\r\n" + 
        		"	        AS select date ,\r\n" + 
        		"	     timeHour  as time\r\n" + 
        		"       ,componentId\r\n"+
        		"	    ,sumState(allStart2ends) as allStart2ends\r\n" + 
        		"	    ,sumState(responseTransCount) as responseTransCount\r\n" + 
        		"	    ,sumState(successTransCount) as successTransCount\r\n" + 
        		"	    ,sumState(allTransCount) as allTransCount\r\n" + 
        		"	    ,sumState(bytesLength) as bytesLength\r\n" + 
        		"	    ,sumState(respBytesLength) as respBytesLength\r\n" + 
        		"	    , Max(bytesLengthMax) AS bytesLengthMax\r\n" + 
        		"		, Min(bytesLengthMin) AS bytesLengthMin\r\n" + 
        		"		, Max(respBytesLengthMax) AS respBytesLengthMax\r\n" + 
        		"		, Min(respBytesLengthMin) AS respBytesLengthMin\r\n" + 
        		"		, Max(responseTimeMax) AS responseTimeMax\r\n" + 
        		"		, Min(responseTimeMin) AS responseTimeMin\r\n" + 
        		" 		,clientIp\r\n" + 
        		"    	,serverIp\r\n" + 
        		"  		,serverPort\r\n" + 
        		"  		,retCode\r\n" + 
        		"  		,transactionType\r\n" + 
        		"  		,transactionChannel"+
        		        ename+"\r\n" + 
        		"        from "+table+"\r\n" + 
        		"        group by  date,timeHour,componentId, clientIp, serverIp,serverPort,  retCode, transactionType,transactionChannel\r\n" + 
        		"        "+ename+";";
        String sql_5=" ATTACH MATERIALIZED VIEW "+view_5+" ENGINE = AggregatingMergeTree(date,(time,componentId, serverIp, clientIp,serverPort, transactionType, transactionChannel, retCode),8192)\r\n" + 
        		"	        AS select date ,\r\n" + 
        		"	     timeFiveMinute  as time\r\n" + 
        		"       ,componentId\r\n"+
        		"	    ,sumState(allStart2ends) as allStart2ends\r\n" + 
        		"	    ,sumState(responseTransCount) as responseTransCount\r\n" + 
        		"	    ,sumState(successTransCount) as successTransCount\r\n" + 
        		"	    ,sumState(allTransCount) as allTransCount\r\n" + 
        		"	    ,sumState(bytesLength) as bytesLength\r\n" + 
        		"	    ,sumState(respBytesLength) as respBytesLength\r\n" + 
        		"	    , Max(bytesLengthMax) AS bytesLengthMax\r\n" + 
        		"		, Min(bytesLengthMin) AS bytesLengthMin\r\n" + 
        		"		, Max(respBytesLengthMax) AS respBytesLengthMax\r\n" + 
        		"		, Min(respBytesLengthMin) AS respBytesLengthMin\r\n" + 
        		"		, Max(responseTimeMax) AS responseTimeMax\r\n" + 
        		"		, Min(responseTimeMin) AS responseTimeMin\r\n" + 
        		" 		,clientIp\r\n" + 
        		"    	,serverIp\r\n" + 
        		"  		,serverPort\r\n" + 
        		"  		,retCode\r\n" + 
        		"  		,transactionType\r\n" + 
        		"  		,transactionChannel"+
        		        ename+"\r\n" + 
        		"        from "+table+"\r\n" + 
        		"        group by  date,timeFiveMinute,componentId, clientIp, serverIp,serverPort,  retCode, transactionType,transactionChannel\r\n" + 
        		"        "+ename+";";
        logger.info("重新绑定视图的sql" + sql);
		ClickHouseUtil.execDDL(sql, host,url);
		
		logger.info("重新绑定视图的sql(5分钟)" + sql_5);
		ClickHouseUtil.execDDL(sql_5, host,url);
		return true;
	}

	@Override
	public List<Map<String, Object>> getMaxAndMinData(int id, long startDate, long endDate,int type) {
		String table =null;
		if(type==1) {
			table=TABLE_FIX_BPM;
		}else {
			table=TABLE_FIX_EXTEND;
		}
		logger.info("numericCols查询："+startDate+","+endDate+","+id);
		List<Map<String, Object>> list=bpmPreMapper.getMaxAndMinData(table,startDate,endDate,type,id);
		return list;
	}

	@Override
	public boolean compareWithTable(String table, List<String> columns,String host,String url) {
		boolean result = false;
        //查询现有的列名
        if(!evUtil.listIsNullOrZero(columns)){
            String sql="DESCRIBE  TABLE  "+table;
			List<JSONObject> tableInfo= ClickHouseUtil.execQuery(sql, host,url);
            Set<String> colSet = new HashSet<>();
            tableInfo.forEach(x->{
                colSet.add(x.get("name").toString());
            });
            boolean flag;
            for(String dstcol : columns){
                flag = true;
                for(String srCol : colSet){
                    if(dstcol.equals(srCol)){
                        flag = false;
                        break;
                    }
                }
                if(flag){
                    result=true;
                    break;
                }
            }
        }
        return result;
    }

	@Override
	public List<Map<String, Object>> selectAllTransCountPercent(Map<String, Object> selectMap) {
		return bpmPreMapper.selectAllTransCountPercent(selectMap);
	}
	@Override
	public List<Map<String, Object>> selectCustomAllTransCountPercent(Map<String, Object> selectMap) {
		return bpmPreMapper.selectCustomAllTransCountPercent(selectMap);
	}
	@Override
	public List<Map<String, Object>> selectBpmSuccessAndfailPercent(Map<String,Object> map) {
		return bpmPreMapper.selectBpmSuccessAndfailPercent(map);
	}

	@Override
	public List<Map<String, Object>> selectExtendSuccessAndfailPercent(Map<String, Object> selectMap) {
		return bpmPreMapper.selectExtendSuccessAndfailPercent(selectMap);
	}

	@Override
	public List<Map<String, Object>> getIndicatorMaxMinAvg(Map<String, Object> selectMap) {
		return bpmPreMapper.getIndicatorMaxMinAvg(selectMap);
	}

	@Override
	public List<Map<String, Object>> getIndicatorMaxMinAvgPercent(Map<String, Object> selectMap) {
		return bpmPreMapper.getIndicatorMaxMinAvgPercent(selectMap);
	}
	
	@Override
	public List<Map<String, Object>> getBpmTableData(Map<String, Object> map) {
		return bpmPreMapper.getBpmTableData(map);
	}

	@Override
	public List<Map<String, Object>> getExtendTableData(Map<String, Object> map) {
		return bpmPreMapper.getExtendTableData(map);
	}

	@Override
	public int getTableDataCount(Map<String, Object> map) {
		return bpmPreMapper.getTableDataCount(map);
	}
	
	@Override
	public int getExtendTableDataCount(Map<String, Object> map) {
		return bpmPreMapper.getExtendTableDataCount(map);
	}
	
	@Override
	public List<Map<String, Object>> getDifTradeType(Map<String, Object> map) {
		return bpmPreMapper.getDifTradeType(map);
	}


	@Override
	public List<String> addPerTableColumns(int componentId, List<String> enameList, String host,String url) {
		List<String> result = new ArrayList<>();
        String table= TABLE_FIX_BPM + componentId+"_local";
        List<String> tableColumns = new ArrayList<>();
        Map<String, Object> param = new HashMap<>(2);
        param.put("table", table);
        String sql="DESCRIBE  TABLE  "+table;
		List<JSONObject> tableInfo =ClickHouseUtil.execQuery(sql, host, url);
        tableInfo.forEach(x->{
        	if(evUtil.getMapStrValue(x,"type").equals("String")) {
        		tableColumns.add(evUtil.getMapStrValue(x,"name"));
        	}
        });
        tableColumns.removeAll(enameList);
        result.addAll(tableColumns);
        result.addAll(enameList);
        return result;
    }



	@Override
	public void createVmTable(int componentId, List<String> columns, String host, String url) {
        String viewName = VIEW_VM_BPM+"_local";
        String clusterName = configurationCache.getValue(CLICKHOUSE_CLUSTER_NAME);
        String clusterDbName = configurationCache.getValue(CLICKHOUSE_CLUSTER_DB_NAME);
		String vmSql = "CREATE TABLE IF NOT EXISTS "+VIEW_VM_BPM + " AS "+viewName+"\n" +
                "        ENGINE = Distributed("+clusterName+", "+clusterDbName+", "+viewName+", rand());";
		String vmSql_5= "CREATE TABLE IF NOT EXISTS "+VIEW_VM_BPM +"_5"+" AS "+viewName+"_5"+"\n" +
                "        ENGINE = Distributed("+clusterName+", "+clusterDbName+", "+viewName+"_5, rand());";
		ClickHouseUtil.execDDL(vmSql, host,url);
		ClickHouseUtil.execDDL(vmSql_5, host,url);
	}


@Override
public Page<Map<String, Object>> selectTablePage(Page<Map<String, Object>> page, DimensionPageParam param,
		List<String> fixed_dimension) {

    Map<String, Object> paramMap = new HashMap<>();
    //handle indicatorConditions(indicators)
    List<JSONObject> filterIndicatorIds = param.getFilterIndicator();
    if(!evUtil.listIsNullOrZero(filterIndicatorIds)){
        List<Integer> indicatorIds = new ArrayList<>();
        for(JSONObject jsonObject : filterIndicatorIds){
            indicatorIds.add(jsonObject.getIntValue("indicatorId"));
        }
        Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream().collect(Collectors.toMap(Indicator::getId,indicator->indicator));
        //generating condition
        List<String> conditionStr = new ArrayList<>();
        String indicatorConditionStr;
        for(JSONObject jsonObject : filterIndicatorIds){
        	
            if(indicatorMap.get(jsonObject.getInteger("indicatorId")) != null){
            	int rule=jsonObject.getIntValue("rule");
        		List<Double> value=(List<Double>) jsonObject.get("value");
            	//交易量allTransCount转换为transCount
            	String name=indicatorMap.get(jsonObject.getInteger("indicatorId")).getColumnName();
            	/*if(name.equals("allTransCount")) {
            		name="transCount";
            	}*/
            	if(rule==1||rule==2) {
            	String str = name
            				+ this.translateIndicatorRule(jsonObject.getIntValue("rule")) + value.get(0);
            	conditionStr.add(str);
            	}
            	if(rule==3||rule==4) {
            		String str=StringUtils.join(value, " , ");
                	str = name+ this.translateIndicatorRule(jsonObject.getIntValue("rule")) +"("+str+")";
                	conditionStr.add(str);
                }
            }
        }
        if(!evUtil.listIsNullOrZero(conditionStr)){
            indicatorConditionStr = StringUtils.join(conditionStr, " AND ");
            paramMap.put("indicatorCondition", indicatorConditionStr);
        }
    }
    
    
  //handle  dimensionConditions (包含ename/dimensionType)
    List<JSONObject> filterDimensionIds = param.getFilterDimension();
    if(!evUtil.listIsNullOrZero(filterDimensionIds)){
        List<String> conditionStr = new ArrayList<>();
        String dimensionConditionStr;
        for(JSONObject jsonObject : filterDimensionIds){
				// 如果是事件 需要判断是否是事件固定维度
				String ename = jsonObject.getString("ename");
				/*if (param.getType().intValue() == 2) {
					if (fixed_dimension.contains(jsonObject.getString("ename"))) {
						ename = jsonObject.getString("ename");
					} else {
						ename = "visitParamExtractString(extendFields,'" + jsonObject.getString("ename") + "')";
					}
				}else {
					ename = jsonObject.getString("ename");
				}*/
				int rule = jsonObject.getIntValue("rule");
				List<String> value = (List<String>) jsonObject.get("value");
				handleFilterDimension(conditionStr, ename, rule, value);
            
        }
        if(!evUtil.listIsNullOrZero(conditionStr)){
        	dimensionConditionStr = StringUtils.join(conditionStr, " AND ");
            paramMap.put("dimensionCondition", " AND " + dimensionConditionStr);
        }
    }

   
    
    //handle dimension columns
    List<String> columnStrList = new ArrayList<>();
    List<String> groupColumnStrList = new ArrayList<>();
    List<JSONObject> columnsList= param.getDimensionColumns();
	if (param.getType().intValue() == 1) {
		if (!evUtil.listIsNullOrZero(columnsList)) {
			for (JSONObject jsonObject : columnsList) {
				columnStrList.add(jsonObject.getString("ename"));
				groupColumnStrList.add(jsonObject.getString("ename"));
			}
		}
	}
	//如果是事件 需要判断是否是事件固定维度
	if (param.getType().intValue() == 2) {
		if (!evUtil.listIsNullOrZero(columnsList)) {
			for (JSONObject jsonObject : columnsList) {
				/*if(fixed_dimension.contains(jsonObject.getString("ename"))) {
					columnStrList.add(jsonObject.getString("ename"));
				}else {
					columnStrList.add("visitParamExtractString(extendFields,'"+jsonObject.getString("ename")+"') AS "+jsonObject.getString("ename"));
				}*/
				columnStrList.add(jsonObject.getString("ename"));
				groupColumnStrList.add(jsonObject.getString("ename"));
			}
		}
	}
    if(!evUtil.listIsNullOrZero(columnStrList)){
        paramMap.put("dimensionColumns",StringUtils.join(columnStrList, " , "));
    }
    IndicatorIntervalParam indicatorParam=IndicatorSupportUtil.getIndicatorInterval(param.getStartTime(), param.getEndTime(), configurationCache);
    paramMap.put("start", indicatorParam.getStart());
    paramMap.put("end", indicatorParam.getEnd());
    //加入date分区条件
    List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(indicatorParam.getStart(), indicatorParam.getEnd());
    paramMap.put("dateStr", dateStrList);
    //int granularity=IndicatorSupportUtil.getIndicatorInterval(param.getStartTime(), param.getEndTime(), configurationCache).getType();
    
    //排序条件
    String orderBy=null;
    //判断排序字段是对比还是原字段
    boolean orderCp=false;
    if(null!=param.getOrder()) {
    	String order=param.getOrder().getString("ename");
    	if(order.substring(order.length()-2).equals("CP")) {
    		orderCp=true;
    		paramMap.put("orderBy", null);
    	}else {
    	String asc=param.getOrder().getInteger("asc")==1?" ASC":" DESC";
    	orderBy=order+asc;
    	paramMap.put("orderBy", orderBy);
    	}
    }else {
    	paramMap.put("orderBy", null);
    }
    
    //table
    List<Map<String, Object>> data = new ArrayList<>();
    //总记录数
    int total=0;
    if (param.getType().intValue()==1) {
    	IndicatorSupportUtil.dealIndicatorTable(3, paramMap, indicatorParam.getType());
    	List<Integer> type = new ArrayList<>();
		type.add(1);
		List<String> indicators = commonService.selectIndicator(type);
		IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
    	paramMap.put("componentId", param.getUuid());
        if(!orderCp) {//根据原数据排序要分页
        	data = bpmPreMapper.selectListByIndicators(page,paramMap);
        	total = bpmPreMapper.getMultidimensionCount(paramMap);
        }else {
        	data = bpmPreMapper.selectListByIndicators(paramMap);
        	total=data.size();
        }
	}
    
    if (param.getType().intValue()==2) {
    	if(!evUtil.listIsNullOrZero(groupColumnStrList)){
            paramMap.put("groupDimensionColumns",StringUtils.join(groupColumnStrList, " , "));
        }
    	IndicatorSupportUtil.dealIndicatorTable(4, paramMap, indicatorParam.getType());
    	List<Integer> type = new ArrayList<>();
		type.add(1);
		type.add(2);
		List<String> indicators = commonService.selectIndicator(type);
		IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
    	paramMap.put("applicationId", param.getApplicationId());
    	paramMap.put("customId", param.getUuid());
        if(!orderCp) {
        	data = bpmPreMapper.selectCustomListByIndicators(page,paramMap);
        	total = bpmPreMapper.getMultidimensionCustomCount(paramMap);
        }else {
        	data = bpmPreMapper.selectCustomListByIndicators(paramMap);
        	total=data.size();
        }
	}
    
    
    
   
    //compare data 
    String date=param.getCompareData();
    List<Map<String, Object>> compareData=new ArrayList<>();
    if(StringUtils.isNotEmpty(date)) {
    	Long compareStart =initCompareTime(date,param.getStartTime());
    	Long compareEnd =initCompareTime(date,param.getEndTime());
    	IndicatorIntervalParam compareParam=IndicatorSupportUtil.getIndicatorInterval(compareStart, compareEnd, configurationCache);
    	compareStart=compareParam.getStart();
    	compareEnd=compareParam.getEnd();
    	//加入date分区条件
        List<String> compareDateStrList = DateUtils.getDateStrByStartAndEnd(compareStart, compareEnd);
        paramMap.put("dateStr", compareDateStrList);
    	paramMap.put("start", compareStart);
        paramMap.put("end", compareEnd);
        paramMap.remove("indicatorCondition");
        if (param.getType().intValue()==1) {
        /*	if(orderCp) {
        		compareData= bpmPreMapper.selectListByIndicators(page,paramMap);
        	}else {*/
        		compareData= bpmPreMapper.selectListByIndicators(paramMap);
        	/*}*/
        }else {
        	/*if(orderCp) {
        		compareData= bpmPreMapper.selectCustomListByIndicators(page,paramMap);
        	}else {*/
        		compareData= bpmPreMapper.selectCustomListByIndicators(paramMap);
        	/*}*/
        }
        
    }
     
    
    //combin result
  //查询条件有维度 根据维度匹配加入
  		for (Map<String, Object> map : data) {
  			boolean flag = false;

  			for (Map<String, Object> compareMap : compareData) {
  				boolean judge = true;
  				//所有的查询维度一样 就匹配上
  				for (String str : groupColumnStrList) {
  					if (!map.get(str).equals(compareMap.get(str))) {
  						judge = false;
  						break;
  					}
  				}
  				if (judge) {
  					//添加指标需修改
  					map.put("allTransCountCP", compareMap.get("allTransCount"));
  					map.put("responseTimeCP", compareMap.get("responseTime"));
  					map.put("responseRateCP", compareMap.get("responseRate"));
  					map.put("successRateCP", compareMap.get("successRate"));
  					if (param.getType().intValue()==2) {
  						map.put("businessSuccessRateCP", compareMap.get("businessSuccessRate"));
  						map.put("moneyCP", compareMap.get("money"));
  					}
  					flag = true;
  					break;
  				}

  			}
  			if (!flag) {
  				map.put("allTransCountCP", null);
  				map.put("responseTimeCP", null);
  				map.put("responseRateCP", null);
  				map.put("successRateCP", null);
  				if (param.getType().intValue()==2) {
  					map.put("businessSuccessRateCP", null);
  					map.put("moneyCP", null);
  				}
  			}
  		}
  	//若是对比排序，需要处理
  	if(orderCp) {
  		String order=param.getOrder().getString("ename");
  		Integer asc=param.getOrder().getInteger("asc");
  		//根据相应维度排序
  		Collections.sort(data, new Comparator<Map<String, Object>>() {
            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
                Double name1 = Double.valueOf(o1.get(order)==null?"-1":o1.get(order).toString()) ;//name1是从你list里面拿出来的一个 
                Double name2 = Double.valueOf(o2.get(order)==null?"-1":o2.get(order).toString()) ; //name1是从你list里面拿出来的第二个name
                if(asc.equals(1)) {
                	return name1.compareTo(name2);
                }else {
                	return 0-name1.compareTo(name2);
                }
            }
        });
  		//取对应分页数据
  		List<Map<String, Object>> subData=new ArrayList<>();
  		Integer pageNumber = param.getPage();
        Integer pageSize= param.getSize();
        //从第几条开始
        int offset=(pageNumber-1)*pageSize;
        int size=0;
  		for (int i = offset; i < data.size(); i++) {
			subData.add(data.get(i));
			size++;
			if(size==pageSize) {
				break;
			}
		}
  		data.clear();
  		data.addAll(subData);
  		subData.clear();
  	}
  	
		
    // tranlate
    List<TranslateDataParam> translate=new ArrayList<>();
    if(!evUtil.listIsNullOrZero(param.getDimensionColumns())){
    	if (param.getType().intValue()==1) {
    		translate=commonService.getComponentTranslate(param.getUuid());
    	}
    	if (param.getType().intValue()==2) {
    		translate=commonService.getCustomTranslate(param.getUuid());
    	}
    	//加入翻译
    	for (Map<String, Object> map : data) {
    		for (String str : groupColumnStrList) {
    			boolean flag=false;
    			for (TranslateDataParam map2 : translate) {
					if(str.equals(map2.getEname())&&map.get(str).equals(map2.getValue())) {
						map.put(str+"Translate", map2.getMapping());
						flag=true;
						break;
					}
				}
    			if(!flag) {
    				map.put(str+"Translate", null);
    			}
			}
		}
    	
    	
    }
	page.setTotal(total);
    page.setRecords(data);
    return page;
}

    /**
     * 多维生成条件sql语句
     * @param ename
     * @param rule
     * @param dimensionValues
     * @return
     */
    public String generateDimensionConditionSqlStr(String ename, int rule, List<String> dimensionValues){
        String str = "";
        //等于 or 不等于
        if(rule == 1 || rule == 2) {
            str = "'"+StringUtils.join(dimensionValues, "' , '")+"'";
            str = "("+ename + IndicatorSupportUtil.translateDimensionRule(rule) +"("+str+"))";
        }
        //包含
        if(rule == 3) {
            for (String string : dimensionValues) {
                if(str.equals("")) {
                    str=ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'%"+string+"%'";
                }else {
                    str=str+" OR "+ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'%"+string+"%'";
                }
            }
            str = "(" + str + ")";
        }
        //不包含
        if(rule==4) {
            for (String string : dimensionValues) {
                if(str.equals("")) {
                    str=ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'%"+string+"%'";
                }else {
                    str=str+" AND "+ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'%"+string+"%'";
                }
            }
            str = "(" + str + ")";
        }
        //开头包含
        if(rule==5) {
            for (String string : dimensionValues) {
                if(str.equals("")) {
                    str=ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'"+string+"%'";
                }else {
                    str=str+" OR "+ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'"+string+"%'";
                }
            }
            str = "(" + str + ")";
        }
        //结尾包含
        if(rule==6) {
            for (String string : dimensionValues) {
                if(str.equals("")) {
                    str = ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'%"+string+"'";
                }else {
                    str = str +" OR "+ename+IndicatorSupportUtil.translateDimensionRule(rule)+"'%"+string+"'";
                }
            }
            str = "(" + str + ")";
        }
        //为空 不为空
        if(rule==7||rule==8) {
            str=ename+IndicatorSupportUtil.translateDimensionRule(rule);
            str = "(" + str + ")";
        }
        return str;
    }



	@Override
	public ReportLineIndicatorParam getReportPreTranIndicator(ReportIndicatorParam param) {

		Integer interval = param.getInterval();
		//0 正常 1 广发特有逻辑 天级数据分段查询数据
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();

    	//handle  dimensionConditions (包含ename/dimensionType)
		List<ReportDimensionParam> dimensionRules = param.getRules();
		//group by ename and rule
        Map<String, List<ReportDimensionParam>> dimensionRuleMap = new HashMap<>();
        String key;
        Map<String, Object> paramMap = new HashMap<>();
        for(ReportDimensionParam dimensionParam : dimensionRules){
            key = dimensionParam.getEname()+"_"+dimensionParam.getRule();
            if(dimensionRuleMap.get(key) == null){
                List<ReportDimensionParam> list = new ArrayList<>();
                list.add(dimensionParam);
                dimensionRuleMap.put(key, list);
            }else{
                dimensionRuleMap.get(key).add(dimensionParam);
            }
        }

        //hand filter dimensions
        String ename;
        int rule;
        List<String> conditionStrList = new ArrayList<>();
        for(Map.Entry<String, List<ReportDimensionParam>> entry : dimensionRuleMap.entrySet()){
            List<String> dimensionFilterValueList = new ArrayList<>();
            List<ReportDimensionParam> dimensionParamList = entry.getValue();
            if(evUtil.listIsNullOrZero(dimensionParamList)){
                continue;
            }
            for(ReportDimensionParam reportDimensionParam : entry.getValue()){
				dimensionFilterValueList.addAll(reportDimensionParam.getDimensionValues());
            }
            rule = dimensionParamList.get(0).getRule();
            ename = dimensionParamList.get(0).getEname();
            //according to the rules, generate sql condition
            conditionStrList.add(generateDimensionConditionSqlStr(ename,rule,dimensionFilterValueList));
        }

		//handle dimension columns
		//app single dimension()
        List<ReportDimensionParam> dataDimensions = param.getDimensions();
		List<String> dimensionValue = dataDimensions.get(0).getDimensionValues();
		String dimensionColumnName = dataDimensions.get(0).getEname();
		List<String> groupColumnStrList = new ArrayList<>(Arrays.asList(dimensionColumnName));
		int dimensionRule = dataDimensions.get(0).getRule();
		if(!evUtil.listIsNullOrZero(groupColumnStrList)){
			//according to the rules, generate dimension sql condition
			if(!evUtil.listIsNullOrZero(dimensionValue)){
				conditionStrList.add(generateDimensionConditionSqlStr(dimensionColumnName,dimensionRule,dimensionValue));
			}
			paramMap.put("dimensionColumns", StringUtils.join(groupColumnStrList,  " , "));
		}

		//filter sql condition
		if(!evUtil.listIsNullOrZero(conditionStrList)){
			String dimensionConditionStr = StringUtils.join(conditionStrList, " AND ");
			paramMap.put("dimensionCondition", " AND " + dimensionConditionStr);
		}

//		long start = param.getStartTime(), end = param.getEndTime();
//		IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, param.getInterval());


		// get indicator columns
		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if(intervalType == 1) {
			dInterval = 5*60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
			paramMap.put("groupColumns", " date ");
		}else{
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval);
			paramMap.put("groupColumns", " time ");
		}


		//加入date分区条件
//		List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(start, end);
//		paramMap.put("dateStr", dateStrList);
//		paramMap.put("start", start);
//		paramMap.put("end", end);


		List<Map<String, Object>> data;
		List<Map<String, Object>> timeParts = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timeParts,dInterval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		paramMap.put("timeStr", " and (" + timeStr + ")");
		paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		List<Long> timeList = IndicatorSupportUtil.generateTimeList(timePart, interval, intervalType);

		IndicatorSupportUtil.dealIndicatorTable(3,paramMap, indicatorParam.getType());
        int componentId = param.getComponentId();
        paramMap.put("componentId", componentId);

		//handle indicators (app:single-indicator )
		Map<Integer, Map<String, Object>> indicatorColumnsNamesMap = indicatorService.getIndicatorMapByIds(param.getIndicators());

		List<Integer> types = new ArrayList<>();
		types.add(1);
		List<String> columnNames = commonService.selectIndicator(types);
		//deal with indicator str
		IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(),paramMap,"indicatorColumnStr",columnNames);
        logger.info("-------bpmPre[查询参数]{}",paramMap.toString());

        data = bpmPreMapper.getAppLineDataByDimension(paramMap);

		//compare
		int compare = param.getCompare();
		List<Map<String, Object>> compareData = new ArrayList<>();
		List<Long> compareTimeList = new ArrayList<>();
		if(compare == 1 || compare == 2){
			//lastCycle
//			Long  compareStartTime = param.getCompareStartTime();
//			Long compareEndTime = param.getCompareEndTime();
//			paramMap.put("start", compareStartTime);
//			paramMap.put("end", compareEndTime);
//			IndicatorIntervalParam indicatorCompareParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(compareStartTime, compareEndTime, param.getInterval());
//			compareTimeList = indicatorCompareParam.getTimeList();
//			paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(compareStartTime,compareEndTime));


			List<Map<String, Object>> timePartsCompare = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(timePartsCompare,dInterval);
			String compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
			paramMap.put("timeStr", " and (" + compareTimeStr + ")");
			paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
			compareTimeList = IndicatorSupportUtil.generateTimeList(compareTimePart, interval, intervalType);
			//deal with indicator str
			IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(),paramMap,"indicatorColumnStr",columnNames);
			compareData = bpmPreMapper.getAppLineDataByDimension(paramMap);
			logger.info("-------bpmPre[上周期查询参数]{}",paramMap.toString());
		}

		ReportLineIndicatorParam result = new ReportLineIndicatorParam();
		result.setTime(indicatorParam.getTimeList());
		//dataMap key:dimensionValue map[key:indicator values:indicatorValue]
		Map<String, Map<Integer,List<String>>> dataMap = new HashMap<>();
		Map<String, Map<Integer,List<String>>> compareDataMap = new HashMap<>();
		if(!evUtil.listIsNullOrZero(dimensionValue)){
			for(String dValue : dimensionValue){
				Map<Integer,List<String>> compareDValueMap = new HashMap<>();
				Map<Integer,List<String>> dValueMap = new HashMap<>();
				for(Integer indicatorName : indicatorColumnsNamesMap.keySet()){
					dValueMap.put(indicatorName, new ArrayList<>());
					compareDValueMap.put(indicatorName, new ArrayList<>());
				}
				compareDataMap.put(dValue, compareDValueMap);
				dataMap.put(dValue, dValueMap);
			}
		}

		//assemble data by time
		IndicatorSupportUtil.assembleCompareMultiData(data,compareData,timeList,compareTimeList,
				groupColumnStrList.get(0), dataMap, compareDataMap,indicatorColumnsNamesMap ,compare,intervalType);

		List<ReportIndicatorDetailParam> detailParams = new ArrayList<>();
		constructPreResult(compare, indicatorColumnsNamesMap, dataMap, compareDataMap, detailParams,1);
		result.setIndicators(detailParams);
		result.setTime(timeList);
        return result;
	}


    @Override
    public ReportLineIndicatorParam getReportExtendPreTranIndicator(ReportIndicatorParam param) {

		Integer interval = param.getInterval();
		//0 正常 1 广发特有逻辑 天级数据分段查询数据
		Integer intervalType = param.getIntervalType() == null ? 0 : param.getIntervalType();

        List<ReportDimensionParam> dimensionRules = param.getRules();
        //group by ename and rule
        Map<String, List<ReportDimensionParam>> dimensionRuleMap = new HashMap<>();
        String key;
        Map<String, Object> paramMap = new HashMap<>();
        for(ReportDimensionParam dimensionParam : dimensionRules){
            key = dimensionParam.getEname()+"_"+dimensionParam.getRule();
            if(dimensionRuleMap.get(key) == null){
                List<ReportDimensionParam> list = new ArrayList<>();
                list.add(dimensionParam);
                dimensionRuleMap.put(key, list);
            }else{
                dimensionRuleMap.get(key).add(dimensionParam);
            }
        }

        //hand filter dimensions
        String ename;
        int rule;
        List<String> conditionStrList = new ArrayList<>();
        for(Map.Entry<String, List<ReportDimensionParam>> entry : dimensionRuleMap.entrySet()){
            List<String> dimensionFilterValueList = new ArrayList<>();
            List<ReportDimensionParam> dimensionParamList = entry.getValue();
            if(evUtil.listIsNullOrZero(dimensionParamList)){
                continue;
            }
            for(ReportDimensionParam reportDimensionParam : entry.getValue()){
				dimensionFilterValueList.addAll(reportDimensionParam.getDimensionValues());
            }
            ename = dimensionParamList.get(0).getEname();
			rule = dimensionParamList.get(0).getRule();
			//according to the rules, generate sql condition
            conditionStrList.add(generateDimensionConditionSqlStr(ename,rule,dimensionFilterValueList));
        }

        //handle dimension columns
        List<ReportDimensionParam> dataDimensions = param.getDimensions();
		List<String> dimensionValue = dataDimensions.get(0).getDimensionValues();
		String dimensionColumnName = dataDimensions.get(0).getEname();
		List<String> groupColumnStrList = new ArrayList<>(Arrays.asList(dimensionColumnName));
		int dimensionRule = dataDimensions.get(0).getRule();
		List<String> columnStrList = new ArrayList<>();
        //event [statistics dimension and analysis dimension]
		if (!evUtil.listIsNullOrZero(groupColumnStrList)) {
			for (String column : groupColumnStrList) {
				columnStrList.add(column);
			}
			if(!evUtil.listIsNullOrZero(dimensionValue)){
				conditionStrList.add(generateDimensionConditionSqlStr(dimensionColumnName,dimensionRule,dimensionValue));
			}
		}

		//dimension sql where condition
		if(!evUtil.listIsNullOrZero(conditionStrList)){
			String dimensionConditionStr = StringUtils.join(conditionStrList, " AND ");
			paramMap.put("dimensionCondition", " AND " + dimensionConditionStr);
		}

		if(!evUtil.listIsNullOrZero(columnStrList)){
			paramMap.put("dimensionColumns", StringUtils.join(columnStrList,  " , "));
		}

		if(!evUtil.listIsNullOrZero(groupColumnStrList)){
			paramMap.put("groupByColumns", StringUtils.join(groupColumnStrList,  " , "));
		}


//        long start = param.getStartTime(), end = param.getEndTime();
//        IndicatorIntervalParam indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(start, end, param.getInterval());
//        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(start, end);
//        paramMap.put("dateStr", dateStrList);
//        paramMap.put("start", start);
//        paramMap.put("end", end);

		IndicatorIntervalParam indicatorParam;
		int dInterval;
		if(intervalType == 1) {
			dInterval = 5*60;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(dInterval);
			paramMap.put("groupColumns", " date ");
		}else{
			dInterval = interval;
			indicatorParam = IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval);
			paramMap.put("groupColumns", " time ");
		}

		List<Map<String, Object>> timeParts = param.getTimePart();
		List<Map<String, Object>> timePart = DateUtils.generateTimeStr(timeParts,dInterval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		paramMap.put("timeStr", " and (" + timeStr + ")");
		paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		List<Long> timeList = IndicatorSupportUtil.generateTimeList(timePart, interval, intervalType);

		IndicatorSupportUtil.dealIndicatorTable(4,paramMap, indicatorParam.getType());
		paramMap.put("applicationId",param.getApplicationId());
		paramMap.put("customId", param.getCustomId());

        //handle indicator columnStr
		List<Integer> types = new ArrayList<>();
		types.add(1);
		types.add(2);
		List<String> columnNames = commonService.selectIndicator(types);
        IndicatorSupportUtil.handleIndicatorStrParamByType(indicatorParam.getType(),paramMap,"indicatorColumnStr",columnNames);
		logger.info("-------extendPre[查询参数]{}",paramMap.toString());
		List<Map<String, Object>> data = bpmPreMapper.getEventLineDataByDimension(paramMap);
        //compare
        int compare = param.getCompare();
        List<Map<String, Object>> compareData = new ArrayList<>();
        List<Long> compareTimeList = new ArrayList<>();
        if(compare == 1 || compare == 2){
            //lastCycle
//            Long  compareStartTime = param.getCompareStartTime();
//            Long compareEndTime = param.getCompareEndTime();
//            paramMap.put("start", compareStartTime);
//            paramMap.put("end", compareEndTime);
//            paramMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(compareStartTime,compareEndTime));
//			compareTimeList = IndicatorSupportUtil.getIndicatorIntervalByInterVal(compareStartTime,
//					compareEndTime, param.getInterval()).getTimeList();

			List<Map<String, Object>> timePartsCompare = param.getTimeCompare();
			List<Map<String, Object>> compareTimePart = DateUtils.generateTimeStr(timePartsCompare,dInterval);
			String compareTimeStr = IndicatorSupportUtil.generateTimeStr(compareTimePart);
			paramMap.put("timeStr", " and (" + compareTimeStr + ")");
			paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(compareTimePart));
			compareTimeList = IndicatorSupportUtil.generateTimeList(compareTimePart, interval, intervalType);
			compareData = bpmPreMapper.getEventLineDataByDimension(paramMap);
			logger.info("-------extendPre[上周期查询参数]{}",paramMap.toString());
        }

        ReportLineIndicatorParam result = new ReportLineIndicatorParam();
        result.setTime(timeList);

		//单事件,单维度,多维度值,多指标
        List<Integer> indicators = param.getIndicators();
		List<Integer> customIds = new ArrayList<>();
		customIds.add(param.getCustomId());
		Map<String, Object> indicatorMap = new HashMap<>(3);
		indicatorMap.put("ids", customIds);
		List<Map<String, Object>> indicatorNames = commonService.selectCustomIndicatorNamesByMap(indicatorMap);

		Map<Integer, Map<String, Object>> indicatorColumnsNamesMap = new HashMap<>();
		for(Integer indicatorId : indicators){
			for(Map<String, Object> map : indicatorNames){
				if(indicatorId == evUtil.getMapLongValue(map,"indicatorId")){
					indicatorColumnsNamesMap.put(indicatorId, map);
					break;
				}
			}
		}

        //init data var  dimensionValue:[indicatorId:values]
        Map<String, Map<Integer,List<String>>> dataMap = new HashMap<>();
        Map<String, Map<Integer,List<String>>> compareDataMap = new HashMap<>();
		if(!evUtil.listIsNullOrZero(dimensionValue)) {
			for (String dValue : dimensionValue) {
				Map<Integer, List<String>> dValueMap = new HashMap<>();
				Map<Integer, List<String>> compareDValueMap = new HashMap<>();

				for(Integer indicatorId : indicatorColumnsNamesMap.keySet()){
					dValueMap.put(indicatorId, new ArrayList<>());
					compareDValueMap.put(indicatorId, new ArrayList<>());
				}
				compareDataMap.put(dValue, compareDValueMap);
				dataMap.put(dValue, dValueMap);

			}
		}

        //construct data
        IndicatorSupportUtil.assembleCompareMultiData(data,compareData,timeList,compareTimeList,
				groupColumnStrList.get(0), dataMap, compareDataMap, indicatorColumnsNamesMap,compare,intervalType);

        List<ReportIndicatorDetailParam> detailParams = new ArrayList<>();
		constructPreResult(compare, indicatorColumnsNamesMap, dataMap, compareDataMap, detailParams, 2);
		result.setIndicators(detailParams);
		return result;
    }

	private void constructPreResult(int compare, Map<Integer, Map<String, Object>> indicatorColumnsNamesMap,
							Map<String, Map<Integer, List<String>>> dataMap, Map<String, Map<Integer, List<String>>> compareDataMap,
									List<ReportIndicatorDetailParam> detailParams, int type) {
		String entryKey;
		Integer daEntryKey;
		for (Map.Entry<String, Map<Integer, List<String>>> entry : dataMap.entrySet()) {
			entryKey = entry.getKey();
			for (Map.Entry<Integer, List<String>> daEntry : entry.getValue().entrySet()) {
				ReportIndicatorDetailParam detailParam = new ReportIndicatorDetailParam();
				detailParam.setValue(daEntry.getValue());
				daEntryKey = daEntry.getKey();
				if (compare == 1 || compare == 2) {
					detailParam.setValueCP(compareDataMap.get(entryKey).get(daEntryKey));
				}
				detailParam.setId(daEntry.getKey());
				if(type == 1){
					//application
					detailParam.setName(evUtil.getMapStrValue(indicatorColumnsNamesMap.get(daEntryKey), "name"));
				}else if(type == 2){
					//event
					detailParam.setName(evUtil.getMapStrValue(indicatorColumnsNamesMap.get(daEntryKey), "indicatorName"));
				}
				detailParam.setUnit(evUtil.getMapStrValue(indicatorColumnsNamesMap.get(daEntryKey), "unit"));
				detailParam.setDimensionValue(entry.getKey());
				detailParams.add(detailParam);
			}
		}
	}

	/**
	 * 组装维度数据
	 * @param compareData
	 * @param data
	 * @param timeList
	 * @param customIndicatorIdMap key:customId value:indicatorIds  Dimension Data has single customId
	 * @param columnsNamesMap
	 * @param seriesNameMap
	 * @return
	 */
	public ReportLineIndicatorParam assembleExtendDimensionLineCompareData(List<Map<String, Object>> compareData, List<Map<String, Object>> data,
			   List<Long> timeList, String dimension,List<String> dimensionValues, Map<Integer,List<Integer>> customIndicatorIdMap, Map<Integer, String> columnsNamesMap, Map<Integer, String> seriesNameMap){
		//group by dimensionValue
		String value = null,compareValue = null;
		Long timeValue = null;
		boolean compareFlag,flag;
		int len = timeList.size();
		Map<String, Object> map = new HashMap<>();
		List<Integer> indicators = customIndicatorIdMap.values().iterator().next();
		//custom indicators name
		if(evUtil.listIsNullOrZero(indicators)){
			throw new RuntimeException("customIndicatorIdMap--没有参数！");
		}
		for(String dimensionValue : dimensionValues){
			List<String> cAllTransCountList = new ArrayList<>(len);
			List<String> cResponseTimeList = new ArrayList<>(len);
			List<String> cResponseRateList = new ArrayList<>(len);
			List<String> cSuccessRateList = new ArrayList<>(len);
			List<String> allTransCountList = new ArrayList<>(len);
			List<String> responseTimeList = new ArrayList<>(len);
			List<String> responseRateList = new ArrayList<>(len);
			List<String> successRateList = new ArrayList<>(len);
			//find by dimensionValue and time
			for(int i = 0; i < len; i++){
				compareFlag = false;
				flag = false;
				//fill data in time(data)
				evaluateList(data, dimension,map,value,flag,dimensionValue,timeList.get(i),timeValue,
						allTransCountList,responseTimeList,responseRateList,successRateList);

				evaluateList(data, dimension,map,value,compareFlag,dimensionValue,timeList.get(i),timeValue,
						cAllTransCountList,cResponseTimeList,cResponseRateList,cSuccessRateList);
			}
		}

		//assemble Data by customIndicatorIdMap[key:customId value:indicators]
		// customId(single)->dimension(single)->indicators[multiple] relation(1:1:n)

//		for(){
//
//		}


		return null;
	}


	/**
	 * 计算指标数据
	 * @param data
	 * @param dimension
	 * @param map
	 * @param value
	 * @param flag
	 * @param dimensionValue
	 * @param time
	 * @param timeValue
	 * @param args
	 * @return
	 */
	private boolean evaluateList(List<Map<String, Object>> data,String dimension, Map<String, Object> map,
								 String value, boolean flag, String dimensionValue, Long time, Long timeValue,  List<String> ... args){
		for(int j = 0; j < data.size(); j++){
			map = data.get(j);
			value = evUtil.getMapStr(map, dimension);
			timeValue = evUtil.getMapLongValue(map,"time");
			if(dimensionValue.equals(value) && time.equals(timeValue)){
				flag = true;
				args[0].add(evUtil.getMapStr(map, "allTransCount"));
				args[1].add(evUtil.getMapStr(map, "responseTime"));
				args[2].add(evUtil.getMapStr(map, "responseRate"));
				args[3].add(evUtil.getMapStr(map, "successRate"));
				break;
			}
		}
		if(!flag){
			args[0].add(null);
			args[1].add(null);
			args[2].add(null);
			args[3].add(null);
		}
		return  flag;
	}


	@Override
	public List<Map<String, Object>> getTopChartData(ReportChartParam re) {
		String dimensionName=re.getDimensions().get(0).getEname();
		List<Integer> indicatorsId = re.getIndicators();
		if(evUtil.listIsNullOrZero(indicatorsId)) {
			return null;
		}
		Integer interval=re.getInterval();
		Integer intervalType=re.getIntervalType()==null?0:re.getIntervalType();
		if(intervalType==1) {
			interval=5*60;
		}
		// 数据类型 1、应用指标2、应用维度 3、事件指标4、事件维度
		Integer dataType = re.getDataType();
		// 1 折线 ，2 柱状 ，3 ，饼图 ，4 ，表格，5 柱状top
		Integer type = re.getType();
		Integer limit = re.getLimit();
		Integer sort = re.getSortType()==null?0:re.getSortType();
		Map<String, Object> map = new HashMap<>();
		// 获取查询时间
		List<Map<String, Object>> timePart=re.getTimePart();
		if(evUtil.listIsNullOrZero(timePart)) {
			return null;
		}
		timePart=DateUtils.generateTimeStr(timePart, interval);
		String timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
		map.put("timeStr", "AND ("+timeStr+")");
		map.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		//指标筛选
		List<JSONObject> filterIndicator = re.getFilterIndicator();
		if(!evUtil.listIsNullOrZero(filterIndicator)){
			map.put("indicatorStr", " ("+ IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterIndicator)+") ");
		}
		// 筛选维度
		List<ReportDimensionParam> filterDimensions = re.getRules();
		List<String> conditionStr = new ArrayList<>();
		String dimensionConditionStr;
		if (!evUtil.listIsNullOrZero(filterDimensions)) {
			for (ReportDimensionParam jsonObject : filterDimensions) {
				// 如果是事件 需要判断是否是事件固定维度
				String ename = jsonObject.getEname();
				Integer rule = jsonObject.getRule();
				List<String> value = (List<String>) jsonObject.getDimensionValues();
				handleFilterDimension(conditionStr, ename, rule, value);

			}
		}
		// 添加维度
		boolean limitFlag=true;
		List<String> columnStrList = new ArrayList<>();
		List<ReportDimensionParam> columnsList = re.getDimensions();
		if (!evUtil.listIsNullOrZero(columnsList)) {
			for (ReportDimensionParam jsonObject : columnsList) {
				String ename = jsonObject.getEname();
				Integer rule = jsonObject.getRule()==null?0:jsonObject.getRule();
				List<String> value = (List<String>) jsonObject.getDimensionValues();
				if (rule.equals(7) || rule.equals(8)) {
					handleFilterDimension(conditionStr, ename, rule, value);
				} else {
					if (!evUtil.listIsNullOrZero(value)) {
						handleFilterDimension(conditionStr, ename, rule, value);
						limitFlag = false;
					}
				}
				columnStrList.add(jsonObject.getEname());
			}
		} else {
			return null;
		}
		if (!evUtil.listIsNullOrZero(conditionStr)) {
			dimensionConditionStr = StringUtils.join(conditionStr, " AND ");
			map.put("dimensionCondition", " AND " + dimensionConditionStr);
		}
		if (!evUtil.listIsNullOrZero(columnStrList)) {
			map.put("dimensionColumns", StringUtils.join(columnStrList, " , "));
		}
		// 指标名(单指标)
		List<Indicator> indicatorList=commonService.findIndicatorByIds(indicatorsId);
		if(evUtil.listIsNullOrZero(indicatorList)) {
			return null;
		}
		String indicator = indicatorList.get(0).getColumnName();
		map.put("indicator", indicator);
		// 如果是top 加入limit 和sort
		if (null != type) {
			if (type.equals(5)) {
				if(limitFlag) {
					map.put("limit", limit);
				}
				if (sort.equals(0)) {
					map.put("sort", "ASC");
				} else {
					map.put("sort", "DESC");
				}
			}
		}
		//获取uuid
		//数据类型 1 应用 ，2 监控点 ，3 组件 ，4IP ，5 端口 ，6事件
		//组件Id
		Integer componentId = null;
		//事件Id
		Integer customId = null;
		//应用Id
		Integer applicationId=null;
		List<Map<String, Object>> uuids=re.getUuids();
		if(evUtil.listIsNullOrZero(uuids)) {
			return null;
		}
		for (Map<String, Object> map2 : uuids) {
			if(map2.get("dataType").toString().equals("3")) {
				componentId=evUtil.getMapIntegerValue(map2, "id");
			}else if(map2.get("dataType").toString().equals("6")) {
				customId=evUtil.getMapIntegerValue(map2, "id");
			}
		}
		applicationId=evUtil.getMapIntegerValue(uuids.get(0), "applicationId");
		Integer granularity = 0;
	    if(intervalType==0) {
	    	granularity=IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval).getType();
	    }else if(intervalType==1){
	    	granularity=2;
	    }
		List<Map<String, Object>> data = new ArrayList<>();
		if (dataType.equals(2)){
			//String table = TABLE_FIX_BPM;
			map.put("componentId", componentId);
			IndicatorSupportUtil.dealIndicatorTable(3, map, granularity);
			List<Integer> typeList = new ArrayList<>();
			typeList.add(1);
			List<String> indicators = commonService.selectIndicator(typeList);
			IndicatorSupportUtil.generateIndicatorColumnStr(map, indicators, evUtil.getMapIntValue(map, "type"));
			logger.info("饼图请求参数{}", map);
			data = bpmPreMapper.getTopChartData(map);
		}
		if (dataType.equals(4)) {
			//String table = TABLE_FIX_EXTEND;
			map.put("applicationId", applicationId);
			map.put("customId", customId);
			IndicatorSupportUtil.dealIndicatorTable(4, map, granularity);
			List<Integer> typeList = new ArrayList<>();
			typeList.add(1);
			typeList.add(2);
			List<String> indicators = commonService.selectIndicator(typeList);
			IndicatorSupportUtil.generateIndicatorColumnStr(map, indicators, evUtil.getMapIntValue(map, "type"));
			logger.info("饼图请求参数{}", map);
			data = bpmPreMapper.getCustomTopChartData(map);
		}
		//根据维度值补数据
		if (!evUtil.listIsNullOrZero(columnsList)) {
			List<String> value = columnsList.get(0).getDimensionValues();
			if (!evUtil.listIsNullOrZero(value)) {
				if(sort.equals(2)) {
					List<Map<String, Object>> newList = new ArrayList<>();
					boolean flag = false;
					for (String string : value) {
						flag = false;
						for (Map<String, Object> map2 : data) {
							if (map2.get(re.getDimensions().get(0).getEname()).equals(string)) {
								flag = true;
								newList.add(map2);
								break;
							}
						}
						if (!flag) {
							Map<String, Object> e = new HashMap<>();
							e.put(re.getDimensions().get(0).getEname(), string);
							e.put(indicator, null);
							newList.add(e);
						}

					}
					data.clear();
					data.addAll(newList);
				} else {
					boolean flag = false;
					for (String string : value) {
						flag = false;
						for (Map<String, Object> map2 : data) {
							if (map2.get(re.getDimensions().get(0).getEname()).equals(string)) {
								flag = true;
								break;
							}
						}
						if (!flag) {
							Map<String, Object> e = new HashMap<>();
							e.put(re.getDimensions().get(0).getEname(), string);
							e.put(indicator, null);
							// 若是升序
							if (sort.equals(0)) {
								data.add(0, e);
							} else {
								data.add(e);
							}
						}

					}
				}
			}
		}
		//top图limit
		if (null != limit) {
			if (type.equals(5) && data.size() > limit) {
				// 取对应分页数据
				List<Map<String, Object>> subData = new ArrayList<>();
				for (int i = 0; i < limit; i++) {
					subData.add(data.get(i));
				}
				data.clear();
				data.addAll(subData);
				subData.clear();
			}
		}
		//翻译
		getTransLateData(re, columnStrList, data);
		//compare data
	    List<Map<String, Object>> compareData=new ArrayList<>();
	    List<Map<String, Object>> timeComPare = re.getTimeCompare();
	    if(!evUtil.listIsNullOrZero(timeComPare)) {
	    	timeComPare=DateUtils.generateTimeStr(timeComPare, interval);
	     	String timeCompareStr = IndicatorSupportUtil.generateTimeStr(timeComPare);
	     	map.put("timeStr", "AND (" + timeCompareStr + ")");
	     	map.put("dateStr", DateUtils.getDateStrbyTimePart(timeComPare));
	     	map.remove("indicatorStr");
	     	map.remove("limit");
	     	logger.info("智能报表top/饼状图对比查询请求参数{}",map);
	     	if (dataType.equals(2)){
	     		compareData = bpmPreMapper.getTopChartData(map);
	     	}else {
	     		compareData = bpmPreMapper.getCustomTopChartData(map);
	     	}
	    }
		//生成返回结果
		List<Map<String, Object>> list=new ArrayList<>();
		for (Map<String, Object> map2 : data) {
			Map<String, Object> resultMap=new HashMap<>();
			if (!evUtil.listIsNullOrZero(timeComPare)) {
				boolean flag = false;
				for (Map<String, Object> map3 : compareData) {
					if (map2.get(dimensionName).equals(map3.get(dimensionName))) {
						resultMap.put("valueCP", map3.get(indicator).toString());
						flag = true;
						break;
					}
				}
				if (!flag) {
					resultMap.put("valueCP", null);
				}
			}
			resultMap.put("dimensionValue", map2.get(dimensionName));
			String name=map2.get(dimensionName+"TransLate")==null?map2.get(dimensionName).toString():map2.get(dimensionName+"TransLate").toString();
			resultMap.put("name", name);
			if(null!=map2.get(indicator)) {
				resultMap.put("value", map2.get(indicator).toString());
			}else {
				resultMap.put("value", map2.get(indicator));
			}
			list.add(resultMap);
		}
		return list;
	}


	/**获取top图翻译
	 * @author CGC
	 * @param param
	 * @param groupColumnStrList
	 * @param data
	 */
	private void getTransLateData(ReportChartParam param, List<String> groupColumnStrList, List<Map<String, Object>> data) {
		Set<TranslateDataParam> translate=new HashSet<>();
		Integer dataType=param.getDataType();
	    if(!evUtil.listIsNullOrZero(param.getDimensions())){
	    	if (dataType==2) {
	    		translate.addAll(commonService.getComponentTranslate(Long.parseLong(param.getUuids().get(0).get("id").toString())));
	    	}
	    	if (dataType==4) {
	    		translate.addAll(commonService.getCustomTranslate(Long.parseLong(param.getUuids().get(0).get("id").toString())));
	    		//分析维度翻译
	    		Component c=commonService.selectComponentByCustomId(Integer.parseInt(param.getUuids().get(0).get("id").toString()));
	    		if(null!=c) {
	    			translate.addAll(commonService.getComponentTranslate(Long.parseLong(c.getId().toString())));
	    		}
	    	}
	    	//加入翻译
	    	for (Map<String, Object> map : data) {
	    		for (String str : groupColumnStrList) {
	    			for (TranslateDataParam map2 : translate) {
						if(str.equals(map2.getEname())&&map.get(str).equals(map2.getValue())) {
							map.put(str+"TransLate", map2.getMapping());
							break;
						}
					}
				}
			}
	    }
	}

	/**
	 * 处理筛选维度
	 *
	 * @param conditionStr
	 * @param ename
	 * @param rule
	 * @param value
	 */
	private void handleFilterDimension(List<String> conditionStr, String ename, int rule, List<String> value) {
		// 等于 不等于
		if (rule == 1 || rule == 2) {
			String str = "'" + StringUtils.join(value, "' , '") + "'";
			str = ename + IndicatorSupportUtil.translateDimensionRule(rule) + "(" + str + ")";
			conditionStr.add(str);
		}
		// 包含
		if (rule == 3) {
			String str = "";
			for (String string : value) {
				if (str.equals("")) {
					str = ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'%" + string + "%'";
				} else {
					str = str + " OR " + ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'%" + string
							+ "%'";
				}
			}
			conditionStr.add("(" + str + ")");
		}
		// 不包含
		if (rule == 4) {
			String str = "";
			for (String string : value) {
				if (str.equals("")) {
					str = ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'%" + string + "%'";
				} else {
					str = str + " AND " + ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'%" + string
							+ "%'";
				}
			}
			conditionStr.add("(" + str + ")");
		}
		// 开头包含
		if (rule == 5) {
			String str = "";
			for (String string : value) {
				if (str.equals("")) {
					str = ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'" + string + "%'";
				} else {
					str = str + " OR " + ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'" + string
							+ "%'";
				}
			}
			conditionStr.add("(" + str + ")");
		}
		// 结尾包含
		if (rule == 6) {
			String str = "";
			for (String string : value) {
				if (str.equals("")) {
					str = ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'%" + string + "'";
				} else {
					str = str + " OR " + ename + IndicatorSupportUtil.translateDimensionRule(rule) + "'%" + string
							+ "'";
				}
			}
			conditionStr.add("(" + str + ")");
		}
		// 为空 不为空
		if (rule == 7 || rule == 8) {
			String str = ename + IndicatorSupportUtil.translateDimensionRule(rule);
			conditionStr.add(str);
		}
	}


	@Override
	public Page<Map<String, Object>> selectReportTablePage(Page<Map<String, Object>> page, ReportTableParam param) {
		Integer interval= param.getInterval();
		Integer intervalType=param.getIntervalType()==null?0:param.getIntervalType();
		if(intervalType==1) {
			interval=5*60;
		}
		Map<String, Object> paramMap = new HashMap<>();
		//智能报表查询标志
		paramMap.put("isReport", 1);
		// paramMap.put("start", param.getStartTime());
		// paramMap.put("end", param.getEndTime());
		// 获取查询时间
		String timeStr;
		List<Map<String, Object>> timePart = param.getTimePart();
		if(!evUtil.listIsNullOrZero(timePart)) {
			timePart=DateUtils.generateTimeStr(timePart, interval);
			timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
			paramMap.put("timeStr", "AND (" + timeStr + ")");
			paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		}else {
			return null;
		}
		//指标筛选
		List<JSONObject> filterIndicator = param.getFilterIndicator();
		List<JSONObject> filterList =new ArrayList<>();
		List<JSONObject> filterListCP =new ArrayList<>();
		if(!evUtil.listIsNullOrZero(filterIndicator)){
			for (JSONObject jsonObject : filterIndicator) {
				Integer isCompare=jsonObject.getIntValue("isCompare");
				if(isCompare.equals(0)) {
					filterList.add(jsonObject);
				}else {
					filterListCP.add(jsonObject);
				}
			}
			if(!evUtil.listIsNullOrZero(filterList)){
			paramMap.put("indicatorCondition", " ("+ IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterList)+") ");
			}		
		}
		// handle dimensionConditions (包含ename/dimensionType)
		List<ReportDimensionParam> filterDimensionIds = param.getRules();
		List<String> conditionStr = new ArrayList<>();
		String dimensionConditionStr;
	    if(!evUtil.listIsNullOrZero(filterDimensionIds)){
			for (ReportDimensionParam jsonObject : filterDimensionIds) {
				String ename = jsonObject.getEname();
				Integer rule = jsonObject.getRule()==null?0:jsonObject.getRule();
				List<String> value = (List<String>) jsonObject.getDimensionValues();
				if (rule.equals(7) || rule.equals(8)) {
					handleFilterDimension(conditionStr, ename, rule, value);
				} else {
					if (!evUtil.listIsNullOrZero(value)) {
						handleFilterDimension(conditionStr, ename, rule, value);
					}
				}
			}
	    }


	    //handle dimension columns
	    List<String> columnStrList = new ArrayList<>();
	    List<String> groupColumnStrList = new ArrayList<>();
	    List<ReportDimensionParam> columnsList= param.getDimensions();
		if (!evUtil.listIsNullOrZero(columnsList)) {
			for (ReportDimensionParam jsonObject : columnsList) {
				String ename = jsonObject.getEname();
				columnStrList.add(ename);
				groupColumnStrList.add(ename);
				Integer rule = jsonObject.getRule()==null?0:jsonObject.getRule();
				List<String> value = (List<String>) jsonObject.getDimensionValues();
				if (rule.equals(7) || rule.equals(8)) {
					handleFilterDimension(conditionStr, ename, rule, value);
				} else {
					if (!evUtil.listIsNullOrZero(value)) {
						handleFilterDimension(conditionStr, ename, rule, value);
					}
				}
			}
		}
		if (!evUtil.listIsNullOrZero(conditionStr)) {
			dimensionConditionStr = StringUtils.join(conditionStr, " AND ");
			paramMap.put("dimensionCondition", " AND " + dimensionConditionStr);
		}
	    //是否勾选时间维度 是否时间维度 0 否 1是
	    if(param.getIsTimeDimension().equals(1)) {
	    	if(intervalType==1) {
	    		columnStrList.add("date");
	    		groupColumnStrList.add("date");
	    	}else {
	    		columnStrList.add("time");
	    		groupColumnStrList.add("time");
	    	}
	    }


	    if(!evUtil.listIsNullOrZero(columnStrList)){
	        paramMap.put("dimensionColumns",StringUtils.join(columnStrList, " , "));
	    }

	    //排序条件
	    String orderBy=null;
	  //判断排序字段是对比还是原字段
	    boolean orderCp=false;
	    if(null!=param.getOrder()) {
	    	String order=param.getOrder().getString("ename");
	    	Integer orderType=param.getOrder().getIntValue("orderType");
	    	if(orderType>2) {
	    		orderCp=true;
	    		paramMap.put("orderBy", null);
	    	}else {
	    	if(intervalType==1&&orderType==0) {
	    		order="date";
	    	}
	    	String asc=param.getOrder().getInteger("sortType")==0?" ASC":" DESC";
	    	orderBy=order+asc;
	    	paramMap.put("orderBy", orderBy);
	    	}
	    }else {
	    	paramMap.put("orderBy", null);
	    }

	    //table
	    //查的原表
	    List<Map<String, Object>> data = new ArrayList<>();
	    int granularity = 0;
	    if(intervalType==0) {
	    	granularity=IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval).getType();
	    }else if(intervalType==1){
	    	granularity=2;
	    }
	    //总记录数
	    int total=0;
	    if (param.getDataType().intValue()==2) {
	    	paramMap.put("componentId", param.getUuids().get(0));
	    	IndicatorSupportUtil.dealIndicatorTable(3, paramMap, granularity);
	    	List<Integer> type = new ArrayList<>();
			type.add(1);
			List<String> indicators = commonService.selectIndicator(type);
			IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
	    	logger.info("智能报表表格查询请求参数{}",paramMap);
	        if(!orderCp) {//根据原数据排序要分页
	        	data = bpmPreMapper.selectListByIndicators(page,paramMap);
	        	total = bpmPreMapper.getMultidimensionCount(paramMap);
	        }else {
	        	data = bpmPreMapper.selectListByIndicators(paramMap);
	        	total=data.size();
	        }
		}

	    if (param.getDataType().intValue()==4) {
	    	if(!evUtil.listIsNullOrZero(groupColumnStrList)){
	            paramMap.put("groupDimensionColumns",StringUtils.join(groupColumnStrList, " , "));
	        }
	    	paramMap.put("applicationId", param.getApplicationId());
	    	paramMap.put("customId", param.getUuids().get(0));
	    	IndicatorSupportUtil.dealIndicatorTable(4, paramMap, granularity);
	    	List<Integer> type = new ArrayList<>();
			type.add(1);
			type.add(2);
			List<String> indicators = commonService.selectIndicator(type);
			IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
	    	logger.info("智能报表表格查询请求参数{}",paramMap);
	        if(!orderCp) {
	        	data = bpmPreMapper.selectCustomListByIndicators(page,paramMap);
	        	total = bpmPreMapper.getMultidimensionCustomCount(paramMap);
	        }else {
	        	data = bpmPreMapper.selectCustomListByIndicators(paramMap);
	        	total=data.size();
	        }
		}
	    if(intervalType==1&&param.getIsTimeDimension().equals(1)) {
        	if(!evUtil.listIsNullOrZero(data)) {
        	for (Map<String, Object> map : data) {
				String date=evUtil.getMapStr(map, "date");
				map.put("time", DateUtils.getDateLong(date));
			}
        	}
        }
	    
	    //compare data
	    List<Map<String, Object>> compareData=new ArrayList<>();
	    //Long compareStart =param.getCompareStartTime();
    	//Long compareEnd =param.getCompareEndTime();
	    List<Map<String, Object>> timeComPare = param.getTimeCompare();
	    Long compareInterval=0L;
	    if(!evUtil.listIsNullOrZero(timeComPare)) {
	    	//计算对比时间间隔
	    	compareInterval=IndicatorSupportUtil.getCompareInterval(timePart, timeComPare);
	    	//加入date分区条件
	        //List<String> compareDateStrList = DateUtils.getDateStrByStartAndEnd(compareStart, compareEnd);
	    	timeComPare=DateUtils.generateTimeStr(timeComPare, interval);
	    	String timeCompareStr = IndicatorSupportUtil.generateTimeStr(timeComPare);
			paramMap.put("timeStr", "AND (" + timeCompareStr + ")");
			paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timeComPare));
			paramMap.remove("indicatorCondition");
	        //paramMap.put("dateStr", compareDateStrList);
	    	//paramMap.put("start", compareStart);
	        //paramMap.put("end", compareEnd);
	        logger.info("智能报表表格对比查询请求参数{}",paramMap);
	        if (param.getDataType().intValue()==2) {
	        /*	if(orderCp) {
	        		compareData= bpmPreMapper.selectListByIndicators(page,paramMap);
	        	}else {*/
	        		compareData= bpmPreMapper.selectListByIndicators(paramMap);
	        	/*}*/
	        }else {
	        	/*if(orderCp) {
	        		compareData= bpmPreMapper.selectCustomListByIndicators(page,paramMap);
	        	}else {*/
	        		compareData= bpmPreMapper.selectCustomListByIndicators(paramMap);
	        	/*}*/
	        }
			// 广发特殊逻辑
			if (intervalType == 1 && param.getIsTimeDimension().equals(1)) {
				if (!evUtil.listIsNullOrZero(compareData)) {
					for (Map<String, Object> map : compareData) {
						String date = evUtil.getMapStr(map, "date");
						map.put("time", DateUtils.getDateLong(date));
						groupColumnStrList.add("time");
						groupColumnStrList.remove("date");
					}
				}
			}
	    }
	    Integer compare=param.getCompare()==null?0:param.getCompare();
		if (compare != 0) {
			combinData(param, groupColumnStrList, data, compareData, compareInterval);
			// 计算对比
			getCompareDiffAndper(param, data);
			// 对比指标筛选
			if (!evUtil.listIsNullOrZero(filterListCP)) {
				getCompareFilter(filterListCP, data);
				total = data.size();
			}
			// 若是对比排序，需要处理
			if (orderCp) {
				String order = param.getOrder().getString("ename");
				Integer asc = param.getOrder().getInteger("sortType");
				// 根据相应维度排序0
				Collections.sort(data, new Comparator<Map<String, Object>>() {
					public int compare(Map<String, Object> o1, Map<String, Object> o2) {
						Double name1 = o1.get(order) == null ? -1
								: Math.abs(Double.parseDouble(o1.get(order).toString()));// name1是从你list里面拿出来的一个
						Double name2 = o2.get(order) == null ? -1
								: Math.abs(Double.parseDouble(o2.get(order).toString())); // name1是从你list里面拿出来的第二个name
						if (asc.equals(0)) {
							return name1.compareTo(name2);
						} else {
							return 0 - name1.compareTo(name2);
						}
					}
				});
				// 取对应分页数据
				List<Map<String, Object>> subData = new ArrayList<>();
				Integer pageNumber = param.getPage();
				Integer pageSize = param.getSize();
				// 从第几条开始
				int offset = (pageNumber - 1) * pageSize;
				int size = 0;
				for (int i = offset; i < data.size(); i++) {
					subData.add(data.get(i));
					size++;
					if (size == pageSize) {
						break;
					}
				}
				data.clear();
				data.addAll(subData);
				subData.clear();
			}
		}
	    // tranlate
		groupColumnStrList.remove("time");
	    getTransLateData(param, groupColumnStrList, data);
		page.setTotal(total);
	    page.setRecords(data);
	    return page;
}

	private void getCompareFilter(List<JSONObject> filterListCP, List<Map<String, Object>> data) {
		//先复制一份
		List<Map<String, Object>> datacopy=new ArrayList<>();
		datacopy.addAll(data);
		for (Map<String, Object> dataMap : datacopy) {
		    for (JSONObject jsob : filterListCP) {
		    	String ename = jsob.getString("ename");
		        int rule=jsob.getIntValue("rule");
		        List<String> value=(List<String>) jsob.get("value");
		        double v2=Double.parseDouble(value.get(0));
		        double v=Math.abs(evUtil.getMapDoubleValue(dataMap, ename));
		        if(rule==1) {
		        	if(!(v>v2)) {
		        		data.remove(dataMap);
		        		break;
		        	}
		        }else if(rule==2) {
		        	if(!(v<v2)) {
		        		data.remove(dataMap);
		        		break;
		        	}
		        }
		        
			}
		}
	}


	/**
	 * 报表添加翻译
	 * @author CGC
	 * @param param
	 * @param groupColumnStrList
	 * @param data
	 */
	private void getTransLateData(ReportTableParam param, List<String> groupColumnStrList,
			List<Map<String, Object>> data) {
		Set<TranslateDataParam> translate=new HashSet<>();
	    if(!evUtil.listIsNullOrZero(param.getDimensions())){
	    	if (param.getDataType().intValue()==2) {
	    		translate.addAll(commonService.getComponentTranslate(Long.parseLong(param.getUuids().get(0).toString())));
	    	}
	    	if (param.getDataType().intValue()==4) {
	    		translate.addAll(commonService.getCustomTranslate(Long.parseLong(param.getUuids().get(0).toString())));
	    		//分析维度翻译
	    		Component c=commonService.selectComponentByCustomId(param.getUuids().get(0));
	    		if(null!=c) {
	    			translate.addAll(commonService.getComponentTranslate(Long.parseLong(c.getId().toString())));
	    		}
	    	}
	    	//加入翻译
	    	for (Map<String, Object> map : data) {
	    		for (String str : groupColumnStrList) {
	    			boolean flag=false;
	    			for (TranslateDataParam map2 : translate) {
						if(str.equals(map2.getEname())&&map.get(str).equals(map2.getValue())) {
							map.put(str+"Translate", map2.getMapping());
							flag=true;
							break;
						}
					}
	    			if(!flag) {
	    				map.put(str+"Translate", null);
	    			}
				}
			}
	    }
	}


	/**计算对比差值和百分比
	 * @author CGC
	 * @param param
	 * @param data
	 */
	private void getCompareDiffAndper(ReportTableParam param, List<Map<String, Object>> data) {
		//添加指标需修改
		for (Map<String, Object> map2 : data) {
			Double transCountPER = null,responseTimePER = null,responseRatePER = null,successRatePER = null,responseTimeDIFF = null,responseRateDIFF = null,successRateDIFF = null;
			Long transCountDIFF = null;
			Long allTransCount=evUtil.getMapLong(map2, "allTransCount");
			Long allTransCountCP=evUtil.getMapLong(map2, "allTransCountCP");
			if(null!=allTransCountCP) {
				transCountDIFF=allTransCount-allTransCountCP;
				transCountPER=evUtil.doubleBitUp((1.0*transCountDIFF/allTransCountCP)*100,2);
			}
			Double responseTimeCP=evUtil.getMapDouble(map2, "responseTimeCP");
			Double responseTime=evUtil.getMapDouble(map2, "responseTime");
			if(null!=responseTimeCP&&!responseTimeCP.equals(0.0)&&null!=responseTime) {
				responseTimeDIFF=evUtil.sub(responseTime, responseTimeCP);
				responseTimePER=evUtil.doubleBitUp((responseTimeDIFF/responseTimeCP)*100,2);
			}
			Double responseRateCP=evUtil.getMapDouble(map2, "responseRateCP");
			Double responseRate=evUtil.getMapDouble(map2, "responseRate");
			if(null!=responseRateCP&&!responseRateCP.equals(0.0)&&null!=responseRate) {
				responseRateDIFF=evUtil.sub(responseRate, responseRateCP);
				responseRatePER=evUtil.doubleBitUp((responseRateDIFF/responseRateCP)*100,2);
			}
			Double successRateCP=evUtil.getMapDouble(map2, "successRateCP");
			Double successRate=evUtil.getMapDouble(map2, "successRate");
			if(null!=successRateCP&&!successRateCP.equals(0.0)&&null!=successRate) {
				successRateDIFF=evUtil.sub(successRate, successRateCP);
				successRatePER=evUtil.doubleBitUp((successRateDIFF/successRateCP)*100,2);
			}
			map2.put("allTransCountDIFF", transCountDIFF);
			map2.put("allTransCountPER", transCountPER);
			map2.put("responseTimeDIFF", responseTimeDIFF);
			map2.put("responseTimePER", responseTimePER);
			map2.put("responseRateDIFF", responseRateDIFF);
			map2.put("responseRatePER", responseRatePER);
			map2.put("successRateDIFF", successRateDIFF);
			map2.put("successRatePER", successRatePER);
			if (param.getDataType().intValue() == 4) {
				Double moneyPER=null,moneyDIFF=null,businessSuccessRatePER=null,businessSuccessRateDIFF=null;
				Double moneyCP=evUtil.getMapDouble(map2, "moneyCP");
				Double money=evUtil.getMapDouble(map2, "money");
				if(null!=moneyCP&&!moneyCP.equals(0.0)) {
					moneyDIFF=evUtil.sub(money, moneyCP);
					moneyPER=evUtil.doubleBitUp((moneyDIFF/moneyCP)*100,2);
				}
				Double businessSuccessRateCP=evUtil.getMapDouble(map2, "businessSuccessRateCP");
				Double businessSuccessRate=evUtil.getMapDouble(map2, "businessSuccessRate");
				if(null!=businessSuccessRateCP&&!businessSuccessRateCP.equals(0.0)&&null!=businessSuccessRate) {
					businessSuccessRateDIFF=evUtil.sub(businessSuccessRate, businessSuccessRateCP);
					businessSuccessRatePER=evUtil.doubleBitUp((businessSuccessRateDIFF/businessSuccessRateCP)*100,2);
				}
				map2.put("moneyPER", moneyPER);
				map2.put("moneyDIFF", moneyDIFF);
				map2.put("businessSuccessRatePER", businessSuccessRatePER);
				map2.put("businessSuccessRateDIFF", businessSuccessRateDIFF);
			}
		}
	}


	@Override
	public ReportDataChartParam getDataChart(ReportChartParam re) {
		ReportDataChartParam chart=new ReportDataChartParam();
		DimensionParam param=new DimensionParam();
		List<Long> timeList=new ArrayList<>();
		List<String> valueList=new ArrayList<>();
		String sumCompare = null;
		String sum = null;
		param.setUuid(Long.parseLong(re.getUuids().get(0).get("id").toString()));
		List<JSONObject> filterDimension=new ArrayList<>();
		List<ReportDimensionParam> rule=re.getRules();
		if(!evUtil.listIsNullOrZero(rule)) {
			for (ReportDimensionParam reportDimensionParam : rule) {
				filterDimension.add((JSONObject) JSONObject.toJSON(reportDimensionParam));
			}
		}
		List<ReportDimensionParam> dimension=re.getDimensions();
		if(!evUtil.listIsNullOrZero(dimension)) {
			for (ReportDimensionParam reportDimensionParam : dimension) {
				filterDimension.add((JSONObject) JSONObject.toJSON(reportDimensionParam));
			}
		}
		param.setApplicationId((Integer) re.getUuids().get(0).get("applicationId"));
		param.setIndicatorIds(re.getIndicators());
		param.setFilterDimension(filterDimension);
		param.setType(re.getDataType());
		List<Map<String, Object>> time= re.getTimePart();
		Integer interval = re.getInterval();
		Integer intervalType=re.getIntervalType()==null?0:re.getIntervalType();
		//证券特殊逻辑
		if (intervalType == 1) {
			interval = 5 * 60;
		}
		for (Map<String, Object> map : time) {
			List<Long> timeP= (List<Long>) map.get("time");
			IndicatorIntervalParam indicatorIntervalParam=IndicatorSupportUtil.getIndicatorIntervalByInterVal(timeP.get(0), timeP.get(1), interval);
	    	Integer granularity=indicatorIntervalParam.getType();
	    	param.setStartTime(indicatorIntervalParam.getStart());
			param.setEndTime(indicatorIntervalParam.getEnd());
	    	Map<String, Object> result=getDataChart(param, interval,intervalType, granularity);
	    	List<Long> timePart= (List<Long>) result.get("time");
			timeList.addAll(timePart);
			List<Map<String, Object>> indicatorResList=(List<Map<String, Object>>) result.get("indicator");
			List<String> value=(List<String>) indicatorResList.get(0).get("value");
			valueList.addAll(value);
		}
		//计算当前值
		List<Map<String, Object>> list= getTopChartData(re);
		if (!evUtil.listIsNullOrZero(list)) {
			if (null != list.get(0).get("value")) {
				sum = list.get(0).get("value").toString();
				List<Map<String, Object>> timeCompare= re.getTimeCompare();
				if (!evUtil.listIsNullOrZero(timeCompare)) {
					if (null != list.get(0).get("valueCP")) {
						sumCompare = list.get(0).get("valueCP").toString();
					}
				}
			}
		}
		chart.setSum(sum);
		chart.setSumCompare(sumCompare);
		chart.setTime(timeList);
		chart.setValue(valueList);
		return chart;
	}
	public Map<String, Object> getDataChart(DimensionParam param, int interval,int intervalType,int type) {
		List<Integer> indicatorIds =param.getIndicatorIds();
        if(evUtil.listIsNullOrZero(indicatorIds)) {
        	return null;
        }
        Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream().collect(Collectors.toMap(Indicator::getId,indicator->indicator));
        List<String> enameList=new ArrayList<>();
		for(Entry<Integer, Indicator> ename:indicatorMap.entrySet()) {
			enameList.add(ename.getValue().getColumnName());
		}
        Long start = param.getStartTime();
		Long end = param.getEndTime();
		List<Long> timeList=new ArrayList<>();
		if(intervalType==1) {
			timeList.add(DateUtils.longToDayTime(start));
		}else {
			timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval * 1000);
		}
		Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("start", start);
        paramMap.put("end", end);
        List<JSONObject> filterDimensionIds = param.getFilterDimension();
        if(!evUtil.listIsNullOrZero(filterDimensionIds)){
        	/*for (JSONObject jsonObject : filterDimensionIds) {
        		columnStrList.add(jsonObject.getString("ename"));
			}*/
            List<String> conditionStr = new ArrayList<>();
            String dimensionCondition;
            for (JSONObject jsonObject : filterDimensionIds) {
				String ename = jsonObject.getString("ename");
				int rule = jsonObject.getIntValue("rule");
				List<String> value = (List<String>) jsonObject.get("dimensionValues");
				handleFilterDimension(conditionStr, ename, rule, value);
			}
            if(!evUtil.listIsNullOrZero(conditionStr)){
            	dimensionCondition = StringUtils.join(conditionStr, " AND ");
                paramMap.put("dimensionCondition", " AND " + dimensionCondition);
            }
        }


        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(param.getStartTime(), param.getEndTime());
        paramMap.put("dateStr", dateStrList);
        IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, enameList, type);
        if(intervalType==1) {
        	paramMap.put("isSecurities", 1);
		}
        List<Map<String, Object>> data = new ArrayList<>();
        if (param.getType().intValue()==2) {
        	String table=null;
        	if(type==0) {
        		table = TABLE_FIX_BPM;
        	}
        	else if(type==2) {
        		table = VIEW_VM_BPM +"_5";
        	}
        	else if(type==1) {
        		table = VIEW_VM_BPM;
        	}
        	else if(type==3) {
        		table = VIEW_VM_BPM+"_d";
        	}
        	else {
        		table = VIEW_VM_BPM;
        	}
        	paramMap.put("type", type);
        	paramMap.put("componentId", param.getUuid());
            paramMap.put("table", table);
            logger.info("数值图请求参数：{}", paramMap);
            data = bpmPreMapper.getMultiChat(paramMap);
		}
        if (param.getType().intValue()==4) {
        	String table = null;
        	if(type==0) {
        		table = TABLE_FIX_EXTEND;
        	}
        	else if(type==2) {
        		table = VIEW_VM_EXTEND+"_5";
        	}
        	else if(type==1){
        		table = VIEW_VM_EXTEND;
        	}
        	else if(type==3){
        		table = VIEW_VM_EXTEND+"_d";
        	}else {
        		table = TABLE_FIX_EXTEND;
        	}
        	paramMap.put("applicationId", param.getApplicationId());
        	paramMap.put("customId", param.getUuid());
            paramMap.put("table", table);
            paramMap.put("type", type);
            logger.info("数值图请求参数：{}", paramMap);
            data = bpmPreMapper.getCustomMultiChat(paramMap);
		}
        
        if (intervalType == 1) {
			if (!evUtil.listIsNullOrZero(data)) {
				for (Map<String, Object> map : data) {
					String date = evUtil.getMapStr(map, "date");
					map.put("time", DateUtils.getDateLong(date));
				}
			}
		}
        data=IndicatorSupportUtil.getForMatList(data,timeList);
        Map<String, Object> resultMap=new HashMap<>();
        resultMap.put("time", timeList);
        if(!evUtil.listIsNullOrZero(indicatorIds)) {
        	List<Map<String, Object>> indicatorResList=new ArrayList<>();
        	for (Map.Entry<Integer, Indicator> entry : indicatorMap.entrySet()) {
        		Map<String, Object> indicatorResMap=new HashMap<>();
        		indicatorResMap.put("id", entry.getKey());
        		List<String> value=new ArrayList<>();
        		String name=indicatorMap.get(entry.getKey()).getColumnName();
    			/*if(name.equals("allTransCount")) {
            		name="transCount";
            	}*/
        		for (Map<String, Object> map : data) {
        			value.add(map.get(name)==null?null:map.get(name).toString());
        		}
        		indicatorResMap.put("value", value);
        		indicatorResList.add(indicatorResMap);
        	}
        	resultMap.put("indicator", indicatorResList);
        }
		return resultMap;
	}


	@Override
	public List<Map<String, Object>> selectReportTable(ReportTableParam param) {
		
		Integer interval= param.getInterval();
		Integer intervalType=param.getIntervalType()==null?0:param.getIntervalType();
		if(intervalType==1) {
			interval=5*60;
		}
		Map<String, Object> paramMap = new HashMap<>();
		//智能报表查询标志
		paramMap.put("isReport", 1);
		// paramMap.put("start", param.getStartTime());
		// paramMap.put("end", param.getEndTime());
		// 获取查询时间
		String timeStr;
		List<Map<String, Object>> timePart = param.getTimePart();
		if(!evUtil.listIsNullOrZero(timePart)) {
			timePart=DateUtils.generateTimeStr(timePart, interval);
			timeStr = IndicatorSupportUtil.generateTimeStr(timePart);
			paramMap.put("timeStr", "AND (" + timeStr + ")");
			paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timePart));
		}else {
			return null;
		}
		//指标筛选
		List<JSONObject> filterIndicator = param.getFilterIndicator();
		List<JSONObject> filterList =new ArrayList<>();
		List<JSONObject> filterListCP =new ArrayList<>();
		if(!evUtil.listIsNullOrZero(filterIndicator)){
			for (JSONObject jsonObject : filterIndicator) {
				Integer isCompare=jsonObject.getIntValue("isCompare");
				if(isCompare.equals(0)) {
					filterList.add(jsonObject);
				}else {
					filterListCP.add(jsonObject);
				}
			}
			if(!evUtil.listIsNullOrZero(filterList)){
			paramMap.put("indicatorCondition", " ("+ IndicatorSupportUtil.generateReportIndicatorFilterSqlStr(filterList)+") ");
			}		
		}
		// handle dimensionConditions (包含ename/dimensionType)
		List<ReportDimensionParam> filterDimensionIds = param.getRules();
		List<String> conditionStr = new ArrayList<>();
		String dimensionConditionStr;
	    if(!evUtil.listIsNullOrZero(filterDimensionIds)){
			for (ReportDimensionParam jsonObject : filterDimensionIds) {
				String ename = jsonObject.getEname();
				Integer rule = jsonObject.getRule()==null?0:jsonObject.getRule();
				List<String> value = (List<String>) jsonObject.getDimensionValues();
				if (rule.equals(7) || rule.equals(8)) {
					handleFilterDimension(conditionStr, ename, rule, value);
				} else {
					if (!evUtil.listIsNullOrZero(value)) {
						handleFilterDimension(conditionStr, ename, rule, value);
					}
				}
			}
	    }


	    //handle dimension columns
	    List<String> columnStrList = new ArrayList<>();
	    List<String> groupColumnStrList = new ArrayList<>();
	    List<ReportDimensionParam> columnsList= param.getDimensions();
		if (!evUtil.listIsNullOrZero(columnsList)) {
			for (ReportDimensionParam jsonObject : columnsList) {
				String ename = jsonObject.getEname();
				columnStrList.add(ename);
				groupColumnStrList.add(ename);
				Integer rule = jsonObject.getRule()==null?0:jsonObject.getRule();
				List<String> value = (List<String>) jsonObject.getDimensionValues();
				if (rule.equals(7) || rule.equals(8)) {
					handleFilterDimension(conditionStr, ename, rule, value);
				} else {
				if (!evUtil.listIsNullOrZero(value)) {
					handleFilterDimension(conditionStr, ename, rule, value);
				}
				}
			}
		}
		if (!evUtil.listIsNullOrZero(conditionStr)) {
			dimensionConditionStr = StringUtils.join(conditionStr, " AND ");
			paramMap.put("dimensionCondition", " AND " + dimensionConditionStr);
		}
		//是否勾选时间维度 是否时间维度 0 否 1是
	    if(param.getIsTimeDimension().equals(1)) {
	    	if(intervalType==1) {
	    		columnStrList.add("date");
	    		groupColumnStrList.add("date");
	    	}else {
	    		columnStrList.add("time");
	    		groupColumnStrList.add("time");
	    	}
	    }



	    if(!evUtil.listIsNullOrZero(columnStrList)){
	        paramMap.put("dimensionColumns",StringUtils.join(columnStrList, " , "));
	    }
	    //加入date分区条件
	    //List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(param.getStartTime(), param.getEndTime());
	    //paramMap.put("dateStr", dateStrList);

	    //排序条件
	    String orderBy=null;
	    //判断排序字段是对比还是原字段
	    boolean orderCp=false;
	    if(null!=param.getOrder()) {
	    	String order=param.getOrder().getString("ename");
	    	Integer orderType=param.getOrder().getIntValue("orderType");
	    	if(orderType>2) {
	    		orderCp=true;
	    		paramMap.put("orderBy", null);
	    	}else {
	    	if(intervalType==1&&orderType==0) {
	    		order="date";
	    	}
	    	String asc=param.getOrder().getInteger("sortType")==0?" ASC":" DESC";
	    	orderBy=order+asc;
	    	paramMap.put("orderBy", orderBy);
	    	paramMap.put("limit", param.getLimit());
	    	}
	    }else {
	    	paramMap.put("orderBy", null);
	    }
	    
	    //table
	    //查的原表
	    List<Map<String, Object>> data = new ArrayList<>();
	    //总记录数
	    int granularity = 0;
	    if(intervalType==0) {
	    	granularity=IndicatorSupportUtil.getIndicatorIntervalByInterVal(interval).getType();
	    }else if(intervalType==1){
	    	granularity=2;
	    }
	    if (param.getDataType().intValue()==2) {
	    	paramMap.put("componentId", param.getUuids().get(0));
	    	IndicatorSupportUtil.dealIndicatorTable(3, paramMap, granularity);
	    	List<Integer> type = new ArrayList<>();
			type.add(1);
			List<String> indicators = commonService.selectIndicator(type);
			IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
	    	logger.info("智能报表表格查询请求参数{}",paramMap);
	    	data = bpmPreMapper.selectListByIndicators(paramMap);
		}

	    if (param.getDataType().intValue()==4) {
	    	if(!evUtil.listIsNullOrZero(groupColumnStrList)){
	            paramMap.put("groupDimensionColumns",StringUtils.join(groupColumnStrList, " , "));
	        }
	    	paramMap.put("applicationId", param.getApplicationId());
	    	paramMap.put("customId", param.getUuids().get(0));
	    	IndicatorSupportUtil.dealIndicatorTable(4, paramMap, granularity);
	    	List<Integer> type = new ArrayList<>();
			type.add(1);
			type.add(2);
			List<String> indicators = commonService.selectIndicator(type);
			IndicatorSupportUtil.generateIndicatorColumnStr(paramMap, indicators, evUtil.getMapIntValue(paramMap, "type"));
	    	logger.info("智能报表表格查询请求参数{}",paramMap);
	    	data = bpmPreMapper.selectCustomListByIndicators(paramMap);
		}
	    if(intervalType==1&&param.getIsTimeDimension().equals(1)) {
        	if(!evUtil.listIsNullOrZero(data)) {
        	for (Map<String, Object> map : data) {
				String date=evUtil.getMapStr(map, "date");
				map.put("time", DateUtils.getDateLong(date));
			}
        	}
        }
	    //compare data
	    List<Map<String, Object>> compareData=new ArrayList<>();
	    //Long compareStart =param.getCompareStartTime();
    	//Long compareEnd =param.getCompareEndTime();
	    List<Map<String, Object>> timeComPare = param.getTimeCompare();
	    Long compareInterval=0L;
	    if(!evUtil.listIsNullOrZero(timeComPare)) {
	    	//计算对比时间间隔
	    	compareInterval=IndicatorSupportUtil.getCompareInterval(timePart, timeComPare);
	    	//加入date分区条件
	        //List<String> compareDateStrList = DateUtils.getDateStrByStartAndEnd(compareStart, compareEnd);
	    	timeComPare=DateUtils.generateTimeStr(timeComPare, interval);
	    	String timeCompareStr = IndicatorSupportUtil.generateTimeStr(timeComPare);
			paramMap.put("timeStr", "AND (" + timeCompareStr + ")");
			paramMap.put("dateStr", DateUtils.getDateStrbyTimePart(timeComPare));
			paramMap.remove("indicatorCondition");
			paramMap.remove("limit");
	        //paramMap.put("dateStr", compareDateStrList);
	    	//paramMap.put("start", compareStart);
	        //paramMap.put("end", compareEnd);
	        logger.info("智能报表表格对比查询请求参数{}",paramMap);
	        if (param.getDataType().intValue()==2) {
	        /*	if(orderCp) {
	        		compareData= bpmPreMapper.selectListByIndicators(page,paramMap);
	        	}else {*/
	        		compareData= bpmPreMapper.selectListByIndicators(paramMap);
	        	/*}*/
	        }else {
	        	/*if(orderCp) {
	        		compareData= bpmPreMapper.selectCustomListByIndicators(page,paramMap);
	        	}else {*/
	        		compareData= bpmPreMapper.selectCustomListByIndicators(paramMap);
	        	/*}*/
	        }
	        //广发特殊逻辑
	        if(intervalType==1&&param.getIsTimeDimension().equals(1)) {
	        	if(!evUtil.listIsNullOrZero(compareData)) {
	        	for (Map<String, Object> map : compareData) {
					String date=evUtil.getMapStr(map, "date");
					map.put("time", DateUtils.getDateLong(date));
					groupColumnStrList.add("time");
					groupColumnStrList.remove("date");
				}
	        	}
	        }
	    }

	    Integer compare=param.getCompare()==null?0:param.getCompare();
		if (compare != 0) {
			// combin result
			// 查询条件有维度 根据维度匹配加入
			combinData(param, groupColumnStrList, data, compareData, compareInterval);
			// 计算对比
			getCompareDiffAndper(param, data);
			// 对比指标筛选
			if (!evUtil.listIsNullOrZero(filterListCP)) {
				getCompareFilter(filterListCP, data);
			}
			// 若是对比排序，需要处理
			if (orderCp) {
				dealOrderCpData(param, data);
			}
		}
	    // tranlate
		groupColumnStrList.remove("time");
	  	getTransLateData(param, groupColumnStrList, data);
	    return data;
}

	private void dealOrderCpData(ReportTableParam param, List<Map<String, Object>> data) {
		String order=param.getOrder().getString("ename");
		Integer asc=param.getOrder().getInteger("sortType");
		//根据相应维度排序0
		Collections.sort(data, new Comparator<Map<String, Object>>() {
		    public int compare(Map<String, Object> o1, Map<String, Object> o2) {
		    	Double name1 =o1.get(order)==null?-1:Math.abs(Double.parseDouble(o1.get(order).toString()));//name1是从你list里面拿出来的一个
		        Double name2 =o2.get(order)==null?-1:Math.abs(Double.parseDouble(o2.get(order).toString())); //name1是从你list里面拿出来的第二个name
		        if(asc.equals(0)) {
		        	return name1.compareTo(name2);
		        }else {
		        	return 0-name1.compareTo(name2);
		        }
		    }
		});
		//取对应分页数据
		List<Map<String, Object>> subData=new ArrayList<>();
		if(null != param.getLimit()){
			int size=param.getLimit()>data.size()?data.size():param.getLimit();
			for (int i = 0; i < size; i++) {
				subData.add(data.get(i));
			}
			data.clear();
			data.addAll(subData);
			subData.clear();
		}
	}

	private void combinData(ReportTableParam param, List<String> groupColumnStrList, List<Map<String, Object>> data,
			List<Map<String, Object>> compareData, Long compareInterval) {
		for (Map<String, Object> map : data) {
			boolean flag = false;

			for (Map<String, Object> compareMap : compareData) {
				boolean judge = true;
				// 勾时间维度
				Long timeCP = null;
				if (param.getIsTimeDimension().equals(1)) {
					timeCP = (Long) compareMap.get("time");
					compareMap.put("time", timeCP - compareInterval);
				}
				// 所有的查询维度一样 就匹配上
				for (String str : groupColumnStrList) {
					if (!map.get(str).equals(compareMap.get(str))) {
						//时间对不上要还原
						compareMap.put("time", timeCP);
						judge = false;
						break;
					}
				}
				if (judge) {
					//添加指标需修改
					map.put("allTransCountCP", compareMap.get("allTransCount"));
					map.put("responseTimeCP", compareMap.get("responseTime"));
					map.put("responseRateCP", compareMap.get("responseRate"));
					map.put("successRateCP", compareMap.get("successRate"));
					if (param.getDataType().intValue() == 4) {
						map.put("businessSuccessRateCP", compareMap.get("businessSuccessRate"));
						map.put("moneyCP", compareMap.get("money"));
					}
					if (param.getIsTimeDimension().equals(1)) {
						map.put("timeCP", timeCP);
					}
					flag = true;
					break;
				}

			}
			if (!flag) {
				//添加指标需修改
				map.put("allTransCountCP", null);
				map.put("responseTimeCP", null);
				map.put("responseRateCP", null);
				map.put("successRateCP", null);
				if (param.getIsTimeDimension().equals(1)) {
					map.put("timeCP", (Long) map.get("time")+compareInterval);
				}
				if (param.getDataType().intValue() == 4) {
					map.put("businessSuccessRateCP", null);
					map.put("moneyCP", null);
				}
			}
		}
	}

}
