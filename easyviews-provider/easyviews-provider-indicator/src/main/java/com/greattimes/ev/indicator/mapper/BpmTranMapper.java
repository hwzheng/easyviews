package com.greattimes.ev.indicator.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import org.apache.ibatis.annotations.Param;

import com.greattimes.ev.indicator.entity.BpmExtendTran;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.param.resp.TransactionTpsParam;

public interface BpmTranMapper {

	/**
	 * 获取当日交易量
	 * @author cgc  
	 * @date 2018年9月27日  
	 * @return
	 */
	List<Map<String, Object>> getTransCountToday(Map<String, Object> selectMap);

	List<BpmTran> selectBpmTran(Map<String, Object> map);

	/**
	 * 当前响应时间 当前响应率 当前成功率
	 * @author cgc  
	 * @date 2018年9月27日  
	 * @param ids
	 * @param current
	 * @return
	 */
	List<Map<String, Object>> getCurrent(@Param("ids") List<Integer> ids,@Param("current") long current);

	/**
	 * 应用指标
	 * @author cgc  
	 * @date 2018年9月28日  
	 * @return
	 */
	List<BpmTran> selectBpmTranByUuid(Map<String, Object> map);

	/**
	 * 拓展的指标数据(通用接口)
	 * @author NJ
	 * @date 2018/10/23 14:46
	 * @param param
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectBpmExtendTranMapByMap(Map<String, Object> param);


	/**
	 * 应用指标
	 * @author NJ
	 * @date 2018/10/23 14:47
	 * @param param
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectBpmTranMapByMap(Map<String, Object> param);

	/**扩展指标
	 * @param map
	 * @return
	 */
	List<BpmExtendTran> selectBpmExtendTranByUuid(Map<String, Object> map);

	/**获取饼图数据
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getPieChartData(Map<String, Object> map);

	/**
	 * 应用数据报表表格接口 根据时间和uud 进行粒度的汇总
	 * @author NJ
	 * @date 2019/4/15 16:52
	 * @param param
	 * @return java.util.List<com.greattimes.ev.indicator.entity.BpmTran>
	 */
	List<BpmTran> selectBpmTranByUuidAndGroups(Map<String, Object> param);
	/**
	 * 应用数据报表表格接口 根据时间和uud 进行粒度的汇总 分页
	 * @author NJ
	 * @date 2019/4/15 14:52
	 * @param param
	 * @return java.util.List<com.greattimes.ev.indicator.entity.BpmTran>
	 */
	List<BpmTran> selectBpmTranByUuidAndGroups(Page<BpmTran> page, Map<String, Object> param);

	/**
	 * 应用数据报表表格接口 根据时间和uud 进行粒度的汇总 获取总数
	 * @author NJ
	 * @date 2019/4/15 15:14
	 * @param param
	 * @return int
	 */
	int selectBpmTranCountByUuidAndGroups(Map<String, Object> param);

	/**
	 * 事件数据报表表格接口 根据时间和uud 进行粒度的汇总
	 * @author NJ
	 * @date 2019/4/15 14:57
	 * @param map
	 * @return java.util.List<com.greattimes.ev.indicator.entity.BpmExtendTran>
	 */
	List<BpmExtendTran> selectBpmExtendTranByUuidAndGroups(Page<BpmExtendTran> page, Map<String, Object> map);

	/**
	 * 事件数据报表表格接口 根据时间和uud 进行粒度的汇总
	 * @author NJ
	 * @date 2019/4/15 14:57
	 * @param map
	 * @return java.util.List<com.greattimes.ev.indicator.entity.BpmExtendTran>
	 */
	List<BpmExtendTran> selectBpmExtendTranByUuidAndGroups(Map<String, Object> map);

	/**
	 * 事件数据报表表格接口 根据时间和uud 进行粒度的汇总 获取总数
	 * @author NJ
	 * @date 2019/4/15 15:14
	 * @param param
	 * @return int
	 */
	int selectBpmExtendTranCountByUuidAndGroups(Map<String, Object> param);

	/**
	 * @param map
	 * @return
	 */
	Map<String, Object> getSumData(Map<String, Object> map);


	/**
	 *  bpm通用接口返回map结构
	 * @author NJ
	 * @date 2019/7/25 17:35
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectBpmTranReturnMap(Map<String, Object> map);


	/**
	 * extend通用接口返回map结构
	 * @author NJ
	 * @date 2019/7/26 11:42
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectBpmExtendTranReturnMap(Map<String, Object> map);

	/**
	 * 事件汇总
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmExtendTran> selectCombineExtendTran(Map<String, Object> map);
	
	/**
	 * 事件汇总指标查询
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmExtendTran> selectCombineExtendTranTable(Map<String, Object> map);

	/**
	 *  bpm根据uuid进行汇总
	 * @author NJ
	 * @date 2019/9/4 17:30
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String,Object>> selectCombineBpmTranTable(Map<String, Object> map);

	/**事件指标数据数据（不聚和）
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmExtendTran> selectExtendTranTable(Map<String, Object> map);

	/**bpm指标数据（不聚和）
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmTran> selectTranTable(Map<String, Object> map);

	/**tps峰值
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<TransactionTpsParam> getTpsMax(Map<String, Object> map);

	/**tps峰值（聚合）
	 * @author CGC
	 * @param map
	 * @return
	 */
	TransactionTpsParam getCombineTpsMax(Map<String, Object> map);

	/**bpm一分钟图
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmTran> selectBpmTranMinuteByUuid(Map<String, Object> map);

	/**
	 * get minute extendData
	 * @author CGC
	 * @param map
	 * @return
	 */
	List<BpmExtendTran> selectBpmExtendTranMinuteByUuid(Map<String, Object> map);
}
