package com.greattimes.ev.indicator.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.entity.Original;
import com.greattimes.ev.indicator.mapper.OriginalMapper;
import com.greattimes.ev.indicator.param.req.OriginalParam;
import com.greattimes.ev.indicator.service.IOriginalService;

/**
 * @author cgc
 *
 */
@Service
public class OriginalServiceImpl implements IOriginalService{
	@Autowired
	private OriginalMapper originalMapper;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public String originSelect(OriginalParam param) {
		String data = null;
		param.setDate(DateUtils.longToDefaultDateStr(param.getTimestamp()/1000));
		logger.info("原始报文请求参数:{}",param);
		List<Original> entity = originalMapper.selectOriginalData(param);
		if (!evUtil.listIsNullOrZero(entity)) {
			data = entity.get(0).getData();
		}
		return data;
	}

}
