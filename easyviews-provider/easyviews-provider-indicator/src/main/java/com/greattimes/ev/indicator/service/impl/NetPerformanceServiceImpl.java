package com.greattimes.ev.indicator.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSON;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.IndicatorIntervalParam;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.utils.IndicatorColumnMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.utils.IndicatorSupportUtil;
import com.greattimes.ev.indicator.mapper.NetPerformanceMapper;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.service.INetPerformanceService;
import org.springframework.util.StopWatch;

/**
 * @author NJ
 * @date 2018/9/26 14:54
 */
@Service
public class NetPerformanceServiceImpl implements INetPerformanceService{

    @Autowired
    private NetPerformanceMapper netPerformanceMapper;

    @Autowired
    private ICommonService commonService;

    @Autowired
    private ConfigurationCache configurationCache;


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public IndicatorDataParam selectNetPerformanceListForChart(IndicatorParam param) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<Integer> uuids = param.getUuid();
        List<Indicator> indicators =  commonService.findIndicatorByIds(param.getIndicator());
        IndicatorDataParam indicatorParam = new IndicatorDataParam();
        Map<String, Object> paramMap = new HashMap<>(4);
        Long start = param.getStart();
        Long end = param.getEnd();
        IndicatorIntervalParam interValParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
        start = interValParam.getStart();
        end = interValParam.getEnd();
        List<Long> timeList = interValParam.getTimeList();
        indicatorParam.setTime(timeList);
        if(evUtil.listIsNullOrZero(indicators)){
            indicatorParam.setIndicator(Collections.emptyList());
            return indicatorParam;
        }

        List<String> columnStr = indicators.stream().map(Indicator::getColumnName).collect(Collectors.toList());

        stopWatch.stop();
        logger.debug("》》》》当前线程：【{}】,redis获取参数：{}ms",Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
        stopWatch.start();
        paramMap.put("start",start);
        paramMap.put("end", end);
        paramMap.put("ids", param.getUuid());

        IndicatorSupportUtil.dealIndicatorTable(5, paramMap, interValParam.getType());

        //查询指标列获取
        IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap, columnStr,"indicatorColumnStr", interValParam.getType(), interValParam.getInterval());

        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(start, end);
        paramMap.put("dateStr", dateStrList);

        logger.debug("selectNetPerformanceListForChart查参数:{}", JSON.toJSONString(paramMap));
        List<Map<String, Object>> list = netPerformanceMapper.selectListByColumnStr(paramMap);
        stopWatch.stop();
        logger.debug("》》》》当前线程" +
                "：【{}】,从指标库获取数据：{}ms",Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
        stopWatch.start();

        /**
         * assemble data
         * 1、group by uuid
         * 2、initDefault Map
         * 3、construct result
         */
        Map<Integer, List<Map<String, Object>>> netGroupMap = new HashMap<>();

        list.stream().forEach(x->
            netGroupMap.computeIfAbsent(evUtil.getMapIntegerValue(x,"uuid"),
                    ArrayList :: new ).add(x)
        );

        uuids.forEach(x ->
            netGroupMap.put(x, initDefaultNetPerformanceMap(
                    netGroupMap.computeIfAbsent( x, ArrayList :: new),
                    timeList, columnStr))
        );

        List<IndicatorDataDetailParam> valueParams = new ArrayList<>();
        for(Indicator indicator : indicators){
            for(Integer uuid : param.getUuid()){
                IndicatorDataDetailParam indicatorValueParam = new IndicatorDataDetailParam();
                indicatorValueParam.setUuid(uuid);
                indicatorValueParam.setId(indicator.getId());
                indicatorValueParam.setValue(IndicatorSupportUtil.getFormatValue(netGroupMap.get(uuid),indicator.getColumnName()));
                valueParams.add(indicatorValueParam);
            }
        }
        indicatorParam.setIndicator(valueParams);
        stopWatch.stop();
        logger.debug("》》》》当前线程：【{}】,组装数据耗时：{}ms",Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
        logger.debug("》》》》当前线程：【{}】,接口总耗时：{}ms",Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());
        return indicatorParam;
    }

    @Override
    public List<Map<String, Object>> selectNetPerformanceListForTable(IndicatorParam param) {
        Long start =  param.getStart();
        Long end = param.getEnd();
        // 获取timeList
        IndicatorIntervalParam interValParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
        start =  interValParam.getStart();
        end = interValParam.getEnd();
        Map<String, Object> map  = new HashMap<>(3);
        map.put("start", start);
        map.put("end", end);
        map.put("ids", param.getUuid());
        map.put("interval", interValParam.getInterval());
        int type = interValParam.getType();
        IndicatorSupportUtil.dealIndicatorTable(5, map, type);
        //加入date分区条件
        map.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
        //查询指标列获取
        IndicatorSupportUtil.generateNetIndicatorColumnStr(map, commonService.selectIndicator(Arrays.asList(3)),"indicatorColumnStr", interValParam.getType(), interValParam.getInterval());
        return netPerformanceMapper.selectListByColumnStr(map);
    }

    @Override
    public IndicatorDataParam selectAppNetPerformanceListForChart(IndicatorParam param) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<Integer> uuids = param.getUuid();

        List<Indicator> indicators =  commonService.findIndicatorByIds(param.getIndicator());
        Map<String, Object> paramMap = new HashMap<>(4);
        IndicatorDataParam indicatorParam = new IndicatorDataParam();
        Long start = param.getStart();
        Long end = param.getEnd();
        //获取timeList和表名
        IndicatorIntervalParam interValParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
        start = interValParam.getStart();
        end = interValParam.getEnd();
        List<Long> timeList = interValParam.getTimeList();
        indicatorParam.setTime(timeList);
        if(evUtil.listIsNullOrZero(indicators)){
            indicatorParam.setIndicator(Collections.emptyList());
            return indicatorParam;
        }

        List<String> columnStr = indicators.stream().map(Indicator::getColumnName).collect(Collectors.toList());

        stopWatch.stop();
        logger.debug("》》》》当前线程：【{}】,redis获取参数：{}ms",Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
        stopWatch.start();

        paramMap.put("start",start);
        paramMap.put("end", end);
        paramMap.put("ids", param.getUuid());

        IndicatorSupportUtil.dealIndicatorTable(5, paramMap, interValParam.getType());

        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(start, end);
        paramMap.put("dateStr", dateStrList);

        //查询指标列获取
        IndicatorSupportUtil.generateNetIndicatorColumnStr(paramMap, columnStr,"indicatorColumnStr", interValParam.getType(), interValParam.getInterval());
        logger.debug("selectAppNetPerformanceListForChart查询参数:{}", JSON.toJSONString(paramMap));
        List<Map<String, Object>> list = netPerformanceMapper.selectListByColumnStr(paramMap);

        stopWatch.stop();
        logger.debug("》》》》当前线程：【{}】,从指标库获取数据：{}ms",Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
        stopWatch.start();

        /**
         * assemble data
         * 1、group by uuid
         * 2、initDefault Map
         * 3、construct result
         */
        Map<Integer, List<Map<String, Object>>> netGroupMap = new HashMap<>();

        list.stream().forEach(x->
            netGroupMap.computeIfAbsent(evUtil.getMapIntegerValue(x,"uuid"), ArrayList :: new).add(x)
        );

        uuids.forEach(x ->
            netGroupMap.put(x, initDefaultNetPerformanceMap(
                    netGroupMap.computeIfAbsent( x, ArrayList :: new),
                    timeList, columnStr))
        );

        List<IndicatorDataDetailParam> valueParams = new ArrayList<>();
        for(Indicator indicator : indicators){
            for(Integer uuid : param.getUuid()){
                IndicatorDataDetailParam indicatorValueParam = new IndicatorDataDetailParam();
                indicatorValueParam.setId(indicator.getId());
                indicatorValueParam.setUuid(uuid);
                indicatorValueParam.setValue(IndicatorSupportUtil.getFormatValue(netGroupMap.get(uuid),indicator.getColumnName()));
                valueParams.add(indicatorValueParam);
            }
        }
        indicatorParam.setIndicator(valueParams);
        stopWatch.stop();
        logger.debug("》》》》当前线程：【{}】,组装数据耗时：{}ms",Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
        logger.debug("》》》》当前线程：【{}】,接口总耗时：{}ms",Thread.currentThread().getId(), stopWatch.getTotalTimeSeconds());

        return indicatorParam;
    }

    @Override
    public List<Map<String, Object>> selectAppNetPerformanceListForTable(IndicatorParam param) {
        Long start =  param.getStart();
        Long end = param.getEnd();
        // 获取timeList
        IndicatorIntervalParam interValParam = IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
        start = interValParam.getStart();
        end = interValParam.getEnd();

        Map<String, Object> map  = new HashMap<>(3);
        map.put("start", start);
        map.put("end", end);
        map.put("ids", param.getUuid());

        IndicatorSupportUtil.dealIndicatorTable(5, map, interValParam.getType());

        //加入date分区条件
        List<String> dateStrList = DateUtils.getDateStrByStartAndEnd(start, end);
        map.put("dateStr", dateStrList);
        //查询指标列获取
        IndicatorSupportUtil.generateNetIndicatorColumnStr(map, commonService.selectIndicator(Arrays.asList(4)), "indicatorColumnStr",interValParam.getType(), interValParam.getInterval());
        logger.debug("selectAppNetPerformanceListForTable查询参数:{}", JSON.toJSONString(map));
        return  netPerformanceMapper.selectListByColumnStr(map);
    }

    /**
     * 根据时间戳初始化网络性能和应用性能的map
     * @author NJ
     * @date 2018/12/29 10:14
     * @param list
     * @param timeList
     * @param columns
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    private List<Map<String, Object>> initDefaultNetPerformanceMap(List<Map<String, Object>> list, List<Long> timeList, List<String> columns){
        List<Map<String, Object>> result = new ArrayList<>();
        boolean flag;
        for (Long time : timeList) {
            flag = false;
            for (Map<String, Object> netMap : list) {
                if (time.equals(evUtil.getMapLong(netMap, "time"))) {
                    result.add(netMap);
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                result.add(initDefaultMap(time, columns));
            }
        }
        return result;
    }

    /**
     * 初始化网络数据map
     * @author NJ
     * @date 2018/12/29 9:47
     * @param time
     * @return void
     */
    public Map<String, Object> initDefaultMap(Long time, List<String> columns){
        Map<String, Object> map = new HashMap<>();
        map.put("time", time);
        for(String col : columns){
            map.put(col, null);
        }
        return map;
    }

    /**
     * 根据指标生成查询指标列
     * @param indicators
     * @param interval
     * @return
     */
    public String generateSqlColumns(List<Indicator> indicators, int type, int interval){
        StringBuilder sb = new StringBuilder(" time,date,applicationId,componentId,level,uuid ");
        if(evUtil.listIsNullOrZero(indicators)){
            return sb.toString();
        }
        // day:3 hour:1 5min:2 second:0
        if(type == 0){
            for(Indicator indicator : indicators){
                if(StringUtils.isNotBlank(IndicatorColumnMap.NETWORK_COLUMNS_MAP_SUM.get(indicator.getColumnName()))){
                    sb.append(",").append(IndicatorColumnMap.NETWORK_COLUMNS_MAP_SUM.get(indicator.getColumnName()).replaceAll("\\$", String.valueOf(interval)));
                }
            }
        }else{
            //day:3 hour:1 5min:2
            for(Indicator indicator : indicators){
                if(StringUtils.isNotBlank(IndicatorColumnMap.NETWORK_COLUMNS_MAP_SUM_MERGE.get(indicator.getColumnName()))){
                    sb.append(",").append(IndicatorColumnMap.NETWORK_COLUMNS_MAP_SUM_MERGE.get(indicator.getColumnName()).replaceAll("\\$", String.valueOf(interval)));
                }
            }
        }
        return sb.toString();
    }

}
