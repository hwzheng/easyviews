package com.greattimes.ev.indicator.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.indicator.entity.NetPerformance;

import java.util.List;
import java.util.Map;

public interface NetPerformanceMapper extends BaseMapper<NetPerformance> {

	/**
	 * 网络性能,根据列名查询
	 * @param param
	 * @return
	 */
	List<Map<String, Object>> selectListByColumnStr(Map<String, Object> param);


	/**
	 * 网络性能
	 * @param param
	 * @return
	 */
	List<Map<String, Object>> selectListByMap(Map<String, Object> param);



	/**
	 * 获取应用性能指标
	 * @param param
	 * @return
	 */
	List<Map<String, Object>> selectAppListByMap(Map<String, Object> param);
}

