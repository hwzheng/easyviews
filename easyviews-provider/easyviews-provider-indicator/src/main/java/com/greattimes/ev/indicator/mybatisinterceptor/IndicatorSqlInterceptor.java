package com.greattimes.ev.indicator.mybatisinterceptor;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.lang.reflect.Field;
import java.util.Properties;

/***
 * 指标视图sql拦截器
 * @author LiHua
 */

@Intercepts(@Signature(type = Executor.class, method = "query",
        args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}))
public class IndicatorSqlInterceptor implements Interceptor {

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        BoundSql boundSql= getBoundSql(invocation);
        String sql=getBoundSql(invocation).getSql();
        sql=sqlHandler(sql);
        if(boundSql!=null){
            //通过反射修改sql语句
            Field field = boundSql.getClass().getDeclaredField("sql");
            field.setAccessible(true);
            field.set(boundSql, sql);
        }
        return invocation.proceed();
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }

    /**
     * 获取sql
     * @param invocation
     * @return
     */
    private  BoundSql getBoundSql(Invocation invocation){
        Object[] args = invocation.getArgs();
        MappedStatement ms = (MappedStatement) args[0];
        if (SqlCommandType.SELECT!=ms.getSqlCommandType()){
            return null;
        }
        Object parameterObject = args[1];
        BoundSql boundSql = ms.getBoundSql(parameterObject);
        return boundSql;
    }

    /**
     * 处理sql
     * @param sql
     * @return
     */
    private String sqlHandler(String sql){
        if(sql.contains("v_transaction_indicator")){
            SqlHandler sh=new TransactionIndicatorSqlHandler();
            sql=sh.handleSql(sql);
        }
        return sql;
    }
}
