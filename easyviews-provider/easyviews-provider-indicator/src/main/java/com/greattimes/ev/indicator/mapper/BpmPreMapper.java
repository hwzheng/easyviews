package com.greattimes.ev.indicator.mapper;



import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.plugins.Page;

public interface BpmPreMapper  {

	List<Map<String, Object>> selectList(Map<String, Object> param);

	List<Map<String, Object>> selectListByIndicators(Map<String, Object> param);
	
	int getMultidimensionCount(Map<String, Object> param);
	
	List<Map<String, Object>> selectCustomListByIndicators(Map<String, Object> param);

	List<Map<String, Object>> getMultiChat(@Param("paramMap") Map<String, Object> paramMap);
	
	List<Map<String, Object>> getCustomMultiChat(@Param("paramMap") Map<String, Object> paramMap);

	List<Map<String, Object>> selectTableInfo(Map<String, Object> param);

	void createPerTable(Map<String, Object> map);

	void createPerExtendTable(Map<String, Object> map);

	void modifyPerTable(Map<String, Object> map);

	List<Map<String, Object>> isExist(@Param("table") String table);

	void createPerView(Map<String, Object> map);

	void createPerExtendView(Map<String, Object> map);

	void dropTable(@Param("tableName")String tableName);

	void createPerMaterializedView(Map<String, Object> map);

	void createPerExtendMaterializedView(Map<String, Object> map);

	void createPerViewHour(Map<String, Object> map);

	void createPerExtendViewHour(Map<String, Object> map);

	void detachTable(@Param("tableName")String tableName);

	void AttachView(Map<String, Object> map);

	void AttachExtendView(Map<String, Object> map);

	List<Map<String, Object>> getMaxAndMinData(@Param("table") String table,@Param("startDate") long startDate,@Param("endDate") long endDate,@Param("type") int type,@Param("id") int id);

	/**查维度占比
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> selectAllTransCountPercent(Map<String, Object> selectMap);

	/**瓶颈成功、失败占比
	 * @param datelist 
	 * @param table 
	 * @param datelist 
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> selectBpmSuccessAndfailPercent(Map<String,Object> map);

	/**瓶颈成功、失败占比
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> selectExtendSuccessAndfailPercent(Map<String, Object> selectMap);

	/**数值类型最大最小平均值
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> getIndicatorMaxMinAvg(Map<String, Object> selectMap);

	/**瓶颈表格数据 bpm
	 * @param tableMap
	 * @return
	 */
	List<Map<String, Object>> getBpmTableData(Map<String, Object> map);
	/**瓶颈表格数据 extend
	 * @param tableMap
	 * @return
	 */
	List<Map<String, Object>> getExtendTableData(Map<String, Object> map);

	/**获取维度值
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getDifTradeType(Map<String, Object> map);

	/**瓶颈表格数据行数
	 * @param map
	 * @return
	 */
	int getTableDataCount(Map<String, Object> map);

	List<Map<String, Object>> selectCustomAllTransCountPercent(Map<String, Object> selectMap);

	int getExtendTableDataCount(Map<String, Object> map);

	List<Map<String, Object>> getIndicatorMaxMinAvgPercent(Map<String, Object> selectMap);

	List<Map<String, Object>> selectListByIndicators(Page<Map<String, Object>> page, Map<String, Object> param);

	List<Map<String, Object>> selectCustomListByIndicators(Page<Map<String, Object>> page,
			Map<String, Object> param);


	List<Map<String, Object>> getTopChartData(Map<String, Object> map);



	List<Map<String, Object>> getCustomTopChartData(Map<String, Object> map);

	/**
	 * 智能报表(折线图)查询接口 按照维度和时间分组 [extendPre]
	 * @author NJ
	 * @date 2019/4/3 19:56
	 * @param param
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> getEventLineDataByDimension(Map<String, Object> param);

	/**
	 * 智能报表(折线图)查询接口 按照维度和时间分组[bpmPre]
	 * @author NJ
	 * @date 2019/4/3 19:57
	 * @param param
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> getAppLineDataByDimension(Map<String, Object> param);

	int getMultidimensionCustomCount(Map<String, Object> param);


}
