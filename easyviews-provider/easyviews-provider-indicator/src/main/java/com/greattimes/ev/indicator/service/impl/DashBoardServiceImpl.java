package com.greattimes.ev.indicator.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.mapper.BpmTranMapper;
import com.greattimes.ev.indicator.param.req.IndicatorIntervalParam;
import com.greattimes.ev.indicator.param.resp.DashBoardDetailParam;
import com.greattimes.ev.indicator.param.resp.DashBoardParam;
import com.greattimes.ev.indicator.service.IDashBoardService;
import com.greattimes.ev.utils.IndicatorSupportUtil;

/**
 * @author cgc
 *
 */
@Service
public class DashBoardServiceImpl implements IDashBoardService {
	@Autowired
	private BpmTranMapper bpmTranMapper;
	@Autowired
	private ICommonService commonService;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ConfigurationCache configurationCache;
	
    private static final String VM_TRANSACTION_INDICATOR = "vm_transaction_indicator";
    private static final String BPMTRAN = "bpmTran";
	@Override
	public DashBoardParam getDashBoardData(List<Integer> ids, Long start, Long end) {

		// 交易量
		IndicatorIntervalParam indicatorParam=IndicatorSupportUtil.getIndicatorInterval(start, end, configurationCache);
		List<Long> timeList = indicatorParam.getTimeList();

		start=indicatorParam.getStart();
		end=indicatorParam.getEnd();
		Map<String, Object> selectMap =new HashMap<>(14);
		selectMap.put("ids", ids);
		selectMap.put("start", start);
		selectMap.put("end", end);
		IndicatorSupportUtil.dealIndicatorTable(1, selectMap, indicatorParam.getType());
		// 拼接指标sql
		List<Integer> type = new ArrayList<>();
		type.add(1);
		List<String> indicators = commonService.selectIndicator(type);
		IndicatorSupportUtil.generateIndicatorColumnStr(selectMap, indicators, evUtil.getMapIntValue(selectMap, "type"));
		//加入date分区条件
        selectMap.put("dateStr", DateUtils.getDateStrByStartAndEnd(start, end));
        logger.debug("监控台chart查询参数：{}", selectMap);
		StopWatch sw = new StopWatch();
		sw.start();

		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTran(selectMap);
		sw.stop();
		logger.debug("监控台chart数据库查询，花费时间：{}ms", sw.getLastTaskTimeMillis());
		// 当日交易量
		Map<String, Object> selectMap2 =new HashMap<>(5);
		selectMap2.put("ids", ids);
		IndicatorSupportUtil.dealIndicatorTable(1, selectMap2, indicatorParam.getType());
		selectMap2.put("date", DateUtils.getDateStr(new Date(end)));
		sw.start();
		logger.debug("监控台当日交易量查询参数：{}", selectMap2);
		List<Map<String, Object>> transCountToday = bpmTranMapper.getTransCountToday(selectMap2);
		sw.stop();
		logger.debug("监控台当日交易量数据库查询，花费时间：{}ms", sw.getLastTaskTimeMillis());
		sw.start();
		Map<Integer, Object> transCountTodayMap = new HashMap<>();
		
		for (Map<String, Object> map : transCountToday) {
			transCountTodayMap.put(Integer.parseInt(map.get("applicationId").toString()), map.get("allTransCount"));
		}
		// 补数据
		ids.forEach(x -> {
			if (!transCountTodayMap.containsKey(x)) {
				transCountTodayMap.put(x, 0);
			}
		});
		
		// group by applicationId
		Map<Integer, List<BpmTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getApplicationId, Collectors.toList()));
		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();
		ids.forEach(x -> resultGroup.put(x, initDefault(grouplist.computeIfAbsent(x, ArrayList :: new), timeList)));
		// assembly data
		DashBoardParam result1 = new DashBoardParam();
		List<DashBoardDetailParam> value = new ArrayList<>();
		result1.setTime(timeList);
		for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
			//添加指标需修改
			DashBoardDetailParam detail = new DashBoardDetailParam();
			detail.setId(entry.getKey());
			detail.setTransCountToday(Integer.parseInt(transCountTodayMap.get(entry.getKey()).toString()));
			double responseTime = 0, responseRate = 0, successRate = 0;
			List<BpmTran> tran=resultGroup.get(entry.getKey());
			if (null != tran.get(tran.size()-1).getResponseTime()) {
				responseTime = tran.get(tran.size()-1).getResponseTime();
			}
			if (null != tran.get(tran.size()-1).getResponseRate()) {
				responseRate = tran.get(tran.size()-1).getResponseRate();
			}
			if (null != tran.get(tran.size()-1).getSuccessRate()) {
				successRate = tran.get(tran.size()-1).getSuccessRate();
			}
			detail.setResponseTime(responseTime);
			detail.setResponseRate(responseRate);
			detail.setSuccessRate(successRate);
			List<BpmTran> bpm = entry.getValue();
			List<Integer> transCountValueData = bpm.stream().map(BpmTran::getAllTransCount)
					.collect(Collectors.toList());
			detail.setTransCountValueData(transCountValueData);
			value.add(detail);
		}
		result1.setValueData(value);
		sw.stop();
		logger.debug("监控台组装数据，花费时间：{}ms", sw.getLastTaskTimeMillis());
		logger.debug("监控台总共花费时间：{}ms", sw.getTotalTimeMillis());
		return result1;
	}

	/**
	 * init default list
	 * 
	 * @param data
	 * @param timeList
	 * @return
	 */
	private List<BpmTran> initDefault(List<BpmTran> data, List<Long> timeList) {
		List<BpmTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : data) {
				if (time.longValue() == trans.getTime().longValue()) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmTran(time));
			}
		}
		return result;
	}
}
