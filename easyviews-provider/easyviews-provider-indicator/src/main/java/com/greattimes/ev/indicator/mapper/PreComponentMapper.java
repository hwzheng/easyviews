package com.greattimes.ev.indicator.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.indicator.entity.PreComponent;

public interface PreComponentMapper extends BaseMapper<PreComponent> {
	List<Map<String, Object>> selectList();
}
