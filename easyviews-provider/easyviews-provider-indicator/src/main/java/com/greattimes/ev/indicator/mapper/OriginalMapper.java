package com.greattimes.ev.indicator.mapper;

import java.util.List;

import com.greattimes.ev.indicator.entity.Original;
import com.greattimes.ev.indicator.param.req.OriginalParam;

public interface OriginalMapper {

	List<Original> selectOriginalData(OriginalParam param);


}
