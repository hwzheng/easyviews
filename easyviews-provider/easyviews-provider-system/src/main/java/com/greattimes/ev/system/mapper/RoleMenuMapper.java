package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.RoleMenu;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-03-23
 */
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    int deleteByRoleId(Integer roleId);
    int insertBatch(@Param("roleId") Integer roleId, @Param("menuIds") List<Integer> menuIds);
}
