package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author LiHua
 * @since 2018-03-23
 */
public interface RoleMapper extends BaseMapper<Role> {
    List<Role> findList();
    List<Role> isHasSameName( @Param("id") Integer id, @Param("name")String name);
    List<Role> findRoleByUserId(@Param("userId") Integer userId);
}
