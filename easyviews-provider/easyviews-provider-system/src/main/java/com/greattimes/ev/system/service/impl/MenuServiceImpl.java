package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Menu;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.mapper.MenuMapper;
import com.greattimes.ev.system.mapper.RoleMapper;
import com.greattimes.ev.system.mapper.UserMapper;
import com.greattimes.ev.system.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-03-23
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

	@Autowired
	private MenuMapper menuMapper;
	@Autowired
	private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;


	@Override
	public List<Map<String, Object>> findRoleMenuTree(int id) {
		Role role = roleMapper.selectById(id);
		List<Map<String, Object>> list = menuMapper.findMenuTreeByRoleIdAndType(id, role.getType());
		List<Map<String, Object>> jtList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : list) {
			if (evUtil.getMapIntValue(map, "parentId") == 0) {
				Map<String, Object> jtMap = new HashMap<String, Object>(4);
				jtMap.put("id", evUtil.getMapIntValue(map, "id"));
				jtMap.put("parentId", evUtil.getMapIntValue(map, "parentId"));
				jtMap.put("title", evUtil.getMapStrValue(map, "cname"));
				jtMap.put("isButton", evUtil.getMapIntValue(map, "isButton"));
				if (evUtil.getMapIntValue(map, "roleId") != -1) {
					jtMap.put("checkAll", true);
				}else{
					jtMap.put("checkAll", false);
				}
				jtList.add(jtMap);
			}
		}
		for (Map<String, Object> map : jtList) {
			getResult(map, list);
		}

		return jtList;
	}


    /**
     * 给每个节点赋值相应的选中的属性
     * @param jtMap
     * @param list
     */
    private void computeResult(Map<String, Object> jtMap, List<Map<String, Object>> list){
        int parentId = evUtil.getMapIntValue(jtMap, "parentId");
        //没有子节点
        if(evUtil.listIsNullOrZero(list)){
            if(parentId == 0){
                jtMap.put("checkAll",false);
            }
        }else{
            List<Map<String, Object>> childrenNodes = getAllChildrenNode(jtMap,list, new ArrayList<>());
            int i = 0;
			boolean isButtonFlag = false;
            for(Map<String, Object> map : childrenNodes){
				if(evUtil.getMapIntValue(map, "isButton") == 1){
					isButtonFlag = true;
					break;
				}
                if (map.get("check") != null && (boolean) map.get("check") == true) {
                    i++;
                }
            }

            //如果子节点含有isButton则不必进行父节点状态的重置操作
//            if(!isButtonFlag){
//				//如果有子节点
//				jtMap.remove("check");
//				if(i == 0){
//					//子节点都没有选中
//					if(parentId == 0 ){
////						jtMap.put("checkAll",false);
////						jtMap.put("indeterminate",false);
//					}else{
////						jtMap.put("expand", false);
//						jtMap.put("check",false);
////						jtMap.put("indeterminate",false);
//					}
//				}else if( i == childrenNodes.size()){
//					if(parentId == 0 ) {
////						jtMap.put("checkAll", true);
////						jtMap.put("indeterminate",false);
//					}else{
////						jtMap.put("expand", true);
//						jtMap.put("check",true);
////						jtMap.put("indeterminate",false);
//					}
//				}else{
//					//至少有一个节点被选中
//					if(parentId == 0 ) {
////						jtMap.put("checkAll", false);
//						jtMap.put("checkAll", true);
////						jtMap.put("indeterminate",true);
//					}else{
////						jtMap.put("expand", true);
//						jtMap.put("check",false);
////						jtMap.put("indeterminate",true);
//					}
//				}
//			}

            for(Map<String, Object> map : childrenNodes){
                computeResult(map, (List<Map<String, Object>>) map.get("children"));
            }
        }
    }


	/**
	 * 获取节点的所有子节点
	 * @param jtMap
	 * @param list
	 * @return
	 */
	private List<Map<String, Object>> getAllChildrenNode(Map<String, Object> jtMap, List<Map<String, Object>> list, List<Map<String, Object>> result){
		if(!evUtil.listIsNullOrZero(list)){
			for(Map<String, Object> map : list) {
				result.add(map);
				if(map.get("children") != null ){
					getAllChildrenNode(map, (List<Map<String, Object>>)map.get("children"), result);
				}
			}
		}
		return result;
	}


	/**
	 * 递归得到封装结果集
	 * @param jtMap
	 * @param list
	 */
	public void getResult(Map<String, Object> jtMap, List<Map<String, Object>> list) {
		for (Map<String, Object> map : list) {
			if (evUtil.getMapIntValue(map, "parentId") == evUtil.getMapIntValue(jtMap, "id")) {
				List<Map<String, Object>> children = new ArrayList<>();
				if (jtMap.get("children") != null) {
					children = (List<Map<String, Object>>) jtMap.get("children");
				}
//				else {
//					children = new ArrayList<Map<String, Object>>();
//				}
				Map<String, Object> childrenMap = new HashMap<String, Object>();
				childrenMap.put("id", evUtil.getMapIntValue(map, "id"));
				childrenMap.put("parentId", evUtil.getMapIntValue(map, "parentId"));
				childrenMap.put("title", evUtil.getMapStrValue(map, "cname"));
				childrenMap.put("isButton", evUtil.getMapIntValue(map, "isButton"));
				if (evUtil.getMapIntValue(map, "roleId") != -1) {
					childrenMap.put("check", true);
				}else{
					childrenMap.put("check", false);
				}
				children.add(childrenMap);
				jtMap.put("children", children);

				getResult(childrenMap, list);
//				if (evUtil.getMapIntValue(map, "isLeaf") != 1) {
//					getResult(childrenMap, list);
//				}
			}
		}
	}

	@Override
	public List<Map<String, Object>> getRouters(User user, String ename,int isShow) {
		Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("ename", ename);
		List<Menu> menu=menuMapper.selectByMap(columnMap);
		if(evUtil.listIsNullOrZero(menu)) {
			return null;
		}
		Integer parentId=menu.get(0).getId();
		Integer id = user.getId();
		int roleId =-1;
		List<Role> roleType = userMapper.getRoleType(id);
		if (!evUtil.listIsNullOrZero(roleType)) {
			roleId = roleType.get(0).getId();
		}
		List<Map<String, Object>> list = menuMapper.getRoutersByRoleId(roleId,isShow);
		List<Map<String, Object>> jtList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : list) {
			if (evUtil.getMapIntValue(map, "parentId") == parentId) {
				Map<String, Object> jtMap = new HashMap<String, Object>(7);
				jtMap.put("id", evUtil.getMapIntValue(map, "id"));
				jtMap.put("parentId", evUtil.getMapIntValue(map, "parentId"));
				jtMap.put("title", evUtil.getMapStrValue(map, "cname"));
				jtMap.put("name", evUtil.getMapStrValue(map, "ename"));
				jtMap.put("component", evUtil.getMapStrValue(map, "component"));
				jtMap.put("path", evUtil.getMapStrValue(map, "href"));
				Map<String, Object> meta = new HashMap<String, Object>(3);
				if(1==evUtil.getMapIntValue(map, "isShow")) {
					meta.put("show", true);
				}else {
					meta.put("show", false);
				}
				meta.put("target", evUtil.getMapStrValue(map, "target"));
				meta.put("keepAlive", initKeepAlive(evUtil.getMapIntValue(map, "isRefresh")));
				meta.put("icon", evUtil.getMapStrValue(map, "icon"));
				jtMap.put("meta", meta);
				jtList.add(jtMap);
			}
		}
		for (Map<String, Object> map : jtList) {
			getRouterChildren(map, list);
		}
		return jtList;
	}
	private boolean initKeepAlive(int rule) throws RuntimeException {
        boolean flag;
        switch (rule){
            case 1 :
            	flag = true;
                break;
            case 0 :
            	flag = false;
                break;
            default:
                throw new RuntimeException("路由KeepAlive参数有误，参数(rule):"+ rule);
        }
        return flag;
    }
	/**
	 * 递归得到路由子集
	 * @param jtMap
	 * @param list
	 */
	public void getRouterChildren(Map<String, Object> jtMap, List<Map<String, Object>> list) {
		for (Map<String, Object> map : list) {
			if (evUtil.getMapIntValue(map, "parentId") == evUtil.getMapIntValue(jtMap, "id")) {
				List<Map<String, Object>> children = new ArrayList<>();
				if (jtMap.get("children") != null) {
					children = (List<Map<String, Object>>) jtMap.get("children");
				}
				Map<String, Object> childrenMap = new HashMap<String, Object>(7);
				childrenMap.put("id", evUtil.getMapIntValue(map, "id"));
				childrenMap.put("parentId", evUtil.getMapIntValue(map, "parentId"));
				childrenMap.put("title", evUtil.getMapStrValue(map, "cname"));
				childrenMap.put("name", evUtil.getMapStrValue(map, "ename"));
				childrenMap.put("component", evUtil.getMapStrValue(map, "component"));
				childrenMap.put("path", evUtil.getMapStrValue(map, "href"));
				Map<String, Object> meta = new HashMap<String, Object>(3);
				if(1==evUtil.getMapIntValue(map, "isShow")) {
					meta.put("show", true);
				}else {
					meta.put("show", false);
				}
				int target = evUtil.getMapIntValue(map, "target");
				meta.put("target", target);
				meta.put("keepAlive", initKeepAlive(evUtil.getMapIntValue(map, "isRefresh")));
				meta.put("icon", evUtil.getMapStrValue(map, "icon"));
				if(target == 2){
					meta.put("href",evUtil.getMapStrValue(map,"href"));
				}
				childrenMap.put("meta", meta);
				children.add(childrenMap);
				jtMap.put("children", children);
				//修改继续递归条件 author by NJ 20181217
				getRouterChildren(childrenMap, list);
//				if (evUtil.getMapIntValue(map, "isLeaf") != 1) {
//					getRouterChildren(childrenMap, list);
//				}
			}
		}
	}


	@Override
	public List<Map<String, Object>> getAllRouters(User user) {
		Integer parentId=0;
		Integer id = user.getId();
		int roleId =-1;
		int type = 0;
		List<Role> roleType = userMapper.getRoleType(id);
		if (!evUtil.listIsNullOrZero(roleType)) {
			roleId = roleType.get(0).getId();
			type = roleType.get(0).getType();
		}
		List<Map<String, Object>> list = menuMapper.getAllRoutersByRoleId(roleId, type);
		List<Map<String, Object>> jtList = new ArrayList<Map<String, Object>>();
		for (Map<String, Object> map : list) {
			if (evUtil.getMapIntValue(map, "parentId") == parentId) {
				Map<String, Object> jtMap = new HashMap<String, Object>(7);
				jtMap.put("id", evUtil.getMapIntValue(map, "id"));
				jtMap.put("parentId", evUtil.getMapIntValue(map, "parentId"));
				jtMap.put("title", evUtil.getMapStrValue(map, "cname"));
				jtMap.put("name", evUtil.getMapStrValue(map, "ename"));
				jtMap.put("component", evUtil.getMapStrValue(map, "component"));
				jtMap.put("path", evUtil.getMapStrValue(map, "href"));
				Map<String, Object> meta = new HashMap<String, Object>(3);
				if(1==evUtil.getMapIntValue(map, "isShow")) {
					meta.put("show", true);
				}else {
					meta.put("show", false);
				}
				int target = evUtil.getMapIntValue(map, "target");
				meta.put("target", target);
				meta.put("keepAlive", initKeepAlive(evUtil.getMapIntValue(map, "isRefresh")));
				meta.put("icon", evUtil.getMapStrValue(map, "icon"));
				if(target == 2){
					meta.put("href",evUtil.getMapStrValue(map,"href"));
				}
				jtMap.put("meta", meta);
				jtList.add(jtMap);
			}
		}
		for (Map<String, Object> map : jtList) {
			getRouterChildren(map, list);
		}
		return jtList;
	}


	@Override
	public List<Map<String, Object>> getRoleMenus() {
		// TODO Auto-generated method stub
		return menuMapper.getAllRoleMenus();
	}
}
