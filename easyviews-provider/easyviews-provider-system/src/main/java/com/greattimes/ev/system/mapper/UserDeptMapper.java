package com.greattimes.ev.system.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.system.entity.UserDept;

/**
 * <p>
 * 用户部门表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
public interface UserDeptMapper extends BaseMapper<UserDept> {

	void saveUser(@Param("groupId") Integer groupId,@Param("ids") List<Integer> ids);

}
