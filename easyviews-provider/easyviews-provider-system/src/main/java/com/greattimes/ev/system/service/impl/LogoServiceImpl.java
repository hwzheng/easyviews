package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.system.entity.Logo;
import com.greattimes.ev.system.mapper.LogoMapper;
import com.greattimes.ev.system.service.ILogoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * logo表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2019-05-06
 */
@Service
public class LogoServiceImpl extends ServiceImpl<LogoMapper, Logo> implements ILogoService {

}
