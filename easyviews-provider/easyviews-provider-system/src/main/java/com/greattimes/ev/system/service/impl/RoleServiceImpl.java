package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.UserRole;
import com.greattimes.ev.system.mapper.RoleMapper;
import com.greattimes.ev.system.mapper.RoleMenuMapper;
import com.greattimes.ev.system.mapper.UserMapper;
import com.greattimes.ev.system.mapper.UserRoleMapper;
import com.greattimes.ev.system.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-03-23
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    private  RoleMapper roleMapper;
    @Autowired
    private  RoleMenuMapper roleMenuMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Role> findList() {
        return roleMapper.findList();
    }

    @Override
    public int saveOrUpdateRole(Role role) {
        if(role.getId() == null){
            roleMapper.insert(role);
        }else{
            roleMapper.updateById(role);
        }
        return role.getId();
    }

    @Override
    public int deleteRoleMenus(int id) {
        //删除角色
        int roleNum = roleMapper.deleteById(id);
        //删除角色-菜单的中间表
        int roleMenNum = roleMenuMapper.deleteByRoleId(id);
        //sys_user_role
        Map<String, Object> map = new HashMap<>(1);
        map.put("roleId", id);
        //删除用户
        List<UserRole> userRole=userRoleMapper.selectByMap(map);
        List<Integer> userIds = userRole.stream().map(UserRole::getUserId).collect(Collectors.toList());
        if(!evUtil.listIsNullOrZero(userIds)) {
        	userMapper.deleteBatchIds(userIds);
        }
        userRoleMapper.deleteByMap(map);
        return roleNum + roleMenNum;
    }

    @Override
    public Role selectRoleById(int id) {
        return roleMapper.selectById(id);
    }

    @Override
    public boolean isHasSameName(Integer id, String name) {
        List<Role> list = roleMapper.isHasSameName(id,name);
        return !evUtil.listIsNullOrZero(list);
    }


    @Override
    public List<Role> findRoleByUserId(int id) {
        return roleMapper.findRoleByUserId(id);
    }
}
