package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.Menu;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author LiHua
 * @since 2018-03-23
 */
public interface MenuMapper extends BaseMapper<Menu> {
    List<Menu> findMenuByIds(Map<String, Object> param);

    List<Map<String,Object>> findMenuTreeByRoleIdAndType(@Param("roleId") int roleId, @Param("type") int type);
	/**
	 * 根据角色id和路由类型获取路由列表
	 * @param isShow 查menu传1，查button传0
	 * @param roleId
	 * @return
	 */
	List<Map<String, Object>> getRoutersByRoleId(@Param("roleId") Integer roleId, @Param("isShow") int isShow);
	/**根据角色id和路由类型获取路由列表
	 * @param roleId
	 * @return
	 */
	List<Map<String, Object>> getAllRoutersByRoleId(@Param("roleId") int roleId, @Param("type") int type);
	
	/**
	 * 获取角色菜单
	 * @return
	 */
	List<Map<String, Object>> getAllRoleMenus();
}
