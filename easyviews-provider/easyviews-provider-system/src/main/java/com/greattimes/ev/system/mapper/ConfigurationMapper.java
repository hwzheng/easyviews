package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.Configuration;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 配置表 Mapper 接口
 * </p>
 *
 * @author LiHua
 * @since 2018-03-22
 */
public interface ConfigurationMapper extends BaseMapper<Configuration> {
    /**
     * 更新参数状态
     * @param map
     */
    void updateConfigurationState(Map<String,Object> map);

    /**
     * 查询参数列表
     * @return
     */
    List<Configuration> selectConfigurationList(Map<String,Object> map);

}
