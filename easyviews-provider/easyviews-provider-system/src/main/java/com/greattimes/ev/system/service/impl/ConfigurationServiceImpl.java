package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Configuration;
import com.greattimes.ev.system.mapper.ConfigurationMapper;
import com.greattimes.ev.system.service.IConfigurationService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 配置表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018/5/10 12:50
 */
@Service
public class ConfigurationServiceImpl extends ServiceImpl<ConfigurationMapper, Configuration> implements IConfigurationService {

    @Autowired
    ConfigurationMapper configurationMapper;

    @Override
    public int update(Configuration configuration) {
        return configurationMapper.updateById(configuration);
    }

    @Override
    public int save(Configuration configuration) {
        return configurationMapper.insert(configuration);
    }

    @Override
    public Configuration delete(int id) {
        Configuration configuration = configurationMapper.selectById(id);
        configurationMapper.deleteById(id);
        return configuration;
    }

    @Override
    public List<Configuration> queryConfiguration(Map<String, Object> param) {
        return configurationMapper.selectConfigurationList(param);
    }

    @Override
    public Configuration updateConfigurationState(int id, int state) {
        Map<String, Object>  param =  new HashMap<>(2);
        param.put("id", id);
        param.put("state", state);
        configurationMapper.updateConfigurationState(param);
        return configurationMapper.selectById(id);
    }

    @Override
    public boolean isHasSameKey(String key, Integer id) {
        Map<String, Object> param = new HashMap<>(2);
        param.put("key", key);
        param.put("notEqId", id);
        return !evUtil.listIsNullOrZero(configurationMapper.selectConfigurationList(param));
    }

    @Override
    public Configuration findConfigurationByKey(String key) {
        Map<String, Object> param = new HashMap<>(1);
        param.put("key", key);
        List<Configuration> list = configurationMapper.selectConfigurationList(param);
        if(!evUtil.listIsNullOrZero(list)){
            return list.get(0);
        }else{
            return null;
        }
    }

}
