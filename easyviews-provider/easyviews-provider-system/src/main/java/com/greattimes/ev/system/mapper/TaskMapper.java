package com.greattimes.ev.system.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.system.entity.Task;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.system.param.req.TaskParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 定时任务表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-07-17
 */
public interface TaskMapper extends BaseMapper<Task> {

    List<TaskParam> findListByMap(Map<String, Object> map);

    List<Map<String, Object>> selectLogByMap(Page<Map<String, Object>> page, Map<String,Object> map);
}
