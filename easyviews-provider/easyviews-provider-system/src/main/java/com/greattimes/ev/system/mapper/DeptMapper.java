package com.greattimes.ev.system.mapper;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.system.entity.User;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.system.entity.Dept;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
public interface DeptMapper extends BaseMapper<Dept> {

	/**查询是否有重名部门
	 * @param nameMap
	 * @return
	 */
	List<Dept> selectRepeatName(Map<String, Object> nameMap);

	/**根据userId查询部门
	 * @param userId
	 * @return
	 */
	List<Dept> selectByUserId(@Param("userId") Integer userId);

	/**
	 * 查询群组和用户
	 * @return
	 */
	List<Map<String, Object>> selectDeptAndUser();

	/**
	 * 根据用户组名称查询用户信息
	 * @param name
	 * @return
	 */
	List<User> selectUsersByDeptName(String name);

    /**
     * 根据TuxTqueueRqAddr 查询缺失服务
     * @param list
     * @return
     */
	List<String> selectMissService(List<String> list);

	/**
	 * 事件台查询所有服务
	 * @return
	 */
	Integer getAllServiceNum();
	/**
	 * 查询事件台服务
	 * @param
	 * @author: nj
	 * @date: 2020-03-06 10:26
	 * @version: 0.0.1
	 * @return: java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectServices();
}
