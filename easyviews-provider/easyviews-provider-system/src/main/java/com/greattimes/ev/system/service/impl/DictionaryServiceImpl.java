package com.greattimes.ev.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Dictionary;
import com.greattimes.ev.system.entity.DictionaryType;
import com.greattimes.ev.system.mapper.DictionaryMapper;
import com.greattimes.ev.system.mapper.DictionaryTypeMapper;
import com.greattimes.ev.system.service.IDictionaryService;

/**
 * <p>
 * 系统字典表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
@Service
public class DictionaryServiceImpl extends ServiceImpl<DictionaryMapper, Dictionary> implements IDictionaryService {

	@Autowired
	private DictionaryMapper dictionaryMapper;
	@Autowired
	private DictionaryTypeMapper dictionaryTypeMapper;

	@Override
	public List<Map<String, Object>> selectDicNameAndValueByDictTypeCode(String typeCode) {
		return dictionaryMapper.selectDicNameAndValueByDictTypeCode(typeCode);
	}

	@Override
	public List<DictionaryType> getDictionaryType() {
		return dictionaryTypeMapper.selectByMap(null);
	}

	@Override
	public Integer saveDictionaryType(DictionaryType dictionaryType) {
		dictionaryTypeMapper.insert(dictionaryType);
		return dictionaryType.getId();
	}

	@Override
	public void editDictionaryType(DictionaryType dictionaryType) {
		DictionaryType dt=dictionaryTypeMapper.selectById(dictionaryType.getId());
		if(!dt.getTypeCode().equals(dictionaryType.getTypeCode())){
			Map<String,Object> tcMap = new HashMap<String,Object>();
			tcMap.put("oldTypeCode", dt.getTypeCode());
			tcMap.put("newTypeCode", dictionaryType.getTypeCode());
			dictionaryMapper.updateByTypeCode(tcMap);
		}
		dictionaryTypeMapper.updateById(dictionaryType);
	}

	@Override
	public void deleteDictionaryType(Integer id) {
		DictionaryType dictionarytype = dictionaryTypeMapper.selectById(id);
		String code = dictionarytype.getTypeCode();
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("typeCode", code);
		List<Integer> idList = new ArrayList<>();
		List<Dictionary> list = dictionaryMapper.selectByMap(columnMap);
		if (!evUtil.listIsNullOrZero(list)) {
			for (Dictionary dictionary : list) {
				idList.add(dictionary.getId());
			}
			dictionaryMapper.deleteBatchIds(idList);
		}
		dictionaryTypeMapper.deleteById(id);
	}

	@Override
	public List<Dictionary> getDictionary(Integer id) {
		List<Dictionary> list = dictionaryMapper.selectDictionaryList(id);
		return list;
	}

	@Override
	public void saveDictionary(Dictionary dictionary) {
		dictionary.setActive(1);
		dictionaryMapper.insert(dictionary);
	}

	@Override
	public void editDictionary(Dictionary dictionary) {
		
		dictionaryMapper.updateById(dictionary);
	}

	@Override
	public void deleteDictionary(Integer id) {
		dictionaryMapper.deleteById(id);
	}

	@Override
	public void updateActive(Dictionary dictionary) {
		dictionaryMapper.updateById(dictionary);
	}

	@Override
	public boolean checkType(DictionaryType dictionaryType, int flag) {
		List<DictionaryType> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<DictionaryType> wrapper = new EntityWrapper<>();
			wrapper.where("typeCode={0}", dictionaryType.getTypeCode()).ne("id", dictionaryType.getId());
			list = dictionaryTypeMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("typeCode", dictionaryType.getTypeCode());
			list = dictionaryTypeMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public boolean checkName(DictionaryType dictionaryType, int flag) {
		List<DictionaryType> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<DictionaryType> wrapper = new EntityWrapper<>();
			wrapper.where("name={0}", dictionaryType.getName()).ne("id", dictionaryType.getId());
			list = dictionaryTypeMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("name", dictionaryType.getName());
			list = dictionaryTypeMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}
	
	@Override
	public boolean checkDictionaryValue(Dictionary dictionary, int flag) {
		List<Dictionary> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<Dictionary> wrapper = new EntityWrapper<>();
			wrapper.where("typeCode={0}", dictionary.getTypeCode()).ne("id", dictionary.getId()).and("value={0}", dictionary.getValue());
			list = dictionaryMapper.selectList(wrapper);
		} else {
			EntityWrapper<Dictionary> wrapper = new EntityWrapper<>();
			wrapper.where("typeCode={0}", dictionary.getTypeCode()).and("value={0}", dictionary.getValue());
			list = dictionaryMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public boolean checkDictionaryName(Dictionary dictionary, int flag) {
		List<Dictionary> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<Dictionary> wrapper = new EntityWrapper<>();
			wrapper.where("name={0}", dictionary.getName()).ne("id", dictionary.getId()).and("typeCode={0}", dictionary.getTypeCode());
			list = dictionaryMapper.selectList(wrapper);
		} else {
			EntityWrapper<Dictionary> wrapper = new EntityWrapper<>();
			wrapper.where("name={0}", dictionary.getName()).and("typeCode={0}", dictionary.getTypeCode());
			list = dictionaryMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);
	}
	@Override
	public List<Dictionary> getDictionaryDetail(Integer id) {
		List<Dictionary> list = dictionaryMapper.selectDictionaryDetail(id);
		return list;
	}
}
