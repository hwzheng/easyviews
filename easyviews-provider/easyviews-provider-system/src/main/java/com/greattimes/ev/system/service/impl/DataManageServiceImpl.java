package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.system.entity.Task;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.mapper.TaskLogsMapper;
import com.greattimes.ev.system.mapper.TaskMapper;
import com.greattimes.ev.system.service.IDataManageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2018/8/20 15:27
 */
@Service
public class DataManageServiceImpl implements IDataManageService{

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TaskMapper taskMapper;
    @Autowired
    private TaskLogsMapper taskLogsMapper;

    @Override
    public Map<String, Integer> selectTaskIdMap() {
        Map<String,Object> map = new HashMap<String,Object>(1);
//        map.put("active",1);
        List<Task> list = taskMapper.selectByMap(map);
        return list.stream().collect(Collectors.toMap(Task::getClassName, Task::getId));
    }

    @Override
    public void insertTaskLogs(TaskLogs taskLogs) {
        taskLogsMapper.insert(taskLogs);
    }
}
