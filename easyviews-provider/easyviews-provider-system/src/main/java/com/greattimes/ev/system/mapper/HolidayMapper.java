package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.Holiday;

import java.util.List;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

/**
 * <p>
 * 节假日表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-09-24
 */
public interface HolidayMapper extends BaseMapper<Holiday> {

	List<Holiday> selectHoliday(Page<Holiday> page);

	void insertBatch(@Param("set") Set<Holiday> set);

}
