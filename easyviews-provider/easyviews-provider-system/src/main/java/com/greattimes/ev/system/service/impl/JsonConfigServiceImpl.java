package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.DictionaryType;
import com.greattimes.ev.system.entity.JsonConfig;
import com.greattimes.ev.system.mapper.DictionaryMapper;
import com.greattimes.ev.system.mapper.JsonConfigMapper;
import com.greattimes.ev.system.param.req.JsonConfigParam;
import com.greattimes.ev.system.service.IJsonConfigService;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统 json参数配置表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2019-03-05
 */
@Service
public class JsonConfigServiceImpl extends ServiceImpl<JsonConfigMapper, JsonConfig> implements IJsonConfigService {
	@Autowired
	private JsonConfigMapper jsonConfigMapper;
	
	@Override
	public boolean checkType(JsonConfig jsonConfig, int flag) {
		List<JsonConfig> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<JsonConfig> wrapper = new EntityWrapper<>();
			wrapper.where("code={0}", jsonConfig.getCode()).ne("id", jsonConfig.getId());
			list = jsonConfigMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("code", jsonConfig.getCode());
			list = jsonConfigMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public boolean checkName(JsonConfig jsonConfig, int flag) {
		List<JsonConfig> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<JsonConfig> wrapper = new EntityWrapper<>();
			wrapper.where("name={0}", jsonConfig.getName()).ne("id", jsonConfig.getId());
			list = jsonConfigMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("name", jsonConfig.getName());
			list = jsonConfigMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public Integer saveJsonType(JsonConfig param,Integer userId) {
		param.setUpdateBy(userId);
		param.setUpdateTime(new Date());
		jsonConfigMapper.insert(param);
		return param.getId();
	}

	@Override
	public void editJsonType(JsonConfig param, Integer userId) {
		param.setUpdateBy(userId);
		param.setUpdateTime(new Date());
		jsonConfigMapper.updateById(param);
		
	}

	@Override
	public JsonConfigParam getJsonDetail(Integer id,String code) {
		return jsonConfigMapper.getJsonDetail(id,code);
	}

}
