package com.greattimes.ev.system.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Configuration;
import com.greattimes.ev.system.entity.Message;
import com.greattimes.ev.system.mapper.ConfigurationMapper;
import com.greattimes.ev.system.mapper.MessageMapper;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import com.greattimes.ev.system.service.IMessageService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 短信 发送表 服务实现类
 * </p>
 *
 * @author LiHua
 * @since 2018-10-10
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements IMessageService {
    @Autowired
    private MessageMapper messageMapper;
    @Autowired
    private ConfigurationMapper configurationMapper;

    @Override
    public List<Message> selectAlarmMessageByParam(int type, int state) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("state", state);
        //适应正则查询相应的消息
        map.put("type", type);
        return messageMapper.selectMessageByTypeAndState(map);
    }

    @Override
    public List<AlarmGroupUserParam> selectAlarmUserList(Map<String, Object> map) {
        return messageMapper.findAlarmGroupUserById(map);
    }

    @Override
    public String selectParameterByKey(String alarmMailFromName) {
        EntityWrapper<Configuration> ew = new EntityWrapper<>();
        ew.setSqlSelect("value").where("`key`={0}", alarmMailFromName).and("active=1");
        List<Object> list = configurationMapper.selectObjs(ew);
        if(evUtil.listIsNullOrZero(list)){
            return "";
        }else{
            return list.get(0).toString();
        }
    }

    @Override
    public void updateAlarmMessageState(Integer id, int state) {
        Message am = new Message();
        am.setId(id);
        am.setState(state);
        messageMapper.updateById(am);
    }


    @Override
    public boolean updateAlarmMessageEventState(List<Integer> ids, int state) {
        messageMapper.updateBatchEventState(ids, state);
        return true;
    }

    @Override
    public boolean updateAlarmMessageKafkaState(List<Message> messages, int state) {
        List<Integer> ids=messages.stream().map(Message::getId).collect(Collectors.toList());
        messageMapper.updateBatchKafkaState(ids, state);
        return true;
    }
}
