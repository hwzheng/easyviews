package com.greattimes.ev.system.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-03-13
 */
public interface UserMapper extends BaseMapper<User> {

	/***
	 * 查询用户菜单
	 * 
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectUserMenu(Map<String, Object> map);

	/**
	 * 查詢全部用户
	 * 
	 * @return
	 */
	List<Map<String, Object>> selectUser(@Param("id") Integer id);

	/**
	 * 查询是否有重复登录名用户
	 * 
	 * @param loginNameMap
	 * @return
	 */
	List<User> selectRepeatName(Map<String, Object> loginNameMap);

	/**
	 * 根据用户id获取角色类型
	 * 
	 * @param userId
	 * @return
	 */
	List<Role> getRoleType(Integer userId);

	/**
	 * 获取用户群组id
	 * 
	 * @param userId
	 * @return
	 */
	List<Integer> getDeptId(Integer userId);

	/**
	 * 查询同群组用户
	 * 
	 * @param deptId
	 * @return
	 */
	List<Map<String, Object>> selectUserByDept(Integer deptId);

	/**
	 * 用户编辑
	 * @param userinfo
	 */
	void updateUser(User userinfo);

	/**
	 * 个人中心查询
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> selectUserPersonal(int id);

	/**
	 * 个人中心信息修改
	 * @param map
	 */
	void personalUpdate(Map<String, Object> map);

	/**userId
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> selectGroupUser(Integer userId);

	/**
	 * 根据报表id查询所有用户信息
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectGroupUserByReportId(Map<String, Object> map);


	/**
	 * 查询所有用户
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectGroupUserForAll(Map<String, Object> map);

}
