package com.greattimes.ev.system.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.HolidayUtil;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Holiday;
import com.greattimes.ev.system.mapper.HolidayMapper;
import com.greattimes.ev.system.service.IHolidayService;

/**
 * <p>
 * 节假日表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2019-09-24
 */
@Service
public class HolidayServiceImpl extends ServiceImpl<HolidayMapper, Holiday> implements IHolidayService {
	@Autowired
	private HolidayMapper holidayMapper;
	@Override
	public Page<Holiday> selectHoliday(Page<Holiday> page) {
		 page.setRecords(holidayMapper.selectHoliday(page));
	     return page;
	}
	@Override
	public void insert(List<Holiday> details) {
		// 查询原有数据
		List<Holiday> oldList = holidayMapper.selectList(null);
		if (!evUtil.listIsNullOrZero(oldList)) {
			for (Holiday oldValue : oldList) {
				boolean flag = false;
				Holiday o = new Holiday();
				for (Holiday newValue : details) {
					if (oldValue.equals(newValue)) {
						o = newValue;
						flag = true;
						break;
					}
				}
				// 合集
				if (flag) {
					details.remove(o);
				}
			}
		}
		// 去重
		Set<Holiday> set = new HashSet<>();
		set.addAll(details);
		if (!evUtil.listIsNullOrZero(details)) {
			holidayMapper.insertBatch(set);
		}
	}
	@Override
	public List<String> getday(Map<String, Object> param) {
		List<String> result=new ArrayList<>();
		String dateStr=evUtil.getMapStrValue(param, "date");
		int num=evUtil.getMapIntegerValue(param, "days");
		//字符串转日期
	    Date date=DateUtils.getDate(dateStr);
	    //获取日期
	    Date date2;
	    EntityWrapper<Holiday> wrapper=new EntityWrapper<>();
	    if(num>0) {
	    	date2=DateUtils.getDate(date, num*2+7);
	    	wrapper.ge("date", date);
	    	wrapper.lt("date", date2);
	    }else {
	    	date2=DateUtils.getDate(date, num*2-7);
	    	wrapper.ge("date", date2);
	    	wrapper.lt("date", date);
	    }
	    List<Holiday> list=holidayMapper.selectList(wrapper);
	    Map<String, Object> holidayMap=new HashMap<String, Object>();
	    if(!evUtil.listIsNullOrZero(list)){
	    	holidayMap=list.stream().collect(Collectors.toMap(Holiday::getDate,Holiday::getWorkday));
	    }
	    int delay = 1;
        while (delay <= Math.abs(num)) {
            // 获取前一天或后一天日期
            Date endDay =HolidayUtil.getDate(date, num);
            date=endDay;
            if (HolidayUtil.isWorkday(endDay,holidayMap)) {
            	result.add(DateUtils.getDateStr(endDay));
                delay++;
            } 
        }
        //若日期向前推，重新排序
        if(num<0) {
        	Collections.reverse(result);
        }
		return result;
	}
}
