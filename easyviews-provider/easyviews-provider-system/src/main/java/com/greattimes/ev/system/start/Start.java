package com.greattimes.ev.system.start;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/***
 * 服务启动入口
 * @author LiHua
 */
public class Start {
	public static ClassPathXmlApplicationContext content;
	static{
		content=new ClassPathXmlApplicationContext("classpath:spring/spring.xml");  
	}
	
	/***
	 * 服务启动入口函数
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			content.start();
			while (true) {
				Thread.currentThread().sleep(Long.MAX_VALUE);
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
