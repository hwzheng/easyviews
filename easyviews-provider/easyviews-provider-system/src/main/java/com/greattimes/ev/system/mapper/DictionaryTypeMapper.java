package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.DictionaryType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统字典类型表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-17
 */
public interface DictionaryTypeMapper extends BaseMapper<DictionaryType> {

}
