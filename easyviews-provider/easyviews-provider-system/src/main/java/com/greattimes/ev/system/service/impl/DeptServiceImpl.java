package com.greattimes.ev.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.greattimes.ev.system.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Dept;
import com.greattimes.ev.system.mapper.DeptMapper;
import com.greattimes.ev.system.mapper.DictionaryMapper;
import com.greattimes.ev.system.mapper.UserDeptMapper;
import com.greattimes.ev.system.param.req.DeptUserParam;
import com.greattimes.ev.system.param.req.UserSimpleParam;
import com.greattimes.ev.system.service.IDeptService;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
@Service
public class DeptServiceImpl extends ServiceImpl<DeptMapper, Dept> implements IDeptService {

	@Autowired
	private DeptMapper deptMapper;
	@Autowired
	private UserDeptMapper userDeptMapper;
	@Autowired
	private DictionaryMapper dictionaryMapper;

	@Override
	public List<Map<String, Object>> selectDept() {
		List<Map<String, Object>> result=new ArrayList<>();
		List<Map<String, Object>> deptType = dictionaryMapper.selectDicNameAndValueByDictTypeCode("USER_GROUP");
		List<Dept> deptList=deptMapper.selectList(null);
		for (Map<String, Object> map : deptType) {
			Map<String, Object> dept=new HashMap<>();
			dept.put("type", Integer.parseInt(map.get("value").toString()));
			dept.put("typeLabel", map.get("label"));
			List<Dept> groups=new ArrayList<>();
			for (Dept dept2 : deptList) {
				if(dept2.getType().equals(Integer.parseInt(map.get("value").toString()))) {
					groups.add(dept2);
				}
			}
			dept.put("groups", groups);
			result.add(dept);
		}
		return result;
	}

	@Override
	public void deleteDept(Integer id) {
		deptMapper.deleteById(id);
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("deptId", id);
		userDeptMapper.deleteByMap(columnMap);
	}

	@Override
	public void updateDept(Dept dept) {
		// TODO Auto-generated method stub
		deptMapper.updateById(dept);
	}

	@Override
	public Boolean checkName(Dept dept, int flag) {
		Map<String, Object> nameMap = new HashMap<>();
		nameMap.put("name", dept.getName());
		List<Dept> deptlist = new ArrayList<>();
		if (flag == 0) {
			nameMap.put("id", dept.getId());
			deptlist = deptMapper.selectRepeatName(nameMap);
		} else {
			deptlist = deptMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(deptlist);
	}

	@Override
	public Integer addDept(Dept dept) {
		// TODO Auto-generated method stub
		deptMapper.insert(dept);
		return dept.getId();
	}

	@Override
	public void saveUser(Integer groupId, List<Integer> ids) {
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("deptId", groupId);
		userDeptMapper.deleteByMap(columnMap);
		if (!evUtil.listIsNullOrZero(ids)) {
			userDeptMapper.saveUser(groupId, ids);
		}
	}

	@Override
	public void deleteUser(Integer groupId, Integer id) {
		Map<String, Object> columnMap = new HashMap<>(2);
		columnMap.put("deptId", groupId);
		columnMap.put("userId", id);
		userDeptMapper.deleteByMap(columnMap);
	}

	@Override
	public List<DeptUserParam> selectDeptAndUser() {
		List<Map<String, Object>> list = deptMapper.selectDeptAndUser();

		List<DeptUserParam> user = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(list)) {
			for (Map<String, Object> map : list) {
				DeptUserParam param = new DeptUserParam();
				param.setGroupId(evUtil.getMapIntValue(map, "groupId"));
				param.setGroupName(evUtil.getMapStrValue(map, "groupName"));
				List<UserSimpleParam> detailList = new ArrayList<>();
				String userIdstr = evUtil.getMapStrValue(map, "userId");
				String namestr = evUtil.getMapStrValue(map, "name");
				if (StringUtils.isNotEmpty(namestr) && StringUtils.isNotEmpty(userIdstr)) {
					String[] userIds = evUtil.getMapStrValue(map, "userId").split(",");
					String[] names = evUtil.getMapStrValue(map, "name").split(",");
					for (int i = 0; i < userIds.length; i++) {
						UserSimpleParam detail = new UserSimpleParam();
						if (userIds[i] != null) {
							detail.setUserId(Integer.valueOf(userIds[i]));
						}
						detail.setName(names[i]);
						detailList.add(detail);
					}
				}
				param.setUser(detailList);
				user.add(param);
			}
		}
		return user;
	}

	@Override
	public List<User> selectUserByDeptName(String name) {
		return deptMapper.selectUsersByDeptName(name);
	}
	@Override
	public List<String> selectMissService(List<String> list){
//		Map<String, Object> map = new HashMap<>();
//		map.put("list",list);
		return deptMapper.selectMissService(list);
	}

	@Override
	public Integer getAllServiceNum() {
        return deptMapper.getAllServiceNum();
	}

	@Override
	public List<Dept> selectDeptByType(Integer type) {
		EntityWrapper<Dept> wrapper=new EntityWrapper<>();
		wrapper.eq("type", type);
		return deptMapper.selectList(wrapper);
	}

	@Override
	public Map<String, Integer> selectAllService() {
		List<Map<String, Object>> maps = deptMapper.selectServices();
		int size = (int)(maps.size() / 0.75 + 1);
		Map<String, Integer> result = new HashMap<>(size);
		maps.stream().forEach(x->result.put(String.valueOf(x.get("tuxTqueueRqAddr")),
				((Double)x.get("tuxTqueueSrvrCnt")).intValue()));
		return result;
	}
}
