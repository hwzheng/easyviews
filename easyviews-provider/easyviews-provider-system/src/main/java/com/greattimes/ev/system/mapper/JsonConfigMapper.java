package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.JsonConfig;
import com.greattimes.ev.system.param.req.JsonConfigParam;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 系统 json参数配置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-05
 */
public interface JsonConfigMapper extends BaseMapper<JsonConfig> {

	JsonConfigParam getJsonDetail(@Param("id")Integer id,@Param("code") String code);

}
