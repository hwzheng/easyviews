package com.greattimes.ev.system.service.impl;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.system.entity.Task;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.mapper.TaskLogsMapper;
import com.greattimes.ev.system.mapper.TaskMapper;
import com.greattimes.ev.system.param.req.TaskParam;
import com.greattimes.ev.system.service.ITaskService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 定时任务表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-07-17
 */
@Service
public class TaskServiceImpl extends ServiceImpl<TaskMapper, Task> implements ITaskService {

    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private TaskLogsMapper taskLogsMapper;


    @Override
    public List<TaskParam> selectListByMap(Map<String, Object> map){
        return taskMapper.findListByMap(map);
    }

    @Override
    public Page<Map<String, Object>> selectLogsListByMap(Page<Map<String, Object>> page, Map<String, Object> param) {
        page.setRecords(taskMapper.selectLogByMap(page,param));
        return page;
    }

}
