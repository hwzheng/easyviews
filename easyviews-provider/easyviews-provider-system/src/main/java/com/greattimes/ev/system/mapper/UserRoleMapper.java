package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.UserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

}
