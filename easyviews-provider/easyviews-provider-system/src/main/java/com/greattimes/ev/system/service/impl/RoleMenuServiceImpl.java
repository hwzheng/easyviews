package com.greattimes.ev.system.service.impl;

import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.RoleMenu;
import com.greattimes.ev.system.mapper.RoleMenuMapper;
import com.greattimes.ev.system.service.IRoleMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LiHua
 * @since 2018-03-23
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

    @Autowired
    private  RoleMenuMapper roleMenuMapper;

    @Override
    public int insertBatch(Integer roleId, List<Integer> menuIds) {
        return roleMenuMapper.insertBatch(roleId,menuIds);
    }

    @Override
    public int saveRoleMenus(Integer roleId, List<Integer> menuIds) {
        int num = 0;
        //根据roleId删除所有记录
        roleMenuMapper.deleteByRoleId(roleId);
        if(!evUtil.listIsNullOrZero(menuIds)){
            num = roleMenuMapper.insertBatch(roleId, menuIds);
        }
        return num;
    }
}
