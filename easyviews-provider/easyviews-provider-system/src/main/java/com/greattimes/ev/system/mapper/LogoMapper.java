package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.Logo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * logo表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-05-06
 */
public interface LogoMapper extends BaseMapper<Logo> {

}
