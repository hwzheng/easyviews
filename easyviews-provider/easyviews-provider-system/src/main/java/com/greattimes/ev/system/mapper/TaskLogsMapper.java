package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.TaskLogs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 定时任务表执行日志表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-20
 */
public interface TaskLogsMapper extends BaseMapper<TaskLogs> {

}
