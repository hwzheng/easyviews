package com.greattimes.ev.system.service.impl;

import java.util.*;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.common.utils.ADAuthUtil;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.system.entity.UserLogs;
import com.greattimes.ev.system.mapper.UserLogsMapper;
import com.greattimes.ev.system.param.req.UserLogsParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.entity.UserRole;
import com.greattimes.ev.system.mapper.UserDeptMapper;
import com.greattimes.ev.system.mapper.UserMapper;
import com.greattimes.ev.system.mapper.UserRoleMapper;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IUserService;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-03-13
 */
@Service
public class UserServiceImpl  extends ServiceImpl<UserMapper, User> implements IUserService {

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private UserDeptMapper userDeptMapper;
	@Autowired
	private UserRoleMapper userRoleMapper;
	@Autowired
	private UserLogsMapper userLogsMapper;

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public User getUser(User user) {
		return userMapper.selectOne(user);
	}

	@Override
	public List<Map<String, Object>> getUserMenu(Integer paranetId, Integer userId) {
		Map<String, Object> param = new HashMap<>();
		param.put("id", userId);
		param.put("parentid", paranetId);
		return userMapper.selectUserMenu(param);
	}

	@Override
	public void deleteUser(Integer id) {
		// 删除sys_user
		userMapper.deleteById(id);
		// 删除sys_user_dept
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("userId", id);
		userDeptMapper.deleteByMap(columnMap);
		// 删除sys_user_role
		userRoleMapper.deleteByMap(columnMap);
	}

	@Override
	public void updateUserState(int id, int state) {
		User user = new User();
		user.setId(id);
		user.setActive(state);
		userMapper.updateById(user);

	}

	@Override
	public int addUser(UserParam user) {
		// sys_user添加
		User userinfo = new User();
		userinfo.setLoginName(user.getLoginName());
		if(user.getType().intValue()==0) {
			//userinfo.setPassword(StringUtils.getMD5(user.getLoginName()+"1234"));
			userinfo.setPassword(user.getPassword());
		}
		userinfo.setEmail(user.getEmail());
		userinfo.setName(user.getName());
		userinfo.setPhone(user.getPhone());
		userinfo.setActive(user.getActive());
		userinfo.setType(user.getType());
		userMapper.insert(userinfo);
		// sys_user_role添加
		if (null != user.getRoleId()) {
			UserRole userrole = new UserRole();
			userrole.setRoleId(user.getRoleId());
			userrole.setUserId(userinfo.getId());
			userRoleMapper.insert(userrole);
		}
		return userinfo.getId();
	}

	@Override
	public Boolean checkLoginName(UserParam user, int flag) {
		Map<String, Object> LoginNameMap = new HashMap<>();
		LoginNameMap.put("loginName", user.getLoginName());
		List<User> userlist = new ArrayList<>();
		if (flag == 1) {
			userlist = userMapper.selectByMap(LoginNameMap);
		} else {
			LoginNameMap.put("id", user.getUserId());
			userlist = userMapper.selectRepeatName(LoginNameMap);
		}
		return evUtil.listIsNullOrZero(userlist);

	}

	@Override
	public int editUser(UserParam user) {
		// sys_user编辑
		User userinfo = new User();
		userinfo.setId(user.getUserId());
		userinfo.setLoginName(user.getLoginName());
		userinfo.setEmail(user.getEmail());
		userinfo.setName(user.getName());
		userinfo.setPhone(user.getPhone());
		userinfo.setActive(user.getActive());
		userinfo.setType(user.getType());
		userMapper.updateUser(userinfo);
		//userMapper.updateById(userinfo);
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("userId", user.getUserId());
		userRoleMapper.deleteByMap(columnMap);
		if (null != user.getRoleId()) {
			// sys_user_role编辑
			UserRole userrole = new UserRole();
			userrole.setUserId(user.getUserId());
			userrole.setRoleId(user.getRoleId());
			userRoleMapper.insert(userrole);
		}
		return user.getUserId();
	}

	@Override
	public List<Map<String, Object>> selectUser() {
		return userMapper.selectUser(null);
	}

	@Override
	public boolean isDefaultAdmin(Integer id) {
		boolean flag = false;
		List<Role> role = userMapper.getRoleType(id);
		if (!evUtil.listIsNullOrZero(role)) {
			if (role.get(0).getType() == 0) {
				flag = true;
			}
		}
		return flag;
	}

	@Override
	public Boolean selectUser(Integer id, String password) {
		Map<String, Object> columnMap=new HashMap<>();
		columnMap.put("id", id);
		columnMap.put("password", password);
		if(evUtil.listIsNullOrZero(userMapper.selectByMap(columnMap))) {
			return false;
		}else {
			return true;
		}
	}

	@Override
	public void updatePassword(Integer id,String password) {
		//User user=userMapper.selectById(id);
		User entity=new User();
		entity.setId(id);
		entity.setPassword(password);
		userMapper.updateById(entity);
	}

	@Override
	public List<Map<String, Object>> selectUserByDept(Integer groupId) {
		return userMapper.selectUserByDept(groupId);
	}

	@Override
	public List<Map<String, Object>> selectUserPersonal(User user) {
		int id=user.getId();
		return userMapper.selectUserPersonal(id);
	}

	@Override
	public void updatePassword(Map<String, Object> map) {
		userMapper.personalUpdate(map);
	}

	@Override
	public User checkUser(User user, Map<String, String> param) {
		User u = null;
		int type = user.getType();
		//普通用户
		if( type == 0){
			if(user.getPassword() != null){
				User entity = new User();
				entity.setLoginName(user.getLoginName());
				entity.setPassword(user.getPassword());
				entity.setActive(1);
				u = userMapper.selectOne(entity);
			}
		}else if( type == 1){
			//域用户
			if(!param.isEmpty()){
				Map<String, Object> userDetailMap = ADAuthUtil.getUserDetail(user.getLoginName(), user.getPassword(), param);
				log.info("ad域验证返回信息："+userDetailMap);
				//校验成功
				if("200".equals(userDetailMap.get("retCode"))){
					User us = new User();
					us.setLoginName(user.getLoginName());
					us.setActive(1);
					us.setType(1);
					u = userMapper.selectOne(us);
					//判断是否同步,电话号码是否为空作为是否更新标志
					if(u.getPhone() == null || u.getPhone().longValue() == 0){
						u.setEmail(evUtil.getMapStrValue(userDetailMap, "mail"));
						u.setName(evUtil.getMapStrValue(userDetailMap, "name"));
						u.setPhone(Long.parseLong(evUtil.getMapStrValue(userDetailMap, "mobile")));
						userMapper.updateAllColumnById(u);
					}
				}
			}
		}
		return u;
	}
	@Override
	public List<Map<String, Object>> selectDetail(Integer id) {
		return null;
	}

	@Override
	public List<Map<String, Object>> selectGroupUser(Integer userId) {

		return userMapper.selectGroupUser(userId);
	}

	@Override
	public List<Map<String, Object>> selectUserByReportId(int id) {
		Map<String,Object> map = new HashMap<>(3);
		map.put("id", id);
		return userMapper.selectGroupUserByReportId(map);
	}

	@Override
	public List<Map<String, Object>> selectUsersExcludeSelf(Integer userId, Integer active) {
		Map<String,Object> map = new HashMap<>();
		map.put("id", userId);
		map.put("active", active);
		return userMapper.selectGroupUserForAll(map);
	}

	@Override
	public void insertUserLogs(UserLogs userlog) {
		userLogsMapper.insert(userlog);
	}

	@Override
	public List<UserLogs> selectUserLogs(UserLogsParam param) {
		Long startTime=param.getStartTime();
		Long endTime=param.getEndTime();
		String user=param.getUser();
		Integer type=param.getType();
		EntityWrapper<UserLogs> wrpper=new EntityWrapper<>();
		wrpper.ge("roleType",param.getRoleType());
		if(null!=startTime){
			wrpper.ge("time",new Date(startTime));
		}
		if(null!=endTime){
			wrpper.le("time",new Date(endTime));
		}
		if(null!=user){
			wrpper.like("user",user);
		}
		if(null!=type){
			wrpper.eq("type",type);
		}
		wrpper.orderBy("time",false);
		return userLogsMapper.selectList(wrpper);
	}

	@Override
	public Role getUserRole(Integer userId) {
		List<Role> role = userMapper.getRoleType(userId);
		if (!evUtil.listIsNullOrZero(role)) {
			return role.get(0);
		}else {
			return null;
		}
	}
}
