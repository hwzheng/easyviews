package com.greattimes.ev.system.mapper;

import com.greattimes.ev.system.entity.UserLogs;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 用户操作日志表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2020-02-25
 */
public interface UserLogsMapper extends BaseMapper<UserLogs> {

}
