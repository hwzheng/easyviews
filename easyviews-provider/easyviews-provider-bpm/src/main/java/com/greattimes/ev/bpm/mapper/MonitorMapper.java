package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Monitor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 监控点表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-07
 */
public interface MonitorMapper extends BaseMapper<Monitor> {

}
