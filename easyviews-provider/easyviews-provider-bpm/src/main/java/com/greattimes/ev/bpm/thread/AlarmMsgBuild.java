package com.greattimes.ev.bpm.thread;

import com.greattimes.ev.bpm.alarm.param.req.AlarmGroupConfigParam;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageProducerService;
import com.greattimes.ev.bpm.start.Start;
import com.greattimes.ev.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * @author NJ
 * @date 2019/1/8 13:48
 */
public class AlarmMsgBuild implements Callable<Boolean>{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private AlarmGroupConfigParam config;

    @Override
    public Boolean call() throws Exception {
        try {
            log.info("进入执行线程时间:{}", DateUtils.currentDatetime());
            IAlarmMassageProducerService service = (IAlarmMassageProducerService) Start.content.getBean("alarmMassageProducerService");
            service.insertTypeDistribute(config);
            //很重要
            if (Thread.interrupted()){
                log.info("AlarmMsgBuild is interrupted !");
                return false;
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return true;
    }

    public AlarmGroupConfigParam getConfig() {
        return config;
    }

    public void setConfig(AlarmGroupConfigParam config) {
        this.config = config;
    }

}
