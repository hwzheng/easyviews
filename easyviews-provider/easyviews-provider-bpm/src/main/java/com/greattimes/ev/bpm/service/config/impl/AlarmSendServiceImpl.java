package com.greattimes.ev.bpm.service.config.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.req.AlarmSendListParam;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.greattimes.ev.bpm.entity.AlarmSend;
import com.greattimes.ev.bpm.mapper.AlarmGroupMapper;
import com.greattimes.ev.bpm.mapper.AlarmSendMapper;
import com.greattimes.ev.bpm.service.config.IAlarmSendService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 告警信息发送配置表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@Service
public class AlarmSendServiceImpl extends ServiceImpl<AlarmSendMapper, AlarmSend> implements IAlarmSendService {
	@Autowired
	private AlarmSendMapper alarmSendMapper;
	@Autowired
	private AlarmGroupMapper alarmGroupMapper;

	@Override
	public List<AlarmSend> selectAlarmSend(Map<String, Object> map) {
		Map<String, Object> colmap = new HashMap<String, Object>();
		colmap.put("applicationId", evUtil.getMapIntValue(map, "applicationId"));
		return alarmSendMapper.selectByMap(colmap);
	}

	@Override
	public void addAlarmSend(AlarmSend alarmSend) {
		alarmSendMapper.insert(alarmSend);
	}

	@Override
	public void editAlarmSend(AlarmSend alarmSend) {
		alarmSendMapper.updateAllColumnById(alarmSend);
	}

	@Override
	public List<AlarmGroup> selectAlarmGroup() {
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("active", 1);
		return alarmGroupMapper.selectByMap(columnMap);
	}

	@Override
	public void deleteAlarmSend(int id) {
		alarmSendMapper.deleteById(id);
	}

	@Override
	public void updateAlarmSendActive(AlarmSendListParam param) {
		List<AlarmSend> list=param.getAlarmSend();
		for (AlarmSend alarmSend : list) {
			alarmSendMapper.updateById(alarmSend);
		}
	}

}
