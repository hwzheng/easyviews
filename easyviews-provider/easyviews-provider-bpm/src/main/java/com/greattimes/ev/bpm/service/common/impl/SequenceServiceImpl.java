package com.greattimes.ev.bpm.service.common.impl;

import com.greattimes.ev.bpm.mapper.SequenceMapper;
import com.greattimes.ev.bpm.service.common.ISequenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author LuoX
 * @since 2017-04-19
 */
@Service
public class SequenceServiceImpl implements ISequenceService {
	@Autowired
	SequenceMapper sequenceMapper;
	@Override
	public int sequenceNextVal(String sequenceName) {
		  Map<String,Object> map=new HashMap<String,Object>();
	      map.put("sequenceName", sequenceName);
		  return sequenceMapper.nextVal(map);
	}
	
}
