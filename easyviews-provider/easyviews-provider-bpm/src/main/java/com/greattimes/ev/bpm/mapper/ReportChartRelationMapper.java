package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartRelation;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表图表绑定关系表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartRelationMapper extends BaseMapper<ReportChartRelation> {

	void batchInsert(@Param("reportId")Integer reportId,@Param("list") List<Integer> ids);

}
