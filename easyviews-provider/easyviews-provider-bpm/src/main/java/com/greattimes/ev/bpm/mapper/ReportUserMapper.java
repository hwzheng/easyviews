package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportUser;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表所属人员表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportUserMapper extends BaseMapper<ReportUser> {

	void batchInsert(@Param("list") List<Integer> list,@Param("id") int id);

}
