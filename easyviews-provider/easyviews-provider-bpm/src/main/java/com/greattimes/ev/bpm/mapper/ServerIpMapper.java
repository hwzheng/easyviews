package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.resp.IpPortParam;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.entity.ServerIp;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 组件IP表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface ServerIpMapper extends BaseMapper<ServerIp> {
    void batchInsert(List<ServerIp> list);
    void batchDeleteByserverGroupIds(List<Integer> list);
	/**
	 * 根据组件获取ip
	 * @param componentId
	 * @return
	 */
	List<DataSourcesRelationParam> selectByComponentId(Integer componentId);

    /**
     * 根据服务组id获取IP port
     * @param serverGroupId
     * @return
     */
	List<DataSourcesRelationParam> selectIpAndport(Integer serverGroupId);

	/**
	 * 根据id批量删除ServerIp
	 * @param list
	 */
	void batchDeleteByIds(List<Integer> list);

	/**
	 * 查询ip
	 * @param list
	 * @author: nj
	 * @date: 2020-03-12 10:54
	 * @version: 0.0.1
	 * @return: java.util.List<com.greattimes.ev.bpm.entity.ServerIp>
	 */ 
	List<ServerIp> selectIpByIpStr(@Param(value = "list") List<String> list);

	/**
	 * 查询ip_port视图
	 * @param list
	 * @author: nj
	 * @date: 2020-03-12 16:54
	 * @version: 0.0.1
	 * @return: java.util.List<com.greattimes.ev.bpm.config.param.resp.IpPortParam>
	 */
	List<IpPortParam> selectIpPortParam(@Param(value = "list") List<String> list);

}
