package com.greattimes.ev.bpm.thread;

import com.greattimes.ev.bpm.alarm.param.req.KafKaConfigParam;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageSendService;
import com.greattimes.ev.bpm.start.Start;
import com.greattimes.ev.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;

/**
 * @author NJ
 * @date 2019/1/8 13:48
 */
public class AlarmMessageSendBuild implements Callable<Boolean>{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private KafKaConfigParam config;

    @Override
    public Boolean call() throws Exception {
        try {
            log.info("进入执行线程时间:{}", DateUtils.currentDatetime());
            IAlarmMassageSendService service = (IAlarmMassageSendService) Start.content.getBean("alarmMassageSendService");
            service.updateMessageAndSend(config);
            //很重要
            if (Thread.interrupted()){
                log.info("AlarmMessageSendBuild is interrupted !");
                return false;
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return true;
    }

    public KafKaConfigParam getConfig() {
        return config;
    }

    public void setConfig(KafKaConfigParam config) {
        this.config = config;
    }

}
