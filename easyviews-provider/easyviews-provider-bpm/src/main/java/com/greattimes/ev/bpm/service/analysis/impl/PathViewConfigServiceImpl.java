package com.greattimes.ev.bpm.service.analysis.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.mapper.*;
import com.greattimes.ev.bpm.service.analysis.IPathViewConfigService;
import com.greattimes.ev.common.utils.evUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2018/9/11 19:54
 */
@Service
public class PathViewConfigServiceImpl implements IPathViewConfigService{

    @Autowired
    private ApplicationMapper applicationMapper;
    @Autowired
    private MonitorMapper monitorMapper;
    @Autowired
    private PathViewMapper pathViewMapper;
    @Autowired
    private PathLineMapper pathLineMapper;
    @Autowired
    private PathNodeMapper pathNodeMapper;
    @Autowired
    private PathAreaMapper pathAreaMapper;
    @Autowired
    private UuidMapper uuidMapper;
    @Autowired
    private ComponentMapper componentMapper;

    @Autowired
    private MonitorComponentMapper monitorComponentMapper;

    @Override
    public List<ApplicationTree> findApplicationTree(Integer userId, List<Integer> appIds) {
        /**
         * to get id from bpm_uuid
         */
        List<Integer> applicationIds = new ArrayList<>();
        if(evUtil.listIsNullOrZero(applicationIds)){
            List<ApplicationParam> applicationParams = applicationMapper.selectApplicationByUserId(userId,1);
            applicationIds = applicationParams.stream().map(ApplicationParam::getId).collect(Collectors.toList());
        }else{
            applicationIds = appIds;
        }
        StringBuilder sb = new StringBuilder("");
        applicationIds.stream().forEach(x->{
            sb.append("applicationid:").append(x).append("|");
        });
        Map<String,Object> uuidMap = new HashMap<>();
        //use REGEXP to match some applicationIds
        if(sb.toString().length() >= 0){
            List<Uuid> uuids = uuidMapper.findUuidByVaule(sb.substring(0,sb.length()-1).toString());
            // to make map(key:value[applicationid:1...],value:id)
            uuids.stream().forEach(x->{
                uuidMap.put(x.getValue(), x.getId());
            });
        }else{
            return new ArrayList<>();
        }

        //all application -> monitor -> (component)
        List<Map<String, Object>> treeData = applicationMapper.findAppTreeData(userId, evUtil.listIsNullOrZero(appIds) == true?null:appIds);
        //group by monitorId to get componentNode
        Map<Integer,List<ApplicationTree>> groupMonitorResult = new LinkedHashMap<>();
        //group by application to get monitorNode
        Map<Integer, Map<Integer,ApplicationTree>> groupApplicationResult = new LinkedHashMap<>();
        Map<Integer, ApplicationTree> applicationTreeMap = new LinkedHashMap<>();
        for(Map<String, Object> data : treeData){
            Integer monitorId = (Integer)data.get("monitorId");
            Integer applicationId = (Integer)data.get("applicationId");
            Integer componentId = (Integer)data.get("componentId");

            Object monActive = data.get("monActive");
            Object comActive = data.get("comActive");

            applicationTreeMap.put(applicationId,this.createApplicationTreeNode(data,1,uuidMap));

            //增加是否激活判断
            if(monitorId != null && monActive != null && Integer.parseInt(monActive.toString())== 1){
                groupApplicationResult.computeIfAbsent(applicationId,
                        LinkedHashMap::new).put(monitorId, this.createApplicationTreeNode(data,2,uuidMap));
            }
            //增加是否激活判断
            if(componentId != null && comActive != null && Integer.parseInt(comActive.toString())== 1){
                groupMonitorResult.computeIfAbsent(monitorId, ArrayList :: new)
                        .add(this.createApplicationTreeNode(data,3,uuidMap));
            }
        }

        //assemble the tree leftNode to rootNode
        List<ApplicationTree> applicationTrees = new ArrayList<>();


        applicationTreeMap.forEach((x,y)->{
            List<ApplicationTree> treeList = new ArrayList<>();
            if(!groupApplicationResult.isEmpty() && groupApplicationResult.get(x) != null){
                groupApplicationResult.get(x).forEach((z,h)->{
                    //z:monitorId  h:monitorNode
                    h.setChildren(groupMonitorResult.get(z));
                    treeList.add(h);
                });
            }
            y.setChildren(treeList);
            applicationTrees.add(y);
        });

//        groupApplicationResult.forEach((x,y)->{
//            //x:applicationId
//            List<ApplicationTree> treeList = new ArrayList<>();
//            y.forEach((n,c)->{
//                //n:monitorId  c:monitorNode
//                c.setChildren(groupMonitorResult.get(n));
//                treeList.add(c);
//            });
//            applicationTreeMap.get(x).setChildren(treeList);
//            applicationTrees.add(applicationTreeMap.get(x));
//        });

        return applicationTrees;
    }

    @Override
    public Map<String, Object> findPathViewDetail(int applicationId) {
        Map<String, Object> result = new HashMap<>(3);
        List<Object> emptyList = Collections.emptyList();

        //path_view
        EntityWrapper<PathView> pathViewEntityWrapper = new EntityWrapper<>();
        pathViewEntityWrapper.where("applicationId={0}", applicationId);
        List<PathView> pathViews = pathViewMapper.selectList(pathViewEntityWrapper);

        if(!evUtil.listIsNullOrZero(pathViews)){
            List<Integer> viewIds = pathViews.stream().map(PathView::getId).collect(Collectors.toList());
            Map<Integer,Integer> pathViewMap = new HashMap<>();
            for(PathView pathView : pathViews){
                pathViewMap.put(pathView.getId(), pathView.getApplicationId());
            }

            //path_line
            EntityWrapper<PathLine> pathLineEntityWrapper = new EntityWrapper<>();
            pathLineEntityWrapper.in("viewId",viewIds);
            result.put("pathLine", pathLineMapper.selectList(pathLineEntityWrapper));

            //path_area
            EntityWrapper<PathArea> pathAreaEntityWrapper = new EntityWrapper<>();
            pathAreaEntityWrapper.in("viewId",viewIds);
            result.put("pathArea", pathAreaMapper.selectList(pathAreaEntityWrapper));

            //path_node
            EntityWrapper<PathNode> pathNodeEntityWrapper = new EntityWrapper<>();
            pathNodeEntityWrapper.in("viewId",viewIds);

            // find all bpm_component to map(key:componentId;value:name)
            Map<Integer, Component> componentMap = new HashMap<>();
            List<Component> components = componentMapper.selectList(null);
            components.stream().forEach(x->{
                componentMap.put(x.getId(), x);
            });
            // bpm_application the same as above
            Map<Integer, Application> applicaitonMap = new HashMap<>();
            List<Application> applications = applicationMapper.selectList(null);
            applications.stream().forEach(x->{
                applicaitonMap.put(x.getId(), x);
            });
            //bpm_monitor the same as above
            Map<Integer,Monitor> monitorMap = new HashMap<>();
            List<Monitor> monitors = monitorMapper.selectList(null);
            monitors.stream().forEach(x->{
                monitorMap.put(x.getId(),x);
            });

            //bpm_monitor_component
            Map<Integer,Integer> monitorComponentMap = new HashMap<>();
            List<MonitorComponent> monitorComponents = monitorComponentMapper.selectList(null);
            monitorComponents.stream().forEach(x->{
                monitorComponentMap.put(x.getComponentId(),x.getMonitorId());
            });

            List<PathNode> pathNodes =  pathNodeMapper.selectList(pathNodeEntityWrapper);

            //todo 配置管理做了修改，路径图也相应进行修改(监控点,组件) 应用不考虑
            Iterator<PathNode> pathNodeIterable = pathNodes.iterator();
            while (pathNodeIterable.hasNext()){
                PathNode x = pathNodeIterable.next();
                if(x.getNodeType() == 1){
                    Application app = applicaitonMap.get(x.getRelevanceId());
                    x.setName(app.getName());
                    x.setActive(app.getActive());
                    x.setApplicationId(app.getId());
                }else if(x.getNodeType() == 2){
                    Monitor monitor = monitorMap.get(x.getRelevanceId());
                    x.setName(monitor.getName());
                    x.setActive(monitor.getActive());
                    x.setApplicationId(monitor.getApplicationid());
                }else if(x.getNodeType() == 3){
                    Component com = componentMap.get(x.getRelevanceId());
                    x.setName(com.getName());
                    x.setActive(com.getActive());
                    x.setApplicationId(com.getApplicationId());
                    x.setMonitorId(monitorComponentMap.get(x.getRelevanceId()));
                }else if(x.getNodeType() == 4){
                    //子节点
                    x.setName("");
                    x.setActive(null);
                    x.setApplicationId(pathViewMap.get(x.getViewId()));
                }
            }
            result.put("pathNode", pathNodes);
        }else{
            result.put("pathLine",emptyList);
            result.put("pathArea",emptyList);
            result.put("pathNode",emptyList);
        }
        return result;
    }

    @Override
    public boolean savePathViewConfig(Map<String, Object> map){
        //editing mode, delete original config firstly
        EntityWrapper<PathView> pathViewEntityWrapper = new EntityWrapper<>();
        pathViewEntityWrapper.where("applicationId={0}", map.get("applicationId"));
        List<PathView> originPathViews = pathViewMapper.selectList(pathViewEntityWrapper);
        if(!evUtil.listIsNullOrZero(originPathViews)){
            Map<String, Object> paramMap = new HashMap<>(1);
            paramMap.put("viewId", originPathViews.get(0).getId());
            //path_view
            pathViewMapper.delete(pathViewEntityWrapper);
            //path_line
            pathLineMapper.deleteByMap(paramMap);
            //path_area
            pathAreaMapper.deleteByMap(paramMap);
            //path_node
            pathNodeMapper.deleteByMap(paramMap);
        }

        //path_view
        PathView pathView = new PathView();
        pathView.setApplicationId((Integer)map.get("applicationId"));
        //类型 1 应用  2 自定义
        pathView.setType((Integer)map.get("type"));
        pathViewMapper.insert(pathView);

        int viewId = pathView.getId();
        //path_line
        List<PathLine> pathLines = (List<PathLine>)map.get("pathLine");
        pathLines.stream().forEach(line->{
            line.setViewId(viewId);
        });
        if(!evUtil.listIsNullOrZero(pathLines)){
            pathLineMapper.batchInsert(pathLines);
        }
        //path_area
        List<PathArea> pathAreas = (List<PathArea>)map.get("pathArea");
        pathAreas.stream().forEach(area->{
            area.setViewId(viewId);
        });
        if(!evUtil.listIsNullOrZero(pathAreas)){
            pathAreaMapper.batchInsert(pathAreas);
        }

        //path_node
        List<PathNode> pathNodes = (List<PathNode>)map.get("pathNode");
        pathNodes.stream().forEach(node->{
            node.setViewId(viewId);
        });
        if(!evUtil.listIsNullOrZero(pathNodes)){
            pathNodeMapper.batchInsert(pathNodes);
        }
        return true;
    }

    /**
     * map to ApplicationTreeNode
     * @author NJ
     * @date 2018/9/13 11:04
     * @param map
     * @param nodeType   1:应用 2:监控点 3:组件 4监控点数据
     * @param uuidMap  匹配uuid
     * @return com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree
     */
    private ApplicationTree createApplicationTreeNode(Map<String, Object> map, Integer nodeType, Map<String, Object> uuidMap){
        ApplicationTree applicationTree = new ApplicationTree();
        Integer applicationId = (Integer)map.get("applicationId");
        Integer monitorId =  (Integer)map.get("monitorId");
        Integer componentId =  (Integer)map.get("componentId");
        if(nodeType == 1){
            applicationTree.setTitle(map.get("applicationName").toString());
            applicationTree.setRelevanceId(applicationId);
            applicationTree.setNodeType(nodeType);
            applicationTree.setParentId(null);
            applicationTree.setId(applicationId);
        }else if(nodeType == 2){
            applicationTree.setTitle(map.get("monitorName").toString());
            applicationTree.setRelevanceId(monitorId);
            applicationTree.setNodeType(nodeType);
            applicationTree.setParentId(applicationId);
            applicationTree.setId(monitorId);
        }else if(nodeType == 3){
            applicationTree.setTitle(map.get("componentName").toString());
            applicationTree.setRelevanceId(componentId);
            applicationTree.setNodeType(nodeType);
            applicationTree.setParentId(monitorId);
            applicationTree.setChildren(null);
            applicationTree.setId(componentId);
        }else{

        }
        return applicationTree;
    }

    @Override
    public boolean deletePathViewByComponentIds(List<Integer> ids) {
        if(evUtil.listIsNullOrZero(ids)){
            return false;
        }

        /**
         * 节点类型 1 应用 2 监控点 3 组件 4 子节点
         */
        // 3 组件
        List<Integer> pathNodeIds = new ArrayList<>();
        EntityWrapper<PathNode> pathNodeComponent = new EntityWrapper<>();
        pathNodeComponent.where("nodeType={0}", 3).in("relevanceId", ids);
        List<PathNode> cPathNodes = pathNodeMapper.selectList(pathNodeComponent);
        pathNodeMapper.delete(pathNodeComponent);
        if(!evUtil.listIsNullOrZero(cPathNodes)){
            pathNodeIds = cPathNodes.stream().map(PathNode::getId).collect(Collectors.toList());
            //4 子节点
            List<PathNode> leafNodes = pathNodeMapper.findNodeByIds(pathNodeIds, 4);
            if(!evUtil.listIsNullOrZero(leafNodes)){
                List<Integer> leafNodeIds = leafNodes.stream().map(PathNode::getId).collect(Collectors.toList());
                EntityWrapper<PathNode> pathNodeLeaf = new EntityWrapper<>();
                pathNodeLeaf.in("id", leafNodeIds);
                pathNodeMapper.delete(pathNodeLeaf);
                pathNodeIds.addAll(leafNodeIds);
            }
            //path_line
            EntityWrapper<PathLine> pathLineEntityWrapper = new EntityWrapper<>();
            pathLineEntityWrapper.in("start", pathNodeIds).or().in("end", pathNodeIds);
            pathLineMapper.delete(pathLineEntityWrapper);
        }
        return true;
    }

    @Override
    public boolean deletePathViewByMonitorIds(List<Integer> ids) {

        /**
         * 节点类型 1 应用 2 监控点 3 组件 4 子节点
         */

        //delete type == 2 监控点
        EntityWrapper<PathNode> pathNodeMonitor = new EntityWrapper<>();
        pathNodeMonitor.where("nodeType={0}", 2).in("relevanceId", ids);
        List<PathNode> mPathNodes = pathNodeMapper.selectList(pathNodeMonitor);
        pathNodeMapper.delete(pathNodeMonitor);

        //path_line
        if(!evUtil.listIsNullOrZero(mPathNodes)){
            List<Integer> pathNodeIds = mPathNodes.stream().map(PathNode::getId).collect(Collectors.toList());
            EntityWrapper<PathLine> pathLineEntityWrapper = new EntityWrapper<>();
            pathLineEntityWrapper.in("start", pathNodeIds).or().in("end", pathNodeIds);
            pathLineMapper.delete(pathLineEntityWrapper);
        }
        return true;
    }
}
