package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.BottleneckTask;

/**
 * <p>
 * 智能瓶颈报表任务表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface BottleneckTaskMapper extends BaseMapper<BottleneckTask> {


	List<Map<String, Object>> getSingleTaskDetail(int itemId);

	List<Map<String, Object>> getTimeTaskDetail(int itemId);

    List<Map<String, Object>> selectTaskDataByItemDayId(Map<String, Object> map);

	List<Map<String, Object>> selectTask(Map<String, Object> map);

}
