package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.FilterFieldValue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 自定义数据过滤字段值表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface FilterFieldValueMapper extends BaseMapper<FilterFieldValue> {

}
