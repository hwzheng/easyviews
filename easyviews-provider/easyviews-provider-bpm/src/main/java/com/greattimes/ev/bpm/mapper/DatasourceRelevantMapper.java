package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.datasources.param.req.DatasourceRelevantParam;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;

/**
 * <p>
 * 数据源关联表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
public interface DatasourceRelevantMapper extends BaseMapper<DatasourceRelevant> {

	/**
	 * 根据应用id查询外部数据源配置
	 * @param applicationId
	 * @return
	 */
	List<DatasourceRelevantParam> selectDatasource(int applicationId);

	/**
	 * 更新
	 * @param re
	 */
	void update(DatasourceRelevant re);

}
