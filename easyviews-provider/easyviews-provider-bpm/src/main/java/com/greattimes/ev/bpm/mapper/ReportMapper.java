package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Report;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportMapper extends BaseMapper<Report> {

	List<Map<String, Object>> getMyReport(@Param("userId")int userId,@Param("orderBy") int orderBy);

	List<Map<String, Object>> getAllReport(@Param("userId")int userId,@Param("orderBy") int orderBy);

	List<Map<String, Object>> getMyChart(@Param("userId")int userId,@Param("orderBy") int orderBy,@Param("dataType") int dataType);

	List<Map<String, Object>> getAllChart(@Param("userId")int userId,@Param("orderBy") int orderBy,@Param("dataType") int dataType);

}
