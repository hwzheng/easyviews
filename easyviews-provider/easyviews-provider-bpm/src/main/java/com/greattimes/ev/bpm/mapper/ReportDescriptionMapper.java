package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportDescription;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表描述表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportDescriptionMapper extends BaseMapper<ReportDescription> {

	List<Map<String, Object>> selectDescriptionList(Integer reportId);

}
