package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.CompIndConstraint;

/**
 * <p>
 * 组件指标约束 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface CompIndConstraintMapper extends BaseMapper<CompIndConstraint> {

	/**
	 * 根据约束组id查询约束值
	 * @param constraintGroupId
	 * @return
	 */
	List<Map<String, Object>> selectByGroupId(Integer constraintGroupId);

}
