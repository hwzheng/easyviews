package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.datasources.param.req.IndicatorGroupParam;
import com.greattimes.ev.bpm.entity.IndicatorGroup;

/**
 * <p>
 * 指标组 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface IndicatorGroupMapper extends BaseMapper<IndicatorGroup> {

    IndicatorGroupParam findIndicatorById(@Param("id") int id);

	/**
	 * 指标组下拉列表查询
	 * @param sourcesId
	 * @return
	 */
	List<Map<String, Object>> selectIndicatorGroup(Integer sourcesId);


	List<IndicatorGroupParam> findIndicatorGroupListBySourcesId(@Param("sourcesId") int sourcesId);

	/**
	 * 获取指标组的挂载点或关联层级
	 * @author cgc  
	 * @date 2018年9月26日  
	 * @return
	 */
	List<Map<String, Object>> selectRelevant();
}
