package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartDataFilter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表数据维度过滤表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartDataFilterMapper extends BaseMapper<ReportChartDataFilter> {

    /**
     * batch save
     * @param list
     */
    void batchInsert(List<ReportChartDataFilter> list);
}
