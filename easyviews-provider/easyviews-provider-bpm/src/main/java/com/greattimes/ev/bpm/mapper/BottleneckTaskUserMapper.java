package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskUser;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表用户表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-29
 */
public interface BottleneckTaskUserMapper extends BaseMapper<BottleneckTaskUser> {

	void batchInsert(@Param("list") List<Integer> list,@Param("id") int id);


}
