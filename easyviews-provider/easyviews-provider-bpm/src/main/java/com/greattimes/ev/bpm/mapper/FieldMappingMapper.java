package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.resp.TranslateDataParam;
import com.greattimes.ev.bpm.entity.FieldMapping;

/**
 * <p>
 * 维度字段映射表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
public interface FieldMappingMapper extends BaseMapper<FieldMapping> {

	/**批量插入维度字段映射表
	 * @param details
	 */
	void insertFieldMapping(List<FieldMapping> details);

	/**
	 * 获取翻译
	 * @author cgc  
	 * @date 2018年10月10日  
	 * @param uuid
	 * @return
	 */
	List<TranslateDataParam> getComponentTranslate(Long uuid);

	int getMappingCount(@Param("componentId")int componentId,@Param("dimensionId") int dimensionId);

	/**
	 * 根据组件id和维度id集合查询维度值
	 * @author NJ
	 * @date 2018/12/13 16:02
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> getMappingCountByDimensionIds(Map<String, Object> map);


	/**
	 * 根据维度值查询翻译
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 * @throw
	 * @author NJ
	 * @date 2019-04-27 13:12
	*/
	List<Map<String, Object>> getFieldMappingByValues(Map<String, Object> map);




}
