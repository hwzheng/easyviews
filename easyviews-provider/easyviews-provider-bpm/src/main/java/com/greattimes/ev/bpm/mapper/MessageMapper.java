package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.system.entity.Message;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 短信 发送表 Mapper 接口
 * </p>
 *
 * @author LiHua
 * @since 2018-10-10
 */
public interface MessageMapper extends BaseMapper<Message> {

    /**
     * 告警发送信息批量插入
     * @param list
     * @return
     */
    int batchInsert(List<Message> list);

    /**
     * 查询所有的告警组人员
     * @param
     * @return
     */
    List<Map<String, Object>> selectUserListByParam(Map<String, Object> map);

    /**
     * 根据告警组id，查询用户列表（不去重，排序）
     * @param map
     * @return
     */
    List<Map<String, Object>> selectOutBoundUserListByAlarmGroupIds(Map<String, Object> map);

    /**
     * 批量更新告警信息状态
     * @param map
     */
    void updateAlarmMessageStateBatch(Map<String, Object> map);

    /**
     * 根据信息的状态和要发送的方式查询信息
     * @param map
     * @return
     */
    List<Message> selectMessageByTypeAndState(Map<String, Object> map);

    /**
     * 查询告警组相关人员
     * @param map
     * @return
     */
    List<AlarmGroupUserParam> findAlarmGroupUserById(Map<String, Object> map);

    /**
     * 根据id批量更新状态eventState状态
     * @param ids
     * @param current
     * @return
     */
    void updateBatchEventState(@Param("ids") List<Integer> ids, @Param("eventState") long current);

    /**
     * 更新kafka推送状态
     * @param ids
     * @param state
     */

    void updateBatchKafkaState(@Param("ids") List<Integer> ids, @Param("state") int state);
}
