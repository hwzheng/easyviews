package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.analysis.param.resp.ReportDescriptionLocationParam;
import com.greattimes.ev.bpm.entity.ReportDescriptionLocation;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表描述位置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-27
 */
public interface ReportDescriptionLocationMapper extends BaseMapper<ReportDescriptionLocation> {

	void batchUpdate(@Param("list")List<ReportDescriptionLocationParam> description);

}
