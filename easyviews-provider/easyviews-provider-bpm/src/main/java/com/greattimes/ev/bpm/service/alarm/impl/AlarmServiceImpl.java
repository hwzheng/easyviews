package com.greattimes.ev.bpm.service.alarm.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.alarm.param.req.AlarmInfoParam;
import com.greattimes.ev.bpm.entity.AlarmInfo;
import com.greattimes.ev.bpm.entity.ApplicationAlarm;
import com.greattimes.ev.bpm.mapper.AbnormalinfoMapper;
import com.greattimes.ev.bpm.mapper.AlarmInfoMapper;
import com.greattimes.ev.bpm.mapper.ApplicationAlarmMapper;
import com.greattimes.ev.bpm.service.alarm.IAlarmService;
import com.greattimes.ev.utils.IndicatorSupportUtil;
import com.greattimes.ev.common.utils.evUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2018/9/20 20:10
 */
@Service
public class AlarmServiceImpl extends ServiceImpl<AlarmInfoMapper, AlarmInfo> implements IAlarmService {

    @Autowired
    private AlarmInfoMapper alarmInfoMapper;

    @Autowired
    private ApplicationAlarmMapper applicationAlarmMapper;

    @Autowired
    private AbnormalinfoMapper abnormalinfoMapper;

    @Override
    public Map<String, Object> selectApplicationAlarm(List<Integer> applicationIds, Long start, Long end, int interval) {
        Map<String, Object> result = new HashMap<>();
        EntityWrapper<ApplicationAlarm> entityWrapper = new EntityWrapper<>();
        if(evUtil.listIsNullOrZero(applicationIds)){
            return result;
        }
        //condition
        if(applicationIds.size() == 1){
            entityWrapper.where("applicationid={0}", applicationIds.get(0));
            if(start.equals(end)){
                entityWrapper.and("timestamp={0}", start);
            }else{
                entityWrapper.ge("timestamp",start);
                entityWrapper.le("timestamp",end);
            }
        }else{
            if(start.equals(end)){
                entityWrapper.and("timestamp={0}", start);
            }else{
                entityWrapper.ge("timestamp",start);
                entityWrapper.le("timestamp",end);
            }
            entityWrapper.in("applicationid", applicationIds);
        }
        entityWrapper.orderBy("timestamp");
        List<ApplicationAlarm> applicationAlarms = applicationAlarmMapper.selectList(entityWrapper);
        List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start,end,interval);
        //group by applicationId
        Map<Integer, List<ApplicationAlarm>> groupAlarm =  applicationAlarms.stream().collect(Collectors.groupingBy(ApplicationAlarm::getApplicationid, Collectors.toList()));
        Map<Integer, List<ApplicationAlarm>> resultGroupAlarm = new HashMap<>();

        applicationIds.forEach(x -> resultGroupAlarm.put(x, initDefaultApplicationAlarm(groupAlarm.computeIfAbsent(x, ArrayList :: new), timeList)));

        List<Map<String, Object>> alarmData = new ArrayList<>();
        for (Map.Entry<Integer, List<ApplicationAlarm>> entry : resultGroupAlarm.entrySet()) {
            Map<String, Object> transDataMap = new HashMap<>();
            transDataMap.put("uuid", entry.getKey());
            transDataMap.put("data", entry.getValue());
            alarmData.add(transDataMap);
        }
        result.put("time", timeList);
        result.put("alarmData", alarmData);
        return result;
    }

    @Override
    public Map<String, Object> selectOtherAlarmAmount(Map<String, Object> map) {
        List<Map<String, Object>> uuids  = (List)map.get("uuids");
        int type = evUtil.getMapIntValue(map, "type");
        List<Map<String, Object>> data;
        //需要关联告警子表alarmDimension
        if(type == 6 || type == 7 || type == 8){
            data = alarmInfoMapper.selectOtherAlarmAmountByDimensionValue(map);
        }else{
            data = alarmInfoMapper.selectOtherAlarmAmount(map);
        }
        long start = evUtil.getMapLongValue(map, "start");
        long end = evUtil.getMapLongValue(map, "end");
//        List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end);
        List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, evUtil.getMapIntValue(map, "interval"));
        //assembly data
        List<Map<String, Object>> dataList= new ArrayList<>();
        for(Map<String, Object> con : uuids){
            List<Integer> uuidValue = (List<Integer>)con.get("value");
            for(Integer i : uuidValue){
                Map<String, Object> amountMap = new HashMap<>();
                amountMap.put("uuid", i);
                amountMap.put("amount",IndicatorSupportUtil.getFormatValue(i,data,timeList,"amount",null));
                dataList.add(amountMap);
            }
        }
        Map<String, Object> result = new HashMap<>();
        result.put("time", timeList);
        result.put("alarmData", dataList);
        return result;
    }

    @Override
    public List<Map<String, Object>> selectOtherSingleAlarmAmount(Map<String, Object> map) {
        Map<String, Object> paramMap = new HashMap<>();
        if(map.get("ids") == null ){
            return Collections.emptyList();
        }
        int type = evUtil.getMapIntValue(map, "type");
        List<Map<String, Object>> uuids = new ArrayList<>();
        paramMap.put("start", evUtil.getMapLongValue(map, "start"));
        paramMap.put("end", evUtil.getMapLongValue(map, "end"));
        Map<String, Object> idsMap = new HashMap<>();
        idsMap.put("value", map.get("ids"));
        idsMap.put("type", map.get("type"));
        uuids.add(idsMap);
        paramMap.put("uuids", uuids);

        List<Map<String, Object>> dataList;
        //需要关联告警子表alarmDimension
        if(type == 6 || type == 7 || type == 8){
            dataList = alarmInfoMapper.selectOtherAlarmAmountByDimensionValue(paramMap);
        }else{
            dataList = alarmInfoMapper.selectOtherAlarmAmount(paramMap);
        }
        return dataList;
    }

    @Override
    public void updateAlarmInfoOpinion(List<Integer> ids, int manageState, String managerdsec) {
        Map<String, Object> param = new HashMap<>();
        param.put("ids", ids);
        param.put("manageState", manageState);
        param.put("managerdsec", managerdsec);
        alarmInfoMapper.updateBatchAlarmInfoOpinionByMap(param);
    }

    @Override
    public AlarmInfo selectAlarmInfoById(int id) {
        return alarmInfoMapper.selectById(id);
    }

    @Deprecated
    @Override
    public Page<AlarmInfo> selectPageByIdAndType(Page<AlarmInfo> page, Map<String, Object> map) {
        page.setRecords(alarmInfoMapper.selectPageByIdAndType(page, map));
        return page;
    }

    /**
     * init default list
     * @param data
     * @param timeList
     * @return
     */
    private List<ApplicationAlarm> initDefaultApplicationAlarm(List<ApplicationAlarm> data, List<Long> timeList ){
        List<ApplicationAlarm> result = new ArrayList<>();
        boolean flag;
        for(Long time : timeList){
            flag = false;
            for(ApplicationAlarm  alarm : data){
                if(time.longValue() == alarm.getTimestamp().longValue()){
                    result.add(alarm);
                    flag  = true;
                    break;
                }
            }
            if(!flag){
                result.add(new ApplicationAlarm(time));
            }
        }
        return result;
    }

    @Override
    public List<AlarmInfo> selectAlarmBrowserByParams(AlarmInfoParam info) {
        Map<String, Object> param = new HashMap<>();
        param.put("start", info.getStart());
        param.put("end", info.getEnd());
        param.put("alarmlevel", info.getAlarmlevel());
        param.put("productType", info.getProductType());
        param.put("constraintDesc", info.getConstraintDesc());
        //applicationId改为list数组
//        param.put("applicationId", info.getApplicationId());
        param.put("applicationId", info.getAuthAppIds());
        param.put("componentId", info.getComponentId());
        param.put("customId", info.getCustomId());
        param.put("interval", info.getBpmInterval());
        return alarmInfoMapper.selectAlarmInfoByParam(param);
    }

    @Override
    public Page<AlarmInfo> selectAlarmBrowserPageByParams(Page<AlarmInfo> page, AlarmInfoParam info) {
        Map<String, Object> param = new HashMap<>();
        param.put("start", info.getStart());
        param.put("end", info.getEnd());
        param.put("alarmlevel", info.getAlarmlevel());
        param.put("productType", info.getProductType());
        param.put("constraintDesc", info.getConstraintDesc());
        //applicationId改为list数组
//        param.put("applicationId", info.getApplicationId());
        param.put("applicationId", info.getAuthAppIds());
        param.put("componentId", info.getComponentId());
        param.put("customId", info.getCustomId());
        param.put("interval", info.getBpmInterval());
        page.setRecords(alarmInfoMapper.selectAlarmInfoByPage(page,param));
        return page;
    }



























    @Override
    public List<AlarmInfo> selectAlarmInfoAndDimensionById(int id) {
        return alarmInfoMapper.selectAlarmInfoAndDimensionById(id);
    }

    @Override
    public List<Map<String, Object>> selectAbnormalinfo(Map<String, Object> map) {
        return abnormalinfoMapper.selectAbnormalinfo(map);
    }


    @Override
    public List<AlarmInfo> selectAlarmInfoByIds(Map<String, Object> map) {
        return alarmInfoMapper.selectAlarmInfoByIds(map);
    }

    @Override
    public List<Map<String, Object>> selectAlarmNumByInterval(Map<String, Object> map) {
        return alarmInfoMapper.selectAlarmNumByInterval(map);
    }
}
