package com.greattimes.ev.bpm.service.common.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.config.param.resp.TranslateDataParam;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.Custom;
import com.greattimes.ev.bpm.entity.DimensionValue;
import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.entity.Server;
import com.greattimes.ev.bpm.entity.ServerIp;
import com.greattimes.ev.bpm.mapper.ComponentMapper;
import com.greattimes.ev.bpm.mapper.CustomMapper;
import com.greattimes.ev.bpm.mapper.DimensionValueMapper;
import com.greattimes.ev.bpm.mapper.FieldMappingMapper;
import com.greattimes.ev.bpm.mapper.IndicatorMapper;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * @author NJ
 * @date 2018/9/27 20:56
 */
@Service
public class CommonServiceImpl implements ICommonService{

    @Autowired
    private IndicatorMapper indicatorMapper;
    @Autowired
    private ComponentMapper componentMapper;
    @Autowired
    private CustomMapper customMapper;
    @Autowired
    private DimensionValueMapper dimensionValueMapper;
    @Autowired
    private FieldMappingMapper fieldMappingMapper;

    @Override
    public List<ProtocolField> findFieldByIds(List<Integer> ids) {
        return indicatorMapper.selectProtocolFiledByIds(ids);
    }

    @Override
    public List<Indicator> findIndicatorByIds(List<Integer> ids) {
        return indicatorMapper.selectIndicatorByIds(ids);
    }

    @Override
    public Map<Integer, Indicator> findIndicatorMapByIds(List<Integer> ids) {
        Map<Integer, Indicator> result = new HashMap<>();
        if(evUtil.listIsNullOrZero(ids)){
            return result;
        }
        List<Indicator> indicators = indicatorMapper.selectIndicatorByIds(ids);
        indicators.forEach(x->{
            result.put(x.getId(), x);
        });
        return result;
    }

    @Override
    public List<ServerIp> findServerIpsByComponentIds(List<Integer> componentIds) {
        return componentMapper.findServerIpsByComponentIds(componentIds);
    }

    @Override
    public List<Server> selectPortByIpId(int iPId) {
        return componentMapper.selectPortByIpId(iPId);
    }

    @Override
    public List<Custom> selectCustomByComponentId(int componentId) {
        EntityWrapper<Custom> ew = new EntityWrapper<>();
        ew.where("componentId={0}", componentId);
        return customMapper.selectList(ew);
    }

    @Override
    public List<DimensionValue> selectDimensionValueByStatisticsDimensionId(int statisDimensionId) {
        return dimensionValueMapper.selectListByStatisDimensionId(statisDimensionId);
    }

	@Override
	public List<TranslateDataParam> getComponentTranslate(Long uuid) {
		return fieldMappingMapper.getComponentTranslate(uuid);
	}

	@Override
	public List<TranslateDataParam> getCustomTranslate(Long uuid) {
		return dimensionValueMapper.getCustomTranslate(uuid);
	}

	@Override
	public String getKpiKey(int type, Map<String, Object> map) {
		String key=null;
		if(type==1) {
			//kpi.数据层级_组件id或事件id_uuid.指标id
			int level=evUtil.getMapIntValue(map, "type");
			int parentId=evUtil.getMapIntValue(map, "parentId");
			int uuid=evUtil.getMapIntValue(map, "uuid");
			int indicatorId=evUtil.getMapIntValue(map, "indicatorId");
			key="kpi."+level+"_"+parentId+"_"+uuid+"."+indicatorId;
		}else if(type==2) {
			//kpi.数据层级_组件id或事件id_维度id_维度值.指标id
			int level=evUtil.getMapIntValue(map, "type");
			int parentId=evUtil.getMapIntValue(map, "parentId");
			int uuid=evUtil.getMapIntValue(map, "uuid");
			String value=evUtil.getMapStrValue(map, "value");
			int indicatorId=evUtil.getMapIntValue(map, "indicatorId");
			key="kpi."+level+"_"+parentId+"_"+uuid+"_"+value+"."+indicatorId;
		}
		return key;
	}

    @Override
    public List<Map<String, Object>> selectCustomIndicatorNamesByMap(Map<String, Object> map) {
        return customMapper.selectCustomIndicatorNameByCustomIds(map);
    }

	@Override
	public Component selectComponentByCustomId(int customId) {
		Component component=componentMapper.selectComponentByCustomId(customId);
		return component;
	}
	
	@Override
    public List<String> selectIndicator(List<Integer> type) {
		List<Map<String, Object>> indicators=indicatorMapper.selectIndicatorDictionary(type);
		List<String> enameList = new ArrayList<>();
		indicators.stream().forEach(x -> enameList.add(x.get("columnName").toString()));
        return enameList;
    }
}
