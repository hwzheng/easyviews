package com.greattimes.ev.bpm.service.config.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.config.param.req.AlarmParam;
import com.greattimes.ev.bpm.config.param.req.AlarmTemplateReqParam;
import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.mapper.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.service.config.IAlarmRuleService;
import com.greattimes.ev.common.utils.evUtil;


/**
 * <p>
 * 告警规则表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@Service
public class AlarmRuleServiceImpl extends ServiceImpl<AlarmRuleMapper, AlarmRule> implements IAlarmRuleService {
	@Autowired
	private AlarmRuleMapper alarmRuleMapper;
	@Autowired
	private AlarmComponentMapper alarmComponentMapper;
	@Autowired
	private AlarmTimeMapper alarmTimeMapper;
	@Autowired
	private AlarmConditionTemplateParamMapper alarmConditionTemplateParamMapper;
	@Autowired
	private AlarmConditionMapper alarmConditionMapper;
	@Autowired
	private AlarmIpMapper alarmIpMapper;
	@Autowired
	private AlarmPortMapper alarmPortMapper;
	@Autowired
	private AlarmFilterMapper alarmFilterMapper;
	@Autowired
	private AlarmFilterValueMapper alarmFilterValueMapper;
	@Autowired
	private AlarmDimensionMapper alarmDimensionMapper;
	@Autowired
	private AlarmConditionMultiDimensionMapper alarmConditionMultiDimensionMapper;
	@Autowired
	private AlarmConditionFilterMapper alarmConditionFilterMapper;
	@Autowired
	private AlarmConditionFilterValueMapper alarmConditionFilterValueMapper;
	@Autowired
	private AlarmMonitorMapper alarmMonitorMapper;
	@Autowired
	private AlarmDatasourceMapper alarmDatasourceMapper;
	@Autowired
	private AlarmDatasourceDimensionMapper alarmDatasourceDimensionMapper;
	@Autowired
	private AlarmCustomMapper alarmCustomMapper;
	@Autowired
	private CustomMapper customMapper;
	@Autowired
	private AlarmConditionLineParamMapper alarmConditionLineParamMapper;

	/**
	 * 应用告警类型
	 */
	private static final int ALARMTYPE_APPLICATION = 1;
	/**
	 * 组件告警类型
	 */
	private static final int ALARMTYPE_COMPONENT = 2;
	/**
	 * ip告警类型
	 */
	private static final int ALARMTYPE_IP = 3;
	/**
	 * ipPort告警类型
	 */
	private static final int ALARMTYPE_IPPORT = 4;
	/**
	 * 单维度告警类型
	 */
	private static final int ALARMTYPE_SINGLEDIMENSION = 5;
	/**
	 * 多维度告警类型
	 */
	private static final int ALARMTYPE_MULTIEDIMENSION = 6;
	/**
	 * 监控点告警类型
	 */
	private static final int ALARMTYPE_MONITOR = 7;
	/**
	 * 数据源告警类型
	 */
	private static final int ALARMTYPE_DATASOURCE = 8;
	/**
	 * 自定义业务告警-业务
	 */
	private static final int ALARMTYPE_CUSTOM_BUSINESS = 9;
	/**
	 * 自定义业务告警-统计维度
	 */
	private static final int ALARMTYPE_CUSTOM_STATISTICS_DIMENSION = 10;
	/**
	 * 自定义业务告警-普通维度
	 */
	private static final int ALARMTYPE_CUSTOM_NORMAL_DIMENSION = 11;
	/**
	 * 自定义业务告警-多维维度
	 */
	private static final int ALARMTYPE_CUSTOM__MULTI_DIMENSION = 12;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<Map<String, Object>> selectAlarmRule(Map<String, Object> map) {
		int applicationId = evUtil.getMapIntValue(map, "applicationId");
		int type = evUtil.getMapIntValue(map, "type");
		//监控点告警
		if(type == ALARMTYPE_MONITOR){
			return alarmRuleMapper.selectMonitorAlarmRule(applicationId, type);
		}else if(type == ALARMTYPE_DATASOURCE){
			List<Map<String, Object>> dataSourceList  = alarmRuleMapper.selectDataSourceAlarmRule(applicationId,type);
			//查询指标名称放入集合中
			List<Map<String, Object>> dataSourceIndicatorList = alarmRuleMapper.selectDataSourceIndicatorByRuleId(applicationId,type);
			Map<String, Object> indicatorNameMap = new HashMap<>();
			dataSourceIndicatorList.stream().forEach(x->{
				indicatorNameMap.put(x.get("id").toString(), x.get("indicatorName"));
			});

			dataSourceList.stream().forEach(x->{
//				x.put("indicatorName", indicatorNameMap.getOrDefault(x.get("id").toString(),null));
				x.put("indicatorName", indicatorNameMap.get(x.get("id").toString()));

			});

			return dataSourceList;
		}else{
			return alarmRuleMapper.selectAlarmRule(applicationId, type);
		}
	}

	@Override
	public void deleteAlarmRule(int id) {
		//先查出来判断type 然后在删除
		AlarmRule alarmRule =  alarmRuleMapper.selectById(id);
		int type = alarmRule.getType();

		//公共删除的表
		//bpm_alarm_rule
		alarmRuleMapper.deleteById(id);
		Map<String, Object> map = new HashMap<>(1);
		map.put("alarmId", id);

		//bpm_alarm_time
		alarmTimeMapper.deleteByMap(map);

		//bpm_alarm_component
		if((type >= ALARMTYPE_COMPONENT && type <= ALARMTYPE_MULTIEDIMENSION)
				|| (type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION)){
			alarmComponentMapper.deleteByMap(map);
		}

		//bpm_alarm_custom
		if(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION){
			alarmCustomMapper.deleteByMap(map);
		}

        //bpm_alarm_datasource
        if(type == ALARMTYPE_DATASOURCE){
			List<AlarmDatasource> alarmDatasourceList = alarmDatasourceMapper.selectByMap(map);
			if(!evUtil.listIsNullOrZero(alarmDatasourceList)){
				alarmDatasourceMapper.deleteByMap(map);
				//bpm_alarm_datasource_dimension
				Map<String, Object> sourceIdMap = new HashMap<>(1);
				sourceIdMap.put("alarmDataSourceId", alarmDatasourceList.get(0).getId());
				alarmDatasourceDimensionMapper.deleteByMap(sourceIdMap);
			}
        }

		//bpm_alarm_ip
		if(type == ALARMTYPE_IP){
			alarmIpMapper.deleteByMap(map);
		}

		//bpm_alarm_port
		if(type == ALARMTYPE_IPPORT){
			alarmPortMapper.deleteByMap(map);
		}

		//bpm_alarm_monitor
		if(type == ALARMTYPE_MONITOR){
			alarmMonitorMapper.deleteByMap(map);
		}

		if(type == ALARMTYPE_SINGLEDIMENSION || type == ALARMTYPE_MULTIEDIMENSION
				|| type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION || type == ALARMTYPE_CUSTOM_NORMAL_DIMENSION
				|| type == ALARMTYPE_CUSTOM__MULTI_DIMENSION){

			//bpm_alarm_dimension
			alarmDimensionMapper.deleteByMap(map);

			//bpm_alarm_filter
			List<AlarmFilter> alarmFilterList = alarmFilterMapper.selectByMap(map);
			if(!evUtil.listIsNullOrZero(alarmFilterList)){
				alarmFilterMapper.deleteByMap(map);
				List<Integer> alarmDimensionIdList = alarmFilterList.stream().map(AlarmFilter::getId).collect(Collectors.toList());
				EntityWrapper<AlarmFilterValue> ew = new EntityWrapper<>();
				ew.in("alarmDimensionId", alarmDimensionIdList);
				alarmFilterValueMapper.delete(ew);
			}
		}

		//bpm_alarm_condition
		List<AlarmCondition> conditionList = alarmConditionMapper.selectByMap(map);
		if(!evUtil.listIsNullOrZero(conditionList)){
			alarmConditionMapper.deleteByMap(map);
			List<Integer> conditionIdList = conditionList.stream().map(AlarmCondition::getId).collect(Collectors.toList());

			//bpm_alarm_condition_template_param
			EntityWrapper<AlarmConditionTemplateParam> templateParamEw = new EntityWrapper<>();
			templateParamEw.in("conditionId", conditionIdList);
			alarmConditionTemplateParamMapper.delete(templateParamEw);

			if(type == ALARMTYPE_MULTIEDIMENSION){
				//bpm_alarm_condition_multi_dimension
				EntityWrapper<AlarmConditionMultiDimension> multiDimensionEw = new EntityWrapper<>();
				multiDimensionEw.in("conditionId", conditionIdList);
				alarmConditionMultiDimensionMapper.delete(multiDimensionEw);

				//bpm_alarm_condition_filter
				EntityWrapper<AlarmConditionFilter> conditionFilterEw = new EntityWrapper<>();
				conditionFilterEw.in("conditionId", conditionIdList);
				List<AlarmConditionFilter> alarmConditionFilters = alarmConditionFilterMapper.selectList(conditionFilterEw);
				if(!evUtil.listIsNullOrZero(alarmConditionFilters)){
					alarmConditionFilterMapper.delete(conditionFilterEw);
					List<Integer> alarmConditionFilterList = alarmConditionFilters.stream().map(AlarmConditionFilter::getId).collect(Collectors.toList());
					EntityWrapper<AlarmConditionFilterValue> conditionFilterValueEw = new EntityWrapper<>();
					conditionFilterValueEw.in("alarmDimensionId",alarmConditionFilterList);
					alarmConditionFilterValueMapper.delete(conditionFilterValueEw);
				}
			}
		}
	}

	@Override
	public AlarmParam getApplicationAlarmDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam alarmParam = alarmRuleMapper.selectApplicationAlarmDetail(alarmId);
		this.setCustomId(alarmParam, alarmId);
		this.handFormateRate(indicatorIds,alarmParam);
		return alarmParam;
	}

	@Override
	public AlarmParam getComponentAlarmDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam alarmParam = alarmRuleMapper.selectComponentAlarmDetail(alarmId);
		List<Integer> list = new ArrayList<>();
		if(alarmParam != null){
			alarmParam.getComponents().stream().forEach( x-> list.add(x.getComponentId()));
			alarmParam.setComponentIds(list);
		}
		this.handFormateRate(indicatorIds, alarmParam);
		return alarmParam;
	}

	@Override
	public AlarmParam getIpAlarmDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam alarmParam = alarmRuleMapper.selectIpAlarmDetail(alarmId);
		List<Integer> ips = new ArrayList<>();
		alarmParam.getAlarmIps().stream().forEach(alarmIp -> ips.add(evUtil.ipToInt(alarmIp.getIp())));
		alarmParam.setIps(ips);
		this.handFormateRate(indicatorIds,alarmParam);
		return alarmParam;
	}

	@Override
	public AlarmParam getIpPortAlarmDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam result = alarmRuleMapper.selectIpAndPortAlarmDetail(alarmId);
		List<String> ipPorts = new ArrayList<>();
		result.getAlarmIps().stream().forEach(x->ipPorts.add(x.getIp()));
		result.setIpPorts(ipPorts);
		this.handFormateRate(indicatorIds,result);
		return result;
	}

	@Override
	public AlarmParam getSingleDimensionDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam param =  alarmRuleMapper.selectSingleDimensionAlarmDetail(alarmId);
		Optional.ofNullable(param).ifPresent(x->{
			List<AlarmDimension> dimensions = x.getAlarmDimensions();
			this.setCustomDimensionId(x);
			if(!evUtil.listIsNullOrZero(dimensions)){
				x.setDimensionId(dimensions.get(0).getDimensionId());
			}
			this.setCustomId(x, alarmId);
		});
		this.handFormateRate(indicatorIds,param);
		return param;
	}

	@Override
	public AlarmParam getMultiDimensionDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam filterAlarm = alarmRuleMapper.selectMultiAlarmFilters(alarmId);
		if(filterAlarm == null){
			return filterAlarm;
		}
		//alarmDimensionIds
		List<Integer> alarmDimensionIds = new ArrayList<>();
		filterAlarm.getAlarmDimensions().forEach(alarmDimension -> {
			alarmDimensionIds.add(alarmDimension.getDimensionId());
		});
		filterAlarm.setDimensionIds(alarmDimensionIds);
		AlarmParam conditionFilterAlarm = alarmRuleMapper.selectMultiAlarmCondition(alarmId);
		conditionFilterAlarm.getCondition().forEach(conditon->{
			List<Integer> mutiDimensionList = new ArrayList<>();
			conditon.getMultiDimensions().forEach(dimen->{
				mutiDimensionList.add(dimen.getDimensionId());
			});
			conditon.setDimensionIds(mutiDimensionList);
		});
		filterAlarm.setCondition(conditionFilterAlarm.getCondition());

		this.setCustomDimensionId(filterAlarm);
		this.setCustomId(filterAlarm, alarmId);
		this.handFormateRate(indicatorIds,filterAlarm);
		return filterAlarm;
	}

	@Override
	public void addAlarm(AlarmParam alarmParam, List<String> indicatorIds) {
		//告警类型 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度，7 监控点
		int type = alarmParam.getType();
		//自定义配置需要查询应用和组件id
		if(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION){
			Custom custom = customMapper.selectById(alarmParam.getCustomId());
			alarmParam.setApplicationId(custom.getApplicationId());
			alarmParam.setComponentId(custom.getComponentId());
		}
		//bpm_alarm_rule
		AlarmRule rule = new AlarmRule();
		BeanUtils.copyProperties(alarmParam,rule);
		//开启状态
		rule.setActive(1);

		alarmRuleMapper.insert(rule);

		//bpm_alarm_time
		Integer alarmId = rule.getId();
		List<AlarmTime> alramTimeList = alarmParam.getTime();
		alramTimeList.stream().forEach(alarmTime->{
			alarmTime.setAlarmId(alarmId);
			alarmTimeMapper.insert(alarmTime);
		});
		//bpm_alarm_condition  &&  bpm_alarm_condition_template_param
		List<AlarmTemplateReqParam> conditions = alarmParam.getCondition();
		conditions.stream().forEach(condition->{
			//bpm_alarm_condition
			AlarmCondition alarmCondition = new AlarmCondition();
			alarmCondition.setAlarmId(alarmId);
			alarmCondition.setTemplateCode(condition.getTemplateId());
			alarmCondition.setRemarks(condition.getRemarks());
			alarmConditionMapper.insert(alarmCondition);
			int conditionId = alarmCondition.getId();
			//bpm_alarm_condition_template_param
			AlarmConditionTemplateParam templateParam = new AlarmConditionTemplateParam();
			BeanUtils.copyProperties(condition,templateParam);
			templateParam.setConditionId(conditionId);
			//特殊指标进行除以100操作
			if(indicatorIds.contains(templateParam.getIndicatorId().toString())){
				templateParam.setThreshold(templateParam.getThreshold()/100.0);
			}
			alarmConditionTemplateParamMapper.insert(templateParam);

			/**
			 1 应用，9 业务 【模板（1/2/3/4/5/6/7）】
			 2 组件，7 监控点，8 数据源，10 统计维度 【模板（1/2/3/4/5/6/7）】
			 3 IP 【模板（1/2/3/4/5/6/7）】
			 4 IP/PORT 【模板（1/2/3/4/5/6/7）】
			 5 组件/单维度，11 普通维度【模板（1/2/3/4/5/6/7/8/9）】
			 6 组件/多维度，12 多级维度 【模板（1/2/3/4/5/6/8/9/10/11）】

			 含有基线打分模板的有:业务9、组件2、数据源8、统计维度10、ip3、port4  组件单维度5  业务普通维度11
			 */

			if(type == 9 || type == 2 || type == 8 || type == 10
					||type == 3 ||  type == 4  || type == 5 || type == 11){
				//基线打分模板
				if(alarmCondition.getTemplateCode() == 7){
					AlarmConditionLineParam alarmConditionLineParam = new AlarmConditionLineParam();
					alarmConditionLineParam.setConditionId(templateParam.getId());
					alarmConditionLineParam.setLowerScore(condition.getLowerScore());
					alarmConditionLineParam.setUpperScore(condition.getUpperScore());
					alarmConditionLineParamMapper.insert(alarmConditionLineParam);
				}
			}

			//多维度查询
			if(type == ALARMTYPE_MULTIEDIMENSION || type == ALARMTYPE_CUSTOM__MULTI_DIMENSION){
				if(type == ALARMTYPE_MULTIEDIMENSION){
					condition.getDimensionIds().forEach(x->{
						//bpm_alarm_condition_multi_dimension
						AlarmConditionMultiDimension alarmConditionMultiDimension = new AlarmConditionMultiDimension();
						alarmConditionMultiDimension.setConditionId(conditionId);
						alarmConditionMultiDimension.setDimensionId(x);
						alarmConditionMultiDimension.setType(0);
						alarmConditionMultiDimensionMapper.insert(alarmConditionMultiDimension);
					});
				}else{
					condition.getCustomDimensionId().forEach(x->{
						//bpm_alarm_condition_multi_dimension
						AlarmConditionMultiDimension alarmConditionMultiDimension = new AlarmConditionMultiDimension();
						alarmConditionMultiDimension.setConditionId(conditionId);
						alarmConditionMultiDimension.setDimensionId(x.getInteger("id"));
						alarmConditionMultiDimension.setType(x.getIntValue("type"));
						alarmConditionMultiDimensionMapper.insert(alarmConditionMultiDimension);
					});
				}
				condition.getFilter().stream().forEach(filter->{
					//bpm_alarm_condition_filter
					AlarmConditionFilter alarmConditionFilter = new AlarmConditionFilter();
					alarmConditionFilter.setConditionId(conditionId);
					alarmConditionFilter.setDimensionId(filter.getDimensionId());
					alarmConditionFilter.setRule(filter.getRule());
					alarmConditionFilter.setType(filter.getType() == null ? 0 : filter.getType());
					alarmConditionFilterMapper.insert(alarmConditionFilter);
					//bpm_alarm_condition_filter_value
					filter.getValue().stream().forEach(x->{
						AlarmConditionFilterValue alarmConditionFilterValue = new AlarmConditionFilterValue();
						alarmConditionFilterValue.setAlarmDimensionId(alarmConditionFilter.getId());
						alarmConditionFilterValue.setValue(x);
						alarmConditionFilterValueMapper.insert(alarmConditionFilterValue);
					});
				});
			}
		});

		/**
		 * 自定义分析配置
		 */
		//bpm_alarm_custom
		if(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION){
			AlarmCustom alarmCustom = new AlarmCustom();
			alarmCustom.setAlarmId(alarmId);
			alarmCustom.setCustomId(alarmParam.getCustomId());
			alarmCustomMapper.insert(alarmCustom);
		}

		if(type == ALARMTYPE_DATASOURCE){
			//bpm_alarm_datasource
			AlarmDatasource alarmDatasource = new AlarmDatasource();
			alarmDatasource.setAlarmId(alarmId);
			alarmDatasource.setDataSourceId(alarmParam.getDataSourceId());
			alarmDatasource.setIndcatorGroupId(alarmParam.getIndcatorGroupId());
			alarmDatasourceMapper.insert(alarmDatasource);

			//bpm_alarm_datasource_dimension
			alarmParam.getRelevantContraintIds().forEach(x->{
				AlarmDatasourceDimension alarmDatasourceDimension = new AlarmDatasourceDimension();
				alarmDatasourceDimension.setAlarmDataSourceId(alarmDatasource.getId());
				alarmDatasourceDimension.setRelevantContraintId(x);
				alarmDatasourceDimensionMapper.insert(alarmDatasourceDimension);
			});
			//bpm_alarm_component
			int componentId = alarmParam.getComponentId();
			AlarmComponent alarmComponent = new AlarmComponent();
			alarmComponent.setAlarmId(alarmId);
			alarmComponent.setComponentId(componentId);
			alarmComponentMapper.insert(alarmComponent);
		}

		//bpm_alarm_monitor
		if(type == ALARMTYPE_MONITOR){
			alarmParam.getMonitorIds().stream().forEach(x->{
				AlarmMonitor alarmMonitor = new AlarmMonitor();
				alarmMonitor.setAlarmId(alarmId);
				alarmMonitor.setMonitorId(x);
				alarmMonitorMapper.insert(alarmMonitor);
			});
		}

		//bpm_alarm_component [组件多值]
		if(type == ALARMTYPE_COMPONENT){
			alarmParam.getComponentIds().stream().forEach(x->{
				AlarmComponent alarmComponent = new AlarmComponent();
				alarmComponent.setAlarmId(alarmId);
				alarmComponent.setComponentId(x.intValue());
				alarmComponentMapper.insert(alarmComponent);
			});
			logger.debug("==========组件告警添加成功！==========");
		}
		//bpm_alarm_component   单值保存 [3ip， 4port，5单维度 ，6 多维度]+自定义告警
		if( (type >= ALARMTYPE_IP && type <= ALARMTYPE_MULTIEDIMENSION) ||
				(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION)){
			int componentId = alarmParam.getComponentId();
			AlarmComponent alarmComponent = new AlarmComponent();
			alarmComponent.setAlarmId(alarmId);
			alarmComponent.setComponentId(componentId);
			alarmComponentMapper.insert(alarmComponent);
		}
		//bpm_alarm_ip
		if(type == ALARMTYPE_IP){
			int componentId = alarmParam.getComponentId();
			alarmParam.getIps().stream().forEach(x->{
				AlarmIp ip = new AlarmIp();
				ip.setAlarmId(alarmId);
				ip.setComponentId(componentId);
				//前台传入为inip 字符串类型
				ip.setIp(evUtil.ipToStr(x));
				alarmIpMapper.insert(ip);
			});
			logger.debug("==========ip告警添加成功！==========");
		}
		//bpm_alarm_port
		if(type == ALARMTYPE_IPPORT){
			int componentId = alarmParam.getComponentId();
			alarmParam.getIpPorts().stream().forEach(x->{
				AlarmPort alarmPort = new AlarmPort();
				alarmPort.setAlarmId(alarmId);
				alarmPort.setComponentId(componentId);
				alarmPort.setIp(x.split(":")[0]);
				alarmPort.setPort(Integer.parseInt(x.split(":")[1]));
				alarmPortMapper.insert(alarmPort);
			});
			logger.debug("==========ip/port告警添加成功！==========");
		}

		//bpm_alarm_filter && bpm_alarm_filter_value
		if(type == ALARMTYPE_SINGLEDIMENSION || type == ALARMTYPE_MULTIEDIMENSION
				|| type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION
				 || type == ALARMTYPE_CUSTOM_NORMAL_DIMENSION
				|| type == ALARMTYPE_CUSTOM__MULTI_DIMENSION){
			// bpm_alarm_dimension
			if(type == ALARMTYPE_SINGLEDIMENSION ){
				AlarmDimension alarmDimension = new AlarmDimension();
				alarmDimension.setDimensionId(alarmParam.getDimensionId());
				alarmDimension.setAlarmId(alarmId);
				alarmDimension.setType(0);
				alarmDimensionMapper.insert(alarmDimension);
			}
			if(type == ALARMTYPE_MULTIEDIMENSION){
				alarmParam.getDimensionIds().stream().forEach(dimen->{
					AlarmDimension alarmDimension = new AlarmDimension();
					alarmDimension.setDimensionId(dimen);
					alarmDimension.setAlarmId(alarmId);
					alarmDimension.setType(0);
					alarmDimensionMapper.insert(alarmDimension);
				});
			}
			if(type == ALARMTYPE_CUSTOM_NORMAL_DIMENSION || type == ALARMTYPE_CUSTOM__MULTI_DIMENSION || type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION){
				alarmParam.getCustomDimensionId().stream().forEach(x->{
					AlarmDimension alarmDimension = new AlarmDimension();
					alarmDimension.setDimensionId(x.getIntValue("id"));
					alarmDimension.setAlarmId(alarmId);
					alarmDimension.setType(x.getIntValue("type"));
					alarmDimensionMapper.insert(alarmDimension);
				});
			}
			alarmParam.getFilter().stream().forEach(x->{
				//bpm_alarm_filter
				AlarmFilter alarmFilter = new AlarmFilter();
				alarmFilter.setAlarmId(alarmId);
				alarmFilter.setDimensionId(x.getDimensionId());
				alarmFilter.setRule(x.getRule());
				alarmFilter.setType(x.getType() == null ? 0 : x.getType());
				alarmFilterMapper.insert(alarmFilter);
				// bpm_alarm_filter_value
				int  alarmFilterId = alarmFilter.getId();
				x.getValue().stream().forEach(y->{
					AlarmFilterValue alarmFilterValue = new AlarmFilterValue();
					alarmFilterValue.setAlarmDimensionId(alarmFilterId);
					alarmFilterValue.setValue(y);
					alarmFilterValueMapper.insert(alarmFilterValue);
				});
			});
			logger.debug("==========维度告警添加成功！==========");
		}
	}

	@Override
	public void updateAlarm(AlarmParam alarmParam, List<String> indicatorIds) {
		//告警类型 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度 ，7 监控点
		int type = alarmParam.getType();
		if(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION ){
			Custom custom = customMapper.selectById(alarmParam.getCustomId());
			alarmParam.setApplicationId(custom.getApplicationId());
			alarmParam.setComponentId(custom.getComponentId());
		}
		//bpm_alarm_rule
		AlarmRule rule = alarmRuleMapper.selectById(alarmParam.getId());
		int active = rule.getActive();
		BeanUtils.copyProperties(alarmParam,rule);
		//开启状态
		rule.setActive(active);
		alarmRuleMapper.updateAllColumnById(rule);
		Integer alarmId = rule.getId();
		//bpm_alarm_time[delete and add  ]
		Map<String, Object> param = new HashMap<String, Object>(1){{put("alarmId",alarmId);}};
		alarmTimeMapper.deleteByMap(param);
		List<AlarmTime> alramTimeList = alarmParam.getTime();
		alramTimeList.stream().forEach(alarmTime->{
			alarmTime.setAlarmId(alarmId);
			alarmTimeMapper.insert(alarmTime);
		});
		//del bpm_alarm_condition && bpm_alarm_template_param
		List<AlarmCondition> conditionList = alarmConditionMapper.selectByMap(param);
		alarmConditionMapper.deleteByMap(param);

		//batch delete conditions
		List<Integer> contempIds = conditionList.stream().map(AlarmCondition::getId).collect(Collectors.toList());
		if(!evUtil.listIsNullOrZero(contempIds)){
			EntityWrapper<AlarmConditionTemplateParam> entityWrapper = new EntityWrapper<>();
			entityWrapper.in("conditionId", contempIds);
			List<AlarmConditionTemplateParam> conditionTemplateParams =
					alarmConditionTemplateParamMapper.selectList(entityWrapper);

			//delete bpm_alarm_condition_template_param
			alarmConditionTemplateParamMapper.delete(entityWrapper);

			if(!evUtil.listIsNullOrZero(conditionTemplateParams)){
				List<Integer> conditionTemplateParamIds = conditionTemplateParams.stream().map(AlarmConditionTemplateParam::getId)
						.collect(Collectors.toList());

				//delete bpm_alarm_condition_line_param
				EntityWrapper<AlarmConditionLineParam> lineParamEntityWrapper = new EntityWrapper<>();
				lineParamEntityWrapper.in("conditionId", conditionTemplateParamIds);
				alarmConditionLineParamMapper.delete(lineParamEntityWrapper);
			}

		}

		List<AlarmTemplateReqParam> conditions = alarmParam.getCondition();
		conditions.stream().forEach(condition->{
			//bpm_alarm_condition
			AlarmCondition alarmCondition = new AlarmCondition();
			alarmCondition.setAlarmId(alarmId);
			alarmCondition.setTemplateCode(condition.getTemplateId());
			alarmCondition.setRemarks(condition.getRemarks());
			alarmConditionMapper.insert(alarmCondition);
			int conditionId = alarmCondition.getId();
			//bpm_alarm_template_param
			AlarmConditionTemplateParam templateParam = new AlarmConditionTemplateParam();
			BeanUtils.copyProperties(condition,templateParam);
			templateParam.setConditionId(conditionId);
			//特殊指标进行除以100操作
			if(indicatorIds.contains(templateParam.getIndicatorId().toString())){
				templateParam.setThreshold(templateParam.getThreshold()/100.0);
			}
			alarmConditionTemplateParamMapper.insert(templateParam);

			/**
			 1 应用，9 业务 【模板（1/2/3/4/5/6/7）】
			 2 组件，7 监控点，8 数据源，10 统计维度 【模板（1/2/3/4/5/6/7）】
			 3 IP 【模板（1/2/3/4/5/6/7）】
			 4 IP/PORT 【模板（1/2/3/4/5/6/7）】
			 5 组件/单维度，11 普通维度【模板（1/2/3/4/5/6/7/8/9）】
			 6 组件/多维度，12 多级维度 【模板（1/2/3/4/5/6/8/9/10/11）】

			 含有基线打分模板的有:业务9、组件2、数据源8、统计维度10、ip3、port4  组件单维度5  业务普通维度11
			 */
			if(type == 9 || type == 2 || type == 8 || type == 10
					||type == 3 ||  type == 4  || type == 5 || type == 11){
				//基线打分模板
				if(alarmCondition.getTemplateCode() == 7){
					AlarmConditionLineParam alarmConditionLineParam = new AlarmConditionLineParam();
					alarmConditionLineParam.setConditionId(templateParam.getId());
					alarmConditionLineParam.setLowerScore(condition.getLowerScore());
					alarmConditionLineParam.setUpperScore(condition.getUpperScore());
					alarmConditionLineParamMapper.insert(alarmConditionLineParam);
				}
			}

			//多维度查询
			if(type == ALARMTYPE_MULTIEDIMENSION || type == ALARMTYPE_CUSTOM__MULTI_DIMENSION){
				//del bpm_alarm_condition_multi_dimension && bpm_alarm_condition_filter && bpm_alarm_condition_filter_value
				conditionList.stream().forEach(conditon->{
					Map<String, Object> map = new HashMap<String, Object>(1){{put("conditionId",conditon.getId());}};
					alarmConditionMultiDimensionMapper.deleteByMap(map);
					List<AlarmConditionFilter> filterList = alarmConditionFilterMapper.selectByMap(map);
					alarmConditionFilterMapper.deleteByMap(map);
					filterList.stream().forEach(filter->{
						Map<String, Object> paramMap = new HashMap<String,Object>(1){{put("alarmDimensionId",filter.getId());}};
						alarmConditionFilterValueMapper.deleteByMap(paramMap);
					});
				});

				if(type == ALARMTYPE_MULTIEDIMENSION ){
					condition.getDimensionIds().forEach(x->{
						//bpm_alarm_condition_multi_dimension
						AlarmConditionMultiDimension alarmConditionMultiDimension = new AlarmConditionMultiDimension();
						alarmConditionMultiDimension.setConditionId(conditionId);
						alarmConditionMultiDimension.setDimensionId(x);
						alarmConditionMultiDimension.setType(0);
						alarmConditionMultiDimensionMapper.insert(alarmConditionMultiDimension);
					});
				}else{
					//使用不同的参数
					condition.getCustomDimensionId().forEach(x->{
						//bpm_alarm_condition_multi_dimension
						AlarmConditionMultiDimension alarmConditionMultiDimension = new AlarmConditionMultiDimension();
						alarmConditionMultiDimension.setConditionId(conditionId);
						alarmConditionMultiDimension.setDimensionId(x.getIntValue("id"));
						alarmConditionMultiDimension.setType(x.getIntValue("type"));
						alarmConditionMultiDimensionMapper.insert(alarmConditionMultiDimension);
					});
				}
				condition.getFilter().stream().forEach(filter->{
					//bpm_alarm_condition_filter
					AlarmConditionFilter alarmConditionFilter = new AlarmConditionFilter();
					alarmConditionFilter.setConditionId(conditionId);
					alarmConditionFilter.setDimensionId(filter.getDimensionId());
					alarmConditionFilter.setRule(filter.getRule());
					alarmConditionFilter.setType(filter.getType() == null ? 0 : filter.getType());
					alarmConditionFilterMapper.insert(alarmConditionFilter);
					//bpm_alarm_condition_filter_value
					filter.getValue().stream().forEach(x->{
						AlarmConditionFilterValue alarmConditionFilterValue = new AlarmConditionFilterValue();
						alarmConditionFilterValue.setAlarmDimensionId(alarmConditionFilter.getId());
						alarmConditionFilterValue.setValue(x);
						alarmConditionFilterValueMapper.insert(alarmConditionFilterValue);
					});
				});
			}
		});
		Map<String, Object> alarmMap = new HashMap<String, Object>(1){{put("alarmId",alarmId);}};

		if(type == ALARMTYPE_DATASOURCE){
			//bpm_alarm_datasource
			AlarmDatasource adsource = new AlarmDatasource();
			adsource.setAlarmId(alarmId);
			AlarmDatasource alarmDatasource =  alarmDatasourceMapper.selectOne(adsource);
			alarmDatasourceMapper.deleteByMap(alarmMap);
			adsource.setAlarmId(alarmId);
			adsource.setDataSourceId(alarmParam.getDataSourceId());
			adsource.setIndcatorGroupId(alarmParam.getIndcatorGroupId());
			alarmDatasourceMapper.insert(adsource);
			//bpm_alarm_datasource_dimension
			Map<String, Object> alarmSourceIdMap = new HashMap<>(1);
			alarmSourceIdMap.put("alarmDataSourceId", alarmDatasource.getId());
			alarmDatasourceDimensionMapper.deleteByMap(alarmSourceIdMap);
			alarmParam.getRelevantContraintIds().forEach(x->{
				AlarmDatasourceDimension alarmDatasourceDimension = new AlarmDatasourceDimension();
				alarmDatasourceDimension.setAlarmDataSourceId(adsource.getId());
				alarmDatasourceDimension.setRelevantContraintId(x);
				alarmDatasourceDimensionMapper.insert(alarmDatasourceDimension);
			});

			alarmComponentMapper.deleteByMap(alarmMap);
			int componentId = alarmParam.getComponentId();
			AlarmComponent alarmComponent = new AlarmComponent();
			alarmComponent.setAlarmId(alarmId);
			alarmComponent.setComponentId(componentId);
			alarmComponentMapper.insert(alarmComponent);
		}

		//bpm_alarm_monitor
		if(type == ALARMTYPE_MONITOR){
			alarmMonitorMapper.deleteByMap(alarmMap);
			alarmParam.getMonitorIds().stream().forEach(x->{
				AlarmMonitor alarmMonitor = new AlarmMonitor();
				alarmMonitor.setAlarmId(alarmId);
				alarmMonitor.setMonitorId(x);
				alarmMonitorMapper.insert(alarmMonitor);
			});
		}

		//bpm_alarm_component [组件多值]
		if(type == ALARMTYPE_COMPONENT){
			// delete bpm_alarm_component
			alarmComponentMapper.deleteByMap(alarmMap);
			alarmParam.getComponentIds().stream().forEach(x->{
				AlarmComponent alarmComponent = new AlarmComponent();
				alarmComponent.setAlarmId(alarmId);
				alarmComponent.setComponentId(x.intValue());
				alarmComponentMapper.insert(alarmComponent);
			});
		}
		//bpm_alarm_component   单值保存 [3ip， 4port，5二级维度 ，6 多维度]
		if((type >= ALARMTYPE_IP && type <= ALARMTYPE_MULTIEDIMENSION) ||
				(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION)){
			alarmComponentMapper.deleteByMap(alarmMap);
			int componentId = alarmParam.getComponentId();
			AlarmComponent alarmComponent = new AlarmComponent();
			alarmComponent.setAlarmId(alarmId);
			alarmComponent.setComponentId(componentId);
			alarmComponentMapper.insert(alarmComponent);
		}
		//bpm_alarm_ip
		if(type == ALARMTYPE_IP){
			alarmIpMapper.deleteByMap(alarmMap);
			int componentId = alarmParam.getComponentId();
			alarmParam.getIps().stream().forEach(x->{
				AlarmIp ip = new AlarmIp();
				ip.setAlarmId(alarmId);
				ip.setComponentId(componentId);
				//前台传入为inip 字符串类型
				ip.setIp(evUtil.ipToStr(x));
				alarmIpMapper.insert(ip);
			});
		}
		//bpm_alarm_port
		if(type == ALARMTYPE_IPPORT){
			alarmPortMapper.deleteByMap(alarmMap);
			int componentId = alarmParam.getComponentId();
			alarmParam.getIpPorts().stream().forEach(x->{
				AlarmPort alarmPort = new AlarmPort();
				alarmPort.setAlarmId(alarmId);
				alarmPort.setComponentId(componentId);
				alarmPort.setIp(x.split(":")[0]);
				alarmPort.setPort(Integer.parseInt(x.split(":")[1]));
				alarmPortMapper.insert(alarmPort);
			});
		}
		//bpm_alarm_dimension && pm_alarm_filter && bpm_alarm_filter_value
		if(type == ALARMTYPE_SINGLEDIMENSION || type == ALARMTYPE_MULTIEDIMENSION
				|| type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION ||  type == ALARMTYPE_CUSTOM_NORMAL_DIMENSION
				|| type == ALARMTYPE_CUSTOM__MULTI_DIMENSION){
			alarmDimensionMapper.deleteByMap(alarmMap);
			List<AlarmFilter> filterList = alarmFilterMapper.selectByMap(alarmMap);
			alarmFilterMapper.deleteByMap(alarmMap);
			filterList.stream().forEach(filter->{
				Map<String, Object> filterMap = new HashMap<String, Object>(1){{put("alarmDimensionId", filter.getId());}};
				alarmFilterValueMapper.deleteByMap(filterMap);
			});
			// bpm_alarm_dimension
			if(type == ALARMTYPE_SINGLEDIMENSION){
				AlarmDimension alarmDimension = new AlarmDimension();
				alarmDimension.setDimensionId(alarmParam.getDimensionId());
				alarmDimension.setAlarmId(alarmId);
				alarmDimension.setType(0);
				alarmDimensionMapper.insert(alarmDimension);
			}
			if(type == ALARMTYPE_MULTIEDIMENSION){
				alarmParam.getDimensionIds().stream().forEach(dimen->{
					AlarmDimension alarmDimension = new AlarmDimension();
					alarmDimension.setDimensionId(dimen);
					alarmDimension.setAlarmId(alarmId);
					alarmDimension.setType(0);
					alarmDimensionMapper.insert(alarmDimension);
				});
			}
			if(type == ALARMTYPE_CUSTOM_NORMAL_DIMENSION || type == ALARMTYPE_CUSTOM__MULTI_DIMENSION || type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION){
				alarmParam.getCustomDimensionId().stream().forEach(x->{
					AlarmDimension alarmDimension = new AlarmDimension();
					alarmDimension.setDimensionId(x.getIntValue("id"));
					alarmDimension.setAlarmId(alarmId);
					alarmDimension.setType(x.getIntValue("type"));
					alarmDimensionMapper.insert(alarmDimension);
				});
			}
			alarmParam.getFilter().stream().forEach(x->{
				//bpm_alarm_filter
				AlarmFilter alarmFilter = new AlarmFilter();
				alarmFilter.setAlarmId(alarmId);
				alarmFilter.setDimensionId(x.getDimensionId());
				alarmFilter.setRule(x.getRule());
				alarmFilter.setType(x.getType() == null ? 0 : x.getType());
				alarmFilterMapper.insert(alarmFilter);
				// bpm_alarm_filter_value
				int  alarmFilterId = alarmFilter.getId();
				x.getValue().stream().forEach(y->{
					AlarmFilterValue alarmFilterValue = new AlarmFilterValue();
					alarmFilterValue.setAlarmDimensionId(alarmFilterId);
					alarmFilterValue.setValue(y);
					alarmFilterValueMapper.insert(alarmFilterValue);
				});
			});
		}
	}

	@Override
	public int updateActive(int id, int active) {
		AlarmRule alarmRule = new AlarmRule();
		alarmRule.setId(id);
		alarmRule.setActive(active);
		return alarmRuleMapper.updateById(alarmRule);
	}

	@Override
	public int updateBatchActive(int appid, int active,int type) {
		alarmRuleMapper.updateBatchActive(appid,active, type);
		return 1;
	}

	@Override
	public boolean updateBatchActiveCustom(int customId, int active, int type) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("customId",customId);
		List<Integer> ids = alarmCustomMapper.selectByMap(map).stream().map(AlarmCustom::getAlarmId).collect(Collectors.toList());
		if(!evUtil.listIsNullOrZero(ids)){
			alarmRuleMapper.updateBatchActiveCustom(ids, active, type);
		}
		return true;
	}

	@Override
	public AlarmParam getMonitorDimensionDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam alarmParam = alarmRuleMapper.selectMonitorAlarmDetail(alarmId);
		List<Integer> list = new ArrayList<>();
		if(alarmParam != null){
			alarmParam.getMonitors().stream().forEach(x->list.add((x.getMonitorId())));
			alarmParam.setMonitorIds(list);
		}
		this.handFormateRate(indicatorIds, alarmParam);
		return alarmParam;
	}

	@Override
	public AlarmParam getDataSourceAlarmDetail(int alarmId, List<String> indicatorIds) {
		AlarmParam alarmParam =alarmRuleMapper.selectDataSourceAlarmDetail(alarmId);
		List<AlarmDatasourceDimension> list = alarmParam.getAlarmDatasourceDimensions();
		if(!evUtil.listIsNullOrZero(list)){
			List<Integer>  datasouceDimensionIds = new ArrayList<>();
			list.forEach(x -> datasouceDimensionIds.add(x.getRelevantContraintId()));
			alarmParam.setRelevantContraintIds(datasouceDimensionIds);
		}
		this.handFormateRate(indicatorIds, alarmParam);
		return alarmParam;
	}


	@Override
	public boolean updateBatchActiveByIds(List<Integer> ids, int active) {
		alarmRuleMapper.updateBatchActiveByIds(ids,active);
		return true;
	}

	@Override
	public List<Map<String, Object>> selectCustomAlarmRule(Map<String, Object> map) {
		return alarmRuleMapper.selectCustomAlarmRule(evUtil.getMapIntValue(map,"customId"),
				evUtil.getMapIntValue(map,"type"));
	}

	@Override
	public boolean deleteAlarmRuleByComponentIds(List<Integer> componentIds) {
		if(evUtil.listIsNullOrZero(componentIds)){
			return true;
		}
		/**
		 * 普通告警
		 * 2 组件， 3ip， 4port，5二级维度 ，6 多维度 ，7 监控点告警 , 8 数据源告警
		 * 9自定义-- 业务 ，10自定义-- 统计维度， 11自定义-- 普通维度， 12自定义--  多维度',
		 */
		EntityWrapper<AlarmComponent> alarmComponentEntityWrapper = new EntityWrapper<>();
		alarmComponentEntityWrapper.in("componentId", componentIds);
		List<AlarmComponent> alarmComponents = alarmComponentMapper.selectList(alarmComponentEntityWrapper);
		Set<Integer> alarmIdSet = alarmComponents.stream().map(AlarmComponent::getAlarmId).collect(Collectors.toSet());
		if(alarmIdSet.isEmpty()){
			return true;
		}
		List<AlarmRule> alarmRules = alarmRuleMapper.selectBatchIds(new ArrayList<>(alarmIdSet));
		// map:key(type) value(alarmrule)
		alarmRules.stream().collect(Collectors.groupingBy(AlarmRule::getType))
				.forEach((x,y)->{
					//组件告警删除判断告警是否拥有该唯一组件
					if(x.intValue() == 2){
						List<Integer> delAlarmIds = new ArrayList<>();
						EntityWrapper<AlarmComponent> entityWrapper = new EntityWrapper<>();
						entityWrapper.in("alarmId", y.stream().map(AlarmRule::getId).collect(Collectors.toList()));
						List<AlarmComponent> alarmComponentList = alarmComponentMapper.selectList(entityWrapper);
						List<AlarmComponent> deleteAlarmComponents = new ArrayList<>();
						alarmComponentList.stream().collect(Collectors.groupingBy(AlarmComponent::getAlarmId, Collectors.mapping(AlarmComponent::getComponentId, Collectors.toList())))
								.forEach((z,p)->{
									//如果给出的组件ids集合包含查询出来的组件集合∈则删除告警信息
									List<Integer> handComponentIds = new ArrayList<>();
									if(componentIds.containsAll(p)){
										delAlarmIds.add(z);
									}else {
										//bpm_alarm_component
										handComponentIds.addAll(p);
										componentIds.retainAll(p);
										if(componentIds.isEmpty()){
											//无交集 do nothing
										}else{
											componentIds.stream().forEach(h->{
												AlarmComponent alarmComponent = new AlarmComponent();
												alarmComponent.setComponentId(h);
												alarmComponent.setAlarmId(z);
												deleteAlarmComponents.add(alarmComponent);
											});
										}
									}
								});
						if(!evUtil.listIsNullOrZero(deleteAlarmComponents)){
							deleteAlarmComponents.stream().forEach(m->{
								EntityWrapper<AlarmComponent> alarmComponentEntityWrappers = new EntityWrapper<>();
								alarmComponentEntityWrappers.where("alarmId={0}",m.getAlarmId());
								alarmComponentEntityWrappers.and("componentId={0}", m.getComponentId());
								alarmComponentMapper.delete(alarmComponentEntityWrappers);
							});
						}
						this.deleteAlarmRuleByIds(delAlarmIds);
					}else{
						this.deleteAlarmRuleByIds(y.stream().map(AlarmRule::getId).collect(Collectors.toList()));
						logger.debug("#####告警删除type{}",x);
					}
				});
		return true;
	}


	@Override
	public boolean deleteAlarmRuleByIds(List<Integer> ids) {
		if(evUtil.listIsNullOrZero(ids)){
			return true;
		}
		List<AlarmRule> alarmRules = alarmRuleMapper.selectBatchIds(ids);
		if(evUtil.listIsNullOrZero(alarmRules)){
			return true;
		}
		List<Integer> alarmIds = alarmRules.stream().map(AlarmRule::getId).collect(Collectors.toList());
		//公共删除的表
		//bpm_alarm_rule
		if(!evUtil.listIsNullOrZero(alarmIds)) {
			alarmRuleMapper.deleteBatchIds(alarmIds);
		}
		EntityWrapper<AlarmTime> alarmTimeEntityWrapper = new EntityWrapper<>();
		alarmTimeEntityWrapper.in("alarmId", alarmIds);
		//bpm_alarm_time
		alarmTimeMapper.delete(alarmTimeEntityWrapper);

		//根据type分类告警
		alarmRules.stream().collect(Collectors.groupingBy(AlarmRule::getType, Collectors.mapping(AlarmRule::getId,Collectors.toList()))).forEach((x, y)->{
			int type = x.intValue();
			//bpm_alarm_component
			if((type >= ALARMTYPE_COMPONENT && type <= ALARMTYPE_MULTIEDIMENSION)
					|| (type >= ALARMTYPE_DATASOURCE && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION)){
				EntityWrapper<AlarmComponent> alarmComponentEntityWrapper = new EntityWrapper<>();
				alarmComponentEntityWrapper.in("alarmId", y);
				alarmComponentMapper.delete(alarmComponentEntityWrapper);
			}

			//bpm_alarm_custom
			if(type >= ALARMTYPE_CUSTOM_BUSINESS && type <= ALARMTYPE_CUSTOM__MULTI_DIMENSION){
				EntityWrapper<AlarmCustom> alarmCustomEntityWrapper = new EntityWrapper<>();
				alarmCustomEntityWrapper.in("alarmId", y);
				alarmCustomMapper.delete(alarmCustomEntityWrapper);
			}

			//bpm_alarm_datasource
			if(type == ALARMTYPE_DATASOURCE){
				EntityWrapper<AlarmDatasource> alarmDatasourceEntityWrapper = new EntityWrapper<>();
				alarmDatasourceEntityWrapper.in("alarmId", y);
				List<AlarmDatasource> alarmDatasourceList = alarmDatasourceMapper.selectList(alarmDatasourceEntityWrapper);
				if(!evUtil.listIsNullOrZero(alarmDatasourceList)){
					alarmDatasourceMapper.delete(alarmDatasourceEntityWrapper);
					//bpm_alarm_datasource_dimension
					EntityWrapper<AlarmDatasourceDimension> alarmDatasourceDimensionEntityWrapper = new EntityWrapper<>();
					alarmDatasourceDimensionEntityWrapper.in("alarmDataSourceId", alarmDatasourceList.stream().map(AlarmDatasource::getDataSourceId).collect(Collectors.toSet()));
					alarmDatasourceDimensionMapper.delete(alarmDatasourceDimensionEntityWrapper);
				}
			}

			//bpm_alarm_ip
			if(type == ALARMTYPE_IP){
				EntityWrapper<AlarmIp> alarmIpEntityWrapper = new EntityWrapper<>();
				alarmIpEntityWrapper.in("alarmId",y);
				alarmIpMapper.delete(alarmIpEntityWrapper);
			}

			//bpm_alarm_port
			if(type == ALARMTYPE_IPPORT){
				EntityWrapper<AlarmPort> alarmPortEntityWrapper = new EntityWrapper<>();
				alarmPortEntityWrapper.in("alarmId",y);
				alarmPortMapper.delete(alarmPortEntityWrapper);
			}

			//bpm_alarm_monitor
			if(type == ALARMTYPE_MONITOR){
				EntityWrapper<AlarmMonitor> alarmMonitorEntityWrapper = new EntityWrapper<>();
				alarmMonitorEntityWrapper.in("alarmId", y);
				alarmMonitorMapper.delete(alarmMonitorEntityWrapper);
			}

			if(type == ALARMTYPE_SINGLEDIMENSION || type == ALARMTYPE_MULTIEDIMENSION
					|| type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION || type == ALARMTYPE_CUSTOM_NORMAL_DIMENSION
					|| type == ALARMTYPE_CUSTOM__MULTI_DIMENSION){

				//bpm_alarm_dimension
				EntityWrapper<AlarmDimension> alarmDimensionEntityWrapper = new EntityWrapper<>();
				alarmDimensionEntityWrapper.in("alarmId",y);
				alarmDimensionMapper.delete(alarmDimensionEntityWrapper);

				//bpm_alarm_filter
				EntityWrapper<AlarmFilter> alarmFilterEntityWrapper = new EntityWrapper<>();
				alarmFilterEntityWrapper.in("alarmId",y);
				List<AlarmFilter> alarmFilterList = alarmFilterMapper.selectList(alarmFilterEntityWrapper);
				if(!evUtil.listIsNullOrZero(alarmFilterList)){
					alarmFilterMapper.delete(alarmFilterEntityWrapper);
					List<Integer> alarmDimensionIdList = alarmFilterList.stream().map(AlarmFilter::getId).collect(Collectors.toList());
					EntityWrapper<AlarmFilterValue> ew = new EntityWrapper<>();
					ew.in("alarmDimensionId", alarmDimensionIdList);
					alarmFilterValueMapper.delete(ew);
				}
			}

			//bpm_alarm_condition
			EntityWrapper<AlarmCondition> alarmConditionEntityWrapper = new EntityWrapper<>();
			alarmConditionEntityWrapper.in("alarmId",y);
			List<AlarmCondition> conditionList = alarmConditionMapper.selectList(alarmConditionEntityWrapper);
			if(!evUtil.listIsNullOrZero(conditionList)){
				alarmConditionMapper.delete(alarmConditionEntityWrapper);
				List<Integer> conditionIdList = conditionList.stream().map(AlarmCondition::getId).collect(Collectors.toList());
				//bpm_alarm_condition_template_param
				EntityWrapper<AlarmConditionTemplateParam> templateParamEw = new EntityWrapper<>();
				templateParamEw.in("conditionId", conditionIdList);
				alarmConditionTemplateParamMapper.delete(templateParamEw);

				if(type == ALARMTYPE_MULTIEDIMENSION){
					//bpm_alarm_condition_multi_dimension
					EntityWrapper<AlarmConditionMultiDimension> multiDimensionEw = new EntityWrapper<>();
					multiDimensionEw.in("conditionId", conditionIdList);
					alarmConditionMultiDimensionMapper.delete(multiDimensionEw);

					//bpm_alarm_condition_filter
					EntityWrapper<AlarmConditionFilter> conditionFilterEw = new EntityWrapper<>();
					conditionFilterEw.in("conditionId", conditionIdList);
					List<AlarmConditionFilter> alarmConditionFilters = alarmConditionFilterMapper.selectList(conditionFilterEw);
					if(!evUtil.listIsNullOrZero(alarmConditionFilters)){
						alarmConditionFilterMapper.delete(conditionFilterEw);
						//bpm_alarm_condition_filter_value
						List<Integer> alarmConditionFilterList = alarmConditionFilters.stream().map(AlarmConditionFilter::getId).collect(Collectors.toList());
						EntityWrapper<AlarmConditionFilterValue> conditionFilterValueEw = new EntityWrapper<>();
						conditionFilterValueEw.in("alarmDimensionId",alarmConditionFilterList);
						alarmConditionFilterValueMapper.delete(conditionFilterValueEw);
					}
				}
			}
		});
		return true;
	}

	@Override
	public boolean deleteAlarmRuleByIpPorts(Set<String> ipStr, Set<String> ipPortStr, int componentId) {
		List<Integer> deleteAlarmIds = new ArrayList<>();
		Map<String, Object> param = new HashMap<>(1);
		param.put("componentId", componentId);
		/**==================bpm_alarm_ip==========================*/
		List<AlarmIp> alarmIps = alarmIpMapper.selectByMap(param);
		if(!evUtil.listIsNullOrZero(alarmIps)){
			Map<String, List<AlarmIp>> groupAlarmIp = new HashMap<>();
			String ipkey;
			for(AlarmIp alarmIp : alarmIps){
				ipkey = alarmIp.getIp();
				if(groupAlarmIp.get(ipkey) == null ){
					List<AlarmIp> alarmIpList = new ArrayList<>();
					alarmIpList.add(alarmIp);
					groupAlarmIp.put(ipkey, alarmIpList);
				}else{
					groupAlarmIp.get(ipkey).add(alarmIp);
				}
			}
			List<AlarmIp> deleteAlarmIps = new ArrayList<>();
			Set<String> ipResult = new HashSet<>();
			alarmIps.stream().collect(Collectors.groupingBy(AlarmIp::getAlarmId, Collectors
					.mapping(AlarmIp::getIp, Collectors.toSet()))).forEach((x,y)->{
						if(y.isEmpty()){
							//do nothing
						}else if(ipStr.isEmpty()){
							// delete alarmRule
							deleteAlarmIds.add(x);
						}else{
							ipResult.addAll(y);
							ipStr.retainAll(y);
							if(ipStr.isEmpty()){
								//delete alarmRule
								deleteAlarmIds.add(x);
							}else{
								ipResult.removeAll(ipStr);
								ipResult.stream().forEach(z->{
									deleteAlarmIps.addAll(groupAlarmIp.get(z));
								});
							}
						}
			});
			//why bpm_alarm_ip and bpm_alarm_port not have identity ???
			Map<String, Object> paramIp = new HashMap<>(3);
			deleteAlarmIps.stream().forEach(alarmIp->{
				paramIp.clear();
				paramIp.put("alarmId", alarmIp.getAlarmId());
				paramIp.put("componentId", alarmIp.getComponentId());
				paramIp.put("ip", alarmIp.getIp());
				alarmIpMapper.deleteByMap(paramIp);
			});
		}

		/**==================bpm_alarm_port==========================*/
		List<AlarmPort> alarmPorts = alarmPortMapper.selectByMap(param);
		if(!evUtil.listIsNullOrZero(alarmPorts)){
			//key:(ip:port)
			Map<String, List<AlarmPort>>  alarmPortsMap = new HashMap<>();
			Set<String> ipPortResult = new HashSet<>();
			List<AlarmPort> deleteAlarmPorts = new ArrayList<>();
			Map<Integer, Set<String>> groupPorts = new HashMap<>();

			alarmPorts.stream().forEach(x->{
				if(alarmPortsMap.get(x.getIp()+":"+x.getPort()) == null){
					List<AlarmPort> alarmPortList = new ArrayList<>();
					alarmPortList.add(x);
					alarmPortsMap.put(x.getIp()+":"+x.getPort(),alarmPortList);
				}else{
					alarmPortsMap.get(x.getIp()+":"+x.getPort()).add(x);
				}

				if(groupPorts.get(x.getAlarmId()) == null){
					Set<String> ipPortSet = new HashSet<>();
					ipPortSet.add(x.getIp()+":"+x.getPort());
					groupPorts.put(x.getAlarmId(),ipPortSet);
				}else{
					groupPorts.get(x.getAlarmId()).add(x.getIp()+":"+x.getPort());
				}
			});
			groupPorts.forEach((x,y)->{
				if(y.isEmpty()){
					//do nothing
				}else if(ipPortStr.isEmpty()){
					// delete alarmRule
					deleteAlarmIds.add(x);
				}else{
					ipPortResult.addAll(y);
					ipPortStr.retainAll(y);
					if(ipPortStr.isEmpty()){
						//delete alarmRule
						deleteAlarmIds.add(x);
					}else{
						ipPortResult.removeAll(ipPortStr);
						ipPortResult.stream().forEach(z->{
							deleteAlarmPorts.addAll(alarmPortsMap.get(z));
						});
					}
				}
			});

			Map<String, Object> paramIpPort = new HashMap<>(3);
			deleteAlarmPorts.stream().forEach(alarmIpPort->{
				paramIpPort.clear();
				paramIpPort.put("alarmId", alarmIpPort.getAlarmId());
				paramIpPort.put("componentId", alarmIpPort.getComponentId());
				paramIpPort.put("ip", alarmIpPort.getIp());
				paramIpPort.put("port", alarmIpPort.getPort());
				alarmPortMapper.deleteByMap(paramIpPort);
			});
		}
		//bpm_alarm_rule  deleteAlarmIds
		this.deleteAlarmRuleByIds(deleteAlarmIds);
		return true;
	}


	@Override
	public List<AlarmMonitor> selectAlarmMonitorByMonitorId(int monitorId) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("monitorId", monitorId);
		return alarmMonitorMapper.selectByMap(map);
	}

	@Override
	public boolean deleteAlarmRuleByDimensionIds(List<Integer> dimensionIds,int type) {
		if(evUtil.listIsNullOrZero(dimensionIds)){
			return true;
		}

		/**
		 * 以下表只要含有维度就将其告警删除
		 * bpm_alarm_filter  bpm_alarm_dimension
		 * bpm_alarm_condition_multi_dimension  bpm_alarm_condition_filter
		 */
		Set<Integer> alarmIds = new HashSet<>();

		//bpm_alarm_filter
		EntityWrapper<AlarmFilter> alarmFilterEntityWrapper = new EntityWrapper<>();
		alarmFilterEntityWrapper.where("type={0}", type);
		alarmFilterEntityWrapper.in("dimensionId", dimensionIds);
		List<AlarmFilter> alarmFilters = alarmFilterMapper.selectList(alarmFilterEntityWrapper);
		alarmIds = alarmFilters.stream().map(AlarmFilter::getAlarmId).collect(Collectors.toSet());

		//bpm_alarm_dimension
		EntityWrapper<AlarmDimension> alarmDimensionEntityWrapper = new EntityWrapper<>();
		alarmDimensionEntityWrapper.where("type={0}", type);
		alarmDimensionEntityWrapper.in("dimensionId",dimensionIds);
		List<AlarmDimension> alarmDimensions = alarmDimensionMapper.selectList(alarmDimensionEntityWrapper);
		alarmIds.addAll(alarmDimensions.stream().map(AlarmDimension::getAlarmId).collect(Collectors.toSet()));

		//bpm_alarm_condition_multi_dimension
		Set<Integer> alarmIdSets = new HashSet<>();
		List<Map<String, Object>> multiConditions = alarmRuleMapper.selectAlarmConditionByMultiDimensionId(dimensionIds);
		multiConditions.stream().forEach(x->{
			alarmIdSets.add((Integer)x.get("alarmId"));
		});

		//bpm_alarm_condition_filter
		List<Map<String, Object>> filterConditions = alarmRuleMapper.selectAlarmConditionByMultiDimensionId(dimensionIds);
		filterConditions.stream().forEach(x->{
			alarmIdSets.add((Integer)x.get("alarmId"));
		});
		alarmIds.addAll(alarmIdSets);
		//deleteAlarmRuleByIds
		this.deleteAlarmRuleByIds(new ArrayList<>(alarmIds));
		return true;
	}

	/**
	 * 告警参数置入customId
	 * @author NJ
	 * @date 2018/9/12 11:34
	 * @param param
	 * @param alarmId
	 * @return com.greattimes.ev.bpm.config.param.req.AlarmParam
	 */
	private AlarmParam setCustomId(AlarmParam param, int alarmId){
		if(param == null || param.getType() == null){
			return param;
		}
		//自定义告警需要customId
		if(param.getType() >= 9 && param.getType() <= 12){
			Map<String, Object> paramMap = new HashMap<>();
			paramMap.put("alarmId", alarmId);
			List<AlarmCustom> alarmCustoms = alarmCustomMapper.selectByMap(paramMap);
			if(!evUtil.listIsNullOrZero(alarmCustoms)){
				param.setCustomId(alarmCustoms.get(0).getCustomId());
			}
		}
		return param;
	}

	/**
	 * 给自定义统计维度和多级维度赋值 CustomDimensionId
	 * @author NJ
	 * @date 2018/9/12 18:09
	 * @param alarmParam
	 * @return void
	 */
	private void setCustomDimensionId(AlarmParam alarmParam){
		//自定义统计维度和多级维度
		int type = alarmParam.getType().intValue();
		if(type == ALARMTYPE_CUSTOM_STATISTICS_DIMENSION ) {
			if (!evUtil.listIsNullOrZero(alarmParam.getFilter())) {
				List<JSONObject> jps = new ArrayList<>();
				alarmParam.getFilter().forEach(z->{
					JSONObject jp = new JSONObject();
					jp.put("id", z.getDimensionId());
					jp.put("type",z.getType());
					jps.add(jp);
				});
				alarmParam.setCustomDimensionId(jps);
			}
		}
		if(type == ALARMTYPE_CUSTOM__MULTI_DIMENSION ){
			List<AlarmDimension> alarmDimensions = alarmParam.getAlarmDimensions();
			if(!evUtil.listIsNullOrZero(alarmDimensions)){
				List<JSONObject> jps = new ArrayList<>();
				alarmDimensions.forEach(x->{
					JSONObject jp = new JSONObject();
					jp.put("id", x.getDimensionId());
					jp.put("type", x.getType());
					jps.add(jp);
					alarmParam.setCustomDimensionId(jps);
				});
			}
			alarmParam.getCondition().forEach(conditon->{
				List<JSONObject> jps = new ArrayList<>();
				List<AlarmConditionMultiDimension> dimensions = conditon.getMultiDimensions();
				dimensions.forEach(dimen->{
					JSONObject jp = new JSONObject();
					jp.put("id", dimen.getDimensionId());
					jp.put("type", dimen.getType());
					jps.add(jp);
				});
				conditon.setCustomDimensionId(jps);
			});
		}
	}

	@Override
	public List<AlarmConditionTemplateParam> selectTemplateParamByRuleId(int ruleId) {
		return alarmRuleMapper.selectTemplateParamByRuleId(ruleId);
	}

	/**
	 * 格式化特殊的指标 有关百分比的指标*100  入库/100
	 * @param indicatorIds
	 */
	private void handFormateRate(List<String> indicatorIds, AlarmParam param){
		if(evUtil.listIsNullOrZero(indicatorIds) || param == null){
			return ;
		}
		List<AlarmTemplateReqParam> templateReqParams = param.getCondition();
		if(!evUtil.listIsNullOrZero(templateReqParams)){
			templateReqParams.forEach(x->{
				if(indicatorIds.contains(x.getIndicatorId().toString())){
					x.setThreshold(x.getThreshold()*100.0);
				}
			});
		}
	}

	@Override
	public List<AlarmRule> selectAlarmRuleByComponentIds(List<Integer> componentIds) {
		return alarmRuleMapper.selectAlarmRuleByComponentIds(componentIds);
	}
}
