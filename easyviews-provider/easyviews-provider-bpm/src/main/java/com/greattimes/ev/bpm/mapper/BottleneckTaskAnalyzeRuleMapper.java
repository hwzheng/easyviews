package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeRule;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表分析规则表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface BottleneckTaskAnalyzeRuleMapper extends BaseMapper<BottleneckTaskAnalyzeRule> {

}
