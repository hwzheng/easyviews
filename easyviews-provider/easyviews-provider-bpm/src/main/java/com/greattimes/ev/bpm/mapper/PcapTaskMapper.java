package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.PcapTask;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.baomidou.mybatisplus.plugins.Page;

/**
 * <p>
 * pca下载任务 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-07
 */
public interface PcapTaskMapper extends BaseMapper<PcapTask> {

	List<Map<String, Object>> selectPcapTaskByMap(Page<Map<String, Object>> page, Map<String, Object> map);

}
