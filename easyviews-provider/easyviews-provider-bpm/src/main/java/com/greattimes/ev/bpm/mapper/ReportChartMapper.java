package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.analysis.param.req.ReportChartParam;
import com.greattimes.ev.bpm.entity.ReportChart;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 智能报表图表表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartMapper extends BaseMapper<ReportChart> {
    /**
     * 根据id查询应用图表
     * @param map
     * @return
     */
    ReportChartParam selectReportById(Map<String, Object> map);

    /**
     * 根据id查询事件图表
     * @param map
     * @return
     */
    ReportChartParam selectEventReportById(Map<String, Object> map);

	List<Map<String, Object>> queryChartList(@Param("reportId")Integer reportId,@Param("userId") Integer userId);

    /**
     * 为报表数据提供配置信息
     * @param map
     * @return
     */
    ReportChartParam selectReportForIndicatorData(Map<String, Object> map);

	List<ReportChartParam> selectReportChartConfig(@Param("ids")List<Integer> ids);

	List<ReportChartParam> selectEventReportChartConfig(@Param("ids")List<Integer> ids);

	/**
	 * 查询组件和事件维度的数据配置
	 * @author NJ
	 * @date 2019/4/17 14:20
     * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectReportDimensionData(Map<String, Object> map);

}
