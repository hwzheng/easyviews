package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.NoticeTable;

/**
 * <p>
 * 功能号映射表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-20
 */
public interface NoticeTableMapper extends BaseMapper<NoticeTable> {

}
