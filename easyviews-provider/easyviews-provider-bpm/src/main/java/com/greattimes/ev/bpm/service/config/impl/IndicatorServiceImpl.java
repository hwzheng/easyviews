package com.greattimes.ev.bpm.service.config.impl;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.mapper.IndicatorMapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-09-27
 */
@Service
public class IndicatorServiceImpl extends ServiceImpl<IndicatorMapper, Indicator> implements IService<Indicator> {

}
