package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.config.param.resp.TranslateDataParam;
import com.greattimes.ev.bpm.entity.DimensionValue;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface DimensionValueMapper extends BaseMapper<DimensionValue> {

	/**
	 * 批量添加
	 * @param set
	 */
	void insertDimensionValue(@Param("set") Set<DimensionValue> set);


	List<DimensionValue> selectListByStatisDimensionId(@Param("id") int id);


	List<TranslateDataParam> getCustomTranslate(Long uuid);


	/**获取维度值个数
	 * @param customId
	 * @param parseInt
	 * @return
	 */
	int getMappingCount(@Param("customId")int customId,@Param("dimensionId") int dimensionId);
	
	/**
	 * 根据多个维度id查询相应维度值的个数
	 * @author NJ
	 * @date 2018/12/13 15:05
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> getMappingCountByDimensionIds(Map<String, Object> map);
}
