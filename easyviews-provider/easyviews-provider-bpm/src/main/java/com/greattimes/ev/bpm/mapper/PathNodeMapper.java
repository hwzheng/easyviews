package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.PathNode;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 路径图节点表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-11
 */
public interface PathNodeMapper extends BaseMapper<PathNode> {
    void batchInsert(List<PathNode> list);
    /**
     * 根据节点查找节点的另一端节点(按照类型)
     * @author NJ
     * @date 2018/12/14 15:59
     * @param ids
     * @param nodeType
     * @return java.util.List<com.greattimes.ev.bpm.entity.PathNode>
     */
    List<PathNode> findNodeByIds(@Param(value = "ids") List<Integer> ids, @Param(value = "nodeType") int nodeType);
}
