package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmConditionFilterValue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警条件维度值表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
public interface AlarmConditionFilterValueMapper extends BaseMapper<AlarmConditionFilterValue> {

}
