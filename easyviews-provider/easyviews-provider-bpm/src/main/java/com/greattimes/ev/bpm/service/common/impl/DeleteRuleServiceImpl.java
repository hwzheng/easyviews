package com.greattimes.ev.bpm.service.common.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.service.analysis.IPathViewConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.mapper.AlarmDataMapper;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.config.IAlarmRuleService;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.bpm.service.config.IRelevantService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.bpm.service.datasources.IDatasourceRelevantService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * @author NJ
 * @date 2018/9/4 14:23
 */
@Service
public class DeleteRuleServiceImpl implements IDeleteRuleService{
	@Autowired
	private ICustomService customService;
	@Autowired
	private IComponentDimensionService componentDimensionService;
    @Autowired
    private IComponentService componentService;
    @Autowired
    private IAlarmRuleService alarmRuleService;
    @Autowired
    private AlarmDataMapper alarmDataMapper;
    @Autowired
    private IProtocolService protocolService;
    @Autowired
    private IDatasourceRelevantService datasourceRelevantService;
    @Autowired
    private IApplicationService applicationService;
    @Autowired
    private IRelevantService relevantService;
    @Autowired
    private IPathViewConfigService pathViewConfigService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean deleteAlarmByIpPorts(Set<String> ipStr, Set<String> ipPortStr, int componentId) {
        alarmRuleService.deleteAlarmRuleByIpPorts(ipStr, ipPortStr, componentId);
        return true;
    }

    @Override
    public boolean deleteComponent(List<Integer> ids) {
        if(evUtil.listIsNullOrZero(ids)){
            return true;
        }
        /**
         * 级联删除涉及5个模块
         * 1、应用管理
         * 2、路径图删除
         * 3、告警配置(普通告警和自定义告警)
         * 4、应用协议
         * 5、外部数据源
         * 6、自定义分析配置
         * 7、数据关联约束
         */
        /**==============1、应用管理==========================*/
        List<Component> components = componentService.delete(ids);

        /**==============2、路径图删除==========================*/
        pathViewConfigService.deletePathViewByComponentIds(ids);
        /**
        /**
         * ==============3、告警配置==========================
         *
         * 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度 ，7 监控点告警 ,8 数据源告警
         * 9自定义-- 业务 ，10自定义-- 统计维度， 11自定义-- 普通维度， 12自定义--  多维度'
         */
        alarmRuleService.deleteAlarmRuleByComponentIds(ids);
        /**==============4、应用协议============================*/

        List<Integer> protocolIds = components.stream().map(Component::getPrototcolId).collect(Collectors.toList());
        protocolService.deleteProtocolPhysical(protocolIds);
        /**==============5、外部数据源(未整合cgc接口)==========================*/
        //TODO 有待处理
        datasourceRelevantService.deleteDatasourceRelevantByComponentIds(ids);
        /**==============6、自定义分析配置=======================*/
        customService.deleteCustomByComponentIds(ids);
		EntityWrapper<AlarmData> wrapper=new EntityWrapper<>();
		wrapper.in("alarmId", ids);
		/**==============7、bpm_alarm_data删除=======================*/
        alarmDataMapper.delete(wrapper);
        return true;
    }

    @Override
    public boolean deleteMonitor(int monitorId) {

        List<MonitorComponent> monitorComponentList =  applicationService.selectMonitorComponentByMonitorId(monitorId);
        //1、删除监控点
        applicationService.deleteMonitor(monitorId);

        //2、删除路径图相关信息
        List<Integer> monitorIds  = new ArrayList<>();
        monitorIds.add(monitorId);
        pathViewConfigService.deletePathViewByMonitorIds(monitorIds);

        //3、删除组件
        this.deleteComponent(monitorComponentList.stream().map(MonitorComponent::getComponentId).collect(Collectors.toList()));

        //4、删除监控点告警(普通)
        List<AlarmMonitor> alarmMonitors = alarmRuleService.selectAlarmMonitorByMonitorId(monitorId);
        alarmRuleService.deleteAlarmRuleByIds(alarmMonitors.stream().map(AlarmMonitor::getAlarmId).collect(Collectors.toList()));

        return true;
    }

    @Override
    public boolean deleteDimension(List<Integer> dimensionIds, int type) {
        alarmRuleService.deleteAlarmRuleByDimensionIds(dimensionIds,type);
        logger.debug("=====调用删除维度逻辑======");
        return true;
    }


	@Override
	public boolean deleteProtocolField(List<Integer> ids) {
		/*===========================删除自定义配置筛选条件======================*/
		customService.deleteFilterField(ids);
		/*===========================删除自定义配置统计维度======================*/
		customService.deletectStatisticsDimension(ids);
		/*===========================删除自定义配置成功值判断====================*/
		customService.deletectSuccessField(ids);
		/*===========================删除映射====================*/
		componentDimensionService.deleteFieldMapping(ids);
		return true;
	}

	@Override
	public boolean deleteDataSource(Integer id) {
		/*===========================删除外部数据配置======================*/
		datasourceRelevantService.deleteRelevantBySourcesId(id);
		return true;
	}

	@Override
	public boolean deleteIndicatorGroup(Integer id) {
		/*===========================删除外部数据配置======================*/
		datasourceRelevantService.deleteRelevantByGroupId(id);
		return true;
	}

	@Override
	public boolean deleteConstraintField(List<Integer> ids) {
		/*===========================删除外部数据配置======================*/
		datasourceRelevantService.deleteRelevantByFieldId(ids);
		return true;
	}

	@Override
	public boolean deleteDatasourceRelevant(Integer id) {
		datasourceRelevantService.delete(id);
		return true;
	}
	
	@Override
	public boolean deleteRelevantData(Integer componentId) {
		relevantService.delete(componentId);
		return true;
	}

}
