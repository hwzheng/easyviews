package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Abnormalinfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-12-03
 */
public interface AbnormalinfoMapper extends BaseMapper<Abnormalinfo> {



    /**
     * 根据条件查询其它基线模板
     * @author NJ
     * @date 2018/12/11 19:38
     * @param param
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<Map<String, Object>> selectAbnormalinfo(Map<String, Object> param);
}
