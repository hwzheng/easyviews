package com.greattimes.ev.bpm.service.config.impl;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.req.MultiScheduleDetailParam;
import com.greattimes.ev.bpm.config.param.req.MultiScheduleParam;
import com.greattimes.ev.bpm.entity.ScheduleRepeat;
import com.greattimes.ev.bpm.entity.ScheduleRepeatDays;
import com.greattimes.ev.bpm.entity.ScheduleRepeatTimes;
import com.greattimes.ev.bpm.entity.ScheduleSingle;
import com.greattimes.ev.bpm.mapper.ScheduleRepeatDaysMapper;
import com.greattimes.ev.bpm.mapper.ScheduleRepeatMapper;
import com.greattimes.ev.bpm.mapper.ScheduleRepeatTimesMapper;
import com.greattimes.ev.bpm.mapper.ScheduleSingleMapper;
import com.greattimes.ev.bpm.service.config.IScheduleService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 单次排期表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@Service
public class ScheduleServiceImpl extends ServiceImpl<ScheduleSingleMapper, ScheduleSingle> implements IScheduleService {
	@Autowired
	private ScheduleSingleMapper scheduleSingleMapper;
	@Autowired
	private ScheduleRepeatMapper scheduleRepeatMapper;
	@Autowired
	private ScheduleRepeatTimesMapper scheduleRepeatTimesMapper;
	@Autowired
	private ScheduleRepeatDaysMapper scheduleRepeatDaysMapper;

	@Override
	public List<ScheduleSingle> selectSingleSchedule(int applicationId) {
		Map<String, Object> colmap = new HashMap<String, Object>();
		colmap.put("applicationId", applicationId);
		return scheduleSingleMapper.selectByMap(colmap);
	}

	@Override
	public void addSingleSchedule(Map<String, Object> map) {
		ScheduleSingle scheduleSingle = new ScheduleSingle();
		scheduleSingle.setApplicationId(evUtil.getMapIntValue(map, "applicationId"));
		scheduleSingle.setRemarks(evUtil.getMapStrValue(map, "remarks"));
		scheduleSingle.setType(evUtil.getMapIntValue(map, "type"));
		scheduleSingle.setStart(new Timestamp(new Date(evUtil.getMapLongValue(map, "start")).getTime()));
		scheduleSingle.setEnd(new Timestamp(new Date(evUtil.getMapLongValue(map, "end")).getTime()));
		scheduleSingleMapper.insert(scheduleSingle);
	}

	@Override
	public void editSingleSchedule(Map<String, Object> map) {
		ScheduleSingle scheduleSingle = new ScheduleSingle();
		scheduleSingle.setId(evUtil.getMapIntValue(map, "id"));
		scheduleSingle.setApplicationId(evUtil.getMapIntValue(map, "applicationId"));
		scheduleSingle.setRemarks(evUtil.getMapStrValue(map, "remarks"));
		scheduleSingle.setType(evUtil.getMapIntValue(map, "type"));
		scheduleSingle.setStart(new Timestamp(new Date(evUtil.getMapLongValue(map, "start")).getTime()));
		scheduleSingle.setEnd(new Timestamp(new Date(evUtil.getMapLongValue(map, "end")).getTime()));
		scheduleSingleMapper.updateById(scheduleSingle);
	}

	@Override
	public List<MultiScheduleDetailParam> selectMultiSchedule(int applicationId) {
		return scheduleRepeatMapper.selectMultiSchedule(applicationId);
	}

	@Override
	public void saveMultiSchedule(MultiScheduleParam param) {
		Map<String, Object> columnMap = new HashMap<>();
		int applicationId = param.getApplicationId();
		columnMap.put("applicationId", applicationId);
		List<ScheduleRepeat> schedule = scheduleRepeatMapper.selectByMap(columnMap);
		Map<String, Object> scheduleId = new HashMap<>();
		// 删除时间表和日期表
		for (ScheduleRepeat scheduleRepeat : schedule) {
			scheduleId.put("scheduleId", scheduleRepeat.getId());
			scheduleRepeatTimesMapper.deleteByMap(scheduleId);
			scheduleRepeatDaysMapper.deleteByMap(scheduleId);
			scheduleId.clear();
		}
		// 删除重复排期表
		scheduleRepeatMapper.deleteByMap(columnMap);
		List<MultiScheduleDetailParam> multiScheduleDetailParam = param.getMultis();
		for (MultiScheduleDetailParam multis : multiScheduleDetailParam) {
			ScheduleRepeat sc = new ScheduleRepeat();
			sc.setActive(multis.getActive());
			sc.setTypes(multis.getTypes());
			sc.setApplicationId(applicationId);
			// 插入重复排期表
			scheduleRepeatMapper.insert(sc);
			String[] days = multis.getDay().split(",");
			for (String string : days) {
				ScheduleRepeatDays repeatDays = new ScheduleRepeatDays();
				repeatDays.setDay(Integer.parseInt(string));
				repeatDays.setScheduleId(sc.getId());
				// 插入日期表
				scheduleRepeatDaysMapper.insert(repeatDays);
			}
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
			if (multis.getTypes() == 2) {
				String[] times = multis.getTime().split(",");
				for (String string : times) {
					String[] time = string.split("~");
					ScheduleRepeatTimes repeatTimes = new ScheduleRepeatTimes();
					repeatTimes.setScheduleId(sc.getId());
					try {
						repeatTimes.setStart(simpleDateFormat.parse(time[0]));
						repeatTimes.setEnd(simpleDateFormat.parse(time[1]));
					} catch (ParseException e) {
						e.printStackTrace();
					}
					// 插入时间表
					scheduleRepeatTimesMapper.insert(repeatTimes);
				}
			}
		}
	}

	@Override
	public void deleteSingleSchedule(int id) {
		scheduleSingleMapper.deleteById(id);
	}

}
