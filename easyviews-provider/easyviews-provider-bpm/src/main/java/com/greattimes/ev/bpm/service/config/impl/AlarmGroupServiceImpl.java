package com.greattimes.ev.bpm.service.config.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.resp.AlarmGroupParam;
import com.greattimes.ev.bpm.entity.AlarmData;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.greattimes.ev.bpm.entity.AlarmLevel;
import com.greattimes.ev.bpm.entity.AlarmSendType;
import com.greattimes.ev.bpm.entity.AlarmSendWay;
import com.greattimes.ev.bpm.mapper.AlarmDataMapper;
import com.greattimes.ev.bpm.mapper.AlarmGroupMapper;
import com.greattimes.ev.bpm.mapper.AlarmGroupUserMapper;
import com.greattimes.ev.bpm.mapper.AlarmLevelMapper;
import com.greattimes.ev.bpm.mapper.AlarmSendTypeMapper;
import com.greattimes.ev.bpm.mapper.AlarmSendWayMapper;
import com.greattimes.ev.bpm.service.config.IAlarmGroupService;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.User;

import io.swagger.models.auth.In;

/**
 * <p>
 * 告警組表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
@Service
public class AlarmGroupServiceImpl extends ServiceImpl<AlarmGroupMapper, AlarmGroup> implements IAlarmGroupService {
	@Autowired
	private AlarmGroupMapper alarmGroupMapper;
	@Autowired
	private AlarmSendTypeMapper alarmSendTypeMapper;
	@Autowired
	private AlarmSendWayMapper alarmSendWayMapper;
	@Autowired
	private AlarmGroupUserMapper alarmGroupUserMapper;
	@Autowired
	private AlarmDataMapper alarmDataMapper;
	@Autowired
	private AlarmLevelMapper alarmLevelMapper;
	@Override
	public boolean checkName(AlarmGroup alarmGroup, int i) {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("name", alarmGroup.getName());
			List<AlarmGroup> list = new ArrayList<>();
			if (i == 1) {
				list = alarmGroupMapper.selectByMap(nameMap);
			} else {
				EntityWrapper<AlarmGroup> wrapper=new EntityWrapper<>();
				wrapper.where("name={0}", alarmGroup.getName()).ne("id", alarmGroup.getId());
				list = alarmGroupMapper.selectList(wrapper);
			}
			return evUtil.listIsNullOrZero(list);
	}
	@Override
	public void deleteAlarmGroup(int id) {
		alarmGroupMapper.deleteById(id);
		Map<String, Object> map=new HashMap<>(1);
		map.put("alarmGroupId", id);
		alarmSendTypeMapper.deleteByMap(map);
		alarmSendWayMapper.deleteByMap(map);
		alarmGroupUserMapper.deleteByMap(map);
		alarmDataMapper.deleteByMap(map);
		alarmLevelMapper.deleteByMap(map);
		
	}
	@Override
	public AlarmGroupParam selectAlarmGroup(Integer id) {
		AlarmGroupParam result=new AlarmGroupParam();
		result.setId(id);
		//user
		List<User> userList=alarmGroupUserMapper.selectUser(id);
		List<Map<String, Object>> u=new ArrayList<>();
		for (User user : userList) {
			Map<String, Object> umap=new HashMap<>(2);
			umap.put("userId", user.getId());
			umap.put("name", user.getName());
			u.add(umap);
		}
		result.setUser(u);
		//basic
		Map<String, Object> map=new HashMap<>(1);
		map.put("alarmGroupId", id);
		List<AlarmSendType> sendType = alarmSendTypeMapper.selectByMap(map);
		List<AlarmSendWay> sendWay=alarmSendWayMapper.selectByMap(map);
		List<AlarmLevel> level=alarmLevelMapper.selectByMap(map);
		Map<String, Object> basic=new HashMap<>();
		List<Integer> levels = level.stream().map(AlarmLevel::getLevel).collect(Collectors.toList());
		basic.put("level", levels);
		for (AlarmSendType t : sendType) {
			basic.put("sendType", t.getSendType());
			basic.put("compressTime", t.getCompressTime());
			basic.put("compressNum", t.getCompressNum());
		}
		List<Integer> ways = sendWay.stream().map(AlarmSendWay::getType).collect(Collectors.toList());
		basic.put("type", ways);
		result.setBasic(basic);
		//compont custom
		List<AlarmData> alarmComponentData=alarmDataMapper.selectByMap(map);
		List<Map<String, Object>> compontData=new ArrayList<>();
		List<Map<String, Object>> alarmEventData=new ArrayList<>();
		for (AlarmData alarmData : alarmComponentData) {
			Map<String, Object> compontMap=new HashMap<>(2);
			if(alarmData.getType()==1) {
				compontMap.put("applicationId", alarmData.getApplicationId());
				compontMap.put("componentId", alarmData.getAlarmId());
				compontData.add(compontMap);
			}
			if(alarmData.getType()==2) {
				compontMap.put("applicationId", alarmData.getApplicationId());
				compontMap.put("eventId", alarmData.getAlarmId());
				alarmEventData.add(compontMap);
			}
		}
		result.setAlarmComponentData(compontData);
		result.setAlarmEventData(alarmEventData);
		return result;
	}
	@Override
	public void saveGroup(AlarmGroupParam param) {
		int alarmGroupId=param.getId();
		Map<String, Object> remap=new HashMap<>(1);
		//删除
		remap.put("alarmGroupId", alarmGroupId);
		alarmSendTypeMapper.deleteByMap(remap);
		alarmSendWayMapper.deleteByMap(remap);
		alarmGroupUserMapper.deleteByMap(remap);
		alarmDataMapper.deleteByMap(remap);
		alarmLevelMapper.deleteByMap(remap);
		List<Map<String, Object>> user=param.getUser();
		if(!evUtil.listIsNullOrZero(user)) {
			alarmGroupUserMapper.batchInsert(user,alarmGroupId);
		}
		Map<String, Object> basic=param.getBasic();
		List<Integer> level=(List<Integer>) basic.get("level");
		if(!evUtil.listIsNullOrZero(level)) {
			alarmLevelMapper.batchInsert(level,alarmGroupId);
		}
		AlarmSendType entity=new AlarmSendType();
		entity.setAlarmGroupId(alarmGroupId);
		entity.setSendType(Integer.parseInt(basic.get("sendType").toString()));
		if(basic.get("compressNum") != null) {
			entity.setCompressNum(Integer.parseInt(basic.get("compressNum").toString()));
		}
		if(basic.get("compressTime") != null) {
			entity.setCompressTime(Integer.parseInt(basic.get("compressTime").toString()));
		}
		alarmSendTypeMapper.insert(entity);
		List<Integer> types=(List<Integer>) basic.get("type");
		if(!evUtil.listIsNullOrZero(types)) {
			alarmSendWayMapper.batchInsert(types,alarmGroupId);
		}
		List<Map<String, Object>> comp=param.getAlarmComponentData();
		List<Map<String, Object>> custom=param.getAlarmEventData();
		List<Map<String, Object>> data=new ArrayList<>();
		if(!evUtil.listIsNullOrZero(comp)) {
			for (Map<String, Object> cmap : comp) {
				Map<String, Object> map=new HashMap<>(4);
				map.put("applicationId", cmap.get("applicationId"));
				map.put("alarmGroupId", alarmGroupId);
				map.put("type", 1);
				map.put("alarmId", cmap.get("componentId"));
				data.add(map);
			}
		}
		if(!evUtil.listIsNullOrZero(custom)) {
			for (Map<String, Object> custommap : custom) {
				Map<String, Object> map=new HashMap<>(4);
				map.put("applicationId", custommap.get("applicationId"));
				map.put("alarmGroupId", alarmGroupId);
				map.put("type", 2);
				map.put("alarmId", custommap.get("eventId"));
				data.add(map);
			}
		}
		alarmDataMapper.batchInsert(data);
	}

}
