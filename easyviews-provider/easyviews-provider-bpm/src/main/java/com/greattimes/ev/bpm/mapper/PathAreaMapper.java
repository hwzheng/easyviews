package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.PathArea;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 路径图分组区域表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-11
 */
public interface PathAreaMapper extends BaseMapper<PathArea> {
    void batchInsert(List<PathArea> list);
}
