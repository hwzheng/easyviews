package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskData;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表结果数据表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-10-25
 */
public interface BottleneckTaskDataMapper extends BaseMapper<BottleneckTaskData> {

}
