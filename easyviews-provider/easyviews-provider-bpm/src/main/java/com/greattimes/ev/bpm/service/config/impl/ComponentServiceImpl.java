package com.greattimes.ev.bpm.service.config.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.enums.SqlLike;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import com.greattimes.ev.bpm.config.param.req.ComponentParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.resp.ComponentChainParam;
import com.greattimes.ev.bpm.config.param.resp.IpPortParam;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.mapper.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.common.ISequenceService;
import com.greattimes.ev.bpm.service.config.IComponentService;
import com.greattimes.ev.common.utils.evUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 应用组件表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
@Service
public class ComponentServiceImpl extends ServiceImpl<ComponentMapper, Component> implements IComponentService {

    @Autowired
    private ComponentMapper componentMapper;
    @Autowired
    private ServerGroupMapper serverGroupMapper;
    @Autowired
    private ServerIpMapper serverIpMapper;
    @Autowired
    private ServerPortMapper serverPortMapper;
    @Autowired
    private UuidMapper uuidMapper;
    @Autowired
    private IndicatorMapper indicatorMapper;
    @Autowired
    private ComponentDimensionMapper componentDimensionMapper;
    @Autowired
    private FieldMappingComponentMapper fieldMappingComponentMapper;
    @Autowired
    private FieldMappingCnfMapper fieldMappingCnfMapper;
    @Autowired
    private FieldMappingMapper fieldMappingMapper;
    @Autowired
    private FieldOtherNameMapper fieldOtherNameMapper;
    @Autowired
    private MonitorComponentMapper monitorComponentMapper;
    @Autowired
    private ProtocolMapper protocolMapper;
    @Autowired
    private ProtocolFieldMapper protocolFieldMapper;
    @Autowired
    private ProtocolMessageMapper protocolMessageMapper;
    @Autowired
    private ProtocolPacketMapper protocolPacketMapper;
    @Autowired
    private IDeleteRuleService deleteRuleService;
    @Autowired
    private ServerMapper serverMapper;
    @Autowired
    private ISequenceService sequenceService;
    @Autowired
    private DataGroupInfoMapper dataGroupInfoMapper;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public List<DictionaryParam> selectComponentNameAndIdByBusinessId(int businessId) {
        return componentMapper.selectComponentNameAndIdByBusinessId(businessId);
    }

    @Override
    public List<DictionaryParam> selectComponentIpById(int id) {
        return componentMapper.selectComponentIpById(id);
    }

    @Override
    public List<DictionaryParam> selectComponentPortByIp(int ip) {
        return componentMapper.selectComponentPortByIp(ip,evUtil.ipToStr(ip));
    }

	@Override
	public Component addOrUpdate(ComponentParam param,String name, int topicNum) throws InvocationTargetException, IllegalAccessException {
        List<ServerGroup> serverGroups;
		List<JSONObject> serverList = param.getServer();
		Integer componentId = param.getId();
		Component componentResult;
		boolean isAdd = (componentId == null);
		String uuIdPrefix;
        int appGroupId;
        int componentGroupId;
        //新增
		if (isAdd) {
            int component_id = sequenceService.sequenceNextVal("seq_uuid");
			// bpm_component
			Component component = new Component();
			BeanUtils.copyProperties(param, component);
			// 新增默认激活状态
			component.setActive(1);
			component.setSort(componentMapper.getMaxComponentSortByAppId(param.getApplicationId()) + 1);
            component.setId(component_id);
			componentMapper.insert(component);
            componentId = component.getId();

            /**
             * bpm_data_group_info  topic 创建
             * groupType
             * 1、同步消息头分组
             * 2、告警应用组
             * 3、告警组件组
             * 4、异步消息头分组
             */
            int applicationId = component.getApplicationId();
            int connectMode = component.getConnectMode();
            // 1、同步消息头分组
            componentGroupId = (componentId % topicNum)+1;
            DataGroupInfo dataGroupInfo_1 = new DataGroupInfo();
            dataGroupInfo_1.setGroupId(componentGroupId);
            dataGroupInfo_1.setGroupName("SyncBpm-"+componentGroupId);
            dataGroupInfo_1.setGroupType(connectMode == 0 ? 1 : 4);
            dataGroupInfo_1.setValue(componentId);
            dataGroupInfoMapper.insert(dataGroupInfo_1);
            //3、告警组件组
            DataGroupInfo dataGroupInfo_3 = new DataGroupInfo();
            dataGroupInfo_3.setGroupId(componentGroupId);
            dataGroupInfo_3.setGroupType(3);
            dataGroupInfo_3.setValue(componentId);
            dataGroupInfoMapper.insert(dataGroupInfo_3);
            //2、告警应用组
            appGroupId = (applicationId % topicNum) + 1;

            EntityWrapper<DataGroupInfo> dataGroupInfoEntityWrapper = new EntityWrapper<>();
            dataGroupInfoEntityWrapper.where("groupType={0} AND groupId={1} AND value = {2}", 2, appGroupId, applicationId);
            List<DataGroupInfo> dataGroupInfos = dataGroupInfoMapper.selectList(dataGroupInfoEntityWrapper);
            if(evUtil.listIsNullOrZero(dataGroupInfos)){
                DataGroupInfo dataGroupInfo_2 = new DataGroupInfo();
                dataGroupInfo_2.setGroupType(2);
                dataGroupInfo_2.setGroupId(appGroupId);
                dataGroupInfo_2.setValue(applicationId);
                dataGroupInfoMapper.insert(dataGroupInfo_2);
            }
            //4、异步消息头分组
            //`connectMode` int(11) NOT NULL COMMENT '连接模式（同步-0 异步-1）',
            if(component.getConnectMode() == 1){
                EntityWrapper<DataGroupInfo> dataGroupInfoEntityWrapper1 = new EntityWrapper<>();
                dataGroupInfoEntityWrapper1.where("groupType={0} AND groupId={1} AND value = {2} AND groupName={3}",
                        4, appGroupId, applicationId, "AsyncBpm-"+appGroupId);
                List<DataGroupInfo> dataGroupInfos1 = dataGroupInfoMapper.selectList(dataGroupInfoEntityWrapper1);
                if(evUtil.listIsNullOrZero(dataGroupInfos1)){
                    DataGroupInfo dataGroupInfo_4 = new DataGroupInfo();
                    dataGroupInfo_4.setGroupType(4);
                    dataGroupInfo_4.setGroupId(appGroupId);
                    dataGroupInfo_4.setValue(applicationId);
                    dataGroupInfo_4.setGroupName("AsyncBpm-"+appGroupId);
                    dataGroupInfoMapper.insert(dataGroupInfo_4);
                }
            }

			// 生成应用协议
			addAppprotocol(componentId, component.getPrototcolId(),name,component.getPatternFieldUp(),component.getPatternFieldDown());

			// bpm_monitor_component
			MonitorComponent monitorComponent = new MonitorComponent(param.getMonitorId(), componentId);
			monitorComponentMapper.insert(monitorComponent);

			// bpm_uuid
			uuIdPrefix = "applicationid:" + param.getApplicationId() + ",componentid";
			List<String> ids = new ArrayList<>();
			ids.add(componentId.toString());
			List<Uuid> uuids = this.handUuids(uuIdPrefix, ids);
			uuidMapper.insert(uuids.get(0));
			uuIdPrefix = uuids.get(0).getValue();


            //hand bpm_server_group && bpm_server_ip && bpm_server_port
            // bpm_server_group 新增
            serverGroups = this.handServerGroups(serverList, componentId);

            while (serverGroups.size() > 30) {
                List<ServerGroup> subList = serverGroups.subList(0, 30);
                serverGroupMapper.batchInsert(subList);
                subList.clear();
            }
            if (serverGroups.size() != 0) {
                serverGroupMapper.batchInsert(serverGroups);
            }


            List<Uuid> ipPortsUuids = new ArrayList<>();
            // bpm_server_ip&&bpm_server_port 参数中内容格式形如ip:192.168.0.1-80,192.168.0.130
            // port:80-90,98-99 处理方式拆分成单值，处理组合方式笛卡尔积
            List<ServerIp> serverIps = this.handServerIps(serverGroups,true);
            List<ServerPort> serverPorts = this.handServerPorts(serverGroups);

            //batch save bpm_server
            List<Server> servers =  this.handIpPortServer(serverIps, serverPorts, true);
            while (servers.size() > 30) {
                List<Server> subList = servers.subList(0, 30);
                serverMapper.batchInsert(subList);
                ipPortsUuids.addAll(this.handServerUuids(uuIdPrefix, subList));
                subList.clear();
            }
            if (servers.size() != 0) {
                serverMapper.batchInsert(servers);
                ipPortsUuids.addAll(this.handServerUuids(uuIdPrefix, servers));
            }

            // batch save bpm_server_ip && bpm_uuid
            Map<String, Integer> ipStrMap = new HashMap<>();
            List<ServerIp> ipResult = new ArrayList<>();
            while (serverIps.size() > 30) {
                List<ServerIp> subList = serverIps.subList(0, 30);
                serverIpMapper.batchInsert(subList);
                subList.stream().forEach(serverIp -> {
                    ipStrMap.put(serverIp.getIp(), serverIp.getId());
                });
                ipResult.addAll(subList);
                subList.clear();
            }
            if (serverIps.size() != 0) {
                serverIpMapper.batchInsert(serverIps);
                serverIps.stream().forEach(serverIp -> {
                    ipStrMap.put(serverIp.getIp(), serverIp.getId());
                });
                ipResult.addAll(serverIps);
            }

            ipPortsUuids.addAll(this.handUuids(uuIdPrefix + ",ip", ipStrMap));

            // batch save bpm_server_port
            while (serverPorts.size() > 30) {
                List<ServerPort> subList = serverPorts.subList(0, 30);
                serverPortMapper.batchInsert(subList);
                subList.clear();
            }
            if (serverPorts.size() != 0) {
                serverPortMapper.batchInsert(serverPorts);
            }

            // bpm_uuid batchsave ipAndPortsUuids
            while (ipPortsUuids.size() > 30) {
                List<Uuid> subList = ipPortsUuids.subList(0, 30);
                uuidMapper.batchInsert(subList);
                subList.clear();
            }
            if (ipPortsUuids.size() != 0) {
                uuidMapper.batchInsert(ipPortsUuids);
            }
            componentResult = componentMapper.selectById(component_id);
        } else {

			// 编辑 bpm_component 修改名称
			Component component = componentMapper.selectById(param.getId());
			Integer active = component.getActive();
			Integer prototcolId = component.getPrototcolId();
			Integer sort = component.getSort();
			BeanUtils.copyProperties(param, component);
            if(component.getMsgLimit() == null){
                component.setMsgLimit(2048);
            }
            if(component.getLogLevel() == null){
                component.setLogLevel(4);
            }
            // 如果连接模式为同步
			if (param.getConnectMode().intValue() == 0) {
			    //清除异步component,serviceType 该字段的值
				component.setComponent(null);
				component.setServiceType(null);

				//清除响应模式
                component.setResponseType(null);
			}
			component.setActive(active);
			component.setPrototcolId(prototcolId);
			component.setSort(sort);
			componentMapper.updateAllColumnById(component);
            componentResult = component;

            /**
             * bpm_data_group_info  topic 创建
             * groupType
             * 1、同步消息头分组
             * 2、告警应用组
             * 3、告警组件组
             * 4、异步消息头分组
             */
            int applicationId = component.getApplicationId();
            int connectMode = component.getConnectMode();
            // 1、同步消息头分组
            componentGroupId = (componentId % topicNum)+1;
            EntityWrapper<DataGroupInfo> dataGroupInfoEntityWrapper = new EntityWrapper<>();
            dataGroupInfoEntityWrapper.where("groupType={0} AND groupId={1} AND value = {2} AND groupName = {3}",
                    connectMode == 0 ? 1 : 4, componentGroupId, componentId, "SyncBpm-"+componentGroupId);
            List<DataGroupInfo> dataGroupInfos = dataGroupInfoMapper.selectList(dataGroupInfoEntityWrapper);
            if(evUtil.listIsNullOrZero(dataGroupInfos)){
                DataGroupInfo dataGroupInfo_1 = new DataGroupInfo();
                dataGroupInfo_1.setGroupId(componentGroupId);
                dataGroupInfo_1.setGroupName("SyncBpm-"+componentGroupId);
                dataGroupInfo_1.setGroupType(connectMode == 0 ? 1 : 4);
                dataGroupInfo_1.setValue(componentId);
                dataGroupInfoMapper.insert(dataGroupInfo_1);
            }

            EntityWrapper<DataGroupInfo> dataGroupInfoEntityWrapper1 = new EntityWrapper<>();
            dataGroupInfoEntityWrapper1.where("groupType={0} AND groupId={1} AND value = {2} ",
                    3, componentGroupId, componentId);
            List<DataGroupInfo> dataGroupInfos1 = dataGroupInfoMapper.selectList(dataGroupInfoEntityWrapper1);
            if(evUtil.listIsNullOrZero(dataGroupInfos1)){
                DataGroupInfo dataGroupInfo_3 = new DataGroupInfo();
                dataGroupInfo_3.setGroupId(componentGroupId);
                dataGroupInfo_3.setGroupType(3);
                dataGroupInfo_3.setValue(componentId);
                dataGroupInfoMapper.insert(dataGroupInfo_3);
            }


            //2、告警应用组
            appGroupId = (applicationId % topicNum) + 1;
            EntityWrapper<DataGroupInfo> dataGroupInfoEntityWrapper2 = new EntityWrapper<>();
            dataGroupInfoEntityWrapper2.where("groupType={0} AND groupId={1} AND value = {2} ",
                    2, appGroupId, applicationId);
            List<DataGroupInfo> dataGroupInfos2 = dataGroupInfoMapper.selectList(dataGroupInfoEntityWrapper2);
            if(evUtil.listIsNullOrZero(dataGroupInfos2)){
                DataGroupInfo dataGroupInfo_2 = new DataGroupInfo();
                dataGroupInfo_2.setGroupType(2);
                dataGroupInfo_2.setGroupId(appGroupId);
                dataGroupInfo_2.setValue(applicationId);
                dataGroupInfoMapper.insert(dataGroupInfo_2);
            }


            //4、异步消息头分组
            //`connectMode` int(11) NOT NULL COMMENT '连接模式（同步-0 异步-1）',
            if(component.getConnectMode() == 1){
                EntityWrapper<DataGroupInfo> dataGroupInfoEntityWrapper3 = new EntityWrapper<>();
                dataGroupInfoEntityWrapper3.where("groupType={0} AND groupId={1} AND value = {2} AND groupName = {3}",
                        4, appGroupId, applicationId, "AsyncBpm-"+appGroupId);
                List<DataGroupInfo> dataGroupInfos3 = dataGroupInfoMapper.selectList(dataGroupInfoEntityWrapper3);
                if(evUtil.listIsNullOrZero(dataGroupInfos3)){
                    DataGroupInfo dataGroupInfo_4 = new DataGroupInfo();
                    dataGroupInfo_4.setGroupType(4);
                    dataGroupInfo_4.setGroupId(appGroupId);
                    dataGroupInfo_4.setGroupName("AsyncBpm-"+appGroupId);
                    dataGroupInfo_4.setValue(applicationId);
                    dataGroupInfoMapper.insert(dataGroupInfo_4);
                }

            }

			// bpm_monitor_component
			MonitorComponent monitorComponent = new MonitorComponent(param.getMonitorId(), componentId);
			Map<String, Object> map = new HashMap<>(1);
			map.put("componentId", componentId);
			monitorComponentMapper.deleteByMap(map);
			monitorComponentMapper.insert(monitorComponent);


			uuIdPrefix = "applicationid:" + component.getApplicationId() + ",componentid:" + component.getId();
//            EntityWrapper<Uuid> ew = new EntityWrapper<>();
            // 后导%
//            ew.like("value", uuIdPrefix, SqlLike.RIGHT);
//            List<Uuid> uuids = uuidMapper.selectList(ew);

            //bpm_server_group
			Wrapper<ServerGroup> serverWrapper = new EntityWrapper<>();
			serverWrapper.eq("componentId", component.getId());
			List<ServerGroup> groupList = serverGroupMapper.selectList(serverWrapper);
			List<Integer> serverGroupIds = new ArrayList<>();
			groupList.stream().forEach(serverGroup -> {
				serverGroupIds.add(serverGroup.getId());
			});

			//delete bpm_server_group
			serverGroupMapper.delete(serverWrapper);

			EntityWrapper<ServerIp> ipEntityWrapper = new EntityWrapper<>();
			ipEntityWrapper.in("serverGroupId", serverGroupIds);
			List<ServerIp> oldServerIps = serverIpMapper.selectList(ipEntityWrapper);

			EntityWrapper<ServerPort> portEntityWrapper = new EntityWrapper<>();
			portEntityWrapper.in("serverGroupId", serverGroupIds);
			List<ServerPort> oldServerPorts = serverPortMapper.selectList(portEntityWrapper);

			EntityWrapper<Server> serverEntityWrapper = new EntityWrapper<>();
            serverEntityWrapper.in("serverGroupId", serverGroupIds);
            List<Server> oldServers = serverMapper.selectList(serverEntityWrapper);


            // bpm_server_group(页面新的数据)
            serverGroups = this.handServerGroups(serverList, componentId);

            while (serverGroups.size() > 30) {
                List<ServerGroup> subList = serverGroups.subList(0, 30);
                serverGroupMapper.batchInsert(subList);
                subList.clear();
            }
            if (serverGroups.size() != 0) {
                serverGroupMapper.batchInsert(serverGroups);
            }

            //新生成的serverPorts
            List<ServerPort> serverPorts = this.handServerPorts(serverGroups);

            //处理serverIps
            List<ServerIp> serverIps = this.handServerIps(serverGroups,false);

            List<Server> servers =  this.handIpPortServer(serverIps, serverPorts,false);

            this.updateServerIps(oldServerIps, serverIps, uuIdPrefix+ ",ip");

            //处理servers and ports
            //delete ports
            if(!evUtil.listIsNullOrZero(oldServerPorts)){
                serverPortMapper.batchDelete(oldServerPorts);
            }
            //add ports 注意bpm_server_port的id不是bpm_uuid里面的值
            while (serverPorts.size() > 30) {
                List<ServerPort> subList = serverPorts.subList(0, 30);
                serverPortMapper.batchInsert(subList);
                subList.clear();
            }
            if (serverPorts.size() != 0) {
                serverPortMapper.batchInsert(serverPorts);
            }
            //delete oldServers
            this.updateServersAndPorts(oldServers, servers,uuIdPrefix);
		}

        /**
         * 编辑组件，ip和port会发生变动导致对告警模块进行删除操作`.
         *
         */
        if(!isAdd){
            List<DictionaryParam> ipDicts = componentMapper.selectComponentIpById(componentId);
            Set<String> ipstrs = new HashSet<>();
            ipDicts.stream().forEach(x->{
                ipstrs.add(x.getLabel());
            });
            Set<String> ipportstr = new HashSet<>();
            Map<String, Object> portMap = new HashMap<>(1);
            portMap.put("componentId",componentId);
            List<DictionaryParam> ipPortDicts = componentMapper.selectComponentPortByComponentAndIp(portMap);
            ipPortDicts.stream().forEach(x->{
                ipportstr.add(x.getLabel());
            });
            //调用删除应用告警(ip,ip/port告警)
            deleteRuleService.deleteAlarmByIpPorts(ipstrs, ipportstr, componentId);
            //删除外部数据
            deleteRuleService.deleteRelevantData(componentId);
        }

		return componentResult;
	}

    /**
     * 更新serverIp
     * @param oldServerIps
     * @param newServerIps
     * @param uuidPrefix
     */
	private void updateServerIps(List<ServerIp> oldServerIps, List<ServerIp> newServerIps, String uuidPrefix){

	    //hand serverIp
	    List<ServerIp> delOldIps = new ArrayList<>();
	    List<ServerIp> updateOldIps = new ArrayList<>();
	    List<ServerIp> insertIps = new ArrayList<>();
	    Integer oldIntIp;
	    Integer newIntIp;
	    boolean flag;
	    for(ServerIp oldIp : oldServerIps){
            oldIntIp = oldIp.getIntIp();
            Iterator<ServerIp> iterator = newServerIps.iterator();
            flag = false;
            while(iterator.hasNext()){
                ServerIp newServerIp = iterator.next();
                newIntIp = newServerIp.getIntIp();
                if(oldIntIp.equals(newIntIp)){
                    //更新操作
                    oldIp.setServerGroupId(newServerIp.getServerGroupId());
                    updateOldIps.add(oldIp);
                    iterator.remove();
                    flag = true;
                    break;
                }
            }
            if(!flag){
                //删除逻辑
                delOldIps.add(oldIp);
            }

        }

        //delete old ServerIp && uuids
        while (delOldIps.size() > 30) {
            List<ServerIp> subList = delOldIps.subList(0, 30);
            List<Integer> deleteIds = subList.stream().map(ServerIp::getId).collect(Collectors.toList());
            serverIpMapper.batchDeleteByIds(deleteIds);
            //delete uuids
            uuidMapper.batchDeleteByIds(deleteIds);
            subList.clear();
        }
        if (delOldIps.size() != 0) {
            List<Integer> deleteIds = delOldIps.stream().map(ServerIp::getId).collect(Collectors.toList());
            serverIpMapper.batchDeleteByIds(deleteIds);
            //delete uuids
            uuidMapper.batchDeleteByIds(deleteIds);
        }

        //update serverIp
        if(!evUtil.listIsNullOrZero(updateOldIps)){
            updateOldIps.stream().forEach(x->{
                serverIpMapper.updateById(x);
            });
        }

        // add new serverIp
        if(!evUtil.listIsNullOrZero(newServerIps)){
            for(ServerIp serverIp : newServerIps){
                //serverIp id
                serverIp.setId(sequenceService.sequenceNextVal("seq_uuid"));
                insertIps.add(serverIp);
            }
            while(insertIps.size() > 30){
                List<ServerIp> subList = insertIps.subList(0, 30);
                serverIpMapper.batchInsert(subList);
                uuidMapper.batchInsert(this.handIpUuids(uuidPrefix, subList));
                subList.clear();
            }
            if(insertIps.size() != 0){
                serverIpMapper.batchInsert(insertIps);
                uuidMapper.batchInsert(this.handIpUuids(uuidPrefix, insertIps));
            }
        }
    }


    /**
     * 更新servers
     * @param oldServers
     * @param newServers
     */
    private void updateServersAndPorts(List<Server> oldServers, List<Server> newServers, String uuidPrefix){
        List<Server> delOldServers = new ArrayList<>();
        List<Server> updateServers = new ArrayList<>();
        List<Server> insertServers =  new ArrayList<>();
        List<Uuid> uuidList = new ArrayList<>();
        Integer oldIntIp,newIntIp,oldPort,newPort;
        boolean flag;
        for(Server oldServer : oldServers){
            oldIntIp = oldServer.getIntIp();
            oldPort = oldServer.getPort();
            Iterator<Server> iterator = newServers.iterator();
            flag = false;
            while(iterator.hasNext()){
                Server newServer = iterator.next();
                newIntIp = newServer.getIntIp();
                newPort = newServer.getPort();
                if(oldIntIp.equals(newIntIp) && oldPort.equals(newPort)){
                    //更新操作
                    oldServer.setServerGroupId(newServer.getServerGroupId());
                    updateServers.add(oldServer);
                    iterator.remove();
                    flag = true;
                    break;
                }
            }
            if(!flag){
                //删除逻辑
                delOldServers.add(oldServer);
            }
        }

        //delete old servers && uuids
        while (delOldServers.size() > 30) {
            List<Server> subList = delOldServers.subList(0, 30);
            List<Integer> deleteIds = subList.stream().map(Server::getId).collect(Collectors.toList());
            serverMapper.batchDeleteByIds(deleteIds);
            //delete uuids
            uuidMapper.batchDeleteByIds(deleteIds);
            subList.clear();
        }
        if (delOldServers.size() != 0) {
            List<Integer> deleteIds = delOldServers.stream().map(Server::getId).collect(Collectors.toList());
            serverMapper.batchDeleteByIds(deleteIds);
            //delete uuids
            uuidMapper.batchDeleteByIds(deleteIds);
        }

        //update servers && port
        if(!evUtil.listIsNullOrZero(updateServers)){
            updateServers.stream().forEach(x->{
                serverMapper.updateById(x);
            });
        }

        // add new servers && serverport
        if(!evUtil.listIsNullOrZero(newServers)){
            for(Server server : newServers){
                //server id
                server.setId(sequenceService.sequenceNextVal("seq_uuid"));
                insertServers.add(server);
                Uuid uuid = new Uuid();
                uuid.setId((long)server.getId());
                uuid.setContraintGroupId(1);
                uuid.setActive(1);
                uuid.setValue(uuidPrefix+",ip:"+server.getIp()+",port:"+ server.getPort());
                uuidList.add(uuid);
            }
            while(insertServers.size() > 30){
                List<Server> subList = insertServers.subList(0, 30);
                serverMapper.batchInsert(subList);
                subList.clear();
            }
            if(insertServers.size() != 0){
                serverMapper.batchInsert(insertServers);
            }
        }
        if(!evUtil.listIsNullOrZero(uuidList)){
            while (uuidList.size() > 30) {
                List<Uuid> subList = uuidList.subList(0, 30);
                uuidMapper.batchInsert(subList);
                subList.clear();
            }
            if (uuidList.size() != 0) {
                uuidMapper.batchInsert(uuidList);
            }
        }
    }


	/**
	 * 参数处理serverGroup
	 * @author NJ
	 * @date 2018/12/5 15:55
	 * @param jsonObjects
	 * @param componentId
	 * @return java.util.List<com.greattimes.ev.bpm.entity.ServerGroup>
	 */
	private List<ServerGroup> handServerGroups(List<JSONObject> jsonObjects, Integer componentId){
        List<ServerGroup> res = new ArrayList<>();
        for (JSONObject jsonObject : jsonObjects) {
            String ipStr, portStr;
            ServerGroup serverGroup = new ServerGroup();
            serverGroup.setComponentId(componentId);
            ipStr = this.formatIpPortStr(jsonObject.getString("ip"));
            portStr = this.formatIpPortStr(jsonObject.getString("port"));
            if (StringUtils.isBlank(ipStr)) {
                return res;
            }
            serverGroup.setIp(ipStr);
            serverGroup.setPort(portStr);
            res.add(serverGroup);
        }
	    return res;
    }

	/**
	 * 生成应用协议
	 *
	 * @param componentId
	 * @param prototcolId
	 * @param name
	 * @throws UnsupportedEncodingException
	 */
	private void addAppprotocol(Integer componentId, Integer prototcolId, String name, Integer patternFieldUp, Integer patternFieldDown){
		Protocol p = protocolMapper.selectById(prototcolId);
		String ret = null;
		try {
			ret = new String(p.getContent(),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jb = JSON.parseObject(ret);
		//JProtocolDetail jp = JSON.toJavaObject(jb, JProtocolDetail.class);
		Integer insertPatternFieldUp = null;
        Integer insertPatternFieldDown = null;
		int id=sequenceService.sequenceNextVal("decode_protocol_id");
		//应用协议的key取“protocol_模版协议id_组件id”
		p.setKey("protocol_"+p.getId()+"_"+componentId);
		p.setId(id);
		jb.put("protocolId", p.getId());
		jb.put("protocolKey", p.getKey());
		//jp.setProtocolId(p.getId());
		//jp.setProtocolKey(p.getKey());
		try {
			p.setContent(jb.toString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		p.setFlag(0);
		p.setCreateBy(name);
		p.setCreateDate(new Date());
		p.setUpdateBy(name);
		p.setUpdateDate(new Date());
		protocolMapper.insertProtocol(p);
		Map<String, Object> col=new HashMap<>(1);
		col.put("protocolId", prototcolId);
		List<ProtocolField> field=protocolFieldMapper.selectByMap(col);
        int insertProtocolId = p.getId();
		for (ProtocolField protocolField : field) {
            //前后追踪字段id,新增和保存协议(模板协议->应用协议) 协议字段的i会变化
            if(patternFieldUp != null && patternFieldDown != null){
                //前后追踪字段相等
                if(patternFieldUp.equals(patternFieldDown) && protocolField.getId().equals(patternFieldUp)){
                    insertPatternFieldDown = insertPatternFieldUp = this.insertProtocolField(protocolField, insertProtocolId);
                }else{
                    if(protocolField.getId().equals(patternFieldUp)){
                        insertPatternFieldUp = this.insertProtocolField(protocolField, insertProtocolId);
                    }else if(protocolField.getId().equals(patternFieldDown)){
                        insertPatternFieldDown = this.insertProtocolField(protocolField, insertProtocolId);
                    }else{
                        this.insertProtocolField(protocolField, insertProtocolId);
                    }
                }
            }else if(patternFieldUp != null && protocolField.getId().equals(patternFieldUp)){
                insertPatternFieldUp = this.insertProtocolField(protocolField, insertProtocolId);
            }else if(patternFieldDown != null && protocolField.getId().equals(patternFieldDown)){
                insertPatternFieldDown = this.insertProtocolField(protocolField, insertProtocolId);
            }else{
                this.insertProtocolField(protocolField, insertProtocolId);
            }
		}
		List<ProtocolMessage> message=protocolMessageMapper.selectByMap(col);
		for (ProtocolMessage protocolMessage : message) {
			protocolMessage.setId(null);
			protocolMessage.setProtocolId(p.getId());
			protocolMessageMapper.insert(protocolMessage);
		}
		List<ProtocolPacket> packet=protocolPacketMapper.selectByMap(col);
		for (ProtocolPacket protocolPacket : packet) {
			protocolPacket.setId(null);
			protocolPacket.setProtocolId(p.getId());
			protocolPacketMapper.insert(protocolPacket);
		}
		Component entity = new Component();
		entity.setId(componentId);
		entity.setPrototcolId(p.getId());
		entity.setPatternFieldUp(insertPatternFieldUp);
		entity.setPatternFieldDown(insertPatternFieldDown);
		componentMapper.updateById(entity);
	}

    @Override
    public List<ComponentParam> selectComponentParamListByAppId(int businessId) {
        return componentMapper.selectComponentParmListByAppId(businessId);
    }

	@Override
	public void updateBatchSort(List<ComponentParam> list) {
		while (list.size() > 10) {
			List<ComponentParam> subList = list.subList(0, 10);
			componentMapper.updateBatchSort(subList);
			subList.clear();
		}
		if (list.size() != 0) {
			componentMapper.updateBatchSort(list);
		}
	}

    @Override
    public int updateActive(int id, int active) {
        Component component = new Component();
        component.setId(id);
        component.setActive(active);
        return componentMapper.updateById(component);
    }

    @Override
    public ComponentParam getComponentDetail(int id){
        Component component = componentMapper.selectById(id);
        ComponentParam componentParam = new ComponentParam();
        BeanUtils.copyProperties(component,componentParam);
        //bpm_server_group
        Wrapper<ServerGroup> serverWrapper = new EntityWrapper<>();
        //添加排序字段
        serverWrapper.eq("componentId", id);
        List<ServerGroup> serverGroups = serverGroupMapper.selectList(serverWrapper);
        List<JSONObject> jsonObjectList = new ArrayList<>();
        serverGroups.stream().forEach(serverGroup -> {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("ip", serverGroup.getIp());
            jsonObject.put("port", serverGroup.getPort());
            jsonObjectList.add(jsonObject);
        });
        componentParam.setServer(jsonObjectList);

        //bpm_monitor_component
        Map<String, Object> map = new HashMap<>(1);
        map.put("componentId", id);
        List<MonitorComponent> monitorComponentList = monitorComponentMapper.selectByMap(map);
        if(!evUtil.listIsNullOrZero(monitorComponentList)){
            componentParam.setMonitorId(monitorComponentList.get(0).getMonitorId());
        }
        return componentParam;
    }

    @Override
    public List<DictionaryParam> selectPortByComponentIdAndIp(int componentId, int ip) {
        Map<String, Object> param = new HashMap<>();
        param.put("componentId",componentId);
        param.put("ip",ip);
        return componentMapper.selectComponentPortByComponentAndIp(param);
    }

	@Override
	public List<DictionaryParam> selectPortByComponentIdAndIps(int componetId, List<Integer> ips) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("componentId", componetId);
		param.put("ips", ips);
		return componentMapper.selectComponentPortByComponentAndIp(param);
	}

    /**
     * 切分ip  参数中内容格式形如ip:192.168.0.1-80\n192.168.0.130
     * @param serverGroups
     * @param needSeq
     * @return
     */
    private List<ServerIp> handServerIps( List<ServerGroup> serverGroups, boolean needSeq){
        List<ServerIp> serverIps = new ArrayList<>();
        int serverGroupId;
        for(ServerGroup serverGroup : serverGroups){
            serverGroupId = serverGroup.getId();
            //bpm_server_ip 处理
            String ipStr = serverGroup.getIp().trim();
            String [] ipStrs = ipStr.split("\n|\r");
            for(String ip : ipStrs){
                if(StringUtils.isBlank(ip)){
                    continue;
                }
                String [] ips =  ip.trim().split("-");
                if(ips.length > 1){
                    //处理该种形式ip:192.168.0.1-80
                    int lastPointIndex = ips[0].lastIndexOf(".");
                    String lastIpNumStr = ips[0].substring(lastPointIndex+1);
                    String fristAndThirdIpNumStr = ips[0].substring(0,lastPointIndex+1);
                    int fristNum = Integer.parseInt(lastIpNumStr);
                    int lastNum = Integer.parseInt(ips[1]);
                    while(fristNum <= lastNum){
                        ServerIp serverIp = new ServerIp();
                        String handIp = fristAndThirdIpNumStr + fristNum;
                        serverIp.setIp(handIp);
                        if(needSeq){
                            serverIp.setId(sequenceService.sequenceNextVal("seq_uuid"));
                        }
                        serverIp.setIntIp(evUtil.ipToInt(handIp));
                        serverIp.setServerGroupId(serverGroupId);
                        serverIps.add(serverIp);
                        fristNum ++;
                    }
                }else{
                    ServerIp serverIp = new ServerIp();
                    serverIp.setIntIp(evUtil.ipToInt(ips[0]));
                    serverIp.setIp(ips[0]);
                    serverIp.setServerGroupId(serverGroupId);
                    if(needSeq){
                        serverIp.setId(sequenceService.sequenceNextVal("seq_uuid"));
                    }
                    serverIps.add(serverIp);
                }
            }
        }
        return serverIps;
    }

    /**
     * port:80-90\n98-99
     * @param serverGroups
     * @return
     */
    private List<ServerPort> handServerPorts( List<ServerGroup> serverGroups){
        List<ServerPort> serverPorts = new ArrayList<>();
        int serverGroupId;
        for(ServerGroup serverGroup : serverGroups){
            serverGroupId = serverGroup.getId();
            //bpm_server_port 处理
            String portStr = serverGroup.getPort().trim();
            String[] portStrs = portStr.split("\n|\r");
            for(String port : portStrs){
                if(StringUtils.isBlank(port)){
                    continue;
                }
                String [] ports =  port.trim().split("-");
                if(ports.length > 1){
                    int portStart = Integer.parseInt(ports[0]);
                    int portEnd = Integer.parseInt(ports[1]);
                    while(portStart <= portEnd){
                        ServerPort serverPort = new ServerPort();
                        serverPort.setPort(portStart);
                        serverPort.setServerGroupId(serverGroupId);
                        serverPorts.add(serverPort);
                        portStart ++;
                    }
                }else{
                    ServerPort serverPort = new ServerPort();
                    serverPort.setPort(Integer.parseInt(ports[0]));
                    serverPort.setServerGroupId(serverGroupId);
                    serverPorts.add(serverPort);
                }
            }
        }
        return serverPorts;
    }

    /**
     * 组装server
     * @param serverIps
     * @param serverPorts
     * @return
     */
    private List<Server> handIpPortServer(List<ServerIp> serverIps, List<ServerPort> serverPorts, boolean needSeq){
        List<Server> servers = new ArrayList<>();
        for(ServerIp serverIp : serverIps){
            for(ServerPort serverPort :serverPorts){
                if(serverIp.getServerGroupId().intValue() == serverPort.getServerGroupId().intValue()){
                    Server server = new Server();
                    if(needSeq){
                        server.setId(sequenceService.sequenceNextVal("seq_uuid"));
                    }
                    server.setIntIp(serverIp.getIntIp());
                    server.setIp(serverIp.getIp());
                    server.setPort(serverPort.getPort());
                    server.setServerGroupId(serverPort.getServerGroupId());
                    servers.add(server);
                }
            }
        }
        return servers;
    }


    /**
     * 组装uuid
     * @param type
     * @param strList
     * @return
     */
    private List<Uuid> handUuids(String type, List<String> strList){
        List<Uuid> result = new ArrayList<>();
        strList.stream().forEach(x->{
            Uuid uuid = new Uuid();
            uuid.setId(Long.parseLong(x));
            uuid.setValue(type+ ":" +x);
            uuid.setContraintGroupId(1);
            uuid.setActive(1);
            result.add(uuid);
        });
        return result;
    }

    /**
     *
     * @author NJ
     * @date 2018/9/19 14:25
     * @param type
     * @param map
     * @return java.util.List<com.greattimes.ev.bpm.entity.Uuid>
     */
    private List<Uuid> handUuids(String type, Map<String, Integer> map ){
        List<Uuid> result = new ArrayList<>();
        map.forEach((x,y)->{
            Uuid uuid = new Uuid();
            uuid.setId((long)y);
            uuid.setValue(type+ ":" +x);
            uuid.setContraintGroupId(1);
            uuid.setActive(1);
            result.add(uuid);
        });
        return result;
    }

    /**
     * 生成serverIp的uuid
     * @param prefix
     * @param serverIps
     * @return
     */
    private List<Uuid> handIpUuids(String prefix, List<ServerIp> serverIps){
        List<Uuid> result = new ArrayList<>();
        serverIps.forEach(x->{
            Uuid uuid = new Uuid();
            uuid.setId((long)x.getId());
            uuid.setValue(prefix+ ":" +x.getIp());
            uuid.setContraintGroupId(1);
            uuid.setActive(1);
            result.add(uuid);
        });
        return result;
    }

    /**
     *  处理server的uuid
     * @author NJ
     * @date 2018/9/18 17:45
     * @param prefix
     * @param list
     * @return java.util.List<com.greattimes.ev.bpm.entity.Uuid>
     */
    private List<Uuid> handServerUuids(String prefix, List<Server> list){
        List<Uuid> uuids = new ArrayList<>();
        for(Server server : list){
            Uuid uuid = new Uuid();
            uuid.setId((long)server.getId());
            uuid.setContraintGroupId(1);
            uuid.setActive(1);
            uuid.setValue(prefix+",ip:"+server.getIp()+",port:"+ server.getPort());
            uuids.add(uuid);
        }
        return uuids;
    }

    /**
     * 处理port的uuid
     * @author NJ
     * @date 2018/6/19 12:50
     * @param prefix
     * @param serverIps
     * @param serverPorts
     * @return java.util.List<com.greattimes.ev.bpm.entity.Uuid>
     */
    private List<Uuid> handPortUuids(String prefix, List<ServerIp> serverIps ,List<ServerPort> serverPorts){
        List<Uuid> result = new ArrayList<>();
        Set<String> portSet = new HashSet<>();
        String uuIdIpPrefix;
        for(ServerIp serverIp : serverIps){
            uuIdIpPrefix = serverIp.getIp();
            for(ServerPort serverPort : serverPorts){
                if(serverIp.getServerGroupId().intValue() == serverPort.getServerGroupId().intValue()){
                    portSet.add(prefix + ",ip:"+ uuIdIpPrefix + ",port:"+ serverPort.getPort());
                }
            }
        }
        for(String portStr : portSet){
            Uuid uuid = new Uuid();
            uuid.setValue(portStr);
            uuid.setContraintGroupId(1);
            uuid.setActive(1);
            result.add(uuid);
        }
        return result;
    }


    @Override
    public boolean isHasSameName(int id, int monitorId, String name) {
//        EntityWrapper<Component> ew = new EntityWrapper<Component>();
//        Component component = new Component();
//        ew.setEntity(component);
//        ew.where("name={0}",name).and("applicationId={0}",applicationId).ne("id", id);
//        List<Component> result = componentMapper.selectList(ew);
//        if(evUtil.listIsNullOrZero(result)){
//            return false;
//        }
//        return true;

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("componentId",id);
        paramMap.put("monitorId",monitorId);
        paramMap.put("name", name);
        List<Component> components = componentMapper.selectComponentByMap(paramMap);
        if(evUtil.listIsNullOrZero(components)){
            return false;
        }
        return true;
    }


    @Override
    public List<DictionaryParam> selectIpPortsByComponentId(int componentId) {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put("componentId",componentId);
        return componentMapper.selectComponentPortByComponentAndIp(param);
    }

    @Override
    public List<Map<String,Object>> selectIndicator(List<Integer> type) {
        return indicatorMapper.selectIndicatorDictionary(type);
    }

    @Override
    public List<Component> delete(List<Integer> ids) {
        if(evUtil.listIsNullOrZero(ids)){
            return new ArrayList<>();
        }
        //bpm_component
        List<Component> components = componentMapper.selectBatchIds(ids);
        componentMapper.deleteBatchIds(ids);

        //bpm_monitor_component
        EntityWrapper<MonitorComponent> monitorComponentEntityWrapper = new EntityWrapper<>();
        monitorComponentEntityWrapper.in("componentId", ids);
        monitorComponentMapper.delete(monitorComponentEntityWrapper);

        //bpm_component_dimension
        EntityWrapper<ComponentDimension> dimensionEntityWrapper = new EntityWrapper<>();
        dimensionEntityWrapper.in("componentId", ids);
        componentDimensionMapper.delete(dimensionEntityWrapper);
        //bpm_server_group
        EntityWrapper<ServerGroup> serverGroupEntityWrapper = new EntityWrapper<>();
        serverGroupEntityWrapper.in("componentId", ids);
        List<ServerGroup> serverGroups = serverGroupMapper.selectList(serverGroupEntityWrapper);
        if(!evUtil.listIsNullOrZero(serverGroups)){
            serverGroupMapper.delete(serverGroupEntityWrapper);
            List<Integer> serverGroupList = serverGroups.stream().map(ServerGroup::getId).collect(Collectors.toList());
            //bpm_server_ip
            EntityWrapper<ServerIp> serverIpWrapper = new EntityWrapper<>();
            serverIpWrapper.in("serverGroupId",serverGroupList);
            serverIpMapper.delete(serverIpWrapper);
            //bpm_server_port
            EntityWrapper<ServerPort> serverPortWrapper = new EntityWrapper<>();
            serverPortWrapper.in("serverGroupId",serverGroupList);
            serverPortMapper.delete(serverPortWrapper);
            //bpm_server
            EntityWrapper<Server> serverWrapper = new EntityWrapper<>();
            serverWrapper.in("serverGroupId",serverGroupList);
            serverMapper.delete(serverWrapper);
        }
        //bpm_field_other_name
        EntityWrapper<FieldOtherName> fieldOtherNameEntityWrapper = new EntityWrapper<>();
        fieldOtherNameEntityWrapper.in("componentId", ids);
        fieldOtherNameMapper.delete(fieldOtherNameEntityWrapper);

        //bpm_field_mapping_component
        EntityWrapper<FieldMappingComponent> fieldMappingComponentEntityWrapper = new EntityWrapper<>();
        fieldMappingComponentEntityWrapper.in("componentId",ids);
        List<FieldMappingComponent> fieldMappingComponents = fieldMappingComponentMapper.selectList(fieldMappingComponentEntityWrapper);
        if(!evUtil.listIsNullOrZero(fieldMappingComponents)){
            Set<Integer> configIds = fieldMappingComponents.stream().map(FieldMappingComponent::getConfigId).collect(Collectors.toSet());
            //根据configId查询bpm_field_mapping_component
            fieldMappingComponentEntityWrapper = new EntityWrapper<>();
            fieldMappingComponentEntityWrapper.in("configId", configIds);
            List<FieldMappingComponent> allFieldMappingComponents = fieldMappingComponentMapper.selectList(fieldMappingComponentEntityWrapper);
            //bpm_field_mapping_component
            fieldMappingComponentMapper.delete(fieldMappingComponentEntityWrapper);
            /**
                按照configId分组，value为组件id集合
                Map<Integer, Set<Integer>> configId
                主要判断配置中组件如果只包含一个则删除相应的表
                TODO: 有待优化
            */
            allFieldMappingComponents.stream().collect(Collectors.groupingBy(FieldMappingComponent::getConfigId,
                    Collectors.mapping(FieldMappingComponent::getComponentId, Collectors.toSet())))
                    .forEach((k,v) ->{
                        //删除主表
                        if(v.size() == 1){
                            //bpm_field_mapping_cnf
                            fieldMappingCnfMapper.deleteById(k);
                            //bpm_field_mapping
                            Map<String, Object> fieldMappingMap = new HashMap<>(1);
                            fieldMappingMap.put("configId", k);
                            fieldMappingMapper.deleteByMap(fieldMappingMap);
                        }
                    });
        }

        /**
         * bpm_uuid
         * delete like
         * 1、'applicationid:1,componentid:1'
         * 2、applicationid:1,componentid:1,ip:1
         * 3、applicationid:1,componentid:1,port:1
         */
        List<String> delUuidStr = new ArrayList<>();
        components.stream().collect(Collectors.groupingBy(Component::getApplicationId,
                Collectors.mapping(Component::getId, Collectors.toList()))).forEach((k,v)->{
                    v.forEach(y->{
                        delUuidStr.add("applicationid:"+k+",componentid:"+y);
                    });
        });
        delUuidStr.forEach(z->{
            EntityWrapper<Uuid> uuidEntityWrapper;
            uuidEntityWrapper = new EntityWrapper<>();
            uuidEntityWrapper.like("value",z,SqlLike.RIGHT);
            uuidMapper.delete(uuidEntityWrapper);
        });
        return components;
    }



    /**
     * 去除多余的回车
     * @author NJ
     * @date 2018/6/15 15:51
     * @param
     * @return java.lang.String
     */
    private String formatIpPortStr(String ipPortStr){
        if(StringUtils.isNotBlank(ipPortStr)){
            ipPortStr = ipPortStr.replaceAll(" ","");
            StringBuilder sb = new StringBuilder("");
            String [] array = ipPortStr.split("\n|\r");
            for(String str : array){
                if(StringUtils.isNotBlank(str)){
                    sb.append(str).append("\n");
                }
            }
            return sb.substring(0,sb.lastIndexOf("\n"));
        }
        return "";
    }

    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder("12344\n\n\n578\n98\n\n\n\n777");
        System.out.println(new ComponentServiceImpl().formatIpPortStr(sb.toString()));

        int len = sb.lastIndexOf("\n");
        System.out.println(len);
        String str1 = sb.substring(0,len);
        System.out.println(str1);



        String str = "125.0.0.1-3\n125.0.0.8";
        String[] strs = str.split("\n|\r");
        String ss = "125.0.0.1-3";
        //处理该种形式ip:192.168.0.1-80
        int lastPointIndex = ss.lastIndexOf(".");
        String lastIpNumStr = ss.substring(lastPointIndex+1);
        String fristAndThirdIpNumStr = ss.substring(0,lastPointIndex+1);
        System.out.println(lastIpNumStr);
        System.out.println(fristAndThirdIpNumStr);

        String restr = "aa bb  ttt ";
        System.out.println(restr.replaceAll(" ", ""));

    }

	@Override
	public List<DataSourcesRelationParam> selectByComponentId(Integer componentId) {
		// 获取组件下的ip
		List<DataSourcesRelationParam> ips = serverIpMapper.selectByComponentId(componentId);
		return ips;
	}

	@Override
	public List<DataSourcesRelationParam> selectIpAndport(Integer componentId) {
		Map<String, Object> col=new HashMap<>();
		col.put("componentId", componentId);
		List<DataSourcesRelationParam> all=new ArrayList<>();
		// 获取组件服务器组
		List<ServerGroup> groupids = serverGroupMapper.selectByMap(col);
		for (ServerGroup serverGroup : groupids) {
			List<DataSourcesRelationParam> li=serverIpMapper.selectIpAndport(serverGroup.getId());
			all.addAll(li);
		}
		return all;
	}

	@Override
	public List<Component> selectComponentByType(Map<String, Object> map) {
		int active=evUtil.getMapIntValue(map, "active");
		if(active==2) {
			/*Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("componentId", value)*/
			map.remove("active");
			return componentMapper.selectByMap(map);
		}
		return componentMapper.selectByMap(map);
	}

    /**
     * 插入field
     * @param protocolField
     * @return
     */
	private int insertProtocolField(ProtocolField protocolField, int protocolId){
        protocolField.setId(null);
        protocolField.setProtocolId(protocolId);
        protocolField.setActive(1);
        protocolFieldMapper.insert(protocolField);
        return protocolField.getId();
    }

	@Override
	public String getCenterIpBycomponentId(Integer componentId) {
		String ip=null;
		List<Map<String, Object>> list=componentMapper.getCenterIpBycomponentId(componentId);
		if(!evUtil.listIsNullOrZero(list)) {
			ip=list.get(0).get("ip").toString();
		}
		return ip;
	}

    @Override
    public List<ComponentChainParam> getComponentChainParamByAppId(int appId) {
        return componentMapper.findComponentChainParamByAppId(appId);
    }

    @Override
    public Map<Integer, Map<String, Object>> getComponentNameByMap(Map<String, Object> map) {
	    List<Map<String, Object>> data = componentMapper.findComponentByMap(map);
	    Map<Integer, Map<String, Object>> result = new HashMap<>();
	    if(!evUtil.listIsNullOrZero(data)){
	        for(Map<String, Object> dataMap : data){
                result.put(evUtil.getMapIntegerValue(dataMap, "componentId"), dataMap);
            }
        }
        return result;
    }

    @Deprecated
    @Override
    public List<Component> selectAiApplicationTreeByType(Map<String, Object> map) {
	    /**
         * 1 应用 2 监控点 3 组件 4 ip 5 port
         */
	    int userId = evUtil.getMapIntValue(map, "userId");
	    Integer type = evUtil.getMapIntegerValue(map, "type");
	    Map<String, Object> param = new HashMap<>();
	    param.put("userId", userId);
        List<Map<String, Object>> data = componentMapper.selectComponentTreeData(param);
        List<ApplicationTree> applicationTrees = new ArrayList<>();
        //appId
        Map<Integer, List<ApplicationTree>> appTree = new HashMap<>();
        //monitor
        Map<Integer, List<ApplicationTree>> monitorTree = new HashMap<>();
        List<Integer> componentIds = new ArrayList<>();
        Integer applicationId, componentId, monitorId;
        String applicationName, monitorName, componentName;
        for(Map<String, Object> mapData : data){

            applicationId = evUtil.getMapIntValue(mapData,"applicationId");
            componentId = evUtil.getMapIntegerValue(mapData,"componentId");
            monitorId = evUtil.getMapIntegerValue(mapData,"monitorId");
            applicationName = evUtil.getMapStrValue(mapData,"applicationName");
            monitorName = evUtil.getMapStrValue(mapData,"monitorName");
            componentName = evUtil.getMapStrValue(mapData,"componentName");

            //monitor_node
            if(monitorTree.get(applicationId) != null){
                ApplicationTree tree = new ApplicationTree();
                tree.setId(monitorId);
                tree.setParentId(applicationId);
                tree.setTitle(monitorName);
                tree.setNodeType(2);
                tree.getChildren().add(new ApplicationTree(componentName,componentId, 3,null));
                monitorTree.get(monitorId).add(tree);
            }else{
                ApplicationTree tree = new ApplicationTree();
                tree.setId(monitorId);
                tree.setParentId(applicationId);
                tree.setTitle(monitorName);
                tree.setNodeType(2);
                List<ApplicationTree> list = new ArrayList<>();
                list.add(new ApplicationTree(componentName,componentId, 3,null));
                monitorTree.put(monitorId, list);
            }
            //application_node
            if(appTree.get(applicationId) != null){
                ApplicationTree tree = new ApplicationTree();
                tree.setId(applicationId);
                tree.setParentId(-1);
                tree.setNodeType(1);
                tree.setTitle(applicationName);
                applicationTrees.add(tree);
            }
            componentIds.add(componentId);
        }


        //通过组件查询ip,port
        Map<String, Object> portParam = new HashMap<>();
        portParam.put("ids", componentIds);
        List<Map<String, Object>> portData = componentMapper.selectIpAndPortData(portParam);
        Map<Integer, List<ApplicationTree>> portTree = new HashMap<>();
        Integer portComponentId, ipId, portId;
        String ip, port;
        for(Map<String, Object> portMap : portData){
            portComponentId = evUtil.getMapIntValue(portMap,"componentId");
            ipId = evUtil.getMapIntegerValue(portMap,"ipId");
            portId = evUtil.getMapIntegerValue(portMap,"portId");
            ip = evUtil.getMapStrValue(portMap,"ipId");
            port = evUtil.getMapStrValue(portMap,"port");
            if(portTree.get(portComponentId) != null){
                ApplicationTree tree = new ApplicationTree();
                tree.setId(ipId);
                tree.setParentId(portComponentId);
                tree.setTitle(ip);
                tree.setNodeType(4);
                tree.getChildren().add(new ApplicationTree(port, portId, ipId,5, null));
            }else{
                ApplicationTree tree = new ApplicationTree();
                tree.setId(ipId);
                tree.setParentId(portComponentId);
                tree.setTitle(ip);
                tree.setNodeType(4);
                List<ApplicationTree> list = new ArrayList<>();
                list.add(new ApplicationTree(port, portId, ipId,5, null));
                tree.getChildren().add(new ApplicationTree(port, portId, ipId,5, null));
            }
        }

        //ip --> component-->monitor-->application
        /**
         * portTree, monitorTree, appTree
         */
        for(ApplicationTree t : applicationTrees){
        }
        return null;
    }


    @Override
    public Map<String, List<ApplicationTree>> selectAiApplicationTree(Map<String, Object> map) {
        /**
         * 1 应用 2 监控点 3 组件 4 ip 5 port
         */
        Map<String, List<ApplicationTree>> result = new HashMap<>();
        //app
        int userId = evUtil.getMapIntValue(map, "userId");
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("userId", userId);
        paramMap.put("active", 1);
        List<ApplicationTree> appTrees =  componentMapper.selectAppTree(paramMap);
        if(evUtil.listIsNullOrZero(appTrees)){
            return result;
        }
        result.put("applicationTree", appTrees);

        List<ApplicationTree> appTrees1 = new ArrayList<>();
        List<ApplicationTree> appTrees2 = new ArrayList<>();
        List<ApplicationTree> appTrees3 = new ArrayList<>();
        List<ApplicationTree> appTrees4 = new ArrayList<>();

        for(ApplicationTree app : appTrees){
            ApplicationTree tempTree1 = new ApplicationTree();
            ApplicationTree tempTree2 = new ApplicationTree();
            ApplicationTree tempTree3 = new ApplicationTree();
            ApplicationTree tempTree4 = new ApplicationTree();
            BeanUtils.copyProperties(app,tempTree1);
            BeanUtils.copyProperties(app,tempTree2);
            BeanUtils.copyProperties(app,tempTree3);
            BeanUtils.copyProperties(app,tempTree4);
            tempTree1.setChildren(new ArrayList<>());
            tempTree2.setChildren(new ArrayList<>());
            tempTree3.setChildren(new ArrayList<>());
            tempTree4.setChildren(new ArrayList<>());
            appTrees1.add(tempTree1);
            appTrees2.add(tempTree2);
            appTrees3.add(tempTree3);
            appTrees4.add(tempTree4);
        }

        List<ApplicationTree> tree1 = new ArrayList<>();
        List<ApplicationTree> tree2 = new ArrayList<>();
        List<ApplicationTree> tree3 = new ArrayList<>();

        List<ApplicationTree> monitorTrees = componentMapper.selectMonitorTree(appTrees1);

        result.put("monitorTree", appTrees1);

        if(evUtil.listIsNullOrZero(monitorTrees)){
            return result;
        }
        appTrees1.forEach(x->buildTree(x,monitorTrees));
        for(ApplicationTree app : monitorTrees){
            ApplicationTree tempTree1 = new ApplicationTree();
            ApplicationTree tempTree2 = new ApplicationTree();
            ApplicationTree tempTree3 = new ApplicationTree();
            BeanUtils.copyProperties(app,tempTree1);
            BeanUtils.copyProperties(app,tempTree2);
            BeanUtils.copyProperties(app,tempTree3);
            tempTree1.setChildren(new ArrayList<>());
            tempTree2.setChildren(new ArrayList<>());
            tempTree3.setChildren(new ArrayList<>());
            tree1.add(tempTree1);
            tree2.add(tempTree2);
            tree3.add(tempTree3);
        }

        //componentTrees
        List<ApplicationTree> componentTrees = componentMapper.selectComponentTree(monitorTrees);
        if(evUtil.listIsNullOrZero(componentTrees)){
            return result;
        }
        for(ApplicationTree app : componentTrees){
            ApplicationTree tempTree1 = new ApplicationTree();
            ApplicationTree tempTree2 = new ApplicationTree();
            ApplicationTree tempTree3 = new ApplicationTree();
            BeanUtils.copyProperties(app,tempTree1);
            BeanUtils.copyProperties(app,tempTree2);
            BeanUtils.copyProperties(app,tempTree3);
            tempTree1.setChildren(new ArrayList<>());
            tempTree2.setChildren(new ArrayList<>());
            tempTree3.setChildren(new ArrayList<>());
            tree1.add(tempTree1);
            tree2.add(tempTree2);
            tree3.add(tempTree3);
        }
        appTrees2.forEach(x->buildTree(x,tree1));
        result.put("componentTree", appTrees2);


        //ipTrees
        List<ApplicationTree> ipTrees = componentMapper.selectIpTree(componentTrees);
        if(evUtil.listIsNullOrZero(ipTrees)){
            return result;
        }
        for(ApplicationTree app : ipTrees){
            ApplicationTree tempTree1 = new ApplicationTree();
            ApplicationTree tempTree2 = new ApplicationTree();
            BeanUtils.copyProperties(app,tempTree1);
            BeanUtils.copyProperties(app,tempTree2);
            tempTree1.setChildren(new ArrayList<>());
            tempTree2.setChildren(new ArrayList<>());
            tree2.add(tempTree1);
            tree3.add(tempTree2);
        }
        appTrees3.forEach(x->buildTree(x,tree2));
        result.put("ipTree", appTrees3);

        //portTrees
        List<ApplicationTree> portTrees = componentMapper.selectPortTree(ipTrees);
        if(evUtil.listIsNullOrZero(portTrees)){
            return result;
        }
        for(ApplicationTree app : portTrees){
            ApplicationTree tempTree1 = new ApplicationTree();
            BeanUtils.copyProperties(app,tempTree1);
            tempTree1.setChildren(new ArrayList<>());
            tree3.add(tempTree1);
        }
        appTrees4.forEach(x->buildTree(x,tree3));
        result.put("portTree", appTrees4);

	    return result;
    }

    @Override
    public Map<Integer, String> selectConfigName(Map<String, Object> map) {
	    Map<Integer, String> result = new HashMap<>();
	    //application-monitor-component-custom
        List<Map<String, Object>> componentData =  componentMapper.selectComponentTreeData(map);
        int applicationId, monitorId, componentId, customId;
        HashSet<Integer> componentIdSet = new HashSet<>();
//        String applicationName, monitorName, componentName,customName;
        for(Map<String, Object> dMap : componentData){
            applicationId = evUtil.getMapIntValue(dMap,"applicationId");
            monitorId = evUtil.getMapIntValue(dMap,"monitorId");
            componentId = evUtil.getMapIntValue(dMap,"componentId");
            customId = evUtil.getMapIntValue(dMap,"customId");
            if(result.get(applicationId) == null){
                result.put(applicationId, evUtil.getMapStrValue(dMap, "applicationName"));
            }
            if(result.get(monitorId) == null){
                result.put(monitorId, evUtil.getMapStrValue(dMap, "monitorName"));
            }
            if(result.get(componentId) == null){
                result.put(componentId, evUtil.getMapStrValue(dMap, "componentName"));
            }
            if(result.get(customId) == null){
                result.put(customId, evUtil.getMapStrValue(dMap, "customName"));
            }
            componentIdSet.add(componentId);
        }

        //ip-port
        if(!componentIdSet.isEmpty()){
            Map<String, Object> portParam = new HashMap<>();
            portParam.put("ids", componentIdSet);
            List<Map<String, Object>> portData = componentMapper.selectIpAndPortData(portParam);
            int ipId, portId;
            for(Map<String, Object> portMap : portData){
                ipId = evUtil.getMapIntValue(portMap,"ipId");
                portId = evUtil.getMapIntValue(portMap,"portId");
                if(result.get(ipId) == null){
                    result.put(ipId, evUtil.getMapStrValue(portMap, "ip"));
                }
                if(result.get(portId) == null){
                    result.put(portId, evUtil.getMapStrValue(portMap, "port"));
                }
            }
        }
        return result;
    }

    /**
     * 递归构造树形结构
     * @param fatherTreeNode
     * @param treeList
     */
    public void buildTree(ApplicationTree fatherTreeNode, List<ApplicationTree> treeList){
        for(ApplicationTree tree : treeList){
            if(tree.getParentId().equals(fatherTreeNode.getId())){
                List<ApplicationTree> children = new ArrayList<>();
                if(fatherTreeNode.getChildren() != null){
                    children = fatherTreeNode.getChildren();
                }
                children.add(tree);
                fatherTreeNode.setChildren(children);
                buildTree(tree, treeList);
            }
        }
    }

    @Override
    public List<ServerIp> selectIpByIpStr(List<String> ip) {
        return serverIpMapper.selectIpByIpStr(ip);
    }

    @Override
    public List<IpPortParam> selectIpPortParamByPortStr(List<String> portStr) {
        return serverIpMapper.selectIpPortParam(portStr);
    }
}
