package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.DataGroupInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-11-15
 */
public interface DataGroupInfoMapper extends BaseMapper<DataGroupInfo> {

}
