package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.DsUuid;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组件指标约束UUID表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-20
 */
public interface DsUuidMapper extends BaseMapper<DsUuid> {

}
