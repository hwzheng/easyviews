package com.greattimes.ev.bpm.service.config.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.entity.PcapTask;
import com.greattimes.ev.bpm.mapper.PcapTaskMapper;
import com.greattimes.ev.bpm.mapper.ServerMapper;
import com.greattimes.ev.bpm.service.config.IPcapTaskService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * pca下载任务 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-11-07
 */
@Service
public class PcapTaskServiceImpl extends ServiceImpl<PcapTaskMapper, PcapTask> implements IPcapTaskService {
	@Autowired
	private PcapTaskMapper pcapTaskMapper;
	@Autowired
	private ServerMapper serverMapper;

	@Override
	public Page<Map<String, Object>> selectPcapTaskByMap(Page<Map<String, Object>> page, Map<String, Object> map) {
        page.setRecords(pcapTaskMapper.selectPcapTaskByMap(page,map));
        return page;
    }

	@Override
	public boolean checkIp(String ip,Integer centerId,String port) {
		List<Map<String, Object>> count=new ArrayList<>();
		if(null==port) {
			count=serverMapper.getIpCount(ip,centerId);
		}else {
			count=serverMapper.getPortCount(ip,centerId,port);
		}
		int num=0;
		if(!evUtil.listIsNullOrZero(count)) {
			num=Integer.parseInt(count.get(0).get("count").toString());
		}
		if (num>0) {
			return true;
		}else {
			return false;
		}
	}

	@Override
	public int insertTask(Integer userId, JSONObject jsonObject) {
		PcapTask pcapTask=new PcapTask();
		Date start=new Date();
		start.setTime(jsonObject.getLongValue("startTime"));
		pcapTask.setStartTime(start);
		Date end=new Date();
		end.setTime(jsonObject.getLongValue("endTime"));
		pcapTask.setEndTime(end);
		pcapTask.setDip(jsonObject.getString("dip"));
		pcapTask.setSip(jsonObject.getString("sip"));
		pcapTask.setPort(jsonObject.getInteger("port"));
		pcapTask.setPortType(jsonObject.getInteger("portType"));
		pcapTask.setDownType(jsonObject.getInteger("downType"));
		pcapTask.setCreateBy(userId);
		pcapTask.setCreateTime(new Date());
		pcapTask.setState(0);
		pcapTask.setCenterId(jsonObject.getInteger("centerId"));
		pcapTaskMapper.insert(pcapTask);
		return pcapTask.getId();
	}


}
