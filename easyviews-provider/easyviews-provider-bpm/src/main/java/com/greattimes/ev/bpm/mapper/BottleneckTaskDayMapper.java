package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskDay;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表日期表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-10-25
 */
public interface BottleneckTaskDayMapper extends BaseMapper<BottleneckTaskDay> {

}
