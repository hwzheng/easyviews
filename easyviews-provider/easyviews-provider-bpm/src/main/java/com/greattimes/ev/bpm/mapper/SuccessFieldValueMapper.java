package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.SuccessFieldValue;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 自定义成功字段值 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface SuccessFieldValueMapper extends BaseMapper<SuccessFieldValue> {

}
