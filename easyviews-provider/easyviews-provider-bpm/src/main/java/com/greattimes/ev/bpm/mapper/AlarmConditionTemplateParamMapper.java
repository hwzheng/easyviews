package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmConditionTemplateParam;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警模板参数表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-05
 */
public interface AlarmConditionTemplateParamMapper extends BaseMapper<AlarmConditionTemplateParam> {

}
