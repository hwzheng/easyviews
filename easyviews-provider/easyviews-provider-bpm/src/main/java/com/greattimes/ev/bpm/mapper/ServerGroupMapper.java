package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ServerGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 组件服务器组 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface ServerGroupMapper extends BaseMapper<ServerGroup> {
    void batchInsert(List<ServerGroup> list);
}
