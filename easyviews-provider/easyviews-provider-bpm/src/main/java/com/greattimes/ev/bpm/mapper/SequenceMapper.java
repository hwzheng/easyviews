package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Sequence;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-07
 */
public interface SequenceMapper extends BaseMapper<Sequence> {
    int nextVal(Map<String,Object> map);
}
