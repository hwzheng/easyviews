package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.DsUuidMapping;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * UUID映射表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-20
 */
public interface DsUuidMappingMapper extends BaseMapper<DsUuidMapping> {

}
