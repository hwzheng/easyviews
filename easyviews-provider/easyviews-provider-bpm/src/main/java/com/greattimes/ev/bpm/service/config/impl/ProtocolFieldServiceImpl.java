package com.greattimes.ev.bpm.service.config.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.mapper.ProtocolFieldMapper;
import com.greattimes.ev.bpm.service.config.IProtocolFieldService;

/**
 * <p>
 * 协议字段表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-10-22
 */
@Service
public class ProtocolFieldServiceImpl extends ServiceImpl<ProtocolFieldMapper, ProtocolField> implements IProtocolFieldService {

}
