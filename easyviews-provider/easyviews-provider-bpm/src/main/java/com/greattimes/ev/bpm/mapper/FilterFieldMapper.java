package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.custom.param.resp.CustomFieldValueParam;
import com.greattimes.ev.bpm.entity.FilterField;

/**
 * <p>
 * 自定义数据过滤字段配置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface FilterFieldMapper extends BaseMapper<FilterField> {

	/**
	 * 过滤条件查询
	 * @param customId
	 * @return
	 */
	List<CustomFieldValueParam> selectFilter(int customId);

	/**
	 * 成功条件查询
	 * @param customId
	 * @return
	 */
	List<CustomFieldValueParam> selectSuccess(int customId);

}
