package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Protocol;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.alibaba.dubbo.config.support.Parameter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 协议表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
public interface ProtocolMapper extends BaseMapper<Protocol> {

	/**
	 * 查询应用协议
	 * @param map 
	 * @return
	 */
	List<Map<String, Object>> selectAppProtocol(Map<String, Object> map);

	void insertProtocol(Protocol dp);

}
