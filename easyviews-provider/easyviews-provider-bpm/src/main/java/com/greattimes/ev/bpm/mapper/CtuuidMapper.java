package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.Ctuuid;

/**
 * <p>
 * 自定义约束UUID表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-09
 */
public interface CtuuidMapper extends BaseMapper<Ctuuid> {

	/**
	 * 保存uuid
	 * @param subList
	 */
	void batchInsert(List<Ctuuid> subList);

	List<Ctuuid> findUuidByVaule(String regStr);

}
