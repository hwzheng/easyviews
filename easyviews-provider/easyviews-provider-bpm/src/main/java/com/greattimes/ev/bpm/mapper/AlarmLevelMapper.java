package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmLevel;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警级别表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
public interface AlarmLevelMapper extends BaseMapper<AlarmLevel> {

	void batchInsert(@Param("list")List<Integer> level,@Param("alarmGroupId") int alarmGroupId);

}
