package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ProtocolPacket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 协议报文分组 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-21
 */
public interface ProtocolPacketMapper extends BaseMapper<ProtocolPacket> {
    void saveProtocolPacket(ProtocolPacket protocolPacket);
}
