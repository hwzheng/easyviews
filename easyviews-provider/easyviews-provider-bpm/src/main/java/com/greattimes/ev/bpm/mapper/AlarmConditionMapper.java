package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmCondition;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警条件模板表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
public interface AlarmConditionMapper extends BaseMapper<AlarmCondition> {

}
