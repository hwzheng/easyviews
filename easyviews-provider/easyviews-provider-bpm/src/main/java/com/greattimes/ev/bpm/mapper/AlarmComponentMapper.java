package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.AlarmComponent;

/**
 * <p>
 * 告警组件表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
public interface AlarmComponentMapper extends BaseMapper<AlarmComponent> {

}
