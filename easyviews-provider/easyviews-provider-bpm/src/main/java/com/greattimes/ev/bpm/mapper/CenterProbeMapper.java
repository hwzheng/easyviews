package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.resp.CenterProbeParam;
import com.greattimes.ev.bpm.entity.CenterProbe;

/**
 * <p>
 * 中心探针表 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-22
 */
public interface CenterProbeMapper extends BaseMapper<CenterProbe> {

	List<CenterProbeParam> selectProbe();

	int getMaxPortCode();


}
