package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.custom.param.resp.CustomFieldParam;
import com.greattimes.ev.bpm.entity.AnalyzeDimension;

/**
 * <p>
 * 自定义分析维度 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface AnalyzeDimensionMapper extends BaseMapper<AnalyzeDimension> {

	/**
	 * 维度信息列表查询
	 * @param customId
	 * @return
	 */
	List<CustomFieldParam> selectDimension(int customId);




}
