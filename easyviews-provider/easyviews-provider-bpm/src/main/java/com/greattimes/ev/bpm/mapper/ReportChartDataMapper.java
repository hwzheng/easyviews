package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartData;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表图表数据表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartDataMapper extends BaseMapper<ReportChartData> {

    /**
     * batch save
     * @param list
     */
    void batchInsert(List<ReportChartData> list);
}
