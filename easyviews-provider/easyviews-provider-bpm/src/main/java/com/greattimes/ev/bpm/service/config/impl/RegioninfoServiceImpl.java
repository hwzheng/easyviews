package com.greattimes.ev.bpm.service.config.impl;

import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.entity.Regioninfo;
import com.greattimes.ev.bpm.mapper.RegioninfoMapper;
import com.greattimes.ev.bpm.service.config.IRegioninfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-07-02
 */
@Service
public class RegioninfoServiceImpl extends ServiceImpl<RegioninfoMapper, Regioninfo> implements IRegioninfoService {
	@Autowired
	private RegioninfoMapper regioninfoMapper;
	@Override
	public List<DictionaryParam> selectProvince() {
		return regioninfoMapper.selectProvince();
	}
	@Override
	public List<DictionaryParam> selectCity(int provinceId) {
		return regioninfoMapper.selectCity(provinceId);
	}
	@Override
	public List<Regioninfo> getCity(Integer province, Integer cityId) {
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("provinceId", province);
		columnMap.put("cityId", cityId);
		return regioninfoMapper.selectByMap(columnMap);
	}

}
