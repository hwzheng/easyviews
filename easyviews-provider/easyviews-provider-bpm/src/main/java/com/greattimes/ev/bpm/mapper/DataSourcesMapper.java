package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.DataSources;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 数据源配置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface DataSourcesMapper extends BaseMapper<DataSources> {

    List<Map<String, Object>> findList();

}
