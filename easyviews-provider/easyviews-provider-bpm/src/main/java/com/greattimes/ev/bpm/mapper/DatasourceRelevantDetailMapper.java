package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.DatasourceRelevantDetail;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 数据源关联详情表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
public interface DatasourceRelevantDetailMapper extends BaseMapper<DatasourceRelevantDetail> {

	/**
	 * 批量插入
	 * @param details
	 */
	void insertRelevantDetail(List<DatasourceRelevantDetail> details);


}
