package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartDataDimension;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表数据维度表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartDataDimensionMapper extends BaseMapper<ReportChartDataDimension> {
    /**
     * batch save
     * @param list
     */
    void batchInsert(List<ReportChartDataDimension> list);
}
