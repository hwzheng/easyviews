package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.MonitorComponent;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 监控点-组件映射表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-07
 */
public interface MonitorComponentMapper extends BaseMapper<MonitorComponent> {

}
