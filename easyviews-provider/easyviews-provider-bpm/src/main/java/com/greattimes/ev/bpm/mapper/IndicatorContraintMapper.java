package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.IndicatorContraint;

/**
 * <p>
 * 数据源指标约束 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-17
 */
public interface IndicatorContraintMapper extends BaseMapper<IndicatorContraint> {

	void batchInsert(List<IndicatorContraint> list);
}
