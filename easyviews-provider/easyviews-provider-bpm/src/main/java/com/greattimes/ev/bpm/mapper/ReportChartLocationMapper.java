package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.analysis.param.resp.ReportChartLocationParam;
import com.greattimes.ev.bpm.entity.ReportChartLocation;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表图表位置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-27
 */
public interface ReportChartLocationMapper extends BaseMapper<ReportChartLocation> {

	void batchUpdate(@Param("list")List<ReportChartLocationParam> chart);

}
