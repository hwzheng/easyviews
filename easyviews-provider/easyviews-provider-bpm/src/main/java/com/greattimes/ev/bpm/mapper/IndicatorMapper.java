package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Indicator;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.bpm.entity.ProtocolField;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-15
 */
public interface IndicatorMapper extends BaseMapper<Indicator> {
    List<Map<String,Object>> selectIndicatorDictionary(@Param("list") List<Integer> list);
    List<Indicator> selectIndicator(@Param("list") List<Integer> list);
    List<Indicator> selectIndicatorByIds(@Param("ids") List<Integer> ids);
    List<ProtocolField> selectProtocolFiledByIds(@Param("ids")List<Integer> ids);

}
