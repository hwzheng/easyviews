package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.UserApplication;

/**
 * <p>
 * 用户应用表 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-18
 */
public interface UserApplicationMapper extends BaseMapper<UserApplication> {

	/**
	 * 批量插入用户应用关系
	 * 
	 * @param list
	 */
	void insertBatch(List<UserApplication> list);

}
