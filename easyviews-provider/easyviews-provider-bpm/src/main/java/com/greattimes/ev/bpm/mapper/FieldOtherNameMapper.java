package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.FieldOtherName;

/**
 * <p>
 * 别名表 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-24
 */
public interface FieldOtherNameMapper extends BaseMapper<FieldOtherName> {

}
