package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.SuccessField;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 自定义成功判断字段 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface SuccessFieldMapper extends BaseMapper<SuccessField> {

}
