package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.AlarmTime;

/**
 * <p>
 * 告警时间表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
public interface AlarmTimeMapper extends BaseMapper<AlarmTime> {

}
