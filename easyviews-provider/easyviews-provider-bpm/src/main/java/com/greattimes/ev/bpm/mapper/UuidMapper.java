package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Uuid;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 组件指标约束UUID表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-30
 */
public interface UuidMapper extends BaseMapper<Uuid> {
    void batchInsert(List<Uuid> list);
    void batchDelete(List<Uuid>  list);
    List<Uuid> findUuidByVaule(String regStr);
    void batchDeleteByIds(List<Integer> list);
}
