package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartIndicatorFilter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表图表指标过滤条件 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2019-05-22
 */
public interface ReportChartIndicatorFilterMapper extends BaseMapper<ReportChartIndicatorFilter> {

    void batchInsert(List<ReportChartIndicatorFilter> list);

}
