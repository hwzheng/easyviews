package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmMonitor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警监控点表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-09
 */
public interface AlarmMonitorMapper extends BaseMapper<AlarmMonitor> {

}
