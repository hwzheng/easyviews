package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.bpm.entity.AlarmInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-20
 */
public interface AlarmInfoMapper extends BaseMapper<AlarmInfo> {
    @Deprecated
    List<AlarmInfo> selectPageByIdAndType(Page<AlarmInfo> page, Map<String, Object> param);

    List<Map<String, Object>> selectOtherAlarmAmount(Map<String, Object> map);

    List<Map<String, Object>> selectOtherAlarmAmountByDimensionValue(Map<String, Object> map);

    List<AlarmInfo> selectAlarmInfoByParam(Map<String, Object> param);

    List<AlarmInfo> selectAlarmInfoByPage(Page<AlarmInfo> page,Map<String, Object> param);

    List<AlarmInfo> selectAlarmInfoByIds(Map<String, Object> map);

    List<AlarmInfo>  selectAlarmInfoAndDimensionById(int id);

    void updateBatchAlarmInfoOpinionByMap(Map<String, Object> param );

    List<AlarmInfo> selectAlarmAppIdByTime(Map<String, Object> param);

    List<Map<String, Object>> selectAlarmInfoByTime(Map<String, Object> param);

    /**
     * 发送类型,达到压缩间隔,查询产生的告警信息
     * @param param
     * @return
     */
    List<Map<String, Object>> selectAlarmInfoByCompressTime(Map<String, Object> param);

    /**
     * 根据不同的颗粒度统计单组件和单事件的告警数量
     * @param param
     * @return
     */
    List<Map<String, Object>> selectAlarmNumByInterval(Map<String, Object> param);

}
