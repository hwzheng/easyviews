package com.greattimes.ev.bpm.service.config.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.enums.SqlLike;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.toolkit.MapUtils;
import com.greattimes.ev.bpm.decode.param.req.JExtend;
import com.greattimes.ev.bpm.decode.param.req.JField;
import com.greattimes.ev.bpm.decode.param.req.JParseGroup;
import com.greattimes.ev.bpm.decode.param.req.JProtocolDetail;
import com.greattimes.ev.bpm.decode.param.req.JVariable;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.Dimension;
import com.greattimes.ev.bpm.entity.Protocol;
import com.greattimes.ev.bpm.entity.ProtocolDimension;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.entity.ProtocolMessage;
import com.greattimes.ev.bpm.entity.ProtocolPacket;
import com.greattimes.ev.bpm.mapper.ComponentMapper;
import com.greattimes.ev.bpm.mapper.DictionaryMapper;
import com.greattimes.ev.bpm.mapper.DimensionMapper;
import com.greattimes.ev.bpm.mapper.ProtocolDimensionMapper;
import com.greattimes.ev.bpm.mapper.ProtocolFieldMapper;
import com.greattimes.ev.bpm.mapper.ProtocolMapper;
import com.greattimes.ev.bpm.mapper.ProtocolMessageMapper;
import com.greattimes.ev.bpm.mapper.ProtocolPacketMapper;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.common.ISequenceService;
import com.greattimes.ev.bpm.service.config.IProtocolService;
import com.greattimes.ev.common.utils.ProtocolUtil;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Dictionary;

/**
 * <p>
 * 协议表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@Service
public class ProtocolServiceImpl extends ServiceImpl<ProtocolMapper, Protocol> implements IProtocolService {
	@Autowired
	private ProtocolMapper protocolMapper;
	@Autowired
	private ComponentMapper componentMapper;
	@Autowired
	private ProtocolFieldMapper protocolFieldMapper;
	@Autowired
	private ProtocolMessageMapper protocolMessageMapper;
	@Autowired
	private ProtocolPacketMapper protocolPacketMapper;
	@Autowired
	private ProtocolDimensionMapper protocolDimensionMapper;
	@Autowired
	private DimensionMapper dimensionMapper;
	@Autowired
	private DictionaryMapper dictionaryMapper;
	@Autowired
	private IDeleteRuleService deleteRuleService;
	@Autowired
    private ISequenceService sequenceService;

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public List<Protocol> selectProtocol() {
		List<Protocol> list = protocolMapper.selectList(null);
		return list;
	}

	@Override
	public void deleteProtocol(Map<String, Object> map) {
		Integer id = evUtil.getMapIntValue(map, "id");
		protocolMapper.deleteById(id);
	}

	@Override
	public List<Map<String, Object>> selectProtocolField(int id, int type,int flag) {
		List<Map<String, Object>> list=new ArrayList<>();
		int protocolId = -1;
		List<ProtocolField> protocolField=new ArrayList<>();
		Map<String, Object> columnMap = new HashMap<>();
		switch (type) {
		case 3:
			List<Component> comp = componentMapper.getPrototcolId(id);
			if (!evUtil.listIsNullOrZero(comp)) {
				protocolId = comp.get(0).getPrototcolId();
				columnMap.put("protocolId", protocolId);
				columnMap.put("flag", flag);
				columnMap.put("customId", id);
				protocolField = protocolFieldMapper.selectFieldWithCustomOtherName(columnMap);
			}
			break;
		case 2:
			Component com = componentMapper.selectById(id);
			protocolId = com.getPrototcolId();
			columnMap.put("protocolId", protocolId);
			columnMap.put("flag", flag);
			protocolField =protocolFieldMapper.selectFieldWithOtherName(columnMap);
			break;
		default:
			protocolId=id;
			EntityWrapper<ProtocolField> wrapper=new EntityWrapper<>();
			wrapper.where("protocolId={0}", protocolId).and("active={0}", 1);
			if(flag==1) {
				wrapper.lt("flag", 1);
			}
			//List<ProtocolField> protocolField = protocolFieldMapper.selectByMap(columnMap);
			protocolField =protocolFieldMapper.selectList(wrapper);
			break;
		}
		for (ProtocolField protocolField2 : protocolField) {
			Map<String, Object> map=new HashMap<>();
			map.put("label", protocolField2.getName());
			map.put("value", protocolField2.getId());
			map.put("key", protocolField2.getEname());
			map.put("type", protocolField2.getType());
			map.put("otherName", protocolField2.getOtherName());
			list.add(map);
		}
		return list;
	}

	@Override
	public Page<Protocol> queryCustomProtocol(Page<Protocol> page, String name) {
		EntityWrapper<Protocol> ew = new EntityWrapper<>();
		ew.ge("state", 0);
		if (StringUtils.isNotBlank(name)) {
			ew.like("name", name, SqlLike.DEFAULT);
		}
		RowBounds rowBounds = new RowBounds(page.getOffset(), page.getLimit());
		List<Protocol> list = protocolMapper.selectPage(rowBounds, ew);
		page.setRecords(list);
		return page;
	}

	@Override
	public String updateProtocolState(Protocol protocol, String url) {
		int state=protocol.getState();
		if(state==-1) {
			List<Integer> ids=new ArrayList<>();
			ids.add(protocol.getId());
			deleteProtocolPhysical(ids);
		}else {
			protocolMapper.updateById(protocol);
		}
		// 通知解码
		/*Map<String, Object> param = new HashMap<String, Object>();
		param.put("protocolId", protocol.getId());
		param.put("state", protocol.getState());

		Map<String, Object> resultMap = HttpClientUtil.postsWithMapReturn(url, JSON.toJSONString(param));
		String statusCode = evUtil.getMapStrValue(resultMap, "statusCode");
		String body = evUtil.getMapStrValue(resultMap, "body");
		if (!"200".equals(statusCode)) {
			log.error("协议状态更新请求解码接口发生异常：" + body);
			throw new RuntimeException(body);
		}
		JSONObject jb = JSON.parseObject(body);
		String retCode = jb.get("retCode").toString();
		String retMessage = "";
		if ("000000".equals(retCode)) {
			retMessage += "状态修改成功！";
		} else if ("100011".equals(retCode) || "100012".equals(retCode) || "100022".equals(retCode)
				|| "100023".equals(retCode)) {
			retMessage += jb.get("retMessage").toString();
		} else {
			String errMessage = jb.get("retMessage").toString();
			log.error("retMessage" + errMessage);
			throw new RuntimeException(errMessage);
		}*/
		return "协议更新成功！";
	}

	@Override
	public List<Map<String, Object>> queryProtocolMessage(Integer protocolId) {
		EntityWrapper<ProtocolMessage> ew = new EntityWrapper<>();
		ew.where("protocolId={0}", protocolId);
		ew.orderBy("id", true);
		List<ProtocolMessage> list = protocolMessageMapper.selectList(ew);
		List<Map<String, Object>> result = new ArrayList<>();
		list.stream().forEach(x -> {
			Map<String, Object> dataMap = new HashMap<>();
			dataMap.put("id", x.getId());
			dataMap.put("message", new String(x.getMessage()));
			dataMap.put("protocolId", x.getProtocolId());
			result.add(dataMap);
		});
		return result;
	}

	@Override
	public List<Map<String, Object>> queryProtocolPacketMessage(int protocolId) {
		EntityWrapper<ProtocolPacket> ew = new EntityWrapper<>();
		ew.where("protocolId={0}", protocolId);
		ew.orderBy("id", true);
		List<ProtocolPacket> list = protocolPacketMapper.selectList(ew);
		List<Map<String, Object>> result = new ArrayList<>();
		list.stream().forEach(y -> {
			Map<String, Object> dataMap = new HashMap<>();
			dataMap.put("id", y.getId());
			dataMap.put("message", new String(y.getMessage()));
			dataMap.put("protocolId", y.getProtocolId());
			result.add(dataMap);
		});
		return result;
	}

	@Override
	public String saveOrUpdateProtocolDetail(String url, String username, JProtocolDetail jProtocolDetail,int flag)
			throws Exception {
		String msg = "";
		Integer protocolId = jProtocolDetail.getProtocolId();
		if (flag== 1) {
			// 新增保存协议
			Protocol dp = new Protocol();
			dp.setId(protocolId);
			dp.setName(jProtocolDetail.getDesc());
			dp.setKey(jProtocolDetail.getProtocolKey());
			dp.setType(ProtocolUtil.getProtocolTypeByProtocolCode(jProtocolDetail.getProtocolCode()));//所属协议
			dp.setMessageFormat(ProtocolUtil.getMessageFormatByMessageType(jProtocolDetail.getMessageType()));//报文格式
			dp.setDesc(jProtocolDetail.getDesc());
			dp.setState(1);
			dp.setBuiltin((jProtocolDetail.getIsBuiltin() == true) ? 1 : 0);
			dp.setProtocolMode(jProtocolDetail.getTransportMode());
			dp.setCreateBy(username);
			dp.setCreateDate(new Date());
			dp.setUpdateBy(username);
			dp.setUpdateDate(new Date());
			dp.setContent(JSON.toJSONString(jProtocolDetail).getBytes());
			dp.setFlag(1);
			dp.setAnalyzeflag(1);
			protocolMapper.insertProtocol(dp);
			protocolId = dp.getId();
			List<JParseGroup> parseGroup = jProtocolDetail.getParseGroup();
			//默认字段
			List<ProtocolField> fieldList=getDeauftField(protocolId);
			// 保存变量列表
			fieldList.addAll(getVariable(jProtocolDetail, protocolId));
			// 保存字段列表
			if (!evUtil.listIsNullOrZero(parseGroup)) {
				fieldList.addAll(getField(jProtocolDetail, protocolId, parseGroup));
			}
			Set<ProtocolField> set = new HashSet<>();
			fieldList.stream().forEach(x -> {
				set.add(x);
			});
			protocolFieldMapper.batchInsert(set);
			// 保存外层message信息
			ProtocolMessage pmc = new ProtocolMessage();
			pmc.setProtocolId(protocolId);
			// 请求分组一定存在
			// 注意此处为blob类型
			pmc.setMessage(jProtocolDetail.getClientSendBytes().getBytes());
			protocolMessageMapper.saveProtocolMessage(pmc);
			jProtocolDetail.setClientSendBytes(null);
			// 响应分组
			if (StringUtils.isNotBlank(jProtocolDetail.getServerSendBytes())) {
				ProtocolMessage pms = new ProtocolMessage();
				pms.setProtocolId(protocolId);
				pms.setMessage(jProtocolDetail.getServerSendBytes().getBytes());
				protocolMessageMapper.saveProtocolMessage(pms);
				jProtocolDetail.setServerSendBytes(null);
			}
			for (JParseGroup jpg : parseGroup) {
				ProtocolPacket pgm = new ProtocolPacket();
				pgm.setProtocolId(protocolId);
				pgm.setMessage(jpg.getMessage().getBytes());
				protocolPacketMapper.saveProtocolPacket(pgm);
				// why set null?
				jpg.setMessage(null);
			}
			msg += "协议添加成功！";
		} else {
			// 修改保存协议
			Protocol dp = new Protocol();
			dp.setId(jProtocolDetail.getProtocolId());
			dp.setName(jProtocolDetail.getDesc());
			dp.setKey(jProtocolDetail.getProtocolKey());
			dp.setType(ProtocolUtil.getProtocolTypeByProtocolCode(jProtocolDetail.getProtocolCode()));
			dp.setMessageFormat(ProtocolUtil.getMessageFormatByMessageType(jProtocolDetail.getMessageType()));
			dp.setDesc(jProtocolDetail.getDesc());
			//dp.setState(1);
			dp.setBuiltin((jProtocolDetail.getIsBuiltin() == true) ? 1 : 0);
			dp.setProtocolMode(jProtocolDetail.getTransportMode());
			dp.setUpdateBy(username);
			dp.setUpdateDate(new Date());
			dp.setContent(JSON.toJSONString(jProtocolDetail).getBytes());
			protocolMapper.updateById(dp);
			List<JParseGroup> parseGroup = jProtocolDetail.getParseGroup();
			ProtocolField entity=new ProtocolField();
			entity.setActive(0);
			EntityWrapper<ProtocolField> wrapper=new EntityWrapper<>();
			wrapper.where("protocolId={0}", protocolId);
			protocolFieldMapper.update(entity, wrapper);
			//删除bpm_protocol_dimension
			EntityWrapper<ProtocolDimension> wrap=new EntityWrapper<>();
			wrap.where("protocolId={0}", protocolId);
			protocolDimensionMapper.delete(wrap);
			//默认字段
			List<ProtocolField> fieldList=getDeauftField(protocolId);
			// 保存变量列表
			fieldList.addAll(getVariable(jProtocolDetail, protocolId));
			// 保存字段列表
			if (!evUtil.listIsNullOrZero(parseGroup)) {
				fieldList.addAll(getField(jProtocolDetail, protocolId, parseGroup));
			}
			Set<ProtocolField> set = new HashSet<>();
			fieldList.stream().forEach(x -> {
				set.add(x);
			});
			//判断数据库是否有协议字段
			Set<ProtocolField> insertSet = new HashSet<>();
			for (ProtocolField protocolField : set) {
				Map<String, Object> columnMap=new HashMap<>();
				columnMap.put("protocolId", protocolField.getProtocolId());
				columnMap.put("name", protocolField.getName());
				columnMap.put("type", protocolField.getType());
				columnMap.put("ename", protocolField.getEname());
				columnMap.put("flag", protocolField.getFlag());
				List<ProtocolField> p=protocolFieldMapper.selectByMap(columnMap);
				if(!evUtil.listIsNullOrZero(p)) {
					List<Integer> ids = p.stream().map(ProtocolField::getId).collect(Collectors.toList());
					protocolFieldMapper.batchUpadteActive(ids);
				}else {
					insertSet.add(protocolField);
				}
			}
			if(null!=insertSet&&insertSet.size()>0) {
				protocolFieldMapper.batchInsert(insertSet);
			}
			//查询该协议下不可用字段
			Map<String, Object> col=new HashMap<>(2);
			col.put("protocolId", protocolId);
			col.put("active", 0);
			List<ProtocolField> pro=protocolFieldMapper.selectByMap(col);
			if(!evUtil.listIsNullOrZero(pro)) {
				List<Integer> ids = pro.stream().map(ProtocolField::getId).collect(Collectors.toList());
				protocolFieldMapper.deleteBatchIds(ids);
				deleteRuleService.deleteProtocolField(ids);
			}
			// 保存外层message信息
			ProtocolMessage pmc = new ProtocolMessage();
			pmc.setProtocolId(protocolId);
			pmc.setMessage(jProtocolDetail.getClientSendBytes().getBytes());
			Map<String, Object> pmcParam = new HashMap<String, Object>(1);
			pmcParam.put("protocolId", protocolId);
			protocolMessageMapper.deleteByMap(pmcParam);
			protocolMessageMapper.saveProtocolMessage(pmc);
			jProtocolDetail.setClientSendBytes(null);
			if (StringUtils.isNotBlank(jProtocolDetail.getServerSendBytes())) {
				ProtocolMessage pms = new ProtocolMessage();
				pms.setProtocolId(protocolId);
				pms.setMessage(jProtocolDetail.getServerSendBytes().getBytes());
				protocolMessageMapper.saveProtocolMessage(pms);
				jProtocolDetail.setServerSendBytes(null);
			}
			Map<String, Object> pgParam = new HashMap<String, Object>(1);
			pgParam.put("protocolId", protocolId);
			protocolPacketMapper.deleteByMap(pgParam);
			for (JParseGroup jpg : parseGroup) {
				ProtocolPacket pgm = new ProtocolPacket();
				pgm.setProtocolId(protocolId);
				pgm.setMessage(jpg.getMessage().getBytes());
				protocolPacketMapper.saveProtocolPacket(pgm);
				jpg.setMessage(null);
			}
			msg += "协议修改成功！";
		}
		// 给解码发送请求
		/*jProtocolDetail.setProtocolId(protocolId);
		Map<String, Object> resultMap = HttpClientUtil.postsWithMapReturn(url, JSON.toJSONString(jProtocolDetail));
		String statusCode = evUtil.getMapStrValue(resultMap, "statusCode");
		String body = evUtil.getMapStrValue(resultMap, "body");
		if (!"200".equals(statusCode)) {
			log.error("协议保存修改请求解码接口发生异常：" + body);
			throw new RuntimeException(body);
		}
		JSONObject jb = JSON.parseObject(body);
		String retCode = jb.get("retCode").toString();
		String retMessage = "";
		if ("000000".equals(retCode)) {
			return msg;
		} else if ("100011".equals(retCode) || "100012".equals(retCode) || "100022".equals(retCode)
				|| "100023".equals(retCode)) {
			retMessage += jb.get("retMessage").toString();
		} else {
			String errMessage = jb.get("retMessage").toString();
			log.error("retMessage" + errMessage);
			throw new RuntimeException(errMessage);
		}*/
		return msg;
	}
	/**
	 *
	 * @author cgc  
	 * @date 2018年10月12日  
	 * @param protocolId
	 * @return
	 */
	private  List<ProtocolField> getDeauftField(int protocolId) {
		List<ProtocolField> list=new ArrayList<>();
		ProtocolField p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(2);
		p.setEname("packetTime");
		p.setName("packetTime");
		p.setFlag(2);
		list.add(p);
		p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(2);
		p.setEname("responseTime");
		p.setName("响应时间");
		p.setFlag(2);
		list.add(p);
		p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(2);
		p.setEname("clientPort");
		p.setName("客户端端口");
		p.setFlag(1);
		list.add(p);
		/*p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(1);
		p.setEname("transactionSeq");
		p.setName("交易流水号");
		p.setFlag(1);;
		list.add(p);*/
		p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(2);
		p.setEname("clientIp");
		p.setName("客户端ip");
		p.setFlag(0);
		list.add(p);
		p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(2);
		p.setEname("serverIp");
		p.setName("服务端ip");
		p.setFlag(0);
		list.add(p);
		p=new ProtocolField();
		p.setProtocolId(protocolId);
		p.setActive(1);
		p.setType(2);
		p.setEname("serverPort");
		p.setName("服务端端口");
		p.setFlag(0);
		list.add(p);
		return list;
	}
	/**
	 * 保存字段列表
	 * 
	 * @param jProtocolDetail
	 * @param protocolId
	 * @param parseGroup
	 */
	private void fieldSave(JProtocolDetail jProtocolDetail, Integer protocolId, List<JParseGroup> parseGroup) {
		for (JParseGroup group : parseGroup) {
			List<JField> field = group.getField();
			if (!evUtil.listIsNullOrZero(field)) {
				for (JField jField : field) {
					if (jField.getIsTransport()) {
						ProtocolField p = new ProtocolField();
						p.setProtocolId(protocolId);
						p.setEname(jField.getKey());
						p.setType(jField.getDataType());
						String name = null;
						if (jField.getUseFor().intValue() == 6) {
							// 查找自定义字段名称
							name = findName(jField.getExtendId(), jProtocolDetail);
						} else {
							name = getName(jField.getUseFor());
						}
						
						EntityWrapper<ProtocolField> wrapper=new EntityWrapper<>();
						wrapper.setEntity(p);
						List<ProtocolField> list=protocolFieldMapper.selectList(wrapper);
						if(evUtil.listIsNullOrZero(list)) {
							p.setName(name);
							p.setActive(1);
							protocolFieldMapper.insert(p);
						}else {
							ProtocolField entity= list.get(0);
							entity.setName(name);
							entity.setActive(1);
							protocolFieldMapper.updateById(entity);
						}
					}
				}
			}
		}
	}
	
	/**
	 * 保存字段列表
	 * 
	 * @param jProtocolDetail
	 * @param protocolId
	 * @param parseGroup
	 */
	private List<ProtocolField> getField(JProtocolDetail jProtocolDetail, Integer protocolId, List<JParseGroup> parseGroup) {
		List<String> dimensionStr=new ArrayList<>();
		List<ProtocolField> fieldList=new ArrayList<>();
		for (JParseGroup group : parseGroup) {
			List<JField> field = group.getField();
			if (!evUtil.listIsNullOrZero(field)) {
				for (JField jField : field) {
					if (jField.getIsTransport()) {
						ProtocolField p = new ProtocolField();
						p.setProtocolId(protocolId);
						p.setType(jField.getDataType());
						String name = null;
						if (jField.getUseFor().intValue() == 6) {
							// 查找自定义字段名称
							p.setEname(findEname(jField.getExtendId(), jProtocolDetail));
							name = findName(jField.getExtendId(), jProtocolDetail);
						} else {
							if(StringUtils.isBlank(jField.getUseForKey())) {
								p.setEname(getEname(jField.getUseFor()));
								name = getName(jField.getUseFor());
							}else {
								p.setEname(jField.getUseForKey());
								name = getDictionaryName(jField.getUseFor(),jField.getUseForKey());
							}
						}
						p.setName(name);
						p.setActive(1);
						//交易流水号和金额分析维度不可以用
						if(jField.getUseFor().intValue() == 2||jField.getUseFor().intValue() == 10) {
							p.setFlag(1);	
						}else {
							p.setFlag(0);
						}
						fieldList.add(p);
					}
					if (jField.getUseFor().intValue() == 6) {
						//拼维度集合，插入bpm_protocol_dimension表
						dimensionStr.add(findEname(jField.getExtendId(), jProtocolDetail));
					}
				}
			}
		}
		if(!evUtil.listIsNullOrZero(dimensionStr)) {
			insertProtocolDimension(dimensionStr,protocolId);
		}
		return fieldList;
	}

	private String getDictionaryName(Integer useFor, String userForKey) {
		Dictionary entity=new Dictionary();
		entity.setTypeCode("FIELD_USE_FOR");
		entity.setActive(1);
		entity.setValue(useFor+":"+userForKey);
		if(null!=dictionaryMapper.selectOne(entity)) {
			return dictionaryMapper.selectOne(entity).getName();
		}else {
			return null;
		}
	}

	/**查找自定义ename
	 * @param extendId
	 * @param jProtocolDetail
	 * @return
	 */
	private String findEname(Integer extendId, JProtocolDetail jProtocolDetail) {
		List<JExtend> extend = jProtocolDetail.getExtend();
		if (!evUtil.listIsNullOrZero(extend)) {
			for (JExtend ex : extend) {
				if (ex.getId().intValue() == extendId.intValue()) {
					return ex.getKey();
				}
			}
			return null;
		} else {
			return null;
		}
	}

	private String getName(Integer useFor) {
		String name = null;
		switch (useFor) {
		case 1:
			name = "交易类型";
			break;
		case 2:
			name = "交易流水号";
			break;
		case 3:
			name = "交易渠道";
			break;
		case 4:
			name = "交易时间";
			break;
		case 5:
			name = "返回码";
			break;
		case 7:
			name = "拆分子组";
			break;
		default:
			break;
		}
		return name;
	}
	
	private String getEname(Integer useFor) {
		String name = null;
		switch (useFor) {
		case 1:
			name = "transactionType";
			break;
		case 2:
			name = "transactionSeq";
			break;
		case 3:
			name = "transactionChannel";
			break;
		case 4:
			name = "TRANS_DATE";
			break;
		case 5:
			name = "retCode";
			break;
		case 7:
			name = "拆分子组";
			break;
		default:
			break;
		}
		return name;
	}

	/**
	 * 查找自定义字段名称
	 * 
	 * @param extendId
	 * @param jProtocolDetail
	 * @return
	 */
	private String findName(Integer extendId, JProtocolDetail jProtocolDetail) {
		List<JExtend> extend = jProtocolDetail.getExtend();
		if (!evUtil.listIsNullOrZero(extend)) {
			for (JExtend ex : extend) {
				if (ex.getId().intValue() == extendId.intValue()) {
					return ex.getName();
				}
			}
			return null;
		} else {
			return null;
		}
	}

	/**
	 * 变量保存
	 * 
	 * @param jProtocolDetail
	 * @param protocolId
	 */
	private void variableSave(JProtocolDetail jProtocolDetail, Integer protocolId) {
		List<JVariable> variables = jProtocolDetail.getVariable();
		if (!evUtil.listIsNullOrZero(variables)) {
			for (JVariable var : variables) {
				if (var.getIsTransport()) {
					ProtocolField entity = new ProtocolField();
					entity.setEname(var.getKey());
					entity.setProtocolId(protocolId);
					entity.setType(var.getType());
					EntityWrapper<ProtocolField> wrapper=new EntityWrapper<>();
					wrapper.setEntity(entity);
					List<ProtocolField> list=protocolFieldMapper.selectList(wrapper);
					if(evUtil.listIsNullOrZero(list)) {
						entity.setName(var.getName());
						entity.setActive(1);
						protocolFieldMapper.insert(entity);
					}else {
						ProtocolField p= list.get(0);
						p.setName(var.getName());
						p.setActive(1);
						protocolFieldMapper.updateById(p);
					}
					
				}
			}
		}
	}
	
	/**
	 * 变量保存
	 * 
	 * @param jProtocolDetail
	 * @param protocolId
	 */
	private List<ProtocolField> getVariable(JProtocolDetail jProtocolDetail, Integer protocolId) {
		List<String> dimensionStr=new ArrayList<>();
		List<ProtocolField> fieldList=new ArrayList<>();
		List<JVariable> variables = jProtocolDetail.getVariable();
		if (!evUtil.listIsNullOrZero(variables)) {
			for (JVariable var : variables) {
				if (var.getIsTransport()) {
					ProtocolField entity = new ProtocolField();
					entity.setEname(var.getAdditionalKey());
					entity.setProtocolId(protocolId);
					entity.setType(var.getType());
					entity.setName(var.getName());
					entity.setActive(1);
					entity.setFlag(0);
					fieldList.add(entity);
				}
				//拼维度集合，插入bpm_protocol_dimension表
				dimensionStr.add(var.getAdditionalKey());
			}
		}
		if(!evUtil.listIsNullOrZero(dimensionStr)) {
			insertProtocolDimension(dimensionStr,protocolId);
		}
		return fieldList;
	}
	/**插入bpm_protocol_dimension表
	 * @param dimensionStr
	 * @param protocolId
	 */
	private void insertProtocolDimension(List<String> dimensionStr, Integer protocolId) {
		EntityWrapper<Dimension> wrapper=new EntityWrapper<>();
		wrapper.in("name", dimensionStr);
		// TODO Auto-generated method stub
		List<Dimension> dimensionList=dimensionMapper.selectList(wrapper);
		if(!evUtil.listIsNullOrZero(dimensionList)) {
			List<Integer> ids = dimensionList.stream().map(Dimension::getId).collect(Collectors.toList());
			protocolDimensionMapper.batchInsert(ids,protocolId);
		}
	}


	@Override
	public List<Map<String, Object>> selectAppProtocol(Integer userId,Map<String, Object> filter) {
		Map<String, Object> map= new HashMap<>();
		map.put("userId", userId);
		List<String> selectList=new ArrayList<>();
		String selectStr2=null;
		if(MapUtils.isNotEmpty(filter)) {
			String applicationName=evUtil.getMapStr(filter, "applicationName");
			if(StringUtils.isNotBlank(applicationName)) {
				selectStr2="and d.`name` like '%"+applicationName+"%'";
				map.put("selectStr2", selectStr2);
			}
			String compontName=evUtil.getMapStr(filter, "compontName");
			if(StringUtils.isNotBlank(compontName)) {
				String selectStr=null;
				selectStr="b.`name` like '%"+compontName+"%'";
				selectList.add(selectStr);
			}
			String name=evUtil.getMapStr(filter, "name");
			if(StringUtils.isNotBlank(name)) {
				String selectStr=null;
				selectStr="a.`name` like '%"+name+"%'";
				selectList.add(selectStr);
			}
			String ip=evUtil.getMapStr(filter, "ip");
			if(StringUtils.isNotBlank(ip)) {
				String selectStr=null;
				selectStr="s.ip like '%"+ip+"%'";
				selectList.add(selectStr);
			}
			String port=evUtil.getMapStr(filter, "port");
			if(StringUtils.isNotBlank(port)) {
				String selectStr=null;
				selectStr="s.port = "+port;
				selectList.add(selectStr);
			}
		}
		String str=null;
		if(!evUtil.listIsNullOrZero(selectList)) {
			str=StringUtils.join(selectList, " AND ");
            map.put("selectStr", " AND " + str);
		}
		return protocolMapper.selectAppProtocol(map);
	}

	@Override
	public boolean addJson(String name, String str) {
		JSONObject jb = JSON.parseObject(str);
		byte[] arr=str.getBytes();
		Protocol entity=new Protocol();
		int id=sequenceService.sequenceNextVal("decode_protocol_id");
		entity.setId(id);
		entity.setKey(jb.getString("protocolKey"));
		entity.setContent(arr);
		entity.setCreateBy(name);
		entity.setCreateDate(new Date());
		entity.setUpdateBy(name);
		entity.setUpdateDate(new Date());
		entity.setName(jb.getString("desc"));
		entity.setDesc(jb.getString("desc"));
		entity.setType(ProtocolUtil.getProtocolTypeByProtocolCode(jb.getString("protocolCode")));
		entity.setMessageFormat(ProtocolUtil.getMessageFormatByMessageType(jb.getString("messageType")));
		entity.setState(1);
		entity.setBuiltin((jb.getBooleanValue("isBuiltin") == true) ? 1 : 0);
		entity.setProtocolMode(jb.getInteger("transportMode"));
		entity.setFlag(1);
		entity.setAnalyzeflag(0);
		protocolMapper.insertProtocol(entity);
		JProtocolDetail jProtocolDetail=jb.toJavaObject(JProtocolDetail.class);
		int protocolId=entity.getId();
		List<JParseGroup> parseGroup = jProtocolDetail.getParseGroup();
		//默认字段
		List<ProtocolField> fieldList=getDeauftField(protocolId);
		// 保存字段列表
		if (!evUtil.listIsNullOrZero(parseGroup)) {
			fieldList.addAll(getField(jProtocolDetail, protocolId, parseGroup));
		}
		Set<ProtocolField> set = new HashSet<>();
		fieldList.stream().forEach(x -> {
			set.add(x);
		});
		protocolFieldMapper.batchInsert(set);
	    return true;
	}
	
	@Override
	public Boolean checkProtocolName(int id,String name, int flag) {
		EntityWrapper<Protocol> wrapper=new EntityWrapper<>();
		wrapper.where("name={0}", name);
		wrapper.in("flag", "1,2");
		wrapper.ne("state", -1);
		List<Protocol> list = new ArrayList<>();
		if (flag == 1) {//添加
			list = protocolMapper.selectList(wrapper);
		} else {
			wrapper.ne("id", id);
			list = protocolMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);

	}
	
	@Override
	public Boolean checkProtocolKey(int id,String key, int flag) {
		EntityWrapper<Protocol> wrapper=new EntityWrapper<>();
		wrapper.where("`key`={0}", key);
		wrapper.ne("state", -1);
		List<Protocol> list = new ArrayList<>();
		if (flag == 1) {//添加
			list = protocolMapper.selectList(wrapper);
		} else {
			wrapper.ne("id", id);
			list = protocolMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);

	}

	@Override
	public boolean deleteProtocolPhysical(List<Integer> ids) {
		if(evUtil.listIsNullOrZero(ids)){
			return true;
		}
		//bpm_protocol
		protocolMapper.deleteBatchIds(ids);

		//bpm_protocol_field
		EntityWrapper<ProtocolField> protocolFieldEntityWrapper = new EntityWrapper<>();
		protocolFieldEntityWrapper.in("protocolId", ids);
		List<Integer> fieldIds=protocolFieldMapper.selectList(protocolFieldEntityWrapper).stream().map(ProtocolField::getId).collect(Collectors.toList());
		deleteRuleService.deleteProtocolField(fieldIds);
		protocolFieldMapper.delete(protocolFieldEntityWrapper);

		//bpm_protocol_packet
		EntityWrapper<ProtocolPacket> protocolPacketEntityWrapper = new EntityWrapper<>();
		protocolPacketEntityWrapper.in("protocolId", ids);
		protocolPacketMapper.delete(protocolPacketEntityWrapper);

		//bpm_protocol_message
		EntityWrapper<ProtocolMessage> protocolMessageEntityWrapper = new EntityWrapper<>();
		protocolMessageEntityWrapper.in("protocolId", ids);
		protocolMessageMapper.delete(protocolMessageEntityWrapper);
		
		return true;
	}

	@Override
	public Boolean updateByUploadJson(String name, String str, int protocolId) {
		JSONObject jb = JSON.parseObject(str);
		byte[] arr=str.getBytes();
		Protocol entity=new Protocol();
		entity.setId(protocolId);
		entity.setKey(jb.getString("protocolKey"));
		entity.setContent(arr);
		entity.setUpdateBy(name);
		entity.setUpdateDate(new Date());
		entity.setName(jb.getString("desc"));
		entity.setDesc(jb.getString("desc"));
		entity.setType(ProtocolUtil.getProtocolTypeByProtocolCode(jb.getString("protocolCode")));
		entity.setMessageFormat(ProtocolUtil.getMessageFormatByMessageType(jb.getString("messageType")));
		entity.setBuiltin((jb.getBooleanValue("isBuiltin") == true) ? 1 : 0);
		entity.setProtocolMode(jb.getInteger("transportMode"));
		protocolMapper.updateById(entity);
		JProtocolDetail jProtocolDetail=jb.toJavaObject(JProtocolDetail.class);
		List<JParseGroup> parseGroup = jProtocolDetail.getParseGroup();
		ProtocolField protocol=new ProtocolField();
		protocol.setActive(0);
		EntityWrapper<ProtocolField> wrapper=new EntityWrapper<>();
		wrapper.where("protocolId={0}", protocolId);
		protocolFieldMapper.update(protocol, wrapper);
		//删除bpm_protocol_dimension
		EntityWrapper<ProtocolDimension> wrap=new EntityWrapper<>();
		wrap.where("protocolId={0}", protocolId);
		protocolDimensionMapper.delete(wrap);
		//默认字段
		List<ProtocolField> fieldList=getDeauftField(protocolId);
		// 保存字段列表
		if (!evUtil.listIsNullOrZero(parseGroup)) {
			fieldList.addAll(getField(jProtocolDetail, protocolId, parseGroup));
		}
		Set<ProtocolField> set = new HashSet<>();
		fieldList.stream().forEach(x -> {
			set.add(x);
		});
		//判断数据库是否有协议字段
		Set<ProtocolField> insertSet = new HashSet<>();
		for (ProtocolField protocolField : set) {
			Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("protocolId", protocolField.getProtocolId());
			columnMap.put("name", protocolField.getName());
			columnMap.put("type", protocolField.getType());
			columnMap.put("ename", protocolField.getEname());
			columnMap.put("flag", protocolField.getFlag());
			List<ProtocolField> p=protocolFieldMapper.selectByMap(columnMap);
			if(!evUtil.listIsNullOrZero(p)) {
				List<Integer> ids = p.stream().map(ProtocolField::getId).collect(Collectors.toList());
				protocolFieldMapper.batchUpadteActive(ids);
			}else {
				insertSet.add(protocolField);
			}
		}
		if(null!=insertSet&&insertSet.size()>0) {
			protocolFieldMapper.batchInsert(insertSet);
		}
		//查询该协议下不可用字段
		Map<String, Object> col=new HashMap<>(2);
		col.put("protocolId", protocolId);
		col.put("active", 0);
		List<ProtocolField> pro=protocolFieldMapper.selectByMap(col);
		if(!evUtil.listIsNullOrZero(pro)) {
			List<Integer> ids = pro.stream().map(ProtocolField::getId).collect(Collectors.toList());
			protocolFieldMapper.deleteBatchIds(ids);
			deleteRuleService.deleteProtocolField(ids);
		}
	    return true;
	}

	@Override
	public List<Dimension> selectProtocolDimension(Integer protocolId) {
		if(null!=protocolId) {
			return dimensionMapper.selectProtocolDimension(protocolId);
		}else {
			return dimensionMapper.selectList(null);
		}
	}

	@Override
	public boolean saveOrUpdateProDimension(Dimension dimension) {
		int result = dimensionMapper.insert(dimension);
		return result > 0 ? true : false;
	}

	@Override
	public List<Dimension> selectProDimension() {
		return dimensionMapper.selectByMap(null);
	}

	@Override
	public Dimension selectProDimensionById(int id) {
		Map<String, Object> param = new HashMap<>(1);
		param.put("id", id);
		List<Dimension> list = dimensionMapper.selectByMap(param);
		if(!evUtil.listIsNullOrZero(list)){
			return list.get(0);
		}else{
			return null;
		}
	}

	@Override
	public List<Dimension> selectProDimensionByName(String name) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("name", name);
		return dimensionMapper.selectByMap(map);
	}

	@Override
	public boolean addLogsJson(String name, String str) {
		JSONObject jb = JSON.parseObject(str);
		byte[] arr=str.getBytes();
		Protocol entity=new Protocol();
		int id=sequenceService.sequenceNextVal("decode_protocol_id");
		entity.setId(id);
		entity.setKey(jb.getString("key"));
		entity.setContent(arr);
		entity.setCreateBy(name);
		entity.setCreateDate(new Date());
		entity.setUpdateBy(name);
		entity.setUpdateDate(new Date());
		entity.setName(jb.getString("desc"));
		entity.setDesc(jb.getString("desc"));
		entity.setType(jb.getString("type"));
		entity.setMessageFormat(jb.getString("messageFormat"));
		entity.setState(1);
		entity.setFlag(jb.getInteger("flag"));
		entity.setAnalyzeflag(0);
		protocolMapper.insertProtocol(entity);
		JProtocolDetail jProtocolDetail=jb.toJavaObject(JProtocolDetail.class);
		int protocolId=entity.getId();
		//默认字段
		List<ProtocolField> fieldList=getDeauftField(protocolId);
		// 保存字段列表
		List<JSONObject> field=(List<JSONObject>) jb.get("field");
		if (!evUtil.listIsNullOrZero(field)) {
			fieldList.addAll(getLogField(jProtocolDetail, protocolId, field));
		}
		Set<ProtocolField> set = new HashSet<>();
		fieldList.stream().forEach(x -> {
			set.add(x);
		});
		protocolFieldMapper.batchInsert(set);
	    return true;
	}

	private List<ProtocolField> getLogField(JProtocolDetail jProtocolDetail, int protocolId,
			List<JSONObject> field) {
		List<String> dimensionStr=new ArrayList<>();
		List<ProtocolField> fieldList=new ArrayList<>();
			if (!evUtil.listIsNullOrZero(field)) {
				for (JSONObject jField : field) {
						ProtocolField p = new ProtocolField();
						p.setProtocolId(protocolId);
						p.setType(jField.getInteger("dataType"));
						String name = null;
						if (jField.getInteger("useFor").equals(6)) {
							// 查找自定义字段名称
							p.setEname(findEname(jField.getInteger("extendId"), jProtocolDetail));
							name = findName(jField.getInteger("extendId"), jProtocolDetail);
							//拼维度集合，插入bpm_protocol_dimension表
							dimensionStr.add(p.getEname());
						} else {
							if(StringUtils.isBlank(jField.getString("useForKey"))) {
								p.setEname(getEname(jField.getInteger("useFor")));
								name = getName(jField.getInteger("useFor"));
							}else {
								p.setEname(jField.getString("useForKey"));
								name = getDictionaryName(jField.getInteger("useFor"),jField.getString("useForKey"));
							}
						}
						p.setName(name);
						p.setActive(1);
						//交易流水号分析维度不可以用
						if(jField.getInteger("useFor").equals(2)) {
							p.setFlag(1);	
						}else {
							p.setFlag(0);
						}
						fieldList.add(p);
				}
			}
		if(!evUtil.listIsNullOrZero(dimensionStr)) {
			insertProtocolDimension(dimensionStr,protocolId);
		}
		return fieldList;
	}

	@Override
	public Boolean updateLogsByUploadJson(String name, String str, int protocolId) {
		JSONObject jb = JSON.parseObject(str);
		byte[] arr=str.getBytes();
		Protocol entity=new Protocol();
		entity.setId(protocolId);
		entity.setKey(jb.getString("key"));
		entity.setContent(arr);
		entity.setUpdateBy(name);
		entity.setUpdateDate(new Date());
		entity.setName(jb.getString("desc"));
		entity.setDesc(jb.getString("desc"));
		entity.setType(jb.getString("type"));
		entity.setMessageFormat(jb.getString("messageFormat"));
		protocolMapper.updateById(entity);
		JProtocolDetail jProtocolDetail=jb.toJavaObject(JProtocolDetail.class);
		ProtocolField protocol=new ProtocolField();
		protocol.setActive(0);
		EntityWrapper<ProtocolField> wrapper=new EntityWrapper<>();
		wrapper.where("protocolId={0}", protocolId);
		protocolFieldMapper.update(protocol, wrapper);
		//删除bpm_protocol_dimension
		EntityWrapper<ProtocolDimension> wrap=new EntityWrapper<>();
		wrap.where("protocolId={0}", protocolId);
		protocolDimensionMapper.delete(wrap);
		//默认字段
		List<ProtocolField> fieldList=getDeauftField(protocolId);
		// 保存字段列表
		List<JSONObject> field=(List<JSONObject>) jb.get("field");
		if (!evUtil.listIsNullOrZero(field)) {
			fieldList.addAll(getLogField(jProtocolDetail, protocolId, field));
		}
		Set<ProtocolField> set = new HashSet<>();
		fieldList.stream().forEach(x -> {
			set.add(x);
		});
		//判断数据库是否有协议字段
		Set<ProtocolField> insertSet = new HashSet<>();
		for (ProtocolField protocolField : set) {
			Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("protocolId", protocolField.getProtocolId());
			columnMap.put("name", protocolField.getName());
			columnMap.put("type", protocolField.getType());
			columnMap.put("ename", protocolField.getEname());
			columnMap.put("flag", protocolField.getFlag());
			List<ProtocolField> p=protocolFieldMapper.selectByMap(columnMap);
			if(!evUtil.listIsNullOrZero(p)) {
				List<Integer> ids = p.stream().map(ProtocolField::getId).collect(Collectors.toList());
				protocolFieldMapper.batchUpadteActive(ids);
			}else {
				insertSet.add(protocolField);
			}
		}
		if(null!=insertSet&&insertSet.size()>0) {
			protocolFieldMapper.batchInsert(insertSet);
		}
		//查询该协议下不可用字段
		Map<String, Object> col=new HashMap<>(2);
		col.put("protocolId", protocolId);
		col.put("active", 0);
		List<ProtocolField> pro=protocolFieldMapper.selectByMap(col);
		if(!evUtil.listIsNullOrZero(pro)) {
			List<Integer> ids = pro.stream().map(ProtocolField::getId).collect(Collectors.toList());
			protocolFieldMapper.deleteBatchIds(ids);
			deleteRuleService.deleteProtocolField(ids);
		}
	    return true;
	}
}
