package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ScheduleRepeatDays;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 重复排期日期 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
public interface ScheduleRepeatDaysMapper extends BaseMapper<ScheduleRepeatDays> {

}
