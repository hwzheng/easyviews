package com.greattimes.ev.bpm.service.custom.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import com.greattimes.ev.bpm.mapper.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomBasicParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomFieldParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomFieldValueParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomParam;
import com.greattimes.ev.bpm.entity.AlarmData;
import com.greattimes.ev.bpm.entity.AnalyzeDimension;
import com.greattimes.ev.bpm.entity.AnalyzeIndicator;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.Ctuuid;
import com.greattimes.ev.bpm.entity.Custom;
import com.greattimes.ev.bpm.entity.CustomOtherName;
import com.greattimes.ev.bpm.entity.DimensionValue;
import com.greattimes.ev.bpm.entity.FilterField;
import com.greattimes.ev.bpm.entity.FilterFieldValue;
import com.greattimes.ev.bpm.entity.StatisticsDimension;
import com.greattimes.ev.bpm.entity.SuccessField;
import com.greattimes.ev.bpm.entity.SuccessFieldValue;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.custom.ICustomService;
import com.greattimes.ev.common.utils.evUtil;


/**
 * <p>
 * 自定义分析配置 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
@Service
public class CustomServiceImpl extends ServiceImpl<CustomMapper, Custom> implements ICustomService {
	@Autowired
	private CustomMapper customMapper;
	@Autowired
	private FilterFieldMapper filterFieldMapper;
	@Autowired
	private AnalyzeDimensionMapper analyzeDimensionMapper;
	@Autowired
	private AnalyzeIndicatorMapper analyzeIndicatorMapper;
	@Autowired
	private CustomOtherNameMapper customOtherNameMapper;
	@Autowired
	private DimensionValueMapper dimensionValueMapper;
	@Autowired
	private FilterFieldValueMapper filterFieldValueMapper;
	@Autowired
	private StatisticsDimensionMapper statisticsDimensionMapper;
	@Autowired
	private SuccessFieldMapper successFieldMapper;
	@Autowired
	private SuccessFieldValueMapper successFieldValueMapper;
	@Autowired
	private ComponentMapper componentMapper;
	@Autowired
	private CtuuidMapper ctuuidMapper;
	@Autowired
	private ApplicationMapper applicationMapper;
	@Autowired
	private SequenceMapper sequenceMapper;
	@Autowired
	private RegioninfoMapper regioninfoMapper;
	@Autowired
	private IDeleteRuleService deleteRuleService;
	@Autowired
    private AlarmDataMapper alarmDataMapper;
	@Autowired
	private FieldMappingMapper fieldMappingMapper;
	@Autowired
	private DictionaryMapper dictionaryMapper;

	@Override
	public List<CustomParam> selectCustom(Integer userId) {
		List<DictionaryParam> list=applicationMapper.selectDictonaryAppByUserId(userId);
		if(evUtil.listIsNullOrZero(list)) {
			return null;
		}else {
			return customMapper.selectCustom(list);
		}
	}

	@Override
	public void updateSort(List<CustomParam> ids) {
		if (ids.size() != 0) {
			customMapper.batchUpdateSort(ids);
		}

	}

	@Override
	public void deleteCustom(int id, int type) {
		Map<String, Object> colmap = new HashMap<String, Object>(1);
		Map<String, Object> col = new HashMap<String, Object>(1);
		colmap.put("customId", id);
		if (type == 1) {
			customMapper.deleteById(id);
			// 自定义指标删除
			analyzeIndicatorMapper.deleteByMap(colmap);
		}
		// 筛选条件删除
		List<FilterField> filterFields = filterFieldMapper.selectByMap(colmap);
		for (FilterField filterField2 : filterFields) {
			col.clear();
			col.put("filterFieldId", filterField2.getId());
			filterFieldValueMapper.deleteByMap(col);
		}
		filterFieldMapper.deleteByMap(colmap);
		// 成功值判断删除
		List<SuccessField> successFields = successFieldMapper.selectByMap(colmap);
		for (SuccessField successField2 : successFields) {
			col.clear();
			col.put("successFieldId", successField2.getId());
			successFieldValueMapper.deleteByMap(col);
		}
		successFieldMapper.deleteByMap(colmap);
		// 统计维度删除
		List<StatisticsDimension> statisticsDimensions = statisticsDimensionMapper.selectByMap(colmap);
		for (StatisticsDimension statisticsDimension2 : statisticsDimensions) {
			col.clear();
			col.put("statisticsDimensionId", statisticsDimension2.getId());
			dimensionValueMapper.deleteByMap(col);
		}
		statisticsDimensionMapper.deleteByMap(colmap);
		// 别名表删除
		customOtherNameMapper.deleteByMap(colmap);
		// 自定义维度删除
		List<AnalyzeDimension> analyzeDimensionList = analyzeDimensionMapper.selectByMap(colmap);
		analyzeDimensionMapper.deleteByMap(colmap);
		// uuid删除
		String value = "customid:" + id;
		deleteUuid(value);

		/**
		 * 统计维度和分析维度删除
		 * @author NJ
		 */
		deleteRuleService.deleteDimension(statisticsDimensions.stream().map(StatisticsDimension::getId).collect(Collectors.toList()), 1);
		deleteRuleService.deleteDimension(analyzeDimensionList.stream().map(AnalyzeDimension::getDimensionId)
		.collect(Collectors.toList()), 0);
		EntityWrapper<AlarmData> wrapper=new EntityWrapper<>();
		wrapper.where("alarmId={0}", id);
		/**==============bpm_alarm_data删除=======================*/
        alarmDataMapper.delete(wrapper);
	}

	/**
	 * 根据value删除uuid
	 * @param value
	 */
	public void deleteUuid(String value) {
		EntityWrapper<Ctuuid> wrapper = new EntityWrapper<>();
		Ctuuid uuid = new Ctuuid();
		wrapper.setEntity(uuid);
		wrapper.like("value", value);
		ctuuidMapper.delete(wrapper);
	}

	@Override
	public void updateActive(List<CustomParam> custom) {
		if (custom.size() != 0) {
			customMapper.batchUpdateActive(custom);
		}
	}

	@Override
	public void updateState(List<CustomParam> custom) {
		if (custom.size() != 0) {
			customMapper.batchUpdateState(custom);
		}
	}

	@Override
	public List<CustomFieldParam> selectDimension(int customId) {
		List<CustomFieldParam> analyzelist = analyzeDimensionMapper.selectDimension(customId);
		/*List<CustomFieldParam> newList = new ArrayList<>();
		// 去除统计维度
		for (CustomFieldParam customFieldParam : analyzelist) {
			Map<String, Object> columnMap = new HashMap<>();
			columnMap.put("customId", customId);
			columnMap.put("dimensionId", customFieldParam.getFieldId());
			List<StatisticsDimension> list = statisticsDimensionMapper.selectByMap(columnMap);
			if (evUtil.listIsNullOrZero(list)) {
				newList.add(customFieldParam);
			}
		}*/
		return analyzelist;
	}

	@Override
	public List<Map<String, Object>> selectIndicator(int customId) {
		return analyzeIndicatorMapper.selectIndicator(customId);
	}

	@Override
	public void addIndicator(int customId, List<CustomFieldParam> list) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("customId", customId);
		analyzeIndicatorMapper.deleteByMap(map);
		if (!evUtil.listIsNullOrZero(list)) {
			for (CustomFieldParam customFieldParam : list) {
				AnalyzeIndicator entity = new AnalyzeIndicator();
				entity.setIndicatorId(customFieldParam.getFieldId());
				entity.setCustomId(customId);
				analyzeIndicatorMapper.insert(entity);
			}
		}
	}

	@Override
	public void addDimension(int customId, List<CustomFieldParam> list) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("customId", customId);
		List<Integer> originDimensionIds = analyzeDimensionMapper.selectByMap(map).stream()
				.map(AnalyzeDimension::getDimensionId).collect(Collectors.toList());
		analyzeDimensionMapper.deleteByMap(map);
		List<Integer> newDimensionIds = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(list)) {
			for (CustomFieldParam customFieldParam : list) {
				AnalyzeDimension entity = new AnalyzeDimension();
				entity.setDimensionId(customFieldParam.getFieldId());
				entity.setCustomId(customId);
				entity.setSort(customFieldParam.getSort());
				analyzeDimensionMapper.insert(entity);
			}
		}
		
		
		//组件维度变化删除相应的告警
		List<Integer> deleteDimensionIds = new ArrayList<>();
		deleteDimensionIds.addAll(originDimensionIds);
		originDimensionIds.retainAll(newDimensionIds);
		deleteDimensionIds.removeAll(originDimensionIds);
		deleteRuleService.deleteDimension(deleteDimensionIds,0);

	}

	@Override
	public List<StatisticsDimension> updateStepAndselectStatistics(int customId) {
		List<StatisticsDimension> list = statisticsDimensionMapper.selectStatisticsDimension(customId);
		// 若不为空，更新步骤状态
		if (!evUtil.listIsNullOrZero(list)) {
			Custom custom = customMapper.selectById(customId);
			String step = custom.getStep();
			char first = step.charAt(0);
			char second = step.charAt(2);
			char fourth = step.charAt(6);
			step = first + "," + second + "," + 1 + "," + fourth;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		} else {
			Custom custom = customMapper.selectById(customId);
			String step = custom.getStep();
			char first = step.charAt(0);
			char second = step.charAt(2);
			char fourth = step.charAt(6);
			step = first + "," + second + "," + 0 + "," + fourth;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		}
		return list;
	}

	@Override
	public List<DimensionParam> selectOtherName(Integer customId, Integer menuId) {
		List<DimensionParam> list = new ArrayList<>();
		switch (menuId) {
		case 0:
			List<Component> comp = componentMapper.getPrototcolId(customId);
			if(!evUtil.listIsNullOrZero(comp)) {
				Integer protocolId = comp.get(0).getPrototcolId();
				list = customOtherNameMapper.selectOtherName(customId, protocolId);
			}
			break;
		case 1:
			list = customOtherNameMapper.selectIndicatorOtherName(customId);
			break;
		default:
			break;
		}
		return list;
	}

	@Override
	public void saveOtherName(DimensionParam param) {
		int menuId = param.getMenuId();
		int customId = param.getCustomId();
		int fieldId = param.getFieldId();
		String othername = param.getOtherName();
		switch (menuId) {
		case 0:
			Map<String, Object> map = new HashMap<>(2);
			map.put("fieldId", fieldId);
			map.put("customId", customId);
			customOtherNameMapper.deleteByMap(map);
			if (!(null == othername || "".equals(othername))) {
				CustomOtherName field = new CustomOtherName();
				field.setCustomId(customId);
				field.setFieldId(fieldId);
				field.setOthername(othername);
				customOtherNameMapper.insert(field);
			}
			break;
		case 1:
			if(othername.equals("")) {
				othername=null;
			}
			customOtherNameMapper.updateAnalyzeIndicator(customId, fieldId, othername);
			break;
		default:
			break;
		}
	}

	@Override
	public int addBasicMsg(int applicationId, int componentId, String name) {
		Map<String,Object> map=new HashMap<String,Object>();
	    map.put("sequenceName", "seq_uuid");
		int id=sequenceMapper.nextVal(map);
		Custom entity = new Custom();
		entity.setName(name);
		entity.setApplicationId(applicationId);
		entity.setComponentId(componentId);
		EntityWrapper<Custom> wrapper = new EntityWrapper<>();
		Custom st = new Custom();
		wrapper.setEntity(st);
		wrapper.orderBy("sort");
		List<Custom> list = customMapper.selectList(wrapper);
		if (!evUtil.listIsNullOrZero(list)) {
			Integer sort = list.get(list.size() - 1).getSort() + 1;
			entity.setSort(sort);
		} else {
			entity.setSort(1);
		}
		entity.setStep("1,0,0,0");
		entity.setId(id);
		customMapper.insert(entity);
		Ctuuid uu = new Ctuuid();
		String value = "applicationid:" + applicationId + ",customid:" + entity.getId();
		uu.setId((long)id);
		uu.setValue(value);
		ctuuidMapper.insert(uu);
		return entity.getId();
	}

	@Override
	public void editBasicMsg(int id, int applicationId, int componentId, String name, Boolean change) {
		Custom entity = new Custom();
		entity.setId(id);
		entity.setName(name);
		entity.setApplicationId(applicationId);
		entity.setComponentId(componentId);
		if (change == false) {
			customMapper.updateById(entity);
		} else {
			// uuid删除
			String value = "customid:" + id;
			deleteUuid(value);
			customMapper.updateCustom(entity);
			deleteCustom(id, 2);
			// uuid添加
			Ctuuid uu = new Ctuuid();
			String val = "applicationid:" + applicationId + ",customid:" + id;
			uu.setValue(val);
			uu.setId(Long.valueOf(String.valueOf(id)));
			ctuuidMapper.insert(uu);
		}
	}

	@Override
	public Custom selectBasicMsg(int id) {
		return customMapper.selectById(id);
	}

	@Override
	public List<DimensionValue> selectDimensionDetail(int statisticsDimensionId) {
		EntityWrapper<DimensionValue> wrapper=new EntityWrapper<>();
		wrapper.where("statisticsDimensionId={0}", statisticsDimensionId).orderBy("value");
		return dimensionValueMapper.selectList(wrapper);
	}

	@Override
	public void editDimension(int id, int dimensionId, String name,Integer useDictionary) {
		List<Map<String, Object>> valuelist = statisticsDimensionMapper.selectUuidValue(id);
		if (!evUtil.listIsNullOrZero(valuelist)) {
			Map<String, Object> map = valuelist.get(0);
			Integer customId = evUtil.getMapIntValue(map, "customId");
			Integer type = evUtil.getMapIntValue(map, "type");
			String ename = evUtil.getMapStrValue(map, "ename");
			String value = null;
			if (0 == type) {
				value = "customid:" + customId + ",dimension:" + ename + ",";
			} else {
				value = "customid:" + customId + ",dimension:" + ename + "_" + type;
			}
			// uuid删除
			deleteUuid(value);
		}
		StatisticsDimension entity = new StatisticsDimension();
		entity.setId(id);
		entity.setDimensionId(dimensionId);
		entity.setName(name);
		entity.setUseDictionary(useDictionary);
		statisticsDimensionMapper.updateById(entity);
		// uuid添加
		saveUuid(id);
	}

	@Override
	public void deleteDimension(int id) {
		List<Map<String, Object>> valuelist = statisticsDimensionMapper.selectUuidValue(id);
		if (!evUtil.listIsNullOrZero(valuelist)) {
			Map<String, Object> map = valuelist.get(0);
			Integer customId = evUtil.getMapIntValue(map, "customId");
			Integer type = evUtil.getMapIntValue(map, "type");
			String ename = evUtil.getMapStrValue(map, "ename");
			String value = null;
			if (0 == type) {
				value = "customid:" + customId + ",dimension:" + ename + ",";
			} else {
				value = "customid:" + customId + ",dimension:" + ename + "_" + type;
			}
			deleteUuid(value);
		}
		statisticsDimensionMapper.deleteById(id);
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("statisticsDimensionId", id);
		dimensionValueMapper.deleteByMap(columnMap);

		/**
		 * 统计维度变化删除告警逻辑
		 * @autor NJ
		 */
		List<Integer> dimensionId = new ArrayList<>();
		dimensionId.add(id);
		deleteRuleService.deleteDimension(dimensionId,1);
	}

	@Override
	public CustomBasicParam updateStepAndselectFilter(int customId) {
		CustomBasicParam param = new CustomBasicParam();
		Custom custom = customMapper.selectById(customId);
		param.setId(customId);
		param.setType(custom.getFilterType());
		param.setExpressions(custom.getFilterExpressions());
		param.setRelation(custom.getFilterRelation());
		List<CustomFieldValueParam> fieldvalue = filterFieldMapper.selectFilter(customId);
		if (!evUtil.listIsNullOrZero(fieldvalue)) {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char third = step.charAt(4);
			char fourth = step.charAt(6);
			step = first + "," + 1 + "," + third + "," + fourth;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		} else {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char third = step.charAt(4);
			char fourth = step.charAt(6);
			step = first + "," + 0 + "," + third + "," + fourth;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		}
		param.setFieldValue(fieldvalue);
		return param;
	}

	@Override
	public CustomBasicParam updateStepAndselectSuccess(int customId) {
		CustomBasicParam param = new CustomBasicParam();
		Custom custom = customMapper.selectById(customId);
		param.setId(customId);
		param.setType(custom.getSuccessType());
		param.setExpressions(custom.getSuccessExpressions());
		param.setRelation(custom.getSuccessRelation());
		List<CustomFieldValueParam> fieldvalue = filterFieldMapper.selectSuccess(customId);
		if (!evUtil.listIsNullOrZero(fieldvalue)) {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char second = step.charAt(2);
			char third = step.charAt(4);
			step = first + "," + second + "," + third + "," + 1;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		} else {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char second = step.charAt(2);
			char third = step.charAt(4);
			step = first + "," + second + "," + third + "," + 0;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		}
		param.setFieldValue(fieldvalue);
		return param;
	}

	@Override
	public void saveFilter(CustomBasicParam param) {
		Map<String, Object> colmap = new HashMap<String, Object>(1);
		Map<String, Object> col = new HashMap<String, Object>(1);
		Integer type = param.getType();
		Integer customId = param.getId();
		List<CustomFieldValueParam> valus = param.getFieldValue();
		/*Custom custom = customMapper.selectById(customId);
		String step = custom.getStep();
		char first = step.charAt(0);
		char third = step.charAt(4);
		char fourth = step.charAt(6);
		step = first + "," + 1 + "," + third + "," + fourth;*/
		Custom en = new Custom();
		en.setId(customId);
		en.setFilterRelation(param.getRelation());
		en.setFilterType(type);
		// en.setStep(step);
		if (null != type) {
			if (1 == type) {
				en.setFilterExpressions(param.getExpressions());
				customMapper.updateById(en);
			} else if (0 == type) {
				String filterExp = "";
				for (CustomFieldValueParam customFieldValueParam : valus) {
					if (null != customFieldValueParam.getIndex()) {
						if (param.getRelation() == 0) {
							filterExp += customFieldValueParam.getIndex() + "&&";
						} else if (param.getRelation() == 1) {
							filterExp += customFieldValueParam.getIndex() + "||";
						}
					}
				}
				if (!"".equals(filterExp)) {
					filterExp = filterExp.substring(0, filterExp.length() - 2);
				}
				en.setFilterExpressions(filterExp);
				customMapper.updateById(en);
			}
		}else if (null == type) {
			customMapper.updateFilter(customId);
		}
		
		colmap.put("customId", customId);
		// 筛选条件删除
		List<FilterField> FilterField = filterFieldMapper.selectByMap(colmap);
		for (FilterField filterField2 : FilterField) {
			col.clear();
			col.put("filterFieldId", filterField2.getId());
			filterFieldValueMapper.deleteByMap(col);
		}
		filterFieldMapper.deleteByMap(colmap);
		// 筛选条件添加
		for (CustomFieldValueParam customFieldValueParam : valus) {
			FilterField entity = new FilterField();
			entity.setDimensionId(customFieldValueParam.getDimensionId());
			entity.setCustomId(customId);
			entity.setRelation(customFieldValueParam.getRelation());
			entity.setIndex(customFieldValueParam.getIndex());
			filterFieldMapper.insert(entity);
			String values = customFieldValueParam.getValues();
			String[] val = values.split("\n");
			for (String string : val) {
				if (!"".equals(string.trim())) {
					FilterFieldValue e = new FilterFieldValue();
					e.setFilterFieldId(entity.getId());
					e.setValue(string.trim());
					filterFieldValueMapper.insert(e);
				}
			}
		}
		//更新step
		List<CustomFieldValueParam> fieldvalue = filterFieldMapper.selectFilter(customId);
		if (!evUtil.listIsNullOrZero(fieldvalue)) {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char third = step.charAt(4);
			char fourth = step.charAt(6);
			step = first + "," + 1 + "," + third + "," + fourth;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		} else {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char third = step.charAt(4);
			char fourth = step.charAt(6);
			step = first + "," + 0 + "," + third + "," + fourth;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		}
	}

	@Override
	public void saveSuccess(CustomBasicParam param) {
		Map<String, Object> colmap = new HashMap<String, Object>(1);
		Map<String, Object> col = new HashMap<String, Object>(1);
		Integer type = param.getType();
		Integer customId = param.getId();
		List<CustomFieldValueParam> valus = param.getFieldValue();
		Custom en = new Custom();
		en.setId(customId);
		en.setSuccessRelation(param.getRelation());
		en.setSuccessType(type);
		if (null != type) {
			if (1 == type) {
				en.setSuccessExpressions(param.getExpressions());
				customMapper.updateById(en);
			} else if (0 == type) {
				String filterExp = "";
				for (CustomFieldValueParam customFieldValueParam : valus) {
					if (null != customFieldValueParam.getIndex()) {
						if (param.getRelation() == 0) {
							filterExp += customFieldValueParam.getIndex() + "&&";
						} else if (param.getRelation() == 1) {
							filterExp += customFieldValueParam.getIndex() + "||";
						}
					}
				}
				if (!"".equals(filterExp)) {
					filterExp = filterExp.substring(0, filterExp.length() - 2);
				}
				en.setSuccessExpressions(filterExp);
				customMapper.updateById(en);
			}
		} else if (null == type) {
			customMapper.updateSuccess(customId);
		}

		colmap.put("customId", customId);
		// 成功值判断删除
		List<SuccessField> SuccessField = successFieldMapper.selectByMap(colmap);
		for (SuccessField successField2 : SuccessField) {
			col.clear();
			col.put("successFieldId", successField2.getId());
			successFieldValueMapper.deleteByMap(col);
		}
		successFieldMapper.deleteByMap(colmap);
		// 成功条件添加
		for (CustomFieldValueParam customFieldValueParam : valus) {
			SuccessField entity = new SuccessField();
			entity.setDimensionId(customFieldValueParam.getDimensionId());
			entity.setCustomId(customId);
			entity.setRelation(customFieldValueParam.getRelation());
			entity.setIndex(customFieldValueParam.getIndex());
			successFieldMapper.insert(entity);
			String values = customFieldValueParam.getValues();
			String[] val = values.split("\n");
			for (String string : val) {
				if (!"".equals(string.trim())) {
					SuccessFieldValue e = new SuccessFieldValue();
					e.setSuccessFieldId(entity.getId());
					e.setValue(string);
					successFieldValueMapper.insert(e);
				}
			}
		}
		//更新step
		List<CustomFieldValueParam> fieldvalue = filterFieldMapper.selectSuccess(customId);
		if (!evUtil.listIsNullOrZero(fieldvalue)) {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char second = step.charAt(2);
			char third = step.charAt(4);
			step = first + "," + second + "," + third + "," + 1;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		} else {
			Custom cus = customMapper.selectById(customId);
			String step = cus.getStep();
			char first = step.charAt(0);
			char second = step.charAt(2);
			char third = step.charAt(4);
			step = first + "," + second + "," + third + "," + 0;
			Custom entity = new Custom();
			entity.setId(customId);
			entity.setStep(step);
			customMapper.updateById(entity);
		}
	}

	@Override
	public void insertDimensionValue(List<DimensionValue> details, int statisticsDimensionId) {
		List<Map<String, Object>> valuelist = statisticsDimensionMapper.selectUuidValue(statisticsDimensionId);
		if (!evUtil.listIsNullOrZero(valuelist)) {
			Map<String, Object> map = valuelist.get(0);
			Integer customId = evUtil.getMapIntValue(map, "customId");
			Integer type = evUtil.getMapIntValue(map, "type");
			String ename = evUtil.getMapStrValue(map, "ename");
			String value = null;
			if (0 == type) {
				value = "customid:" + customId + ",dimension:" + ename + ",";
			} else {
				value = "customid:" + customId + ",dimension:" + ename + "_" + type;
			}
			deleteUuid(value);
		}
		List<Integer> deleteIds=new ArrayList<>();
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("statisticsDimensionId", statisticsDimensionId);
		//查询原有数据
		List<DimensionValue> oldList=dimensionValueMapper.selectByMap(columnMap);
		if(!evUtil.listIsNullOrZero(oldList)) {
			for (DimensionValue oldValue : oldList) {
				int newId = 0;
				boolean flag=false;
				for (DimensionValue newValue : details) {
					if(oldValue.equals(newValue)) {
						newId=newValue.getId();
						flag=true;
						break;
					}
				}
				//合集
				if(flag) {
					DimensionValue o=new DimensionValue();
					o.setId(newId);
					o.setCustomId(oldValue.getCustomId());
					o.setName(oldValue.getName());
					o.setStatisticsDimensionId(oldValue.getStatisticsDimensionId());
					o.setValue(oldValue.getValue());
					details.remove(o);
				}else {
					deleteIds.add(oldValue.getId());
				}
			}
		}
		//删除
		if(!evUtil.listIsNullOrZero(deleteIds)) {
			dimensionValueMapper.deleteBatchIds(deleteIds);
		}
		//去重
		Set<DimensionValue> set = new HashSet<>();
		set.addAll(details);
		if(!evUtil.listIsNullOrZero(details)) {
			dimensionValueMapper.insertDimensionValue(set);
		}
	}

	@Override
	public Integer addstatisticsDimension(StatisticsDimension dimension) {
		Map<String,Object> map=new HashMap<String,Object>();
	    map.put("sequenceName", "seq_uuid");
		int id=sequenceMapper.nextVal(map);
		dimension.setId(id);
		statisticsDimensionMapper.insert(dimension);
		Integer statisticsDimensionId=dimension.getId();
		EntityWrapper<AnalyzeDimension> wrapper=new EntityWrapper<>();
		wrapper.where("customId={0}", dimension.getCustomId()).and("dimensionId={0}", dimension.getDimensionId());
		//删除相同的分析指标
		if(dimension.getType().equals(0)) {
			analyzeDimensionMapper.delete(wrapper);
		}
		return statisticsDimensionId;
	}

	@Override
	public Boolean checkName(StatisticsDimension statisticsDimension, int flag) {
		List<StatisticsDimension> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<StatisticsDimension> wrapper = new EntityWrapper<>();
			StatisticsDimension st = new StatisticsDimension();
			wrapper.setEntity(st);
			wrapper.where("name={0}", statisticsDimension.getName())
					.and("customId={0}", statisticsDimension.getCustomId()).ne("id", statisticsDimension.getId());
			list = statisticsDimensionMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("name", statisticsDimension.getName());
			nameMap.put("customId", statisticsDimension.getCustomId());
			list = statisticsDimensionMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public Boolean checkCustomName(CustomParam cs,int componentId,int flag) {
		List<Custom> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<Custom> wrapper = new EntityWrapper<>();
			Custom st = new Custom();
			wrapper.setEntity(st);
			wrapper.where("name={0}", cs.getName()).and("componentId={0}", componentId).ne("id", cs.getId());
			list = customMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("name", cs.getName());
			nameMap.put("componentId", componentId);
			list = customMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public String selectStep(int customId) {
		Custom cs = customMapper.selectById(customId);
		return cs.getStep();
	}

	@Override
	public void saveUuid(int statisticsDimensionId) {
		Integer useDictionary=statisticsDimensionMapper.selectById(statisticsDimensionId).getUseDictionary();
		List<Map<String, Object>> valuelist = statisticsDimensionMapper.selectUuidValue(statisticsDimensionId);
		String value = null;
		List<Ctuuid> uulist=new ArrayList<>();
		if (useDictionary.equals(0)) {
			for (Map<String, Object> map : valuelist) {
				if (Integer.parseInt(map.get("type").toString()) == 0) {
					value = "applicationid:" + map.get("applicationId") + ",customid:" + map.get("customId")
							+ ",dimension:" + map.get("ename") + ",value:" + map.get("value");
				} else {
					value = "applicationid:" + map.get("applicationId") + ",customid:" + map.get("customId")
							+ ",dimension:" + map.get("ename") + "_" + map.get("type") + ",value:" + map.get("value");
				}
				Ctuuid entity = new Ctuuid();
				entity.setValue(value);
				entity.setId(Long.valueOf(String.valueOf(map.get("id"))).longValue());
				uulist.add(entity);
				// ctuuidMapper.insert(entity);
			}
		}else {
			for (Map<String, Object> map : valuelist) {
				if (Integer.parseInt(map.get("type").toString()) == 0) {
					value = "applicationid:" + map.get("applicationId") + ",customid:" + map.get("customId")
							+ ",dimension:" + map.get("ename") + ",value:" + map.get("id");
				} else {
					value = "applicationid:" + map.get("applicationId") + ",customid:" + map.get("customId")
							+ ",dimension:" + map.get("ename") + "_" + map.get("type") + ",value:" + map.get("id");
				}
				
				Ctuuid entity = new Ctuuid();
				entity.setValue(value);
				entity.setId(Long.valueOf(String.valueOf(map.get("id"))).longValue());
				uulist.add(entity);
				// ctuuidMapper.insert(entity);
			}
		}
		while(uulist.size() > 30){
            List<Ctuuid> subList = uulist.subList(0, 30);
            ctuuidMapper.batchInsert(subList);
            subList.clear();
        }
        if(uulist.size()!=0){
        	ctuuidMapper.batchInsert(uulist);
        }
	}

	@Override
	public List<Map<String, Object>> selectDimensionByCustomIdAndType(int customId, int type) {
		List<Map<String, Object>>  list = new ArrayList<>();
		if(type >= -1 && type <= 1){
			list=customMapper.selectDimensionByCustomIdAndType(customId, type);
			if (type != 0) {
				for (Map<String, Object> x : list) {
					String ename = x.get("ename").toString();
					if (!x.get("dimensionType").toString().equals("0")) {
						ename = ename + "_" + x.get("dimensionType").toString();
					}
					x.put("ename", ename);
				}
			}
			return list;
		}else{
			return list;
		}
	}

	@Override
	public List<Map<String, Object>> selectDimensionOtherNameByCustomIdAndType(int customId, int type) {
		List<Map<String, Object>>  list = new ArrayList<>();
		if(type >= -1 && type <= 1){
			list=customMapper.selectDimensionOtherNameByCustomIdAndType(customId, type);
			for (Map<String, Object> x : list) {
				if (type != 0) {
					String ename = x.get("ename").toString();
					if (!x.get("dimensionType").toString().equals("0")) {
						//数据类型 : 0 普通 1 省份 2 地市 3 地区
						if (x.get("dimensionType").toString().equals("1")) {
							ename="province";
						}
						if (x.get("dimensionType").toString().equals("2")) {
							ename="city";
						}
						//ename = ename + "_" + x.get("dimensionType").toString();
						int count=dimensionValueMapper.getMappingCount(customId,Integer.parseInt(x.get("dimensionId").toString()));
						if(count>0) {
							x.put("translate", true);
						}else {
							x.put("translate", false);
						}
					}else {
						//分析维度
						if(x.get("type").toString().equals("0")) {
							int componentId=componentMapper.selectComponentByCustomId(customId).getId();
							int count=fieldMappingMapper.getMappingCount(componentId,Integer.parseInt(x.get("dimensionId").toString()));
							if(count>0) {
								x.put("translate", true);
							}else {
								x.put("translate", false);
							}
						}else {//统计维度翻译
							int count=dimensionValueMapper.getMappingCount(customId,Integer.parseInt(x.get("dimensionId").toString()));
							if(count>0) {
								x.put("translate", true);
							}else {
								x.put("translate", false);
							}
						}
					}
					x.put("ename", ename);
				}
				
			}
			return list;
		}else{
			return list;
		}
	}

	@Override
	public List<Map<String, Object>> selectCustomDimensionByCustomIdAndType(int customId, int type) {
		return customMapper.selectCustomDimensionByCustomIdAndType(customId, type);
	}

	@Override
	public List<Map<String, Object>> selectTraceDimensionByParam(Map<String, Object> param) {
		int type = evUtil.getMapIntValue(param, "type");
		List<Map<String, Object>> list = customMapper.selectTraceDimensionField(param);
		//查询是否有翻译值
		if(!evUtil.listIsNullOrZero(list)){
			List<Integer> dimensionIds = new ArrayList<>();
			for(Map<String, Object> map : list){
				dimensionIds.add(evUtil.getMapIntegerValue(map, "id"));
			}
			Map<String, Object> dimensionMap = new HashMap<>();
			dimensionMap.put("dimensionIds", dimensionIds);
			List<Map<String, Object>> dimensionValueCountList = new ArrayList<>();
			if(type == 0){
				dimensionMap.put("componentId", evUtil.getMapIntValue(param, "componentId"));
				dimensionValueCountList = fieldMappingMapper.getMappingCountByDimensionIds(dimensionMap);
			}else{
				dimensionMap.put("customId", evUtil.getMapIntValue(param, "customId"));
				dimensionValueCountList = dimensionValueMapper.getMappingCountByDimensionIds(dimensionMap);
				dimensionMap.put("componentId", evUtil.getMapIntValue(param, "componentId"));
				dimensionValueCountList.addAll(fieldMappingMapper.getMappingCountByDimensionIds(dimensionMap));
			}
			boolean flag;
			for(Map<String, Object> dimenMap : list){
				int dimensionId = evUtil.getMapIntValue(dimenMap, "id");
				if(evUtil.listIsNullOrZero(dimensionValueCountList)){
					dimenMap.put("translate", false);
				}else{
					flag = false;
					for(Map<String, Object> tmap : dimensionValueCountList){
						if(dimensionId == evUtil.getMapIntValue(tmap, "dimensionId")){
							flag = true;
							dimenMap.put("translate", true);
							break;
						}
					}
					if(!flag){
						dimenMap.put("translate", false);
					}
				}


			}
		}
		return list;
	}

	@Override
	public List<DictionaryParam> selectStatisticsDimensionValueDict(int id) {
		return customMapper.selectStatisticsDimensionValueDict(id);
	}

	@Override
	public boolean deleteCustomByComponentIds(List<Integer> ids) {
		if(evUtil.listIsNullOrZero(ids)){
			return true;
		}
		//ct_custom
		EntityWrapper<Custom> customEntityWrapper = new EntityWrapper<>();
		customEntityWrapper.in("componentId",ids);
		List<Custom> customList = customMapper.selectList(customEntityWrapper);
		customMapper.delete(customEntityWrapper);
		List<Integer> customIds = customList.stream().map(Custom::getId).collect(Collectors.toList());

		if(!evUtil.listIsNullOrZero(customIds)){
			//ct_filter_field
			EntityWrapper<FilterField> filterFieldEntityWrapper = new EntityWrapper<>();
			filterFieldEntityWrapper.in("customId",customIds);
			List<FilterField> filterFields = filterFieldMapper.selectList(filterFieldEntityWrapper);
			filterFieldMapper.delete(filterFieldEntityWrapper);
			//ct_filter_field_value
			if(!evUtil.listIsNullOrZero(filterFields)){
				EntityWrapper<FilterFieldValue> filterFieldValueEntityWrapper = new EntityWrapper<>();
				filterFieldValueEntityWrapper.in("filterFieldId",filterFields.stream().map(FilterField::getId).collect(Collectors.toList()) );
				filterFieldValueMapper.delete(filterFieldValueEntityWrapper);
			}

			//ct_success_field
			EntityWrapper<SuccessField> successFieldEntityWrapper = new EntityWrapper<>();
			successFieldEntityWrapper.in("customId", customIds);
			List<SuccessField> successFields = successFieldMapper.selectList(successFieldEntityWrapper);
			successFieldMapper.delete(successFieldEntityWrapper);
			//ct_success_field_value
			if(!evUtil.listIsNullOrZero(successFields)){
				EntityWrapper<SuccessFieldValue> successFieldValueEntityWrapper = new EntityWrapper<>();
				successFieldValueEntityWrapper.in("successFieldId", successFields.stream().map(SuccessField::getId).collect(Collectors.toList()));
				successFieldValueMapper.delete(successFieldValueEntityWrapper);
			}

			//ct_statistics_dimension
			EntityWrapper<StatisticsDimension> statisticsDimensionEntityWrapper = new EntityWrapper<>();
			statisticsDimensionEntityWrapper.in("customId", customIds);
			List<StatisticsDimension> statisticsDimensions = statisticsDimensionMapper.selectList(statisticsDimensionEntityWrapper);
			statisticsDimensionMapper.delete(statisticsDimensionEntityWrapper);
			//ct_dimension_value
			if(!evUtil.listIsNullOrZero(statisticsDimensions)){
				EntityWrapper<DimensionValue> dimensionValueEntityWrapper = new EntityWrapper<>();
				dimensionValueEntityWrapper.in("statisticsDimensionId",statisticsDimensions.stream().map(StatisticsDimension::getId).collect(Collectors.toList()));
				dimensionValueMapper.delete(dimensionValueEntityWrapper);
			}

			//ct_field_other_name
			EntityWrapper<CustomOtherName> fieldOtherNameEntityWrapper = new EntityWrapper<>();
			fieldOtherNameEntityWrapper.in("customId", customIds);
			customOtherNameMapper.delete(fieldOtherNameEntityWrapper);

			//ct_analyze_dimension
			EntityWrapper<AnalyzeDimension> analyzeDimensionEntityWrapper = new EntityWrapper<>();
			analyzeDimensionEntityWrapper.in("customId", customIds);
			analyzeDimensionMapper.delete(analyzeDimensionEntityWrapper);

			//ct_analyze_indicator
			EntityWrapper<AnalyzeIndicator> analyzeIndicatorEntityWrapper = new EntityWrapper<>();
			analyzeIndicatorEntityWrapper.in("customId", customIds);
			analyzeIndicatorMapper.delete(analyzeIndicatorEntityWrapper);

			//ct_uuid
			customIds.stream().forEach(x->{
				deleteUuid("customid:"+x);
			});
		}
		return true;
	}

	@Override
	public void deleteFilterField(List<Integer> ids) {
		Map<String, Object> col = new HashMap<String, Object>(1);
		EntityWrapper<FilterField> wrapper=new EntityWrapper<>();
		wrapper.in("dimensionId", ids);
		List<FilterField> FilterField = filterFieldMapper.selectList(wrapper);
		for (FilterField filterField2 : FilterField) {
			col.clear();
			col.put("filterFieldId", filterField2.getId());
			filterFieldValueMapper.deleteByMap(col);
		}
		filterFieldMapper.delete(wrapper);
	}

	@Override
	public void deletectStatisticsDimension(List<Integer> ids) {
		EntityWrapper<StatisticsDimension> wrapper=new EntityWrapper<>();
		wrapper.in("dimensionId", ids);
		List<StatisticsDimension> list=statisticsDimensionMapper.selectList(wrapper);
		for (StatisticsDimension statisticsDimension : list) {
			deleteDimension(statisticsDimension.getId());
		}
	}

	@Override
	public void deletectSuccessField(List<Integer> ids) {
		Map<String, Object> col = new HashMap<String, Object>(1);
		EntityWrapper<SuccessField> wrapper=new EntityWrapper<>();
		wrapper.in("dimensionId", ids);
		List<SuccessField> SuccessField = successFieldMapper.selectList(wrapper);
		for (SuccessField successField2 : SuccessField) {
			col.clear();
			col.put("successFieldId", successField2.getId());
			successFieldValueMapper.deleteByMap(col);
		}
		successFieldMapper.delete(wrapper);
	}

	@Override
	public List<Map<String, Object>> findCustomIndicatorByComponentId(int componentId) {
		Map<String, Object> param = new HashMap<>(1);
		param.put("componentId", componentId);
		return analyzeIndicatorMapper.selectAnalyzeIndicator(param);
	}

	@Override
	public List<Map<String, Object>> selectCustomTreeByAppId(int appId) {
		List<Map<String, Object>> data = customMapper.selectCustomAndComponentNameByAppId(appId);
		Map<Integer, List<Map<String,Object>>> componentMap = new LinkedHashMap<>();

		data.stream().forEach( x -> componentMap.computeIfAbsent(evUtil.getMapIntValue(x,"componentId"),
				ArrayList :: new).add(x));

		List<Map<String, Object>> result = new ArrayList<>();
		for (Map.Entry<Integer, List<Map<String,Object>>> entry : componentMap.entrySet()) {
			Integer componentId=entry.getKey();
			List<Map<String,Object>> children=entry.getValue();
			Map<String, Object> resultMap=new HashMap<>();
			if (!evUtil.listIsNullOrZero(children)) {
				resultMap.put("id", componentId);
				resultMap.put("title", children.get(0).get("componentName"));
				resultMap.put("type", 0);
				resultMap.put("expand", false);
				resultMap.put("dataSource", children.get(0).get("dataSource"));
			}
			List<Map<String,Object>> child=new ArrayList<>();
			for (Map<String, Object> map : children) {
				Map<String, Object> childMap=new HashMap<>();
				childMap.put("id", evUtil.getMapIntValue(map, "id"));
				childMap.put("title", evUtil.getMapStrValue(map, "name"));
				childMap.put("type", 1);
				childMap.put("expand", false);
				child.add(childMap);
			}
			resultMap.put("children", child);
			result.add(resultMap);
		}
		if(!evUtil.listIsNullOrZero(result)) {
			result.get(0).put("expand", true);
		}
		return result;
	}

	@Override
	public List<Custom> selectCustomBycomponentId(Map<String, Object> map) {
		int active=evUtil.getMapIntValue(map, "active");
		if(active==2) {
			/*Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("componentId", value)*/
			map.remove("active");
			return customMapper.selectByMap(map);
		}
		return customMapper.selectByMap(map);
	}

	@Override
	public List<Map<String, Object>> selectCommonDimension(Integer customId) {
		// TODO Auto-generated method stub
		return customMapper.selectCommonDimension(customId);
	}

	@Override
	public List<ApplicationTree> selectCustomTreeForAi(Map<String, Object> map) {
		int userId = evUtil.getMapIntValue(map, "userId");
		Map<String, Object> paramMap = new HashMap<>(4);
		paramMap.put("userId", userId);
		paramMap.put("active", 1);
		List<ApplicationTree> appTrees =  componentMapper.selectAppTree(paramMap);
		if(evUtil.listIsNullOrZero(appTrees)){
			return Collections.emptyList();
		}
		paramMap.clear();
		paramMap.put("ids",appTrees.stream().map(ApplicationTree::getId).collect(Collectors.toList()));
		List<Map<String, Object>> result = customMapper.selectCustomTreeForAi(paramMap);
		//construct tree nodes, not use hashSet
		Map<Integer, Boolean> monitorMap = new HashMap<>(3);
		Map<Integer, Boolean> componentMap = new HashMap<>(3);
		Map<Integer, Boolean> customMap = new HashMap<>(3);
		Integer applicationId, monitorId, componentId, customId, indicatorId;
		List<ApplicationTree> treeList = new ArrayList<>();
		for(Map<String, Object> dataMap : result){
			applicationId = evUtil.getMapIntegerValue(dataMap, "applicationId");
			monitorId = evUtil.getMapIntegerValue(dataMap,"monitorId");
			componentId = evUtil.getMapIntegerValue(dataMap, "componentId");
			customId = evUtil.getMapIntegerValue(dataMap, "customId");
			indicatorId = evUtil.getMapIntegerValue(dataMap, "indicatorId");
			if(monitorMap.get(monitorId) == null){
				monitorMap.put(monitorId,true);
				treeList.add(new ApplicationTree(evUtil.getMapStrValue(dataMap, "monitorName"),monitorId, applicationId,2,null));
			}
			if(componentMap.get(componentId) == null){
				componentMap.put(componentId,true);
				treeList.add(new ApplicationTree(evUtil.getMapStrValue(dataMap, "componentName"),componentId,monitorId,3,null));
			}
			if(customMap.get(customId) == null) {
				customMap.put(customId, true);
				treeList.add(new ApplicationTree(evUtil.getMapStrValue(dataMap, "customName"), customId, componentId, 4, null));
			}
			//indicator is leaf
			treeList.add(new ApplicationTree(evUtil.getMapStrValue(dataMap, "indicatorName"), indicatorId,customId, 5, null));
		}
//		appTrees.forEach(x->constructTree(x,treeList));
		appTrees.forEach(x->buildChildNodes(x,treeList));
		return appTrees;
	}
	/**
	 * 递归构造树形结构
	 * @param fatherTreeNode
	 * @param treeList
	 */
	public void constructTree(ApplicationTree fatherTreeNode, List<ApplicationTree> treeList){
		Iterator<ApplicationTree> iterator = treeList.iterator();
		while(iterator.hasNext()){
			ApplicationTree tree = iterator.next();
			if(tree.getParentId().equals(fatherTreeNode.getId())){
				List<ApplicationTree> children = new ArrayList<>();
				if(fatherTreeNode.getChildren() != null){
					children = fatherTreeNode.getChildren();
				}
				children.add(tree);
				iterator.remove();
				fatherTreeNode.setChildren(children);
				constructTree(tree, treeList);
//				System.out.println("size:"+treeList.size());
			}
		}

//		for(ApplicationTree tree : treeList){
//			if(tree.getParentId().equals(fatherTreeNode.getId())){
//				List<ApplicationTree> children = new ArrayList<>();
//				if(fatherTreeNode.getChildren() != null){
//					children = fatherTreeNode.getChildren();
//				}
//				children.add(tree);
//				fatherTreeNode.setChildren(children);
//				constructTree(tree, treeList);
//				System.out.println("########:"+(i++));
//			}
//		}
	}

	public void buildChildNodes(ApplicationTree node, List<ApplicationTree> nodes) {
		List<ApplicationTree> children = this.getChildNodes(node,nodes);
		if (!children.isEmpty()) {
			for (ApplicationTree child : children) {
				buildChildNodes(child, nodes);
			}
			node.setChildren(children);
		}
	}

	public List<ApplicationTree> getChildNodes(ApplicationTree pnode, List<ApplicationTree> nodes) {
		List<ApplicationTree> childNodes = new ArrayList<>();
//		for (ApplicationTree n : nodes) {
//			if (pnode.getId().equals(n.getParentId())) {
//				childNodes.add(n);
//			}
//		}
		Iterator<ApplicationTree> iterator = nodes.iterator();
		while(iterator.hasNext()){
			ApplicationTree tree =  iterator.next();
//			if (pnode.getId().equals(iterator.next().getParentId())) {
			if (pnode.getNodeType() != 5 && (pnode.getId().intValue() == tree.getParentId().intValue())) {
//				System.out.println(pnode.getId()+":"+tree.getParentId());
				childNodes.add(tree);
				iterator.remove();
			}
		}
		return childNodes;
	}



	@Override
	public List<Map<String, Object>> selectCustomIndicatorNameByCustomIds(Map<String, Object> map) {
		return customMapper.selectCustomIndicatorNameByCustomIds(map);
	}

	@Override
	public List<ApplicationTree> selectStatisticsDimensionForTree(Map<String, Object> map) {
		List<Map<String, Object>> list = customMapper.selectStatisticsDimensionForTree(map);
		List<ApplicationTree> rootTrees = new ArrayList<>();
		List<ApplicationTree> leafTrees = new ArrayList<>();
		Map<Integer, Integer>  rootMap = new HashMap<>();
		Integer staticId, dimensionId, dimensionValeId, type,customId;
		String dimensionName, dimensionValueName,dimensionVale, ename;
		int count;
		for(Map<String, Object> dataMap : list){
			staticId = evUtil.getMapIntegerValue(dataMap,"staticId");
			dimensionName = evUtil.getMapStrValue(dataMap,"dimensionName");
			dimensionValueName = evUtil.getMapStrValue(dataMap, "dimensionValueName");
			dimensionVale = evUtil.getMapStrValue(dataMap, "dimensionVale");
			ename = evUtil.getMapStrValue(dataMap, "ename");
			dimensionValeId = evUtil.getMapIntegerValue(dataMap, "dimensionValeId");
			customId = evUtil.getMapIntegerValue(dataMap, "customId");
			dimensionId = evUtil.getMapIntegerValue(dataMap, "dimensionId");
			type = evUtil.getMapIntegerValue(dataMap, "type");
			if(rootMap.get(staticId) == null){
				JSONObject extraData = new JSONObject();
				extraData.put("dimensionId", dimensionId);
				//数据类型 : 0 普通 1 省份 2 地市 3 地区
				if (type==1) {
					ename="province";
				}
				if (type==2) {
					ename="city";
				}
				extraData.put("ename", ename);
				count=dimensionValueMapper.getMappingCount(customId,dimensionId);
				if(count > 0) {
					extraData.put("translate", true);
				}else {
					extraData.put("translate", false);
				}
				rootTrees.add(new ApplicationTree(dimensionName,staticId,-1,1,extraData,new ArrayList<>()));
				rootMap.put(staticId,1);
			}
			if(dimensionValeId != null){
				JSONObject extraData = new JSONObject();
				extraData.put("dimensionId", dimensionId);
				//数据类型 : 0 普通 1 省份 2 地市 3 地区
				if (type==1) {
					ename="province";
				}
				if (type==2) {
					ename="city";
				}
				extraData.put("ename", ename);
				//extraData.put("ename", type == 0 ? ename : ename+"_"+type);
				extraData.put("dimensionValue", dimensionVale);
				leafTrees.add(new ApplicationTree(dimensionValueName,dimensionValeId, staticId, 2,extraData, new ArrayList<>()));
			}
		}
		rootTrees.forEach(x->constructTree(x,leafTrees));
		return rootTrees;
	}

}
