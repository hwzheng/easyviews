package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.alarm.param.req.AlarmGroupConfigParam;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.greattimes.ev.system.entity.Message;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 告警組表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
public interface AlarmGroupMapper extends BaseMapper<AlarmGroup> {
    /**
     * 查询告警组配置
     * @author NJ
     * @date 2019/1/8 10:11
     * @param map
     * @return java.util.List<com.greattimes.ev.bpm.alarm.param.req.AlarmGroupConfigParam>
     */
    List<AlarmGroupConfigParam> selectAlarmGroupConfig(Map<String, Object> map);

    /***
     * 保存告警信息
     * @author NJ
     * @date 2019/1/8 19:22
     * @param list
     * @return void
     */
    int insertBatchSysMessage(List<Message> list);

}
