package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.config.param.req.MultiScheduleDetailParam;
import com.greattimes.ev.bpm.entity.ScheduleRepeat;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 重复排期表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
public interface ScheduleRepeatMapper extends BaseMapper<ScheduleRepeat> {

	/**
	 * 重复排期列表查询
	 * @param applicationId
	 * @return
	 */
	List<MultiScheduleDetailParam> selectMultiSchedule(int applicationId);

}
