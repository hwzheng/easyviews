package com.greattimes.ev.bpm.mapper;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.DatasourceRelevantRelation;

/**
 * <p>
 * 数据源关联关系表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-16
 */
public interface DatasourceRelevantRelationMapper extends BaseMapper<DatasourceRelevantRelation> {

	List<Integer> selectByRelevantId(Integer id);

}
