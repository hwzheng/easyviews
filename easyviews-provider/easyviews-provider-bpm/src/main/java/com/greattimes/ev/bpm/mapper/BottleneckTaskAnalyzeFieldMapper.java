package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeField;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表任务分析字段表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface BottleneckTaskAnalyzeFieldMapper extends BaseMapper<BottleneckTaskAnalyzeField> {

	/**分析字段查询
	 * @param taskId
	 * @return
	 */
	List<BottleneckTaskAnalyzeField> selectAnalyzeField(int taskId);

	/**批量插入
	 * @param field
	 */
	void batchInsert(@Param("list") List<BottleneckTaskAnalyzeField> field);

}
