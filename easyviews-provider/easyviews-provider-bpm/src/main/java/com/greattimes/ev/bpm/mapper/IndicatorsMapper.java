package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.entity.Indicators;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 数据源指标表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface IndicatorsMapper extends BaseMapper<Indicators> {

    void batchInsert(List<Indicators> indicatorsList);


	/**
	 * 根据配置id查询指标
	 * @param id
	 * @return
	 */
	List<Indicators> getIndicators(int id);
	List<DictionaryParam> selectDsIndicatorDictByGroupId(@Param("id") int id);


}
