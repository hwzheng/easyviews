package com.greattimes.ev.bpm.service.notice.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.entity.Center;
import com.greattimes.ev.bpm.entity.Notice;
import com.greattimes.ev.bpm.mapper.CenterMapper;
import com.greattimes.ev.bpm.mapper.NoticeMapper;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.common.utils.evUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

/**
 * <p>
 * 通知表 服务实现类
 * </p>
 *
 * @author NJ
 * @since 2018-11-20
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

    @Autowired
    private NoticeMapper noticeMapper;
    @Autowired
    private CenterMapper centerMapper;

    @Override
    public boolean insertNoticeByTime(Notice notice) {
        if(notice.getCode() == null || notice.getType() == null
                || notice.getTimeStamp() == null){
            return false;
        }
        Long timeStamp = notice.getTimeStamp();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timeStamp);
        calendar.set(Calendar.MILLISECOND,0);
        calendar.set(Calendar.SECOND, 0);
        Notice insertNotice = new Notice();
        insertNotice.setCode(notice.getCode());
        insertNotice.setTime(calendar.getTime());
        insertNotice.setType(notice.getType());
        Notice iNotice = noticeMapper.selectOne(insertNotice);
        if(iNotice == null){
            insertNotice.setState(0);
            noticeMapper.insert(insertNotice);
        }
        return true;
    }

    @Override
    public boolean insertNoticeByParam(Integer code, String name, Integer type, Integer state) {
        //state 消费状态 0未消费 1消费中2 消费成功 3消费失败
        if(code == null || type == null ){
            return false;
        }
        EntityWrapper<Center> wrapper=new EntityWrapper<>();
        wrapper.where("active={0}", 1);
		List<Center> centerList = centerMapper.selectList(wrapper);
		if(evUtil.listIsNullOrZero(centerList)) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MILLISECOND,0);
        calendar.set(Calendar.SECOND, 0);
		for (Center center : centerList) {
			Notice insertNotice = new Notice();
	        insertNotice.setCode(code);
	        insertNotice.setTime(calendar.getTime());
	        insertNotice.setType(type);
	        insertNotice.setCenterId(center.getId());
	        Notice iNotice = noticeMapper.selectOne(insertNotice);
	        if(iNotice == null){
	            insertNotice.setState(state);
	            insertNotice.setName(name);
	            noticeMapper.insert(insertNotice);
	        }
		}
        return true;
    }
}
