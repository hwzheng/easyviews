package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ProtocolDimension;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 协议维度表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-02-14
 */
public interface ProtocolDimensionMapper extends BaseMapper<ProtocolDimension> {

	void batchInsert(@Param("ids")List<Integer> ids,@Param("protocolId") Integer protocolId);

}
