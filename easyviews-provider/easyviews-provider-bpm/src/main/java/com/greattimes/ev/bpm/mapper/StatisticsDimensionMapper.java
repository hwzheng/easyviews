package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.StatisticsDimension;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 自定义分析统计维度表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface StatisticsDimensionMapper extends BaseMapper<StatisticsDimension> {

	/**查询统计维度
	 * @param customId
	 * @return
	 */
	List<StatisticsDimension> selectStatisticsDimension(int customId);

	List<StatisticsDimension> selectStatisticsDimensionByCustomIds(List<Integer> list);

	/**
	 * 查询uuid的value值各字段
	 * @param statisticsDimensionId
	 * @return
	 */
	List<Map<String, Object>> selectUuidValue(int statisticsDimensionId);


	List<StatisticsDimension> selectStatisticsDimensionByComponentId(int componentId);


}
