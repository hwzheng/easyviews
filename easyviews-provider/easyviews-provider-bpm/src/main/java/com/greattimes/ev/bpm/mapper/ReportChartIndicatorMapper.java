package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartIndicator;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 智能报表图表指标表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartIndicatorMapper extends BaseMapper<ReportChartIndicator> {
    /**
     * 批量保存
     * @param list
     */
    void batchInsert(List<ReportChartIndicator> list);

    /**
     * 查询报表应用指标信息
     * @author NJ
     * @date 2019/4/18 16:00
     * @param map
     * @return java.util.List<com.greattimes.ev.bpm.entity.ReportChartIndicator>
     */
    List<ReportChartIndicator> selectReportChartIndicatorByMap(Map<String, Object> map);


}
