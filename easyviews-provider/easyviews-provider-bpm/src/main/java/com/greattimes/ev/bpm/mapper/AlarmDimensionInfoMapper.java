package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmDimensionInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-20
 */
public interface AlarmDimensionInfoMapper extends BaseMapper<AlarmDimensionInfo> {

}
