package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartLineColor;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表颜色 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2020-01-13
 */
public interface ReportChartLineColorMapper extends BaseMapper<ReportChartLineColor> {

    /**
     * 批量增加颜色
     * @param reportChartLineColors
     */
    void batchInsert(List<ReportChartLineColor> reportChartLineColors);

}
