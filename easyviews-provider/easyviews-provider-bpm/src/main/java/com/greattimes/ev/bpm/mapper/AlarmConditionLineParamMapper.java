package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmConditionLineParam;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 基线告警模板参数表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-11-13
 */
public interface AlarmConditionLineParamMapper extends BaseMapper<AlarmConditionLineParam> {

}
