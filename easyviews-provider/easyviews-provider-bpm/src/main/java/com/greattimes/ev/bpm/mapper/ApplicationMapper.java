package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.resp.ApplicationNameChain;
import com.greattimes.ev.bpm.config.param.resp.MonitorParam;
import com.greattimes.ev.bpm.entity.Application;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 应用表 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-18
 */
public interface ApplicationMapper extends BaseMapper<Application> {

	/**
	 * 查询
	 * 
	 * @param userId
	 * @return
	 */

    List<DictionaryParam> selectDictonaryAppByUserId(@Param("userId") int userId);

    List<ApplicationParam> selectApplicationByUserId(@Param("userId") int userId, @Param("active") int active);

	void batchUpdateSort(List<ApplicationParam> list);

	int getMaxAppSortByUserid(int userId);

	void updateBatchActive(@Param("userId") int userId, @Param("active")int active);

	void updateBatchAlarmStatus(@Param("userId") int userId, @Param("state")int state);

	int getMaxAppSort();

	void batchUpdateMonitorSort(List<MonitorParam> list);

	List<MonitorParam> selectMonitorList(@Param("applicationId") int id);

	List<DictionaryParam> selectMonitorDictionaryByAppId(@Param("applicationId") int id);

	List<DictionaryParam> selectComponentDictionaryByMonitorId(@Param("monitorId") int id);

	/**
	 * 查询应用和uuid
	 * @author cgc  
	 * @date 2018年9月18日  
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> selectAppUuidByUserId(int userId);

	/**
	 * 查询监控点和uuid
	 * @author cgc  
	 * @date 2018年9月18日  
	 * @param appId
	 * @return
	 */
	List<Map<String, Object>> selectMonitorUuidByAppId(@Param("applicationId") int appId);

	List<Map<String, Object>> findAppTreeData(@Param("userId")int userId, @Param("appIds") List<Integer> appIds);

	List<Map<String, Object>> selectComponentByMonitorId(@Param("monitorId")int monitorId);

	List<ApplicationNameChain> selectMonitorNameChain(@Param("ids") List<Integer> list);
	List<ApplicationNameChain> selectComponentNameChain(@Param("ids")List<Integer> list);
	List<ApplicationNameChain> selectIpNameChain(@Param("ids")List<Integer> list);
	List<ApplicationNameChain> selectPortNameChain(@Param("ids")List<Integer> list);
	List<ApplicationNameChain> selectCustomNameChain(@Param("ids")List<Integer> list);

}
