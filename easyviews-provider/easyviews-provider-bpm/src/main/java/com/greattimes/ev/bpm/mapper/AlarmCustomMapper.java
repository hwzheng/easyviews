package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmCustom;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 自定义告警 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-28
 */
public interface AlarmCustomMapper extends BaseMapper<AlarmCustom> {

}
