package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ConstraintField;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 数据源约束字段表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface ConstraintFieldMapper extends BaseMapper<ConstraintField> {
    void batchInsert(List<ConstraintField> list);
}
