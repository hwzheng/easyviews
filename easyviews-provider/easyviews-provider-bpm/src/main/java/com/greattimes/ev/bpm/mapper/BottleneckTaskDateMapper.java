package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskDate;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表任务数据日期表 （针对单次任务有效） Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface BottleneckTaskDateMapper extends BaseMapper<BottleneckTaskDate> {

}
