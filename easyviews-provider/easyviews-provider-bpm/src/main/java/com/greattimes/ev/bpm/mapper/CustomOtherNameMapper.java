package com.greattimes.ev.bpm.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.entity.CustomOtherName;

/**
 * <p>
 * 别名表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
public interface CustomOtherNameMapper extends BaseMapper<CustomOtherName> {

	/**
	 * 别名查询
	 * @param customId
	 * @param protocolId 
	 * @return
	 */
	List<DimensionParam> selectOtherName(@Param("customId") Integer customId, @Param("protocolId") Integer protocolId);

	/**
	 * 查询指标别名
	 * @param customId
	 * @return
	 */
	List<DimensionParam> selectIndicatorOtherName(int customId);

	/**
	 * 修改别名
	 * @param customId
	 * @param fieldId
	 * @param othername
	 */
	void updateOtherName(@Param("customId") int customId, @Param("fieldId") int fieldId,
			@Param("othername") String othername);

	/**
	 * 修改指标别名
	 * @param customId
	 * @param fieldId
	 * @param othername
	 */
	void updateAnalyzeIndicator(@Param("customId") int customId, @Param("fieldId") int fieldId,
			@Param("othername") String othername);

}
