package com.greattimes.ev.bpm.service.common.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.mapper.IndicatorMapper;
import com.greattimes.ev.bpm.service.common.IIndicatorService;
import com.greattimes.ev.common.utils.evUtil;


/**
 * 指标服务类
 */
@Service
public class IndicatorServiceImpl implements IIndicatorService {
	@Autowired
	private IndicatorMapper indicatorMapper;

	@Override
	public Map<Integer, String> getIndicatorName(List<Integer> indicator) {
		Map<Integer, String> map = new HashMap<>();
		if(!evUtil.listIsNullOrZero(indicator)) {
			List<Indicator> indicators = indicatorMapper.selectBatchIds(indicator);
			for (Indicator in : indicators) {
				map.put(in.getId(), in.getColumnName());
			}
		}
		return map;
	}

	@Override
	public Map<Integer, Map<String, Object>> getIndicatorMapByIds(List<Integer> ids) {
		Map<Integer, Map<String, Object>> result = new HashMap<>();
		if(!evUtil.listIsNullOrZero(ids)) {
			EntityWrapper<Indicator> entityWrapper = new EntityWrapper<>();
			entityWrapper.in("id", ids);
			List<Map<String, Object>> list = indicatorMapper.selectMaps(entityWrapper);
			for (Map<String, Object> indicatorMap : list) {
				result.put(Integer.parseInt(indicatorMap.get("id").toString()),indicatorMap);
			}
		}
		return result;
	}
}
