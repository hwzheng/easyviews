package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmFilter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警数据过滤表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
public interface AlarmFilterMapper extends BaseMapper<AlarmFilter> {

}
