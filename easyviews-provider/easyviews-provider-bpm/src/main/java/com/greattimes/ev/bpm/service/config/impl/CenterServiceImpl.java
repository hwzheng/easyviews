package com.greattimes.ev.bpm.service.config.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.resp.CenterProbeParam;
import com.greattimes.ev.bpm.config.param.resp.CenterProbePortParam;
import com.greattimes.ev.bpm.entity.Center;
import com.greattimes.ev.bpm.entity.CenterProbe;
import com.greattimes.ev.bpm.entity.CenterProbePort;
import com.greattimes.ev.bpm.mapper.CenterMapper;
import com.greattimes.ev.bpm.mapper.CenterProbeMapper;
import com.greattimes.ev.bpm.mapper.CenterProbePortMapper;
import com.greattimes.ev.bpm.service.config.ICenterService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 中心表 服务实现类
 * </p>
 *
 * @author Cgc
 * @since 2018-05-22
 */
@Service
public class CenterServiceImpl extends ServiceImpl<CenterMapper, Center> implements ICenterService {

	@Autowired
	private CenterMapper centerMapper;
	@Autowired
	private CenterProbeMapper centerProbeMapper;
	@Autowired
	private CenterProbePortMapper centerProbePortMapper;

	@Override
	public List<Center> getCenters(int active) {
		if(active == -1){
			//查询全部
			return centerMapper.selectList(null);
		}else{
			EntityWrapper<Center> centerEntityWrapper = new EntityWrapper<>();
			centerEntityWrapper.where("active={0}",active);
			return centerMapper.selectList(centerEntityWrapper);
		}
	}

	@Override
	public void save(Center centerinfo) {
		centerinfo.setProbeNum(0);
		centerMapper.insert(centerinfo);
	}

	@Override
	public Boolean checkCenterName(Center center, int flag) {
		Map<String, Object> nameMap = new HashMap<>();
		nameMap.put("name", center.getName());
		List<Center> centerlist = new ArrayList<>();
		if (flag == 1) {
			centerlist = centerMapper.selectByMap(nameMap);
		} else {
			nameMap.put("id", center.getId());
			centerlist = centerMapper.selectRepeatName(nameMap);
		}
		return evUtil.listIsNullOrZero(centerlist);
	}

	@Override
	public void update(Center centerinfo) {
		centerMapper.updateById(centerinfo);
	}

	@Override
	public void delete(Integer id) {
		centerMapper.deleteById(id);
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("centerId", id);
		/*List<CenterProbe> list=centerProbeMapper.selectByMap(columnMap);
		for (CenterProbe centerProbe : list) {
			Map<String, Object> map=new HashMap<>();
			map.put("probeId", centerProbe.getId());
			centerProbePortMapper.deleteByMap(map);
		}*/
		centerProbeMapper.deleteByMap(columnMap);
	}

	@Override
	public CenterProbe selectProbe(Integer id) {
		return centerProbeMapper.selectById(id);
	}

	@Override
	public List<CenterProbeParam> selectProbe() {
		return centerProbeMapper.selectProbe();
	}

	@Override
	public Boolean checkProbeName(CenterProbe centerprobe, int flag) {
		Map<String, Object> nameMap = new HashMap<>();
		nameMap.put("name", centerprobe.getName());
		List<CenterProbe> centerlist = new ArrayList<>();
		if (flag == 1) {
			centerlist = centerProbeMapper.selectByMap(nameMap);
		} else {
			EntityWrapper<CenterProbe> wrapper =new EntityWrapper<>();
			wrapper.where("name={0}",centerprobe.getName()).ne("id",centerprobe.getId() );
			centerlist = centerProbeMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(centerlist);
	}

	@Override
	public void save(CenterProbe centerprobe) {
		//最大端口号
		int portCode=0;
		int count=centerProbeMapper.selectCount(null);
		if(count>0) {
			portCode=centerProbeMapper.getMaxPortCode()+5;
		}
		centerprobe.setPortCode(portCode);
		centerProbeMapper.insert(centerprobe);
		//更新中心探针数
		int num=centerMapper.selectById(centerprobe.getCenterId()).getProbeNum();
		Center cen=new Center();
		cen.setId(centerprobe.getCenterId());
		cen.setProbeNum(num+1);
		centerMapper.updateById(cen);
	}

	@Override
	public void update(CenterProbe centerprobe) {
		centerProbeMapper.updateById(centerprobe);
	}

	@Override
	public void deleteProbe(Integer id) {
		/*Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("probeId", id);
		centerProbePortMapper.deleteByMap(columnMap);*/
		CenterProbe center=centerProbeMapper.selectById(id);
		centerProbeMapper.deleteById(id);
		// 更新中心探针数
		int num = centerMapper.selectById(center.getCenterId()).getProbeNum();
		Center cen = new Center();
		cen.setId(center.getCenterId());
		cen.setProbeNum(num-1);
		centerMapper.updateById(cen);
	}

	@Override
	public List<CenterProbePortParam> selectProbePort(Integer id) {
		return centerProbePortMapper.selectProbePort(id);
	}

	@Override
	public void save(CenterProbePort port) {
		centerProbePortMapper.insert(port);
	}

	@Override
	public void update(CenterProbePort port) {
		centerProbePortMapper.updateById(port);
	}

	@Override
	public void deletePort(Integer id) {
		centerProbePortMapper.deleteById(id);
	}
	@Override
	public boolean updateCenterActive(int id, int active) {
		Center center  = new Center();
		center.setId(id);
		center.setActive(active);
		centerMapper.updateById(center);
		return true;
	}

	@Override
	public Boolean checkProbeIp(CenterProbe centerprobe, int flag) {
		Map<String, Object> nameMap = new HashMap<>();
		nameMap.put("ip", centerprobe.getIp());
		nameMap.put("centerId", centerprobe.getCenterId());
		List<CenterProbe> centerlist = new ArrayList<>();
		if (flag == 1) {
			centerlist = centerProbeMapper.selectByMap(nameMap);
		} else {
			EntityWrapper<CenterProbe> wrapper =new EntityWrapper<>();
			wrapper.where("ip={0}",centerprobe.getIp()).and("centerId={0}",centerprobe.getCenterId()).ne("id",centerprobe.getId());
			centerlist = centerProbeMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(centerlist);
	}

	@Override
	public List<Integer> getProbeInfo(Integer centerId) {
		List<Integer> data=new ArrayList<>();
		if(null==centerId) {
			return data;
		}else {
			EntityWrapper<CenterProbe> wrapper=new EntityWrapper<>();
			CenterProbe entity=new CenterProbe();
			entity.setCenterId(centerId);
			wrapper.setEntity(entity);
			List<CenterProbe> probe=centerProbeMapper.selectList(wrapper);
			for (CenterProbe centerProbe : probe) {
				Integer portNum=centerProbe.getPortNum();
				Integer portCode=centerProbe.getPortCode();
				if(null!=portCode&&null!=portNum) {
					for (int i = 0; i < portNum; i++) {
						data.add(portCode+i);
					}
				}
			}
		}
		return data;
	}
}
