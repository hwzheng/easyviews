package com.greattimes.ev.bpm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.Notice;

/**
 * <p>
 * 通知表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-20
 */
public interface NoticeMapper extends BaseMapper<Notice> {

}
