package com.greattimes.ev.bpm.service.alarm.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.bpm.alarm.param.req.KafKaConfigParam;
import com.greattimes.ev.bpm.mapper.MessageMapper;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageSendService;
import com.greattimes.ev.bpm.thread.AlarmMessageSendBuild;
import com.greattimes.ev.bpm.thread.TimeOut;
import com.greattimes.ev.common.Thread.ThreadPool;
import com.greattimes.ev.common.utils.KafkaSendResultHandler;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Message;
import com.greattimes.ev.system.param.resp.KafKaAlarmMessageParam;
import com.greattimes.ev.system.service.IMessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @author CGC
 * @date 2019-11-22 15:21:47
 */
@Service
public class AlarmMassageSendServiceImpl implements IAlarmMassageSendService {

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private KafkaSendResultHandler producerListener;
    @Autowired
    private KafkaTemplate kafkaTemplate;
	@Autowired
	private MessageMapper messageMapper;
    /**
     * 告警状态-正常
     */
    private static final String NORMAL = "normal";
    /**
     * 告警状态-异常
     */
    private static final String EXCEPT = "except";
    /**
     * 告警源
     */
    private static final String DATASTR = "easyviews";
    /**
     * 告警类型
     */
    private static final String TYPE = "app";

    @Override
    public void messageSend(JSONObject jsonObject) {
		Integer size = jsonObject.getInteger("alarm_kafka_batch_size");
		String topic = jsonObject.getString("alarm_kafka_topic");
        JSONArray appIds = jsonObject.getJSONArray("appIds");
        JSONObject obj;
        Map<String, String> result = new HashMap<>();
        for(int i = 0; i < appIds.size(); i++){
            obj = appIds.getJSONObject(i);
            result.put(obj.getString("easyviews"), obj.getString("thirdParty"));
        }
        ExecutorService executor = ThreadPool.getExecutorService();
        KafKaConfigParam config=new KafKaConfigParam();
        try {
            //kafka发送信息
            config.setSize(size);
            config.setTopic(topic);
            config.setAppRelevant(result);
            AlarmMessageSendBuild task = new AlarmMessageSendBuild();
            task.setConfig(config);
            Future<Boolean> future =executor.submit(task);
            TimeOut to = new TimeOut();
            to.setFuture(future);
            to.setName("平安告警推送");
            executor.submit(to);
        } catch (Exception e) {
            // TODO: handle exception
            log.error("平安告警推送开启执行线程失败：>>"+e.toString());
        }

    }

    @Override
    public void updateMessageAndSend(KafKaConfigParam config) {
        //查询待发送数据
        EntityWrapper<Message> entityWrapper = new EntityWrapper<>();
        entityWrapper.where("kafkaState={0}", 0).le("sendType", 2);
        List<Message> messages = messageMapper.selectList(entityWrapper);
        if(evUtil.listIsNullOrZero(messages)){
            log.info("无相应的平安告警推送待发送！");
            return ;
        }

        //更新数据状态
        //状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
        List<Integer> ids=messages.stream().map(Message::getId).collect(Collectors.toList());
        messageMapper.updateBatchKafkaState(ids,1);

        log.info("##############alarmMessageListSize:"+ messages.size()+"##############");
        //log.info("##############message:{}", messages.toString());

        //==============================发送至kafka==============================
        sendAlarmMessageToKafka(messages, config.getAppRelevant(),config.getSize(), config.getTopic());
    }

    /**
     * kafka 发送消息
     * @param kafkaParams
     * @param appRelevant
     * @param batchNum
     * @param topic
     */
    private void sendAlarmMessageToKafka(List<Message> kafkaParams, Map<String,String > appRelevant,int batchNum, String topic) {
        if(!kafkaParams.isEmpty()){
            try {
                kafkaTemplate.setProducerListener(producerListener);
                while (kafkaParams.size() > batchNum) {
                    List<Message> subList = kafkaParams.subList(0, batchNum);
                    List<Integer> ids=new ArrayList<>();
                    List<KafKaAlarmMessageParam> result=new ArrayList<>();
                    generateMessage(subList, ids, appRelevant,result);
                    kafkaTemplate.send(topic, JSON.toJSONString(result, SerializerFeature.DisableCircularReferenceDetect));
					messageMapper.updateBatchKafkaState(ids,2);
                    subList.clear();
                }
                if (kafkaParams.size() != 0) {
                    List<Integer> ids=new ArrayList<>();
                    List<KafKaAlarmMessageParam> result=new ArrayList<>();
                    generateMessage(kafkaParams, ids,appRelevant, result);
                    kafkaTemplate.send(topic, JSON.toJSONString(result, SerializerFeature.DisableCircularReferenceDetect));
                    messageMapper.updateBatchKafkaState(ids,2);
                }
            } catch (Exception e) {
                e.printStackTrace();
                List<Integer> ids=kafkaParams.stream().map(Message::getId).collect(Collectors.toList());
                messageMapper.updateBatchKafkaState(ids,3);
                log.info("kafka指标推送异常：{}", e.getMessage());
            } finally {
            }
        }
    }

    private void generateMessage(List<Message> subList, List<Integer> ids,Map<String,String > appRelevant, List<KafKaAlarmMessageParam> result) {
        for (Message message : subList) {
            ids.add(message.getId());
            KafKaAlarmMessageParam param=new KafKaAlarmMessageParam();
            param.setLabels(message);
            Map<String,Object> annotations=new HashMap<>();
            annotations.put("summary",message.getMessage());
            param.setAnnotations(annotations);
            param.setStatus(message.getAlarmType()==1?NORMAL:EXCEPT);
            if (NORMAL.equals(param.getStatus())){
                param.setEndsAt(message.getTimestamp()/1000);
            }else{
                param.setStartsAt(message.getTimestamp()/1000);
            }
            param.setTrackId(message.getAlarmKey());
            param.setDsu("");
            //Integer subsysid= Integer.parseInt(appRelevant.get(message.getApplicationId().toString()));
            String relevantStr = appRelevant.get(message.getApplicationId().toString());
            param.setSubsysid(relevantStr!=null?Integer.parseInt(relevantStr):message.getApplicationId());
            Integer level=message.getAlarmLevel();
            String levelStr = null;
            switch (level){
                case 1:
                    levelStr="Critical";
                    break;
                case 2:
                    levelStr="Major";
                    break;
                case 3:
                    levelStr="Minor";
                    break;
                case 4:
                    levelStr="Warning";
                    break;
                case 5:
                    levelStr="Info";
                    break;
            }
            param.setLevel(levelStr);
            param.setDataSrc(DATASTR);
            param.setType(TYPE);
            param.setSummary(message.getMessage());
            param.setTimestamp(message.getTimestamp()/1000);
            param.setRuleId(message.getRuleId());
            result.add(param);
        }
    }


}
