package com.greattimes.ev.bpm.service.alarm.impl;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.bpm.alarm.param.req.AlarmGroupConfigParam;
import com.greattimes.ev.bpm.alarm.param.req.AlarmLevelParam;
import com.greattimes.ev.bpm.mapper.AlarmGroupMapper;
import com.greattimes.ev.bpm.mapper.AlarmInfoMapper;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageProducerService;
import com.greattimes.ev.bpm.thread.AlarmMsgBuild;
import com.greattimes.ev.bpm.thread.TimeOut;
import com.greattimes.ev.common.Thread.ThreadPool;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.system.entity.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2019/1/7 18:53
 */
@Service
public class AlarmMassageProducerServiceImpl implements IAlarmMassageProducerService{

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AlarmGroupMapper alarmGroupMapper;

    @Autowired
    private AlarmInfoMapper alarmInfoMapper;


//    private static Map<String, Long> alarmNumMap = new HashMap<>();

    /**
     * 2有告警，立即发送通知，相同告警不再发送，告警恢复后发送通知
     * 记录相同告警连续的时间戳
     * key:configId, value:[key:ruleId,value:时间戳]
     */
//    private static Map<Integer, Map<Integer, Long>> alarmNumMap = new ConcurrentHashMap<>();

    /**
     * 2有告警，立即发送通知，相同告警不再发送，告警恢复后发送通知
     * 告警恢复发送通知，记录恢复的告警信息
     *
     * key:configId, value:[key:ruleId,value:message]
     */
//    private static Map<Integer, Map<Integer, Message>> alarmMessageMap = new ConcurrentHashMap<>();

    private static Map<String, Long> alarmNumMap1 = new ConcurrentHashMap<>();

    private static Map<String, Message> alarmMessageMap1 = new ConcurrentHashMap<>();

    /**
     * 上次执行查询的开始时间
     */
    private static Map<Integer, Long> timeMap = new ConcurrentHashMap<>();

    @Override
    public int messageProducer(JSONObject jsonObject) {
        /**
         * 告警数据延迟时间(s)
         */
        int alarmDelay = jsonObject.getIntValue("alarmDelay");
        /**
         * interval bpm数据颗粒度(s) 数据产生的最小粒度
         */
        int bpmInterval = jsonObject.getIntValue("bpmInterval");
        /**
         * 告警发送信息压制阈值(单位:分钟) alarm_send_interval_threshold
         */
        int sendInterval = jsonObject.getIntValue("sendInterval");
        System.out.println("#####JSON###" + jsonObject.toJSONString());
        long time = System.currentTimeMillis() - alarmDelay * 1000;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(time));
        c.set(Calendar.MILLISECOND, 0);
        //默认时间格式化为bpm数据颗粒度的整数倍(前提:最小颗粒度<=60s)
        c.set(Calendar.SECOND, (c.get(Calendar.SECOND) / bpmInterval) * bpmInterval);


        Map<String, Object> map = new HashMap<>(3);
        map.put("time", c.getTimeInMillis());

        map.put("interval", bpmInterval);
        map.put("alarmSendInterval",sendInterval);
        map.put("active", 1);
        /**
         * 查询已经被激活的所有告警组信息,按照每个告警组分配线程任务
         */
        log.debug("查询告警组参数：{}", map);
        List<AlarmGroupConfigParam> groupConfigs = alarmGroupMapper.selectAlarmGroupConfig(map);

        log.debug("##############groupConfigParams:{}", groupConfigs.toString());

        ExecutorService executor = ThreadPool.getExecutorService();

        for(AlarmGroupConfigParam groupConfig : groupConfigs){
            try {
                /**
                 * 告警发送类型
                 * 1有告警，立即发送通知
                 * 2有告警，立即发送通知，相同告警不再发送，告警恢复后发送通知
                 * 3有告警，达到压缩间隔发送一次通知
                 * 4有告警，达到压缩条数发送一次通知
                 * 5有告警，达到压缩间隔和压缩条数发送一次通知
                 */
                AlarmMsgBuild task = new AlarmMsgBuild();
                task.setConfig(groupConfig);
                Future<Boolean> future =executor.submit(task);
                TimeOut to = new TimeOut();
                to.setFuture(future);
                to.setName(groupConfig.getId().toString());
                executor.submit(to);
            } catch (Exception e) {
                // TODO: handle exception
                log.error("告警信息定时任务开启执行线程失败：>>"+e.toString());
                continue;
            }
        }
        return 0;
    }

    //@Transactional(readOnly = false, isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRED)
    @Override
    public void insertTypeDistribute(AlarmGroupConfigParam alarmConfig) {
        /**
         * 告警发送类型
         * 1有告警，立即发送通知
         * 2有告警，立即发送通知，相同告警不再发送，告警恢复后发送通知
         * 3有告警，达到压缩间隔发送一次通知
         * 4有告警，达到压缩条数发送一次通知(一个计算单元内告警)
         * 5有告警，达到压缩间隔和压缩条数发送一次通知
         */
        switch (alarmConfig.getSendType()) {
            case 1:
                everyTime(alarmConfig);
                break;
            case 2:
                everyTimeByRule(alarmConfig);
                break;
            case 3:
                accumulateTime(alarmConfig);
                break;
            case 4:
                accumulateNum(alarmConfig);
                break;
            case 5:
                accumulateTime(alarmConfig);
                break;
        }
    }

    /**
     * 时间间隔压制消息
     * @author NJ
     * @date 2019/1/9 16:38
     * @param conf
     * @return void
     */
    public void accumulateTime(AlarmGroupConfigParam conf){
        //压缩时间是相应间隔的倍数,如果不是bpm颗粒度的倍数则 + 1
        int interval = conf.getInterval();
        int compressTime = conf.getCompressTime() * 60 * 1000;
        int space = compressTime % interval == 0 ? compressTime : (compressTime / interval + 1) * interval;
        conf.setStartTime(conf.getTime() - space);
        int id = conf.getId();
        if(timeMap.get(id) != null){
            long start = timeMap.get(id);
            if(conf.getTime() - start >= compressTime ){
                log.info("上次执行的时间:{}",DateUtils.longToDefaultDateTimeStr(conf.getTime()));
                setStatisticsMessage(conf);
            }else{
                log.info("跳过的执行的时间:{}",DateUtils.longToDefaultDateTimeStr(conf.getTime()));
                return ;
            }
        }else{
            log.info("上次执行的时间:{}",DateUtils.longToDefaultDateTimeStr(conf.getTime()));
            setStatisticsMessage(conf);
        }
    }

    /**
     * 4有告警，达到压缩条数发送一次通知(一个计算单元内告警)
     * @param config
     */
    public void accumulateNum(AlarmGroupConfigParam config){
        /**
         * 默认处理为一个计算单元内告警达到压缩条数
         */
        Long start = config.getTime() - config.getInterval() * 1000;
        config.setStartTime(start);
        setStatisticsMessage(config);
    }

    /**
     * 3有告警，达到压缩间隔发送一次通知
     * 4有告警，达到压缩条数发送一次通知(一个计算单元内告警)
     * 5有告警，达到压缩间隔和压缩条数发送一次通知
     * @param config
     * @return
     */
    public int setStatisticsMessage(AlarmGroupConfigParam config){
        Map<String, Object> map = new HashMap<>(3);
        map.put("start", config.getStartTime());
        map.put("end", config.getTime());
        map.put("levels", config.getAlarmLevels().stream().map(AlarmLevelParam::getLevel).collect(Collectors.toList()));
        Map<Integer, List<Integer>> typeIdMap = new HashMap<>();
        config.getAlarmData().stream().forEach(x->{
            if(typeIdMap.get(x.getType()) != null){
                typeIdMap.get(x.getType()).add(x.getAlarmId());
            }else{
                List<Integer> list = new ArrayList<>();
                list.add(x.getAlarmId());
                typeIdMap.put(x.getType(), list);
            }
        });
        map.put("typeIdMap", typeIdMap);
        map.put("interval", config.getInterval());
        List<Map<String, Object>> alarmInfoMaps;
        // 描述内容：应用在interval个计算单元 发生告警次数 n次
        List<Message> messageList = new ArrayList<>();
        int sendType = config.getSendType();
        if(sendType == 3) {
            log.debug("查询告警结果参数：>>{}", map);
            alarmInfoMaps = alarmInfoMapper.selectAlarmInfoByCompressTime(map);
            log.debug("查询告警结果：>>{}", alarmInfoMaps.toString());
            //3有告警，达到压缩间隔发送一次通知
            for(Map<String, Object> infoMap : alarmInfoMaps){
                messageList.addAll(this.setMsg(config, infoMap));
            }
            timeMap.put(config.getId(), config.getTime());
        }else if(sendType == 4 || sendType == 5){
            //4有告警，达到压缩条数发送一次通知
            //5有告警，达到压缩间隔和压缩条数发送一次通知
            map.put("compressNum", config.getCompressNum());
            log.debug("查询告警结果参数：>>{}", map);
            alarmInfoMaps = alarmInfoMapper.selectAlarmInfoByCompressTime(map);
            log.debug("查询告警结果：>>{}", alarmInfoMaps.toString());
            for(Map<String, Object> infoMap : alarmInfoMaps){
                messageList.addAll(this.setMsg(config, infoMap));
            }
            timeMap.put(config.getId(), config.getTime());
        }
        return batchInsert(messageList);
    }

    /**
     * 有告警，立即发送通知 type:1
     * @param conf
     * @return
     */
    public int everyTime(AlarmGroupConfigParam conf){
        /**
         * 开始时间为结束时间减去bpm数据颗粒度
         */
        conf.setStartTime(conf.getTime() - conf.getInterval() * 1000);
        return buildEveryTimeMessage(conf);
    }

    /**
     * 2有告警，立即发送通知，相同告警不再发送，告警恢复后发送通知
     * @param conf
     * @return
     */
    public int everyTimeByRule(AlarmGroupConfigParam conf){
        /**
         * 开始时间为结束时间减去bpm数据颗粒度
         */
        conf.setStartTime(conf.getTime() - conf.getInterval() * 1000);
        return buildEveryTimeMessageByRule(conf);
    }

    /**
     * 2有告警，立即发送通知，相同告警不再发送，告警恢复后发送通知
     * @param config
     * @return
     */
    public int buildEveryTimeMessageByRule(AlarmGroupConfigParam config){
        Map<String, Object> map = new HashMap<>(3);
        map.put("end", config.getTime());
        map.put("start", config.getStartTime());
        map.put("levels", config.getAlarmLevels().stream().map(AlarmLevelParam::getLevel).collect(Collectors.toList()));
        Map<Integer, List<Integer>> typeIdMap = new HashMap<>();
        config.getAlarmData().stream().forEach(x->{
            if(typeIdMap.get(x.getType()) == null){
                List<Integer> list = new ArrayList<>();
                list.add(x.getAlarmId());
                typeIdMap.put(x.getType(), list);
            }else{
                typeIdMap.get(x.getType()).add(x.getAlarmId());
            }
        });
        map.put("typeIdMap", typeIdMap);
        map.put("interval", config.getInterval());
        log.debug("查询告警结果参数：>>{}", map);
        List<Map<String, Object>> alarmInfoMaps = alarmInfoMapper.selectAlarmInfoByTime(map);
        log.debug("查询告警结果：>>{}", alarmInfoMaps.toString());
        List<Message> messageList = new ArrayList<>();
        //告警恢复
        log.info("####查询告警缓存alarmMessageMap:{}", alarmMessageMap1);
        log.info("####查询告警缓存alarmNumMap:{}", alarmNumMap1);

        /**
         * 告警恢复逻辑,使用上一次的alarmMessageMap与当前查询到的告警信息做比较
         * 如果当前告警信息没有，则说明alarmMessageMap中的告警已经恢复
         */
        String entryKey,preKey = config.getId()+"_",tempKey,groupKey;
        boolean isRecoverFlag;
        Integer ruleId;
        Iterator<Map.Entry<String, Message>> mapIterator = alarmMessageMap1.entrySet().iterator();
        /**
         * 从缓存map寻找查看是否重复
         */
        while (mapIterator.hasNext()){
            Map.Entry<String, Message> entry = mapIterator.next();
            entryKey = entry.getKey();
            if(entryKey.contains(preKey)){
                isRecoverFlag = true;
                for(Map<String, Object> info : alarmInfoMaps){
//                    ruleId = evUtil.getMapIntegerValue(info, "ruleId");
                    groupKey = evUtil.getMapStrValue(info,"groupKey");
//                    tempKey = preKey + ruleId + groupKey;
                    tempKey = preKey  + groupKey;
                    //重复的告警不发送
                    if(entryKey.equals(tempKey)){
                        isRecoverFlag = false;
                        //替换message为最新的告警数据
                        List<Message> messages = this.setMsg(config,info);
                        alarmMessageMap1.put(entryKey, messages.get(0));
                        break;
                    }
                }
                //告警的恢复 1、构造恢复短信 2、删除上一次key
                if(isRecoverFlag){
                    messageList.addAll(this.rebuildMsg(entry.getValue(), config));
                    alarmMessageMap1.remove(entryKey);
                    if(alarmNumMap1 != null){
                        alarmNumMap1.remove(entryKey);
                    }
                }
            }
        }

        //新的告警
        for(Map<String, Object> info : alarmInfoMaps){
//            tempKey = preKey + evUtil.getMapIntegerValue(info, "ruleId");
            tempKey = preKey + evUtil.getMapStrValue(info, "groupKey");
            //需要发送的新告警
            if(alarmMessageMap1.get(tempKey) == null){
                List<Message> messages = this.setMsg(config,info);
                if(evUtil.listIsNullOrZero(messages)){
                    continue;
                }
                Message message = messages.get(0);
                alarmMessageMap1.put(tempKey, message);
                messageList.addAll(messages);
                alarmNumMap1.put(tempKey, config.getTime());
            }else{
                if(alarmNumMap1.get(tempKey) != null
                        && config.getTime() - alarmNumMap1.get(tempKey) >= config.getAlarmSendInterval() * 60 * 1000){
                    //超过系统配置持续产生告警计算单元阈值,发送消息 alarm_send_interval_threshold
                    List<Message> messages = this.setMsg(config,info);
                    if(evUtil.listIsNullOrZero(messages)){
                        continue;
                    }
                    messageList.addAll(messages);
                    alarmNumMap1.put(tempKey, config.getTime());
                }
            }
        }
        return batchInsert(messageList);
    }

    /***
     * 有告警立即发送 type=1
     * @return
     */
    public int buildEveryTimeMessage(AlarmGroupConfigParam config){
        //告警数据+告警描述
        Map<String, Object> map = new HashMap<>(3);
        map.put("start", config.getStartTime());
        map.put("end", config.getTime());
        map.put("levels", config.getAlarmLevels().stream().map(AlarmLevelParam::getLevel).collect(Collectors.toList()));
        Map<Integer, List<Integer>> typeIds = new HashMap<>();
        config.getAlarmData().stream().forEach(x->{
            if(typeIds.get(x.getType()) == null){
                List<Integer> list = new ArrayList<>();
                list.add(x.getAlarmId());
                typeIds.put(x.getType(), list);
            }else{
                typeIds.get(x.getType()).add(x.getAlarmId());
            }
        });
        map.put("typeIdMap", typeIds);
        map.put("interval", config.getInterval());
        log.debug("查询告警结果参数：>>{}", map);
        List<Map<String, Object>> alarmInfoMaps = alarmInfoMapper.selectAlarmInfoByTime(map);
        log.debug("查询告警结果：>>{}", alarmInfoMaps.toString());
        List<Message> messageList = new ArrayList<>();
        for(Map<String, Object> infoMap : alarmInfoMaps){
            messageList.addAll(this.setMsg(config,infoMap));
        }
        int num = batchInsert(messageList);
        return num;
    }

    /**
     * 告警组与告警信息 1对1
     * @param config
     * @param infoMap
     * @return
     */
    public List<Message> setMsg(AlarmGroupConfigParam config, Map<String, Object> infoMap){
        List<Message> messageList = new ArrayList<>();
        List<String> typeList = config.getTypeList();
        if(evUtil.listIsNullOrZero(typeList)){
            return messageList;
        }
        //告警发送方式 1 邮件 ，2 短信 ，3app ，4外呼
        int sendType;
        for(String type : typeList){
            Message message = new Message();
            message.setMessage(evUtil.getMapStrValue(infoMap,"message"));
            message.setAlarmGroupId(config.getId().toString());
            message.setInsertTime(new Date());
            message.setTimestamp(config.getTime());
            message.setType(type);

            //新增5个参数 平安和辽宁现场需要
            sendType = config.getSendType();
            message.setSendType(sendType);
            message.setAlarmLevel(evUtil.getMapIntegerValue(infoMap,"alarmlevel"));
            message.setAlarmType(0);
            message.setApplicationId(evUtil.getMapIntegerValue(infoMap,"applicationId"));
            message.setEventState(0);
            Integer ruleId = evUtil.getMapIntegerValue(infoMap, "ruleId") == null ? -1 : evUtil.getMapIntegerValue(infoMap, "ruleId");
            message.setRuleId(ruleId);
            //非压缩方式，有唯一的告警标识
            if(sendType == 1 ||  sendType == 2){
                message.setAlarmKey(evUtil.getMapStrValue(infoMap, "alarmKey"));
            }
            message.setGroupKey(evUtil.getMapStrValue(infoMap,"groupKey"));
            messageList.add(message);
        }
        return messageList;
    }

    /**
     * 恢复告警消息的重构
     * @param message
     * @return
     */
    public List<Message> rebuildMsg(Message message, AlarmGroupConfigParam config){
        List<Message> messageList = new ArrayList<>();
        List<String> typeList = config.getTypeList();
        if(evUtil.listIsNullOrZero(typeList)){
            return messageList;
        }
        //告警发送方式 1 邮件 ，2 短信 ，3app ，4外呼
        int sendType;
        for(String type : typeList){
            Message newMessage = new Message();
            newMessage.setMessage("告警恢复(时间:"+ DateUtils.longToHHmmssStr(config.getTime())+") "+ message.getMessage());
            newMessage.setAlarmGroupId(message.getAlarmGroupId());
            newMessage.setInsertTime(new Date());
            newMessage.setTimestamp(config.getTime());
            newMessage.setType(type);
            //新增5个参数 平安和辽宁现场需要
            newMessage.setSendType(message.getSendType());
            newMessage.setAlarmLevel(message.getAlarmLevel());
            newMessage.setAlarmType(1);
            newMessage.setApplicationId(message.getApplicationId());
            newMessage.setEventState(0);
            Integer ruleId = message.getRuleId() == null ? -1 : message.getRuleId();
            newMessage.setRuleId(ruleId);
            sendType = message.getSendType();
            //非压缩方式，有唯一的告警标识
            if(sendType == 1 ||  sendType == 2){
                newMessage.setAlarmKey(message.getAlarmKey());
            }
            messageList.add(newMessage);
        }
        return messageList;
    }

    /**
     * 批量保存消息表
     * @param list
     * @return
     */
    public int batchInsert(List<Message> list){
        if(list.size() == 0 ){
            log.info("未查询到告警数据");
            return 0;
        }
        int num = 0;
        while (list.size() > 500) {
            List<Message> in = list.subList(0, 500);
            //执行insert
            num += alarmGroupMapper.insertBatchSysMessage(in);
            list.subList(0, 500).clear();
        }
        //执行insert
        num += alarmGroupMapper.insertBatchSysMessage(list);
        return num;
    }


    @Override
    public boolean clearMessageMap(int configId) {
        //告警状态的改变会清除全局map信息
        String entryKey,preKey = configId+"_";
        if(alarmMessageMap1 != null){
            Iterator<Map.Entry<String, Message>> mapIterator1 = alarmMessageMap1.entrySet().iterator();
            while (mapIterator1.hasNext()){
                Map.Entry<String, Message> entry = mapIterator1.next();
                entryKey = entry.getKey();
                if(entryKey.contains(preKey)){
                    mapIterator1.remove();
                }
            }
        }

        if(alarmNumMap1 != null){
            Iterator<Map.Entry<String, Long>> mapIterator2 = alarmNumMap1.entrySet().iterator();
            while (mapIterator2.hasNext()){
                Map.Entry<String, Long> entry = mapIterator2.next();
                entryKey = entry.getKey();
                if(entryKey.contains(preKey)){
                    mapIterator2.remove();
                }
            }
        }

        if(timeMap != null){
            timeMap.remove(configId);
        }
        log.info("告警组状态改变清除告警短信的全局信息成功！");
        return true;
    }

    @Override
    public boolean resetMessageMap() {
        alarmNumMap1 = new ConcurrentHashMap<>();
        alarmMessageMap1 = new ConcurrentHashMap<>();
        timeMap = new ConcurrentHashMap<>();
        log.info("定时任务状态改变重置告警短信的全局信息成功！");
        return true;
    }
}
