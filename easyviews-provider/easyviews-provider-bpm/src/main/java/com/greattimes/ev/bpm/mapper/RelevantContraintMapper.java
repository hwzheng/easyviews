package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.entity.RelevantContraint;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 数据源关联约束 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-17
 */
public interface RelevantContraintMapper extends BaseMapper<RelevantContraint> {

	void RelevantContraint(RelevantContraint re);

	/**上传插入
	 * @param details
	 */
	void insertRelevantContraint(List<com.greattimes.ev.bpm.entity.RelevantContraint> list);
	List<DictionaryParam> selectRelevantContraintDictByMap(Map<String, Object> map);

	/**
	 * 查询外部数据下组件配置的指标组
	 * @param componentId
	 * @return
	 */
	List<Integer> getGroupIds(int componentId);

	/**
	 * 获取不在对应IP下的关联数据
	 * @param set
	 * @param componentId
	 * @param indicatorGroupId
	 * @return
	 */
	List<RelevantContraint> getInvalid(@Param("set") Set<DataSourcesRelationParam> set,@Param("componentId") int componentId,
			@Param("indicatorGroupId") int indicatorGroupId);

	/**
	 * 获取不在对应IP+port下的关联数据
	 * @param set
	 * @param componentId
	 * @param indicatorGroupId
	 * @return
	 */
	List<RelevantContraint> getInvalidByIpAndPort(@Param("set") Set<DataSourcesRelationParam> set,@Param("componentId") int componentId,
			@Param("indicatorGroupId") int indicatorGroupId);
}
