package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmGroupUser;
import com.greattimes.ev.system.entity.User;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警组人员表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
public interface AlarmGroupUserMapper extends BaseMapper<AlarmGroupUser> {

	List<User> selectUser(@Param("id") Integer id);

	void batchInsert(@Param("list")List<Map<String, Object>> list,@Param("alarmGroupId") int alarmGroupId);

}
