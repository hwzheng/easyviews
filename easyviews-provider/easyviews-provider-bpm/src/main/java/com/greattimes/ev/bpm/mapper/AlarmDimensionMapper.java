package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmDimension;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警 维度 表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
public interface AlarmDimensionMapper extends BaseMapper<AlarmDimension> {

}
