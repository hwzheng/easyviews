package com.greattimes.ev.bpm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.ApplicationAlarm;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-20
 */
public interface ApplicationAlarmMapper extends BaseMapper<ApplicationAlarm> {


    /**
     * 查询有告警的应用id 
     * @param param
     * @author: nj
     * @date: 2020-08-17 15:24
     * @version: 0.0.1
     * @return: java.util.List<java.lang.Integer>
     */ 
    List<Integer> selectAppIds(Map<String, Object> param);


}
