package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmSend;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警信息发送配置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
public interface AlarmSendMapper extends BaseMapper<AlarmSend> {

}
