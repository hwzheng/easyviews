package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartDataIndicator;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.ReportChartIndicator;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 智能报表事件指标标表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2019-03-29
 */
public interface ReportChartDataIndicatorMapper extends BaseMapper<ReportChartDataIndicator> {
    /**
     * 批量保存
     * @param list
     */
    void batchInsert(List<ReportChartDataIndicator> list);

    /**
     * 根据chartId 查询事件的指标
     * @author NJ
     * @date 2019/4/18 16:51
     * @param map
     * @return java.util.List<com.greattimes.ev.bpm.entity.ReportChartIndicator>
     */
    List<ReportChartIndicator> selectReportChartDataIndicatorByMap(Map<String, Object> map);
}
