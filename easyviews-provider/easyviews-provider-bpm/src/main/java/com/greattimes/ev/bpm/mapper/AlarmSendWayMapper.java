package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmSendWay;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警发送方式 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
public interface AlarmSendWayMapper extends BaseMapper<AlarmSendWay> {

	void batchInsert(@Param("list")List<Integer> types,@Param("alarmGroupId") int alarmGroupId);

}
