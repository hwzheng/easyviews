package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ComponentIndicators;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组件指标表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface ComponentIndicatorsMapper extends BaseMapper<ComponentIndicators> {


	/**
	 * 查詢約束組
	 * @param componentId
	 * @return
	 */
	List<Map<String, Object>> selectContraintGroup(Integer componentId);

}
