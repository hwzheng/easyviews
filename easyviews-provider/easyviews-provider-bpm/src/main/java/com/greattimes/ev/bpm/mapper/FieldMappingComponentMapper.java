package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.FieldMappingComponent;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 维度字段映射配置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
public interface FieldMappingComponentMapper extends BaseMapper<FieldMappingComponent> {

}
