package com.greattimes.ev.bpm.service.config.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.config.param.req.ComponentDimensionParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.config.param.resp.DimensionLableParam;
import com.greattimes.ev.bpm.config.param.resp.FieldMappingDetailParam;
import com.greattimes.ev.bpm.config.param.resp.FieldMappingParam;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.ComponentDimension;
import com.greattimes.ev.bpm.entity.FieldMapping;
import com.greattimes.ev.bpm.entity.FieldMappingCnf;
import com.greattimes.ev.bpm.entity.FieldMappingComponent;
import com.greattimes.ev.bpm.entity.FieldOtherName;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.mapper.ComponentDimensionMapper;
import com.greattimes.ev.bpm.mapper.ComponentMapper;
import com.greattimes.ev.bpm.mapper.FieldMappingCnfMapper;
import com.greattimes.ev.bpm.mapper.FieldMappingComponentMapper;
import com.greattimes.ev.bpm.mapper.FieldMappingMapper;
import com.greattimes.ev.bpm.mapper.FieldOtherNameMapper;
import com.greattimes.ev.bpm.mapper.ProtocolFieldMapper;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.config.IComponentDimensionService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 组件分析维度 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
@Service
public class ComponentDimensionServiceImpl extends ServiceImpl<ComponentDimensionMapper, ComponentDimension>
		implements IComponentDimensionService {

	@Autowired
	private ProtocolFieldMapper protocolFieldMapper;
	@Autowired
	private ComponentDimensionMapper componentDimensionMapper;
	@Autowired
	private FieldOtherNameMapper fieldOthernameMapper;
	@Autowired
	private FieldMappingCnfMapper fieldMappingCnfMapper;
	@Autowired
	private FieldMappingComponentMapper fieldMappingComponentMapper;
	@Autowired
	private FieldMappingMapper fieldMappingMapper;
	@Autowired
	private ComponentMapper componentMapper;
	@Autowired
	private IDeleteRuleService deleteRuleService;
	
	@Override
	public List<Map<String, Object>> selectDimension(int componentId) {
		return protocolFieldMapper.selectDimension(componentId);
	}

	@Override
	public List<Map<String, Object>> selectDimensionNameBycomponentId(int componentId) {
		List<Map<String, Object>> list=protocolFieldMapper.selectDimensionNameBycomponentId(componentId);
		for (Map<String, Object> map : list) {
			int count=fieldMappingMapper.getMappingCount(componentId,Integer.parseInt(map.get("fieldId").toString()));
			if(count>0) {
				map.put("translate", true);
			}else {
				map.put("translate", false);
			}
		}
		return list;
	}

	@Override
	public void saveComponentDimens(ComponentDimensionParam param) {
		int componentId = param.getComponentId();
		Map<String, Object> map = new HashMap<>(1);
		map.put("componentId", componentId);
		List<ComponentDimension> componentDimensions = componentDimensionMapper.selectByMap(map);
		List<Integer> originDimension = componentDimensions.stream().map(ComponentDimension::getDimensionId).collect(Collectors.toList());
		componentDimensionMapper.deleteByMap(map);
		List<DimensionParam> dimensionParam = null;
		dimensionParam = param.getDimension();
		List<Integer> newDimensionIds = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(dimensionParam)) {
			for (DimensionParam dimension : dimensionParam) {
				ComponentDimension componentdimension = new ComponentDimension();
				componentdimension.setComponentId(componentId);
				componentdimension.setDimensionId(dimension.getFieldId());
				componentdimension.setSort(dimension.getSort());
				componentDimensionMapper.insert(componentdimension);
				newDimensionIds.add(dimension.getFieldId());
			}
		}
		
		
		//组件维度变化删除相应的告警
		List<Integer> deleteDimensionIds = new ArrayList<>();
		deleteDimensionIds.addAll(originDimension);
		originDimension.retainAll(newDimensionIds);
		deleteDimensionIds.removeAll(originDimension);

		deleteRuleService.deleteDimension(deleteDimensionIds,0);

	}

	@Override
	public List<DimensionParam> selectOtherName(int componentId) {
		Component com = componentMapper.selectById(componentId);
		Integer protocolId = com.getPrototcolId();
		return protocolFieldMapper.selectOtherName(componentId,protocolId);
	}

	@Override
	public void saveOtherName(DimensionParam param) {
		int componentId = param.getComponentId();
		int fieldId = param.getFieldId();
		Map<String, Object> map = new HashMap<>(2);
		map.put("fieldId", fieldId);
		map.put("componentId", componentId);
		fieldOthernameMapper.deleteByMap(map);
		FieldOtherName field = new FieldOtherName();
		if (!(null == param.getOtherName() || "".equals(param.getOtherName()))) {
			field.setComponentId(componentId);
			field.setFieldId(param.getFieldId());
			field.setOthername(param.getOtherName());
			fieldOthernameMapper.insert(field);
		}
	}

	@Override
	public List<FieldMappingParam> selectFieldMapping(int applicationId) {
		List<Map<String, Object>> mappingList = fieldMappingCnfMapper.selectFieldMapping(applicationId);

		List<FieldMappingParam> FieldMappingList = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(mappingList)) {
			for (Map<String, Object> map : mappingList) {
				FieldMappingParam fieldmappingparam = new FieldMappingParam();
				fieldmappingparam.setId(evUtil.getMapIntValue(map, "id"));
				if (map.get("updateTime") != null) {
					fieldmappingparam.setUpdateTime((Date) map.get("updateTime"));
				}
				fieldmappingparam.setApplicationId(applicationId);
				fieldmappingparam.setUpdateUser(evUtil.getMapStrValue(map, "updateUser"));
				List<FieldMappingDetailParam> detailList = new ArrayList<>();
				// 获取componentId,componentName,dimensionId,dimensionName拆分为数组
				String[] componentIds = evUtil.getMapStrValue(map, "componentId").split(",");
				String[] componentNames = evUtil.getMapStrValue(map, "componentName").split(",");
				String[] dimensionIds = evUtil.getMapStrValue(map, "dimensionId").split(",");
				String[] dimensionNames = evUtil.getMapStrValue(map, "dimensionName").split(",");
				for (int i = 0; i < dimensionNames.length; i++) {
					FieldMappingDetailParam detail = new FieldMappingDetailParam();
					if (componentIds[i] != null) {
						detail.setComponentId(Integer.valueOf(componentIds[i]));
					}
					detail.setComponentName(componentNames[i]);
					if (dimensionIds[i] != null) {
						detail.setDimensionId(Integer.valueOf(dimensionIds[i]));
					}
					detail.setDimensionName(dimensionNames[i]);
					detailList.add(detail);
				}
				fieldmappingparam.setDetail(detailList);
				FieldMappingList.add(fieldmappingparam);
			}
		}
		return FieldMappingList;
	}

	@Override
	public void addFieldMapping(Map<String, Object> map, String loginName) {
		FieldMappingCnf fieldmappingcnf = new FieldMappingCnf();
		fieldmappingcnf.setApplicationId(evUtil.getMapIntValue(map, "applicationId"));
		fieldmappingcnf.setUpdateUser(loginName);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		fieldmappingcnf.setUpdateTime(time);
		fieldMappingCnfMapper.insert(fieldmappingcnf);
		List<Integer> componentIds = (List<Integer>) map.get("componentIds");
		List<Integer> dimensionIds = (List<Integer>) map.get("dimensionIds");
		if (null != componentIds && null != dimensionIds) {
			for (int i = 0; i < dimensionIds.size(); i++) {
				FieldMappingComponent fieldmappingcomponent = new FieldMappingComponent();
				fieldmappingcomponent.setComponentId(componentIds.get(i));
				fieldmappingcomponent.setDimensionId(dimensionIds.get(i));
				fieldmappingcomponent.setConfigId(fieldmappingcnf.getId());
				fieldMappingComponentMapper.insert(fieldmappingcomponent);
			}
		}
	}

	@Override
	public void editFieldMapping(Map<String, Object> map, String loginName) {
		FieldMappingCnf fieldmappingcnf = new FieldMappingCnf();
		fieldmappingcnf.setId(evUtil.getMapIntValue(map, "id"));
		fieldmappingcnf.setApplicationId(evUtil.getMapIntValue(map, "applicationId"));
		fieldmappingcnf.setUpdateUser(loginName);
		Timestamp time = new Timestamp(System.currentTimeMillis());
		fieldmappingcnf.setUpdateTime(time);
		fieldMappingCnfMapper.updateById(fieldmappingcnf);
		// 删除原有映射
		Map<String, Object> colmap = new HashMap<String, Object>();
		colmap.put("configId", evUtil.getMapIntValue(map, "id"));
		fieldMappingComponentMapper.deleteByMap(colmap);
		// 获取组件和维度数组
		List<Integer> componentIds = (List<Integer>) map.get("componentIds");
		List<Integer> dimensionIds = (List<Integer>) map.get("dimensionIds");
		if (null != componentIds && null != dimensionIds) {
			for (int i = 0; i < dimensionIds.size(); i++) {
				FieldMappingComponent fieldmappingcomponent = new FieldMappingComponent();
				fieldmappingcomponent.setComponentId(componentIds.get(i));
				fieldmappingcomponent.setDimensionId(dimensionIds.get(i));
				fieldmappingcomponent.setConfigId(fieldmappingcnf.getId());
				fieldMappingComponentMapper.insert(fieldmappingcomponent);
			}
		}
	}

	@Override
	public void deleteFieldMapping(Integer id) {
		fieldMappingCnfMapper.deleteById(id);
		Map<String, Object> colmap = new HashMap<String, Object>();
		colmap.put("configId", id);
		fieldMappingComponentMapper.deleteByMap(colmap);
		fieldMappingMapper.deleteByMap(colmap);
	}

	@Override
	public List<FieldMapping> selectMapping(Integer configId) {
		Map<String, Object> colmap = new HashMap<String, Object>();
		colmap.put("configId", configId);
		return fieldMappingMapper.selectByMap(colmap);
	}

	@Override
	public void insertFieldMapping(List<FieldMapping> details, String loginName, int configId, int applicationId) {
		Map<String, Object> columnMap = new HashMap<String, Object>();
		columnMap.put("configId", configId);
		fieldMappingMapper.deleteByMap(columnMap);
		fieldMappingMapper.insertFieldMapping(details);
		EntityWrapper<FieldMappingCnf> ew = new EntityWrapper<>();
		FieldMappingCnf t = new FieldMappingCnf();
		t.setId(configId);
		ew.setEntity(t);
		FieldMappingCnf entity = new FieldMappingCnf();
		entity.setUpdateUser(loginName);
		entity.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		entity.setApplicationId(applicationId);
		fieldMappingCnfMapper.update(entity, ew);
	}

	@Override
	public List<ProtocolField> selectDimensionByComponentId(int componentId) {
		return protocolFieldMapper.selectDimensionByComponentId(componentId);
	}

	@Override
	public List<DimensionLableParam> loadSelect(int applicationId) {
		List<DimensionLableParam> DimensionLableParamList = new ArrayList<>();
		// 根据applicationId查询组件
		List<DictionaryParam> DictionaryParamlist = componentMapper.selectComponentNameAndIdByBusinessId(applicationId);
		for (DictionaryParam dictionaryParam : DictionaryParamlist) {
			DimensionLableParam dimensionLableParam = new DimensionLableParam();
			dimensionLableParam.setId((Integer) dictionaryParam.getValue());
			dimensionLableParam.setName(dictionaryParam.getLabel());
			// 根据compontId查询维度
			List<ProtocolField> ProtocolFieldList = protocolFieldMapper.selectDimensionWithOtherName((Integer) dictionaryParam.getValue());
			dimensionLableParam.setDimension(ProtocolFieldList);
			DimensionLableParamList.add(dimensionLableParam);
		}
		return DimensionLableParamList;
	}

	@Override
	public void deleteFieldMapping(List<Integer> ids) {
		EntityWrapper<FieldMappingComponent> wrapper=new EntityWrapper<>();
		wrapper.in("dimensionId", ids);
		List<FieldMappingComponent> field = fieldMappingComponentMapper.selectList(wrapper);
		for (FieldMappingComponent fieldMappingComponent : field) {
			deleteFieldMapping(fieldMappingComponent.getConfigId());
		}
	}

	@Override
	public Map<Integer, List<Map<String, Object>>> selectDimensionNameByIds(List<Integer> componentIds) {
		Map<Integer, List<Map<String, Object>>> resultMap = new HashMap<>();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("ids", componentIds);
		List<Map<String, Object>> data = protocolFieldMapper.selectDimensionNameByIds(paramMap);
		int componentId;
		if(!evUtil.listIsNullOrZero(data)){
			for(Map<String, Object> dataMap : data){
				componentId = evUtil.getMapIntegerValue(dataMap,"componentId");
				if(resultMap.get(componentId) != null){
					resultMap.get(componentId).add(dataMap);
				}else{
					List<Map<String, Object>> list = new ArrayList<>();
					list.add(dataMap);
					resultMap.put(componentId,list);
				}
			}
		}
		return resultMap;
	}

	@Override
	public Map<Integer, Map<String, Map<String, Object>>> selectDimensionNameMapByIds(List<Integer> componentIds) {
		Map<Integer, Map<String, Map<String, Object>>> resultMap = new HashMap<>();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("ids", componentIds);
		List<Map<String, Object>> data = protocolFieldMapper.selectDimensionNameByIds(paramMap);

		data.stream().forEach( x->
			resultMap.computeIfAbsent(evUtil.getMapIntegerValue(x,"componentId"), HashMap::new)
					.put(evUtil.getMapStrValue(x, "ename"), x));

		return resultMap;
	}

	@Override
	public void updateSort(List<DimensionParam> list) {
		while (list.size() > 30) {
			List<DimensionParam> subList = list.subList(0, 30);
			protocolFieldMapper.batchUpdateSort(subList);
			subList.clear();
		}
		if (list.size() != 0) {
			protocolFieldMapper.batchUpdateSort(list);
		}
	}

	@Override
	public int selectMaxSortByComponentId(int componentId) {
		return componentDimensionMapper.getMaxSortComponentDimension(componentId);
	}
}
