package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomParam;
import com.greattimes.ev.bpm.entity.Custom;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 自定义分析配置 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
public interface CustomMapper extends BaseMapper<Custom> {

	/**
	 * 自定义配置列表查询
	 * @param list 
	 * @return
	 */
	List<CustomParam> selectCustom(List<DictionaryParam> list);

	/**
	 * 自定义配置列表排序
	 * @param list
	 */
	void batchUpdateSort(List<CustomParam> list);

	/**
	 * 自定义配置批量激活
	 * @param custom
	 */
	void batchUpdateActive(List<CustomParam> custom);

	/**
	 * 自定义配置批量告警状态修改
	 * @param custom
	 */
	void batchUpdateState(List<CustomParam> custom);

	/**
	 * 基本配置修改
	 * @param entity
	 */
	void updateCustom(Custom entity);

	/**
	 * 添加成功条件时修改
	 * @param id
	 */
	void updateSuccess(Integer id);


	List<Map<String, Object>> selectDimensionByCustomIdAndType(@Param("customId") int customId, @Param("type") int type);

	/**
	 * 根据id查询自定义维度(统计+分析)
	 * @author NJ
	 * @date 2019/4/17 15:38
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectDimensionByCustomIdAndTypeByIds(Map<String,Object> map);

	List<Map<String, Object>> selectDimensionOtherNameByCustomIdAndType(@Param("customId") int customId, @Param("type") int type);

	List<Map<String, Object>> selectCustomDimensionByCustomIdAndType(@Param("customId") int customId, @Param("type") int type);


	List<Map<String, Object>> selectTraceDimensionField(Map<String, Object> param);

	List<DictionaryParam> selectStatisticsDimensionValueDict(int id);

	List<Map<String, Object>> selectCustomAndComponentNameByAppId(int appId);

	/**查询分析维度和类型为普通的统计维度
	 * @param customId
	 * @return
	 */
	List<Map<String, Object>> selectCommonDimension(@Param("customId") Integer customId);

	/**添加筛选条件时修改
	 * @param id
	 */
	void updateFilter(Integer id);

	/**获取事件及分析维度个数
	 * @param componentId
	 * @return
	 */
	List<Map<String, Object>> getCustomAndAnalyzeCount(@Param("componentId")int componentId);

	/**
	 * 事件树--智能报表
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectCustomTreeForAi(Map<String, Object> map);

	/**
	 * 根据事件id查询事件的指标名称
	 * @author NJ
	 * @date 2019/4/8 8:50
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectCustomIndicatorNameByCustomIds(Map<String, Object> map);


	/**
	 * 根据事件id查询事件维度
	 * @author NJ
	 * @date 2019/4/8 16:33
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectStatisticsDimensionForTree(Map<String, Object> map);

	/**
	 * 根据ename查询统计维度值得翻译
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 * @throw
	 * @author NJ
	 * @date 2019-04-27 14:15
	*/
	List<Map<String, Object>> selectStatisticsDimensionByEnames(Map<String, Object> map);

}
