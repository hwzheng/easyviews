package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ScheduleRepeatTimes;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 重复排期时间表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
public interface ScheduleRepeatTimesMapper extends BaseMapper<ScheduleRepeatTimes> {

}
