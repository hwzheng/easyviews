package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.PathLine;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 路径图连线表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-11
 */
public interface PathLineMapper extends BaseMapper<PathLine> {
    void batchInsert(List<PathLine> list);
}
