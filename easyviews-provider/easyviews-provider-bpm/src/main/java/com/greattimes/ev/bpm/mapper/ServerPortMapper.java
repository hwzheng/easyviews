package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ServerPort;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 组件IP表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface ServerPortMapper extends BaseMapper<ServerPort> {
    void batchInsert(List<ServerPort> list);
    void batchDeleteByserverGroupIds(List<Integer> list);
    void batchDelete(List<ServerPort> list);
}
