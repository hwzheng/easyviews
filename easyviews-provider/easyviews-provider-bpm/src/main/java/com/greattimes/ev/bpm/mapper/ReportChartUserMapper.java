package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表图表所属人员表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface ReportChartUserMapper extends BaseMapper<ReportChartUser> {
    /**
     * 批量保存
     * @param list
     */
    void batchInsert(List<ReportChartUser> list);
}
