package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportChartTableField;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 智能报表表格展示字段 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2019-05-28
 */
public interface ReportChartTableFieldMapper extends BaseMapper<ReportChartTableField> {
    void batchInsert(List<ReportChartTableField> reportChartTableFieldList);
}
