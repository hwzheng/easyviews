package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Server;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 组件ip/port组 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-18
 */
public interface ServerMapper extends BaseMapper<Server> {
    void batchInsert(List<Server> list);
	void batchDeleteByIds(List<Integer> list);


	/**查询port个数
	 * @param ip
	 * @param centerId 
	 * @param port 
	 * @return
	 */
	List<Map<String, Object>> getPortCount(@Param("ip") String ip,@Param("centerId") Integer centerId,@Param("port") String port);
	/**
	 * 查询ip个数
	 * @author CGC
	 * @param ip
	 * @param centerId
	 * @return
	 */
	List<Map<String, Object>> getIpCount(@Param("ip")String ip, @Param("centerId")Integer centerId);
}
