package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ReportMailUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能报表邮件发送表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-03-27
 */
public interface ReportMailUserMapper extends BaseMapper<ReportMailUser> {

}
