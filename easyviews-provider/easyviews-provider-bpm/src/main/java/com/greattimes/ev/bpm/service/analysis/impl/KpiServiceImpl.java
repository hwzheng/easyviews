package com.greattimes.ev.bpm.service.analysis.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.greattimes.ev.bpm.config.param.req.ExtendChooseDetailParam;
import com.greattimes.ev.bpm.config.param.req.ExtendChooseParam;
import com.greattimes.ev.bpm.config.param.resp.ExtendChooseDataParam;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;
import com.greattimes.ev.bpm.entity.DsUuid;
import com.greattimes.ev.bpm.entity.RelevantContraint;
import com.greattimes.ev.bpm.mapper.DatasourceRelevantMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMapper;
import com.greattimes.ev.bpm.mapper.IndicatorGroupMapper;
import com.greattimes.ev.bpm.mapper.RelevantContraintMapper;
import com.greattimes.ev.bpm.service.analysis.IKpiService;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.entity.SourceTran;

/**
 * @author NJ
 * @date 2018/9/11 19:54
 */
@Service
public class KpiServiceImpl implements IKpiService{
	@Autowired
	private IndicatorGroupMapper indicatorGroupMapper;
	@Autowired
	private DatasourceRelevantMapper datasourceRelevantMapper;
	@Autowired
	private RelevantContraintMapper relevantContraintMapper;
	@Autowired
	private DsUuidMapper dsUuidMapper;

	@Override
	public List<Map<String, Object>> getIndicatorGroup() {
		List<Map<String, Object>> list=new ArrayList<>();
		Map<String, Object> map=new HashMap<>(2);
		map.put("label", "交易性能");
		map.put("value", 1);
		list.add(map);
		Map<String, Object> map2=new HashMap<>(2);
		map2.put("label", "网络性能");
		map2.put("value", 3);
		list.add(map2);
		Map<String, Object> map3=new HashMap<>(2);
		map3.put("label", "应用性能");
		map3.put("value", 4);
		list.add(map3);
		List<Map<String, Object>> grouplist=indicatorGroupMapper.selectRelevant();
		for (Map<String, Object> map4 : grouplist) {
			Map<String, Object> map5=new HashMap<>();
			map5.put("label",map4.get("label"));
			map5.put("value","extend_"+ map4.get("value"));
			map5.put("sourcesId",map4.get("sourcesId"));
			list.add(map5);
		}
		return list;
	}

	@Override
	public List<ExtendChooseDataParam> getExtendChoose(ExtendChooseParam param) {
		Integer groupId=param.getGroupId();
		Integer dataSourceId=param.getSourcesId();
		List<Map<String, Object>> chooseList=new ArrayList<>();
		List<ExtendChooseDetailParam> choose=param.getChoose();
		if(evUtil.listIsNullOrZero(choose)) {
			return null;
		}else {
			Map<Integer, List<ExtendChooseDetailParam>> grouplist = choose.stream()
					.collect(Collectors.groupingBy(ExtendChooseDetailParam::getComponentId, Collectors.toList()));
			for (Map.Entry<Integer, List<ExtendChooseDetailParam>> entry : grouplist.entrySet()) {
				Map<String, Object> chooseMap=new HashMap<>();
				chooseMap.put("componentId", entry.getKey());
				Set<Integer> types=new HashSet<>();
				List<ExtendChooseDetailParam> detail=entry.getValue();
				for (ExtendChooseDetailParam extendChooseDetailParam : detail) {
					types.add(extendChooseDetailParam.getType());
				}
				chooseMap.put("types", types);
				chooseList.add(chooseMap);
			}
			
		}
		List<ExtendChooseDataParam> result=new ArrayList<>();
		if(!evUtil.listIsNullOrZero(chooseList)) {
			for (Map<String, Object> map : chooseList) {
				ExtendChooseDataParam resultParam=new ExtendChooseDataParam();
				//获取当前挂载点
				Integer componentId=evUtil.getMapIntegerValue(map, "componentId");
				resultParam.setComponentId(componentId);
				Map<String, Object> columnMap=new HashMap<>();
				columnMap.put("componentId", componentId);
				columnMap.put("indicatorGroupId", groupId);
				List<DatasourceRelevant> relevantList=datasourceRelevantMapper.selectByMap(columnMap);
				List<Map<String, Object>> list=new ArrayList<>();
				if(!evUtil.listIsNullOrZero(relevantList)) {
					Integer mount = relevantList.get(0).getMount();
					Integer relevantField=relevantList.get(0).getRelevantField();
					if(null==mount&&null==relevantField) {
						resultParam.setType(3);
					}else if(null==relevantField) {
						resultParam.setType(mount);
					}else{
						resultParam.setType(relevantField);
					}
					//判断当前节点是否和挂载点一致
					if(!resultParam.getType().equals(3)) {
						Set<Integer> types=(Set<Integer>) map.get("types");
						if(types.contains(resultParam.getType())) {
							// 获取全部关联约束
							List<RelevantContraint> alllist = relevantContraintMapper.selectByMap(columnMap);
							if(!evUtil.listIsNullOrZero(alllist)) {
								for (RelevantContraint relevantContraint : alllist) {
									Map<String, Object> chooseMap=new HashMap<>();
									EntityWrapper<DsUuid> wrapper=new EntityWrapper<>();
									DsUuid dsuuid=new DsUuid();
									dsuuid.setActive(1);
									dsuuid.setDataSourceId(dataSourceId);
									List<DsUuid> uuidList=new ArrayList<>();
									switch (resultParam.getType()) {
									//挂载到组件
									case 0:
										chooseMap.put("value", relevantContraint.getValue());
										dsuuid.setValue(relevantContraint.getValue());
										wrapper.setEntity(dsuuid);
										uuidList= dsUuidMapper.selectList(wrapper);
										break;
									//挂载到ip
									case 1:
										chooseMap.put("value", relevantContraint.getIp());
										dsuuid.setValue(relevantContraint.getValue());
										wrapper.setEntity(dsuuid);
										uuidList= dsUuidMapper.selectList(wrapper);
										break;
										//挂载到port
									case 2:
										chooseMap.put("value", relevantContraint.getIp()+":"+relevantContraint.getPort());
										dsuuid.setValue(relevantContraint.getValue());
										wrapper.setEntity(dsuuid);
										uuidList= dsUuidMapper.selectList(wrapper);
										break;
									default:
										break;
									}
									if(!evUtil.listIsNullOrZero(uuidList)) {
										chooseMap.put("innerUUID", uuidList.get(0).getId());
									}
									list.add(chooseMap);
								}
							}
						}
					}
					
				}else {
					resultParam.setType(3);
				}
				resultParam.setChoose(list);
				result.add(resultParam);
			}
		}
		return result;
	}
	
}
