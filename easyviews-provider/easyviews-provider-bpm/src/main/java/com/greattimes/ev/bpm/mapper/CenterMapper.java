package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.Center;

/**
 * <p>
 * 中心表 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-22
 */
public interface CenterMapper extends BaseMapper<Center> {


	/**
	 * 查询是否有重复中心名
	 * 
	 * @param nameMap
	 * @return
	 */
	List<Center> selectRepeatName(Map<String, Object> nameMap);

}
