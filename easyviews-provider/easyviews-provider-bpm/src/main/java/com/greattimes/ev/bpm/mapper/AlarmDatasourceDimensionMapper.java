package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmDatasourceDimension;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警 数据源维度 表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-16
 */
public interface AlarmDatasourceDimensionMapper extends BaseMapper<AlarmDatasourceDimension> {

}
