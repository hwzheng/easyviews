package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Abnormaldimension;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-12-03
 */
public interface AbnormaldimensionMapper extends BaseMapper<Abnormaldimension> {

}
