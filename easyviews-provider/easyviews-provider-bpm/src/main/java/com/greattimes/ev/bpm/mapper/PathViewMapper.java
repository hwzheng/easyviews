package com.greattimes.ev.bpm.mapper;


import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.PathView;

/**
 * <p>
 * 路径图 视图表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-09-13
 */
public interface PathViewMapper extends BaseMapper<PathView> {

}
