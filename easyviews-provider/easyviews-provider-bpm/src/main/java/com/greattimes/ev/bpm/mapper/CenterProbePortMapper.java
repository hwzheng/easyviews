package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.config.param.resp.CenterProbePortParam;
import com.greattimes.ev.bpm.entity.CenterProbePort;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 探针口表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-08-08
 */
public interface CenterProbePortMapper extends BaseMapper<CenterProbePort> {

	List<CenterProbePortParam> selectProbePort(@Param("id") Integer id);

}
