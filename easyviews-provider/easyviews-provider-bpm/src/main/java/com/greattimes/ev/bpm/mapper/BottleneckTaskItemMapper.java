package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskItem;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表任务子表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface BottleneckTaskItemMapper extends BaseMapper<BottleneckTaskItem> {

	/**查询子任务及步骤
	 * @param columnMap
	 * @return
	 */
	List<BottleneckTaskItem> selectItemStep(Map<String, Object> columnMap);

}
