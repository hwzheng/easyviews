package com.greattimes.ev.bpm.service.datasources.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.greattimes.ev.bpm.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.datasources.param.req.DatasourceRelevantParam;
import com.greattimes.ev.bpm.mapper.DatasourceRelevantDetailMapper;
import com.greattimes.ev.bpm.mapper.DatasourceRelevantMapper;
import com.greattimes.ev.bpm.mapper.DatasourceRelevantRelationMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMappingMapper;
import com.greattimes.ev.bpm.mapper.IndicatorContraintMapper;
import com.greattimes.ev.bpm.mapper.IndicatorsMapper;
import com.greattimes.ev.bpm.mapper.RelevantContraintMapper;
import com.greattimes.ev.bpm.mapper.ServerGroupMapper;
import com.greattimes.ev.bpm.mapper.ServerIpMapper;
import com.greattimes.ev.bpm.service.datasources.IDatasourceRelevantService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 数据源关联表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
@Service
public class DatasourceRelevantServiceImpl extends ServiceImpl<DatasourceRelevantMapper, DatasourceRelevant>
		implements IDatasourceRelevantService {
	@Autowired
	private DatasourceRelevantMapper datasourceRelevantMapper;
	@Autowired
	private DatasourceRelevantDetailMapper datasourceRelevantDetailMapper;
	@Autowired
	private IndicatorsMapper indicatorsMapper;
	@Autowired
	private DatasourceRelevantRelationMapper datasourceRelevantRelationMapper;
	@Autowired
	private ServerGroupMapper serverGroupMapper;
	@Autowired
	private ServerIpMapper serverIpMapper;
	@Autowired
	private IndicatorContraintMapper indicatorContraintMapper;
	@Autowired
	private RelevantContraintMapper relevantContraintMapper;
	@Autowired
	private DsUuidMapper dsUuidMapper;
	@Autowired
	private DsUuidMappingMapper dsUuidMappingMapper;

	@Override
	public void add(int userId, DatasourceRelevantParam param) {
		DatasourceRelevant relevant = new DatasourceRelevant();
		Integer relevantField = param.getRelevantField();
		Integer componentId = param.getComponentId();
		Integer indicatorGroupId = param.getIndicatorGroupId();
		Date date = new Date();
		relevant.setCreateTime(date);
		relevant.setCreateBy(userId);
		relevant.setUpdateBy(userId);
		relevant.setUpdateTime(date);
		relevant.setComponentId(param.getComponentId());
		relevant.setDataSourceId(param.getDataSourceId());
		relevant.setIndicatorGroupId(param.getIndicatorGroupId());
		relevant.setShowDimension(param.getShowDimension());
		relevant.setRelevantField(param.getRelevantField());
		relevant.setMount(param.getMount());
		datasourceRelevantMapper.insert(relevant);
		List<Integer> feild = param.getField();
		if (!evUtil.listIsNullOrZero(feild)) {
			for (Integer integer : feild) {
				DatasourceRelevantRelation re = new DatasourceRelevantRelation();
				re.setRelevantField(integer);
				re.setDataSourceRelevantId(relevant.getId());
				datasourceRelevantRelationMapper.insert(re);
			}
		}
		handelData(relevant, relevantField, componentId, indicatorGroupId);
	}

	/**
	 * 数据的后续处理
	 * 
	 * @param relevant
	 * @param relevantField
	 * @param componentId
	 * @param indicatorGroupId
	 */
	private void handelData(DatasourceRelevant relevant, Integer relevantField, Integer componentId,
			Integer indicatorGroupId) {
		Map<String, Object> col = new HashMap<>();
		// 1.匹配数据源指标约束
		if (null != relevantField) {
			switch (relevantField) {
			// 只关联ip
			case 1:
				// 获取组件下的ip
				List<DataSourcesRelationParam> ips = serverIpMapper.selectByComponentId(componentId);
				// 获取数据源指标约束
				col.clear();
				col.put("indicatorGroupId", indicatorGroupId);
				List<IndicatorContraint> list = indicatorContraintMapper.selectByMap(col);
				// value值匹配
				for (DataSourcesRelationParam dataSourcesIpParam : ips) {
					for (IndicatorContraint indicatorContraint : list) {
						if (indicatorContraint.getValue().contains(dataSourcesIpParam.getValue())) {
							IndicatorContraint entity = new IndicatorContraint();
							entity.setId(indicatorContraint.getId());
							entity.setMark(1);
							// 更新指标约束表mark
							indicatorContraintMapper.updateById(entity);
							dataSourcesIpParam.setOuterUuid(indicatorContraint.getOuterUuid());
							dataSourcesIpParam.setIndicatorId(indicatorContraint.getIndicatorId());
							// 匹配成功，插入数据源关联约束表及uuid表
							insertRelevantContraint(dataSourcesIpParam, indicatorContraint, componentId,
									indicatorGroupId, relevant.getDataSourceId());
						}
					}
				}
				break;
			// 关联了ip+port
			case 2:
				col.clear();
				col.put("componentId", componentId);
				List<DataSourcesRelationParam> all = new ArrayList<>();
				// 获取组件服务器组
				List<ServerGroup> groupids = serverGroupMapper.selectByMap(col);
				for (ServerGroup serverGroup : groupids) {
					List<DataSourcesRelationParam> li = serverIpMapper.selectIpAndport(serverGroup.getId());
					all.addAll(li);
				}
				// 获取数据源指标约束
				col.clear();
				col.put("indicatorGroupId", indicatorGroupId);
				List<IndicatorContraint> IndicatorContraints = indicatorContraintMapper.selectByMap(col);
				for (DataSourcesRelationParam dataSourcesIpParam : all) {
					for (IndicatorContraint indicatorContraint : IndicatorContraints) {
						if (indicatorContraint.getValue().contains(dataSourcesIpParam.getValue())) {
							IndicatorContraint entity = new IndicatorContraint();
							entity.setId(indicatorContraint.getId());
							entity.setMark(1);
							// 更新指标约束表mark
							indicatorContraintMapper.updateById(entity);
							dataSourcesIpParam.setOuterUuid(indicatorContraint.getOuterUuid());
							dataSourcesIpParam.setIndicatorId(indicatorContraint.getIndicatorId());
							// 匹配成功，插入数据源关联约束表及uuid表
							insertRelevantContraint(dataSourcesIpParam, indicatorContraint, componentId,
									indicatorGroupId, relevant.getId());
						}
					}
				}
				break;
			default:
				break;
			}
		}
	}

	/**
	 * 插入数据源关联约束表及uuid表
	 * 
	 * @param dataSourcesIpParam
	 * @param indicatorContraint
	 * @param componentId
	 * @param indicatorGroupId
	 * @param datasourseId
	 */
	private void insertRelevantContraint(DataSourcesRelationParam dataSourcesIpParam,
			IndicatorContraint indicatorContraint, Integer componentId, Integer indicatorGroupId,
			Integer datasourseId) {
		RelevantContraint re = new RelevantContraint();
		re.setComponentId(componentId);
		re.setIndicatorGroupId(indicatorGroupId);
		re.setIp(dataSourcesIpParam.getIp());
		Integer port = -1;
		if (dataSourcesIpParam.getPort() != null) {
			port = dataSourcesIpParam.getPort();
		}
		re.setPort(port);
		re.setValue(indicatorContraint.getValue());
		// relevantContraintMapper.RelevantContraint(re);
		EntityWrapper<RelevantContraint> wrapper = new EntityWrapper<>();
		wrapper.setEntity(re);
		if (evUtil.listIsNullOrZero(relevantContraintMapper.selectList(wrapper))) {
			relevantContraintMapper.insert(re);
		}
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("dataSourceId", datasourseId);
		columnMap.put("value", re.getValue());
		if (evUtil.listIsNullOrZero(dsUuidMapper.selectByMap(columnMap))) {
			DsUuid dsuuid = new DsUuid();
			dsuuid.setActive(1);
			dsuuid.setDataSourceId(datasourseId);
			dsuuid.setValue(re.getValue());
			// 插入uuid表
			dsUuidMapper.insert(dsuuid);
			EntityWrapper<DsUuidMapping> ds = new EntityWrapper<>();
			DsUuidMapping dsmapping = new DsUuidMapping();
			dsmapping.setDataSourceId(datasourseId);
			dsmapping.setIndicatorId(dataSourcesIpParam.getIndicatorId());
			dsmapping.setInnerUUID(dsuuid.getId().intValue());
			dsmapping.setOuterUUID(dataSourcesIpParam.getOuterUuid() + "");
			ds.setEntity(dsmapping);
			if (evUtil.listIsNullOrZero(dsUuidMappingMapper.selectList(ds))) {
				// 插入uuid映射表
				dsUuidMappingMapper.insert(dsmapping);
			}
		}

	}

	@Override
	public List<DatasourceRelevantParam> select(int applicationId) {
		List<DatasourceRelevantParam> dataList = datasourceRelevantMapper.selectDatasource(applicationId);
		for (DatasourceRelevantParam datasourceRelevantParam : dataList) {
			List<Integer> list = datasourceRelevantRelationMapper.selectByRelevantId(datasourceRelevantParam.getId());
			datasourceRelevantParam.setField(list);
		}
		return dataList;
	}

	@Override
	public void delete(int id) {
		DatasourceRelevant da=datasourceRelevantMapper.selectById(id);
		int componentId=da.getComponentId();
		int indicatorGroupId=da.getIndicatorGroupId();
		int sourcesId=da.getDataSourceId();
		//删除外部数据关联数据
		deleteRelevantData(componentId,indicatorGroupId,sourcesId);
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("dataSourceRelevantId", id);
		datasourceRelevantRelationMapper.deleteByMap(columnMap);
		datasourceRelevantMapper.deleteById(id);
	}

	/**
	 * 删除外部数据关联数据
	 * @param componentId
	 * @param indicatorGroupId
	 * @param sourcesId
	 */
	private void deleteRelevantData(int componentId, int indicatorGroupId,int sourcesId) {
		Map<String, Object> map=new HashMap<>();
		map.put("componentId", componentId);
		map.put("indicatorGroupId", indicatorGroupId);
		List<RelevantContraint> relevantcontraint=relevantContraintMapper.selectByMap(map);
		//删ds_relevant_contraint
		relevantContraintMapper.deleteByMap(map);
		if(!evUtil.listIsNullOrZero(relevantcontraint)) {
			List<String> values = relevantcontraint.stream().map(RelevantContraint::getValue).collect(Collectors.toList());
			EntityWrapper<DsUuid> wrapper=new EntityWrapper<>();
			wrapper.where("dataSourceId", sourcesId).in("value", values);
			List<DsUuid> dsuuid=dsUuidMapper.selectList(wrapper);
			if(!evUtil.listIsNullOrZero(dsuuid)) {
				List<Long> innerId = dsuuid.stream().map(DsUuid::getId).collect(Collectors.toList());
				EntityWrapper<DsUuidMapping> wra=new EntityWrapper<>();
				wra.in("innerUUID", innerId);
				dsUuidMappingMapper.delete(wra);
			}
			dsUuidMapper.delete(wrapper);
		}
	}

	@Override
	public void edit(DatasourceRelevantParam relevant, int userId) {
		int id = relevant.getId();
		DatasourceRelevant re = new DatasourceRelevant();
		// 查询原数据
		// DatasourceRelevant oldData = datasourceRelevantMapper.selectById(id);
		Date date = new Date();
		re.setId(id);
		re.setUpdateBy(userId);
		re.setUpdateTime(date);
		re.setComponentId(relevant.getComponentId());
		re.setDataSourceId(relevant.getDataSourceId());
		re.setIndicatorGroupId(relevant.getIndicatorGroupId());
		re.setShowDimension(relevant.getShowDimension());
		re.setRelevantField(relevant.getRelevantField());
		re.setMount(relevant.getMount());
		datasourceRelevantMapper.update(re);
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("dataSourceRelevantId", id);
		datasourceRelevantRelationMapper.deleteByMap(columnMap);
		List<Integer> feild = relevant.getField();
		if (!evUtil.listIsNullOrZero(feild)) {
			for (Integer integer : feild) {
				DatasourceRelevantRelation relation = new DatasourceRelevantRelation();
				relation.setRelevantField(integer);
				relation.setDataSourceRelevantId(relevant.getId());
				datasourceRelevantRelationMapper.insert(relation);
			}
		}
		handelData(re, re.getRelevantField(), re.getComponentId(), re.getIndicatorGroupId());
		/*
		 * if (!(oldData.getRelevantField().equals(relevant.getRelevantField()) &&
		 * oldData.getIndicatorGroupId().intValue() ==
		 * relevant.getIndicatorGroupId().intValue())) { Map<String, Object> columnMap2
		 * = new HashMap<>(1); columnMap2.put("dataSourceRelevantId", id);
		 * datasourceRelevantDetailMapper.deleteByMap(columnMap2); }
		 */
	}

	@Override
	public List<Indicators> getIndicators(int id) {
		return indicatorsMapper.getIndicators(id);
	}

	@Override
	public DatasourceRelevant getDatasource(int id) {
		return datasourceRelevantMapper.selectById(id);
	}

	@Override
	public List<DatasourceRelevantDetail> selectDetail(int dataSourceRelevantId, int indicator) {
		Map<String, Object> columnMap = new HashMap<>(2);
		columnMap.put("dataSourceRelevantId", dataSourceRelevantId);
		columnMap.put("indicator", indicator);
		return datasourceRelevantDetailMapper.selectByMap(columnMap);
	}

	@Override
	public List<DatasourceRelevantDetail> getIndicators(int groupId, String indicatorName) {
		Map<String, Object> columnMap = new HashMap<>(2);
		columnMap.put("groupId", groupId);
		columnMap.put("name", indicatorName);
		return datasourceRelevantDetailMapper.selectByMap(columnMap);
	}

	@Override
	public void insertRelevantDetail(List<DatasourceRelevantDetail> details) {
		datasourceRelevantDetailMapper.insertRelevantDetail(details);
	}

	@Override
	public void insert(List<RelevantContraint> details, int datasourseId) {
		for (RelevantContraint relevantContraint : details) {
			EntityWrapper<RelevantContraint> wrapper = new EntityWrapper<>();
			wrapper.setEntity(relevantContraint);
			if (evUtil.listIsNullOrZero(relevantContraintMapper.selectList(wrapper))) {
				relevantContraintMapper.insert(relevantContraint);
				String value = relevantContraint.getValue();
				int indicator = relevantContraint.getIndicatorGroupId();
				EntityWrapper<IndicatorContraint> w = new EntityWrapper<>();
				IndicatorContraint entity = new IndicatorContraint();
				entity.setIndicatorGroupId(indicator);
				entity.setValue(value);
				w.setEntity(entity);
				// 获取outteruuid
				List<IndicatorContraint> list = indicatorContraintMapper.selectList(w);
				if (!evUtil.listIsNullOrZero(list)) {
					for (IndicatorContraint indicatorContraint : list) {
						// 更新mark
						IndicatorContraint in = new IndicatorContraint();
						in.setId(indicatorContraint.getId());
						in.setMark(1);
						indicatorContraintMapper.updateById(in);
					}
					// 插入uuid表
					Map<String, Object> columnMap = new HashMap<>();
					columnMap.put("dataSourceId", datasourseId);
					columnMap.put("value", relevantContraint.getValue());
					if (evUtil.listIsNullOrZero(dsUuidMapper.selectByMap(columnMap))) {
						DsUuid dsuuid = new DsUuid();
						dsuuid.setActive(1);
						dsuuid.setDataSourceId(datasourseId);
						dsuuid.setValue(relevantContraint.getValue());
						// 插入uuid表
						dsUuidMapper.insert(dsuuid);
						// 插入映射表
						for (IndicatorContraint indicatorContraint : list) {
							EntityWrapper<DsUuidMapping> ds = new EntityWrapper<>();
							DsUuidMapping dsmapping = new DsUuidMapping();
							dsmapping.setDataSourceId(datasourseId);
							dsmapping.setIndicatorId(indicatorContraint.getIndicatorId());
							dsmapping.setInnerUUID(dsuuid.getId().intValue());
							dsmapping.setOuterUUID(indicatorContraint.getOuterUuid() + "");
							ds.setEntity(dsmapping);
							if (evUtil.listIsNullOrZero(dsUuidMappingMapper.selectList(ds))) {
								// 插入uuid映射表
								dsUuidMappingMapper.insert(dsmapping);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void updateActive(int id, int showDimension) {
		DatasourceRelevant data = new DatasourceRelevant();
		data.setId(id);
		data.setShowDimension(showDimension);
		datasourceRelevantMapper.updateById(data);
	}

	@Override
	public List<RelevantContraint> slectRelevant(Integer componentId, Integer indicatorGroupId) {
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("componentId", componentId);
		columnMap.put("indicatorGroupId", indicatorGroupId);
		return relevantContraintMapper.selectByMap(columnMap);
	}

	@Override
	public List<IndicatorContraint> selectIndicator(Integer indicatorGroupId) {
		Map<String, Object> col = new HashMap<>();
		col.put("indicatorGroupId", indicatorGroupId);
		List<IndicatorContraint> list = indicatorContraintMapper.selectByMap(col);
		return list;
	}

	@Override
	public List<IndicatorContraint> selectIndicator(int groupId, int i) {
		EntityWrapper<IndicatorContraint> wrapper = new EntityWrapper<>();
		IndicatorContraint in = new IndicatorContraint();
		in.setIndicatorGroupId(groupId);
		in.setMark(i);
		wrapper.setEntity(in);
		wrapper.orderBy("value");
		List<IndicatorContraint> list = indicatorContraintMapper.selectList(wrapper);
		return list;
	}

	@Override
	public void deleteRelevantBySourcesId(Integer id) {
		Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("dataSourceId", id);
		List<DatasourceRelevant> re=datasourceRelevantMapper.selectByMap(columnMap);
		if(!evUtil.listIsNullOrZero(re)) {
			for (DatasourceRelevant datasourceRelevant : re) {
				delete(datasourceRelevant.getId());
			}
		}
	}

	@Override
	public void deleteRelevantByGroupId(Integer id) {
		Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("indicatorGroupId", id);
		List<DatasourceRelevant> re=datasourceRelevantMapper.selectByMap(columnMap);
		if(!evUtil.listIsNullOrZero(re)) {
			for (DatasourceRelevant datasourceRelevant : re) {
				delete(datasourceRelevant.getId());
			}
		}		
	}

	@Override
	public void deleteRelevantByFieldId(List<Integer> ids) {
		EntityWrapper<DatasourceRelevantRelation> wrapper=new EntityWrapper<>();
		wrapper.in("RelevantField", ids);
		List<DatasourceRelevantRelation> re=datasourceRelevantRelationMapper.selectList(wrapper);
		if(!evUtil.listIsNullOrZero(re)) {
			for (DatasourceRelevantRelation datasourceRelevantRelation : re) {
				delete(datasourceRelevantRelation.getDataSourceRelevantId());
			}
		}
		
	}

	@Override
	public boolean deleteDatasourceRelevantByComponentIds(List<Integer> ids) {
		if(evUtil.listIsNullOrZero(ids)){
			return true;
		}
		EntityWrapper<DatasourceRelevant> datasourceRelevantEntityWrapper = new EntityWrapper<>();
		datasourceRelevantEntityWrapper.in("componentId", ids);
		List<DatasourceRelevant> datasourceRelevantList = datasourceRelevantMapper.selectList(datasourceRelevantEntityWrapper);
		for (DatasourceRelevant datasourceRelevant : datasourceRelevantList) {
			delete(datasourceRelevant.getId());
		}
		return true;
	}
}
