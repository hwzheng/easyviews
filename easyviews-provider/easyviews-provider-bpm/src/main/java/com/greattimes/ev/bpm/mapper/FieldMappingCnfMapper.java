package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.FieldMappingCnf;

/**
 * <p>
 * 维度字段映射组件表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
public interface FieldMappingCnfMapper extends BaseMapper<FieldMappingCnf> {

	/**映射配置查询
	 * @param applicationId
	 * @return
	 */
	List<Map<String, Object>> selectFieldMapping(int applicationId);

}
