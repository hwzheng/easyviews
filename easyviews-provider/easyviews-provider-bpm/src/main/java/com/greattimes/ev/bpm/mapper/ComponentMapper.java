package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import com.greattimes.ev.bpm.config.param.resp.ComponentChainParam;
import com.greattimes.ev.bpm.entity.Server;
import com.greattimes.ev.bpm.entity.ServerIp;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.req.ComponentParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.entity.Component;

/**
 * <p>
 * 应用组件表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface ComponentMapper extends BaseMapper<Component> {
    List<DictionaryParam> selectComponentNameAndIdByBusinessId(@Param("businessId") int businessId);
    List<DictionaryParam> selectComponentIpById(@Param("id") int id);
    List<DictionaryParam> selectComponentPortByIp(@Param("ip") int ip, @Param("ipStr") String ipStr);
    List<DictionaryParam> selectComponentPortByComponentAndIp(Map<String,Object> param);
    void updateBatchSort(List<ComponentParam> list);
    int getMaxComponentSortByAppId(@Param("applicationId") int applicationId);
	/**
	 * 根据customId获取协议id
	 * @param id
	 * @return
	 */
	List<Component> getPrototcolId(int customId);

	List<ComponentParam> selectComponentParmListByAppId(@Param("applicationId") int id);
	List<Map<String, Object>> selectIpAndportByComponentId(@Param("id") int id);

	List<Server> selectPortByIpId(@Param("id") int ipId);

	List<ServerIp> findServerIpsByComponentIds(@Param("ids")List<Integer> ids);

	List<Server> findServerByComponentIds(@Param("ids")List<Integer> ids);

	/**
	 * 同一监控点下组件名称不能重复
	 * @author NJ
	 * @date 2018/11/21 19:31
	 * @param map
	 * @return java.util.List<com.greattimes.ev.bpm.entity.Component>
	 */
	List<Component> selectComponentByMap(Map<String, Object> map);
	List<Map<String, Object>> getCenterIpBycomponentId(@Param("id")Integer id);

	/**
	 * 根据应用查询该应用下所有的组件的前后追踪字段
	 * @author NJ
	 * @date 2018/11/27 14:44
	 * @param id
	 * @return java.util.List<com.greattimes.ev.bpm.config.param.resp.ComponentChainParam>
	 */
	List<ComponentChainParam> findComponentChainParamByAppId(@Param("applicationId")Integer id);

	/**
	 * 根据组件id查找组件名称
	 * @author NJ
	 * @date 2018/12/3 18:39
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> findComponentByMap(Map<String, Object> map);

	/**
	 * 根据监控点id查找组件
	 * @author NJ
	 * @date 2018/12/13 20:20
	 * @param monitorIds
	 * @return java.util.List<com.greattimes.ev.bpm.entity.Component>
	 */
	List<Component> selectComponentsByMonitorIds(List<Integer> monitorIds);

	/**
	 * 查询应用-监控点-组件 left join custom
	 * @author NJ
	 * @date 2019/3/27 11:21
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectComponentTreeData(Map<String, Object> map);

	/**
	 * 组件的ip和port数据
	 * @author NJ
	 * @date 2019/3/27 15:20
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectIpAndPortData(Map<String, Object> map);
	/**
	 * 应用树
	 * @param map
	 * @return
	 */
	List<ApplicationTree> selectAppTree(Map<String, Object> map);
	/**
	 * 监控点
	 * @param appTrees
	 * @return
	 */
	List<ApplicationTree> selectMonitorTree(@Param("appTrees") List<ApplicationTree> appTrees);
	/**
	 * 组件
	 * @param appTrees
	 * @return
	 */
	List<ApplicationTree> selectComponentTree(@Param("appTrees")List<ApplicationTree> appTrees);
	/**
	 * ip
	 * @param appTrees
	 * @return
	 */
	List<ApplicationTree> selectIpTree(@Param("appTrees")List<ApplicationTree> appTrees);
	/**
	 * port
	 * @param appTrees
	 * @return
	 */
	List<ApplicationTree> selectPortTree(@Param("appTrees")List<ApplicationTree> appTrees);
	/**
	 * @author CGC
	 * @param customId
	 * @return
	 */
	Component selectComponentByCustomId(@Param("customId")Integer customId);



}
