package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmSendType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警信息发送类型 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
public interface AlarmSendTypeMapper extends BaseMapper<AlarmSendType> {

}
