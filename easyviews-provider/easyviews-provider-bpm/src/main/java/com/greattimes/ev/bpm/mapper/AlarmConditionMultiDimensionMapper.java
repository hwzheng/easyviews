package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmConditionMultiDimension;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警 多维度 表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-05
 */
public interface AlarmConditionMultiDimensionMapper extends BaseMapper<AlarmConditionMultiDimension> {

}
