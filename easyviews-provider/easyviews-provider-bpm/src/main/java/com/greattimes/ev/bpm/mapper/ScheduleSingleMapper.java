package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ScheduleSingle;

import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 单次排期表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
public interface ScheduleSingleMapper extends BaseMapper<ScheduleSingle> {

	/**
	 * 单次排期新增
	 * @param colmap
	 */
	void insertByMap(Map<String, Object> colmap);

}
