package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.system.entity.Dictionary;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 系统字典表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface DictionaryMapper extends BaseMapper<Dictionary> {
    List<Map<String, Object>> selectDicNameAndValueByDictTypeCode(String typeCode);

	/**
	 * 根据字典组ID查询字典
	 * @param id
	 * @return
	 */
	List<Dictionary> selectDictionaryList(Integer id);

	/**
	 * 更新字典类型
	 * @param tcMap
	 */
	void updateByTypeCode(Map<String, Object> tcMap);

	/**
	 * 获取字典详情
	 * @param id
	 * @return
	 */
	List<Dictionary> selectDictionaryDetail(@Param("id") Integer id);
}
