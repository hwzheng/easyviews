package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmPort;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警port表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
public interface AlarmPortMapper extends BaseMapper<AlarmPort> {

}
