package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.entity.ProtocolField;

/**
 * <p>
 * 协议字段表 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-23
 */
public interface ProtocolFieldMapper extends BaseMapper<ProtocolField> {

    /**
     * 分析维度信息查询
     * @param componentId
     * @return
     */
	List<Map<String, Object>> selectDimension(int componentId);

	/**
	 * 别名查询
	 * 
	 * @param componentId
	 * @param protocolId 
	 * @return
	 */
	List<DimensionParam> selectOtherName(@Param("componentId") int componentId,@Param("protocolId") Integer protocolId);

	/**根据componentId查询对应维度信息
	 * @param componentId
	 * @return
	 */
	List<ProtocolField> selectDimensionByComponentId(int componentId);
	
	/**根据componentId查询对应维度信息
	 * @param componentId
	 * @return
	 */
	List<ProtocolField> selectDimensionWithOtherName(int componentId);

	/**
	 * 组件维度数据
	 * @author NJ
	 * @date 2019/4/17 16:19
	 * @param map
	 * @return java.util.List<com.greattimes.ev.bpm.entity.ProtocolField>
	 */
	List<Map<String, Object>> selectDimensionByComponentIds(Map<String, Object> map);


	/**
	 * 批量插入
	 * @author cgc  
	 * @date 2018年10月13日  
	 * @param set
	 */
	void batchInsert(@Param("set") Set<ProtocolField> set);


	/**更新状态
	 * @param ids
	 */
	void batchUpadteActive(@Param("ids") List<Integer> ids);


	/**
	 * 根据组件查询维度的别名和正名
	 * @author NJ
	 * @date 2018/10/18 9:57
	 * @param componentId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectDimensionNameBycomponentId(int componentId);


	/**
	 * 根据组件id查询维度别名
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectDimensionNameByIds(Map<String, Object> map);

	void batchUpdateSort(List<DimensionParam> list);

	/**（组件）带别名字段
	 * @param columnMap
	 * @return
	 */
	List<ProtocolField> selectFieldWithOtherName(Map<String, Object> columnMap);

	/**（事件）带别名字段
	 * @author CGC
	 * @param columnMap
	 * @return
	 */
	List<ProtocolField> selectFieldWithCustomOtherName(Map<String, Object> columnMap);

}
