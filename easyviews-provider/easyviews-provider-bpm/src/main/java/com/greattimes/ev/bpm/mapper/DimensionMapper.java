package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.Dimension;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 协议可选维度配置表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2019-02-14
 */
public interface DimensionMapper extends BaseMapper<Dimension> {

	List<Dimension> selectProtocolDimension(@Param("protocolId")int protocolId);

}
