package com.greattimes.ev.bpm.thread;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class TimeOut implements Runnable {
	
	Logger log= LoggerFactory.getLogger("msgproducer");

	private Future<Boolean> future ;
	
	private String name;
	
	private final int TIMEOUT=40;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			// 设定40秒超时
			future.get(TIMEOUT, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException
				| TimeoutException e) {
			 // TODO: handle exception
			 log.error(name+": >>" +e.getMessage()+"/rn"+e.toString());
			// 中断执行此任务的线程
			 future.cancel(true);
		}
	}
	public Future<Boolean> getFuture() {
		return future;
	}

	public void setFuture(Future<Boolean> future) {
		this.future = future;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
