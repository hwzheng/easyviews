package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.ComponentDimension;

/**
 * <p>
 * 组件分析维度 Mapper 接口
 * </p>
 *
 * @author Cgc
 * @since 2018-05-23
 */
public interface ComponentDimensionMapper extends BaseMapper<ComponentDimension> {
    /**
     * 根据组件id查询
     * @author NJ
     * @date 2019/2/18 11:49
     * @param componentId
     * @return int
     */
    int getMaxSortComponentDimension(int componentId);
}
