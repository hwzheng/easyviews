package com.greattimes.ev.bpm.service.analysis.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.analysis.param.resp.MetaParam;
import com.greattimes.ev.bpm.entity.BottleneckTask;
import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeField;
import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeRule;
import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeRuleThreshold;
import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeRuleValue;
import com.greattimes.ev.bpm.entity.BottleneckTaskData;
import com.greattimes.ev.bpm.entity.BottleneckTaskDate;
import com.greattimes.ev.bpm.entity.BottleneckTaskDay;
import com.greattimes.ev.bpm.entity.BottleneckTaskItem;
import com.greattimes.ev.bpm.entity.BottleneckTaskTime;
import com.greattimes.ev.bpm.entity.BottleneckTaskUser;
import com.greattimes.ev.bpm.mapper.BottleneckTaskAnalyzeFieldMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskAnalyzeRuleMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskAnalyzeRuleThresholdMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskAnalyzeRuleValueMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskDataMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskDateMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskDayMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskItemMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskTimeMapper;
import com.greattimes.ev.bpm.mapper.BottleneckTaskUserMapper;
import com.greattimes.ev.bpm.mapper.CustomMapper;
import com.greattimes.ev.bpm.mapper.ProtocolFieldMapper;
import com.greattimes.ev.bpm.service.analysis.IBottleneckTaskService;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 智能瓶颈报表任务表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@Service
public class BottleneckTaskServiceImpl extends ServiceImpl<BottleneckTaskMapper, BottleneckTask>
		implements IBottleneckTaskService {
	
	@Autowired
	private BottleneckTaskAnalyzeFieldMapper bottleneckTaskAnalyzeFieldMapper;
	@Autowired
	private BottleneckTaskAnalyzeRuleMapper bottleneckTaskAnalyzeRuleMapper;
	@Autowired
	private BottleneckTaskAnalyzeRuleThresholdMapper bottleneckTaskAnalyzeRuleThresholdMapper;
	@Autowired
	private BottleneckTaskAnalyzeRuleValueMapper bottleneckTaskAnalyzeRuleValueMapper;
	@Autowired
	private BottleneckTaskDateMapper bottleneckTaskDateMapper;
	@Autowired
	private BottleneckTaskMapper bottleneckTaskMapper;
	@Autowired
    private BottleneckTaskDayMapper bottleneckTaskDayMapper;
	@Autowired
	private BottleneckTaskDataMapper bottleneckTaskDataMapper;
	@Autowired
	private BottleneckTaskTimeMapper bottleneckTaskTimeMapper;
	@Autowired
	private BottleneckTaskItemMapper bottleneckTaskItemMapper;
	@Autowired
	private ProtocolFieldMapper protocolFieldMapper;
	@Autowired
	private CustomMapper customMapper;
	@Autowired
	private BottleneckTaskUserMapper bottleneckTaskUserMapper;
	
    static SimpleDateFormat  sdf = new SimpleDateFormat("yyyyMMdd");
    static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    Logger log=LoggerFactory.getLogger(this.getClass());
	@Override
	public List<BottleneckTaskAnalyzeField> getAnalyzeField(int taskId) {
		return bottleneckTaskAnalyzeFieldMapper.selectAnalyzeField(taskId);
	}

	@Override
	public MetaParam getMeta(String taskId) {
		MetaParam meta = new MetaParam();
		/*// 如果是定时
		if (taskId.length() > 8) {
			taskId = taskId.substring(8, taskId.length());
		}*/
		
		List<BottleneckTaskAnalyzeField> field = bottleneckTaskAnalyzeFieldMapper.selectAnalyzeField(Integer.parseInt(taskId));
		List<String> stringCols=new ArrayList<>();
		for (BottleneckTaskAnalyzeField f : field) {
			if(f.getType().equals(2)) {
				stringCols.add(f.getKey());
			}
		}
		//List<String> stringCols = field.stream().map(BottleneckTaskAnalyzeField::getKey).collect(Collectors.toList());
		//List<String> stringCols = field.stream().map(BottleneckTaskAnalyzeField::getName).collect(Collectors.toList());
		/*List<List<Object>> stringCols=new ArrayList<>();
		for (BottleneckTaskAnalyzeField bottleneckTaskAnalyzeField : field) {
			List<Object> num=new ArrayList<>();
			num.add(bottleneckTaskAnalyzeField.getKey());
			num.add(bottleneckTaskAnalyzeField.getName());
			stringCols.add(num);
		}*/
		Map<String, Object> map=new HashMap<>();
		for (BottleneckTaskAnalyzeField bottleneckTaskAnalyzeField : field) {
			map.put(bottleneckTaskAnalyzeField.getKey(), bottleneckTaskAnalyzeField.getName());
		}
		String label = null;
		String labelOp = null;
		String labelValue = null;

		List<BottleneckTaskAnalyzeField> numericField = new ArrayList<>();
		for (BottleneckTaskAnalyzeField bottleneckTaskAnalyzeField : field) {
			// 数字类型字段
			if (bottleneckTaskAnalyzeField.getType().equals(1)) {
				numericField.add(bottleneckTaskAnalyzeField);
			}
			// 目标属性
			if (bottleneckTaskAnalyzeField.getUse().equals(1)) {
				label = bottleneckTaskAnalyzeField.getKey();
				//label = bottleneckTaskAnalyzeField.getName();
				int analyzeFieldId = bottleneckTaskAnalyzeField.getId();
				Map<String, Object> columnMap=new HashMap<>();
				columnMap.put("analyzeFieldId", analyzeFieldId);
				List<BottleneckTaskAnalyzeRule> rule= bottleneckTaskAnalyzeRuleMapper.selectByMap(columnMap);
				if(!evUtil.listIsNullOrZero(rule)) {
					int analyzeRule=rule.get(0).getAnalyzeRule();
					int analyzeRuleId=rule.get(0).getId();
					labelOp=getAnalyzeRule(analyzeRule);
					Map<String, Object> col=new HashMap<>(1);
					col.put("analyzeRuleId", analyzeRuleId);
					if(analyzeRule==1||analyzeRule==2) {
						List<BottleneckTaskAnalyzeRuleValue> threshold =bottleneckTaskAnalyzeRuleValueMapper.selectByMap(col);
						if(!evUtil.listIsNullOrZero(threshold)) {
							labelValue=threshold.get(0).getValue();
						}
					}
					if(analyzeRule==3||analyzeRule==4) {
						List<BottleneckTaskAnalyzeRuleThreshold> threshold =bottleneckTaskAnalyzeRuleThresholdMapper.selectByMap(col);
						if(!evUtil.listIsNullOrZero(threshold)) {
							labelValue=threshold.get(0).getAnalyzeThreshold().toString();
						}
					}
				}
			}
		}
		List<List<Object>> numericCols=new ArrayList<>();
		for (BottleneckTaskAnalyzeField bottleneckTaskAnalyzeField : numericField) {
			List<Object> num=new ArrayList<>();
			num.add(bottleneckTaskAnalyzeField.getKey());
			num.add(bottleneckTaskAnalyzeField.getName());
			numericCols.add(num);
		}
		meta.setHeaderMap(map);
		meta.setNumericCols(numericCols);
		meta.setLabel(label);
		meta.setLabelOp(labelOp);
		meta.setStringCols(stringCols);
		meta.setLabelValue(labelValue);
		return meta;
	}

	private String getAnalyzeRule(int analyzeRule) {
		String rule=null;
		switch (analyzeRule) {
		case 1:
			rule="=";
			break;
		case 2:
			rule="!=";
			break;
		case 3:
			rule=">";
			break;
		case 4:
			rule="<=";
			break;
			
		default:
			log.error("瓶颈分析analyzeRule错误"+analyzeRule);
			break;
		}
		return rule;
	}

	@Override
	public List<Map<String, Object>> getSingleTaskDetail(int itemId) {
		return bottleneckTaskMapper.getSingleTaskDetail(itemId);
	}

	@Override
	public List<Map<String, Object>> getTimeTaskDetail(int itemId) {
		return bottleneckTaskMapper.getTimeTaskDetail(itemId);
	}
	@Override
    public Map<String, Object> selectTaskDataByItemDayId(String itemDayId){
        Map<String,Object> param = new HashMap<>();
        if(String.valueOf(itemDayId).length() > 8){
            // 定时任务
            String dateStr = itemDayId.substring(0,8);
            String id = itemDayId.substring(8,itemDayId.length());
            param.put("date", dateStr);
            param.put("taskItemId", id);
        }else{
            param.put("taskItemId", itemDayId);
        }
        List<Map<String, Object>> result = bottleneckTaskMapper.selectTaskDataByItemDayId(param);
        if(!evUtil.listIsNullOrZero(result)){
            return result.get(0);
        }else{
            return Collections.EMPTY_MAP;
        }
    }

    @Override
    public void insertTaskData(String itemDayId, JSONObject jsonObject) throws ParseException {
        BottleneckTaskDay taskDay = new BottleneckTaskDay();
        BottleneckTaskData taskData = new BottleneckTaskData();
        EntityWrapper<BottleneckTaskDay> ew = new EntityWrapper<>();

		/**
		 * 如果存在包含data,则说明解析成功,但不会包含progress,需要后台写入
		 */
		boolean isHasData = false;
        String data = jsonObject.getString("data");
        if(StringUtils.isNotBlank(data)){
			isHasData = true;
		}

        if(String.valueOf(itemDayId).length() > 8){
            String dateStr = itemDayId.substring(0,8);
            String id = itemDayId.substring(8,itemDayId.length());
            if(isHasData){
            	//1、开始 2、数据读取 3、机器学习建模 4、模型优化 5、反常属性检测 6、A/B检测 7、完成
				taskDay.setStep(7);
			}else{
				taskDay.setStep(Integer.parseInt(jsonObject.getString("progress")));
			}
            Date date = sdf.parse(dateStr);
            taskDay.setDate(date);
            taskDay.setTaskItemId(Integer.parseInt(id));
            ew.where("taskItemId={0}", Integer.parseInt(id)).and("date={0}", date);
        }else{

            taskDay.setTaskItemId(Integer.parseInt(itemDayId));
            taskDay.setDate(new Date());
            if(isHasData){
				taskDay.setStep(7);
			}else{
				taskDay.setStep(Integer.parseInt(jsonObject.getString("progress")));
			}
            ew.where("taskItemId={0}", Integer.parseInt(itemDayId));
        }

        List<BottleneckTaskDay> days = bottleneckTaskDayMapper.selectList(ew);

        if(evUtil.listIsNullOrZero(days)){
            bottleneckTaskDayMapper.insert(taskDay);
        }else{
            taskDay.setId(days.get(0).getId());
            bottleneckTaskDayMapper.updateAllColumnById(taskDay);
        }

		if(isHasData){
			taskData.setData(data);
			taskData.setItemDayId(taskDay.getId());

			EntityWrapper<BottleneckTaskData> entityWrapper = new EntityWrapper<>();
			entityWrapper.where("itemDayId={0}", taskDay.getId());
			List<BottleneckTaskData> taskDatas = bottleneckTaskDataMapper.selectList(entityWrapper);
			if(evUtil.listIsNullOrZero(taskDatas)){
				bottleneckTaskDataMapper.insert(taskData);
			}else{
				taskData.setId(taskDatas.get(0).getId());
				bottleneckTaskDataMapper.updateAllColumnById(taskData);
			}
		}

    }

	@Override
	public Integer addTask(JSONObject jsonObject,Integer userId) throws ParseException {
		String name=jsonObject.getString("name");
		Integer dataType=jsonObject.getInteger("dataType");
		Integer type=jsonObject.getInteger("type");
		Integer applicationId=jsonObject.getInteger("applicationId");
		Integer componentId=jsonObject.getInteger("componentId");
		Integer customId=jsonObject.getInteger("customId");
		int permission=jsonObject.getIntValue("permission");
		List<Integer> permissionIds=(List<Integer>) jsonObject.get("permissionIds");
		String desc =jsonObject.getString("desc");
		List<Long> timeRange=(List<Long>) jsonObject.get("timeRange");
		BottleneckTask entity=new BottleneckTask();
		entity.setName(name);
		entity.setType(type);
		entity.setDataType(dataType);
		entity.setApplicationId(applicationId);
		entity.setComponentId(componentId);
		entity.setCustomId(customId);
		entity.setPermission(permission);
		entity.setPermission(permission);
		entity.setDesc(desc);
		Date date=new Date();
		entity.setCreateTime(date);
		entity.setCreateBy(userId);
		bottleneckTaskMapper.insert(entity);
		//单次 -插入日期表
		if (!evUtil.listIsNullOrZero(timeRange)) {
			if (entity.getType().equals(0)) {
				BottleneckTaskDate dateParam = new BottleneckTaskDate();
				dateParam.setTaskId(entity.getId());
				Date endDate = new Date();
				endDate.setTime(timeRange.get(1));
				dateParam.setEndDate(endDate);
				Date startDate = new Date();
				startDate.setTime(timeRange.get(0));
				dateParam.setStartDate(startDate);
				bottleneckTaskDateMapper.insert(dateParam);
			}
		}
		//定时 -插入时间表
		if(entity.getType().equals(1)) {
			List<BottleneckTaskTime> timeList=new ArrayList<>();
			List<Map<String, Object>> timePart=(List<Map<String, Object>>) jsonObject.get("timePart");
			for (Map<String, Object> map : timePart) {
				List<String> time=(List<String>) map.get("time");
				if (!evUtil.listIsNullOrZero(time)) {
					BottleneckTaskTime timeParam = new BottleneckTaskTime();
					timeParam.setStartTime(timeFormat.parse(time.get(0)));
					timeParam.setEndTime(timeFormat.parse(time.get(1)));
					timeParam.setTaskId(entity.getId());
					timeList.add(timeParam);
				}
			}
			bottleneckTaskTimeMapper.batchInsert(timeList);
		}
		//插入人员表
		if(entity.getPermission().equals(2)) {
			bottleneckTaskUserMapper.batchInsert(permissionIds, entity.getId());
		}
		return entity.getId();
	}

	@Override
	public List<Map<String, Object>> selectTask(Map<String, Object> map) {
		int type=(int) map.get("type");
		List<Map<String, Object>> taskList=bottleneckTaskMapper.selectTask(map);
		if(!evUtil.listIsNullOrZero(taskList)) {
			for (Map<String, Object> map2 : taskList) {
				map2.put("expand", false);
				List<Map<String, Object>> children =new ArrayList<>();
				Map<String, Object> columnMap=new HashMap<>(1);
				columnMap.put("taskId", map2.get("id"));
				List<BottleneckTaskItem> itemList=new ArrayList<>();
				//报表类型 0单次 1 日报
				if(type==1) {
					itemList=bottleneckTaskItemMapper.selectByMap(columnMap);
				}
				if(type==0) {
					itemList=bottleneckTaskItemMapper.selectItemStep(columnMap);
				}
				if(!evUtil.listIsNullOrZero(itemList)) {
					for (BottleneckTaskItem bottleneckTaskItem : itemList) {
						Map<String, Object> map3=new HashMap<>(4);
						map3.put("id",bottleneckTaskItem.getId());
						map3.put("title", bottleneckTaskItem.getName());
						map3.put("expand", false);
						if(type==0) {
							int step=bottleneckTaskItem.getStep()==null?0:bottleneckTaskItem.getStep();
							int state;
							if(step==0) {
								state=2;
							}else if(step==7) {
								state=3;
							}else if(step<0) {
								state=4;
							}else {
								state=2;
							}
							map3.put("state", state);
						}
						children.add(map3);
					}
				}
				map2.put("children", children);
			}
		}
		return taskList;
	}

	@Override
	public List<Map<String, Object>> selectColumns(int id) {
		//字段类型 type 1 数字 2 类别	字段属性 property 1 维度 2 指标
		List<Map<String, Object>> result=new ArrayList<>();
		BottleneckTask task=bottleneckTaskMapper.selectById(id);
		int dateType=task.getDataType();
		if(dateType==1) {//bpm
			List<Map<String, Object>> dimension=protocolFieldMapper.selectDimensionNameBycomponentId(task.getComponentId());
			for (Map<String, Object> map : dimension) {
				Map<String, Object> map2 = new HashMap<>(4);
				map2.put("title", map.get("trueName"));
				map2.put("key", map.get("ename"));
				map2.put("unit", "");
				//字段类型 1 数字 2 类别
				map2.put("type", 2);
				//字段属性 1 维度 2 指标
				map2.put("property", 1);
				result.add(map2);
			}
			Map<String, Object> map2 = new HashMap<>(4);
			map2.put("title", "应用成功/失败");
			map2.put("key", "retStatus");
			map2.put("unit", "");
			map2.put("type", 2);
			map2.put("property", 2);
			result.add(map2);
		}
		if(dateType==2) {//事件
			List<Map<String, Object>> dimension=customMapper.selectCommonDimension(task.getCustomId());
			/*for (Map<String, Object> x : dimension) {
				String ename = x.get("ename").toString();
				if (!x.get("dimensionType").toString().equals("0")) {
					ename = ename + "_" + x.get("dimensionType").toString();
				}
				x.put("ename", ename);
			}*/
			for (Map<String, Object> map : dimension) {
				Map<String, Object> map2 = new HashMap<>(4);
				map2.put("title", map.get("trueName"));
				map2.put("key", map.get("ename"));
				map2.put("unit", "");
				map2.put("type", 2);
				map2.put("property", 1);
				result.add(map2);
			}
			Map<String, Object> map2 = new HashMap<>(4);
			map2.put("title", "事件成功/失败");
			map2.put("key", "businessStatus");
			map2.put("unit", "");
			map2.put("type", 2);
			map2.put("property", 2);
			result.add(map2);
			map2 = new HashMap<>(4);
			map2.put("title", "金额 ");
			map2.put("key", "money");
			map2.put("unit", "");
			map2.put("type", 1);
			map2.put("property", 2);
			result.add(map2);
		}
		Map<String, Object> map2 = new HashMap<>(4);
		map2.put("title", "响应/无响应");
		map2.put("key", "existResponse");
		map2.put("unit", "");
		map2.put("type", 2);
		map2.put("property", 2);
		result.add(map2);
		map2 = new HashMap<>(4);
		map2.put("title", "请求字节");
		map2.put("key", "bytesLength");
		map2.put("unit", "");
		map2.put("type", 1);
		map2.put("property", 2);
		result.add(map2);
		map2 = new HashMap<>(4);
		map2.put("title", "响应字节");
		map2.put("key", "respBytesLength");
		map2.put("unit", "");
		map2.put("type", 1);
		map2.put("property", 2);
		result.add(map2);
		map2 = new HashMap<>(4);
		map2.put("title", "响应时间");
		map2.put("key", "responseTime");
		map2.put("unit", "ms");
		map2.put("type", 1);
		map2.put("property", 2);
		result.add(map2);
		return result;
	}

	@Override
	public void updateTask(JSONObject jsonObject, Integer userId) {
		int id=jsonObject.getIntValue("id");
		String name=jsonObject.getString("name");
		int permission=jsonObject.getIntValue("permission");
		List<Integer> permissionIds=(List<Integer>) jsonObject.get("permissionIds");
		String desc =jsonObject.getString("desc");
		BottleneckTask entity=new BottleneckTask();
		entity.setId(id);
		entity.setName(name);
		entity.setPermission(permission);
		entity.setDesc(desc);
		entity.setUpdateBy(userId);
		entity.setUpdateTime(new Date());
		bottleneckTaskMapper.updateById(entity);
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("taskId", id);
		bottleneckTaskUserMapper.deleteByMap(columnMap);
		if (permission == 2) {
			bottleneckTaskUserMapper.batchInsert(permissionIds, id);
		}
	}

	@Override
	public void deleteTask(int id) {
		EntityWrapper<BottleneckTaskItem> wrapper=new EntityWrapper<>();
		wrapper.where("taskId={0}", id);
		List<BottleneckTaskItem> items=bottleneckTaskItemMapper.selectList(wrapper);
		List<Integer> itemIds=items.stream().map(BottleneckTaskItem::getId).collect(Collectors.toList());
		deleteItem(itemIds);
		bottleneckTaskMapper.deleteById(id);
		Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("taskId", id);
		bottleneckTaskDateMapper.deleteByMap(columnMap);
		bottleneckTaskTimeMapper.deleteByMap(columnMap);
		bottleneckTaskUserMapper.deleteByMap(columnMap);
	}

	@Override
	public Integer addItem(JSONObject jsonObject) {
		String name=jsonObject.getString("name");
		int taskId=jsonObject.getIntValue("datalistId");
		int active=jsonObject.getIntValue("active");
		BottleneckTaskItem entity=new BottleneckTaskItem(name, taskId, active);
		bottleneckTaskItemMapper.insert(entity);
		//字段表
		List<Map<String, Object>> fieldList=(List<Map<String, Object>>) jsonObject.get("field");
		List<BottleneckTaskAnalyzeField> field=new ArrayList<>();
		for (Map<String, Object> map : fieldList) {
			Integer taskItemId=entity.getId();
			String fieldName =evUtil.getMapStrValue(map, "name");
			String key =evUtil.getMapStrValue(map, "key");
			int type=evUtil.getMapIntValue(map, "type");
			int property=evUtil.getMapIntValue(map, "property");
			int use=evUtil.getMapIntValue(map, "use");
			BottleneckTaskAnalyzeField f=new BottleneckTaskAnalyzeField(taskItemId, fieldName, key, use, type, property);
			field.add(f);
		}
		bottleneckTaskAnalyzeFieldMapper.batchInsert(field);
		//规则和值
		Map<String, Object> ruleThreshold=(Map<String, Object>) jsonObject.get("ruleThreshold");
		Map<String, Object> ruleCategory=(Map<String, Object>) jsonObject.get("ruleCategory");
		if(!CollectionUtils.isEmpty(ruleCategory)) {
			String key=evUtil.getMapStrValue(ruleCategory, "key");
			int rule=evUtil.getMapIntValue(ruleCategory, "rule");
			String value=evUtil.getMapStrValue(ruleCategory, "value");
			/*Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("key", key);
			columnMap.put("taskItemId", entity.getId());*/
			BottleneckTaskAnalyzeField wra=new BottleneckTaskAnalyzeField();
			wra.setKey(key);
			wra.setTaskItemId(entity.getId());
			EntityWrapper<BottleneckTaskAnalyzeField> BottleneckTaskAnalyzeField=new EntityWrapper<>();
			BottleneckTaskAnalyzeField.setEntity(wra);
			List<BottleneckTaskAnalyzeField> fieldOne=bottleneckTaskAnalyzeFieldMapper.selectList(BottleneckTaskAnalyzeField);
			int analyzeFieldId = 0;
			if (!evUtil.listIsNullOrZero(fieldOne)) {
				analyzeFieldId=fieldOne.get(0).getId();
			}else {
				log.error("未找到对应Field的id,key:'"+key+"'"+",taskItemId: "+entity.getId());
			}
			BottleneckTaskAnalyzeRule ruleParam=new BottleneckTaskAnalyzeRule();
			ruleParam.setAnalyzeFieldId(analyzeFieldId);
			ruleParam.setAnalyzeRule(rule);
			bottleneckTaskAnalyzeRuleMapper.insert(ruleParam);
			BottleneckTaskAnalyzeRuleValue valueParam =new BottleneckTaskAnalyzeRuleValue();
			valueParam.setAnalyzeRuleId(ruleParam.getId());
			valueParam.setValue(value);
			bottleneckTaskAnalyzeRuleValueMapper.insert(valueParam );
		}
		if(!CollectionUtils.isEmpty(ruleThreshold)) {
			String key=evUtil.getMapStrValue(ruleThreshold, "key");
			int rule=evUtil.getMapIntValue(ruleThreshold, "rule");
			if(rule==1) {
				rule=4;
			}if(rule==2) {
				rule=3;
			}
			int value=evUtil.getMapIntValue(ruleThreshold, "value");
			/*Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("key", key);
			columnMap.put("taskItemId", entity.getId());*/
			BottleneckTaskAnalyzeField wra=new BottleneckTaskAnalyzeField();
			wra.setKey(key);
			wra.setTaskItemId(entity.getId());
			EntityWrapper<BottleneckTaskAnalyzeField> BottleneckTaskAnalyzeField=new EntityWrapper<>();
			BottleneckTaskAnalyzeField.setEntity(wra);
			List<BottleneckTaskAnalyzeField> fieldOne=bottleneckTaskAnalyzeFieldMapper.selectList(BottleneckTaskAnalyzeField);
			int analyzeFieldId = 0;
			if (!evUtil.listIsNullOrZero(fieldOne)) {
				analyzeFieldId=fieldOne.get(0).getId();
			}else {
				log.error("未找到对应Field的id,key:'"+key+"'"+",taskItemId: "+entity.getId());
			}
			BottleneckTaskAnalyzeRule ruleParam=new BottleneckTaskAnalyzeRule();
			ruleParam.setAnalyzeFieldId(analyzeFieldId);
			ruleParam.setAnalyzeRule(rule);
			bottleneckTaskAnalyzeRuleMapper.insert(ruleParam);
			BottleneckTaskAnalyzeRuleThreshold thresholdParam =new BottleneckTaskAnalyzeRuleThreshold();
			thresholdParam.setAnalyzeRuleId(ruleParam.getId());
			thresholdParam.setAnalyzeThreshold(value);
			bottleneckTaskAnalyzeRuleThresholdMapper.insert(thresholdParam);
		}
		return entity.getId();
		
	}

	@Override
	public void updateItem(JSONObject jsonObject) {
		BottleneckTaskItem entity=new BottleneckTaskItem();
		entity.setId(jsonObject.getInteger("id"));
		entity.setName(jsonObject.getString("name"));
		entity.setActive(jsonObject.getInteger("active"));
		bottleneckTaskItemMapper.updateById(entity);
	}

	@Override
	public Map<String, Object> getItemDetail(int id) {
		Map<String, Object> resault= new HashMap<>();
		BottleneckTaskItem item=bottleneckTaskItemMapper.selectById(id);
		resault.put("datalistId", item.getTaskId());
		resault.put("name", item.getName());
		resault.put("active", item.getActive());
		List<Map<String, Object>> field=new ArrayList<>();
		Map<String, Object> ruleThreshold= new HashMap<>(3);
		Map<String, Object> ruleCategory= new HashMap<>(3);
		Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("taskItemId", id);
		List<BottleneckTaskAnalyzeField> fieldList=bottleneckTaskAnalyzeFieldMapper.selectByMap(columnMap);
		for (BottleneckTaskAnalyzeField b : fieldList) {
			Map<String, Object> detail=new HashMap<>();
			detail.put("key", b.getKey());
			detail.put("use", b.getUse());
			detail.put("name", b.getName());
			detail.put("type", b.getType());
			detail.put("property", b.getProperty());
			if(b.getKey().equals("responseTime")) {
				detail.put("unit", "ms");
			}else {
				detail.put("unit", "");
			}
			field.add(detail);
			// 目标属性
			if (b.getUse().equals(1)) {
				int analyzeFieldId = b.getId();
				Map<String, Object> col = new HashMap<>(1);
				col.put("analyzeFieldId", analyzeFieldId);
				List<BottleneckTaskAnalyzeRule> rule = bottleneckTaskAnalyzeRuleMapper.selectByMap(col);
				if (!evUtil.listIsNullOrZero(rule)) {
					int analyzeRule = rule.get(0).getAnalyzeRule();
					int analyzeRuleId = rule.get(0).getId();
				    col = new HashMap<>(1);
					col.put("analyzeRuleId", analyzeRuleId);
					if (analyzeRule == 1 || analyzeRule == 2) {
						List<BottleneckTaskAnalyzeRuleValue> threshold = bottleneckTaskAnalyzeRuleValueMapper
								.selectByMap(col);
						if (!evUtil.listIsNullOrZero(threshold)) {
							ruleCategory.put("key", b.getKey());
							ruleCategory.put("rule", analyzeRule);
							ruleCategory.put("value", threshold.get(0).getValue());
						}
					}
					if (analyzeRule == 3 || analyzeRule == 4) {
						List<BottleneckTaskAnalyzeRuleThreshold> threshold = bottleneckTaskAnalyzeRuleThresholdMapper
								.selectByMap(col);
						if (!evUtil.listIsNullOrZero(threshold)) {
							ruleThreshold.put("key", b.getKey());
							if(analyzeRule == 3) {
								analyzeRule=2;
							}
							if(analyzeRule == 4) {
								analyzeRule=1;
							}
							ruleThreshold.put("rule", analyzeRule);
							ruleThreshold.put("value", threshold.get(0).getAnalyzeThreshold());
						}
					}
				}
			}
		}
		resault.put("ruleThreshold", ruleThreshold);
		resault.put("ruleCategory", ruleCategory);
		resault.put("field", field);
		return resault;
	}

	@Override
	public void deleteItem(List<Integer> ids) {
		if(evUtil.listIsNullOrZero(ids)) {
			return;
		}
		EntityWrapper<BottleneckTaskDay> daywra=new EntityWrapper<>();
		daywra.in("taskItemId", ids);
		List<BottleneckTaskDay> days=bottleneckTaskDayMapper.selectList(daywra);
		List<Integer> itemDayIds = days.stream().map(BottleneckTaskDay::getId).collect(Collectors.toList());
		EntityWrapper<BottleneckTaskData> datawra=new EntityWrapper<>();
		datawra.in("itemDayId", itemDayIds);
		bottleneckTaskDataMapper.delete(datawra);
		if(!evUtil.listIsNullOrZero(itemDayIds)) {
			bottleneckTaskDayMapper.deleteBatchIds(itemDayIds);
		}
		
		EntityWrapper<BottleneckTaskAnalyzeField> fiewra=new EntityWrapper<>();
		fiewra.in("taskItemId", ids);
		List<BottleneckTaskAnalyzeField> fies=bottleneckTaskAnalyzeFieldMapper.selectList(fiewra);
		List<Integer> analyzeFieldIds= fies.stream().map(BottleneckTaskAnalyzeField::getId).collect(Collectors.toList());
		EntityWrapper<BottleneckTaskAnalyzeRule> rulewra=new EntityWrapper<>();
		rulewra.in("analyzeFieldId", analyzeFieldIds);
		List<BottleneckTaskAnalyzeRule> rules=bottleneckTaskAnalyzeRuleMapper.selectList(rulewra);
		List<Integer> analyzeRuleIds= rules.stream().map(BottleneckTaskAnalyzeRule::getId).collect(Collectors.toList());
		EntityWrapper<BottleneckTaskAnalyzeRuleThreshold> threswra=new EntityWrapper<>();
		threswra.in("analyzeRuleId", analyzeRuleIds);
		bottleneckTaskAnalyzeRuleThresholdMapper.delete(threswra);
		EntityWrapper<BottleneckTaskAnalyzeRuleValue> valuewra=new EntityWrapper<>();
		valuewra.in("analyzeRuleId", analyzeRuleIds);
		bottleneckTaskAnalyzeRuleValueMapper.delete(valuewra);
		if(!evUtil.listIsNullOrZero(analyzeRuleIds)) {
			bottleneckTaskAnalyzeRuleMapper.deleteBatchIds(analyzeRuleIds);
		}
		if(!evUtil.listIsNullOrZero(analyzeFieldIds)) {
			bottleneckTaskAnalyzeFieldMapper.deleteBatchIds(analyzeFieldIds);
		}
		if(!evUtil.listIsNullOrZero(ids)) {
			bottleneckTaskItemMapper.deleteBatchIds(ids);
		}
		
	}

	@Override
	public void updateStep(String itemDayId) throws ParseException {
		BottleneckTaskDay taskDay = new BottleneckTaskDay();
		EntityWrapper<BottleneckTaskDay> ew = new EntityWrapper<>();
		if (String.valueOf(itemDayId).length() > 8) {
			String dateStr = itemDayId.substring(0, 8);
			String id = itemDayId.substring(8, itemDayId.length());
			taskDay.setStep(0);
			Date date = sdf.parse(dateStr);
			taskDay.setDate(date);
			taskDay.setTaskItemId(Integer.parseInt(id));
			ew.where("taskItemId={0}", Integer.parseInt(id)).and("date={0}", date);
		} else {
			taskDay.setTaskItemId(Integer.parseInt(itemDayId));
			taskDay.setDate(null);
			taskDay.setStep(0);
			ew.where("taskItemId={0}", Integer.parseInt(itemDayId));
		}

		List<BottleneckTaskDay> days = bottleneckTaskDayMapper.selectList(ew);

		if (evUtil.listIsNullOrZero(days)) {
			bottleneckTaskDayMapper.insert(taskDay);
		} else {
			taskDay.setId(days.get(0).getId());
			bottleneckTaskDayMapper.updateAllColumnById(taskDay);
		}
	}

	@Override
	public List<Long> selectDate(int id) {
		Map<String, Object> columnMap= new HashMap<>();
		columnMap.put("taskItemId", id);
		List<BottleneckTaskDay> day=bottleneckTaskDayMapper.selectByMap(columnMap);
		List<Long> dates=new ArrayList<>();
		for (BottleneckTaskDay bottleneckTaskDay : day) {
			dates.add(bottleneckTaskDay.getDate().getTime());
		}
		return dates;
	}

	@Override
	public Map<String, Object> getStep(String itemDayId) throws ParseException {
		EntityWrapper<BottleneckTaskDay> ew = new EntityWrapper<>();
		if (String.valueOf(itemDayId).length() > 8) {
			String dateStr = itemDayId.substring(0, 8);
			String id = itemDayId.substring(8, itemDayId.length());
			Date date = sdf.parse(dateStr);
			ew.where("taskItemId={0}", Integer.parseInt(id)).and("date={0}", date);
		} else {
			ew.where("taskItemId={0}", Integer.parseInt(itemDayId));
		}

		List<BottleneckTaskDay> days = bottleneckTaskDayMapper.selectList(ew);
		Map<String, Object> map=new HashMap<>();
		if (evUtil.listIsNullOrZero(days)) {
			map.put("step", 0);
			map.put("state", 1);
		} else {
			map.put("step", days.get(0).getStep());
			if (days.get(0).getStep().equals(7)) {
				map.put("state", 3);
			}else if(days.get(0).getStep().intValue()<0) {
				map.put("state", 4);
			}
			else {
				map.put("state", 2);
			}
		}
		return map;
	}


	@Override
	public Map<String, Object> selectTaskDetail(Map<String, Object> map) {
		int id=(int) map.get("id");
		String time=evUtil.getMapStrValue(map, "time");
		Map<String, Object> result=new HashMap<>();
		BottleneckTask task=bottleneckTaskMapper.selectById(id);
		result.put("name", task.getName());
		result.put("dataType", task.getDataType());
		result.put("applicationId", task.getApplicationId());
		result.put("componentId", task.getComponentId());
		result.put("customId", task.getCustomId());
		result.put("type", task.getType());
		result.put("permission", task.getPermission());
		result.put("desc", task.getDesc());
		//若共享范围是部分人
		if(task.getPermission().equals(2)) {
			EntityWrapper<BottleneckTaskUser> wrapper=new EntityWrapper<>();
			wrapper.where("taskId={0}", id);
			List<BottleneckTaskUser> user=bottleneckTaskUserMapper.selectList(wrapper);
			List<Integer> permissionIds = user.stream().map(BottleneckTaskUser::getUserId).collect(Collectors.toList());
			result.put("permissionIds", permissionIds);
		}
		//单次
		if(task.getType().equals(0)) {
			Map<String, Object> columnMap=new HashMap<>(1);
			columnMap.put("taskId", id);
			List<BottleneckTaskDate> datelist=bottleneckTaskDateMapper.selectByMap(columnMap);
			if (!evUtil.listIsNullOrZero(datelist)) {
				List<Long> date=new ArrayList<>();
				date.add(datelist.get(0).getStartDate().getTime());
				date.add(datelist.get(0).getEndDate().getTime());
				result.put("singleTime", date);
			}
		}else {
			Map<String, Object> columnMap=new HashMap<>(1);
			columnMap.put("taskId", id);
			List<BottleneckTaskTime> timelist=bottleneckTaskTimeMapper.selectByMap(columnMap);
			if (!evUtil.listIsNullOrZero(timelist)) {
				List<List<String>> time1=new ArrayList<>();
				for (BottleneckTaskTime bottleneckTaskTime : timelist) {
					List<String> time2=new ArrayList<>();
					time2.add(timeFormat.format(bottleneckTaskTime.getStartTime()));
					time2.add(timeFormat.format(bottleneckTaskTime.getEndTime()));
					time1.add(time2);
				}
				result.put("timePart", time1);
			}
			result.put("timingTime", time);
		}
		return result;
	}
	
	@Override
	public Boolean checkTaskName(Integer id,String name, int flag) {
		EntityWrapper<BottleneckTask> wrapper=new EntityWrapper<>();
		wrapper.where("name={0}", name);
		List<BottleneckTask> list = new ArrayList<>();
		if (flag == 1) {//添加
			list = bottleneckTaskMapper.selectList(wrapper);
		} else {
			wrapper.ne("id", id);
			list = bottleneckTaskMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);

	}
	
	@Override
	public Boolean checkItemName(Integer id,String name, int flag) {
		EntityWrapper<BottleneckTaskItem> wrapper=new EntityWrapper<>();
		wrapper.where("name={0}", name);
		List<BottleneckTaskItem> list = new ArrayList<>();
		if (flag == 1) {//添加
			list = bottleneckTaskItemMapper.selectList(wrapper);
		} else {
			wrapper.ne("id", id);
			list = bottleneckTaskItemMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);

	}
}
