package com.greattimes.ev.bpm.service.analysis.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.mapper.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartDataParam;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartDimensionParam;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartIndicatorFilterParam;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartParam;
import com.greattimes.ev.bpm.analysis.param.req.ReportDimensionParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportChartDataRespParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportChartDimensionRespParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportChartLocationParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportDescriptionLocationParam;
import com.greattimes.ev.bpm.entity.Application;
import com.greattimes.ev.bpm.entity.Component;
import com.greattimes.ev.bpm.entity.Custom;
import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.entity.Monitor;
import com.greattimes.ev.bpm.entity.Report;
import com.greattimes.ev.bpm.entity.ReportChart;
import com.greattimes.ev.bpm.entity.ReportChartData;
import com.greattimes.ev.bpm.entity.ReportChartDataDimension;
import com.greattimes.ev.bpm.entity.ReportChartDataFilter;
import com.greattimes.ev.bpm.entity.ReportChartDataIndicator;
import com.greattimes.ev.bpm.entity.ReportChartIndicator;
import com.greattimes.ev.bpm.entity.ReportChartIndicatorFilter;
import com.greattimes.ev.bpm.entity.ReportChartLocation;
import com.greattimes.ev.bpm.entity.ReportChartRelation;
import com.greattimes.ev.bpm.entity.ReportChartTableField;
import com.greattimes.ev.bpm.entity.ReportChartUser;
import com.greattimes.ev.bpm.entity.ReportDescription;
import com.greattimes.ev.bpm.entity.ReportDescriptionLocation;
import com.greattimes.ev.bpm.entity.ReportUser;
import com.greattimes.ev.bpm.entity.Server;
import com.greattimes.ev.bpm.entity.ServerIp;
import com.greattimes.ev.bpm.mapper.ApplicationMapper;
import com.greattimes.ev.bpm.mapper.ComponentMapper;
import com.greattimes.ev.bpm.mapper.CustomMapper;
import com.greattimes.ev.bpm.mapper.FieldMappingMapper;
import com.greattimes.ev.bpm.mapper.IndicatorMapper;
import com.greattimes.ev.bpm.mapper.MonitorMapper;
import com.greattimes.ev.bpm.mapper.ProtocolFieldMapper;
import com.greattimes.ev.bpm.mapper.ReportChartDataDimensionMapper;
import com.greattimes.ev.bpm.mapper.ReportChartDataFilterMapper;
import com.greattimes.ev.bpm.mapper.ReportChartDataIndicatorMapper;
import com.greattimes.ev.bpm.mapper.ReportChartDataMapper;
import com.greattimes.ev.bpm.mapper.ReportChartIndicatorFilterMapper;
import com.greattimes.ev.bpm.mapper.ReportChartIndicatorMapper;
import com.greattimes.ev.bpm.mapper.ReportChartLocationMapper;
import com.greattimes.ev.bpm.mapper.ReportChartMapper;
import com.greattimes.ev.bpm.mapper.ReportChartRelationMapper;
import com.greattimes.ev.bpm.mapper.ReportChartTableFieldMapper;
import com.greattimes.ev.bpm.mapper.ReportChartUserMapper;
import com.greattimes.ev.bpm.mapper.ReportDescriptionLocationMapper;
import com.greattimes.ev.bpm.mapper.ReportDescriptionMapper;
import com.greattimes.ev.bpm.mapper.ReportMailUserMapper;
import com.greattimes.ev.bpm.mapper.ReportMapper;
import com.greattimes.ev.bpm.mapper.ReportUserMapper;
import com.greattimes.ev.bpm.mapper.ServerIpMapper;
import com.greattimes.ev.bpm.mapper.ServerMapper;
import com.greattimes.ev.bpm.mapper.ServerPortMapper;
import com.greattimes.ev.bpm.service.analysis.IReportService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 智能报表表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@Service
public class ReportServiceImpl extends ServiceImpl<ReportMapper, Report> implements IReportService {
	@Autowired
	private ReportMapper reportMapper;
	@Autowired
	private ReportUserMapper reportUserMapper;
	@Autowired
	private ReportChartRelationMapper reportChartRelationMapper;
	@Autowired
	private ReportDescriptionMapper reportDescriptionMapper;
	@Autowired
	private ReportMailUserMapper reportMailUserMapper;
	@Autowired
	private ReportChartLocationMapper reportChartLocationMapper;
	@Autowired
	private ReportDescriptionLocationMapper reportDescriptionLocationMapper;
	@Autowired
	private ReportChartMapper reportChartMapper;
    @Autowired
    private ReportChartUserMapper reportChartUserMapper;
    @Autowired
    private ReportChartIndicatorMapper reportChartIndicatorMapper;
    @Autowired
    private ReportChartDataFilterMapper reportChartDataFilterMapper;
    @Autowired
    private ReportChartDataDimensionMapper reportChartDataDimensionMapper;
    @Autowired
    private ReportChartDataMapper reportChartDataMapper;
    @Autowired
    private ReportChartDataIndicatorMapper reportChartDataIndicatorMapper;
    @Autowired
    private ApplicationMapper applicationMapper;
    @Autowired
    private MonitorMapper monitorMapper;
    @Autowired
    private ComponentMapper componentMapper;
	@Autowired
    private ServerIpMapper serverIpMapper;
	@Autowired
	private ServerPortMapper serverPortMapper;
	@Autowired
	private ServerMapper serverMapper;
	@Autowired
	private CustomMapper customMapper;
	@Autowired
	private ProtocolFieldMapper protocolFieldMapper;
	@Autowired
	private IndicatorMapper indicatorMapper;
	@Autowired
	private FieldMappingMapper fieldMappingMapper;
	@Autowired
	private ReportChartIndicatorFilterMapper reportChartIndicatorFilterMapper;
	@Autowired
	private ReportChartTableFieldMapper reportChartTableFiledMapper;
	@Autowired
	private ReportChartLineColorMapper reportChartLineColorMapper;


	@Override
	public boolean checkReportName(Integer id, String name, int flag) {
		EntityWrapper<Report> wrapper=new EntityWrapper<>();
		wrapper.where("name={0}", name);
		List<Report> list = new ArrayList<>();
		if (flag == 1) {//添加
			list = reportMapper.selectList(wrapper);
		} else {
			wrapper.ne("id", id);
			list = reportMapper.selectList(wrapper);
		}
		return evUtil.listIsNullOrZero(list);

	}

	@Override
	public Integer addReport(JSONObject jsonObject, Integer userId) {
		Date date=new Date();
		Report entity=new Report();
		List<Integer> permissionIds=(List<Integer>) jsonObject.get("groupIds");
		entity.setName(jsonObject.getString("name"));
		entity.setCreateBy(userId);
		entity.setCreateTime(date);
		entity.setUpdateTime(date);
		entity.setPermission(jsonObject.getIntValue("permission"));
		reportMapper.insert(entity);
		// 插入人员表
		if (entity.getPermission().equals(2)) {
			reportUserMapper.batchInsert(permissionIds, entity.getId());
		}
		return entity.getId();
	}

	@Override
	public void editReport(JSONObject jsonObject, Integer userId) {
		Date date=new Date();
		int id=jsonObject.getIntValue("id");
		String name=jsonObject.getString("name");
		int permission=jsonObject.getIntValue("permission");
		List<Integer> permissionIds=(List<Integer>) jsonObject.get("groupIds");
		Report entity=new Report();
		entity.setId(id);
		entity.setName(name);
		entity.setPermission(permission);
		entity.setUpdateTime(date);
		reportMapper.updateById(entity);
		Map<String, Object> columnMap = new HashMap<>(1);
		columnMap.put("reportId", id);
		reportUserMapper.deleteByMap(columnMap);
		if (permission == 2) {
			reportUserMapper.batchInsert(permissionIds, id);
		}
	}

	@Override
	public Map<String, Object> queryReport(Integer id) {
		Map<String, Object> map=new HashMap<>(4);
		Report report=reportMapper.selectById(id);
		map.put("id", id);
		map.put("name", report.getName());
		map.put("permission", report.getPermission());
		if(report.getPermission().equals(2)) {
			EntityWrapper<ReportUser> wrapper=new EntityWrapper<>();
			wrapper.eq("reportId", id);
			List<ReportUser> groupList=reportUserMapper.selectList(wrapper);
			List<Integer> groupIds=groupList.stream().map(ReportUser::getUserGroupId).collect(Collectors.toList());
			map.put("groupIds", groupIds);
		}
		return map;
	}

	@Override
	public void deleteReport(Integer id) {
		reportMapper.deleteById(id);
		Map<String, Object> columnMap=new HashMap<>();
		columnMap.put("reportId",id);
		//删除图报表关系
		reportChartRelationMapper.deleteByMap(columnMap);
		//删除报表人员
		reportUserMapper.deleteByMap(columnMap);
		//删除描述
		reportDescriptionMapper.deleteByMap(columnMap);
		//删除邮件发送
		reportMailUserMapper.deleteByMap(columnMap);
	}

	@Override
	public List<Map<String, Object>> queryReportList(Map<String, Object> map) {
		List<Map<String, Object>> result=new ArrayList<>();
		//报表所属类型0我的1全部
		int type=evUtil.getMapIntValue(map, "type");
		//按照更新时间排序0升序1降序
		int orderBy=evUtil.getMapIntValue(map, "orderBy");
		int userId=evUtil.getMapIntValue(map, "userId");
		if(type==0) {
			result=reportMapper.getMyReport(userId,orderBy);
		}else {
			result=reportMapper.getAllReport(userId,orderBy);
		}
		return result;
	}

	@Override
	public Integer addDescription(ReportDescription description) {
		reportDescriptionMapper.insert(description);
		return description.getId();
	}

	@Override
	public void editDescription(ReportDescription description) {
		reportDescriptionMapper.updateById(description);
	}

	@Override
	public void deleteDescription(Integer id) {
		reportDescriptionMapper.deleteById(id);
		Map<String, Object> columnMap=new HashMap<>();
		columnMap.put("descriptionId", id);
		reportDescriptionLocationMapper.deleteByMap(columnMap);
	}

	@Override
	public List<Map<String, Object>> queryDescriptionList(Integer reportId) {
		List<Map<String, Object>> result=reportDescriptionMapper.selectDescriptionList(reportId);
		for (Map<String, Object> map : result) {
			map.put("isChart", false);
		}
		return result;
	}

	@Override
	public void insertChartRelation(Integer reportId, List<Map<String, Object>> charts) {
		for (Map<String, Object> map : charts) {
			ReportChartRelation entity = new ReportChartRelation();
			entity.setChartId(evUtil.getMapIntegerValue(map, "id"));
			entity.setReportId(reportId);
			EntityWrapper<ReportChartRelation> wrapper = new EntityWrapper<>();
			wrapper.setEntity(entity);
			//防止重复保存
			if (reportChartRelationMapper.selectCount(wrapper) == 0) {
				reportChartRelationMapper.insert(entity);
				ReportChartLocation en = new ReportChartLocation();
				en.setChartRelationId(entity.getId());
				en.setHeight(evUtil.getMapDoubleValue(map, "h"));
				en.setLeft(evUtil.getMapDoubleValue(map, "x"));
				en.setTop(evUtil.getMapDoubleValue(map, "y"));
				en.setWidth(evUtil.getMapDoubleValue(map, "w"));
				reportChartLocationMapper.insert(en);
			}
		}
	}

    @Override
	public void deleteChartRelation(Integer reportId, Integer id) {
    	EntityWrapper<ReportChartRelation> wrapper=new EntityWrapper<>();
		wrapper.eq("reportId", reportId);
		wrapper.eq("chartId", id);
		List<ReportChartRelation> relationList= reportChartRelationMapper.selectList(wrapper);
		if(!evUtil.listIsNullOrZero(relationList)) {
			Map<String, Object> columnMap=new HashMap<>();
			columnMap.put("chartRelationId",relationList.get(0).getId());
			reportChartLocationMapper.deleteByMap(columnMap);
		}
		reportChartRelationMapper.delete(wrapper);
	}

	@Override
	public List<Map<String, Object>> queryAvailableChart(Map<String, Object> map) {
		List<Integer> chartIds=new ArrayList<>();
		List<Integer> eventchartIds=new ArrayList<>();
		List<Map<String, Object>> result=new ArrayList<>();
		//报表所属类型0我的1全部
		int type=evUtil.getMapIntValue(map, "type");
		//按照更新时间排序0升序1降序
		int orderBy=evUtil.getMapIntValue(map, "orderBy");
		int userId=evUtil.getMapIntValue(map, "userId");
		int dataType=evUtil.getMapIntValue(map, "dataType");
		if(type==0) {
			result=reportMapper.getMyChart(userId,orderBy,dataType);
		}else {
			result=reportMapper.getAllChart(userId,orderBy,dataType);
		}
		for (Map<String, Object> map2 : result) {
			map2.put("isChart", true);
			//数据类型 1 应用kpi 2 应用维度 3 事件kpi 4 事件维度
			if(dataType==1||dataType==2) {
				chartIds.add((Integer) map2.get("id"));
			}else if(dataType==3||dataType==4) {
				eventchartIds.add((Integer) map2.get("id"));
			}
		}
		addChartConfig(chartIds, eventchartIds, result);
		return result;
	}

	@Override
	public void saveLocation(List<ReportDescriptionLocationParam> description, List<ReportChartLocationParam> chart) {
		if(!evUtil.listIsNullOrZero(description)) {
			reportDescriptionLocationMapper.batchUpdate(description);
		}
		//找charRetaiontId
		/*for (ReportChartLocation reportChartLocation : chart) {
			ReportChartRelation entity=new ReportChartRelation();
			entity.setChartId(reportChartLocation.getChartId());
			entity.setReportId(reportChartLocation.getReportId());
			Integer charRetaiontId;
			if(null!=reportChartRelationMapper.selectOne(entity)) {
				charRetaiontId = reportChartRelationMapper.selectOne(entity).getId();
			}
		}*/
		if(!evUtil.listIsNullOrZero(chart)) {
			reportChartLocationMapper.batchUpdate(chart);
		}
	}

	@Override
	public void saveDescriptionLocation(ReportDescriptionLocation lo) {
		reportDescriptionLocationMapper.insert(lo);

	}

	@Override
	public List<Map<String, Object>> queryChartList(Integer reportId,Integer userId) {
		List<Integer> chartIds=new ArrayList<>();
		List<Integer> eventchartIds=new ArrayList<>();
		List<Map<String, Object>> result=reportChartMapper.queryChartList(reportId,userId);
		for (Map<String, Object> map : result) {
			map.put("authority", map.get("authority"));
			map.put("isChart", true);
			// 数据类型 1 2应用 3 4 事件 
			Integer dataType=evUtil.getMapIntegerValue(map, "dataType");
			if (null != dataType) {
				if (dataType==1||dataType==2) {
					chartIds.add((Integer) map.get("id"));
				} else if (dataType==3||dataType==4) {
					eventchartIds.add((Integer) map.get("id"));
				}
			} else {// 已经被删除的图
				map.put("eventConfig", null);
				map.put("applicationConfig", null);
			}
		}
		addChartConfig(chartIds, eventchartIds, result);
		return result;
	}

	private void addChartConfig(List<Integer> chartIds, List<Integer> eventchartIds, List<Map<String, Object>> result) {
		List<ReportChartParam> configList =new ArrayList<>();
		List<ReportChartParam> eventConfigList=new ArrayList<>();
		//应用()图配置
		if(!evUtil.listIsNullOrZero(chartIds)) {
			configList = reportChartMapper.selectReportChartConfig(chartIds);
		}
		if(!evUtil.listIsNullOrZero(eventchartIds)) {
		//事件()图配置
			eventConfigList = reportChartMapper.selectEventReportChartConfig(eventchartIds);
		}
		if(!evUtil.listIsNullOrZero(eventConfigList)) {
			for (Map<String, Object> map : result) {
				for (ReportChartParam event : eventConfigList) {
					if(map.get("id").toString().equals(event.getId().toString())) {
						map.put("eventConfig", event);
						break;
					}
				}
			}
		}
		if(!evUtil.listIsNullOrZero(configList)) {
			for (Map<String, Object> map : result) {
				for (ReportChartParam config : configList) {
					if(map.get("id").toString().equals(config.getId().toString())) {
						map.put("applicationConfig", config);
						break;
					}
				}
			}
		}
	}


	@Override
	public boolean checkReportCharName(Integer Id, Integer userId, String name) {
		boolean isRepeat = false;
		EntityWrapper<ReportChart> entityWrapper = new EntityWrapper<>();
		if(Id == null){
			//add
			entityWrapper.where("createBy={0}", userId).and("name={0}", name);
		}else{
			entityWrapper.where("createBy={0}", userId).and("name={0}", name)
			.ne("id", Id);
		}
		List<ReportChart> reportCharts = reportChartMapper.selectList(entityWrapper);
		if(!evUtil.listIsNullOrZero(reportCharts)){
			isRepeat = true;
		}
		return isRepeat;
	}

	@Override
    public ReportChart saveOrUpdateReportChart(ReportChartParam param) {
		boolean isAdd = param.getId() == null ? true : false;
		/**
		 * 数据类型 1 应用 2 事件 3 漏斗
		 */
		int dataType = param.getDataType();
		int reportChartId;
		Date nowTime  = new Date();
		ReportChart reportChart = new ReportChart();
		if(isAdd){
			//ai_report_chart
			BeanUtils.copyProperties(param, reportChart);
			//自定义属性的赋值
            reportChart.setCreateTime(nowTime);
            reportChart.setUpdateTime(nowTime);
			reportChartMapper.insert(reportChart);
			reportChartId = reportChart.getId();
			//add return reportCHat
			param.setId(reportChartId);
			//ai_report_chart_relation
			ReportChartRelation reportChartRelation = new ReportChartRelation(param.getReportId(), reportChartId);
			reportChartRelationMapper.insert(reportChartRelation);
		}else{
			//ai_report_chart
			reportChart = reportChartMapper.selectById(param.getId());
			BeanUtils.copyProperties(param, reportChart);
            reportChart.setUpdateTime(nowTime);
            //时间类型 1 动态时间  2 静态时间 3 自定义
            if(reportChart.getTimeType().equals(2) || reportChart.getTimeType().equals(3)){
				reportChart.setStartTime(new Date(reportChart.getStartTime().getTime()));
				reportChart.setEndTime(new Date(reportChart.getEndTime().getTime()));
			}else if(reportChart.getTimeType().equals(1)){
				reportChart.setStartTime(null);
				reportChart.setEndTime(null);
			}
//			reportChartMapper.updateById(reportChart);
            reportChartMapper.updateAllColumnById(reportChart);
			reportChartId = reportChart.getId();
		}
		//ai_report_chart_user
		if(!isAdd){
			EntityWrapper<ReportChartUser> reportChartUserEntityWrapper = new EntityWrapper<>();
			reportChartUserEntityWrapper.where("chartId={0}", reportChartId);
			reportChartUserMapper.delete(reportChartUserEntityWrapper);
		}
		List<Integer> users = param.getUserIds();
		if(!evUtil.listIsNullOrZero(users)){
			List<ReportChartUser> reportChartUsers = new ArrayList<>();
			users.forEach(x-> reportChartUsers.add(new ReportChartUser(reportChartId,x)));
			reportChartUserMapper.batchInsert(reportChartUsers);
		}
		//application
		if(dataType == 1||dataType == 2){
			//ai_report_chart_indicator
			if(!isAdd){
				EntityWrapper<ReportChartIndicator> reportChartIndicatorEntityWrapper = new EntityWrapper<>();
				reportChartIndicatorEntityWrapper.where("chartId={0}", reportChartId);
				reportChartIndicatorMapper.delete(reportChartIndicatorEntityWrapper);
			}
			List<Integer> indicators = param.getIndicators();
			if(!evUtil.listIsNullOrZero(indicators)){
				List<ReportChartIndicator> reportChartIndicators = new ArrayList<>();
				indicators.forEach(x-> reportChartIndicators.add(new ReportChartIndicator(x, reportChartId)));
				reportChartIndicatorMapper.batchInsert(reportChartIndicators);
			}
		}
		//ai_report_chart_data_filter
		if(!isAdd){
			EntityWrapper<ReportChartDataFilter> reportChartDataFilterEntityWrapper = new EntityWrapper<>();
			reportChartDataFilterEntityWrapper.where("chartId={0}", reportChartId);
			reportChartDataFilterMapper.delete(reportChartDataFilterEntityWrapper);
		}
		List<ReportDimensionParam> filters = param.getRules();
		if(!evUtil.listIsNullOrZero(filters)){
			List<ReportChartDataFilter> reportChartDataFilters = new ArrayList<>();
			filters.forEach(x->{
					ReportChartDataFilter reportChartDataFilter = new ReportChartDataFilter();
					BeanUtils.copyProperties(x,reportChartDataFilter);
					reportChartDataFilter.setChartId(reportChartId);
					reportChartDataFilters.add(reportChartDataFilter);
				}
			);
			reportChartDataFilterMapper.batchInsert(reportChartDataFilters);
		}
		//ai_report_chart_data_dimension
		if(!isAdd){
			EntityWrapper<ReportChartDataDimension> reportChartDataDimensionEntityWrapper = new EntityWrapper<>();
			reportChartDataDimensionEntityWrapper.where("chartId={0}", reportChartId);
			reportChartDataDimensionMapper.delete(reportChartDataDimensionEntityWrapper);
		}
		List<ReportDimensionParam> dimensions = param.getDimensions();
		if(!evUtil.listIsNullOrZero(dimensions)){
			List<ReportChartDataDimension> reportChartDataDimensions = new ArrayList<>();
            dimensions.forEach(x->{
					ReportChartDataDimension reportChartDataDimension = new ReportChartDataDimension();
					BeanUtils.copyProperties(x,reportChartDataDimension);
					reportChartDataDimension.setChartId(reportChartId);
					reportChartDataDimensions.add(reportChartDataDimension);
				}
			);
			reportChartDataDimensionMapper.batchInsert(reportChartDataDimensions);
		}

		//ai_report_chart_indicator_filter
		if(!isAdd){
			EntityWrapper<ReportChartIndicatorFilter> indicatorFilterEntityWrapper = new EntityWrapper<>();
			indicatorFilterEntityWrapper.where("chartId={0}", reportChartId);
			reportChartIndicatorFilterMapper.delete(indicatorFilterEntityWrapper);
		}
		List<ReportChartIndicatorFilterParam> reportChartIndicatorFilterParams = param.getFilterIndicator();
		if(!evUtil.listIsNullOrZero(reportChartIndicatorFilterParams)){
			List<ReportChartIndicatorFilter> reportChartIndicatorFilters = new ArrayList<>();
			reportChartIndicatorFilterParams.forEach(x->{
				ReportChartIndicatorFilter indicatorFilter = new ReportChartIndicatorFilter();
				BeanUtils.copyProperties(x,indicatorFilter);
				indicatorFilter.setChartId(reportChartId);
				reportChartIndicatorFilters.add(indicatorFilter);
			});
			reportChartIndicatorFilterMapper.batchInsert(reportChartIndicatorFilters);
		}

		//ai_report_chart_table_field
		if(!isAdd){
			EntityWrapper<ReportChartTableField> reportChartTableFieldEntityWrapper = new EntityWrapper<>();
			reportChartTableFieldEntityWrapper.where("chartId={0}", reportChartId);
			reportChartTableFiledMapper.delete(reportChartTableFieldEntityWrapper);
		}
		List<String> fields = param.getFields();
		if(!evUtil.listIsNullOrZero(fields)){
			List<ReportChartTableField> reportChartTableFields = new ArrayList<>();
			fields.forEach(x->{
				ReportChartTableField reportChartTableField = new ReportChartTableField();
				reportChartTableField.setField(x);
				reportChartTableField.setChartId(reportChartId);
				reportChartTableFields.add(reportChartTableField);
			});
			reportChartTableFiledMapper.batchInsert(reportChartTableFields);
		}


		//ai_report_chart_data
		if(!isAdd){
			EntityWrapper<ReportChartData> reportChartDataEntityWrapper = new EntityWrapper<>();
			reportChartDataEntityWrapper.where("chartId={0}", reportChartId);
			//ai_report_chart_data_indicator [delete]
			if(dataType == 3||dataType == 4){
				List<ReportChartData> reportChartDataList = reportChartDataMapper.selectList(reportChartDataEntityWrapper);
				List<Integer> reportChartDataIds = reportChartDataList.stream().collect(Collectors.mapping(ReportChartData::getId, Collectors.toList()));
				if(!evUtil.listIsNullOrZero(reportChartDataIds)){
					EntityWrapper<ReportChartDataIndicator> reportChartDataIndicatorEntityWrapper = new EntityWrapper<>();
					reportChartDataIndicatorEntityWrapper.in("chartDataId", reportChartDataIds);
					reportChartDataIndicatorMapper.delete(reportChartDataIndicatorEntityWrapper);
				}
			}
			reportChartDataMapper.delete(reportChartDataEntityWrapper);
		}
		List<ReportChartDataParam> reportChartDataParams = param.getChartData();
		if(!evUtil.listIsNullOrZero(reportChartDataParams)){
			reportChartDataParams.forEach(x->{
				ReportChartData reportChartData = new ReportChartData();
				BeanUtils.copyProperties(x, reportChartData);
				reportChartData.setChartId(reportChartId);
				reportChartDataMapper.insert(reportChartData);
				//ai_report_chart_data_indicator 事件指标表
				if(dataType == 3||dataType == 4){
					if(!evUtil.listIsNullOrZero(x.getIndicators())){
						List<ReportChartDataIndicator> reportChartDataIndicators = new ArrayList<>();
						x.getIndicators().forEach(y->{
							ReportChartDataIndicator reportChartDataIndicator = new ReportChartDataIndicator(y,reportChartData.getId());
							reportChartDataIndicators.add(reportChartDataIndicator);
						});
						reportChartDataIndicatorMapper.batchInsert(reportChartDataIndicators);
					}
				}
			});
		}

			//ai_report_chart_line_color
		if(!isAdd){
			EntityWrapper<ReportChartLineColor> reportChartLineColorEntityWrapper = new EntityWrapper<>();
			reportChartLineColorEntityWrapper.where("chartId={0}", reportChartId);
			reportChartLineColorMapper.delete(reportChartLineColorEntityWrapper);
		}
		List<String> colorStrList = param.getInputColorList();
		if(!evUtil.listIsNullOrZero(colorStrList)){
			List<ReportChartLineColor> reportChartLineColors = new ArrayList<>();
			colorStrList.forEach(x -> {
				ReportChartLineColor color = new ReportChartLineColor();
				color.setChartId(reportChartId);
				color.setColor(x);
				reportChartLineColors.add(color);
			});
			reportChartLineColorMapper.batchInsert(reportChartLineColors);
		}
        return reportChart;
    }


	@Override
	public boolean deleteReportChartByIds(List<Integer> ids) {
		if(evUtil.listIsNullOrZero(ids)){
			return false;
		}
		List<ReportChart> reportCharts = reportChartMapper.selectBatchIds(ids);
//		ReportChart reportChart = reportChartMapper.selectById(id);
		//ai_report_chart
		reportChartMapper.deleteBatchIds(ids);
		//ai_report_chart_user
		EntityWrapper<ReportChartUser> reportChartUserEntityWrapper = new EntityWrapper<>();
		reportChartUserEntityWrapper.in("chartId", ids);
		reportChartUserMapper.delete(reportChartUserEntityWrapper);
		//ai_report_chart_indicator_filter
		EntityWrapper<ReportChartIndicatorFilter> reportChartIndicatorFilterEntityWrapper = new EntityWrapper<>();
		reportChartIndicatorFilterEntityWrapper.in("chartId", ids);
		reportChartIndicatorFilterMapper.delete(reportChartIndicatorFilterEntityWrapper);
		//ai_report_chart_indicator
		EntityWrapper<ReportChartIndicator> reportChartIndicatorEntityWrapper = new EntityWrapper<>();
		reportChartIndicatorEntityWrapper.in("chartId", ids);
		reportChartIndicatorMapper.delete(reportChartIndicatorEntityWrapper);
		//ai_report_chart_data_filter
		EntityWrapper<ReportChartDataFilter> reportChartDataFilterEntityWrapper = new EntityWrapper<>();
		reportChartDataFilterEntityWrapper.in("chartId", ids);
		reportChartDataFilterMapper.delete(reportChartDataFilterEntityWrapper);
		//ai_report_chart_data_dimension
		EntityWrapper<ReportChartDataDimension> reportChartDataDimensionEntityWrapper = new EntityWrapper<>();
		reportChartDataDimensionEntityWrapper.in("chartId", ids);
		reportChartDataDimensionMapper.delete(reportChartDataDimensionEntityWrapper);
		//ai_report_chart_table_field
		EntityWrapper<ReportChartTableField> reportChartTableFieldEntityWrapper = new EntityWrapper<>();
		reportChartTableFieldEntityWrapper.in("chartId={0}", ids);
		reportChartTableFiledMapper.delete(reportChartTableFieldEntityWrapper);
		//ai_report_chart_data
		EntityWrapper<ReportChartData> reportChartDataEntityWrapper = new EntityWrapper<>();
		reportChartDataEntityWrapper.in("chartId", ids);
		List<Integer> reportChartDataIds = new ArrayList<>();
		for(ReportChart reportChart : reportCharts){
			if(reportChart.getDataType() == 2){
				//ai_report_chart_data_indicator (event [delete])
				reportChartDataIds.addAll(reportChartDataMapper.selectList(reportChartDataEntityWrapper)
						.stream().map(ReportChartData::getId).collect(Collectors.toList()));
			}
		}
		EntityWrapper<ReportChartDataIndicator> reportChartDataIndicatorEntityWrapper = new EntityWrapper<>();
		if(!evUtil.listIsNullOrZero(reportChartDataIds)){
			reportChartDataIndicatorEntityWrapper.in("chartDataId", reportChartDataIds);
			reportChartDataIndicatorMapper.delete(reportChartDataIndicatorEntityWrapper);
		}
		reportChartDataMapper.delete(reportChartDataEntityWrapper);

		//ai_report_chart_line_color
		EntityWrapper<ReportChartLineColor> reportChartLineColorEntityWrapper = new EntityWrapper<>();
		if(!evUtil.listIsNullOrZero(ids)){
			reportChartLineColorEntityWrapper.in("chartId", ids);
			reportChartLineColorMapper.delete(reportChartLineColorEntityWrapper);
		}
		return true;
	}

    @Override
    public ReportChartParam findReportChartByMap(Map<String, Object> map) {
		ReportChartParam result = reportChartMapper.selectReportById(map);
        return result;
    }

	@Override
	public ReportChartParam findEventReportChartByMap(Map<String, Object> map) {
		return reportChartMapper.selectEventReportById(map);
	}

	@Override
	public ReportChartParam selectReportForIndicatorData(Map<String, Object> map) {
		return reportChartMapper.selectReportForIndicatorData(map);
	}


	@Override
	public Map<String, Object> selectReportChartDataByChartIds(List<Integer> ids) {
//		Map<String, Object> resultMap = new HashMap<>();
//		Map<Integer, Map<Integer,ReportChartDataParam>> result = new HashMap<>();
//		EntityWrapper<ReportChartData> entityWrapper = new EntityWrapper<>();
//		entityWrapper.in("chartId", ids);
//		List<ReportChartData> reportChartDataList = reportChartDataMapper.selectList(entityWrapper);
//		if(evUtil.listIsNullOrZero(reportChartDataList)){
//			return resultMap;
//		}
//
//
//		//hand application indicators
//		Map<String, Object> indicatorParamMap = new HashMap<>();
//		indicatorParamMap.put("ids", ids);
//		List<ReportChartIndicator> chartAppIndicators = reportChartIndicatorMapper.selectReportChartIndicatorByMap(indicatorParamMap);
//		Map<Integer, ReportChartIndicator> indicatorAppMap = new HashMap<>();
//		if(evUtil.listIsNullOrZero(chartAppIndicators)){
//			chartAppIndicators.forEach(x->{
//				indicatorAppMap.put(x.getChartId(), x);
//			});
//		}
//
//		//hand custom indicators  chartId:[customId:indicator]
//		Map<Integer, Map<Integer, List<ReportChartIndicator>>> customIndicatorMap = new HashMap<>();
//		List<ReportChartIndicator> chartCustomIndicators = reportChartDataIndicatorMapper.selectReportChartDataIndicatorByMap(indicatorParamMap);
//		if(!evUtil.listIsNullOrZero(chartCustomIndicators)){
//			chartCustomIndicators.forEach(x->{
//				if(customIndicatorMap.get(x.getChartId()) != null){
//					Map<Integer, List<ReportChartIndicator>> cIndicatorMap = customIndicatorMap.get(x.getCustomId());
//					if(cIndicatorMap.get(x.getCustomId()) != null){
//						cIndicatorMap.get(x.getCustomId()).add(x);
//					}else{
//						List<ReportChartIndicator> indicators = new ArrayList<>();
//						indicators.add(x);
//						cIndicatorMap.put(x.getCustomId(), indicators);
//					}
//				}else{
//					Map<Integer, List<ReportChartIndicator>> cIndicatorMap = new HashMap<>();
//					List<ReportChartIndicator> indicators = new ArrayList<>();
//					indicators.add(x);
//					cIndicatorMap.put(x.getCustomId(), indicators);
//					customIndicatorMap.put(x.getCustomId(),cIndicatorMap);
//				}
//			});
//		}
//
//
//		//find indicators[indicatorId, indicatorName, indicatorName]
//		Set<Integer> applicationIds = new HashSet<>();
//		Set<Integer> monitorIds = new HashSet<>();
//		Set<Integer> componentIds = new HashSet<>();
//		Set<Integer> ipIds = new HashSet<>();
//		Set<Integer> portIds = new HashSet<>();
//		Set<Integer> customIds = new HashSet<>();
//		Integer applicationId, monitorId, componentId, ipId, portId, customId, type,chartId;
//
//		//根据type查询相应节点名称
//		for(ReportChartData x : reportChartDataList){
//			type = x.getDataType();
//			applicationId = x.getApplicationId();
//			monitorId = x.getMonitorId();
//			chartId = x.getChartId();
//			componentId = x.getComponentId();
//			portId = x.getPortId();
//			ipId = x.getIpId();
//			customId = x.getCustomId();
//			if(type == 1){
//				applicationIds.add(applicationId);
//				constructMap(result, applicationId, chartId, x);
//			}else if(type == 2){
//				applicationIds.add(applicationId);
//				monitorIds.add(monitorId);
//				constructMap(result, monitorId, chartId, x);
//			}else if(type == 3){
//				applicationIds.add(applicationId);
//				monitorIds.add(monitorId);
//				componentIds.add(componentId);
//				constructMap(result, componentId, chartId, x);
//			}else if(type == 4){
//				applicationIds.add(applicationId);
//				monitorIds.add(monitorId);
//				componentIds.add(componentId);
//				ipIds.add(ipId);
//				constructMap(result, ipId, chartId, x);
//			}else if(type == 5){
//				applicationIds.add(applicationId);
//				monitorIds.add(monitorId);
//				componentIds.add(componentId);
//				ipIds.add(ipId);
//				portIds.add(portId);
//				constructMap(result, portId, chartId, x);
//			}else if(type == 6){
//				applicationIds.add(applicationId);
//				monitorIds.add(monitorId);
//				componentIds.add(componentId);
//				customIds.add(customId);
//				constructMap(result, customId, chartId, x);
//			}
//		}
//
//		List<Application> applications;
//		List<Component> components;
//		List<Monitor> monitors;
//		List<ServerIp> serverIps;
//		List<ServerPort> serverPorts;
//		List<Custom> customs;
//		Map<Integer, String> nameMap = new HashMap<>();
//		if(!evUtil.collectionIsNullOrZero(applicationIds)){
//			applications = applicationMapper.selectBatchIds(new ArrayList<>(applicationIds));
//			applications.forEach(x-> nameMap.put(x.getId(), x.getName()));
//		}
//		if(!evUtil.collectionIsNullOrZero(monitorIds)){
//			monitors = monitorMapper.selectBatchIds(new ArrayList<>(monitorIds));
//			monitors.forEach(x-> nameMap.put(x.getId(), x.getName()));
//		}
//		if(!evUtil.collectionIsNullOrZero(componentIds)){
//			components = componentMapper.selectBatchIds(new ArrayList<>(componentIds));
//			components.forEach(x-> nameMap.put(x.getId(), x.getName()));
//		}
//		if(!evUtil.collectionIsNullOrZero(ipIds)){
//			serverIps = serverIpMapper.selectBatchIds(new ArrayList<>(ipIds));
//			serverIps.forEach(x-> nameMap.put(x.getId(), x.getIp()));
//		}
//		if(!evUtil.collectionIsNullOrZero(portIds)){
//			serverPorts = serverPortMapper.selectBatchIds(new ArrayList<>(portIds));
//			serverPorts.forEach(x-> nameMap.put(x.getId(), x.getPort().toString()));
//		}
//		if(!evUtil.collectionIsNullOrZero(customIds)){
//			customs = customMapper.selectBatchIds(new ArrayList<>(customIds));
//			customs.forEach(x-> nameMap.put(x.getId(), x.getName()));
//		}
//
//		//find dimension name by chartIds
//		Map<String, Object> dimParamMap = new HashMap<>(3);
//		dimParamMap.put("chartIds", ids);
//		List<Map<String, Object>> dimensionMapList = reportChartMapper.selectReportDimensionData(dimParamMap);
//
//		//component dimension Map(key:chartId-value:[key:componentId-[ename]])
//		Map<Integer, Map<Integer, Set<String>>> componentDimensionMap = new HashMap<>();
//		Map<Integer, Map<Integer, Set<String>>> customDimensionMap = new HashMap<>();
//		Set<Integer> demComponentIdSet = new HashSet<>();
//		Set<Integer> demCustomIdSet = new HashSet<>();
//		Integer dataType, demComponentId,demCustomId, demCharId;
//		String demName;
//		for(Map<String, Object> dimensionMap : dimensionMapList){
//			dataType = evUtil.getMapIntegerValue(dimensionMap,"dataType");
//			demComponentId = evUtil.getMapIntegerValue(dimensionMap,"componentId");
//			demCustomId = evUtil.getMapIntegerValue(dimensionMap,"customId");
//			demCharId = evUtil.getMapIntegerValue(dimensionMap,"chartId");
//			demName = evUtil.getMapStrValue(dimensionMap,"ename");
//			if(dataType == 3){
//				//component dimension map
//				constructDimensionMap(componentDimensionMap, demComponentId, demCharId, demName);
//				demComponentIdSet.add(demComponentId);
//			}else{
//				//custom dimension map
//				constructDimensionMap(customDimensionMap, demCustomId, demCharId, demName);
//				demCustomIdSet.add(demCustomId);
//			}
//		}
//
//		//find dimensions name
//		//hand componentDimension
//		Map<Integer, Map<String, Map<String, Object>>> comMap = constructComponentDimensionMap(demComponentIdSet,null);
//		//hand customDimension
//		Map<Integer, Map<String, Map<String, Object>>> customMap = constructCustomDimensionMap(demCustomIdSet,null);
//
//		List<Map<String, Object>> listData = new ArrayList<>();
//		Integer chartKey;
//		for(Map.Entry<Integer, Map<Integer,ReportChartDataParam>> entry : result.entrySet()){
//			Map<String,Object> entryMap = new HashMap<>(4);
//			chartKey = entry.getKey();
//			entryMap.put("chartId", chartKey);
//			List<Map<String, Object>> listMap = new ArrayList<>();
//			//hand dimension
//			Map<Integer, Set<String>> componentDimensionSetMap = componentDimensionMap.get(chartKey);
//			Map<Integer, Set<String>> customDimensionSetMap = customDimensionMap.get(chartKey);
//			//hand indicator
//			ReportChartIndicator appIndicator = indicatorAppMap.get(chartKey);
//			Map<Integer, List<ReportChartIndicator>> customIndicator = customIndicatorMap.get(chartKey);
//			for(Map.Entry<Integer,ReportChartDataParam> rc : entry.getValue().entrySet()){
//				Map<String,Object> map = new HashMap<>(4);
//				ReportChartDataParam reportChartData = rc.getValue();
//				reportChartData.setApplicationName(nameMap.get(reportChartData.getApplicationId()));
//				reportChartData.setMonitorName(nameMap.get(reportChartData.getMonitorId()));
//				reportChartData.setComponentName(nameMap.get(reportChartData.getComponentId()));
//				reportChartData.setIp(nameMap.get(reportChartData.getIpId()));
//				reportChartData.setPort(nameMap.get(reportChartData.getPortId()));
//				reportChartData.setCustomName(nameMap.get(reportChartData.getCustomId()));
//				//chart componentId 含有维度
//				if( componentDimensionSetMap != null && !componentDimensionSetMap.isEmpty()){
//					constructDimension(comMap.get(reportChartData.getComponentId()),componentDimensionSetMap.get(reportChartData.getComponentId()), reportChartData);
//				}
//				//event dimension
//				if( componentDimensionSetMap != null && !componentDimensionSetMap.isEmpty()){
//					constructDimension(customMap.get(reportChartData.getCustomId()),customDimensionSetMap.get(reportChartData.getCustomId()), reportChartData);
//				}
//
//				//hand app indicator
//				if(appIndicator != null){
//					List<ReportChartIndicator> tempIndicators = new ArrayList<>();
//					tempIndicators.add(appIndicator);
//					reportChartData.setIndicatorValues(tempIndicators);
//				}
//				//hand event indicators
//				if(customIndicator != null && !customIndicator.isEmpty() ){
//					reportChartData.setIndicatorValues(customIndicator.get(reportChartData.getCustomId()));
//				}
//
//				map.put("uuid",rc.getKey());
//				map.put("reportChartData", reportChartData);
//				listMap.add(map);
//			}
//			entryMap.put("data", listMap);
//			listData.add(entryMap);
//		}
//		resultMap.put("configs",listData);
//		return resultMap;
		return null;
	}

	private void constructDimension(Map<String, Map<String, Object>> comDimensionMap, Set<String> nameSet, ReportChartDataRespParam reportChartData) {
		if(nameSet != null && !nameSet.isEmpty()){
//			List<JSONObject> dimensionValueList = new ArrayList<>();
			List<ReportChartDimensionRespParam> dimensionValueList = new ArrayList<>();
			Map<String,Object> dimenMap;
			if(comDimensionMap != null){
				for(String dimenEname : nameSet){
					ReportChartDimensionRespParam dimensionParam = new ReportChartDimensionRespParam();
					dimenMap = comDimensionMap.get(dimenEname);

					if(dimenMap != null){
//						jsonObject.put("ename", evUtil.getMapStrValue(dimenMap,"ename"));
//						jsonObject.put("othername", evUtil.getMapStrValue(dimenMap,"othername"));
//						jsonObject.put("name", evUtil.getMapStrValue(dimenMap,"name"));
//						jsonObject.put("values",dimenMap.get("values"));
//						dimensionValueList.add(jsonObject);
						dimensionParam.setEname(evUtil.getMapStrValue(dimenMap,"ename"));
						dimensionParam.setOthername(evUtil.getMapStrValue(dimenMap,"othername"));
						dimensionParam.setName(evUtil.getMapStrValue(dimenMap,"name"));
						dimensionParam.setValues((List)dimenMap.get("values"));
						dimensionValueList.add(dimensionParam);
					}
				}
			}
			reportChartData.setDimensionValues(dimensionValueList);
		}
	}



	@Override
	public Map<String, Object> selectReportChartDataByUuid(List<ReportChartDataParam> list) {
		Map<String, Object> result = new HashMap<>();
		if(evUtil.listIsNullOrZero(list)){
			result.put("configs", new ArrayList<>());
			return result;
		}
		//1 应用 2 监控点 3 组件4 ip 5 port 6 事件
		Set<Integer> applicationIds = new HashSet<>();
		Set<Integer> monitorIds = new HashSet<>();
		Set<Integer> componentIds = new HashSet<>();
		Set<Integer> ipIds = new HashSet<>();
		Set<Integer> portIds = new HashSet<>();
		Set<Integer> customIds = new HashSet<>();
		/**
		 * type 数据类型 1 应用 ，2 监控点 ，3 组件 ，4 IP ，5 端口 ，6事件
		 */
		Integer type;
		Map<Integer, ReportChartDataParam> map = new HashMap<>();
//		Integer isDimension;
		Integer dimensionType;
		Set<Integer> dimenComponentIds = new HashSet<>();
		Set<Integer> dimenCustomIds = new HashSet<>();
		Map<Integer, Set<String>> componentDimensionMap = new HashMap<>();
		Map<Integer, Set<String>> customDimensionMap = new HashMap<>();
		List<ReportChartDimensionParam> dimensions;
		Set<Integer> customIndicatorIdSet = new HashSet<>();
		Map<String, Object> dimensionValueTranslateMap = new HashMap<>();
		//customId OR componentId : [ename:translateMap]
		Map<Integer, Map<String, List<Map<String,Object>>>> dimensionValuesMap = new HashMap<>();
		for(ReportChartDataParam report : list){
			type = report.getDataType();
			/**
			 * 0 不是维度 1组件维度 2事件维度
			 */
			dimensionType = report.getDimensionType();
			if(type == 1){
				applicationIds.add(report.getApplicationId());
				map.put(report.getApplicationId(), report);
			}else if(type == 2){
				applicationIds.add(report.getApplicationId());
				monitorIds.add(report.getMonitorId());
				map.put(report.getMonitorId(), report);
			}else if(type == 3){
				applicationIds.add(report.getApplicationId());
				monitorIds.add(report.getMonitorId());
				componentIds.add(report.getComponentId());
				map.put(report.getComponentId(), report);
			}else if(type == 4){
				applicationIds.add(report.getApplicationId());
				monitorIds.add(report.getMonitorId());
				componentIds.add(report.getComponentId());
				ipIds.add(report.getIpId());
				map.put(report.getIpId(), report);
			}else if(type == 5){
				applicationIds.add(report.getApplicationId());
				monitorIds.add(report.getMonitorId());
				componentIds.add(report.getComponentId());
				ipIds.add(report.getIpId());
				portIds.add(report.getPortId());
				map.put(report.getPortId(), report);
			}else if(type == 6){
				applicationIds.add(report.getApplicationId());
				monitorIds.add(report.getMonitorId());
				componentIds.add(report.getComponentId());
				customIds.add(report.getCustomId());
				map.put(report.getCustomId(), report);
				customIndicatorIdSet.add(report.getCustomId());
			}

			//hand dimension
			//component dimension
			if(dimensionType == 1){
				dimensions = report.getDimensionValues();
				if(!evUtil.listIsNullOrZero(dimensions)){
					dimenComponentIds.add(report.getComponentId());
					List<String> componentEnameStrs = new ArrayList<>();
					dimensions.forEach(x->{
//						String ename = x.getString("ename");
						String ename = x.getEname();
						componentEnameStrs.add(ename);
						//value translate param:(componentId;dimensionId;value]
//						JSONArray values = x.getJSONArray("values");
						List<String> values = x.getValues();
						if(values != null && !values.isEmpty()){
//							List<String> dimensionValues = values.toJavaList(String.class);
							if(!evUtil.listIsNullOrZero(values)){
								dimensionValueTranslateMap.put("componentId", report.getComponentId());
//								dimensionValueTranslateMap.put("dimensionId", x.getInteger("dimensionId"));
								dimensionValueTranslateMap.put("dimensionId", x.getDimensionId());
//								dimensionValueTranslateMap.put("values", dimensionValues);
								dimensionValueTranslateMap.put("values", values);
								List<Map<String,Object>> translates = fieldMappingMapper.getFieldMappingByValues(dimensionValueTranslateMap);
								Map<String, List<Map<String, Object>>> stringMapMap = dimensionValuesMap.get(report.getComponentId());
								if(stringMapMap != null){
									if(stringMapMap.get(ename) != null){
										stringMapMap.get(ename).addAll(translates);
									}else{
										stringMapMap.put(ename, translates);
									}
								}else{
									Map<String, List<Map<String, Object>>> sMap = new HashMap<>();
									sMap.put(ename, translates);
									dimensionValuesMap.put(report.getComponentId(), sMap);
								}
							}
						}
					});
					if(componentDimensionMap.get(report.getComponentId()) != null){
						componentDimensionMap.get(report.getComponentId()).addAll(componentEnameStrs);
					}else{
						componentDimensionMap.put(report.getComponentId(), new HashSet<>(componentEnameStrs));
					}
				}
			}

			//custom dimension
			if(dimensionType == 2){
				dimensions = report.getDimensionValues();
				if(!evUtil.listIsNullOrZero(dimensions)){
					dimenCustomIds.add(report.getCustomId());
					List<String> enameStrs = new ArrayList<>();
					dimensions.forEach(x->{
//						String ename = x.getString("ename");
						String ename = x.getEname();
						enameStrs.add(ename);
						//value translate param:(componentId;dimensionId;value]
//						JSONArray values = x.getJSONArray("values");
						List<String> values = x.getValues();

						//维度类型( 0分析维度 1统计维度 -1 所有) 此处只处理统计维度
//						Integer dimeType = x.getInteger("type");
						Integer dimeType = x.getType();
						if(dimeType != null && dimeType.intValue() == 1 && values != null && !values.isEmpty()){
//							List<String> dimensionValues = values.toJavaList(String.class);
							if(!evUtil.listIsNullOrZero(values)){
								dimensionValueTranslateMap.put("customId", report.getCustomId());
//								dimensionValueTranslateMap.put("dimensionId", x.getInteger("dimensionId"));
								dimensionValueTranslateMap.put("dimensionId", x.getDimensionId());
//								dimensionValueTranslateMap.put("values", dimensionValues);
								dimensionValueTranslateMap.put("values", values);
								int dType;
								if("province".equals(ename)){
									dType = 1;
								}else if("city".equals(ename)){
									dType = 2;
								}else{
									dType = 0;
								}
								dimensionValueTranslateMap.put("type",dType);
								List<Map<String,Object>> translates = customMapper.selectStatisticsDimensionByEnames(dimensionValueTranslateMap);
								Map<String, List<Map<String, Object>>> stringMapMap = dimensionValuesMap.get(report.getCustomId());
								if(stringMapMap != null){
									if(stringMapMap.get(ename) != null){
										stringMapMap.get(ename).addAll(translates);
									}else{
										stringMapMap.put(ename, translates);
									}
								}else{
									Map<String, List<Map<String, Object>>> sMap = new HashMap<>();
									sMap.put(ename, translates);
									dimensionValuesMap.put(report.getCustomId(), sMap);
								}
							}
						}
					});

					if(customDimensionMap.get(report.getCustomId()) != null){
						customDimensionMap.get(report.getCustomId()).addAll(enameStrs);
					}else{
						customDimensionMap.put(report.getCustomId(), new HashSet<>(enameStrs));
					}
				}
			}


		}

		//hand componentDimension  [dimension-translate dimensionValue-translate]
		Map<Integer, Map<String, Map<String, Object>>> comMap = constructComponentDimensionMap(dimenComponentIds,dimensionValuesMap);

		//hand customDimension [dimension-translate dimensionValue-translate]
		Map<Integer, Map<String, Map<String, Object>>> customMap = constructCustomDimensionMap(dimenCustomIds,dimensionValuesMap);

		//customId:[indicatorId:reportChartIndicator]
		Map<Integer, Map<Integer, ReportChartIndicator>> customIndicatorMap = new HashMap<>();
		if(!customIndicatorIdSet.isEmpty()){
			//hand event indicator
			Map<String, Object> customMap1 = new HashMap<>();
			customMap1.put("ids", customIndicatorIdSet);
			List<Map<String, Object>> cIndicatorList = customMapper.selectCustomIndicatorNameByCustomIds(customMap1);
			Integer customId, indicatorId;
			String otherName,name,unit,ename;
			if(!evUtil.listIsNullOrZero(cIndicatorList)){
				for(Map<String, Object> indicatorMap : cIndicatorList){
					customId = evUtil.getMapIntegerValue(indicatorMap,"customId");
					indicatorId = evUtil.getMapLong(indicatorMap,"indicatorId").intValue();
					otherName = evUtil.getMapStrValue(indicatorMap,"indicatorName");
					name = evUtil.getMapStrValue(indicatorMap,"name");
					ename = evUtil.getMapStrValue(indicatorMap,"columnName");
					unit = evUtil.getMapStrValue(indicatorMap,"unit");
					if(customIndicatorMap.get(customId) != null){
						customIndicatorMap.get(customId).put(indicatorId,
								new ReportChartIndicator(indicatorId,name,otherName, unit,customId,ename));
					}else{
						Map<Integer, ReportChartIndicator> iMap = new HashMap<>();
						iMap.put(indicatorId, new ReportChartIndicator(indicatorId,name,otherName, unit,customId,ename));
						customIndicatorMap.put(customId, iMap);
					}
				}
			}
		}
		// app indicator
		Map<Integer, ReportChartIndicator> appReportIndicatorMap = new HashMap<>();
		List<Indicator> indicatorList = indicatorMapper.selectList(null);
		indicatorList.forEach(x->{
			appReportIndicatorMap.put(x.getId(), new ReportChartIndicator(x.getId(),
						x.getName(),null, x.getUnit(),null, x.getColumnName()));

		});

		List<Application> applications;
		List<Component> components;
		List<Monitor> monitors;
		List<ServerIp> serverIps;
//		List<ServerPort> serverPorts;
		List<Server> servers;
		List<Custom> customs;

		Map<Integer, String> nameMap = new HashMap<>();
		if(!evUtil.collectionIsNullOrZero(applicationIds)){
			applications = applicationMapper.selectBatchIds(new ArrayList<>(applicationIds));
			applications.forEach(x-> nameMap.put(x.getId(), x.getName()));
		}
		if(!evUtil.collectionIsNullOrZero(monitorIds)){
			monitors = monitorMapper.selectBatchIds(new ArrayList<>(monitorIds));
			monitors.forEach(x-> nameMap.put(x.getId(), x.getName()));
		}
		if(!evUtil.collectionIsNullOrZero(componentIds)){
			components = componentMapper.selectBatchIds(new ArrayList<>(componentIds));
			components.forEach(x-> nameMap.put(x.getId(), x.getName()));
		}
		if(!evUtil.collectionIsNullOrZero(ipIds)){
			serverIps = serverIpMapper.selectBatchIds(new ArrayList<>(ipIds));
			serverIps.forEach(x-> nameMap.put(x.getId(), x.getIp()));
		}
		if(!evUtil.collectionIsNullOrZero(portIds)){
			servers = serverMapper.selectBatchIds(new ArrayList<>(portIds));
			servers.forEach(x-> nameMap.put(x.getId(), x.getPort().toString()));
		}
		if(!evUtil.collectionIsNullOrZero(customIds)){
			customs = customMapper.selectBatchIds(new ArrayList<>(customIds));
			customs.forEach(x-> nameMap.put(x.getId(), x.getName()));
		}

		List<Map<String, ReportChartDataRespParam>> reportChartDataList = new ArrayList<>();
		List<ReportChartIndicator> reportChartIndicatorsFromParam;
		int dType;
		for(Map.Entry<Integer, ReportChartDataParam> repEntry : map.entrySet()){
			Map<String, ReportChartDataRespParam> mapResult = new HashMap<>();
			ReportChartDataParam reportChartDataParam = repEntry.getValue();
			ReportChartDataRespParam reportChartDataRespParam = new ReportChartDataRespParam();
			BeanUtils.copyProperties(reportChartDataParam,reportChartDataRespParam);
			reportChartDataRespParam.setUuid(repEntry.getKey());
			reportChartDataRespParam.setApplicationName(nameMap.get(reportChartDataParam.getApplicationId()));
			reportChartDataRespParam.setMonitorName(nameMap.get(reportChartDataParam.getMonitorId()));
			reportChartDataRespParam.setComponentName(nameMap.get(reportChartDataParam.getComponentId()));
			reportChartDataRespParam.setIp(nameMap.get(reportChartDataParam.getIpId()));
			reportChartDataRespParam.setPort(nameMap.get(reportChartDataParam.getPortId()));
			reportChartDataRespParam.setCustomName(nameMap.get(reportChartDataParam.getCustomId()));
			Set<String> componentDimensionSet = componentDimensionMap.get(repEntry.getKey());
			Set<String> customDimensionSet = customDimensionMap.get(repEntry.getKey());

			//chart componentId 含有维度
			if(componentDimensionSet != null && !componentDimensionSet.isEmpty()){
				constructDimension(comMap.get(reportChartDataParam.getComponentId()),componentDimensionSet, reportChartDataRespParam);
			}
			//event dimension
			if(customDimensionSet != null && !customDimensionSet.isEmpty()){
				constructDimension(customMap.get(reportChartDataParam.getCustomId()),customDimensionSet, reportChartDataRespParam);
			}

			//hand indicator
			reportChartIndicatorsFromParam = reportChartDataParam.getIndicatorValues();
			dType = reportChartDataParam.getType();
			if(dType == 1){
				//app indicator
				List<ReportChartIndicator> indList = new ArrayList<>();
				reportChartIndicatorsFromParam.forEach(x->{
					indList.add(appReportIndicatorMap.get(x.getIndicatorId()));
				});
				reportChartDataRespParam.setIndicatorValues(indList);
			}else if(dType == 2){
				//event indicator
				Map<Integer, ReportChartIndicator> custIndicatorMap  = customIndicatorMap.get(reportChartDataParam.getCustomId());
				if(custIndicatorMap == null || custIndicatorMap.isEmpty()){
					throw new RuntimeException("事件已经删除");
				}
				List<ReportChartIndicator> indList = new ArrayList<>();
				reportChartIndicatorsFromParam.forEach(x->{
					indList.add(custIndicatorMap.get(x.getIndicatorId()));
				});
				reportChartDataRespParam.setIndicatorValues(indList);
			}else{

			}
			mapResult.put("reportChartData", reportChartDataRespParam);
			reportChartDataList.add(mapResult);
		}
		result.put("configs", reportChartDataList);
		return result;
	}

	/**
	 * componentId:[ename:data]
	 * @param dimenComponentIds
	 * @return
	 */
	private Map<Integer, Map<String, Map<String, Object>>> constructComponentDimensionMap(Set<Integer> dimenComponentIds, Map<Integer, Map<String, List<Map<String, Object>>>> translateValMap) {
		Map<Integer, Map<String, Map<String, Object>>> result = new HashMap<>();
		if(dimenComponentIds.isEmpty()){
			return result;
		}

		//component dimension   [componentId:[colName:[dimensionName, dimensionOtherName]]]
		Map<String, Object> pMap = new HashMap<>(3);
		pMap.put("ids", new ArrayList<>(dimenComponentIds));
		List<Map<String, Object>> fields = protocolFieldMapper.selectDimensionByComponentIds(pMap);

		Integer dComponentId;
		String ename;
		//c.name,c.ename,othername,
		for(Map<String, Object> field : fields){
			dComponentId =  evUtil.getMapIntegerValue(field,"componentId");
			ename = evUtil.getMapStrValue(field, "ename");
			if(translateValMap.get(dComponentId) != null){
				field.put("values", translateValMap.get(dComponentId).get(ename));
			}

			result.computeIfAbsent(dComponentId, HashMap::new).put(ename,field);

		}
		return result;
	}

	/**
	 * customId:[ename:data]
	 * @param dimenCustomIds
	 * @return
	 */
	private Map<Integer, Map<String, Map<String, Object>>> constructCustomDimensionMap(Set<Integer> dimenCustomIds,  Map<Integer, Map<String, List<Map<String, Object>>>> translateValMap) {
		Map<Integer, Map<String, Map<String, Object>>> customMap = new HashMap<>();
		if(!dimenCustomIds.isEmpty()){
			//custom dimension name map
			Map<String, Object> pMap = new HashMap<>(3);
			pMap.put("ids", dimenCustomIds);
			customMap = this.selectDimensionMapByIdsToSpecialMap(pMap,translateValMap);
		}
		return customMap;
	}


	public Map<Integer, Map<String, Map<String, Object>>> selectDimensionMapByIdsToSpecialMap(Map<String, Object> map,  Map<Integer, Map<String, List<Map<String, Object>>>> translateValMap) {
		Map<Integer, Map<String, Map<String, Object>>> result = new HashMap<>();
		List<Map<String, Object>> maps = customMapper.selectDimensionByCustomIdAndTypeByIds(map);
		Integer customId;
		String ename;
		Map<String,List<Map<String, Object>>> obj;
		for(Map<String, Object> dataMap : maps){
			customId = evUtil.getMapIntegerValue(dataMap, "customId");
			ename = evUtil.getMapStrValue(dataMap, "ename");
			obj = translateValMap.get(customId);
			if(obj != null){
				dataMap.put("values", obj.get(ename));
			}

			result.computeIfAbsent(customId, HashMap::new).put(ename,dataMap);

		}
		return result;
	}


	@Override
	public boolean deleteChartByIds(Set<Integer> ids, Integer userId) {
		if(evUtil.collectionIsNullOrZero(ids) || userId == null){
			return false;
		}
		EntityWrapper<ReportChart> entityWrapper = new EntityWrapper<>();
		entityWrapper.where("createBy={0}", userId);
		List<ReportChart> list = reportChartMapper.selectList(entityWrapper);
		List<Integer> ownerChartIds = list.stream().map(ReportChart::getId).collect(Collectors.toList());
		Set<Integer> result = new HashSet<>();
		result.addAll(ids);
		result.retainAll(ownerChartIds);
		//delete report_chart...
		if(!evUtil.collectionIsNullOrZero(result)){
			this.deleteReportChartByIds(new ArrayList<>(result));
		}
		//delete ai_report_chart_user
		ids.removeAll(ownerChartIds);
		if(!evUtil.collectionIsNullOrZero(ids)){
			EntityWrapper<ReportChartUser> reportChartUserEntityWrapper = new EntityWrapper<>();
			reportChartUserEntityWrapper.in("chartId", ids).and("userId={0}",userId);
			reportChartUserMapper.delete(reportChartUserEntityWrapper);
		}
		return true;
	}

	@Override
	public void updateDescriptionLocation(ReportDescriptionLocation lo) {
		EntityWrapper<ReportDescriptionLocation> wrapper = new EntityWrapper<>();
		wrapper.eq("descriptionId", lo.getDescriptionId());
		reportDescriptionLocationMapper.update(lo, wrapper);
		
	}

	@Override
	public void editChartRelation(Integer reportId, Integer chartId, Integer chartRelationId) {
		ReportChartRelation entity=new ReportChartRelation();
		entity.setId(chartRelationId);
		entity.setChartId(chartId);
		entity.setReportId(reportId);
		reportChartRelationMapper.updateById(entity);
		
	}
}
