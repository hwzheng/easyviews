package com.greattimes.ev.bpm.service.config.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;
import com.greattimes.ev.bpm.entity.DsUuid;
import com.greattimes.ev.bpm.entity.DsUuidMapping;
import com.greattimes.ev.bpm.entity.RelevantContraint;
import com.greattimes.ev.bpm.entity.ServerGroup;
import com.greattimes.ev.bpm.mapper.DatasourceRelevantMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMappingMapper;
import com.greattimes.ev.bpm.mapper.RelevantContraintMapper;
import com.greattimes.ev.bpm.mapper.ServerGroupMapper;
import com.greattimes.ev.bpm.mapper.ServerIpMapper;
import com.greattimes.ev.bpm.service.config.IRelevantService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 数据源关联表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
@Service
public class RelevantServiceImpl extends ServiceImpl<DatasourceRelevantMapper, DatasourceRelevant>
		implements IRelevantService {
	@Autowired
	private DatasourceRelevantMapper datasourceRelevantMapper;
	@Autowired
	private RelevantContraintMapper relevantContraintMapper;
	@Autowired
	private DsUuidMapper dsUuidMapper;
	@Autowired
	private DsUuidMappingMapper dsUuidMappingMapper;
	@Autowired
	private ServerGroupMapper serverGroupMapper;
	@Autowired
	private ServerIpMapper serverIpMapper;

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
	public void delete(int componentId) {
		// 查询外部数据下组件配置的指标组
		List<Integer> indicatorGroupids = relevantContraintMapper.getGroupIds(componentId);
		for (Integer indicatorGroupId : indicatorGroupids) {
			// 获取外部数据关联层级
			Map<String, Object> col = new HashMap<>(2);
			col.put("componentId", componentId);
			col.put("indicatorGroupId", indicatorGroupId);
			List<DatasourceRelevant> relevant = datasourceRelevantMapper.selectByMap(col);
			if (relevant.size() > 1) {
				logger.error("组件[" + componentId + "]下指标组["+indicatorGroupId+"]有多个关联关系");
			}
			if (!evUtil.listIsNullOrZero(relevant)) {
				//获取组件下的IP port
				Set<DataSourcesRelationParam> set=getIpAndPort(componentId);
				Integer relevantField = relevant.get(0).getRelevantField();
				Integer datasourceId=relevant.get(0).getDataSourceId();
				if (null != relevantField) {
					switch (relevantField) {
					case 1:
						deleteByIp(set, componentId, indicatorGroupId,datasourceId);
						break;
					case 2:
						deleteByIpAndPort(set, componentId, indicatorGroupId,datasourceId);
						break;
					default:
						logger.error("组件[" + componentId + "]下指标组["+indicatorGroupId+"]关联关系参数错误！");
						break;
					}
				}else {
					Integer mount=relevant.get(0).getMount();
					switch (mount) {
					case 0:
						break;
					case 1:
						deleteByIp(set, componentId, indicatorGroupId,datasourceId);
						break;
					case 2:
						deleteByIpAndPort(set, componentId, indicatorGroupId,datasourceId);
						break;
					default:
						logger.error("组件[" + componentId + "]下指标组["+indicatorGroupId+"]关联关系参数错误！");
						break;
					}
				}
			}

		}

	}

	/**
	 * 根据ip+port删
	 * @param set 组件下IP port集合
	 * @param componentId
	 * @param indicatorGroupId
	 * @param datasourceId
	 */
	private void deleteByIpAndPort(Set<DataSourcesRelationParam> set, int componentId, Integer indicatorGroupId,
			Integer datasourceId) {
		List<RelevantContraint> re = relevantContraintMapper.getInvalidByIpAndPort(set, componentId, indicatorGroupId);
		deleteRelevantUuid(re,datasourceId);
		List<Integer> idList = re.stream().map(RelevantContraint::getId).collect(Collectors.toList());
		if(!evUtil.listIsNullOrZero(idList)) {
			relevantContraintMapper.deleteBatchIds(idList);
		}
	}

	/**
	 * 根据IP删
	 * @param set 组件下IP port集合
	 * @param componentId
	 * @param indicatorGroupId
	 * @param datasourceId 
	 */
	private void deleteByIp(Set<DataSourcesRelationParam> set, int componentId, int indicatorGroupId, int datasourceId) {
		List<RelevantContraint> re = relevantContraintMapper.getInvalid(set, componentId, indicatorGroupId);
		deleteRelevantUuid(re,datasourceId);
		List<Integer> idList = re.stream().map(RelevantContraint::getId).collect(Collectors.toList());
		if(!evUtil.listIsNullOrZero(idList)) {
			relevantContraintMapper.deleteBatchIds(idList);
		}
	}
	
	/**
	 * 删除uuid
	 * @param relevantcontraint
	 * @param sourcesId
	 */
	private void deleteRelevantUuid(List<RelevantContraint> relevantcontraint, int sourcesId) {
		if(!evUtil.listIsNullOrZero(relevantcontraint)) {
			List<String> values = relevantcontraint.stream().map(RelevantContraint::getValue).collect(Collectors.toList());
			EntityWrapper<DsUuid> wrapper=new EntityWrapper<>();
			wrapper.where("dataSourceId", sourcesId).in("value", values);
			List<DsUuid> dsuuid=dsUuidMapper.selectList(wrapper);
			if(!evUtil.listIsNullOrZero(dsuuid)) {
				List<Long> innerId = dsuuid.stream().map(DsUuid::getId).collect(Collectors.toList());
				EntityWrapper<DsUuidMapping> wra=new EntityWrapper<>();
				wra.in("innerUUID", innerId);
				dsUuidMappingMapper.delete(wra);
			}
			dsUuidMapper.delete(wrapper);
		}
	}
	/**
	 * 获取组件下的IP port、
	 * 
	 * @param componentId
	 * @return
	 */
	private Set<DataSourcesRelationParam> getIpAndPort(int componentId) {
		// 获取组件下的IP port、
		Map<String, Object> col = new HashMap<>(1);
		col.put("componentId", componentId);
		List<DataSourcesRelationParam> all = new ArrayList<>();
		// 获取组件服务器组
		List<ServerGroup> groupids = serverGroupMapper.selectByMap(col);
		for (ServerGroup serverGroup : groupids) {
			List<DataSourcesRelationParam> li = serverIpMapper.selectIpAndport(serverGroup.getId());
			all.addAll(li);
		}
		// 去重
		Set<DataSourcesRelationParam> set = new HashSet<>();
		set.addAll(all);
		return set;
	}
}
