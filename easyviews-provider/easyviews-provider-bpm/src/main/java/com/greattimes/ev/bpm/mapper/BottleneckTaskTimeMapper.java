package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.BottleneckTaskTime;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 智能瓶颈报表任务数据时间表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface BottleneckTaskTimeMapper extends BaseMapper<BottleneckTaskTime> {

	/**批量插入
	 * @param timeList
	 */
	void batchInsert(@Param("list")List<BottleneckTaskTime> list);

}
