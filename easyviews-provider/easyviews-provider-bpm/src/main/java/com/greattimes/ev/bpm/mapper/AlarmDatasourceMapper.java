package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmDatasource;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警数据源表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-15
 */
public interface AlarmDatasourceMapper extends BaseMapper<AlarmDatasource> {

}
