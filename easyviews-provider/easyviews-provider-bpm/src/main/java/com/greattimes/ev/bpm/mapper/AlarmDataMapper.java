package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmData;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警数据表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
public interface AlarmDataMapper extends BaseMapper<AlarmData> {

	List<Map<String, Object>> getCustom(Integer id);

	void batchInsert(@Param("list")List<Map<String, Object>> list);

}
