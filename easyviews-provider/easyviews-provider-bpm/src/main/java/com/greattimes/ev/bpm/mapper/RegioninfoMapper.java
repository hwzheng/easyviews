package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.entity.Regioninfo;

import java.util.List;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-02
 */
public interface RegioninfoMapper extends BaseMapper<Regioninfo> {

	/**
	 * 省份查询
	 * @return
	 */
	List<DictionaryParam> selectProvince();

	/**
	 * 地市查询
	 * @param provinceId
	 * @return
	 */
	List<DictionaryParam> selectCity(int provinceId);

}
