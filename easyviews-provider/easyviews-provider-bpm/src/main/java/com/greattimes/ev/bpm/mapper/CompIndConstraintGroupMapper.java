package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.CompIndConstraintGroup;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 组件指标约束 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface CompIndConstraintGroupMapper extends BaseMapper<CompIndConstraintGroup> {

}
