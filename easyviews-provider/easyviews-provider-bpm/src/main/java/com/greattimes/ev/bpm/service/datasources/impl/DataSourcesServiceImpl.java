package com.greattimes.ev.bpm.service.datasources.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.datasources.param.req.CompIndConstraintGroupParam;
import com.greattimes.ev.bpm.datasources.param.req.CompIndContraintParam;
import com.greattimes.ev.bpm.datasources.param.req.ComponentIndicatorsParam;
import com.greattimes.ev.bpm.datasources.param.req.ConstraintFieldParam;
import com.greattimes.ev.bpm.datasources.param.req.IndicatorGroupParam;
import com.greattimes.ev.bpm.datasources.param.req.IndicatorsParam;
import com.greattimes.ev.bpm.entity.CompIndConstraint;
import com.greattimes.ev.bpm.entity.CompIndConstraintGroup;
import com.greattimes.ev.bpm.entity.ComponentIndicators;
import com.greattimes.ev.bpm.entity.ConstraintField;
import com.greattimes.ev.bpm.entity.DataSources;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;
import com.greattimes.ev.bpm.entity.DsUuid;
import com.greattimes.ev.bpm.entity.DsUuidMapping;
import com.greattimes.ev.bpm.entity.Dscomponent;
import com.greattimes.ev.bpm.entity.IndicatorContraint;
import com.greattimes.ev.bpm.entity.IndicatorGroup;
import com.greattimes.ev.bpm.entity.Indicators;
import com.greattimes.ev.bpm.entity.RelevantContraint;
import com.greattimes.ev.bpm.mapper.CompIndConstraintGroupMapper;
import com.greattimes.ev.bpm.mapper.CompIndConstraintMapper;
import com.greattimes.ev.bpm.mapper.ComponentIndicatorsMapper;
import com.greattimes.ev.bpm.mapper.ConstraintFieldMapper;
import com.greattimes.ev.bpm.mapper.DataSourcesMapper;
import com.greattimes.ev.bpm.mapper.DatasourceRelevantMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMapper;
import com.greattimes.ev.bpm.mapper.DsUuidMappingMapper;
import com.greattimes.ev.bpm.mapper.DscomponentMapper;
import com.greattimes.ev.bpm.mapper.IndicatorContraintMapper;
import com.greattimes.ev.bpm.mapper.IndicatorGroupMapper;
import com.greattimes.ev.bpm.mapper.IndicatorsMapper;
import com.greattimes.ev.bpm.mapper.RelevantContraintMapper;
import com.greattimes.ev.bpm.service.common.IDeleteRuleService;
import com.greattimes.ev.bpm.service.datasources.IDataSourcesService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 数据源配置表 服务实现类
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@Service
public class DataSourcesServiceImpl extends ServiceImpl<DataSourcesMapper, DataSources> implements IDataSourcesService {
	@Autowired
	private DscomponentMapper dscomponentMapper;
	@Autowired
	private IndicatorGroupMapper indicatorGroupMapper;
	@Autowired
	private ComponentIndicatorsMapper componentIndicatorsMapper;
	@Autowired
	private CompIndConstraintMapper compIndConstraintMapper;
	@Autowired
	private CompIndConstraintGroupMapper compIndConstraintGroupMapper;
	@Autowired
	private DataSourcesMapper dataSourcesMapper;
	@Autowired
	private IndicatorsMapper indicatorsMapper;
	@Autowired
	private ConstraintFieldMapper constraintFieldMapper;
	@Autowired
	private IndicatorContraintMapper indicatorContraintMapper;
	@Autowired
	private RelevantContraintMapper relevantContraintMapper;
	@Autowired
	private DatasourceRelevantMapper datasourceRelevantMapper;
	@Autowired
	private DsUuidMapper dsuuidmapper;
	@Autowired
	private DsUuidMappingMapper dsuuidmappingmapper;

	@Override
	public List<Map<String, Object>> selectComponent(Integer sourcesId) {
		EntityWrapper<Dscomponent> wrapper = new EntityWrapper<>();
		wrapper.where("sourcesId={0}", sourcesId).orderBy("parentId,sort");
		List<Dscomponent> list = dscomponentMapper.selectList(wrapper);
		List<Map<String, Object>> jtList = new ArrayList<Map<String, Object>>();
		for (Dscomponent dscomponent : list) {
			if (dscomponent.getParentId() == -1) {
				Map<String, Object> jtMap = new HashMap<String, Object>();
				jtMap.put("id", dscomponent.getId());
				jtMap.put("title", dscomponent.getName());
				jtMap.put("parentId", dscomponent.getParentId());
				jtList.add(jtMap);
			}
		}
		for (Map<String, Object> map : jtList) {
			getComponentChildren(map, list);
		}
		if (!evUtil.listIsNullOrZero(jtList)) {
			expandParent(jtList.get(0), list);
		}
		return jtList;
	}

	/**
	 * 展开第一个叶子节点
	 * 
	 * @param map
	 * @param list
	 * @return
	 */
	private void expandParent(Map<String, Object> map, List<Dscomponent> list) {
		for (Dscomponent ds : list) {
			if (ds.getParentId() == evUtil.getMapIntValue(map, "id")) {
				List<Map<String, Object>> li=(List<Map<String, Object>>) map.get("children");
				if(!evUtil.listIsNullOrZero(li)) {
					expandParent(li.get(0), list);
				}
			} else {
				map.put("expand", true);
			}
		}
	}

	/**
	 * 数据源组件子节点查询
	 * 
	 * @param jtMap
	 * @param list
	 */
	private void getComponentChildren(Map<String, Object> jtMap, List<Dscomponent> list) {
		for (Dscomponent ds : list) {
			if (ds.getParentId() == evUtil.getMapIntValue(jtMap, "id")) {
				List<Map<String, Object>> children;
				if (jtMap.get("children") != null) {
					children = (List<Map<String, Object>>) jtMap.get("children");
				} else {
					children = new ArrayList<Map<String, Object>>();
				}
				Map<String, Object> childrenMap = new HashMap<String, Object>();
				childrenMap.put("id", ds.getId());
				childrenMap.put("title", ds.getName());
				childrenMap.put("parentId", ds.getParentId());
				children.add(childrenMap);
				jtMap.put("children", children);
				getComponentChildren(childrenMap, list);
			}
		}
	}

	@Override
	public int addComponent(Integer sourcesId, Integer treeId, String title) {
		EntityWrapper<Dscomponent> wrapper = new EntityWrapper<>();
		wrapper.where("sourcesId={0}", sourcesId).eq("parentId", treeId).orderBy("sort", false);
		List<Dscomponent> list = dscomponentMapper.selectList(wrapper);
		Dscomponent entity = new Dscomponent();
		if (evUtil.listIsNullOrZero(list)) {
			entity.setSort(1);
		} else {
			entity.setSort(list.get(0).getSort() + 1);
		}
		entity.setSourcesId(sourcesId);
		entity.setName(title);
		entity.setParentId(treeId);
		dscomponentMapper.insert(entity);
		return entity.getId();
	}

	@Override
	public void deleteComponent(Integer treeId) {
		List<Integer> idList = new ArrayList<>();
		getCompontlist(treeId, idList);
		idList.add(treeId);
		// 删除数据配置
		for (Integer integer : idList) {
			Map<String, Object> columnMap = new HashMap<>();
			columnMap.put("componentId", integer);
			List<ComponentIndicators> ids = componentIndicatorsMapper.selectByMap(columnMap);
			for (ComponentIndicators componentIndicators : ids) {
				deleteContraint(componentIndicators.getId());
			}
		}
		if(!evUtil.listIsNullOrZero(idList)) {
			dscomponentMapper.deleteBatchIds(idList);
		}
	}

	/**
	 * 获取子节点list
	 * 
	 * @param treeId
	 * @param ids
	 */
	private void getCompontlist(Integer treeId, List<Integer> ids) {
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("parentId", treeId);
		List<Dscomponent> ds = dscomponentMapper.selectByMap(columnMap);
		if (!evUtil.listIsNullOrZero(ds)) {
			for (Dscomponent dscomponent : ds) {
				ids.add(dscomponent.getId());
				getCompontlist(dscomponent.getId(), ids);
			}
		}
	}

	@Override
	public Boolean checkName(Dscomponent ds, int flag) {
		List<Dscomponent> list = new ArrayList<>();
		if (flag == 0) {
			EntityWrapper<Dscomponent> wrapper = new EntityWrapper<>();
			wrapper.where("name={0}", ds.getName()).and("sourcesId={0}", ds.getSourcesId()).ne("id", ds.getId());
			list = dscomponentMapper.selectList(wrapper);
		} else {
			Map<String, Object> nameMap = new HashMap<>();
			nameMap.put("name", ds.getName());
			nameMap.put("sourcesId", ds.getSourcesId());
			list = dscomponentMapper.selectByMap(nameMap);
		}
		return evUtil.listIsNullOrZero(list);
	}

	@Override
	public void editComponent(Integer sourcesId, Integer treeId, String title) {
		Dscomponent entity = new Dscomponent();
		entity.setId(treeId);
		entity.setSourcesId(sourcesId);
		entity.setName(title);
		dscomponentMapper.updateById(entity);
	}

	@Override
	public List<ComponentIndicatorsParam> selectIndicatorGroup(Integer sourcesId) {
		List<ComponentIndicatorsParam> paramList = new ArrayList<>();
		List<Map<String, Object>> list = indicatorGroupMapper.selectIndicatorGroup(sourcesId);
		if (!evUtil.listIsNullOrZero(list)) {
			for (Map<String, Object> map : list) {
				ComponentIndicatorsParam in = new ComponentIndicatorsParam();
				in.setId(evUtil.getMapIntValue(map, "id"));
				in.setName(evUtil.getMapStrValue(map, "name"));
				String str = evUtil.getMapStrValue(map, "constraintName");
				String fieldId = evUtil.getMapStrValue(map, "fieldId");
				List<CompIndContraintParam> constraints = new ArrayList<>();
				if (!str.equals("")) {
					String[] arr = str.split(",");
					String[] arr1 = fieldId.split(",");
					for (int i = 0; i < arr.length; i++) {
						CompIndContraintParam cf = new CompIndContraintParam();
						cf.setName(arr[i]);
						cf.setFieldId(Integer.parseInt(arr1[i]));
						constraints.add(cf);
					}
				}
				in.setConstraint(constraints);
				paramList.add(in);
			}
		}
		return paramList;
	}

	@Override
	public List<ComponentIndicatorsParam> selectContraint(Integer componentId) {
		List<ComponentIndicatorsParam> paramList = new ArrayList<>();
		List<Map<String, Object>> list = componentIndicatorsMapper.selectContraintGroup(componentId);
		if (!evUtil.listIsNullOrZero(list)) {
			for (Map<String, Object> map : list) {
				ComponentIndicatorsParam in = new ComponentIndicatorsParam();
				in.setId(evUtil.getMapIntValue(map, "id"));
				in.setIndicatorGroupId(evUtil.getMapIntValue(map, "indicatorGroupId"));
				in.setName(evUtil.getMapStrValue(map, "name"));
				in.setActive(evUtil.getMapIntValue(map, "active"));
				String groupIds = evUtil.getMapStrValue(map, "constraintGroupId");
				// 约束组层
				List<CompIndConstraintGroupParam> constraintValue = new ArrayList<>();
				if (!groupIds.equals("")) {
					String[] arr = groupIds.split(",");
					for (String string : arr) {
						CompIndConstraintGroupParam p = new CompIndConstraintGroupParam();
						p.setConstraintGroupId(Integer.parseInt(string));
						// 约束值层
						List<CompIndContraintParam> valueList = new ArrayList<>();
						List<Map<String, Object>> values = compIndConstraintMapper
								.selectByGroupId(p.getConstraintGroupId());
						for (Map<String, Object> map2 : values) {
							CompIndContraintParam contraint = new CompIndContraintParam();
							contraint.setFieldId(evUtil.getMapIntValue(map2, "fieldId"));
							contraint.setName(evUtil.getMapStrValue(map2, "name"));
							contraint.setValue(evUtil.getMapStrValue(map2, "value"));
							valueList.add(contraint);
						}
						p.setConstraintGroup(valueList);
						constraintValue.add(p);
					}
				}
				in.setConstraintValue(constraintValue);
				paramList.add(in);
			}
		}
		return paramList;
	}

	@Override
	public void updateActive(Integer id, Integer active) {
		ComponentIndicators entity = new ComponentIndicators();
		entity.setId(id);
		entity.setActive(active);
		componentIndicatorsMapper.updateById(entity);
	}

	@Override
	public void addContraint(ComponentIndicatorsParam param) {
		Integer indicatorGroupId = param.getIndicatorGroupId();
		Integer componentId = param.getComponentId();
		ComponentIndicators entity = new ComponentIndicators();
		entity.setComponentId(componentId);
		entity.setIndicatorGroupId(indicatorGroupId);
		entity.setActive(1);
		componentIndicatorsMapper.insert(entity);
		List<CompIndConstraintGroupParam> valus = param.getConstraintValue();
		addconstraintValue(entity, valus);
	}

	/**
	 * 添加约束值
	 * 
	 * @param entity
	 * @param valus
	 */
	private void addconstraintValue(ComponentIndicators entity, List<CompIndConstraintGroupParam> valus) {
		for (CompIndConstraintGroupParam compIndConstraintGroupParam : valus) {
			CompIndConstraintGroup en = new CompIndConstraintGroup();
			en.setCompIndicatorsId(entity.getId());
			compIndConstraintGroupMapper.insert(en);
			List<CompIndContraintParam> constraintGroup = compIndConstraintGroupParam.getConstraintGroup();
			for (CompIndContraintParam compIndContraintParam : constraintGroup) {
				String value = compIndContraintParam.getValue();
				String[] arr = value.split("\n");
				for (String string : arr) {
					CompIndConstraint e = new CompIndConstraint();
					e.setConstraintGroupId(en.getId());
					e.setFieldId(compIndContraintParam.getFieldId());
					e.setValue(string);
					compIndConstraintMapper.insert(e);
				}
			}
		}
	}

	@Override
	public void editContraint(ComponentIndicatorsParam param) {
		Integer id = param.getId();
		Integer indicatorGroupId = param.getIndicatorGroupId();
		ComponentIndicators entity = new ComponentIndicators();
		entity.setId(id);
		entity.setIndicatorGroupId(indicatorGroupId);
		componentIndicatorsMapper.updateById(entity);
		deleteConstraintValue(id);
		List<CompIndConstraintGroupParam> valus = param.getConstraintValue();
		addconstraintValue(entity, valus);
	}

	/**
	 * 删除约束值
	 * 
	 * @param id
	 */
	private void deleteConstraintValue(Integer id) {
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("compIndicatorsId", id);
		List<CompIndConstraintGroup> groupids = compIndConstraintGroupMapper.selectByMap(columnMap);
		for (CompIndConstraintGroup compIndConstraintGroup : groupids) {
			columnMap.clear();
			columnMap.put("constraintGroupId", compIndConstraintGroup.getId());
			compIndConstraintMapper.deleteByMap(columnMap);
		}
		columnMap.clear();
		columnMap.put("compIndicatorsId", id);
		compIndConstraintGroupMapper.deleteByMap(columnMap);
	}

	@Override
	public List<Map<String, Object>> select() {
		return dataSourcesMapper.findList();
	}

	@Override
	public void deleteDataSourceById(int id) {
		Map<String, Object> columnMap = new HashMap<>();
		columnMap.put("sourcesId", id);
		List<IndicatorGroup> group = indicatorGroupMapper.selectByMap(columnMap);
		if (!evUtil.listIsNullOrZero(group)) {
			List<Integer> ids = group.stream().map(IndicatorGroup::getId).collect(Collectors.toList());
			for (Integer groupId : ids) {
				// 删除指标组
				deleteGroupById(groupId);
			}
		}
		// ds_data_sources
		dataSourcesMapper.deleteById(id);
	}

	@Override
	public DataSources saveOrUpdate(DataSources dataSources, int userId) {
		Date now = new Date();
		if (dataSources.getId() == null) {
			// 新增
			dataSources.setActive(1);
			dataSources.setSyncType(1);
			dataSources.setUpdateBy(userId);
			dataSources.setUpdateTime(now);
			dataSources.setCreateBy(userId);
			dataSources.setCreateTime(now);
			dataSourcesMapper.insert(dataSources);
			return dataSources;
		} else {
			// 更新
			DataSources sourceDataSources = dataSourcesMapper.selectById(dataSources.getId());
			sourceDataSources.setUpdateBy(userId);
			sourceDataSources.setUpdateTime(now);
			sourceDataSources.setDesc(dataSources.getDesc());
			sourceDataSources.setName(dataSources.getName());
			sourceDataSources.setType(dataSources.getType());
			dataSourcesMapper.updateAllColumnById(sourceDataSources);
			return sourceDataSources;
		}
	}

	@Override
	public List<Map<String, Object>> selectIndicatorGroupBySourcesId(int sourcesId) {
		List<Map<String, Object>> result = new ArrayList<>();
		List<IndicatorGroupParam> list = indicatorGroupMapper.findIndicatorGroupListBySourcesId(sourcesId);
		if (!evUtil.listIsNullOrZero(list)) {
			list.stream().forEach(x -> {
				Map<String, Object> map = new HashMap<>();
				map.put("id", x.getId());
				map.put("name", x.getName());
				StringBuilder sb = new StringBuilder("");
				if (!evUtil.listIsNullOrZero(x.getConstraints())) {
					x.getConstraints().forEach(y -> sb.append(y.getName()).append(","));
				}
				map.put("constraintName", sb.length() > 1 ? sb.substring(0, sb.lastIndexOf(",")) : sb.toString());
				map.put("constraints", x.getConstraints());
				map.put("level", x.getLevel());
				map.put("interval", x.getInterval() + x.getUnitName());
				sb.delete(0, sb.length());
				if (!evUtil.listIsNullOrZero(x.getIndicators())) {
					x.getIndicators().forEach(y -> sb.append(y.getName()).append(","));
				}
				map.put("indicatorName", sb.length() > 1 ? sb.substring(0, sb.lastIndexOf(",")) : sb.toString());
				map.put("indicators", x.getIndicators());
				result.add(map);
			});
		}
		return result;
	}

	@Override
	public int saveOrUpdateIndicator(IndicatorGroupParam indicatorGroupParam) {
		boolean isAdd = false;
		if (indicatorGroupParam.getId() == null) {
			isAdd = true;
		}
		boolean constraintFieldHasChanage = false;
		// ds_indicator_group
		IndicatorGroup indicatorGroup = new IndicatorGroup();
		if (!isAdd) {
			// edit
			indicatorGroup = indicatorGroupMapper.selectById(indicatorGroupParam.getId());
			// 判断约束是否改变
			List<ConstraintFieldParam> constraintFieldParams = indicatorGroupParam.getConstraints();
			// 原来约束
			EntityWrapper<ConstraintField> constraintFieldEntityWrapper = new EntityWrapper<>();
			constraintFieldEntityWrapper.where("groupId={0}", indicatorGroupParam.getId()).orderBy("sort", true);
			List<ConstraintField> constraintFieldList = constraintFieldMapper.selectList(constraintFieldEntityWrapper);
			if (indicatorGroup.getLevel().intValue() != indicatorGroupParam.getLevel().intValue()) {
				constraintFieldHasChanage = true;
			} else {
				if (constraintFieldParams.size() != constraintFieldList.size()) {
					constraintFieldHasChanage = true;
				} else {
					for (int i = 0; i < constraintFieldParams.size(); i++) {
						if (!constraintFieldParams.get(i).getName().equals(constraintFieldList.get(i).getName())) {
							constraintFieldHasChanage = true;
							break;
						}
					}
				}
			}
			BeanUtils.copyProperties(indicatorGroupParam, indicatorGroup);
			indicatorGroupMapper.updateAllColumnById(indicatorGroup);
			Map<String, Object> map = new HashMap<>(1);
			map.put("groupId", indicatorGroup.getId());
			// delete ds_indicators
			indicatorsMapper.deleteByMap(map);
			// 约束改变,先删除后增加ds_constraint_field
			if (constraintFieldHasChanage) {
				// delete ds_constraint_field
				constraintFieldMapper.deleteByMap(map);
			}
			// ds_indicator_contraint
			Map<String, Object> colmap = new HashMap<>(1);
			colmap.put("indicatorGroupId", indicatorGroupParam.getId());
			indicatorContraintMapper.deleteByMap(colmap);
		} else {
			// add
			// source target
			BeanUtils.copyProperties(indicatorGroupParam, indicatorGroup);
			indicatorGroup.setStorageMode(1);
			indicatorGroupMapper.insert(indicatorGroup);
		}

		Integer groupId = indicatorGroup.getId();
		// ds_indicators
		List<IndicatorsParam> indicatorsParamList = indicatorGroupParam.getIndicators();
		List<Indicators> indicatorsList = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(indicatorsParamList)) {
			int i = 1;
			for (IndicatorsParam indicatorsParam : indicatorsParamList) {
				Indicators indicators = new Indicators();
				indicators.setGroupId(groupId);
				indicators.setName(indicatorsParam.getName());
				indicators.setUnit(indicatorsParam.getUnit());
				indicators.setSort(i++);
				indicatorsList.add(indicators);
			}
			indicatorsMapper.batchInsert(indicatorsList);
		}

		/**
		 * 编辑状态并且约束没有改变，则不进行ds_constraint_field增加操作
		 */
		if (!isAdd && !constraintFieldHasChanage) {
			return 1;
		}
		// ds_constraint_field
		List<ConstraintFieldParam> constraintFieldParamList = indicatorGroupParam.getConstraints();
		List<ConstraintField> constraintFieldList = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(constraintFieldParamList)) {
			int i = 1;
			for (ConstraintFieldParam constraintFieldParam : constraintFieldParamList) {
				ConstraintField constraintField = new ConstraintField();
				constraintField.setGroupId(groupId);
				constraintField.setName(constraintFieldParam.getName());
				constraintField.setSort(i++);
				constraintFieldList.add(constraintField);
			}
			constraintFieldMapper.batchInsert(constraintFieldList);
		}


		return 1;
	}

	@Override
	public IndicatorGroupParam indicatorDetail(int id) {
		return indicatorGroupMapper.findIndicatorById(id);
	}

	@Override
	public boolean isHasSameIndicatorGroupName(String name, int id, int sourcesId) {
		EntityWrapper<IndicatorGroup> ew = new EntityWrapper<>();
		ew.where("name={0}", name).and("sourcesId={0}", sourcesId).ne("id", id);
		List<IndicatorGroup> result = indicatorGroupMapper.selectList(ew);
		if (evUtil.listIsNullOrZero(result)) {
			return false;
		}
		return true;
	}

	@Override
	public int deleteGroupById(int id) {
		Map<String, Object> map = new HashMap<>(1);
		map.clear();
		map.put("groupId", id);

		// ds_indicators
		indicatorsMapper.deleteByMap(map);

		// ds_constraint_field
		constraintFieldMapper.deleteByMap(map);
		// ds_indicator_group
		indicatorGroupMapper.deleteById(id);
		// ds_indicator_contraint
		map.clear();
		map.put("indicatorGroupId", id);
		indicatorContraintMapper.deleteByMap(map);
		//deleteRelevantData(id);
		
		return 1;
	}

	/**
	 * 删除数据关联的相关表
	 * @param id 指标组id
	 */
	/*private void deleteRelevantData(int id) {
		Map<String, Object> map = new HashMap<>();
		// 获取数据源id
		int sources = indicatorGroupMapper.selectById(id).getSourcesId();
		// ds_indicator_contraint
		map.clear();
		map.put("indicatorGroupId", id);
		indicatorContraintMapper.deleteByMap(map);
		List<RelevantContraint> re = relevantContraintMapper.selectByMap(map);
		if (!evUtil.listIsNullOrZero(re)) {
			List<String> value = re.stream().map(RelevantContraint::getValue).collect(Collectors.toList());
			EntityWrapper<DsUuid> wrapper = new EntityWrapper<>();
			wrapper.where("dataSourceId={0}", sources);
			wrapper.in("value", value);
			List<DsUuid> dsuuid=dsuuidmapper.selectList(wrapper);
			if(!evUtil.listIsNullOrZero(dsuuid)) {
				List<Long> innerId = dsuuid.stream().map(DsUuid::getId).collect(Collectors.toList());
				EntityWrapper<DsUuidMapping> wra=new EntityWrapper<>();
				wra.in("innerUUID", innerId);
				// ds_uuid_mapping
				dsuuidmappingmapper.delete(wra);
			}
			// ds_uuid
			dsuuidmapper.delete(wrapper);
		}
		// ds_relevant_contraint
		relevantContraintMapper.deleteByMap(map);
	}*/

	@Override
	public boolean isHasSameDataSourceName(String name, int id) {
		EntityWrapper<DataSources> ew = new EntityWrapper<>();
		ew.where("name={0}", name).ne("id", id);
		List<DataSources> result = dataSourcesMapper.selectList(ew);
		if (evUtil.listIsNullOrZero(result)) {
			return false;
		}
		return true;
	}

	@Override
	public void deleteContraint(int id) {
		deleteConstraintValue(id);
		componentIndicatorsMapper.deleteById(id);
	}

	@Override
	public boolean checkChildren(Integer componentId, Integer sourcesId) {
		boolean flag = false;
		EntityWrapper<Dscomponent> wrapper = new EntityWrapper<>();
		wrapper.where("sourcesId={0}", sourcesId).orderBy("parentId,sort");
		List<Dscomponent> list = dscomponentMapper.selectList(wrapper);
		for (Dscomponent ds : list) {
			if (ds.getParentId().intValue() == componentId.intValue()) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	@Override
	public List<Indicators> selectDsIndicatorsByGroupId(int groupId) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("groupId", groupId);
		return indicatorsMapper.selectByMap(map);
	}

	@Override
	public Map<String, Integer> selectDsIndicatorsMapByGroupId(int groupId) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("groupId", groupId);
		return indicatorsMapper.selectByMap(map).stream()
				.collect(Collectors.toMap(Indicators::getName, Indicators::getId));
	}

	@Override
	public void insertBatchDsIndicatorContraint(List<IndicatorContraint> indicatorContraints) {
		while (indicatorContraints.size() > 30) {
			List<IndicatorContraint> subList = indicatorContraints.subList(0, 30);
			indicatorContraintMapper.batchInsert(subList);
			subList.clear();
		}
		if (indicatorContraints.size() != 0) {
			indicatorContraintMapper.batchInsert(indicatorContraints);
		}
	}

	@Override
	public List<IndicatorContraint> selectIndicatorContraintByGroupId(int id) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("indicatorGroupId", id);
		return indicatorContraintMapper.selectByMap(map);
	}

	@Override
	public List<DictionaryParam> selectRelevantContraintDict(int componentId, int dataSourceId, int groupId) {
		List<DictionaryParam> dictionaryParams = new ArrayList<>();
		Map<String, Object> map = new HashMap<>();
		map.put("componentId", componentId);
		map.put("dataSourceId", dataSourceId);
		map.put("indicatorGroupId", groupId);
		List<DatasourceRelevant> datasourceRelevants = datasourceRelevantMapper.selectByMap(map);
		if (evUtil.listIsNullOrZero(datasourceRelevants)) {
			return dictionaryParams;
		}
		/**
		 * '关联层级 1 ip 2 port', '挂载点 0组件 1 ip 2 port',
		 */
		Integer relevantField = datasourceRelevants.get(0).getRelevantField();
		Integer mount = datasourceRelevants.get(0).getMount();
		if (relevantField != null) {
			if (relevantField.intValue() == 1) {
				map.put("port", -1);
			}
		} else {
			if (mount == 0) {
				map.put("port", -1);
				map.put("ip", -1);
			} else if (mount == 1) {
				map.put("port", -1);
			}
		}
		return relevantContraintMapper.selectRelevantContraintDictByMap(map);
	}

	@Override
	public void deleteIndicatorContraintByMap(Map<String, Object> map) {
		indicatorContraintMapper.deleteByMap(map);
	}
}
