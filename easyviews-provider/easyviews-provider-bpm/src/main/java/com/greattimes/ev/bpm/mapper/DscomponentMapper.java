package com.greattimes.ev.bpm.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.Dscomponent;

/**
 * <p>
 * 数据源组件表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface DscomponentMapper extends BaseMapper<Dscomponent> {

}
