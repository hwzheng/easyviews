package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.AnalyzeIndicator;

/**
 * <p>
 * 自定义分析指标 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-06-27
 */
public interface AnalyzeIndicatorMapper extends BaseMapper<AnalyzeIndicator> {

	/**
	 * 指标信息列表查询
	 * @param customId
	 * @return
	 */
	List<Map<String, Object>> selectIndicator(int customId);

	/**
	 * 查询分析指标 主要提供给路径图使用
	 * @author NJ
	 * @date 2018/9/29 10:51
	 * @param param
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectAnalyzeIndicator(Map<String, Object> param);
}
