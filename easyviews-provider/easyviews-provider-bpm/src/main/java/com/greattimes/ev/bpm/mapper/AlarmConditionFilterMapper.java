package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.AlarmConditionFilter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 告警条件数据过滤表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
public interface AlarmConditionFilterMapper extends BaseMapper<AlarmConditionFilter> {

}
