package com.greattimes.ev.bpm.service.config.impl;

import java.util.*;
import java.util.stream.Collectors;

import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.mapper.*;
import com.greattimes.ev.bpm.service.common.ISequenceService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.entity.Columns;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.config.param.req.ComponentParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.resp.MonitorParam;
import com.greattimes.ev.bpm.service.config.IApplicationService;
import com.greattimes.ev.common.utils.evUtil;

/**
 * <p>
 * 应用表 服务实现类
 * </p>
 *
 * @author Cgc
 * @since 2018-05-18
 */
@Service
public class ApplicationServiceImpl extends ServiceImpl<ApplicationMapper, Application> implements IApplicationService {

	private static final int BATCHNUM = 30;

	@Autowired
	private ApplicationMapper applicationMapper;
	@Autowired
	private UserApplicationMapper userApplicationMapper;
	@Autowired
	private ApplicationCenterMapper applicationCenterMapper;
	@Autowired
	private UuidMapper uuidMapper;
	@Autowired
	private MonitorMapper monitorMapper;
	@Autowired
	private MonitorComponentMapper monitorComponentMapper;
	@Autowired
	private ISequenceService sequenceService;
	@Autowired
	private ComponentMapper componentMapper;
	@Autowired
	private CustomMapper customMapper;
	@Autowired
	private SequenceMapper sequenceMapper;
	@Autowired
	private StatisticsDimensionMapper statisticsDimensionMapper;
	@Autowired
	private DimensionValueMapper dimensionValueMapper;
	@Autowired
	private CtuuidMapper ctuuidMapper;
	@Autowired
	private AlarmDataMapper alarmDataMapper;
	@Autowired
	private AnalyzeIndicatorMapper analyzeIndicatorMapper;
	@Autowired
	private ApplicationAlarmMapper applicationAlarmMapper;



	private Logger logger = LoggerFactory.getLogger(this.getClass());
	private int sequence_add=2;
	/**
	 * 获取自增序列当前值
	 * @author cgc  
	 * @date 2018年10月12日  
	 * @return
	 */
	private int createMenuId() {
		int id = 0;
		Map<String, Object> columnMap=new HashMap<>(1);
		columnMap.put("name", "seq_uuid");
		List<Sequence> se=sequenceMapper.selectByMap(columnMap);
		if(!evUtil.listIsNullOrZero(se)) {
			id=se.get(0).getCurrentValue();
		}else {
			logger.error("生成菜单id时未能找到当前序列");
		}
		return id;

	}
	
	@Override
	public List<ApplicationParam> selectApplications(int userId) {
		return applicationMapper.selectApplicationByUserId(userId, 2);
	}

	@Override
	public List<Application> selectApplications() {
		EntityWrapper<Application> wrapper=new EntityWrapper<>();
		wrapper.ne("active", 2);
		return applicationMapper.selectList(wrapper);
	}

	@Override
	public void deleteRelation(Integer userId, Integer applicationId) {
		Map<String, Object> columnMap = new HashMap<>(2);
		columnMap.put("userId", userId);
		columnMap.put("applicationId", applicationId);
		userApplicationMapper.deleteByMap(columnMap);
	}

	@Override
	public void saveUserApplication(int userId, List<Integer> userAppIds) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("userId", userId);
		userApplicationMapper.deleteByMap(map);
		List<UserApplication> list = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(userAppIds)) {
			for (Integer appid : userAppIds) {
				UserApplication userapp = new UserApplication();
				userapp.setUserId(userId);
				userapp.setApplicationId(appid);
				list.add(userapp);
			}
			userApplicationMapper.insertBatch(list);
		}

	}

	@Override
	public List<DictionaryParam> selectAppAsDictonaryByUserId(int userId) {
		return applicationMapper.selectDictonaryAppByUserId(userId);
	}

	@Override
	public List<ApplicationParam> selectApplicationByUserId(int userId) {
		return applicationMapper.selectApplicationByUserId(userId, 2);
	}
	@Override
	public List<ApplicationParam> selectActiveApplication(int userId) {
		return applicationMapper.selectApplicationByUserId(userId, 1);
	}
	@Override
	public int updateActive(int id, int active) {
		Application app = new Application();
		app.setId(id);
		app.setActive(active);
		return applicationMapper.updateById(app);
	}

	@Override
	public int updateAlarmStatus(int id, int alarmState) {
		Application app = new Application();
		app.setId(id);
		app.setAlarmState(alarmState);
		return applicationMapper.updateById(app);
	}

	@Override
	public void updateSort(List<ApplicationParam> list) {
		while (list.size() > BATCHNUM) {
			List<ApplicationParam> subList = list.subList(0, BATCHNUM);
			applicationMapper.batchUpdateSort(subList);
			subList.clear();
		}
		if (list.size() != 0) {
			applicationMapper.batchUpdateSort(list);
		}
	}

	@Override
	public int delete(int id) {
		// bpm_user_application
		Map<String, Object> map = new HashMap<>(1);
		map.put("applicationId", id);
		// bpm_user_application 逻辑删除
		userApplicationMapper.deleteByMap(map);
		//bpm_alarm_data 删除
		alarmDataMapper.deleteByMap(map);
		Application app = new Application();
		app.setId(id);
		// use active replace state express delete
		app.setActive(2);
		return applicationMapper.updateById(app);
	}

	@Override
	public int add(int userId, ApplicationParam param) {
		int app_Id = sequenceService.sequenceNextVal("seq_uuid");
		// application
		Application app = new Application();
		app.setId(app_Id);
		app.setName(param.getName());
		// 激活状态
		app.setActive(param.getActive());
		// 告警开启
		app.setAlarmState(param.getAlarmState());
		app.setCreateBy(userId);
		app.setUpdateBy(userId);
		app.setSort(applicationMapper.getMaxAppSort() + 1);
		app.setMonitorNum(param.getMonitorNum());
		// 未删除状态
		app.setState(0);
		Date nowDate = new Date();
		app.setCreateTime(nowDate);
		app.setUpdateTime(nowDate);
		applicationMapper.insert(app);
		// bpm_user_application
		UserApplication userApplication = new UserApplication();
		userApplication.setUserId(userId);
		userApplication.setApplicationId(app.getId());
		List<UserApplication> userAppList = new ArrayList<>();
		userAppList.add(userApplication);
		userApplicationMapper.insertBatch(userAppList);
		// bpm_application_center
		List<ApplicationCenter> appCenterList = new ArrayList<>();
		int appId = app.getId();
		param.getCenterIds().stream().forEach(x -> appCenterList.add(new ApplicationCenter(appId, x)));
		// Arrays.asList(centerIds).stream().forEach(x -> appCenterList.add(new
		// ApplicationCenter(appId,x)));
		while (appCenterList.size() > BATCHNUM) {
			List<ApplicationCenter> subList = appCenterList.subList(0, BATCHNUM);
			applicationCenterMapper.batchInsertAppCenter(subList);
			subList.clear();
		}
		if (appCenterList.size() != 0) {
			applicationCenterMapper.batchInsertAppCenter(appCenterList);
		}
		// bpm_uuid
		String uuIdValue = "applicationid:" + app.getId();
		Map<String, Object> map = new HashMap<String, Object>(1) {
			{
				put("value", uuIdValue);
			}
		};
		List<Uuid> uids = uuidMapper.selectByMap(map);
		if (evUtil.listIsNullOrZero(uids)) {
			Uuid uuid = new Uuid();
			uuid.setId((long) app_Id);
			uuid.setActive(1);
			uuid.setContraintGroupId(1);
			uuid.setValue(uuIdValue);
			uuidMapper.insert(uuid);
		}
		return app.getId();
	}

	@Override
	public int edit(int userId, ApplicationParam param) {
		// application
		Application app = applicationMapper.selectById(param.getId());
		if (app == null) {
			return 0;
		}
		app.setUpdateTime(new Date());
		app.setUpdateBy(userId);
		app.setName(param.getName());
		app.setActive(param.getActive());
		app.setAlarmState(param.getAlarmState());
		app.setMonitorNum(param.getMonitorNum());
		applicationMapper.updateById(app);
		// bpm_application_center 先删除，后新增
		List<ApplicationCenter> appCenterList = new ArrayList<>();
		param.getCenterIds().stream().forEach(x -> appCenterList.add(new ApplicationCenter(app.getId(), x)));
		Map<String, Object> map = new HashMap<String, Object>(1) {
			{
				put("applicationId", app.getId());
			}
		};
		applicationCenterMapper.deleteByMap(map);
		// 新增
		while (appCenterList.size() > BATCHNUM) {
			List<ApplicationCenter> subList = appCenterList.subList(0, BATCHNUM);
			applicationCenterMapper.batchInsertAppCenter(subList);
			subList.clear();
		}
		if (appCenterList.size() != 0) {
			applicationCenterMapper.batchInsertAppCenter(appCenterList);
		}
		return 1;
	}

	@Override
	public ApplicationParam getDetail(int id) {
		ApplicationParam param = new ApplicationParam();
		BeanUtils.copyProperties(applicationMapper.selectById(id), param);
		// 查询centerIds
		Map<String, Object> map = new HashMap<String, Object>(1) {
			{
				put("applicationId", id);
			}
		};
		List<ApplicationCenter> list = applicationCenterMapper.selectByMap(map);
		List<Integer> centerIds = new ArrayList<>();
		list.stream().forEach(x -> centerIds.add(x.getCenterId()));
		param.setCenterIds(centerIds);
		return param;
	}

	@Override
	public int updateBatchActive(int id, int active) {
		applicationMapper.updateBatchActive(id, active);
		return 1;
	}

	@Override
	public int updateBatchAlarmStatus(int userId, int alarmState) {
		applicationMapper.updateBatchAlarmStatus(userId, alarmState);
		return 1;
	}

	@Override
	public boolean isHasSameAppName(int id, String name) {
		EntityWrapper<Application> ew = new EntityWrapper<Application>();
		Application application = new Application();
		ew.setEntity(application);
		// 排除逻辑删除的应用 author by nj
		ew.where("name={0}", name).and("active != 2").ne("id", id);
		List<Application> result = applicationMapper.selectList(ew);
		if (evUtil.listIsNullOrZero(result)) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isHasSameMonitorName(int appId, int monitorId, String name, boolean isAdd) {
		if(!isAdd){
			Monitor monitor = monitorMapper.selectById(monitorId);
			appId = monitor.getApplicationid();
		}
		EntityWrapper<Monitor> entityWrapper = new EntityWrapper<>();
		entityWrapper.where("name={0}", name).
				and("applicationid={0}", appId).ne("id", monitorId);
		List<Monitor> monitors = monitorMapper.selectList(entityWrapper);
		if(evUtil.listIsNullOrZero(monitors)){
			return false;
		}
		return true;
	}

	@Override
	public boolean saveOrUpdateMonitor(String name, Integer componentNum, Integer applicationId, Integer id) {
		boolean result = false;
		// 新增
		if (id == null) {
			Monitor monitor = new Monitor();
			monitor.setActive(1);
			monitor.setId(sequenceService.sequenceNextVal("seq_uuid"));
			monitor.setApplicationid(applicationId);
			monitor.setName(name);
			monitor.setComponentId(componentNum);
			EntityWrapper<Monitor> ew = new EntityWrapper<>();
			ew.setSqlSelect(Columns.create().column("IFNULL(MAX(sort), 0)", "sort")).where("applicationid={0}",
					applicationId);
			monitor.setSort(monitorMapper.selectList(ew).get(0).getSort() + 1);
			Integer num = monitorMapper.insert(monitor);
			if (num != null && num.intValue() == 1) {
				result = true;
			}
			// uuid
			Uuid uuid = new Uuid();
			uuid.setId((long) monitor.getId());
			uuid.setContraintGroupId(1);
			uuid.setActive(1);
			uuid.setValue("applicationid:" + applicationId + "," + "monitorid:" + monitor.getId());
			uuidMapper.insert(uuid);
		} else {
			// 编辑
			Monitor monitor = new Monitor();
			monitor.setId(id);
			monitor.setName(name);
			monitor.setComponentId(componentNum);
			Integer num = monitorMapper.updateById(monitor);
			if (num != null && num.intValue() == 1) {
				result = true;
			}
		}
		return result;
	}

	@Override
	public boolean deleteMonitor(int id) {
		Monitor monitor = monitorMapper.selectById(id);
		// bpm_monitor
		monitorMapper.deleteById(id);
		// bpm_monitor_component
		Map<String, Object> map = new HashMap<>(1);
		map.put("monitorId", id);
		monitorComponentMapper.deleteByMap(map);
		// uuid
		map.clear();
		map.put("value", "applicationid:" + monitor.getApplicationid() + "," + "monitorid:" + monitor.getId());
		uuidMapper.deleteByMap(map);

		return true;
	}

	@Override
	public void updateMonitorSort(List<MonitorParam> list) {
		while (list.size() > BATCHNUM) {
			List<MonitorParam> subList = list.subList(0, BATCHNUM);
			applicationMapper.batchUpdateMonitorSort(subList);
			subList.clear();
		}
		if (list.size() != 0) {
			applicationMapper.batchUpdateMonitorSort(list);
		}
	}

	@Override
	public List<MonitorParam> selectMonitorList(int applicationId) {
		return applicationMapper.selectMonitorList(applicationId);
	}

	@Override
	public Map<String, Object> selectMonitorDetail(int id) {
		EntityWrapper<Monitor> ew = new EntityWrapper<>();
		ew.setSqlSelect("id, name,componentId AS componentNum").where("id={0}", id);

		List<Map<String, Object>> result = monitorMapper.selectMaps(ew);
		if (!evUtil.listIsNullOrZero(result)) {
			return result.get(0);
		}
		return null;
	}

	@Override
	public List<DictionaryParam> selectMonitorDictionaryByAppId(int appId) {
		return applicationMapper.selectMonitorDictionaryByAppId(appId);
	}

	@Override
	public List<DictionaryParam> selectComponentDictionaryByMonitorId(int monitorId) {
		return applicationMapper.selectComponentDictionaryByMonitorId(monitorId);
	}

	@Override
	public List<MonitorComponent> selectMonitorComponentByMonitorId(int monitorId) {
		Map<String, Object> map = new HashMap<>(1);
		map.put("monitorId", monitorId);
		return monitorComponentMapper.selectByMap(map);
	}

	@Override
	public List<Map<String, Object>> selectMenu(int userId, int type, List<Integer> checkedId, Integer level,
			Integer id, Integer flag, int applicationId) {
		List<Map<String, Object>> menu = new ArrayList<>();
		if (null == level) {
			// 应用层
			menu = getApplication(userId, flag, applicationId);
		} else if (2 == level) {
			menu = getMonitor(applicationId, flag);
		}
		if (type == 1) {
			// 选中相应节点
			if (!evUtil.listIsNullOrZero(checkedId)) {
				for (Integer integer : checkedId) {
					checked(menu, integer);
				}
			}
		}
		return menu;
	}

	/**
	 * 选中相应节点
	 *
	 * @author cgc
	 * @date 2018年9月25日
	 * @param menu
	 * @param id
	 */
	private void checked(List<Map<String, Object>> menu, Integer id) {
		boolean flag = false;
		if (null != menu) {
			for (Map<String, Object> map : menu) {
				if (evUtil.getMapIntValue(map, "id") == id.intValue()) {
					map.put("check", true);
					flag = true;
					break;
				}
			}
			if (!flag) {
				for (Map<String, Object> map : menu) {
					checked((List<Map<String, Object>>) map.get("children"), id);
				}
			}
		}
	}

	/**
	 * 菜单应用层
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param userId
	 * @param flag 
	 * @param applicationId 
	 * @return
	 */
	public List<Map<String, Object>> getApplication(int userId, Integer flag, int applicationId) {
		List<Map<String, Object>> app = new ArrayList<>();
		List<ApplicationParam> appParam = applicationMapper.selectApplicationByUserId(userId,1);
		appParam.stream().forEach(x -> {
			Map<String, Object> appMap = new HashMap<>();
			appMap.put("id", x.getId());
			appMap.put("title", x.getName());
			appMap.put("check", false);
			appMap.put("expand", false);
			appMap.put("type", "application");
			app.add(appMap);
		});
		if (!evUtil.listIsNullOrZero(app)) {
			for (Map<String, Object> map : app) {
				int appId = (int) map.get("id");
				// 监控点层
				if (appId == applicationId) {
					List<Map<String, Object>> mon = getMonitor(appId, flag);
					if (!evUtil.listIsNullOrZero(mon)) {
						map.put("children", mon);
					}
				}
			}
		}
		return app;
	}

	/**
	 * 菜单监控点层
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param appId
	 * @param flag 
	 * @return
	 */
	public List<Map<String, Object>> getMonitor(int appId, Integer flag) {
		List<Map<String, Object>> mon = new ArrayList<>();
		List<DictionaryParam> monParam = applicationMapper.selectMonitorDictionaryByAppId(appId);
		monParam.stream().forEach(x -> {
			Map<String, Object> monMap = new HashMap<>();
			monMap.put("id", Integer.parseInt(x.getValue().toString()));
			monMap.put("parentId", appId);
			monMap.put("name", x.getLabel());
			monMap.put("title", x.getLabel());
			monMap.put("check", false);
			monMap.put("expand", false);
			monMap.put("type", "monitor");
			mon.add(monMap);
		});
		if (!evUtil.listIsNullOrZero(mon)) {
			for (Map<String, Object> map2 : mon) {
				int monitorId = evUtil.getMapIntValue(map2, "id");
				// 组件层
				List<Map<String, Object>> comp =new ArrayList<>();
				//多维
				if(flag.equals(1)) {
					comp = getMultiTreeComponent(monitorId);
				}else {//kpi
					comp = getComponent(monitorId);
				}
				if (!evUtil.listIsNullOrZero(comp)) {
					map2.put("children", comp);
				}
			}
		}
		return mon;

	}

	/**
	 * 菜单组件层
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param monitorId
	 * @return
	 */
	public List<Map<String, Object>> getComponent(int monitorId) {
		List<Map<String, Object>> comp = new ArrayList<>();
		List<Map<String, Object>> comParam = applicationMapper.selectComponentByMonitorId(monitorId);
		comParam.stream().forEach(x -> {
			Map<String, Object> comMap = new HashMap<>();
			comMap.put("id", Integer.parseInt(x.get("value").toString()));
			comMap.put("parentId", monitorId);
			comMap.put("title", x.get("label"));
			comMap.put("check", false);
			comMap.put("key", 0);
			comMap.put("expand", false);
			comMap.put("type", "component");
			comMap.put("statisticalMode", x.get("statisticalMode"));
			comp.add(comMap);
		});
		if (!evUtil.listIsNullOrZero(comp)) {
			for (Map<String, Object> map3 : comp) {
				int statisticalMode=Integer.parseInt(map3.get("statisticalMode").toString());
				List<Map<String, Object>> ipAndCusAll = new ArrayList<>(2);
				int componentId = evUtil.getMapIntValue(map3, "id");
				if(statisticalMode!=1) {
				// ip节点
				Map<String, Object> ipMap = new HashMap<>();
				ipMap.put("parentId", map3.get("id"));
				int ipid=createMenuId()+sequence_add;
				ipMap.put("id", ipid);
				sequence_add++;
				ipMap.put("title", "IP");
				ipMap.put("check", false);
				ipMap.put("checkall", false);
				ipMap.put("indeterminate", false);
				ipMap.put("type", "ipList");
				ipMap.put("expand", false);
				
				// ip层
				List<Map<String, Object>> ip = getIp(componentId,ipid,statisticalMode);
				ipMap.put("children", ip);
				ipAndCusAll.add(ipMap);
				}
				// 业务节点
				Map<String, Object> cusMap = new HashMap<>();
				int cusid=createMenuId()+sequence_add;
				// 业务层
				List<Map<String, Object>> custom = getCustom(componentId,cusid);
				if (!evUtil.listIsNullOrZero(custom)) {
					cusMap.put("id", cusid);
					sequence_add++;
					cusMap.put("parentId", map3.get("id"));
					cusMap.put("title", "事件");
					cusMap.put("check", false);
					cusMap.put("checkall", false);
					cusMap.put("indeterminate", false);
					cusMap.put("type", "customList");
					cusMap.put("expand", false);
					cusMap.put("children", custom);
					ipAndCusAll.add(cusMap);
				}
				if (!evUtil.listIsNullOrZero(ipAndCusAll)) {
					map3.put("children", ipAndCusAll);
				}
			}
		}
		return comp;

	}
	/**
	 * 菜单组件层
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param monitorId
	 * @return
	 */
	public List<Map<String, Object>> getMultiTreeComponent(int monitorId) {
		List<Map<String, Object>> comp = new ArrayList<>();
		List<Map<String, Object>> comParam = applicationMapper.selectComponentByMonitorId(monitorId);
		comParam.stream().forEach(x -> {
			Map<String, Object> comMap = new HashMap<>();
			comMap.put("id", Integer.parseInt(x.get("value").toString()));
			comMap.put("parentId", monitorId);
			comMap.put("title", x.get("label"));
			comMap.put("check", false);
			comMap.put("key", 0);
			comMap.put("expand", false);
			comMap.put("type", "component");
			comMap.put("statisticalMode", x.get("statisticalMode"));
			comp.add(comMap);
		});
		if (!evUtil.listIsNullOrZero(comp)) {
			for (Map<String, Object> map3 : comp) {
				List<Map<String, Object>> custom =new ArrayList<>();
				Map<String, Object> columnMap=new HashMap<>();
				columnMap.put("componentId", map3.get("id"));
				columnMap.put("active", 1);
				List<Custom> customList= customMapper.selectByMap(columnMap);
				customList.stream().forEach(x -> {
					Map<String, Object> cusMap = new HashMap<>();
					cusMap.put("id", x.getId());
					cusMap.put("parentId", x.getComponentId());
					cusMap.put("title", x.getName());
					cusMap.put("check", false);
					cusMap.put("expand", false);
					cusMap.put("type", "custom");
					custom.add(cusMap);
				});
				if(!evUtil.listIsNullOrZero(custom)) {
					map3.put("children", custom);
				}
			}
		}
		return comp;

	}

	/**
	 * 菜单业务层
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param componentId
	 * @param cusid 
	 * @return
	 */
	public List<Map<String, Object>> getCustom(int componentId, int cusid) {
		List<Map<String, Object>> custom = new ArrayList<>();
		List<Map<String, Object>> cusParam=customMapper.getCustomAndAnalyzeCount(componentId);
		cusParam.stream().forEach(x -> {
			Map<String, Object> cusMap = new HashMap<>();
			cusMap.put("id", evUtil.getMapIntegerValue(x, "id"));
			cusMap.put("parentId", cusid);
			cusMap.put("title", evUtil.getMapStrValue(x, "name"));
			cusMap.put("check", false);
			cusMap.put("expand", false);
			cusMap.put("type", "custom");
			if(evUtil.getMapIntValue(x, "count")>0) {
				cusMap.put("haveAnalyzeDimension", true);
			}else {
				cusMap.put("haveAnalyzeDimension", false);
			}
			custom.add(cusMap);
		});
		if (!evUtil.listIsNullOrZero(custom)) {
			for (Map<String, Object> map4 : custom) {
				int customId = evUtil.getMapIntValue(map4, "id");
				List<Map<String, Object>> indicator=analyzeIndicatorMapper.selectIndicator(customId);
				//根据indicatorId排序
				Collections.sort(indicator, new Comparator<Map<String, Object>>() {
		            public int compare(Map<String, Object> o1, Map<String, Object> o2) {
		                Integer name1 = Integer.valueOf(o1.get("indicatorId").toString()) ;//name1是从你list里面拿出来的一个 
		                Integer name2 = Integer.valueOf(o2.get("indicatorId").toString()) ; //name2是从你list里面拿出来的第二个name
		                return name1.compareTo(name2);
		            }
		        });
				for (Map<String, Object> map : indicator) {
					map.put("label", map.get("name"));
					map.remove("name");
					map.put("value", map.get("indicatorId"));
					map.remove("indicatorId");
				}
				map4.put("indicators", indicator);
				// 维度层
				List<Map<String, Object>> dimension = getDimension(customId,indicator);
				if (!evUtil.listIsNullOrZero(dimension)) {
					map4.put("children", dimension);
				}
			}
		}
		return custom;
	}

	/**
	 * 维度层级
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param customId
	 * @param indicator 
	 * @return
	 */
	private List<Map<String, Object>> getDimension(int customId, List<Map<String, Object>> indicator) {
		List<Map<String, Object>> dimension = new ArrayList<>();
		List<StatisticsDimension> dimensionParam = statisticsDimensionMapper.selectStatisticsDimension(customId);
		for (StatisticsDimension statisticsDimension : dimensionParam) {
			/*if(!statisticsDimension.getType().equals(0)) {
				String ename=statisticsDimension.getEname()+"_"+statisticsDimension.getType();
				statisticsDimension.setEname(ename);
			}*/
			if(statisticsDimension.getType().equals(1)) {
				String ename="province";
				statisticsDimension.setEname(ename);
			}else if(statisticsDimension.getType().equals(2)) {
				String ename="city";
				statisticsDimension.setEname(ename);
			}
			Map<String, Object> dimensionMap = new HashMap<>();
			dimensionMap.put("id", statisticsDimension.getId());
			dimensionMap.put("title", statisticsDimension.getName());
			dimensionMap.put("ename", statisticsDimension.getEname());
			dimensionMap.put("check", false);
			dimensionMap.put("checkall", false);
			dimensionMap.put("type", "dimension");
			dimensionMap.put("expand", false);
			dimensionMap.put("useDictionary", statisticsDimension.getUseDictionary());
			dimension.add(dimensionMap);
		}
		if (!evUtil.listIsNullOrZero(dimension)) {
			for (Map<String, Object> map4 : dimension) {
				map4.put("parentId", customId);
				int dimensionId = evUtil.getMapIntValue(map4, "id");
				// 维度值层
				List<Map<String, Object>> dimensionValue = getDimensionValue(dimensionId,indicator);
				if (!evUtil.listIsNullOrZero(dimensionValue)) {
					map4.put("children", dimensionValue);
				}
			}
		}
		return dimension;
	}

	/**
	 * 获取维度值
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param dimensionId
	 * @param indicator 
	 * @return
	 */
	private List<Map<String, Object>> getDimensionValue(int dimensionId, List<Map<String, Object>> indicator) {
		List<Map<String, Object>> dimension = new ArrayList<>();
		EntityWrapper<DimensionValue> wrapper = new EntityWrapper<>();
		wrapper.where("statisticsDimensionId={0}", dimensionId).orderBy("value");
		List<DimensionValue> dimensionParam = dimensionValueMapper.selectList(wrapper);
		dimensionParam.stream().forEach(x -> {
			Map<String, Object> cusMap = new HashMap<>();
			cusMap.put("id", Integer.parseInt(x.getId().toString()));
			cusMap.put("parentId", dimensionId);
			cusMap.put("title", x.getName());
			cusMap.put("dataValue", x.getValue());
			cusMap.put("type", "dimensionValue");
			cusMap.put("check", false);
			cusMap.put("expand", false);
			cusMap.put("indicators", indicator);
			dimension.add(cusMap);
		});
		return dimension;
	}

	/**
	 * 菜单ip层
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param componentId
	 * @param ipid 
	 * @param statisticalMode 
	 * @return
	 */
	public List<Map<String, Object>> getIp(int componentId, int ipid, int statisticalMode) {
		List<Map<String, Object>> ip = new ArrayList<>();
		List<Map<String, Object>> ipParam = componentMapper.selectIpAndportByComponentId(componentId);
		if (!evUtil.listIsNullOrZero(ipParam)) {
			ipParam.stream().forEach(x -> {
				Map<String, Object> ipMap = new HashMap<>();
				ipMap.put("id", evUtil.getMapIntValue(x, "id"));
				ipMap.put("parentId", ipid);
				ipMap.put("title", evUtil.getMapStrValue(x, "ip"));
				ipMap.put("check", false);
				ipMap.put("key", 1);
				ipMap.put("expand", false);
				ipMap.put("type", "ip");
				if(statisticalMode!=2) {
				// port层
					List<Map<String, Object>> port = getPort(x);
					ipMap.put("children", port);
				}
				ip.add(ipMap);
			});
		}
		return ip;
	}

	/**
	 *
	 * @author cgc
	 * @date 2018年9月19日
	 * @param x
	 * @author cgc
	 * @date 2018年9月19日
	 * @return
	 */
	public List<Map<String, Object>> getPort(Map<String, Object> x) {
		List<Map<String, Object>> port = new ArrayList<>();
		String portId[] = evUtil.getMapStrValue(x, "portId").split(",");
		String title[] = evUtil.getMapStrValue(x, "port").split(",");
		Map<String, Object> portMap = new HashMap<>();
		portMap.put("parentId", evUtil.getMapIntValue(x, "id"));
		int portid=createMenuId()+sequence_add;
		sequence_add++;
		portMap.put("id", portid);
		portMap.put("title", "全部");
		portMap.put("check", false);
		portMap.put("checkall", false);
		portMap.put("type", "portList");
		portMap.put("indeterminate", false);
		portMap.put("expand", false);
		port.add(portMap);
		for (int i = 0; i < portId.length; i++) {
			Map<String, Object> portMap2 = new HashMap<>();
			portMap2.put("title", title[i]);
			portMap2.put("parentId", evUtil.getMapIntValue(x, "id"));
			portMap2.put("id", Integer.parseInt(portId[i]));
			portMap2.put("check", false);
			portMap2.put("key", 2);
			portMap2.put("expand", false);
			portMap2.put("type", "port");
			port.add(portMap2);
		}
		return port;
	}

	@Override
	public List<Map<String, Object>> selectComponentTree(int componentId) {

		// bpm_component
		Component component = componentMapper.selectById(componentId);
		//组件数据统计模式1 组件 2 ip 3端口
		int statisticalMode = component.getStatisticalMode();

		List<Integer> componentIds = new ArrayList<>();
		componentIds.add(componentId);

		// bpm_server_ip
		List<ServerIp> serverIps = new ArrayList<>();
		if(statisticalMode == 2 || statisticalMode == 3){
			serverIps = componentMapper.findServerIpsByComponentIds(componentIds);
		}
		// bpm_server port
		List<Server> servers = new ArrayList<>();
		if(statisticalMode == 3){
			servers = componentMapper.findServerByComponentIds(componentIds);
		}

		// convert to map(key:serverGroupId, value:List<Id>)
		Map<Integer, List<Integer>> portMap = servers.stream().collect(Collectors.groupingBy(Server::getServerGroupId,
				Collectors.mapping(Server::getId, Collectors.toList())));

		EntityWrapper<Custom> customEntityWrapper = new EntityWrapper<>();
		customEntityWrapper.where("componentId={0} and active=1", componentId,1);
		List<Custom> customList = customMapper.selectList(customEntityWrapper);

		Map<Integer, List<StatisticsDimension>> staticMap = new HashMap<>();
		if(!evUtil.listIsNullOrZero(customList)){
			List<Integer> customIds  = customList.stream().map(Custom::getId).collect(Collectors.toList());
			List<StatisticsDimension> statisticsDimensions = statisticsDimensionMapper.selectStatisticsDimensionByCustomIds(customIds);
			// key:customId
			if(!evUtil.listIsNullOrZero(statisticsDimensions)){
				staticMap = statisticsDimensions.stream().collect(Collectors.groupingBy(StatisticsDimension::getCustomId, Collectors.toList()));
			}
		}

		/**
		 * structure tree
		 *
		 * componentId->([全部ip]ip)-> ->([业务] 业务...)--> 维度...
		 */
		// first level
		Map<String, Object> rootNode = new HashMap<>();
		rootNode.put("title",component.getName());
		rootNode.put("uuids", Arrays.asList(component.getId()));
		rootNode.put("type",1);

		// second level
		List<Map<String, Object>> secondList = new ArrayList<>();
		if (!evUtil.listIsNullOrZero(serverIps)) {
			Map<String, Object> ipRootNode = new HashMap<>();
			ipRootNode.put("title","全部IP");
			ipRootNode.put("type",2);
			ipRootNode.put("uuids", Arrays.asList(component.getId()));
			secondList.add(ipRootNode);
		}

		if(!evUtil.listIsNullOrZero(customList)){
			Map<String,Object> customNode = new HashMap<>();
			customNode.put("title","事件");
			customNode.put("type",4);
			customNode.put("uuids", Arrays.asList(component.getId()));
			secondList.add(customNode);
		}
		rootNode.put("children", secondList);

		// third level
		// ip
		List<Map<String, Object>> ipThirdLevel = new ArrayList<>();
		if( statisticalMode == 3){
			serverIps.forEach(x -> {
				Map<String, Object> ipNode = new HashMap<>();
				ipNode.put("title", x.getIp());
				ipNode.put("type",3);
				ipNode.put("uuids", Arrays.asList(x.getId()));
				ipThirdLevel.add(ipNode);
			});
		}

		if (!evUtil.listIsNullOrZero(ipThirdLevel)) {
			List<Map<String, Object>> secondNodeList = (List<Map<String, Object>>) rootNode.get("children");
			secondNodeList.get(0).put("children", ipThirdLevel);
		}

		// custom 只展示分析维度
		List<Map<String, Object>> customNodeList = new ArrayList<>();
		for(Custom custom : customList){
			Map<String, Object> customNodeMap = new HashMap<>();
			customNodeMap.put("title", custom.getName());
			customNodeMap.put("uuids", Arrays.asList(custom.getId()));

			List<StatisticsDimension> statisticsDimensions = staticMap.get(custom.getId());
			if(!evUtil.listIsNullOrZero(statisticsDimensions)){
				List<Map<String, Object>> dimensionNodeList = new ArrayList<>();
				Integer staticType;
				for(StatisticsDimension statisticsDimension : statisticsDimensions){
					Map<String, Object> map = new HashMap<>();
					map.put("title",statisticsDimension.getName());
					map.put("type",5);
//					map.put("ename", statisticsDimension.getType() == 0 ? statisticsDimension.getEname():statisticsDimension.getEname()
//						+"_"+ statisticsDimension.getType());
					staticType =  statisticsDimension.getType();
					if(staticType == 1){
						map.put("ename", "province");
					}else if(staticType == 2){
						map.put("ename", "city");
					}else{
						map.put("ename", statisticsDimension.getEname());
					}
					map.put("uuids", Arrays.asList(statisticsDimension.getId()));
					dimensionNodeList.add(map);
				}
				customNodeMap.put("children", dimensionNodeList);
			}
			customNodeList.add(customNodeMap);
		}

		if(!evUtil.listIsNullOrZero(customNodeList)){
			List<Map<String, Object>> secondNodeList = (List<Map<String, Object>>)rootNode.get("children");
			for(Map<String, Object> map : secondNodeList){
				if(evUtil.getMapIntValue(map,"type") == 4){
					//自定义业务根节点
					map.put("children",customNodeList);
					break;
				}
			}
		}
		return Arrays.asList(rootNode);
	}

	@Override
	public Set<Long> selectAllUuidByUserId(int userId) {
		List<ApplicationParam> apps = applicationMapper.selectApplicationByUserId(userId, 1);
		StringBuilder sb = new StringBuilder("");
		apps.stream().forEach(x->{
			sb.append("applicationid:").append(x.getId()).append("|");
		});
		Map<String,Object> uuidMap = new HashMap<>();
		Set<Long> uuidSet = new HashSet<>();
		//use REGEXP to match some applicationIds
		if(sb.toString().length() >= 0){
			String regStr = sb.substring(0,sb.length()-1).toString();
			List<Uuid> bpmUuids = uuidMapper.findUuidByVaule(regStr);
			// to make map(key:value[applicationid:1...],value:id)
			uuidSet.addAll(bpmUuids.stream().map(Uuid::getId).collect(Collectors.toSet()));
			List<Ctuuid> ctuuids = ctuuidMapper.findUuidByVaule(regStr);
			uuidSet.addAll(ctuuids.stream().map(Ctuuid::getId).collect(Collectors.toSet()));
 		}
		return uuidSet;
	}

	@Override
	public List<Map<String, Object>> getAlarmGrouopComponentTree(int userId) {
		List<Map<String, Object>> menu = new ArrayList<>();
		List<ApplicationParam> apps = applicationMapper.selectApplicationByUserId(userId, 1);
		if(!evUtil.listIsNullOrZero(apps)) {
			for (ApplicationParam applicationParam : apps) {
				int id=applicationParam.getId();
				List<ComponentParam> componentParam=componentMapper.selectComponentParmListByAppId(id);
				if(!evUtil.listIsNullOrZero(componentParam)) {
					Map<String, Object> app=new HashMap<>(4);
					app.put("id", id);
					app.put("title", applicationParam.getName());
					app.put("type", 1);
					List<Map<String, Object>> children = new ArrayList<>();
					for (ComponentParam cparam : componentParam) {
						Map<String, Object> compont=new HashMap<>(3);
						compont.put("id", cparam.getId());
						compont.put("title", cparam.getName());
						compont.put("type", 2);
						children.add(compont);
					}
					app.put("children", children);
					menu.add(app);
				}
				
			}
		}
		return menu;
	}

	@Override
	public List<Map<String, Object>> getAlarmGrouopEventTree(int userId) {
		List<Map<String, Object>> menu = getAlarmGrouopComponentTree(userId);
		if (!evUtil.listIsNullOrZero(menu)) {
			for (int i = 0; i < menu.size(); i++) {
				List<Map<String, Object>> children = (List<Map<String, Object>>) menu.get(i).get("children");
				List<Map<String, Object>> removeList=new ArrayList<>();
				for (int j = 0; j < children.size(); j++) {
					Map<String, Object> columnMap=new HashMap<>(1);
					columnMap.put("componentId", children.get(j).get("id"));
					List<Custom> customList=customMapper.selectByMap(columnMap);
					if(evUtil.listIsNullOrZero(customList)) {
						removeList.add(children.get(j));
						//children.remove(j);
					}else {
						List<Map<String, Object>> childrenList =new ArrayList<>() ;
						for (Custom custom : customList) {
							Map<String, Object> custommap=new HashMap<>(3);
							custommap.put("id", custom.getId());
							custommap.put("title", custom.getName());
							custommap.put("type", 3);
							childrenList.add(custommap);
						}
						children.get(j).put("children", childrenList);
					}
					
				}
				children.removeAll(removeList);
			}
		}
		//去除没有组件的应用
		List<Map<String, Object>> remove = new ArrayList<>();
		for (Map<String, Object> map : menu) {
			List<Map<String, Object>> children = (List<Map<String, Object>>) map.get("children");
			if(evUtil.listIsNullOrZero(children)) {
				remove.add(map);
			}
		}
		menu.removeAll(remove);
		return menu;
	}

	@Override
	public Map<Integer, String> namesMap(List<Integer> uuids) {
		//bpm_application bpm_component
		Map<Integer, String> result = new HashMap<>();
		if(!evUtil.collectionIsNullOrZero(uuids)){
			applicationMapper.selectBatchIds(uuids).forEach(x-> {
				result.put(x.getId(), x.getName());
				uuids.remove(x.getId());
			});
		}
		if(!evUtil.collectionIsNullOrZero(uuids)){
			applicationMapper.selectMonitorNameChain(uuids).forEach(x->{
				result.put(x.getMonitorId(), x.getApplicationName()+"/"+x.getMonitorName());
				uuids.remove(x.getMonitorId());
			});
		}

		if(!evUtil.collectionIsNullOrZero(uuids)){
			applicationMapper.selectComponentNameChain(uuids).forEach(x->{
				result.put(x.getComponentId(), x.getApplicationName()+"/"+x.getMonitorName()+"/"+x.getComponentName());
				uuids.remove(x.getMonitorId());
			});
		}
		if(!evUtil.collectionIsNullOrZero(uuids)){
			applicationMapper.selectIpNameChain(uuids).forEach(x->{
				result.put(x.getIpId(), x.getApplicationName()+"/"+x.getMonitorName()+"/"+x.getComponentName()+"/"+x.getIp());
				uuids.remove(x.getMonitorId());
			});
		}
		if(!evUtil.collectionIsNullOrZero(uuids)){
			applicationMapper.selectPortNameChain(uuids).forEach(x->{
				result.put(x.getPortId(), x.getApplicationName()+"/"+x.getMonitorName()+"/"+x.getComponentName()+"/"+x.getIp()+"/"+x.getPort());
				uuids.remove(x.getMonitorId());
			});
		}
		return result;
	}

	@Override
	public Map<Integer, String> customNameMap(List<Integer> uuids) {
		Map<Integer, String> result = new HashMap<>();
		if(!evUtil.collectionIsNullOrZero(uuids)){
			applicationMapper.selectCustomNameChain(uuids).forEach(x->{
				result.put(x.getCustomId(), x.getApplicationName()+"/"+x.getMonitorName()+"/"+x.getComponentName()+"/"+x.getCustomName());
			});
		}
		return result;
	}


	@Override
	public List<Integer> findAppIdByAlarmInfo(int userId, long start, long end, int limit) {
		List<ApplicationParam> applicationParams = applicationMapper.selectApplicationByUserId(userId, 1);
		List<Integer> ids = applicationParams.stream().map(ApplicationParam::getId).collect(Collectors.toList());
		if(ids.size() < limit){
			throw new RuntimeException("参数limit错误！");
		}
		Map<String, Object> param = new HashMap<>();
		param.put("ids", ids);
		param.put("start", start);
		param.put("end", end);
		param.put("limit", limit);
		List<Integer> appIds = applicationAlarmMapper.selectAppIds(param);
		int size = appIds.size();
		if(appIds.size() < limit) {
			ids.removeAll(appIds);
			appIds.addAll(ids.subList(0, limit - size));
		}
		return appIds;
	}
}
