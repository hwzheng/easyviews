package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ProtocolMessage;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.Map;

/**
 * <p>
 * 协议报文表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-08-21
 */
public interface ProtocolMessageMapper extends BaseMapper<ProtocolMessage> {
    void saveProtocolMessage(ProtocolMessage protocolMessage);
}
