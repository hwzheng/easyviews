package com.greattimes.ev.bpm.mapper;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.bpm.config.param.req.AlarmParam;
import com.greattimes.ev.bpm.entity.AlarmConditionTemplateParam;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.bpm.entity.AlarmRule;

/**
 * <p>
 * 告警规则表 Mapper 接口
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
public interface AlarmRuleMapper extends BaseMapper<AlarmRule> {

	/**
	 * 告警查询
	 * @param applicationId
	 * @param type
	 * @return
	 */
	List<Map<String, Object>> selectAlarmRule(@Param("applicationId") int applicationId, @Param("type") int type);

	AlarmParam selectApplicationAlarmDetail(@Param("alarmId")int alarmId);

	AlarmParam selectComponentAlarmDetail(@Param("alarmId")int alarmId);

	AlarmParam selectIpAlarmDetail(@Param("alarmId")int alarmId);

	AlarmParam selectIpAndPortAlarmDetail(@Param("alarmId")int alarmId);

	AlarmParam selectSingleDimensionAlarmDetail(@Param("alarmId")int alarmId);

	AlarmParam selectMultiAlarmCondition(@Param("alarmId")int alarmId);

	AlarmParam selectMultiAlarmFilters(@Param("alarmId")int alarmId);

	void updateBatchActive(@Param("appId")int appId, @Param("active")int active, @Param("type") int type);

	void updateBatchActiveCustom(@Param("ids")List<Integer> ids, @Param("active")int active, @Param("type") int type);

	void updateBatchActiveByIds(@Param("ids") List<Integer> ids, @Param("active") int active);

	AlarmParam selectMonitorAlarmDetail(@Param("alarmId")int alarmId);

	List<Map<String, Object>> selectMonitorAlarmRule(@Param("applicationId") int applicationId, @Param("type") int type);

	List<Map<String, Object>> selectDataSourceAlarmRule(@Param("applicationId") int applicationId, @Param("type") int type);

	AlarmParam selectDataSourceAlarmDetail(@Param("alarmId")int alarmId);

	List<Map<String, Object>>  selectDataSourceIndicatorByRuleId(@Param("applicationId") int applicationId, @Param("type") int type);

	List<Map<String, Object>> selectCustomAlarmRule(@Param("customId") int customId, @Param("type") int type);

	List<Map<String, Object>> selectAlarmConditionByMultiDimensionId(List<Integer> dimensionIds);

	List<Map<String, Object>> selectAlarmConditionByFilterDimensionId(List<Integer> dimensionIds);

	List<AlarmConditionTemplateParam> selectTemplateParamByRuleId(int ruleId);

	List<AlarmRule> selectAlarmRuleByComponentIds(@Param("list") List<Integer> list);
}
