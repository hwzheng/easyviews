package com.greattimes.ev.bpm.mapper;

import com.greattimes.ev.bpm.entity.ApplicationCenter;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 应用中心表 Mapper 接口
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface ApplicationCenterMapper extends BaseMapper<ApplicationCenter> {

    int batchInsertAppCenter(List<ApplicationCenter> list);

}
