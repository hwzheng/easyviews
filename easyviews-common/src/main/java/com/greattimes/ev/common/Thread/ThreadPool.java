package com.greattimes.ev.common.Thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ThreadPool {
	private  static ExecutorService executorService;
	
	static{
		if(executorService==null){

			executorService=Executors.newCachedThreadPool();
		}
	}
	public static ExecutorService getExecutorService() {
		return executorService;
	}
}
