package com.greattimes.ev.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.greattimes.ev.common.constants.OperaterType;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperateLog {
	/***
	 *操作信息描述
	 * @return
	 */
	String desc() default "";
	
	/***
	 * 操作日志类型 
	 * @return
	 */
	OperaterType type() default OperaterType.SELECT;
	
	/***
	 * 是否保存到数据库 默认false
	 * @return
	 */
	boolean isInsertDB() default false;
}
