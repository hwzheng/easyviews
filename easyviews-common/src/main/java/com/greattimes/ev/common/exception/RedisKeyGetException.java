package com.greattimes.ev.common.exception;

/**
  * @Description: redis key 获取异常
  * @Author: NJ
  * @CreateDate:2019-05-06 15:03
  * @Version:1.0
*/
public class RedisKeyGetException extends RuntimeException{

    public RedisKeyGetException(String message) {
        super(message);
    }

}
