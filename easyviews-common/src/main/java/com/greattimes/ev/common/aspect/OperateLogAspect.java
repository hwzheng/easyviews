package com.greattimes.ev.common.aspect;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.alibaba.fastjson.JSON;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.model.User;
import com.greattimes.ev.common.utils.StringUtils;

@Aspect
@Component
public class OperateLogAspect {
	Logger log=LoggerFactory.getLogger("operate");
	public OperateLogAspect() {
		log.info("operation log is open...");
	}
	
	@AfterReturning("execution(* com.greattimes.ev..*.*(..)) && @annotation(ol)")  
    public void afterReturn(JoinPoint joinPoint ,OperateLog ol) throws Throwable {          
		records( joinPoint, ol,null);
    } 
	
	@AfterThrowing(pointcut="execution(* com.greattimes.ev..*.*(..)) && @annotation(ol)",throwing="e")  
    public void afterThrow(JoinPoint joinPoint ,OperateLog ol,Exception e) throws Throwable {          
		records( joinPoint, ol,e);
    } 
	
	/**
	 * 日志记录方式 
	 * 数据库 和 日志文件
	 * 目前只打日志 
	 * **/
	private void records(JoinPoint joinPoint,OperateLog ol,Exception e){
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();  
		HttpSession session = request.getSession(); 
		User user = JSON.parseObject(session.getAttribute("member").toString(), User.class);  
		String ip = request.getRemoteAddr();
		log.info("----------operate info start!--------------");
		log.info("user:【{}{}】",user.getId(),user.getName());
		log.info("IP:【{}】",ip);
		log.info("action:{}",ol.desc());
		log.info("actionType:{}",ol.type());
		if(e!=null){
			log.error("操作异常：",e.toString());
			log.error("operate error!");
		}else{
			log.info("operate success!");
		}
		log.info("----------operate info end!--------------");
	}
}
