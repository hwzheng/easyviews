package com.greattimes.ev.common.utils;  
  

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;


/**
 * 日期时间工具类 
 */  
public class DateUtils {

    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter FORMATTER1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    private static final DateTimeFormatter FORMATTER2 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH");
    private static final DateTimeFormatter FORMATTER3 = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final DateTimeFormatter FORMATTER4 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

    private static final int INTERVAL_FIVE_SECONDS = 5;
    private static int INTERVAL_MINUTE = 60;
    private static final int INTERVAL_FIVE_MINUTE = 5 * 60;
    private static final int INTERVAL_HOUR = 60 * 60;
    private static final int INTERVAL_DAY = 24 * 60 * 60;


    /**
     * 日期格式 yyyy-MM-dd
     * @return
     */
    public static String currentDate() {
        return LocalDate.now().format(FORMATTER);
    }

    /** 
     * 获得当前日期时间 
     * <p> 
     * 日期时间格式yyyy-MM-dd HH:mm:ss 
     *  
     * @return 
     */  
    public static String currentDatetime() {
        return LocalDateTime.now().format(FORMATTER1);
    }

    /** 
     * 获得当前日期时间 
     * <p> 
     * @return 
     */  
    public static String format(Date date,String format) {
    	if(null==format||"".equals(format)){
    		format="yyyy-MM-dd HH:mm:ss";
    	}
    	SimpleDateFormat dateFormat= new SimpleDateFormat(format);
        return dateFormat.format(date);  
    }


    /**
     * 时间戳转换成日期格式字符串
     * @param seconds 精确到秒的字符串
     * @param format
     * @return
     */
    public static String timeStamp2Date(String seconds,String format) {  
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){  
            return "";  
        }  
        if(format == null || format.isEmpty()) format = "yyyy-MM-dd HH:mm:ss";  
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        Instant instant = Instant.ofEpochMilli(Long.valueOf(seconds));
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime l=LocalDateTime.ofInstant(instant, zone);
        return l.format(formatter);
    }  

    /**
     * 日期格式字符串转换成时间戳 (没有除1000)
     * @param date_str 字符串日期
     * @param format 如：yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String date2TimeStamp(String date_str,String format){  
        try {  
            //SimpleDateFormat sdf = new SimpleDateFormat(format);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
            LocalDateTime l=LocalDateTime.parse(date_str,formatter);
            ZoneId zone = ZoneId.systemDefault();
            Instant instant = l.atZone(zone).toInstant();
            return String.valueOf(instant.toEpochMilli());
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return "";  
    }  


    /**
     * 根据开始和结束时间获取天数日期集合
     * @author NJ
     * @date 2018/11/29 14:24
     * @param
     * @return java.util.List<java.lang.String>
     */
    public static List<String> getDateStrByStartAndEnd(Long start, Long end){
        if(start == null || end == null){
            throw new IllegalArgumentException("开始时间和结束时间不能为空！");
        }
        if(start > end){
            throw new IllegalArgumentException("开始时间不能大于结束时间！");
        }
        List<String> tableStr =  new ArrayList<>();
        //String endStr = dateFormat.format(end);
        LocalDateTime  endDate = Instant.ofEpochMilli(end).atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime  startDate = Instant.ofEpochMilli(start).atZone(ZoneId.systemDefault()).toLocalDateTime();
        String endStr = FORMATTER.format(endDate);
        //Calendar startCal = Calendar.getInstance();
        //Date startDate = new Date(start);
        //startCal.setTime(startDate);
        while(true){
            String startStr = FORMATTER.format(startDate);
            tableStr.add(startStr);
            if(!startStr.equals(endStr)){
            	startDate=startDate.plusDays(1);
            }else{
                break;
            }
        }
        return tableStr;
    }

    /**
     * 根据开始和结束时间拼接日期sql  format(yyyy-mm-dd)
     * eg:  date >= '2019-03-02' and date <= '2019-03-03'
     * @author NJ
     * @date 2019/3/5 9:32
     * @param start
     * @param end
     * @return java.lang.String
     */
    public static String generateDateSqlStr(Long start, Long end, String column){
        if(start == null || end == null){
            throw new IllegalArgumentException("开始时间和结束时间不能为空！");
        }
        if(start > end){
            throw new IllegalArgumentException("开始时间不能大于结束时间！");
        }
        StringBuilder sb;
        LocalDateTime  localDateStart = Instant.ofEpochMilli(start).atZone(ZoneId.systemDefault()).toLocalDateTime();
        LocalDateTime localDateEnd = Instant.ofEpochMilli(end).atZone(ZoneId.systemDefault()).toLocalDateTime();
        String startStr = FORMATTER.format(localDateStart);
        String endStr = FORMATTER.format(localDateEnd);
        if(startStr.equals(endStr)){
            sb = new StringBuilder(17 + column.length());
            sb.append(" ") .append(column).append(" = '").append(endStr).append("' ");
        }else{
            sb = new StringBuilder(40 + column.length()*2);
            sb.append(" ").append(column).append(" >= '").append(startStr).append("' AND ")
                .append(column).append(" <= '").append(endStr).append("' ");
        }

        return sb.toString();
    }

    @Deprecated
	public static Long longToFiveMinute(Long time) { 
        Date date=new Date(time);
        Calendar c1 = Calendar.getInstance();
    	c1.setTime(date);
    	int min=c1.get(Calendar.MINUTE);
    	min=((min-1)/5)*5;
    	c1.set(Calendar.MINUTE, min);
    	c1.set(Calendar.SECOND, 0);
        return c1.getTime().getTime();
    }
	
	/**格式化到分钟
	 * @author CGC
	 * @param time
	 * @param num eg：格式到5分钟 传5
	 * @return
	 */
	public static Long longToMinute(Long time,int num) { 
        Date date=new Date(time);
        Calendar c1 = Calendar.getInstance();
    	c1.setTime(date);
    	int min=c1.get(Calendar.MINUTE);
    	if(num!=1) {
    		min=(min/num)*num;
    	}
    	c1.set(Calendar.MINUTE, min);
    	c1.set(Calendar.SECOND, 0);
        return c1.getTime().getTime();
    }

	/**格式化到小时
	 * @author CGC
	 * @param time
	 * @param num 
	 * @return
	 */
	public static Long longToHour(Long time,int num) { 
        Date date=new Date(time);
        Calendar c1 = Calendar.getInstance();
    	c1.setTime(date);
    	int hour=c1.get(Calendar.HOUR);
    	if(num!=1) {
    		hour=(hour/num)*num;
    	}
    	c1.set(Calendar.HOUR, hour);
    	c1.set(Calendar.MINUTE, 0);
    	c1.set(Calendar.SECOND, 0);
        return c1.getTime().getTime();
    }

	/** 获取日期
	 * @author CGC
	 * @param datelist 格式[{stat:11111111,end:222222},...]
	 * @return
	 */
	public static List<String> getDateStr(List<Map<String, Object>> datelist) {
		Set<String> s=new java.util.HashSet<>();
		for (Map<String, Object> map : datelist) {
			Long start=evUtil.getMapLong(map, "start");
			Long end=evUtil.getMapLong(map, "end");
			List<String> l=getDateStrByStartAndEnd(start, end);
			s.addAll(l);
		}
		List<String> result = new ArrayList<String>();
		result.addAll(s);
		return result;
	}

    /**
     * 格式化为默认的时间戳
     * yyyy-MM-dd
     * 2019-05-30 11:03:15
     * @param time
     * @return
     */
	public static String longToDefaultDateStr(long time){
	    return  FORMATTER.format(Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDateTime());
    }
    /**
     * yyyy-MM-dd HH:mm:ss
     * @param time
     * @return
     */
    public static String longToDefaultDateTimeStr(long time){
        return  FORMATTER1.format(Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDateTime());
    }
    
    /**
     * yyyy-MM-dd HH:mm:ss.SSS
     * @param time
     * @return
     */
    public static String longToDefaultDateTimeMicrosecond(long time){
    	return  FORMATTER4.format(LocalDateTime.ofInstant(Instant.ofEpochMilli(Math.floorDiv(time, 1000)), ZoneId.systemDefault()));
    }

    /** 获取日期
     * @author CGC
     * @param datelist 格式"timePart": [{"time": [1558072050000,1558075650000]}...]
     * @return
     */
    public static List<String> getDateStrbyTimePart(List<Map<String, Object>> datelist) {
        Set<String> s=new java.util.HashSet<>();
        for (Map<String, Object> map : datelist) {
            List<Long> time=(List<Long>) map.get("time");
            if (!evUtil.listIsNullOrZero(time)) {
                Long start = time.get(0);
                Long end = time.get(1);
                List<String> l = getDateStrByStartAndEnd(start, end);
                s.addAll(l);
            }
        }
        List<String> result = new ArrayList<String>();
        result.addAll(s);
        return result;
    }


    /**
     * HH:mm:ss
     * @param time
     * @return
     */
    public static String longToHHmmssStr(long time){
        return  FORMATTER3.format(Instant.ofEpochMilli(time).atZone(ZoneId.systemDefault()).toLocalDateTime());
    }



    /**
     * 1分钟转化
     * @author NJ
     * @date 2019/6/18 16:57
     * @param time
     * @return java.lang.Long
     */
    public static Long longToOneMinuteTime(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        dateTime = dateTime.withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    /**
     * 5s转化
     * @author cgc
     * @date 2019/6/18 16:57
     * @param time
     * @return java.lang.Long
     */
    public static Long longToFiveSecondTime(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        dateTime = dateTime.withSecond(dateTime.getSecond()/5*5);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    
    /**
     * 5s转化（向后）
     * @author cgc
     * @date 2019/6/18 16:57
     * @param time
     * @return java.lang.Long
     */
    public static Long longToFiveSecondTimeBehind(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        int second=dateTime.getSecond();
        if(second%5!=0) {
        	dateTime = Instant.ofEpochMilli(time+5000).
                    atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        dateTime = dateTime.withSecond((dateTime.getSecond())/5*5);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    /**
     * 5分钟转化
     * @author NJ
     * @date 2019/6/18 16:57
     * @param time
     * @return java.lang.Long
     */
    public static Long longToFiveMinuteTime(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        dateTime = dateTime.withMinute(dateTime.getMinute()/5*5).withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    
    /**
     * 5分钟转化（向后）
     * @author cgc
     * @date 2019/6/18 16:57
     * @param time
     * @return java.lang.Long
     */
    public static Long longToFiveMinuteTimeBehind(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        int minute=dateTime.getMinute();
        int second=dateTime.getSecond();
        if(minute%5!=0||second!=0) {
        	dateTime = Instant.ofEpochMilli(time+5*60*1000).
                    atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        dateTime = dateTime.withMinute((dateTime.getMinute())/5*5).withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }


    /**
     * 将时间戳转换为小时级 每一小时为当前小时00分到59分
     * @author NJ
     * @date 2019/6/18 18:10
     * @param time
     * @return java.lang.Long
     */
    public static Long longToHourTime(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        dateTime = dateTime.withMinute(0).withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    
    /**
     * 将时间戳转换为小时级 (往后推)
     * @author cgc
     * @date 2019/6/18 18:10
     * @param time
     * @return java.lang.Long
     */
    public static Long longToHourTimeBehind(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        int minute=dateTime.getMinute();
        int second=dateTime.getSecond();
        if(!(minute==0&&second==0)) {
        	dateTime = Instant.ofEpochMilli(time+(60*60*1000)).
                    atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        dateTime = dateTime.withMinute(0).withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**
     * 将时间戳转换为天级数据
     * @author NJ
     * @date 2019/6/18 18:10
     * @param time
     * @return java.lang.Long
     */
    public static Long longToDayTime(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        dateTime = dateTime.withHour(0).withMinute(0).withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
    /**
     * 将时间戳转换为天级数据(往后推)
     * @author cgc
     * @date 2019/6/18 18:10
     * @param time
     * @return java.lang.Long
     */
    public static Long longToDayTimeBehind(Long time) {
        LocalDateTime dateTime = Instant.ofEpochMilli(time).
                atZone(ZoneId.systemDefault()).toLocalDateTime();
        int hour=dateTime.getHour();
        int minute=dateTime.getMinute();
        int second=dateTime.getSecond();
        if(!(hour==0&&minute==0&&second==0)) {
        	dateTime = Instant.ofEpochMilli(time+(24*60*60*1000)).
                    atZone(ZoneId.systemDefault()).toLocalDateTime();
        }
        dateTime = dateTime.withHour(0).withMinute(0).withSecond(0);
        return dateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    /**将日期字符串转换成long
     * @author CGC
     * @param date eg:'2019-07-02'
     * @return
     */
    public static long getDateLong(String date) {
    	LocalDate timeLocal= LocalDate.parse(date, FORMATTER);
    	ZoneId zone = ZoneId.systemDefault();
        Instant instant = timeLocal.atStartOfDay().atZone(zone).toInstant();
        return instant.toEpochMilli();
    }
    /**将日期字符串转换成long
     * @author CGC
     * @param date eg:'2019-07-02'
     * @return
     */
    public static long getDateLong(Date date) {
    	Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        String dateStr=localDateTime.format(FORMATTER);
        return getDateLong(dateStr);
    }
    /**将日期字符串转换成String
     * @author CGC
     * @param date eg:'2019-07-02'
     * @return
     */
    public static String getDateStr(Date date) {
    	Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        String dateStr=localDateTime.format(FORMATTER);
        return dateStr;
    }
    /**将日期字符串转换成Date
     * @author CGC
     * @param dateStr eg:'2019-07-02'
     * @return
     */
    public static Date getDate(String dateStr) {
		LocalDate localDate=LocalDate.parse(dateStr,FORMATTER);
		ZoneId zone = ZoneId.systemDefault();
	    Instant instant = localDate.atStartOfDay().atZone(zone).toInstant();
	    Date date=Date.from(instant);
        return date;
    }
    /**
     * 处理分段时间(格式化)
     * @param timePart
     * @param interval 
     * @return
     */
	public static List<Map<String, Object>> generateTimeStr(List<Map<String, Object>> timePart, Integer interval) {
		List<Map<String, Object>> timeList = new ArrayList<>();
		for (Map<String, Object> timeMap : timePart) {
			Map<String, Object> newMap = new HashMap<>();
			List<Long> time = (List<Long>) timeMap.get("time");
			if (!evUtil.listIsNullOrZero(time)) {
				Long start = time.get(0);
				Long end = time.get(1);
				if (interval == INTERVAL_FIVE_SECONDS) {
					start = DateUtils.longToFiveSecondTimeBehind(start);
					end = DateUtils.longToFiveSecondTime(end);
				} else if (interval == INTERVAL_FIVE_MINUTE) {
					start = DateUtils.longToFiveMinuteTimeBehind(start);
					end = DateUtils.longToFiveMinuteTime(end);
				} else if (interval == INTERVAL_HOUR) {
					start = DateUtils.longToHourTimeBehind(start);
					end = DateUtils.longToHourTime(end);
				} else if (interval == INTERVAL_DAY) {
					start = DateUtils.longToDayTimeBehind(start);
					end = DateUtils.longToDayTime(end);
				}else if(interval == INTERVAL_MINUTE) {
					start = DateUtils.longToMinute(start,1);
					end = DateUtils.longToMinute(end,1);
				}
				List<Long> newTime = new ArrayList<>();
				newTime.add(start);
				newTime.add(end);
				newMap.put("time", newTime);
				timeList.add(newMap);
			}
		}
		return timeList;
	}
	/**
     * 获取前n天或后n天日期
     *
     * @param date 日期
     * @param n    
     * @return
     */
    public static Date getDate(Date date, int n) {
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, n);
        date = calendar.getTime();
        return date;
    }
    
    public static void main(String[] args) {
        //2019-06-18 15:56:55
        long time = 1573178859952185L;
        Date date=new Date(time);
        String str="2019-07-16";
        //System.out.println(longToFiveMinuteTime(time));
        //System.out.println(longToHourTime(time));
        System.out.println(longToDefaultDateTimeMicrosecond(time));

    }
}