package com.greattimes.ev.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description:节假日工具类
 * @Auther: CGC
 * @Date: 2019-10-10 19:50
 * @Version: 1.0.0
 */
public class HolidayUtil {
 
    /**
     * 获取前一天或后一天日期
     *
     * @param date 日期
     * @param n    判断参数
     * @return
     */
    public static Date getDate(Date date, int n) {
        if (n > 0) {    // 获取前一天
            date = getTomorrow(date);
        }
        if (n < 0) {    // 获取后一天
            date = getYesterday(date);
        }
        return date;
    }
 
    /**
     * 获取后一天的日期
     *
     * @param date
     * @return
     */
    public static Date getTomorrow(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, +1);
        date = calendar.getTime();
        return date;
    }
 
    /**
     * 获取前一天的日期
     *
     * @param date
     * @return
     */
    public static Date getYesterday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        date = calendar.getTime();
        return date;
    }
 
    /**
     * 判断是否是周末
     *
     * @param sdate
     * @return
     * @throws Exception
     */
    public static boolean isWeekend(Date sdate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(sdate);
        if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
            return true;
        } else {
            return false;
        }
 
    }
 

	/**
	 * 判断是否是工作日
	 * @author CGC
	 * @param endDay
	 * @param holidayMap
	 * @return
	 */
	public static boolean isWorkday(Date endDay, Map<String, Object> holidayMap){
		String date = DateUtils.getDateStr(endDay);
		Integer workday=evUtil.getMapIntegerValue(holidayMap, date);
		if(null!=workday) {
			if(0==workday) {
				return false;
			}
			else {
				return true;
			}
		}else {
			if(isWeekend(endDay)) {
				return false;
			}else {
				return true;
			}
		}
	}
}
