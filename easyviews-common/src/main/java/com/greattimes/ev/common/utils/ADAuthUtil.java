package com.greattimes.ev.common.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.*;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class ADAuthUtil {

	static Logger log = LoggerFactory.getLogger(ADAuthUtil.class);
	
	public static Map<String, String> CONFIG=new HashMap<String, String>();
	/**
	 * AD域IP，必须填写正确
	 */
	public static final String HOST="ad_host";
	/**
	 * 域名例:@noker.com
	 */
	public static final String DOMAIN="ad_domain";
	/**
	 * 端口，一般默认389
	 */
	public static final String PORT="ad_port";
	/**
	 * 搜索域节点，如：OU=SDB,DC=sdb,DC=local
	 */
	public static final String SEARCHBASE="ad_searchBase";

	/**
	 * 判断用户名密码是否在AD域中存在  xuyj以前方法
	 * @param username
	 * @param password
	 * @return
	 */
	@Deprecated
	public static boolean authenticate(String username, String password) {
		//固定写法
		String url = new String("ldap://" + CONFIG.get(HOST) + ":" + CONFIG.get(PORT));
        String user = username.indexOf(CONFIG.get(DOMAIN)) > 0 ? username : username
                + CONFIG.get(DOMAIN);
		//实例化一个Env
        Hashtable env = new Hashtable();
        DirContext ctx = null;
		//LDAP访问安全级别(none,simple,strong),一种模式，这么写就行
        env.put(Context.SECURITY_AUTHENTICATION, "simple");
        env.put(Context.SECURITY_PRINCIPAL, user);
        env.put(Context.SECURITY_CREDENTIALS, password);
		// LDAP工厂类
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL, url);
        try {
			// 初始化上下文
            ctx = new InitialDirContext(env);
            return true;
        } catch (AuthenticationException e) {
            e.printStackTrace();
            return false;
        } catch (javax.naming.CommunicationException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally{
            if(null!=ctx){
                try {
                    ctx.close();
                    ctx=null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
	
	/**
	 * 得到ad域用户的手机号
	 * @param userName  用户名
	 * @param password  密码
	 * @return 对应ad域用户的手机号，没有就返回null
	 */
	public static String getPhoneNumber(String userName, String password){
		String url = new String("ldap://" + CONFIG.get(HOST) + ":" + CONFIG.get(PORT));
		String user = userName.indexOf(CONFIG.get(DOMAIN)) > 0 ? userName : userName
	                + CONFIG.get(DOMAIN);
		log.info("AD域user:"+user);
		//实例化一个Env
		Hashtable env = new Hashtable();
		String telephonenumber=null;
		LdapContext ctx = null;
		//LDAP访问安全级别(none,simple,strong),一种模式，这么写就行
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user);
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, url);
		try {
			ctx = new InitialLdapContext(env, null);
			SearchControls searchCtls = new SearchControls();
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			//String searchFilter = "(&(objectCategory=person)(objectClass=user)(name=张三))";
			//查询过滤条件
			String searchFilter = "SAMAccountName="+userName;
			//搜索域节点OU=人事,DC=greattimes,DC=com
			String searchBase = CONFIG.get(SEARCHBASE);
			//返回结果集
			String returnedAtts[] = {"mobile","telephonenumber"};
			searchCtls.setReturningAttributes(returnedAtts);
			NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter,searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attributes = sr.getAttributes();
				String numberStr = attributes.get("mobile").toString();
				if(StringUtils.isNotBlank(numberStr)){
					//得到对应的电话号码
					telephonenumber = numberStr.substring(numberStr.lastIndexOf(":")+1).trim();
				}
			}
			log.info("AD域身份验证成功,电话号码是:"+telephonenumber);
		} catch (AuthenticationException e) {
			log.error("AD域身份验证失败!");
			log.error(e.getMessage());
			e.printStackTrace();
			return telephonenumber;
		} catch (javax.naming.CommunicationException e) {
			log.error("AD域连接失败!");
			log.error(e.getMessage());
			e.printStackTrace();
			return telephonenumber;
		}catch(NullPointerException e){
			log.error("AD域用户手机号码为空!");
			log.error(e.getMessage());
			e.printStackTrace();
			return telephonenumber;
		} catch (Exception e) {
			log.error("AD域身份验证未知异常!");
			log.error(e.getMessage());
			e.printStackTrace();
			return telephonenumber;
		} finally{
			if(null!=ctx){
				try {
					ctx.close();
					ctx=null;
				} catch (Exception e) {
					log.error(e.getMessage());
					e.printStackTrace();
				}
			}
		}
		return telephonenumber;
	}
	/**
	 * 得到ad域用户详细信息（包括姓名，手机号，邮箱）
	 * @param userName  用户名
	 * @param password  密码
	 * @return 封装mobile和mail的map集合
	 */
	public static Map<String,Object> getUserDetail(String userName, String password,Map<String, String> param){
		String url = new String("ldap://" + param.get(HOST) + ":" + param.get(PORT));
		String user = userName.indexOf(param.get(DOMAIN)) > 0 ? userName : userName+ param.get(DOMAIN);
		// String url = new String("ldap://" +"10.5.32.14"+":"+"389");//ldap://10.5.32.14:389
		//String user = "ex_kjyyb_wanglei@sdb.local";//admin2@sdb.local
		//log.info("AD域user:"+user);
		//log.info("url"+url);
		String mobile = null;
		String mail = null;
		String cn = null;
		String name = null;
		Map<String,Object> resultMap = new HashMap<String,Object>();
		LdapContext ctx = null;
        //实例化一个Env
		Hashtable env = new Hashtable();
        //LDAP访问安全级别(none,simple,strong),一种模式，这么写就行
		env.put(Context.SECURITY_AUTHENTICATION, "simple");
		env.put(Context.SECURITY_PRINCIPAL, user);
		env.put(Context.SECURITY_CREDENTIALS, password);
		env.put(Context.INITIAL_CONTEXT_FACTORY,
				"com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, url);
		try {
			ctx = new InitialLdapContext(env, null);
			SearchControls searchCtls = new SearchControls();
			searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			//String searchFilter = "(&(objectCategory=person)(objectClass=user)(name=张三))";
            //查询过滤条件
			String searchFilter = "SAMAccountName="+userName;
            //搜索域节点OU=人事,DC=greattimes,DC=com
			String searchBase = param.get(SEARCHBASE);
			//String searchBase = "OU=SDB,DC=sdb,DC=local";//搜索域节点OU=人事,DC=greattimes,DC=com
            //返回结果集
			String returnedAtts[] = {"mobile","mail","cn","name","userPrincipalName"};
			searchCtls.setReturningAttributes(returnedAtts);
			NamingEnumeration<SearchResult> answer = ctx.search(searchBase, searchFilter,searchCtls);
			while (answer.hasMoreElements()) {
				SearchResult sr = (SearchResult) answer.next();
				Attributes attributes = sr.getAttributes();
				try{
					String mobileStr = attributes.get("mobile").toString();
					if(StringUtils.isNotBlank(mobileStr)){
                        //得到对应的手机号码
						mobile = mobileStr.substring(mobileStr.lastIndexOf(":")+1).trim();
					}
				}catch(Exception e){
					e.printStackTrace();
					log.error("用户手机不存在");
				}

				try{
					String mailStr = attributes.get("mail").toString();
					if(StringUtils.isNotBlank(mailStr)){
                        //得到对应的邮箱
						mail = mailStr.substring(mailStr.lastIndexOf(":")+1).trim();
					}
				}catch(Exception e){
					e.printStackTrace();
					log.error("用户邮箱不存在");
				}

				try{
					String cnStr = attributes.get("cn").toString();
					if(StringUtils.isNotBlank(cnStr)){
                        //得到对应的邮箱
						cn = cnStr.substring(cnStr.lastIndexOf(":")+1).trim();
					}
				}catch(Exception e){
					e.printStackTrace();
					log.error("用户cn不存在");
				}
				try{
					String nameStr = attributes.get("name").toString();
					if(StringUtils.isNotBlank(nameStr)){
                        //得到对应的邮箱
						name = nameStr.substring(nameStr.lastIndexOf(":")+1).trim();
					}
				}catch(Exception e){
					e.printStackTrace();
					log.error("用户name不存在");
				}
			}
			resultMap.put("mobile", mobile);
			resultMap.put("mail", mail);
			resultMap.put("cn", cn);
			resultMap.put("name", name);
            //200代表成功，500代表失败
			resultMap.put("retCode", "200");
				log.info("AD域身份验证成功:"+resultMap);
		} catch (AuthenticationException e) {
			log.info("AD域身份验证失败!");
			log.info(e.getMessage());
			e.printStackTrace();
            //200代表成功，500代表身份验证失败，501代表AD域连接失败，502代表未知异常
			resultMap.put("retCode", "500");
			return resultMap;
		} catch (javax.naming.CommunicationException e) {
			log.info("AD域连接失败!");
			log.info(e.getMessage());
			e.printStackTrace();
            //200代表成功，500代表身份验证失败，501代表AD域连接失败，502代表未知异常
			resultMap.put("retCode", "501");
			return resultMap;
		} catch (Exception e) {
			log.info("AD域身份验证未知异常!");
			log.info(e.getMessage());
			e.printStackTrace();
            //200代表成功，500代表身份验证失败，501代表AD域连接失败，502代表未知异常
			resultMap.put("retCode", "502");
			return resultMap;
		} finally{
			if(null!=ctx){
				try {
					ctx.close();
					ctx=null;
				} catch (Exception e) {
					log.info(e.getMessage());
					e.printStackTrace();
				}
			}
		}
	    return resultMap;
	}
}
