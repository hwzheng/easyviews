package com.greattimes.ev.common.utils;


import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.*;

/**
 * 工具类
 * @author LH
 *
 */
public  class evUtil {
	
	
	private evUtil() {
	     // Cannot be instantiated
	}
	
	/**
	 * list 空或size为0判断
	 * @param list
	 * @return
	 */
	public static boolean listIsNullOrZero(List<?> list){
		return list==null|| list.size()==0;
	}

	/**
	 * 判断collection 集合为空
	 * @param collection
	 * @return
	 */
	public static boolean collectionIsNullOrZero(Collection<?> collection){
		return collection == null|| collection.size()==0;
	}
	
	/**
	 * map获取value 值类型 string
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getMapStrValue(Map<String, Object> map,String key){
		return map.get(key)==null?"":map.get(key)+"";
	}

	/**
	 * map获取value 值类型 string
	 * @param map
	 * @param key
	 * @return
	 */
	public static String getMapStr(Map<String, Object> map,String key){
		return map.get(key)==null ? null :map.get(key)+"";
	}


	/**
	 * map获取value 值类型 int
	 * @param map
	 * @param key
	 * @return
	 */
	public static int getMapIntValue(Map<String, Object> map,String key){
		return map.get(key)==null?0:Integer.parseInt(map.get(key)+"");
	}

	/**
	 * map获取value 值类型 Integer
	 * @author NJ
	 * @date 2018/12/3 19:41
	 * @param map
	 * @param key
	 * @return java.lang.Integer
	 */
	public static Integer getMapIntegerValue(Map<String, Object> map,String key){
		return (Integer)map.get(key);
	}
	
	/**
	 * map获取value 值类型 long
	 * @param map
	 * @param key
	 * @return
	 */
	public static long getMapLongValue(Map<String, Object> map,String key){
		return map.get(key)==null?0:Long.parseLong(map.get(key)+"") ;
	}

	/**
	 * map获取value 值类型 Long
	 * @author NJ
	 * @date 2018/12/29 9:38
	 * @param map
	 * @param key
	 * @return java.lang.Long
	 */
	public static Long getMapLong(Map<String, Object> map, String key){
		return (Long)map.get(key);
	}


	/**
	 * map获取value 值类型 double
	 * @param map
	 * @param key
	 * @return
	 */
	public static double getMapDoubleValue(Map<String, Object> map,String key){
		return map.get(key)==null?0:Double.parseDouble(map.get(key)+"") ;
	}
	
	/**
	 * map获取value 值类型 double
	 * @param map
	 * @param key
	 * @return
	 */
	public static Double getMapDouble(Map<String, Object> map,String key){
		return (Double)map.get(key);
	}
	/**
	 * String 数组转list
	 * @param arr
	 * @return
	 */
	public static List<String> arr2List(String[] arr){
		List<String> list=new ArrayList<String>();
		for (int i = 0; i < arr.length; i++) {
			list.add(arr[i]);
		}
		return list;
	}
	
	/**
	* @Description 将一个int型的ip地址转化为xx.xx.xx.xx类型
	* @Method ipToStr
	* @param ip
	* @return String
	* 将整数值进行右移位操作（>>）  
    * 右移24位，右移时高位补0，得到的数字即为第一段IP  
    * 右移16位，右移时高位补0，得到的数字即为第二段IP 
    * 右移8位，右移时高位补0，得到的数字即为第三段IP  
    * 最后一段的为第四段IP 
	*/
	
	public static String ipToStr(int ip) {
		return  (ip & 0xFF) + "." + (ip >> 8 & 0xFF) + "." + (ip >> 16 & 0xFF) + "." + (ip >> 24 & 0xFF) ;
	}
	
	/**
	* @Description 将xx.xx.xx.xx类型的ip地址转化为int型
	* @Method ipToInt
	* @param ip
	* @return int
	 */
	public static int ipToInt(String ip) {
		String[] ipnum = ip.split("\\.");
		if(ipnum.length != 4) {
			throw new IllegalArgumentException("wrong ip string");
		} else {
			return (Integer.parseInt(ipnum[3])<<24) + 
				(Integer.parseInt(ipnum[2])<<16) + 
				(Integer.parseInt(ipnum[1])<<8) + 
				(Integer.parseInt(ipnum[0])) ;
		}
	}
	
	
	/**
	 * 格式化
	 * @param val
	 * @return
	 */
	public static String formatVal(Double val){
		String value="0";
		if(val!=null){
			if(val<0) val=0.00;
			if(val<0.01 && val>0) val=0.01;
			DecimalFormat df = new DecimalFormat("0.00");
			value = df.format(val);
		}
		return value;
	}
	/**
	 * 格式化保留四位小数
	 * @param val
	 * @return
	 */
	public static String formatValF(Double val){
		String value="0";
		if(val!=null){
			if(val<0) val=0.00;
			if(val<0.01 && val>0) val=0.01;
			DecimalFormat df = new DecimalFormat("0.0000");
			value = df.format(val);
		}
		return value;
	}
	
	/**
	 * 获取监控类型表名
	 * @param type 1:指标表 2：预处理表
	 * @param monitorType
	 * @return
	 */
	public static String getMonitorTable(int type,String monitorType,long time){
		String table=null;
		if(type==1){
			table="bpm_tran_";
			String date=DateUtils.format(new Date(time), "yyyyMM");
			table=table+monitorType+"_0_"+date;
		}else{
			table="bpm_prebusyness_";
			String date=DateUtils.format(new Date(time), "yyyyMMdd");
			table=table+monitorType+"_0_"+date;
		}
		return table;
	}

	/**四舍五入保留x位
	 * @author CGC
	 * @param d
	 * @param bit
	 * @return
	 */
	public static Double doubleBitUp(double d,int bit) {
	    if (d == 0.0)
	        return d;
	    double pow = Math.pow(10, bit);
	    return (double)Math.round(d*pow)/pow;
	}
	/** 
     * double 相减 
     * @param d1 
     * @param d2 
     * @return 
     */ 
    public static double sub(double d1,double d2){ 
        BigDecimal bd1 = new BigDecimal(Double.toString(d1)); 
        BigDecimal bd2 = new BigDecimal(Double.toString(d2)); 
        return bd1.subtract(bd2).doubleValue(); 
    } 
}
