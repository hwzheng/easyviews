package com.greattimes.ev.common.model;



import java.io.Serializable;

/**
 * json 结果返回对象
 */
@SuppressWarnings("serial")
public class JsonResult<T> implements Serializable {
    private String code;
    private String msg;
    private String exception;
    private T data;
    
	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}
}
