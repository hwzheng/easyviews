package com.greattimes.ev.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

/***
 * 字符串工具类
 * @author LiHua
 */
public class StringUtils {
	
	private static final Pattern INT_PATTERN = Pattern.compile("^\\d+$");

	public static String valueOf(Object obj) {
		return (obj == null) ? null : obj.toString();
	}

	public static String valueOfNullStr(Object obj) {
		return (obj == null) ? "" : obj.toString();
	}

	/**
	 * 字符串是否为空
	 * @param str
	 * @return
	 */
	public static  boolean isEmpty(String str){
	  return str==null || "".equals(str) ? true:false;
	}
	
	 /**
     * 字符串是否非空
     * @param str source string.
     * @return is not empty.
     */
    public static boolean isNotEmpty(String str) {
        return str != null && str.length() > 0;
    }

    /**
     * 字符串相等
     * @param s1
     * @param s2
     * @return equals
     */
    public static boolean isEquals(String s1, String s2) {
        if (s1 == null && s2 == null)
            return true;
        if (s1 == null || s2 == null)
            return false;
        return s1.equals(s2);
    }

    /**
     * 字符串是否为数字.
     * @param str
     * @return is integer
     */
    public static boolean isInteger(String str) {
        if (str == null || str.length() == 0)
            return false;
        return INT_PATTERN.matcher(str).matches();
    }

    /**
     * 字符串转int
     * @param str
     * @return
     */
    public static int parseInteger(String str) {
        if (!isInteger(str))
            return 0;
        return Integer.parseInt(str);
    }
	
	/**
	 * 字符串链接
	 * @param strs
	 * @return
	 */
	public static String concat(String... strs){
		if(strs.length==0){
			return "";
		}else{
			StringBuffer sb=new StringBuffer();
			for (String string : strs) {
				sb.append(string);
			}
			return sb.toString();
		}
	}

	/**
	 * 判断指定字符串是否等于null或空字符串
	 *
	 * @param str
	 *            指定字符串
	 * @return 如果等于null或空字符串则返回true，否则返回false
	 */
	public static boolean isBlank(String str) {
		return str == null || "".equals(str.trim());
	}

	/**
	 * 判断指定字符串是否不等于null和空字符串
	 *
	 * @param str
	 *            指定字符串
	 * @return 如果不等于null和空字符串则返回true，否则返回false
	 */
	public static boolean isNotBlank(String str) {
		return !isBlank(str);
	}
	
	/** 对字符串进行MD5编码 */
	public static String getMD5(String password) {
		try {
			// 得到一个信息摘要器
			MessageDigest digest = MessageDigest.getInstance("md5");
			byte[] result = digest.digest(password.getBytes());
			StringBuffer buffer = new StringBuffer();
			// 把每一个byte 做一个与运算 0xff;
			for (byte b : result) {
				// 与运算
				int number = b & 0xff;// 加盐
				String str = Integer.toHexString(number);
				if (str.length() == 1) {
					buffer.append("0");
				}
				buffer.append(str);
			}

			// 标准的md5加密后的结果
			return buffer.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "";
		}
	}
}
