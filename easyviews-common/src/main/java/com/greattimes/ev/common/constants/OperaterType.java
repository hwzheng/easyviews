package com.greattimes.ev.common.constants;
/**
 * 用户操作动作
 * @author LiHua
 *
 */
public enum OperaterType {
	DELETE,
	INSERT,
	UPDATE,
	SELECT,
}
