package com.greattimes.ev.common.utils;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

public class ReportUtil extends BufferedOutputStream{
	public ReportUtil(OutputStream out) {
		super(out);
	}
	/**写入数据
	 * @author CGC
	 * @param csvLine
	 * @throws IOException
	 */
	public void writeLine(final List<String> csvLine) throws IOException {
		StringBuffer sb = new StringBuffer();
		
		for (int i = 0; i < csvLine.size(); i++) {
			String line = csvLine.get(i);
			if (line == null) {
				line = "";
			}
			sb.append("\"").append(line.replaceAll("\"", "\"\"")).append("\",");
		}
		super.write(StringUtils.valueOfNullStr(sb.deleteCharAt(sb.length() - 1)).getBytes());
		super.write("\r\n".getBytes());
	}
	
	public static <T> void downLoadCsv(List<T> data, List<Map<String, String>> headers,String fileName,ReportUtil re,HttpServletResponse response) throws IOException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		byte[] uft8bom={(byte)0xef,(byte)0xbb,(byte)0xbf};
		re.write(uft8bom);
		response.setCharacterEncoding("GBK");
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
        
        String key;
		//create header
        List<String> headerLine = new ArrayList<String>();
        for (int j = 0; j < headers.size(); j++) {
        	Map<String, String> headerMap = headers.get(j);
        	for(Map.Entry<String, String> entry:headerMap.entrySet())
			headerLine.add(entry.getValue());
		}
        re.writeLine(headerLine);
        //write data
        T t;
		Map map;
		Object property;
		for (int i = 0; i < data.size(); i++) {
			List<String> csvLine = new ArrayList<String>();
			t = data.get(i);
            for (int j = 0; j < headers.size(); j++) {
            	key = headers.get(j).keySet().iterator().next();
                if(t instanceof  Map){
                	map = (Map)t;
                	if("timestamp".equals(key)){
                		String str=new BigDecimal(evUtil.getMapStrValue(map, key)).toPlainString();
                		csvLine.add(DateUtils.longToDefaultDateTimeMicrosecond(Long.parseLong(str))+"\t");
                	}else if("responseState".equals(key)) {
                		String responseState=evUtil.getMapStrValue(map, key).equals("0")?"有响应":"无响应";
                		csvLine.add(responseState);
                	}else if("transState".equals(key)) {
                		String transState=evUtil.getMapStrValue(map, key).equals("0")?"成功":"失败";
                		csvLine.add(transState);
                	}else {
                		csvLine.add(StringUtils.valueOfNullStr(map.get(key))+"\t");
                	}
                }else {
                	property = PropertyUtils.describe(t).get(key);
                	if("timestamp".equals(key)){
                		csvLine.add(DateUtils.longToDefaultDateTimeMicrosecond(Long.parseLong(StringUtils.valueOfNullStr(property)))+"\t");
                	}else if("responseState".equals(key)) {
                		String responseState=StringUtils.valueOfNullStr(property).equals("0")?"有响应":"无响应";
                		csvLine.add(responseState);
                	}else if("transState".equals(key)) {
                		String transState=StringUtils.valueOfNullStr(property).equals("0")?"成功":"失败";
                		csvLine.add(transState);
                	}else {
                		csvLine.add(StringUtils.valueOfNullStr(property)+"\t");
                	}
                }
            }
            re.writeLine(csvLine);
		}
		
	}
	/**
	 * EXCEl导出公共方法
	 * @param fileName 文件名称
	 * @param firstTitle 文件工作表名称
	 * @param secondTitle 文件表头标题
	 * @param tableTitleLine 文件中表格标题行数据
	 * @param resultData 文件中表格内容数据
	 * @param response 响应
	 * @throws IOException
	 */
	public static void reportEXCEL(String fileName, String firstTitle,
			String secondTitle, List<String> tableTitleLine,
			List<List<String>> resultData, HttpServletResponse response)
			throws IOException {

		response.addHeader("Content-Disposition", "attachment;filename="
				+ fileName);
		response.setContentType("application/octet-stream");
		response.setCharacterEncoding("utf-8");
		ServletOutputStream out = response.getOutputStream();

		HSSFWorkbook workbook = new HSSFWorkbook(); // 创建工作簿对象
		HSSFCellStyle columnTopStyle = getColumnTopStyle(workbook);// 获取列头样式对象
		HSSFCellStyle style = getStyle(workbook); // 单元格样式对象
		HSSFSheet sheet; // 创建工作表

		try {
			sheet = workbook.createSheet(firstTitle);
			// 产生表格标题行
			HSSFRow rowm = sheet.createRow(0);
			HSSFCell cellTiltle = rowm.createCell(0);
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, tableTitleLine
					.size() - 1));
			cellTiltle.setCellStyle(style);
			cellTiltle.setCellValue(secondTitle);

			// 表头---------------------------------------------------------------------------------
			HSSFRow rowRowName = sheet.createRow(2); // 在索引2的位置创建行(最顶端的行开始的第二行)

			for (int i = 0; i < tableTitleLine.size(); i++) {
				HSSFCell cellRowName = rowRowName.createCell(i); // 创建列头对应个数的单元格
				cellRowName.setCellType(HSSFCell.CELL_TYPE_STRING); // 设置列头单元格的数据类型
				cellRowName.setCellValue(new HSSFRichTextString(tableTitleLine
						.get(i))); // 设置列头单元格的值
				cellRowName.setCellStyle(columnTopStyle); // 设置列头单元格样
			}

			for (int i = 0; i < resultData.size(); i++) {
				HSSFRow row = sheet.createRow(i + 3);// 创建所需的行数
				HSSFCell xcell = null;
				for (int j = 0; j < resultData.get(i).size(); j++) {
					xcell = row.createCell(j, HSSFCell.CELL_TYPE_STRING);
					xcell.setCellValue(resultData.get(i).get(j));
					xcell.setCellStyle(style);
				}
			}

			// 让列宽随着导出的列长自动适应
			for (int colNum = 0; colNum < tableTitleLine.size(); colNum++) {
				int columnWidth = sheet.getColumnWidth(colNum) / 512;
				for (int rowNum = 0; rowNum < sheet.getLastRowNum(); rowNum++) {
					HSSFRow currentRow;
					// 当前行未被使用过
					if (sheet.getRow(rowNum) == null) {
						currentRow = sheet.createRow(rowNum);
					} else {
						currentRow = sheet.getRow(rowNum);
					}
					if (currentRow.getCell(colNum) != null) {
						HSSFCell currentCell = currentRow.getCell(colNum);
						if (currentCell.getCellType() == HSSFCell.CELL_TYPE_STRING) {
							int length = currentCell.getStringCellValue()
									.getBytes().length;
							if (columnWidth < length) {
								columnWidth = length;
							}
						}
					}
				}

				if (colNum == 0) {
					sheet.setColumnWidth(colNum, 10 * 512);
				} else {
					sheet.setColumnWidth(colNum, (columnWidth + 4) * 512);
				}

			}
			workbook.write(out);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}
	
	/*
	 * 列头单元格样式
	 */
	private static HSSFCellStyle getColumnTopStyle(HSSFWorkbook workbook) {
		// 设置字体
		HSSFFont font = workbook.createFont();
		// 设置字体大小
		font.setFontHeightInPoints((short) 11);
		// 字体加粗
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 设置字体名字
		font.setFontName("Microsoft YaHei");

		font.setColor(HSSFColor.WHITE.index);
		// 设置样式;
		HSSFCellStyle style = workbook.createCellStyle();

		style.setFillForegroundColor(HSSFColor.TEAL.index);
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		// 设置底边框;
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		// 设置底边框颜色;
		style.setBottomBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 设置左边框;
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		// 设置左边框颜色;
		style.setLeftBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 设置右边框;
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		// 设置右边框颜色;
		style.setRightBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 设置顶边框;
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// 设置顶边框颜色;
		style.setTopBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 在样式用应用设置的字体;
		style.setFont(font);
		// 设置自动换行;
		style.setWrapText(false);
		// 设置水平对齐的样式为居中对齐;
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 设置垂直对齐的样式为居中对齐;
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;
	}
	
	/*
	 * 列数据信息单元格样式
	 */
	private static HSSFCellStyle getStyle(HSSFWorkbook workbook) {
		// 设置字体
		HSSFFont font = workbook.createFont();
		// 设置字体大小
		// font.setFontHeightInPoints((short)10);
		// 字体加粗
		// font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		// 设置字体名字
		font.setFontName("Microsoft YaHei");
		// 设置样式;
		HSSFCellStyle style = workbook.createCellStyle();
		// 设置底边框;
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		// 设置底边框颜色;
		style.setBottomBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 设置左边框;
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		// 设置左边框颜色;
		style.setLeftBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 设置右边框;
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		// 设置右边框颜色;
		style.setRightBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 设置顶边框;
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		// 设置顶边框颜色;
		style.setTopBorderColor(HSSFColor.GREY_50_PERCENT.index);
		// 在样式用应用设置的字体;
		style.setFont(font);
		// 设置自动换行;
		style.setWrapText(false);
		// 设置水平对齐的样式为居中对齐;
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		// 设置垂直对齐的样式为居中对齐;
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);

		return style;
	}
}
