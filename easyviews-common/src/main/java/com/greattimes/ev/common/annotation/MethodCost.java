package com.greattimes.ev.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface MethodCost {
	
	/**
	 * 方法描述
	 * @return
	 */
	String desc() default ""; 
	
	/**
	 * 是否打印日志
	 * @return
	 */
	boolean active() default true;
	
}
