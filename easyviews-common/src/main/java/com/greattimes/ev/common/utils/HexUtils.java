package com.greattimes.ev.common.utils;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Pattern;

/**
 * @Description 16进制工具
 * @Project DecodeEngine
 * @Company www.greattimes.com.cn
 * @Copyright Copyright(c) 2015-2016
 * @Author Bin.Fu
 * @CreateDate 2015年5月22日 下午6:00:31
 * @UpdateUser
 * @UpdateDate
 * @UpdateRemark
 * @Version 1.0
 */
public final class HexUtils {

	/**
	 * @Description 16进制数字匹配表达式
	 */
	private final static Pattern HEX_NUMBER_PATTERN = Pattern.compile("^([a-f0-9A-F]+)$");

	/**
	 * @Description 将字节转换为16进制字符串
	 * @param byt 字节
	 * @return
	 */
	public static String toString(byte byt) {
		String hex = Integer.toHexString(byt & 0xff);
		return hex.length() < 2 ? "0" + hex : hex;
	}

	/**
	 * @Description 将字节数组转换为16进制字符串
	 * @param bytes 字节数组
	 * @return
	 */
	public static String toString(byte[] bytes) {
		return toStringWithFormat(bytes, false);
	}

	/**
	 * @Description 将自己数组转换为16进制字符串，并进行格式优化
	 * @param bytes 字节数组
	 * @return
	 */
	public static String toPrettyString(byte[] bytes) {
		return toStringWithFormat(bytes, true);
	}

	/**
	 * @Description 将自己数组转换为16进制字符串，并根据条件进行格式优化
	 * @param bytes 字节数组
	 * @param isFormat 是否进行格式优化
	 * @return
	 */
	private static String toStringWithFormat(byte[] bytes, boolean isFormat) {
		String line = System.getProperty("line.separator");
		StringBuilder stringBuilder = new StringBuilder();
		if (bytes == null || bytes.length <= 0) {
			return "";
		}
		for (int i = 0; i < bytes.length; i++) {
			String hex = Integer.toHexString(bytes[i] & 0xff);
			if (isFormat) {
				if (i != 0) {
					if (i % 16 == 0) {
						stringBuilder.append(line);
					} else if (i % 8 == 0) {
						stringBuilder.append("  ");
					} else {
						stringBuilder.append(" ");
					}
				}
			}
			if (hex.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hex);
		}
		return stringBuilder.toString();
	}

	/**
	 * @Description 判断指定字符串是否为16进制数字字符串
	 * @param string 指定字符串
	 * @return
	 */
	public static boolean isHexNumber(String string) {
		if (StringUtils.isBlank(string) || StringUtils.isEmpty(string)) {
			return false;
		}
		return HEX_NUMBER_PATTERN.matcher(string).matches();
	}

	/**
	 * @Description 将指定的16进制字符串转字节数组
	 * @param hex 指定的16进制字符串
	 * @return
	 */
	public static byte[] toBytes(String hex) {
		int len = (hex.length() / 2);
		byte[] result = new byte[len];
		char[] chars = hex.toUpperCase().toCharArray();
		for (int i = 0; i < len; i++) {
			int pos = i * 2;
			result[i] = (byte) (toByte(chars[pos]) << 4 | toByte(chars[pos + 1]));
		}
		return result;
	}

	/**
	 * @Description 将指定的字符转换为字节
	 * @param chr 指定的字符
	 * @return
	 */
	private static int toByte(char chr) {
		byte byt = (byte) "0123456789ABCDEF".indexOf(chr);
		return byt;
	}

}
