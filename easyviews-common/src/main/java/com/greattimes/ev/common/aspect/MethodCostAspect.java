package com.greattimes.ev.common.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.greattimes.ev.common.annotation.MethodCost;
import com.greattimes.ev.common.utils.StringUtils;
@Aspect
@Component
public class MethodCostAspect {
	Logger log=LoggerFactory.getLogger("performance");
	
	public MethodCostAspect() {
		super();
		// TODO Auto-generated constructor stub
		log.info("performance log is open...");
	}

	@Around("execution(* com.greattimes.ev..*.*(..)) && @annotation(mc)")  
    public Object around(ProceedingJoinPoint joinPoint ,MethodCost mc) throws Throwable {          
        long startTime = System.currentTimeMillis();          
        Object obj= joinPoint.proceed();  
        Long costTime = System.currentTimeMillis() - startTime;     
        String methodName = joinPoint.getSignature().getName();          
        String className=joinPoint.getTarget().getClass().getName();
        String desc=mc.desc();
    /*    String key=ConfigConstants.LOG_ASPECT_PRINT;
        String flag=ConfigConstants.getValue(key);
        if( flag!=null && "true".equals(flag)){ //强制开关参数 当前不生效屏蔽
        	log.info(  className+"["+methodName + "] "+desc+" finished ! Cost : [" + costTime + "] ms");
        }*/
        //记录执行请求耗时  
        if(mc.active()){
        	log.info(StringUtils.concat(className ,"." ,methodName,"[",desc,"] finished ! Cost : [",costTime.toString(),"] ms"));
        }
        return obj;
    }   
}
