package com.greattimes.ev.common.constants;

/**
 * @author NJ
 * @date 2018/8/14 15:38
 */
public class ConfigConstants {

    public static final String ZERO_STRING = "0";

    public static final String ONE_STRING = "1";

    /**	是否开启AD域校验 0： 否 1： 是*/
    public static final String HAS_AD_VERIFICATION = "has_ad_verification";

    /**	AD域HOST地址*/
    public static final String AD_HOST = "ad_host";

    /**	AD域PORT地址*/
    public static final String AD_PORT = "ad_port";

    /**	AD域DOMAIN地址*/
    public static final String AD_DOMAIN = "ad_domain";

    /**	AD域SERACHBASE地址*/
    public static final String AD_SEARCHBASE = "ad_searchbase";

    /**	版本号 1.0 含有ad域功能*/
    public static final String VERSION = "version";

    /**	自定义解码，修改协议状态通知解码url*/
    public static final String  DECODE_STATE_CHANGE_URL= "decode_state_change_url";

    /**	自定义解码，根据协议id,查询协议详情url*/
    public static final String DECODE_DETAIL_URL = "decode_detail_url";

    /**	自定义解码，协议保存或更新通知解码url*/
    public static final String DECODE_SAVE_UPDATE_URL = "decode_save_update_url";
    /**	自定义解码，ip port*/
    public static final String DECODE_HOST = "decode_host";

    /**	bpm数据颗粒度(s)*/
    public static final String BPM_INTERVAL = "bpm_interval";
    /**	bpm数据小时颗粒度(s)*/
    public static final String BPM_INTERVAL_HOUR = "bpm_interval_hour";
    /**	bpm数据5分钟颗粒度(s)*/
    public static final String BPM_INTERVAL_FIVE = "bpm_interval_five";
    /**	bpm数据颗粒度切换阈值*/
    public static final String BPM_INTERVAL_THRESHOLD = "bpm_interval_threshold";
    /**	bpm数据颗粒度5分钟切换阈值*/
    public static final String BPM_INTERVAL_THRESHOLD_FIVE = "bpm_interval_threshold_five";
    /**	bpm数据颗粒度天切换阈值*/
    public static final String BPM_INTERVAL_THRESHOLD_DAY = "bpm_interval_threshold_day";
    /**Pcap下载IP/PORT*/
    public static final String DECODEMANAGER_HOST = "decode_manager_host";
    /**	Pcap数据下载url*/
    public static final String PCAP_DOWNLOAD_URL = "pcap_download_url";
    /**Pcap数据下载取消url*/
    public static final String PCAP_DOWNLOAD_CANCEL_URL = "pcap_download_cancel_url";
    /**单笔追踪数据包下载url*/
    public static final String SESSION_DOWNLOAD_URL = "session_download_url";
    /**单笔追踪数据报文获取下载url*/
    public static final String MESSAGE_DOWNLOAD_URL = "message_download_url";
    
    /**	kpi，ip port*/
    public static final String KPI_HOST = "qinghua_baseline_host";
    /**	kpi基带url*/
    public static final String KPI_BASEBAND = "qinghua_baseband";
    
    /**	智能多维瓶颈分析单次分析请求接口*/
    public static final String AI_ETL_URL = "ai_etl_url";
    /**	交易追踪端口*/
    public static final String TRANSACTION_TRACE_PORT = "transaction_trace_port";

    /** 指标格式化为小数的id集合*/
    public static final String INDICATOR_RATE_IDS = "indicator_rate_ids";

}
