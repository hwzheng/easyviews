package com.greattimes.ev.common.utils;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellRangeAddress;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

public class ProtocolUtil {
	/**
	 * 根据协议类型编码获得协议类型值
	 * 
	 * @param protocolCode
	 * @return
	 */
	public static String getProtocolTypeByProtocolCode(String protocolCode) {
		String protocolType = "";
		if(StringUtils.isBlank(protocolCode)) {
			return protocolType;
		}
		switch (protocolCode) {
		case "100":
			protocolType = "TCP";
			break;
		case "110":
			protocolType = "HTTP";
			break;
		case "111":
			protocolType = "HESSIAN";
			break;
		case "112":
			protocolType = "SOAP";
			break;
		case "113":
			protocolType = "HTML";
			break;
		case "120":
			protocolType = "JOLT";
			break;
		case "130":
			protocolType = "TNS";
			break;
		case "140":
			protocolType = "WMQ";
			break;
		case "X9":
			protocolType = "BUILTIN";
			break;
		}
		return protocolType;
	}
	
	/**
	 * 根据报文类型编码获的报文格式
	 * 
	 * @param messageType
	 * @return
	 */
	public static String getMessageFormatByMessageType(String messageType) {
		String messageFormat = "";
		if(StringUtils.isBlank(messageType)) {
			return messageFormat;
		}
		String str = "";
		String[] arr=messageType.split(",");
		for (String string : arr) {
			switch (string) {
			case "1":
				str = "TEXT";
				break;
			case "2":
				str = "JSON";
				break;
			case "3":
				str = "KVP";
				break;
			case "4":
				str = "SOP";
				break;
			case "5":
				str = "TLV";
				break;
			case "6":
				str = "XML";
				break;
			case "7":
				str = "8583";
				break;
			case "110":
				str = "HTTP";
				break;
			case "111":
				str = "HESSIAN";
				break;
			case "112":
				str = "SOAP";
				break;
			case "113":
				str = "HTML";
				break;
			case "120":
				str = "JOLT";
				break;
			case "130":
				str = "TNS";
				break;
			case "140":
				str = "WMQ";
				break;
			}
			messageFormat=messageFormat+","+str;
		}
		messageFormat = messageFormat.substring(1, messageFormat.length());
		return messageFormat;
	}
	
	/**
	 * 根据协议类型值获得协议类型编码
	 * 
	 * @param protocolCode
	 * @return
	 */
	public static String getProtocolCodeByProtocolType(String ProtocolType) {
		String ProtocolCode = "";
		switch (ProtocolType) {
		case "TCP":
			ProtocolCode = "100";
			break;
		case "HTTP":
			ProtocolCode = "110";
			break;
		case "HESSIAN":
			ProtocolCode = "111";
			break;
		case "SOAP":
			ProtocolCode = "112";
			break;
		case "JOLT":
			ProtocolCode = "120";
			break;
		case "TNS":
			ProtocolCode = "130";
			break;
		case "WMQ":
			ProtocolCode = "140";
			break;
		case "BUILTIN":
			ProtocolCode = "X9";
			break;
		}
		return ProtocolCode;
	}
	
	/**
	 * 根据报文格式获的报文类型编码
	 * 
	 * @param messageType
	 * @return
	 */
	public static String getMessageTypeByMessageFormat(String messageFormat) {
		String messageType = "";
		String str = "";
		String[] arr=messageFormat.split(",");
		for (String string : arr) {
			switch (string) {
			case "TEXT":
				str = "1";
				break;
			case "JSON":
				str = "2";
				break;
			case "KVP":
				str = "3";
				break;
			case "SOP":
				str = "4";
				break;
			case "TLV":
				str = "5";
				break;
			case "XML":
				str = "6";
				break;
			case "8583":
				str = "7";
				break;
			case "HTTP":
				str = "110";
				break;
			case "HESSIAN":
				str = "111";
				break;
			case "SOAP":
				str = "112";
				break;
			case "HTML":
				str = "113";
				break;
			case "JOLT":
				str = "120";
				break;
			case "TNS":
				str = "130";
				break;
			case "WMQ":
				str = "140";
				break;
			}
			messageType=messageType+","+str;
		}
		messageType = messageType.substring(1, messageType.length());
		return messageType;
	}
}
