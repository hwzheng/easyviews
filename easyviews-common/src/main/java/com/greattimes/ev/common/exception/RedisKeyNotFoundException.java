package com.greattimes.ev.common.exception;

/**
 * 自定义redis获取不到key异常
 * @author NJ
 * @date 2019/3/25 13:44
 */
public class RedisKeyNotFoundException extends RuntimeException{

    public RedisKeyNotFoundException(String message) {
        super(message);
    }

}
