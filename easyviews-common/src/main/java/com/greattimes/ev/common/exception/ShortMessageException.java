package com.greattimes.ev.common.exception;

/**
 * @author NJ
 * @date 2018/10/12 15:22
 */
public class ShortMessageException  extends RuntimeException{

    public ShortMessageException() {
    }
    public ShortMessageException(String msg) {
        super(msg);
    }
}
