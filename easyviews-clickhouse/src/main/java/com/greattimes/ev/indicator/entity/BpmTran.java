package com.greattimes.ev.indicator.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("bpmTran")
public class BpmTran extends Model<BpmTran> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer allStart2ends;
	private Integer allTransCount;
	private int applicationId;

	private Date date;
	private Integer responseTransCount;
	private Integer successTransCount;
	private Long time;
	private int uuid;
	@TableField(exist = false)
	private Double responseTime;
	@TableField(exist = false)
	private Double responseRate;
	@TableField(exist = false)
	private Double successRate;

	public Integer getAllStart2ends() {
		return allStart2ends;
	}

	public void setAllStart2ends(Integer allStart2ends) {
		this.allStart2ends = allStart2ends;
	}

	public Integer getAllTransCount() {
		return allTransCount;
	}

	public void setAllTransCount(Integer allTransCount) {
		this.allTransCount = allTransCount;
	}

	public int getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getResponseTransCount() {
		return responseTransCount;
	}

	public void setResponseTransCount(Integer responseTransCount) {
		this.responseTransCount = responseTransCount;
	}

	public Integer getSuccessTransCount() {
		return successTransCount;
	}

	public void setSuccessTransCount(Integer successTransCount) {
		this.successTransCount = successTransCount;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public int getUuid() {
		return uuid;
	}

	public void setUuid(int uuid) {
		this.uuid = uuid;
	}

	public Double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Double responseTime) {
		this.responseTime = responseTime;
	}

	public Double getResponseRate() {
		return responseRate;
	}

	public void setResponseRate(Double responseRate) {
		this.responseRate = responseRate;
	}

	public Double getSuccessRate() {
		return successRate;
	}

	public void setSuccessRate(Double successRate) {
		this.successRate = successRate;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}

	public BpmTran() {
		super();
	}

	public BpmTran(Long time) {
		super();
		this.time = time;
		this.responseTime = null;
		this.responseRate = null;
		this.successRate = null;
		this.allTransCount=null;

	}

}