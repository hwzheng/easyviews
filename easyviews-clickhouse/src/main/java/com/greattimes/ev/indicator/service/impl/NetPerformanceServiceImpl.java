package com.greattimes.ev.indicator.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.common.utils.IndicatorSupportUtil;
import com.greattimes.ev.indicator.mapper.NetPerformanceMapper;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.service.INetPerformanceService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/26 14:54
 */
@Service
public class NetPerformanceServiceImpl implements INetPerformanceService{

    @Autowired
    private NetPerformanceMapper netPerformanceMapper;

    @Autowired
    private ICommonService commonService;

    @Override
    public IndicatorDataParam selectNetPerformanceListForChart(com.greattimes.ev.indicator.param.req.IndicatorParam param) {
        List<Indicator> indicators =  commonService.findIndicatorByIds(param.getIndicator());
        Map<String, Object> paramMap = new HashMap<>(4);
        paramMap.put("start",param.getStart());
        paramMap.put("end", param.getEnd());
        paramMap.put("uuid", param.getUuid());
        List<Map<String, Object>> list = netPerformanceMapper.selectListByMap(paramMap);
        List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(param.getStart(),param.getEnd(),60000);
        //todo need to optimizer
        List<IndicatorDataDetailParam> valueParams = new ArrayList<>();
        for(Indicator indicator : indicators){
            for(Integer uuid : param.getUuid()){
                IndicatorDataDetailParam indicatorValueParam = new IndicatorDataDetailParam();
                indicatorValueParam.setId(indicator.getId());
                indicatorValueParam.setUuid(uuid);
                indicatorValueParam.setValue(IndicatorSupportUtil.getFormatValue(uuid,list,timeList,
                        indicator.getColumnName(),indicator.getColumnType()));
                valueParams.add(indicatorValueParam);
            }
        }
        IndicatorDataParam indicatorParam = new IndicatorDataParam();
        indicatorParam.setTime(timeList);
        indicatorParam.setIndicator(valueParams);
        return indicatorParam;
    }

    @Override
    public List<Map<String, Object>> selectNetPerformanceListForTable(com.greattimes.ev.indicator.param.req.IndicatorParam param) throws Exception {
        Map<String, Object> map  = new HashMap<>(3);
        map.put("start", param.getStart());
        map.put("end", param.getEnd());
        map.put("uuid", param.getUuid());
        return netPerformanceMapper.selectListByMap(map);
    }



}
