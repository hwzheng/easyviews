package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author cgc
 *
 */
@ApiModel
public class IndicatorDataDetailParam implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty("指标id")
	private Integer id;
	@ApiModelProperty("指标维度id")
	private Integer uuid;
	@ApiModelProperty("指标值数组")
	private List<String> value;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUuid() {
		return uuid;
	}
	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}
	public List<String> getValue() {
		return value;
	}
	public void setValue(List<String> value) {
		this.value = value;
	}
	
}
