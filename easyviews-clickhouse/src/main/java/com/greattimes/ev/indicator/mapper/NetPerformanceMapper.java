package com.greattimes.ev.indicator.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.indicator.entity.NetPerformance;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface NetPerformanceMapper extends BaseMapper<NetPerformance> {

	List<Map<String, Object>> selectListByMap(Map<String, Object> param);

	List<Map<String, Object>> selectListByUuids(@Param("ids") List<Integer> ids, @Param("start") long start, @Param("end") long end);

	List<Map<String, Object>> selectAppListByUuids(@Param("ids") List<Integer> ids, @Param("start") long start, @Param("end") long end);
}

