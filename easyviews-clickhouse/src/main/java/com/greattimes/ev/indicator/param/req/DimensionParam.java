package com.greattimes.ev.indicator.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/9/26 19:30
 */
@ApiModel
public class DimensionParam implements Serializable{

    private static final long serialVersionUID = 74842112462251959L;

    /**
     * 查询维度uuid
     */
    @ApiModelProperty("查询维度uuid")
    private Long uuid;

    /**
     * 1 组件 2 业务
     */
    @ApiModelProperty("查询维度uuid")
    private Integer  type;

    /**
     * 开始时间戳
     */
    @ApiModelProperty("查询维度uuid")
    private Long  startTime;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long  endTime;
    /**
     * 维度值id
     */
    @ApiModelProperty("维度值id")
    private List<Integer> dimensionIds;
    /**
     * 指标筛选
     *    indicatorId	int	是	指标id
     *    rule	int	是	指标筛选规则（1大于，2小于，3等于，4不等于）
     *    value	Num	是	值
     */
    @ApiModelProperty("指标筛选")
    private List<JSONObject> filterIndicatorId;

    /**
     * 维度筛选
     *    dimensionId	int	是	维度值id
     *    rule	int	是	维度筛选规则（1包含，2不包含，3等于，4不等于）
     *    value	string	是	值
     */
    @ApiModelProperty("维度筛选")
    private List<JSONObject> filterDimensionId;

    /**
     * 对比日期 20180926
     */
    private String compareData;

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<Integer> getDimensionIds() {
        return dimensionIds;
    }

    public void setDimensionIds(List<Integer> dimensionIds) {
        this.dimensionIds = dimensionIds;
    }

    public List<JSONObject> getFilterIndicatorId() {
        return filterIndicatorId;
    }

    public void setFilterIndicatorId(List<JSONObject> filterIndicatorId) {
        this.filterIndicatorId = filterIndicatorId;
    }

    public List<JSONObject> getFilterDimensionId() {
        return filterDimensionId;
    }

    public void setFilterDimensionId(List<JSONObject> filterDimensionId) {
        this.filterDimensionId = filterDimensionId;
    }

    public String getCompareData() {
        return compareData;
    }

    public void setCompareData(String compareData) {
        this.compareData = compareData;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "DimensionParam{" +
                "uuid=" + uuid +
                ", type=" + type +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", dimensionIds=" + dimensionIds +
                ", filterIndicatorId=" + filterIndicatorId +
                ", filterDimensionId=" + filterDimensionId +
                ", compareData='" + compareData + '\'' +
                '}';
    }
}
