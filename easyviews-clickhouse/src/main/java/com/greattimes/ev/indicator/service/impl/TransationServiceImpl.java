package com.greattimes.ev.indicator.service.impl;

import java.util.*;
import java.util.stream.Collector;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.greattimes.ev.bpm.entity.*;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.mapper.NetPerformanceMapper;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;
import com.greattimes.ev.indicator.service.INetPerformanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.bpm.service.common.IIndicatorService;
import com.greattimes.ev.common.utils.IndicatorSupportUtil;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.entity.BpmFCS;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.mapper.BpmFCSMapper;
import com.greattimes.ev.indicator.mapper.BpmTranMapper;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataDetailParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;
import com.greattimes.ev.indicator.service.ITransactionService;

/**
 * @author cgc
 *
 */
@Service
public class TransationServiceImpl implements ITransactionService {
	@Autowired
	private BpmTranMapper bpmTranMapper;
	@Autowired
	private BpmFCSMapper bpmFCSMapper;
	@Autowired
	private IIndicatorService indicatorService;
	@Autowired
	private ICommonService commonService;
	@Autowired
	private NetPerformanceMapper netPerformanceMapper;

	@Override
	public IndicatorDataParam chart(IndicatorParam param, int interval) {
		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuid = param.getUuid();
		if(evUtil.listIsNullOrZero(uuid)) {
			return result;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Integer> indicator = param.getIndicator();
		// 获取指标名
		Map<Integer, String> colnameList = indicatorService.getIndicatorName(indicator);
		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval * 1000);
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(uuid, start, end);
		// group by uuid
		Map<Integer, List<BpmTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();
		uuid.forEach(x -> {
			if (grouplist.containsKey(x)) {
				resultGroup.put(x, initDefault(grouplist.get(x), timeList));
			} else {
				// init default list
				resultGroup.put(x, initDefault(new ArrayList<>(), timeList));
			}
		});
		// assembly data
		List<IndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
			for (Integer integer : indicator) {
				IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
				detail.setId(integer);
				detail.setUuid(entry.getKey());
				List<BpmTran> bpm = entry.getValue();
				// 置为map
				List<Map<String, Object>> mapbpm = new ArrayList<>();
				bpm.stream().forEach(x -> {
					Map<String, Object> dataMap = new HashMap<>();
					dataMap.put("allTransCount", x.getAllTransCount());
					dataMap.put("responseTime", x.getResponseTime());
					dataMap.put("responseRate", x.getResponseRate());
					dataMap.put("successRate", x.getSuccessRate());
					mapbpm.add(dataMap);
				});
				// 获取指标名
				String colname = colnameList.get(integer);
				// 根据指标名取值
				List<String> invalue = new ArrayList<>();
				mapbpm.stream().forEach(x -> {
					String str = null;
					if (null != x.get(colname)) {
						str = x.get(colname).toString();
					}
					invalue.add(str);
				});

				detail.setValue(invalue);
				value.add(detail);
			}
			result.setIndicator(value);
		}
		return result;
	}

	/**
	 * init default list
	 * 
	 * @param data
	 * @param timeList
	 * @return
	 */
	private List<BpmTran> initDefault(List<BpmTran> data, List<Long> timeList) {
		List<BpmTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : data) {
				if (time.equals(trans.getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmTran(time));
			}
		}
		return result;
	}

	@Override
	public List<TransactionTableParam> table(IndicatorParam param, int interval) {
		List<TransactionTableParam> table = new ArrayList<>();
		List<Integer> uuid = param.getUuid();
		if(evUtil.listIsNullOrZero(uuid)) {
			return table;
		}
		Long start = param.getStart();
		Long end = param.getEnd();
		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval * 1000);
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(uuid, start, end);
		// group by uuid
		Map<Integer, List<BpmTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getUuid, Collectors.toList()));
		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();

		uuid.forEach(x -> resultGroup.put(x, initDefault(grouplist.computeIfAbsent(x, ArrayList :: new), timeList)));

		// assembly data
		for (int i = 0; i < timeList.size(); i++) {
			for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
				TransactionTableParam trans = new TransactionTableParam();
				trans.setTime(timeList.get(i));
				trans.setUuid(entry.getKey());
				trans.setAllTransCount(entry.getValue().get(i).getAllTransCount());
				trans.setResponseRate(entry.getValue().get(i).getResponseRate());
				trans.setResponseTime(entry.getValue().get(i).getResponseTime());
				trans.setSuccessRate(entry.getValue().get(i).getSuccessRate());
				table.add(trans);
			}
		}
		return table;
	}

	@Override
	public Map<String, Object> selectAppTransByMap(Map<String, Object> param) {
		long start = evUtil.getMapLongValue(param, "start");
		long end = evUtil.getMapLongValue(param, "end");
		int applicationId = evUtil.getMapIntValue(param,"applicationId");
		int interval = evUtil.getMapIntValue(param,"interval");
		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end,interval*1000);
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTranByUuid(Arrays.asList(applicationId), start, end);
		List<Integer> dataList = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : bpmTranList) {
				if (time.equals(trans.getTime())) {
					dataList.add(trans.getAllTransCount());
					flag = true;
					break;
				}
			}
			if (!flag) {
				dataList.add(null);
			}
		}
		Map<String, Object> result = new HashMap<>();
		result.put("time", timeList);
		result.put("transCount", dataList);
		return result;
	}

	@Override
	public IndicatorDataParam baseline(IndicatorParam param, int interval) {
		IndicatorDataParam result = new IndicatorDataParam();
		List<Integer> uuidList = param.getUuid();
		if(evUtil.listIsNullOrZero(uuidList)) {
			return result;
		}
		Integer uuid=uuidList.get(0);
		Long start = param.getStart();
		Long end = param.getEnd();
		Integer indicator = null;
		if(!evUtil.listIsNullOrZero(param.getIndicator())) {
			indicator=param.getIndicator().get(0);
		}
		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval * 1000);
		List<BpmFCS> bpmFCSList = bpmFCSMapper.selectBpmFCSByUuid(uuid,indicator,start,end);
		bpmFCSList=initDefaultBpmFCS(bpmFCSList, timeList,uuid,indicator);
		// assembly data
		List<IndicatorDataDetailParam> value = new ArrayList<>();
		result.setTime(timeList);
		IndicatorDataDetailParam detail = new IndicatorDataDetailParam();
		detail.setId(indicator);
		detail.setUuid(uuid);
		List<Double> valueDouble = bpmFCSList.stream().map(BpmFCS::getFcsdata).collect(Collectors.toList());
		List<String> valueStr=new ArrayList<>();
		//转换成List<String>
		for (Double double1 : valueDouble) {
			if(null!=double1) {
				valueStr.add(double1.toString());
			}
			else {
				valueStr.add(null);
			}
		}
		detail.setValue(valueStr);
		value.add(detail);
		result.setIndicator(value);
		return result;
	}

	private List<BpmFCS> initDefaultBpmFCS(List<BpmFCS> data, List<Long> timeList, Integer uuid, Integer indicator) {
		List<BpmFCS> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmFCS trans : data) {
				if (time.equals(trans.getTime())) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmFCS(time,uuid,indicator));
			}
		}
		return result;
	}

	@Override
	public List<Map<String, Object>> selectTransByMap(Map<String, Object> param) {
		int uuid = evUtil.getMapIntValue(param,"uuid");
		int type = evUtil.getMapIntValue(param, "type");
		//1、交易性能 2、网络性能 3、应用性能
		int indicatorType = evUtil.getMapIntValue(param, "indicatorType");
		/**
		 * 组件1 全部ip 2  ip 3 业务4  维度5
		 */
		List<Integer> ids = Arrays.asList(uuid);
		List<Integer> uuids = new ArrayList<>();
		Map<Integer, Object> nameMap = new HashMap<>();
		Map<String, Object> alarmParamMap = new HashMap<>();
		Map<Integer, Object> customMap = new HashMap<>();
		if(type == 1 || type == 2){
			List<ServerIp> serverIps = commonService.findServerIpsByComponentIds(ids);
			for(ServerIp serverIp : serverIps){
				uuids.add(serverIp.getId());
				nameMap.put(serverIp.getId(), serverIp.getIp());
			}
			alarmParamMap.put("productType", 1);
			alarmParamMap.put("datalevel", 3);

		}else if(type == 3){
			List<Server>  servers = commonService.selectPortByIpId(uuid);
			for(Server server : servers){
				uuids.add(server.getId());
				nameMap.put(server.getId(), server.getPort());
			}
			alarmParamMap.put("productType", 1);
			alarmParamMap.put("datalevel", 4);
		}else if(type == 4){
			List<Custom> customs = commonService.selectCustomByComponentId(uuid);
			for(Custom custom : customs){
				uuids.add(custom.getId());
				nameMap.put(custom.getId(), custom.getName());
				customMap.put(custom.getId(),custom.getId());
			}
			alarmParamMap.put("productType", 2);
			alarmParamMap.put("datalevel", 1);


		}else if(type == 5){
			List<DimensionValue> values = commonService.selectDimensionValueByStatisticsDimensionId(uuid);
			for(DimensionValue dimensionValue : values){
				uuids.add(dimensionValue.getId());
				nameMap.put(dimensionValue.getId(), dimensionValue.getName());
				customMap.put(dimensionValue.getId(), dimensionValue.getCustomId());
			}
			/**
			 * 1 扩展级 ，2 统计维度，3分析维度，4多维度，
			 */
			alarmParamMap.put("productType", 2);
			alarmParamMap.put("datalevel", Arrays.asList(2,3,4));
		}

		if(evUtil.listIsNullOrZero(uuids)){
			return Collections.emptyList();
		}

		Long start = evUtil.getMapLongValue(param, "start");
		Long end = evUtil.getMapLongValue(param, "end");

		List<Map<String, Object>> dataList = new ArrayList<>();
		//custom
		if(type == 4 || type == 5){
			if(indicatorType == 1){
				//交易
				dataList = bpmTranMapper.selectBpmExtendTranMapByUuid(uuids, start, end);
				dataList.stream().forEach(x->{
					x.put("name", nameMap.get(x.get("uuid")));
					x.put("alarmamount", 0);
					x.put("customId",customMap.get(x.get("uuid")));
				});
			}else if(indicatorType == 2){
				//网络
				dataList = netPerformanceMapper.selectListByUuids(uuids, start, end);
				dataList.stream().forEach(x->{
					x.put("alarmamount", 0);
					x.put("name", nameMap.get(x.get("uuid")));
					x.put("customId",customMap.get(x.get("uuid")));
					//ConnectionRequest,ConnectionOK,ConnectionClose,ConnectionRestClose,ConnectionTimeoutClose,Connections
					x.remove("ConnectionRequest");
					x.remove("ConnectionOK");
					x.remove("ConnectionClose");
					x.remove("ConnectionRestClose");
					x.remove("ConnectionTimeoutClose");
					x.remove("Connections");
				});
			}else if(indicatorType == 3){
				//应用
				dataList = netPerformanceMapper.selectAppListByUuids(uuids, start, end);
				dataList.stream().forEach(x->{
					x.put("name", nameMap.get(x.get("uuid")));
					x.put("customId",customMap.get(x.get("uuid")));
					x.put("alarmamount", 0);
				});
			}
		}else{
			if(indicatorType == 1){
				dataList = bpmTranMapper.selectBpmTranMapByUuid(uuids, start, end);
				dataList.stream().forEach(x->{
					x.put("name", nameMap.get(evUtil.getMapIntValue(x,"uuid")));
					x.put("alarmamount", 0);
//					map.put("name", nameMap.get(evUtil.getMapIntValue(x,"uuid")));
//					map.put("responseTime", evUtil.getMapDoubleValue(x,"responseTime"));
//					map.put("responseRate", evUtil.getMapDoubleValue(x, "responseRate"));
//					map.put("successRate", evUtil.getMapDoubleValue(x,"successRate"));
//					map.put("allTransCount", evUtil.getMapIntValue(x,"allTransCount"));
//					map.put("uuid", evUtil.getMapLongValue(x,"uuid"));
				});
			}else if(indicatorType == 2){
				dataList = netPerformanceMapper.selectListByUuids(uuids, start, end);
				dataList.stream().forEach(x->{
					x.put("name", nameMap.get(evUtil.getMapIntValue(x,"uuid")));
					x.put("alarmamount", 0);
					x.put("customId",customMap.get(x.get("uuid")));
					x.remove("ConnectionRequest");
					x.remove("ConnectionOK");
					x.remove("ConnectionClose");
					x.remove("ConnectionRestClose");
					x.remove("ConnectionTimeoutClose");
					x.remove("Connections");
				});
			}else if(indicatorType == 3){
				dataList = netPerformanceMapper.selectAppListByUuids(uuids, start, end);
				dataList.stream().forEach(x->{
					x.put("name", nameMap.get(x.get("uuid")));
					x.put("alarmamount", 0);
					x.put("customId",customMap.get(x.get("uuid")));
				});
			}
		}
		return dataList;
	}
}
