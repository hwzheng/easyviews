package com.greattimes.ev.indicator.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.indicator.entity.BpmFCS;

public interface BpmFCSMapper extends BaseMapper<BpmFCS> {

	/**
	 * 基线查询
	 * @author cgc  
	 * @date 2018年9月30日  
	 * @param uuid
	 * @param indicator
	 * @param start
	 * @param end
	 * @return
	 */
	List<BpmFCS> selectBpmFCSByUuid(@Param("uuid")Integer uuid,@Param("indicator") Integer indicator,@Param("start") Long start,@Param("end") Long end);
	
}
