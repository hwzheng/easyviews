package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 *
 */
@ApiModel
public class DashBoardParam implements Serializable {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("時間数组")
	private List<Long> time;
	@ApiModelProperty("指标数据")
	private List<DashBoardDetailParam> valueData;

	public List<Long> getTime() {
		return time;
	}

	public void setTime(List<Long> time) {
		this.time = time;
	}

	public List<DashBoardDetailParam> getValueData() {
		return valueData;
	}

	public void setValueData(List<DashBoardDetailParam> valueData) {
		this.valueData = valueData;
	}


}