package com.greattimes.ev.indicator.service;

import java.util.List;


import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;

import java.util.Map;

public interface ITransactionService {

	/**
	 * 交易指标图表类
	 * @author cgc  
	 * @date 2018年9月28日  
	 * @param param
	 * @param interval 
	 * @return
	 */
	IndicatorDataParam chart(IndicatorParam param, int interval);

	/**
	 * 交易指标表格
	 * @author cgc  
	 * @date 2018年9月29日  
	 * @param param
	 * @param interval
	 * @return
	 */
	List<TransactionTableParam> table(IndicatorParam param, int interval);

	/**
	 * 路径图交易量钢琴键查询
	 * @author NJ
	 * @date 2018/9/29 14:53
	 * @param param
	 * @return java.util.Map<java.lang.String,java.lang.Object>
	 */
	Map<String, Object> selectAppTransByMap(Map<String, Object> param);

	/**
	 * 基线查询
	 * @author cgc  
	 * @date 2018年9月30日  
	 * @param param
	 * @param interval
	 * @return
	 */
	IndicatorDataParam baseline(IndicatorParam param, int interval);


	/**    
	 * 查询普普通和拓展指标数据
	 * @author NJ  
	 * @date 2018/9/30 15:35
	 * @param param  
	 * @return java.util.List<com.greattimes.ev.indicator.param.resp.TransactionTableParam>  
	 */
	List<Map<String, Object>>  selectTransByMap(Map<String, Object> param);



}
