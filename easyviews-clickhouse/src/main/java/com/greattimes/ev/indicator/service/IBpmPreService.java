package com.greattimes.ev.indicator.service;

import com.greattimes.ev.indicator.param.req.DimensionParam;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/26 21:02
 */
public interface IBpmPreService {
    /**
     * 10.2.1图表类接口
     * @param param
     * @return
     */
    List<Map<String, Object>> selectListForChart(DimensionParam param);

    /**
     * 10.2.2表格类接口
     * @param param
     * @return
     */
    List<Map<String, Object>> selectListForTable(DimensionParam param);
}
