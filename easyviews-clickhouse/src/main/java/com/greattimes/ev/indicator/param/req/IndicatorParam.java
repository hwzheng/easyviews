package com.greattimes.ev.indicator.param.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/9/26 15:29
 */
@ApiModel
public class IndicatorParam implements Serializable{

    private static final long serialVersionUID = -5295604994500088318L;
    /**
     * 指标维度UUID
     */
    @ApiModelProperty("指标维度UUID")
    private List<Integer> uuid;
    /**
     * 开始时间戳
     */
    @ApiModelProperty("开始时间戳")
    private Long start;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long end;
    /**
     * 指标id
     */
    @ApiModelProperty("指标id")
    private List<Integer> indicator;

    public List<Integer> getUuid() {
        return uuid;
    }

    public void setUuid(List<Integer> uuid) {
        this.uuid = uuid;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public List<Integer> getIndicator() {
        return indicator;
    }

    public void setIndicator(List<Integer> indicator) {
        this.indicator = indicator;
    }

    @Override
    public String toString() {
        return "IndicatorParam{" +
                "uuid=" + uuid +
                ", start=" + start +
                ", end=" + end +
                ", indicator=" + indicator +
                '}';
    }
}
