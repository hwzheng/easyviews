package com.greattimes.ev.indicator.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.indicator.mapper.PreComponentMapper;
import com.greattimes.ev.indicator.service.ITestService;

@Service
public class TestService implements ITestService{

	@Override
	public List<Map<String, Object>> select() {
		// TODO Auto-generated method stub
		return mp.selectList();
	}
	@Autowired
	private PreComponentMapper mp;
}
