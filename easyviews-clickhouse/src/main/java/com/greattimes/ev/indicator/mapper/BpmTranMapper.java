package com.greattimes.ev.indicator.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.greattimes.ev.indicator.entity.BpmTran;

public interface BpmTranMapper extends BaseMapper<BpmTran> {

	List<BpmTran> selectByWhereStr(@Param("str") String str);

	/**
	 * 获取当日交易量
	 * @author cgc  
	 * @date 2018年9月27日  
	 * @param ids
	 * @param current
	 * @param zero
	 * @return
	 */
	List<Map<String, Object>> getTransCountToday(@Param("ids") List<Integer> ids,@Param("current") long current,@Param("zero") long zero);

	List<BpmTran> selectBpmTran(@Param("ids") List<Integer> ids,@Param("start") long start,@Param("end") long end);

	/**
	 * 当前响应时间 当前响应率 当前成功率
	 * @author cgc  
	 * @date 2018年9月27日  
	 * @param ids
	 * @param current
	 * @return
	 */
	List<Map<String, Object>> getCurrent(@Param("ids") List<Integer> ids,@Param("current") long current);

	/**
	 * 应用指标
	 * @author cgc  
	 * @date 2018年9月28日  
	 * @param uuid
	 * @param start
	 * @param end
	 * @return
	 */
	List<BpmTran> selectBpmTranByUuid(@Param("ids") List<Integer> uuid,@Param("start") Long start,@Param("end") Long end);

	/**
	 * 拓展的指标数据
	 * @author NJ
	 * @date 2018/10/8 15:33
	 * @param uuid
	 * @param start
	 * @param end
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectBpmExtendTranMapByUuid(@Param("ids") List<Integer> uuid,@Param("start") Long start,@Param("end") Long end);


	/**
	 * 应用指标
	 * @author cgc
	 * @date 2018年9月28日
	 * @param uuid
	 * @param start
	 * @param end
	 * @return
	 */
	List<Map<String, Object>> selectBpmTranMapByUuid(@Param("ids") List<Integer> uuid,@Param("start") Long start,@Param("end") Long end);

}
