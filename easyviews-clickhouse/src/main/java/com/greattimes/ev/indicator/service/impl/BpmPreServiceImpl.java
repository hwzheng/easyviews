package com.greattimes.ev.indicator.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.bpm.entity.Indicator;
import com.greattimes.ev.bpm.entity.ProtocolField;
import com.greattimes.ev.bpm.service.common.ICommonService;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.indicator.param.req.DimensionParam;
import com.greattimes.ev.indicator.mapper.BpmPreMapper;
import com.greattimes.ev.indicator.service.IBpmPreService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author NJ
 * @date 2018/9/26 21:03
 */
@Service
public class BpmPreServiceImpl implements IBpmPreService{

    @Autowired
    private BpmPreMapper bpmPreMapper;

    @Autowired
    private ICommonService commonService;

    private static final String PER_TABLE_FIX = "bpmPre_";

    @Override
    public List<Map<String, Object>> selectListForChart(DimensionParam param) {
        return null;
    }

    @Override
    public List<Map<String, Object>> selectListForTable(DimensionParam param){

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("start", param.getStartTime());
        paramMap.put("end", param.getEndTime());

        //handle indicatorConditions(indicators)
        List<JSONObject> filterIndicatorIds = param.getFilterIndicatorId();
        if(!evUtil.listIsNullOrZero(filterIndicatorIds)){
            List<Integer> indicatorIds = new ArrayList<>();
            for(JSONObject jsonObject : filterIndicatorIds){
                indicatorIds.add(jsonObject.getIntValue("indicatorId"));
            }
            Map<Integer, Indicator> indicatorMap = commonService.findIndicatorByIds(indicatorIds).stream().collect(Collectors.toMap(Indicator::getId,indicator->indicator));
            //generating condition
            List<String> conditionStr = new ArrayList<>();
            String indicatorConditionStr;
            for(JSONObject jsonObject : filterIndicatorIds){
                if(indicatorMap.get(jsonObject.getInteger("indicatorId")) != null){
                    String str = indicatorMap.get(jsonObject.getInteger("indicatorId")).getColumnName()
                            + this.translateRule(jsonObject.getIntValue("rule")) + jsonObject.get("value");
                    conditionStr.add(str);
                }
            }
            if(evUtil.listIsNullOrZero(conditionStr)){
                indicatorConditionStr = StringUtils.join(conditionStr, " AND ");
                paramMap.put("indicatorCondition", " AND " +indicatorConditionStr);
            }
        }

        //handle  dimensionConditions (include translate)
        if(!evUtil.listIsNullOrZero(param.getFilterDimensionId())){
            List<JSONObject> filterDimensionIds = param.getFilterDimensionId();
            List<Integer> dimensionIds = new ArrayList<>();
            filterDimensionIds.forEach(x->{
                dimensionIds.add(x.getIntValue("dimensionId"));
            });
            Map<Integer, ProtocolField> fieldMap = commonService.findFieldByIds(dimensionIds)
                    .stream().collect(Collectors.toMap(ProtocolField::getId, field->field));
            List<String> conditionStr = new ArrayList<>();
            String indicatorConditionStr;
            for(JSONObject jsonObject : filterIndicatorIds){
                if(fieldMap.get(jsonObject.getInteger("dimensionId")) != null){
                    String str = fieldMap.get(jsonObject.getInteger("dimensionId")).getEname()
                            + this.translateRule(jsonObject.getIntValue("rule")) + jsonObject.get("value");
                    conditionStr.add(str);
                }
            }
            if(evUtil.listIsNullOrZero(conditionStr)){
                indicatorConditionStr = StringUtils.join(conditionStr, " AND ");
                paramMap.put("dimensionCondition", " AND " + indicatorConditionStr);
            }
        }

        //todo how to do tranlate  ???

        //handle dimension columns
        if(!evUtil.listIsNullOrZero(param.getDimensionIds())){

            Map<Integer, ProtocolField> fieldMap = commonService.findFieldByIds(param.getDimensionIds())
                    .stream().collect(Collectors.toMap(ProtocolField::getId, field->field));
            List<String> columnStrList = new ArrayList<>();
            param.getDimensionIds().forEach(x->{
                columnStrList.add("SUM("+fieldMap.get(x).getEname()+") AS " + fieldMap.get(x).getEname());
            });
            if(!evUtil.listIsNullOrZero(columnStrList)){
                paramMap.put("dimensionColumns", "," + StringUtils.join(columnStrList, " , "));
            }
        }
        //todo compare data ???
        //table
        String table = PER_TABLE_FIX + param.getUuid();
        paramMap.put("table", table);
        List<Map<String, Object>> data = bpmPreMapper.selectListByIndicators(paramMap);
        return data;
    }


    /**
     * 筛选规则转化为sql语句条件
     * @param rule
     * @return
     * @throws RuntimeException
     */
    private String translateRule(int rule) throws RuntimeException {
        String sql;
        switch (rule){
            case 1 :
                sql = " > ";
                break;
            case 2 :
                sql = " < ";
                break;
            case 3 :
                sql = " = ";
                break;
            case 4 :
                sql = " != ";
                break;
            default:
                throw new RuntimeException("筛选规则参数有误，参数(rule):"+ rule);
        }
        return sql;
    }
}
