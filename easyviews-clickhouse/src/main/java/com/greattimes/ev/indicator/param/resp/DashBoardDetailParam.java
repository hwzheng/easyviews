package com.greattimes.ev.indicator.param.resp;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 *
 */
@ApiModel
public class DashBoardDetailParam {
	@ApiModelProperty("应用id")
	private Integer id;
	@ApiModelProperty("当日交易量")
	private Integer transCountToday;
	@ApiModelProperty("当前响应时间")
	private double responseTime;
	@ApiModelProperty("当前响应率")
	private double responseRate;
	@ApiModelProperty("当前成功率")
	private double successRate;
	@ApiModelProperty("交易量")
	private List<Integer> transCountValueData;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTransCountToday() {
		return transCountToday;
	}

	public void setTransCountToday(Integer transCountToday) {
		this.transCountToday = transCountToday;
	}

	public double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(double responseTime) {
		this.responseTime = responseTime;
	}

	public double getResponseRate() {
		return responseRate;
	}

	public void setResponseRate(double responseRate) {
		this.responseRate = responseRate;
	}

	public double getSuccessRate() {
		return successRate;
	}

	public void setSuccessRate(double successRate) {
		this.successRate = successRate;
	}

	public List<Integer> getTransCountValueData() {
		return transCountValueData;
	}

	public void setTransCountValueData(List<Integer> transCountValueData) {
		this.transCountValueData = transCountValueData;
	}

}
