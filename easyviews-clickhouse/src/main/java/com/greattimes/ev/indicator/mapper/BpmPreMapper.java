package com.greattimes.ev.indicator.mapper;


import java.util.List;
import java.util.Map;

public interface BpmPreMapper  {

	List<Map<String, Object>> selectList(Map<String, Object> param);

	List<Map<String, Object>> selectListByIndicators(Map<String, Object> param);
}
