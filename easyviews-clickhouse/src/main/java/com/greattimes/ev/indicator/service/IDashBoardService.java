package com.greattimes.ev.indicator.service;

import java.util.List;

import com.greattimes.ev.indicator.param.resp.DashBoardParam;

public interface IDashBoardService {

	DashBoardParam selectApplicationAlarm(List<Integer> ids, Long start, Long end, int interval);
}
