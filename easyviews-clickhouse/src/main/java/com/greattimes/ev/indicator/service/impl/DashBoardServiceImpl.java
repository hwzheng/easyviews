package com.greattimes.ev.indicator.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greattimes.ev.common.utils.IndicatorSupportUtil;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.mapper.BpmTranMapper;
import com.greattimes.ev.indicator.param.resp.DashBoardDetailParam;
import com.greattimes.ev.indicator.param.resp.DashBoardParam;
import com.greattimes.ev.indicator.service.IDashBoardService;

/**
 * @author cgc
 *
 */
@Service
public class DashBoardServiceImpl implements IDashBoardService {
	@Autowired
	private BpmTranMapper bpmTranMapper;

	@Override
	public DashBoardParam selectApplicationAlarm(List<Integer> ids, Long start, Long end, int interval) {
		long zero = end / (1000 * 3600 * 24) * (1000 * 3600 * 24) - TimeZone.getDefault().getRawOffset();

		/*
		 * String str = " "; String inStr = ""; // condition if (ids.size() == 1) { str
		 * = str + "WHERE applicationId=" + ids.get(0); if (start.longValue() ==
		 * end.longValue()) { str = str + " AND time=" + start; } else { str = str +
		 * " AND time>=" + start; str = str + " AND time<=" + end; } } else { if
		 * (start.longValue() == end.longValue()) { str = str + " WHERE time=" + start;
		 * } else { str = str + " WHERE time>=" + start; str = str + " AND time<=" +
		 * end; } for (Integer integer : ids) { inStr = inStr + "," + integer; } inStr =
		 * inStr.substring(1, inStr.length()); str = str + " AND applicationId in(" +
		 * inStr + ")"; }
		 * 
		 * str = str + " ORDER BY time";
		 */

		List<Long> timeList = IndicatorSupportUtil.getFormatTimeList(start, end, interval * 1000);
		// 交易量
		// List<BpmTran> bpmTranList = bpmTranMapper.selectByWhereStr(str);
		List<BpmTran> bpmTranList = bpmTranMapper.selectBpmTran(ids, start, end);
		// 当日交易量
		List<Map<String, Object>> transCountToday = bpmTranMapper.getTransCountToday(ids, end, zero);
		Map<Integer, Object> transCountTodayMap = new HashMap<>();
		for (Map<String, Object> map : transCountToday) {
			transCountTodayMap.put(Integer.parseInt(map.get("applicationId").toString()), map.get("allTransCount"));
		}
		// 补数据
		ids.forEach(x -> {
			if (!transCountTodayMap.containsKey(x)) {
				transCountTodayMap.put(x, 0);
			}
		});
		// 当前响应时间 当前响应率 当前成功率
		List<Map<String, Object>> currentList = bpmTranMapper.getCurrent(ids, end);
		Map<Integer, Map<String, Object>> currentMap = new HashMap<>();
		for (Map<String, Object> map : currentList) {
			currentMap.put(Integer.parseInt(map.get("applicationId").toString()), map);
		}
		// 补数据
		ids.forEach(x -> {
			if (!currentMap.containsKey(x)) {
				currentMap.put(x, new HashMap<>());
			}
		});
		// group by applicationId
		Map<Integer, List<BpmTran>> grouplist = bpmTranList.stream()
				.collect(Collectors.groupingBy(BpmTran::getApplicationId, Collectors.toList()));
		Map<Integer, List<BpmTran>> resultGroup = new LinkedHashMap<>();
		ids.forEach(x -> {
			if (grouplist.containsKey(x)) {
				resultGroup.put(x, initDefault(grouplist.get(x), timeList));
			} else {
				// init default list
				resultGroup.put(x, initDefault(new ArrayList<>(), timeList));
			}
		});
		// assembly data
		DashBoardParam result1 = new DashBoardParam();
		List<DashBoardDetailParam> value = new ArrayList<>();
		result1.setTime(timeList);
		for (Map.Entry<Integer, List<BpmTran>> entry : resultGroup.entrySet()) {
			DashBoardDetailParam detail = new DashBoardDetailParam();
			detail.setId(entry.getKey());
			detail.setTransCountToday(Integer.parseInt(transCountTodayMap.get(entry.getKey()).toString()));
			double responseTime = 0, responseRate = 0, successRate = 0;
			if (null != currentMap.get(entry.getKey()).get("responseTime")) {
				responseTime = (double) currentMap.get(entry.getKey()).get("responseTime");
			}
			if (null != currentMap.get(entry.getKey()).get("responseRate")) {
				responseRate = (double) currentMap.get(entry.getKey()).get("responseRate");
			}
			if (null != currentMap.get(entry.getKey()).get("successRate")) {
				successRate = (double) currentMap.get(entry.getKey()).get("successRate");
			}
			detail.setResponseTime(responseTime);
			detail.setResponseRate(responseRate);
			detail.setSuccessRate(successRate);
			List<BpmTran> bpm = entry.getValue();
			List<Integer> transCountValueData = bpm.stream().map(BpmTran::getAllTransCount)
					.collect(Collectors.toList());
			detail.setTransCountValueData(transCountValueData);
			value.add(detail);
		}
		result1.setValueData(value);
		return result1;
	}

	/**
	 * init default list
	 * 
	 * @param data
	 * @param timeList
	 * @return
	 */
	private List<BpmTran> initDefault(List<BpmTran> data, List<Long> timeList) {
		List<BpmTran> result = new ArrayList<>();
		boolean flag;
		for (Long time : timeList) {
			flag = false;
			for (BpmTran trans : data) {
				if (time.longValue() == trans.getTime().longValue()) {
					result.add(trans);
					flag = true;
					break;
				}
			}
			if (!flag) {
				result.add(new BpmTran(time));
			}
		}
		return result;
	}
}
