package com.greattimes.ev.indicator.service;


import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/26 14:49
 */
public interface INetPerformanceService {
    /**
     * 10.2.1图表类接口
     * @param param
     * @return
     */
    IndicatorDataParam selectNetPerformanceListForChart(com.greattimes.ev.indicator.param.req.IndicatorParam param);

    /**
     * 10.2.2表格类接口
     * @param param
     * @return
     */
    List<Map<String, Object>> selectNetPerformanceListForTable(com.greattimes.ev.indicator.param.req.IndicatorParam param) throws Exception;
}
