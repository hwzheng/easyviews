package test;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.alibaba.fastjson.JSON;
import com.greattimes.ev.indicator.service.ITestService;

import ru.yandex.clickhouse.ClickHouseConnectionImpl;
import ru.yandex.clickhouse.ClickHouseDataSource;

@RunWith(SpringJUnit4ClassRunner.class) //使用junit4进行测试  
@ContextConfiguration(locations={"classpath:spring/spring.xml"}) //加载配置文件   
public class ClickHouseConnect {
		
		@Autowired
		private ITestService service;
	
		@Test
		public void testConnect() throws SQLException{
			ClickHouseDataSource dataSource = new ClickHouseDataSource(
	                "jdbc:clickhouse://192.168.0.158:8123/default?option1=one%20two&option2=y");
	        ClickHouseConnectionImpl connection = (ClickHouseConnectionImpl) dataSource.getConnection();
	        final String sql = "SELECT * FROM preComp_3All_20180322  LIMIT 1";

	        PreparedStatement statement = connection.prepareStatement(sql);
	        ResultSet resultSet = statement.executeQuery();
	        List list=convertList(resultSet);
	        System.out.println(JSON.toJSONString(list));
		}
		
		
		@SuppressWarnings("rawtypes")
		@Test
		public void testMybatis() {
			List list= service.select();
			System.out.println(JSON.toJSONString(list));
		}
	        
	    private static List convertList(ResultSet rs) throws SQLException{
        	List list = new ArrayList();
        	ResultSetMetaData md = rs.getMetaData();//获取键名
        	int columnCount = md.getColumnCount();//获取行的数量
        	while (rs.next()) {
	        	Map rowData = new HashMap();//声明Map
	        	for (int i = 1; i <= columnCount; i++) {
	        		rowData.put(md.getColumnName(i), rs.getObject(i));//获取键名及值
	        	}
	        	list.add(rowData);
        	}
        	return list;
	    }    
}
