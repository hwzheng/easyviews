package com.greattimes.ev.indicator.service;

import com.greattimes.ev.indicator.param.req.OriginalParam;

public interface IOriginalService {

	String originSelect(OriginalParam param);

}
