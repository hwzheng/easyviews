package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 指标图返回结果包装类
 * 
 * @author cgc
 *
 */
@ApiModel
public class IndicatorDataParam implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty("時間数组")
	private List<Long> time;
	@ApiModelProperty("指标数据")
	private List<IndicatorDataDetailParam> indicator;
	public List<Long> getTime() {
		return time;
	}
	public void setTime(List<Long> time) {
		this.time = time;
	}
	public List<IndicatorDataDetailParam> getIndicator() {
		return indicator;
	}
	public void setIndicator(List<IndicatorDataDetailParam> indicator) {
		this.indicator = indicator;
	}
}
