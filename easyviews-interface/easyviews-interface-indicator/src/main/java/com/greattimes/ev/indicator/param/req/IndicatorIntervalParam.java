package com.greattimes.ev.indicator.param.req;

import java.util.List;

/**
 * @author NJ
 * @date 2018/10/15 20:59
 */
public class IndicatorIntervalParam {
    private String table;
    private List<Long> timeList;
    private long start;
    private long end;
    /**
     * 颗粒度 单位s
     */
    private Integer interval;
    /**
     * 颗粒度标志  0 分钟  1 小时  2 5分钟
     */
    private int type;

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<Long> getTimeList() {
        return timeList;
    }

    public void setTimeList(List<Long> timeList) {
        this.timeList = timeList;
    }

    public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    @Override
    public String toString() {
        return "IndicatorIntervalParam{" +
                "table='" + table + '\'' +
                ", timeList=" + timeList +
                ", start=" + start +
                ", end=" + end +
                ", interval=" + interval +
                ", type=" + type +
                '}';
    }
}
