package com.greattimes.ev.indicator.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/4/2 13:14
 */
public class ReportDimensionParam implements Serializable{

    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private List<String> dimensionValues;

	private String dimensionValue;

    private Integer dimensionId;

    private Integer rule;

    private String ename;

    private Integer type;

    private Integer dimensionFlag;

    public List<String> getDimensionValues() {
        return dimensionValues;
    }

    public void setDimensionValues(List<String> dimensionValues) {
        this.dimensionValues = dimensionValues;
    }

    public Integer getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(Integer dimensionId) {
        this.dimensionId = dimensionId;
    }

    public Integer getRule() {
        return rule;
    }

    public void setRule(Integer rule) {
        this.rule = rule;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

    public Integer getDimensionFlag() {
        return dimensionFlag;
    }

    public void setDimensionFlag(Integer dimensionFlag) {
        this.dimensionFlag = dimensionFlag;
    }

    @Override
    public String toString() {
        return "ReportDimensionParam{" +
                "dimensionValues=" + dimensionValues +
                ", dimensionValue='" + dimensionValue + '\'' +
                ", dimensionId=" + dimensionId +
                ", rule=" + rule +
                ", ename='" + ename + '\'' +
                ", type=" + type +
                ", dimensionFlag=" + dimensionFlag +
                '}';
    }


}
