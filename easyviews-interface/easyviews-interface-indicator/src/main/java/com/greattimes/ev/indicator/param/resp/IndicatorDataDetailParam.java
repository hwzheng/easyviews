package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author cgc
 *
 */
@ApiModel
public class IndicatorDataDetailParam implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty("指标id")
	private Integer id;
	@ApiModelProperty("指标名称")
	private String name;
	@ApiModelProperty("单位")
	private String unit;
	@ApiModelProperty("指标维度id")
	private Integer uuid;
	@ApiModelProperty("指标类型")
	private Integer type;
	@ApiModelProperty("指标值数组")
	private List<String> value;
	@ApiModelProperty("满足删选的时间数组")
	private List<Long> abnormalTime;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUuid() {
		return uuid;
	}
	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}
	public List<String> getValue() {
		return value;
	}
	public void setValue(List<String> value) {
		this.value = value;
	}

	public List<Long> getAbnormalTime() {
		return abnormalTime;
	}

	public void setAbnormalTime(List<Long> abnormalTime) {
		this.abnormalTime = abnormalTime;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "IndicatorDataDetailParam{" +
				"id=" + id +
				", name='" + name + '\'' +
				", unit='" + unit + '\'' +
				", uuid=" + uuid +
				", type=" + type +
				", value=" + value +
				", abnormalTime=" + abnormalTime +
				'}';
	}
}
