package com.greattimes.ev.liaoning.service;

import com.greattimes.ev.liaoning.entity.JDetailData;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/3/1 15:48
 */
public interface ILiaoNingEventService {

    /**
     * 初始化接口
     * @param start
     * @return
     */
    Map<String, Object> initQuery(long start);

    /**
     * 详情查询
     * @param timestamp
     * @return
     */
    List<JDetailData> detailQuery(long timestamp);
    /**
     * 告警具体服务详情查询
     * @param timestamp
     * @return
     */
//    List<JDetailData> detailAlarmQuery(long timestamp);

    /**
     * 根据给定时间戳获取服务数
     * @param param
     * @return
     */
    Integer getServiceCountByTime(Map<String, Object> param);
    /**
     * 获取服务数
     * 表存在mysql中
     * @return
     */
    @Deprecated
    Integer getAllServiceNum();
    /**
     * 前n服务数
     * @param time
     * @param topN
     * @return
     */
    List<Map<String,Object>> getServiceTopN(Long time,Integer topN);
    /**
     * 缺少服务数据  tuxedo_queue(msyql)与auto_tuxedoinfo(clickhouse) 表不从属于同一种数据库
     * @param time
     * @return
     */
    @Deprecated
    List<String> getMissService(Long time);
    /**
     * 历史查询
     * @param timestamp
     * @param name
     * @param type
     * @return
     */
    Map<String, Object>  historyQuery(long timestamp, String name, int type);

    Long getRecentAlarm(Map<String, Object> param);

    Long getRecentTotal(Map<String, Object> param);

    List<String> getTuxTqueueRqAddr(long time);

    /**
     * 批量传入测试数据
     * @author NJ
     * @date 2019/12/26 14:12
     * @param list
     * @return boolean
     */
    boolean insertBatchTuxedoinfo(List<Map<String, Object>> list);



    /**
     * 辽宁事件台告警服务信息
     * @param time
     * @author: nj
     * @date: 2020-03-06 10:15
     * @version: 0.0.1
     * @return: java.util.Map<java.lang.String,java.lang.Integer>
     */
    Map<String,Integer> getServicesMap(long time);

}
