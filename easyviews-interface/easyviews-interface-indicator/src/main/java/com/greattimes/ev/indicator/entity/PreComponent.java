package com.greattimes.ev.indicator.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

@TableName("preComp_3All_20180322")
public class PreComponent extends Model<PreComponent> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@TableId(value="date", type= IdType.AUTO)
	private Long date;
	
	private String transactionChannel;
	
	private String tranactionType;
	
	private int allStart2ends;
	
	private int clientIp;
	
	private int serverIp;
	
	private Long time;
	
	private int responseTransCount;
	
	private int serverPort;
	
	private String retCode;
	
	private int successTransCount;
	
	private int allTransCount;
	
	public void setDate(Long date){
	this.date = date;
	}
	public Long getDate(){
	return this.date;
	}
	public void setTransactionChannel(String transactionChannel){
	this.transactionChannel = transactionChannel;
	}
	public String getTransactionChannel(){
	return this.transactionChannel;
	}
	public void setTranactionType(String tranactionType){
	this.tranactionType = tranactionType;
	}
	public String getTranactionType(){
	return this.tranactionType;
	}
	public void setAllStart2ends(int allStart2ends){
	this.allStart2ends = allStart2ends;
	}
	public int getAllStart2ends(){
	return this.allStart2ends;
	}
	public void setClientIp(int clientIp){
	this.clientIp = clientIp;
	}
	public int getClientIp(){
	return this.clientIp;
	}
	public void setServerIp(int serverIp){
	this.serverIp = serverIp;
	}
	public int getServerIp(){
	return this.serverIp;
	}
	public void setTime(Long time){
	this.time = time;
	}
	public Long getTime(){
	return this.time;
	}
	public void setResponseTransCount(int responseTransCount){
	this.responseTransCount = responseTransCount;
	}
	public int getResponseTransCount(){
	return this.responseTransCount;
	}
	public void setServerPort(int serverPort){
	this.serverPort = serverPort;
	}
	public int getServerPort(){
	return this.serverPort;
	}
	public void setRetCode(String retCode){
	this.retCode = retCode;
	}
	public String getRetCode(){
	return this.retCode;
	}
	public void setSuccessTransCount(int successTransCount){
	this.successTransCount = successTransCount;
	}
	public int getSuccessTransCount(){
	return this.successTransCount;
	}
	public void setAllTransCount(int allTransCount){
	this.allTransCount = allTransCount;
	}
	public int getAllTransCount(){
	return this.allTransCount;
	}
	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.date;
	}

}