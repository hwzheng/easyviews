package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 * @date 2019/03/01 19:30
 */
@ApiModel
public class TransactionTpsParam implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * UUID
     */
    @ApiModelProperty("UUID")
    private Integer uuid;
    /**
     * tps峰值
     */
    @ApiModelProperty("tps峰值")
    private Integer maxTps;
	public Integer getUuid() {
		return uuid;
	}
	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}
	public Integer getMaxTps() {
		return maxTps;
	}
	public void setMaxTps(Integer maxTps) {
		this.maxTps = maxTps;
	}
	@Override
	public String toString() {
		return "TpsParam [uuid=" + uuid + ", maxTps=" + maxTps + "]";
	}
	public TransactionTpsParam() {
		super();
	}
	public TransactionTpsParam(Integer uuid) {
		super();
		this.uuid = uuid;
		this.maxTps = null;
	}
    
}
