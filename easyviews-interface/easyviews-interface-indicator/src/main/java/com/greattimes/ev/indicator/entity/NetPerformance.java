package com.greattimes.ev.indicator.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

@TableName("netPerformance")
public class NetPerformance extends Model<NetPerformance> {
	
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String date;
	private Long time;
	private Integer applicationId;
	private Integer componentId;
	private Integer uuid;
	private Long InboundThroughput;
	private Long OutboundThroughput;
	private Long InboundGoodput;
	private Long OutboundGoodput;
	private Long InboundTraffic;
	private Long OutboundTraffic;
	private Long InPayload;
	private Long OutPayload;
	private Long PacketInboundThroughput;
	private Long PacketOutboundThroughput;
	private Long PacketInboundTraffic;
	private Long PacketOutboundTraffic;
	private Long PacketInboundPayload;
	private Long PacketOutboundPayload;
	private Long PacketInboundRetransmissionRate;
	private Long PacketOutboundRetransmissionRate;
	private Long InboundRetransmissionRate;
	private Long OutboundRetransmissionRate;
	private Long InboundPacketLoss;
	private Long OutboundPacketLoss;
	private Long InboundZeroPacket;
	private Long OutboundZeroPacket;
	private Long InboundRTT;
	private Long OutboundRTT;

	public NetPerformance(){
	}

	public NetPerformance(Long time) {
		this.time = time;
		this.InboundThroughput = null;
		this.OutboundThroughput = null;
		this.InboundGoodput = null;
		this.OutboundGoodput = null;
		this.InboundTraffic = null;
		this.OutboundTraffic = null;
		this.InPayload = null;
		this.OutPayload = null;
		this.PacketInboundThroughput = null;
		this.PacketOutboundThroughput = null;
		this.PacketInboundTraffic = null;
		this.PacketOutboundTraffic = null;
		this.PacketInboundPayload = null;
		this.PacketOutboundPayload = null;
		this.PacketInboundRetransmissionRate = null;
		this.PacketOutboundRetransmissionRate = null;
		this.InboundRetransmissionRate = null;
		this.OutboundRetransmissionRate = null;
		this.InboundPacketLoss = null;
		this.OutboundPacketLoss = null;
		this.InboundZeroPacket = null;
		this.OutboundZeroPacket = null;
		this.InboundRTT = null;
		this.OutboundRTT = null;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getUuid() {
		return uuid;
	}

	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}

	public Long getInboundThroughput() {
		return InboundThroughput;
	}

	public void setInboundThroughput(Long inboundThroughput) {
		InboundThroughput = inboundThroughput;
	}

	public Long getOutboundThroughput() {
		return OutboundThroughput;
	}

	public void setOutboundThroughput(Long outboundThroughput) {
		OutboundThroughput = outboundThroughput;
	}

	public Long getInboundGoodput() {
		return InboundGoodput;
	}

	public void setInboundGoodput(Long inboundGoodput) {
		InboundGoodput = inboundGoodput;
	}

	public Long getOutboundGoodput() {
		return OutboundGoodput;
	}

	public void setOutboundGoodput(Long outboundGoodput) {
		OutboundGoodput = outboundGoodput;
	}

	public Long getInboundTraffic() {
		return InboundTraffic;
	}

	public void setInboundTraffic(Long inboundTraffic) {
		InboundTraffic = inboundTraffic;
	}

	public Long getOutboundTraffic() {
		return OutboundTraffic;
	}

	public void setOutboundTraffic(Long outboundTraffic) {
		OutboundTraffic = outboundTraffic;
	}

	public Long getInPayload() {
		return InPayload;
	}

	public void setInPayload(Long inPayload) {
		InPayload = inPayload;
	}

	public Long getOutPayload() {
		return OutPayload;
	}

	public void setOutPayload(Long outPayload) {
		OutPayload = outPayload;
	}

	public Long getPacketInboundThroughput() {
		return PacketInboundThroughput;
	}

	public void setPacketInboundThroughput(Long packetInboundThroughput) {
		PacketInboundThroughput = packetInboundThroughput;
	}

	public Long getPacketOutboundThroughput() {
		return PacketOutboundThroughput;
	}

	public void setPacketOutboundThroughput(Long packetOutboundThroughput) {
		PacketOutboundThroughput = packetOutboundThroughput;
	}

	public Long getPacketInboundTraffic() {
		return PacketInboundTraffic;
	}

	public void setPacketInboundTraffic(Long packetInboundTraffic) {
		PacketInboundTraffic = packetInboundTraffic;
	}

	public Long getPacketOutboundTraffic() {
		return PacketOutboundTraffic;
	}

	public void setPacketOutboundTraffic(Long packetOutboundTraffic) {
		PacketOutboundTraffic = packetOutboundTraffic;
	}

	public Long getPacketInboundPayload() {
		return PacketInboundPayload;
	}

	public void setPacketInboundPayload(Long packetInboundPayload) {
		PacketInboundPayload = packetInboundPayload;
	}

	public Long getPacketOutboundPayload() {
		return PacketOutboundPayload;
	}

	public void setPacketOutboundPayload(Long packetOutboundPayload) {
		PacketOutboundPayload = packetOutboundPayload;
	}

	public Long getPacketInboundRetransmissionRate() {
		return PacketInboundRetransmissionRate;
	}

	public void setPacketInboundRetransmissionRate(Long packetInboundRetransmissionRate) {
		PacketInboundRetransmissionRate = packetInboundRetransmissionRate;
	}

	public Long getPacketOutboundRetransmissionRate() {
		return PacketOutboundRetransmissionRate;
	}

	public void setPacketOutboundRetransmissionRate(Long packetOutboundRetransmissionRate) {
		PacketOutboundRetransmissionRate = packetOutboundRetransmissionRate;
	}

	public Long getInboundRetransmissionRate() {
		return InboundRetransmissionRate;
	}

	public void setInboundRetransmissionRate(Long inboundRetransmissionRate) {
		InboundRetransmissionRate = inboundRetransmissionRate;
	}

	public Long getOutboundRetransmissionRate() {
		return OutboundRetransmissionRate;
	}

	public void setOutboundRetransmissionRate(Long outboundRetransmissionRate) {
		OutboundRetransmissionRate = outboundRetransmissionRate;
	}

	public Long getInboundPacketLoss() {
		return InboundPacketLoss;
	}

	public void setInboundPacketLoss(Long inboundPacketLoss) {
		InboundPacketLoss = inboundPacketLoss;
	}

	public Long getOutboundPacketLoss() {
		return OutboundPacketLoss;
	}

	public void setOutboundPacketLoss(Long outboundPacketLoss) {
		OutboundPacketLoss = outboundPacketLoss;
	}

	public Long getInboundZeroPacket() {
		return InboundZeroPacket;
	}

	public void setInboundZeroPacket(Long inboundZeroPacket) {
		InboundZeroPacket = inboundZeroPacket;
	}

	public Long getOutboundZeroPacket() {
		return OutboundZeroPacket;
	}

	public void setOutboundZeroPacket(Long outboundZeroPacket) {
		OutboundZeroPacket = outboundZeroPacket;
	}

	public Long getInboundRTT() {
		return InboundRTT;
	}

	public void setInboundRTT(Long inboundRTT) {
		InboundRTT = inboundRTT;
	}

	public Long getOutboundRTT() {
		return OutboundRTT;
	}

	public void setOutboundRTT(Long outboundRTT) {
		OutboundRTT = outboundRTT;
	}

	@Override
	public String toString() {
		return "NetPerformance{" +
				"date='" + date + '\'' +
				", time=" + time +
				", applicationId=" + applicationId +
				", componentId=" + componentId +
				", uuid=" + uuid +
				", InboundThroughput=" + InboundThroughput +
				", OutboundThroughput=" + OutboundThroughput +
				", InboundGoodput=" + InboundGoodput +
				", OutboundGoodput=" + OutboundGoodput +
				", InboundTraffic=" + InboundTraffic +
				", OutboundTraffic=" + OutboundTraffic +
				", InPayload=" + InPayload +
				", OutPayload=" + OutPayload +
				", PacketInboundThroughput=" + PacketInboundThroughput +
				", PacketOutboundThroughput=" + PacketOutboundThroughput +
				", PacketInboundTraffic=" + PacketInboundTraffic +
				", PacketOutboundTraffic=" + PacketOutboundTraffic +
				", PacketInboundPayload=" + PacketInboundPayload +
				", PacketOutboundPayload=" + PacketOutboundPayload +
				", PacketInboundRetransmissionRate=" + PacketInboundRetransmissionRate +
				", PacketOutboundRetransmissionRate=" + PacketOutboundRetransmissionRate +
				", InboundRetransmissionRate=" + InboundRetransmissionRate +
				", OutboundRetransmissionRate=" + OutboundRetransmissionRate +
				", InboundPacketLoss=" + InboundPacketLoss +
				", OutboundPacketLoss=" + OutboundPacketLoss +
				", InboundZeroPacket=" + InboundZeroPacket +
				", OutboundZeroPacket=" + OutboundZeroPacket +
				", InboundRTT=" + InboundRTT +
				", OutboundRTT=" + OutboundRTT +
				'}';
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.date;
	}

}