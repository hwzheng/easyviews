package com.greattimes.ev.liaoning.entity;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/3/1 16:07
 */
public class JDetailData implements Serializable{
    private static final long serialVersionUID = 1L;
    private String name;
    private Integer serviceNum;
    private Integer waitNum;
    private boolean isAlarm;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getServiceNum() {
        return serviceNum;
    }

    public void setServiceNum(Integer serviceNum) {
        this.serviceNum = serviceNum;
    }

    public Integer getWaitNum() {
        return waitNum;
    }

    public void setWaitNum(Integer waitNum) {
        this.waitNum = waitNum;
    }

    public boolean isAlarm() {
        return isAlarm;
    }

    public void setAlarm(boolean alarm) {
        isAlarm = alarm;
    }

    @Override
    public String toString() {
        return "JDetailData{" +
                "name='" + name + '\'' +
                ", serviceNum=" + serviceNum +
                ", waitNum=" + waitNum +
                ", isAlarm=" + isAlarm +
                '}';
    }
}
