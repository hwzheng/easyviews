package com.greattimes.ev.indicator.entity;

import java.io.Serializable;
import java.util.Date;


public class BpmExtendTran implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//添加指标需修改
	private Integer applicationId;
	private Long time;
	private Integer uuid;
	private Date date;

	private Integer responseTransCount;
	private Integer responseTransCountCP;
	private Integer successTransCount;
	private Integer successTransCountCP;
	private Integer allStart2ends;
	private Integer allStart2endsCP;
	private Integer allTransCount;
	private Integer allTransCountCP;
	private Double responseTime;
	private Double responseTimeCP;
	private Double responseRate;
	private Double responseRateCP;
	private Double successRate;
	private Double successRateCP;
	private Double money;
	private Double moneyCP;
	private Double businessSuccessRate;
	private Double businessSuccessRateCP;

	private Double transCountPER;
	private Integer transCountDIFF;


	private Double allTransCountPER;
	private Integer allTransCountDIFF;

	private Double responseTransCountPER;
	private Integer responseTransCountDIFF;

	private Double successTransCountPER;
	private Integer successTransCountDIFF;

	private Double responseTimePER;
	private Double responseTimeDIFF;

	private Double responseRatePER;
	private Double responseRateDIFF;

	private Double successRatePER;
	private Double successRateDIFF;


	private Double moneyPER;
	private Double moneyDIFF;

	private Double businessSuccessRatePER;
	private Double businessSuccessRateDIFF;

	private Long timeCP;

	private String name;


	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getUuid() {
		return uuid;
	}

	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}
	public BpmExtendTran() {
		super();
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getResponseTransCount() {
		return responseTransCount;
	}

	public void setResponseTransCount(Integer responseTransCount) {
		this.responseTransCount = responseTransCount;
	}

	public Integer getResponseTransCountCP() {
		return responseTransCountCP;
	}

	public void setResponseTransCountCP(Integer responseTransCountCP) {
		this.responseTransCountCP = responseTransCountCP;
	}

	public Integer getSuccessTransCount() {
		return successTransCount;
	}

	public void setSuccessTransCount(Integer successTransCount) {
		this.successTransCount = successTransCount;
	}

	public Integer getSuccessTransCountCP() {
		return successTransCountCP;
	}

	public void setSuccessTransCountCP(Integer successTransCountCP) {
		this.successTransCountCP = successTransCountCP;
	}

	public Integer getAllStart2ends() {
		return allStart2ends;
	}

	public void setAllStart2ends(Integer allStart2ends) {
		this.allStart2ends = allStart2ends;
	}

	public Integer getAllStart2endsCP() {
		return allStart2endsCP;
	}

	public void setAllStart2endsCP(Integer allStart2endsCP) {
		this.allStart2endsCP = allStart2endsCP;
	}

	public Integer getAllTransCount() {
		return allTransCount;
	}

	public void setAllTransCount(Integer allTransCount) {
		this.allTransCount = allTransCount;
	}

	public Integer getAllTransCountCP() {
		return allTransCountCP;
	}

	public void setAllTransCountCP(Integer allTransCountCP) {
		this.allTransCountCP = allTransCountCP;
	}

	public Double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Double responseTime) {
		this.responseTime = responseTime;
	}

	public Double getResponseTimeCP() {
		return responseTimeCP;
	}

	public void setResponseTimeCP(Double responseTimeCP) {
		this.responseTimeCP = responseTimeCP;
	}

	public Double getResponseRate() {
		return responseRate;
	}

	public void setResponseRate(Double responseRate) {
		this.responseRate = responseRate;
	}

	public Double getResponseRateCP() {
		return responseRateCP;
	}

	public void setResponseRateCP(Double responseRateCP) {
		this.responseRateCP = responseRateCP;
	}

	public Double getSuccessRate() {
		return successRate;
	}

	public void setSuccessRate(Double successRate) {
		this.successRate = successRate;
	}

	public Double getSuccessRateCP() {
		return successRateCP;
	}

	public void setSuccessRateCP(Double successRateCP) {
		this.successRateCP = successRateCP;
	}

	public Double getMoney() {
		return money;
	}

	public void setMoney(Double money) {
		this.money = money;
	}

	public Double getMoneyCP() {
		return moneyCP;
	}

	public void setMoneyCP(Double moneyCP) {
		this.moneyCP = moneyCP;
	}

	public Double getBusinessSuccessRate() {
		return businessSuccessRate;
	}

	public void setBusinessSuccessRate(Double businessSuccessRate) {
		this.businessSuccessRate = businessSuccessRate;
	}

	public Double getBusinessSuccessRateCP() {
		return businessSuccessRateCP;
	}

	public void setBusinessSuccessRateCP(Double businessSuccessRateCP) {
		this.businessSuccessRateCP = businessSuccessRateCP;
	}

	public Double getTransCountPER() {
		return transCountPER;
	}

	public void setTransCountPER(Double transCountPER) {
		this.transCountPER = transCountPER;
	}

	public Integer getTransCountDIFF() {
		return transCountDIFF;
	}

	public void setTransCountDIFF(Integer transCountDIFF) {
		this.transCountDIFF = transCountDIFF;
	}

	public Double getAllTransCountPER() {
		return allTransCountPER;
	}

	public void setAllTransCountPER(Double allTransCountPER) {
		this.allTransCountPER = allTransCountPER;
	}

	public Integer getAllTransCountDIFF() {
		return allTransCountDIFF;
	}

	public void setAllTransCountDIFF(Integer allTransCountDIFF) {
		this.allTransCountDIFF = allTransCountDIFF;
	}

	public Double getResponseTransCountPER() {
		return responseTransCountPER;
	}

	public void setResponseTransCountPER(Double responseTransCountPER) {
		this.responseTransCountPER = responseTransCountPER;
	}

	public Integer getResponseTransCountDIFF() {
		return responseTransCountDIFF;
	}

	public void setResponseTransCountDIFF(Integer responseTransCountDIFF) {
		this.responseTransCountDIFF = responseTransCountDIFF;
	}

	public Double getSuccessTransCountPER() {
		return successTransCountPER;
	}

	public void setSuccessTransCountPER(Double successTransCountPER) {
		this.successTransCountPER = successTransCountPER;
	}

	public Integer getSuccessTransCountDIFF() {
		return successTransCountDIFF;
	}

	public void setSuccessTransCountDIFF(Integer successTransCountDIFF) {
		this.successTransCountDIFF = successTransCountDIFF;
	}

	public Double getResponseTimePER() {
		return responseTimePER;
	}

	public void setResponseTimePER(Double responseTimePER) {
		this.responseTimePER = responseTimePER;
	}

	public Double getResponseTimeDIFF() {
		return responseTimeDIFF;
	}

	public void setResponseTimeDIFF(Double responseTimeDIFF) {
		this.responseTimeDIFF = responseTimeDIFF;
	}

	public Double getResponseRatePER() {
		return responseRatePER;
	}

	public void setResponseRatePER(Double responseRatePER) {
		this.responseRatePER = responseRatePER;
	}

	public Double getResponseRateDIFF() {
		return responseRateDIFF;
	}

	public void setResponseRateDIFF(Double responseRateDIFF) {
		this.responseRateDIFF = responseRateDIFF;
	}

	public Double getSuccessRatePER() {
		return successRatePER;
	}

	public void setSuccessRatePER(Double successRatePER) {
		this.successRatePER = successRatePER;
	}

	public Double getSuccessRateDIFF() {
		return successRateDIFF;
	}

	public void setSuccessRateDIFF(Double successRateDIFF) {
		this.successRateDIFF = successRateDIFF;
	}

	public Double getMoneyPER() {
		return moneyPER;
	}

	public void setMoneyPER(Double moneyPER) {
		this.moneyPER = moneyPER;
	}

	public Double getMoneyDIFF() {
		return moneyDIFF;
	}

	public void setMoneyDIFF(Double moneyDIFF) {
		this.moneyDIFF = moneyDIFF;
	}

	public Double getBusinessSuccessRatePER() {
		return businessSuccessRatePER;
	}

	public void setBusinessSuccessRatePER(Double businessSuccessRatePER) {
		this.businessSuccessRatePER = businessSuccessRatePER;
	}

	public Double getBusinessSuccessRateDIFF() {
		return businessSuccessRateDIFF;
	}

	public void setBusinessSuccessRateDIFF(Double businessSuccessRateDIFF) {
		this.businessSuccessRateDIFF = businessSuccessRateDIFF;
	}

	public Long getTimeCP() {
		return timeCP;
	}

	public void setTimeCP(Long timeCP) {
		this.timeCP = timeCP;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "BpmExtendTran{" +
				"applicationId=" + applicationId +
				", time=" + time +
				", uuid=" + uuid +
				", date=" + date +
				", responseTransCount=" + responseTransCount +
				", responseTransCountCP=" + responseTransCountCP +
				", successTransCount=" + successTransCount +
				", successTransCountCP=" + successTransCountCP +
				", allStart2ends=" + allStart2ends +
				", allStart2endsCP=" + allStart2endsCP +
				", allTransCount=" + allTransCount +
				", allTransCountCP=" + allTransCountCP +
				", responseTime=" + responseTime +
				", responseTimeCP=" + responseTimeCP +
				", responseRate=" + responseRate +
				", responseRateCP=" + responseRateCP +
				", successRate=" + successRate +
				", successRateCP=" + successRateCP +
				", money=" + money +
				", moneyCP=" + moneyCP +
				", businessSuccessRate=" + businessSuccessRate +
				", businessSuccessRateCP=" + businessSuccessRateCP +
				", transCountPER=" + transCountPER +
				", transCountDIFF=" + transCountDIFF +
				", allTransCountPER=" + allTransCountPER +
				", allTransCountDIFF=" + allTransCountDIFF +
				", responseTransCountPER=" + responseTransCountPER +
				", responseTransCountDIFF=" + responseTransCountDIFF +
				", successTransCountPER=" + successTransCountPER +
				", successTransCountDIFF=" + successTransCountDIFF +
				", responseTimePER=" + responseTimePER +
				", responseTimeDIFF=" + responseTimeDIFF +
				", responseRatePER=" + responseRatePER +
				", responseRateDIFF=" + responseRateDIFF +
				", successRatePER=" + successRatePER +
				", successRateDIFF=" + successRateDIFF +
				", moneyPER=" + moneyPER +
				", moneyDIFF=" + moneyDIFF +
				", businessSuccessRatePER=" + businessSuccessRatePER +
				", businessSuccessRateDIFF=" + businessSuccessRateDIFF +
				", timeCP=" + timeCP +
				", name='" + name + '\'' +
				'}';
	}

	public BpmExtendTran(Long time) {
		//添加指标需修改
		super();
		this.time = time;
		this.responseTime = null;
		this.responseRate = null;
		this.successRate = null;
		this.allTransCount=null;
		this.businessSuccessRate=null;
		this.money=null;
	}
	
	public BpmExtendTran(Integer uuid) {
		//添加指标需修改
		super();
		this.uuid = uuid;
		this.responseTime = null;
		this.responseRate = null;
		this.successRate = null;
		this.allTransCount=null;
		this.businessSuccessRate=null;
		this.money=null;
	}

}