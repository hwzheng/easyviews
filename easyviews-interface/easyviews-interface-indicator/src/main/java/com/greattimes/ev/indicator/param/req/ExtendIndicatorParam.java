package com.greattimes.ev.indicator.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author NJ
 * @date 2018/9/26 15:29
 */
@ApiModel
public class ExtendIndicatorParam implements Serializable{

    private static final long serialVersionUID = -5295604994500088318L;
    /**
     * 数据源id
     */
    private Integer sourcesId;
    /**
     * 颗粒度
     */
    private Integer interval;
    /**
     * 颗粒度
     */
    private Integer groupId;

    /**
     * 数据源唯一id
     */
    @ApiModelProperty("数据源唯一id")
    private List<String> innerUUID;
    /**
     * 开始时间戳
     */
    @ApiModelProperty("开始时间戳")
    private Long start;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long end;
    /**
     * 指标id
     */
    @ApiModelProperty("指标id")
    private List<Integer> indicator;
	public Integer getSourcesId() {
		return sourcesId;
	}
	public void setSourcesId(Integer sourcesId) {
		this.sourcesId = sourcesId;
	}
	public List<String> getInnerUUID() {
		return innerUUID;
	}
	public void setInnerUUID(List<String> innerUUID) {
		this.innerUUID = innerUUID;
	}
	public Long getStart() {
		return start;
	}
	public void setStart(Long start) {
		this.start = start;
	}
	public Long getEnd() {
		return end;
	}
	public void setEnd(Long end) {
		this.end = end;
	}
	public List<Integer> getIndicator() {
		return indicator;
	}
	public void setIndicator(List<Integer> indicator) {
		this.indicator = indicator;
	}
	public Integer getInterval() {
		return interval;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}


}
