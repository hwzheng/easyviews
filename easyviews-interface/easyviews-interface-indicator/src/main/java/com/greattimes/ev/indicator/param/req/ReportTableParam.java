package com.greattimes.ev.indicator.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/4/15 10:29
 */
@ApiModel
public class ReportTableParam implements Serializable{
    private static final long serialVersionUID = 3758659164231103246L;
	@ApiModelProperty("应用id")
    Integer applicationId;
    /**
     * 1、应用指标2、应用维度
     * 3、事件指标4、事件维度
     */
    @ApiModelProperty("数据类型")
    private Integer dataType;
    @ApiModelProperty("颗粒度类型 0 正常 1 广发")
    private Integer intervalType;
    @ApiModelProperty("查询维度uuid")
    private List<Integer> uuids;
    @ApiModelProperty("颗粒度")
    private Integer interval;
    @ApiModelProperty("开始时间戳")
    private Long startTime;
    @ApiModelProperty("结束时间戳")
    private Long endTime;
    @ApiModelProperty("0 无对，1 上周期 2 自定义")
    private Integer compare;
    @ApiModelProperty("对比的开始时间")
    private Long compareStartTime;
    @ApiModelProperty("对比的结束时间")
    private Long compareEndTime;
    @ApiModelProperty("是否时间维度 0 否 1 是")
    private Integer isTimeDimension;
    /**
     * ename
     * sortType
	 * orderType 排序字段类型 0 时间戳 1 指标 2 维度 3 对比指标  4对比百分比 5 对比差值
     */
    @ApiModelProperty("是否时间维度 0 否 1 是")
    private JSONObject order;
    /**
     * 维度列数组
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    @ApiModelProperty("维度列数组")
    private List<ReportDimensionParam> dimensions;
    /**
     * 维度筛选条件
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    @ApiModelProperty("维度筛选条件")
    private List<ReportDimensionParam> rules;
    /**
     * 指标筛选
     *    indicatorId	int	是	指标id
     *    rule	int	是	指标筛选规则（1大于，2小于，3等于，4不等于）
     *    value	Num	是	值
     */
    @ApiModelProperty("指标筛选")
    private List<JSONObject> filterIndicator;
    /**
     * 每页数量
     */
    @ApiModelProperty("每页数量")
    private Integer size;
    @ApiModelProperty("当前页")
    private Integer page;
    /**
     * 时间集合 
     */
    private List<Map<String,Object>> timePart;
    /**
     * 时间集合 
     */
    private List<Map<String,Object>> timeCompare;
	/**
	 * 名称map
	 */
	private Map<Integer, String> nameMap;

	/**
	 * 限制条数
	 */
	private Integer limit;
	/**
	 * 下载需要的表头
	 * headers
	 *	name
	 *	value
	 */
	private List<JSONObject> headers;

	public Map<Integer, String> getNameMap() {
		return nameMap;
	}

	public void setNameMap(Map<Integer, String> nameMap) {
		this.nameMap = nameMap;
	}

	public Integer getDataType() {
		return dataType;
	}
	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}
	public List<Integer> getUuids() {
		return uuids;
	}
	public void setUuids(List<Integer> uuids) {
		this.uuids = uuids;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getInterval() {
		return interval;
	}
	public void setInterval(Integer interval) {
		this.interval = interval;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getEndTime() {
		return endTime;
	}
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
	public Integer getCompare() {
		return compare;
	}
	public void setCompare(Integer compare) {
		this.compare = compare;
	}
	public Long getCompareStartTime() {
		return compareStartTime;
	}
	public void setCompareStartTime(Long compareStartTime) {
		this.compareStartTime = compareStartTime;
	}
	public Long getCompareEndTime() {
		return compareEndTime;
	}
	public void setCompareEndTime(Long compareEndTime) {
		this.compareEndTime = compareEndTime;
	}
	public Integer getIsTimeDimension() {
		return isTimeDimension;
	}
	public void setIsTimeDimension(Integer isTimeDimension) {
		this.isTimeDimension = isTimeDimension;
	}
	public JSONObject getOrder() {
		return order;
	}
	public void setOrder(JSONObject order) {
		this.order = order;
	}
	public List<ReportDimensionParam> getDimensions() {
		return dimensions;
	}
	public void setDimensions(List<ReportDimensionParam> dimensions) {
		this.dimensions = dimensions;
	}
	public List<ReportDimensionParam> getRules() {
		return rules;
	}
	public void setRules(List<ReportDimensionParam> rules) {
		this.rules = rules;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public List<Map<String, Object>> getTimePart() {
		return timePart;
	}
	public void setTimePart(List<Map<String, Object>> timePart) {
		this.timePart = timePart;
	}
	public List<Map<String, Object>> getTimeCompare() {
		return timeCompare;
	}
	public void setTimeCompare(List<Map<String, Object>> timeCompare) {
		this.timeCompare = timeCompare;
	}

	public List<JSONObject> getFilterIndicator() {
		return filterIndicator;
	}

	public void setFilterIndicator(List<JSONObject> filterIndicator) {
		this.filterIndicator = filterIndicator;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public List<JSONObject> getHeaders() {
		return headers;
	}

	public void setHeaders(List<JSONObject> headers) {
		this.headers = headers;
	}

	public Integer getIntervalType() {
		return intervalType;
	}

	public void setIntervalType(Integer intervalType) {
		this.intervalType = intervalType;
	}

	@Override
	public String toString() {
		return "ReportTableParam [applicationId=" + applicationId + ", dataType=" + dataType + ", intervalType="
				+ intervalType + ", uuids=" + uuids + ", interval=" + interval + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", compare=" + compare + ", compareStartTime=" + compareStartTime
				+ ", compareEndTime=" + compareEndTime + ", isTimeDimension=" + isTimeDimension + ", order=" + order
				+ ", dimensions=" + dimensions + ", rules=" + rules + ", filterIndicator=" + filterIndicator + ", size="
				+ size + ", page=" + page + ", timePart=" + timePart + ", timeCompare=" + timeCompare + ", nameMap="
				+ nameMap + ", limit=" + limit + ", headers=" + headers + "]";
	}



}
