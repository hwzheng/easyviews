package com.greattimes.ev.indicator.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author cgc
 * @date 2019/03/01 19:30
 */
@ApiModel
public class DimensionPageParam implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 查询维度uuid
     */
    @ApiModelProperty("查询维度uuid")
    private Long uuid;
    
    /**
     * 1 组件 2 业务
     */
    @ApiModelProperty("应用id")
    private Integer  applicationId;

    /**
     * 1 组件 2 业务
     */
    @ApiModelProperty("1 组件 2 业务")
    private Integer  type;

    /**
     * 开始时间戳
     */
    @ApiModelProperty("开始时间戳")
    private Long  startTime;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long  endTime;
    /**
     * 维度值id
     */
    @ApiModelProperty("维度列")
    private List<JSONObject> dimensionColumns;
    /**
     * 指标id
     */
    @ApiModelProperty("指标id")
    private List<Integer> indicatorIds;
    /**
     * 指标筛选
     *    indicatorId	int	是	指标id
     *    rule	int	是	指标筛选规则（1大于，2小于，3等于，4不等于）
     *    value	Num	是	值
     */
    @ApiModelProperty("指标筛选")
    private List<JSONObject> filterIndicator;

    /**
     * 维度筛选
     *    dimensionId	int	是	维度值id
     *    rule	int	是	维度筛选规则（1包含，2不包含，3等于，4不等于）
     *    value	string	是	值
     */
    @ApiModelProperty("维度筛选")
    private List<JSONObject> filterDimension;

    /**
     * 对比日期 20180926
     */
    @ApiModelProperty("对比日期 20180926")
    private String compareData;
    
    /**
     * 排序指标
     */
    @ApiModelProperty("排序指标")
    private JSONObject order;
    
    /**
     * 每页数量
     */
    @ApiModelProperty("每页数量")
    private Integer  size;
    
    /**
     * 当前页码
     */
    @ApiModelProperty("当前页码 ")
    private Integer  page;
    
    @ApiModelProperty("表头")
    private List<JSONObject> header;

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }



    public List<JSONObject> getDimensionColumns() {
		return dimensionColumns;
	}

	public void setDimensionColumns(List<JSONObject> dimensionColumns) {
		this.dimensionColumns = dimensionColumns;
	}

	public List<JSONObject> getFilterIndicator() {
		return filterIndicator;
	}

	public void setFilterIndicator(List<JSONObject> filterIndicator) {
		this.filterIndicator = filterIndicator;
	}

	public List<JSONObject> getFilterDimension() {
		return filterDimension;
	}

	public void setFilterDimension(List<JSONObject> filterDimension) {
		this.filterDimension = filterDimension;
	}

	public String getCompareData() {
        return compareData;
    }

    public void setCompareData(String compareData) {
        this.compareData = compareData;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public List<Integer> getIndicatorIds() {
		return indicatorIds;
	}

	public void setIndicatorIds(List<Integer> indicatorIds) {
		this.indicatorIds = indicatorIds;
	}

	public JSONObject getOrder() {
		return order;
	}

	public void setOrder(JSONObject order) {
		this.order = order;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public List<JSONObject> getHeader() {
		return header;
	}

	public void setHeader(List<JSONObject> header) {
		this.header = header;
	}

	@Override
	public String toString() {
		return "DimensionPageParam [uuid=" + uuid + ", applicationId=" + applicationId + ", type=" + type
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", dimensionColumns=" + dimensionColumns
				+ ", indicatorIds=" + indicatorIds + ", filterIndicator=" + filterIndicator + ", filterDimension="
				+ filterDimension + ", compareData=" + compareData + ", order=" + order + ", size=" + size + ", page="
				+ page + "]";
	}

}
