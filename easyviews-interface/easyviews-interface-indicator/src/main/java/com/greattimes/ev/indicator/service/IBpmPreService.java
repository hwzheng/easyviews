package com.greattimes.ev.indicator.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.indicator.param.req.DimensionPageParam;
import com.greattimes.ev.indicator.param.req.DimensionParam;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportChartParam;
import com.greattimes.ev.indicator.param.req.ReportIndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportTableParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.ReportDataChartParam;
import com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam;

/**
 * @author NJ
 * @date 2018/9/26 21:02
 */
public interface IBpmPreService {
    /**
     * 10.2.1图表类接口
     * @param param
     * @return
     */
    Map<String, Object> selectListForChart(DimensionParam param);

    /**
     * 10.2.2表格类接口
     * @param param
     * @param fixed_dimension 预处理维度的非json字段
     * @return
     */
    List<Map<String, Object>> selectListForTable(DimensionParam param, List<String> fixed_dimension);

    /**
     * 创建表  bpmPre_ && extendPre_
     * @author NJ
     * @date 2018/10/15 10:18
     * @param componentId
     * @param columns
     * @param url
     * @return
     */
    boolean addPerTable(int componentId, List<String> columns,String url);
    
    /**
     * 创建表  bpmPre_ && extendPre_的视图
     * @param id
     * @param columns
     * @param type 1:bpmPre_  2:extendPre_
     * @return
     */
   // boolean createPerView(int id, List<String> columns, int type);
    /**
     * 给表增加列
     * @param table
     * @param columns
     * @param host
     * @param url
     * @return
     */
    boolean updatePerTable(String table, List<String> columns, String host,String url);
    
    /**判断表是否存在
     * @param table
     * @param host
     * @param url
     * @return
     */
    boolean isExist(String table, String host,String url);
    /**
     * 查询指标数据
     * @param param
     * @param interval 
     * @return
     */
    @Deprecated
    IndicatorDataParam getIndicatorBydimension(IndicatorParam param, int interval);

	void dropTable(String tableName, String host,String url);

	/**创建物化视图
	 * @param componentId
	 * @param columns
	 * @param url
	 * @return
	 */
	boolean createPerMaterializedView(int componentId, List<String> columns, String url);

	/**
	 * 创建小时视图
	 * @param id
	 * @param columns
	 * @param i 1:bpmPre_  2:extendPre_
	 * @return
	 */
	//boolean createPerHouer(int id, List<String> columns, int i);

	/**分离视图
	 * @param string
	 * @param host
	 * @param url
	 */
	void detachTable(String string, String host, String url);

	/**更新视图
	 * @param table
	 * @param columns
	 * @param host
	 * @param url
	 * @return
	 */
	boolean updateView(String table, List<String> columns, String host,String url);

	/**重新绑定视图
	 * @param id
	 * @param enameList
	 * @param host
	 * @param url
	 * @return
	 */
	boolean AttachView(List<String> enameList, String host,String url);

	/**获取最大最小值
	 * @param customId
	 * @param startDate
	 * @param endDate
	 * @param type  1 bpm 2事件
	 * @return
	 */
	List<Map<String, Object>> getMaxAndMinData(int customId, long startDate, long endDate, int type);

	/**和表字段比较-是否有多的字段
	 * @param table
	 * @param columns
	 * @param host
	 * @param url
	 * @return
	 */
	boolean compareWithTable(String table, List<String> columns, String host,String url);
	/**查维度占比
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> selectAllTransCountPercent(Map<String, Object> selectMap);

	/**瓶颈成功、失败占比
	 * @param selectMap 
	
	 * @return
	 */
	//List<Map<String, Object>> selectBpmSuccessAndfailPercent(List<Map<String, Object>> datelist, String table);
	List<Map<String, Object>> selectBpmSuccessAndfailPercent(Map<String, Object> selectMap);
	/**瓶颈成功、失败占比
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> selectExtendSuccessAndfailPercent(Map<String, Object> selectMap);

	/**数值类型最大最小平均值
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> getIndicatorMaxMinAvg(Map<String, Object> selectMap);

	/**瓶颈表格数据 bpm
	 * @param tableMap
	 * @return
	 */
	List<Map<String, Object>> getBpmTableData(Map<String, Object> map);
	/**瓶颈表格数据 extend
	 * @param tableMap
	 * @return
	 */
	List<Map<String, Object>> getExtendTableData(Map<String, Object> map);

	/**获取维度值
	 * @param selectMap
	 * @return
	 */
	List<Map<String, Object>> getDifTradeType(Map<String, Object> map);

	/**瓶颈表格数据行数
	 * @param map
	 * @return
	 */
	int getTableDataCount(Map<String, Object> map);

	/**将原表中多的维度列加入list
	 * @param componentId
	 * @param enameList
	 * @param host
	 * @param url
	 * @return
	 */
	List<String> addPerTableColumns(int componentId, List<String> enameList, String host, String url);

	/**
	 * 测试clickhouse创建表
	 * @author NJ
	 * @date 2018/12/21 15:04
	 * @param
	 * @return boolean
	 */
	//boolean testCreateHouseTable();

	List<Map<String, Object>> selectCustomAllTransCountPercent(Map<String, Object> selectMap);

	int getExtendTableDataCount(Map<String, Object> tableMap);

	/**
	 * @param componentId  该参数已弃用
	 * @param columns 该参数已弃用
	 * @param host
	 * @param url
	 */
	void createVmTable(int componentId, List<String> columns, String host,String url);

	List<Map<String, Object>> getIndicatorMaxMinAvgPercent(Map<String, Object> selectMap);

	Page<Map<String, Object>> selectTablePage(Page<Map<String, Object>> page, DimensionPageParam param, List<String> fixed_dimension);

	boolean CreateClusterTable(List<String> columns, String url);

	List<Map<String, Object>> getTopChartData(ReportChartParam re);

	/**
	 * 智能报表-获取应用维度(折线图,统计图,数值图)
	 * @author NJ
	 * @date 2019/4/2 20:38
	 * @param param
	 * @return com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam
	 */
	ReportLineIndicatorParam getReportPreTranIndicator(ReportIndicatorParam param);

	/**
	 *  智能报表-获取事件维度数据(折线图,统计图,数值图)
	 *  单事件,单维度,多维度值,多指标
	 * @author NJ
	 * @date 2019/4/9 15:16
	 * @param param
	 * @return com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam
	 */
	ReportLineIndicatorParam getReportExtendPreTranIndicator(ReportIndicatorParam param);

	/**
	 * 智能报表-表格
	 * @param page
	 * @param param
	 * @param fixed_dimension
	 * @return
	 */
	Page<Map<String, Object>> selectReportTablePage(Page<Map<String, Object>> page, ReportTableParam param) ;

	/**
	 * 智能报表-数值图
	 * @param re
	 * @return
	 */
	ReportDataChartParam getDataChart(ReportChartParam re);

	/** 智能报表-表格
	 * @author CGC
	 * @param param
	 * @return
	 */
	List<Map<String, Object>> selectReportTable(ReportTableParam param);

	Map<String, Object> selectAnalyzeListForChart(DimensionParam param);


}
