package com.greattimes.ev.indicator.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/9/26 19:30
 */
@ApiModel
public class DimensionChartParam implements Serializable{

    private static final long serialVersionUID = 74842112462251959L;

    /**
     * 查询维度uuid
     */
    @ApiModelProperty("查询维度uuid")
    private Long uuid;
    
    /**
     * 1 组件 2 业务
     */
    @ApiModelProperty("应用id")
    private Integer  applicationId;

    /**
     * 1 组件 2 业务
     */
    @ApiModelProperty("1 组件 2 业务")
    private Integer  type;

    /**
     * 开始时间戳
     */
    @ApiModelProperty("开始时间戳")
    private Long  startTime;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long  endTime;
    /**
     * 对比开始时间戳
     */
    @ApiModelProperty("对比开始时间戳")
    private Long  compareStartTime;
    /**
     * 对比结束时间戳
     */
    @ApiModelProperty("对比结束时间戳")
    private Long  compareEndTime;
    /**
     * 指标id
     */
    @ApiModelProperty("指标id")
    private List<Integer> indicatorIds;
    /**
     * 所选数据
     */
    @ApiModelProperty("所选数据")
    private List<JSONObject> choose;
	public Long getUuid() {
		return uuid;
	}
	public void setUuid(Long uuid) {
		this.uuid = uuid;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getEndTime() {
		return endTime;
	}
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
	public Long getCompareStartTime() {
		return compareStartTime;
	}
	public void setCompareStartTime(Long compareStartTime) {
		this.compareStartTime = compareStartTime;
	}
	public Long getCompareEndTime() {
		return compareEndTime;
	}
	public void setCompareEndTime(Long compareEndTime) {
		this.compareEndTime = compareEndTime;
	}
	public List<Integer> getIndicatorIds() {
		return indicatorIds;
	}
	public void setIndicatorIds(List<Integer> indicatorIds) {
		this.indicatorIds = indicatorIds;
	}
	public List<JSONObject> getChoose() {
		return choose;
	}
	public void setChoose(List<JSONObject> choose) {
		this.choose = choose;
	}


}
