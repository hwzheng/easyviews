package com.greattimes.ev.indicator.service;

import java.util.List;

import com.greattimes.ev.indicator.param.resp.DashBoardParam;

public interface IDashBoardService {

	/**
	 * 监控台数据查询
	 * @author cgc  
	 * @date 2018年10月9日  
	 * @param ids
	 * @param start
	 * @param end
	 * @return
	 */
	DashBoardParam getDashBoardData(List<Integer> ids, Long start, Long end);
}
