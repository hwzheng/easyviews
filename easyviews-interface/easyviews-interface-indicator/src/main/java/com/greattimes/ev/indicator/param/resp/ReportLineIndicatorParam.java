package com.greattimes.ev.indicator.param.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/4/1 19:37
 */
@ApiModel
public class ReportLineIndicatorParam  implements Serializable{

    private static final long serialVersionUID = -557221678725209036L;

    @ApiModelProperty("时间数组")
    private List<Long> time;
    @ApiModelProperty("指标数据")
    private List<ReportIndicatorDetailParam> indicators;
    @ApiModelProperty("事件id")
    private Integer customId;
    public List<ReportIndicatorDetailParam> getIndicators() {
        return indicators;
    }
    public void setIndicators(List<ReportIndicatorDetailParam> indicators) {
        this.indicators = indicators;
    }
    public List<Long> getTime() {
        return time;
    }
    public void setTime(List<Long> time) {
        this.time = time;
    }
    public Integer getCustomId() {
        return customId;
    }
    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

}
