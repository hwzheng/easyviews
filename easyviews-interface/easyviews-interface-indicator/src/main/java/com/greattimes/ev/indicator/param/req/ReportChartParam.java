package com.greattimes.ev.indicator.param.req;


import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author NJ
 * @date 2019/3/28 13:08
 */
public class ReportChartParam implements Serializable{
    private static final long serialVersionUID = 8439758096120126420L;

    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 图标类型 1 折线 ，2 柱状 ，3 ，饼图 ，4 ，表格，5  柱状top
     */
    private Integer type;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 时间类型 1 动态时间  2 静态时间
     */
    private Integer timeType;
    /**
     * 动态时间  0 今日， 1昨日   ，2上周  ，3上月   ，4过去7天，5过去14天，6过去30天，7过去180天
     */
    private Integer dynTime;
    /**
     * 对比  0 无对比 ， 1 上周期
     */
    private Integer compare;
    /**
     * 限制条数
     */
    private Integer limit;
    /**
     * 排序方式 0 升序 1 降序
     */
    private Integer sortType;
    /**
     * 共享人员的id
     */
    private List<Integer> userIds;

    /**
     * 维度数组
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    private List<ReportDimensionParam> dimensions;
    /**
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    private List<ReportDimensionParam> rules;
    /**
     * 指标筛选
     *    indicatorId	int	是	指标id
     *    rule	int	是	指标筛选规则（1大于，2小于，3等于，4不等于）
     *    value	Num	是	值
     */
    @ApiModelProperty("指标筛选")
    private List<JSONObject> filterIndicator;
    /**
     * 指标id数组
     */
    private List<Integer> indicators;
    /**
     * 数据配置表
     */
    private List<ReportChartDataParam> chartData;
    /**
     * 数据颗粒度
     */
    private Integer interval;
    /**
     * 0 正常 1 证券类
     */
    private Integer intervalType;
    /**
     * 数据类型 1 应用 2 事件 3 漏斗
     */
    private Integer dataType;
    /**
     * 是否维度 0 否 1是
     */
    private Integer isDimension;
    /**
     * 报表创建人
     */
    private Integer createBy;
    /**
     * 动态开始时间
     */
    private Date start;
    /**
     * 动态结束时间
     */
    private Date end;
    
    /**
     * 时间集合 
     */
    private List<Map<String,Object>> timePart;
    /**
     * 时间集合 
     */
    private List<Map<String,Object>> timeCompare;
    /**
     * 选中节点集合
     */
    private List<Map<String,Object>> uuids;
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getDynTime() {
        return dynTime;
    }

    public void setDynTime(Integer dynTime) {
        this.dynTime = dynTime;
    }

    public Integer getCompare() {
        return compare;
    }

    public void setCompare(Integer compare) {
        this.compare = compare;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getSortType() {
        return sortType;
    }

    public void setSortType(Integer sortType) {
        this.sortType = sortType;
    }

    public List<Integer> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Integer> userIds) {
        this.userIds = userIds;
    }

    public List<ReportDimensionParam> getDimensions() {
        return dimensions;
    }

    public void setDimensions(List<ReportDimensionParam> dimensions) {
        this.dimensions = dimensions;
    }

    public List<ReportDimensionParam> getRules() {
        return rules;
    }

    public void setRules(List<ReportDimensionParam> rules) {
        this.rules = rules;
    }

    public List<Integer> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Integer> indicators) {
        this.indicators = indicators;
    }

    public List<ReportChartDataParam> getChartData() {
        return chartData;
    }

    public void setChartData(List<ReportChartDataParam> chartData) {
        this.chartData = chartData;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getIsDimension() {
        return isDimension;
    }

    public void setIsDimension(Integer isDimension) {
        this.isDimension = isDimension;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Date getStart() {
        return start;
    }


    public Date getEnd() {
        return end;
    }


    public List<JSONObject> getFilterIndicator() {
		return filterIndicator;
	}

	public void setFilterIndicator(List<JSONObject> filterIndicator) {
		this.filterIndicator = filterIndicator;
	}

	public List<Map<String, Object>> getTimePart() {
		return timePart;
	}

	public void setTimePart(List<Map<String, Object>> timePart) {
		this.timePart = timePart;
	}

	public List<Map<String, Object>> getTimeCompare() {
		return timeCompare;
	}

	public void setTimeCompare(List<Map<String, Object>> timeCompare) {
		this.timeCompare = timeCompare;
	}

	public List<Map<String, Object>> getUuids() {
		return uuids;
	}

	public void setUuids(List<Map<String, Object>> uuids) {
		this.uuids = uuids;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Integer getIntervalType() {
		return intervalType;
	}

	public void setIntervalType(Integer intervalType) {
		this.intervalType = intervalType;
	}

	@Override
	public String toString() {
		return "ReportChartParam [id=" + id + ", name=" + name + ", type=" + type + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", timeType=" + timeType + ", dynTime=" + dynTime + ", compare=" + compare
				+ ", limit=" + limit + ", sortType=" + sortType + ", userIds=" + userIds + ", dimensions=" + dimensions
				+ ", rules=" + rules + ", filterIndicator=" + filterIndicator + ", indicators=" + indicators
				+ ", chartData=" + chartData + ", interval=" + interval + ", intervalType=" + intervalType
				+ ", dataType=" + dataType + ", isDimension=" + isDimension + ", createBy=" + createBy + ", start="
				+ start + ", end=" + end + ", timePart=" + timePart + ", timeCompare=" + timeCompare + ", uuids="
				+ uuids + "]";
	}

}
