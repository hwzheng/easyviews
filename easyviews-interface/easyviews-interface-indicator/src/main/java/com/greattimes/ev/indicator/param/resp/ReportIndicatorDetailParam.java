package com.greattimes.ev.indicator.param.resp;

import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/4/1 21:17
 */
public class ReportIndicatorDetailParam implements Serializable{
    private static final long serialVersionUID = -6110061639361301961L;
    @ApiModelProperty("指标id")
    private Integer id;
    @ApiModelProperty("指标名称")
    private String name;
    @ApiModelProperty("单位")
    private String unit;
    @ApiModelProperty("维度id")
    private Integer uuid;
    @ApiModelProperty("指标类型")
    private Integer type;
    @ApiModelProperty("指标数据")
    private List<String> value;
    @ApiModelProperty("对比数据")
    private List<String> valueCP;
    @ApiModelProperty("线的标识")
    private String seriesName;
    @ApiModelProperty("维度值")
    private String dimensionValue;

    public String getDimensionValue() {
        return dimensionValue;
    }

    public void setDimensionValue(String dimensionValue) {
        this.dimensionValue = dimensionValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public List<String> getValueCP() {
        return valueCP;
    }

    public void setValueCP(List<String> valueCP) {
        this.valueCP = valueCP;
    }

    public String getSeriesName() {
        return seriesName;
    }

    public void setSeriesName(String seriesName) {
        this.seriesName = seriesName;
    }

    @Override
    public String toString() {
        return "ReportIndicatorDetailParam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", unit='" + unit + '\'' +
                ", uuid=" + uuid +
                ", type=" + type +
                ", value=" + value +
                ", valueCP=" + valueCP +
                ", seriesName='" + seriesName + '\'' +
                ", dimensionValue='" + dimensionValue + '\'' +
                '}';
    }
}
