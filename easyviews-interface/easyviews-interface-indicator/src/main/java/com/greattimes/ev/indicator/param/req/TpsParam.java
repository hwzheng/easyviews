package com.greattimes.ev.indicator.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 * @date 2019/03/01 19:30
 */
@ApiModel
public class TpsParam implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * UUID
     */
    @ApiModelProperty("UUID")
    private List<Integer> uuid;
    /**
     * 开始时间戳
     */
    @ApiModelProperty("开始时间戳")
    private Long start;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long end;
    
    /**
     * 请求标志 1组件 2事件
     */
    @ApiModelProperty("请求标志 1组件 2事件")
    private Integer type;

	public List<Integer> getUuid() {
		return uuid;
	}

	public void setUuid(List<Integer> uuid) {
		this.uuid = uuid;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "TpsParam [uuid=" + uuid + ", start=" + start + ", end=" + end + ", type=" + type + "]";
	}
    
}
