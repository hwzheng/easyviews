package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
/**
 * @author cgc
 *
 */
@ApiModel
public class ExtendIndicatorDataDetailParam implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty("指标id")
	private Integer id;
	@ApiModelProperty("数据源唯一id")
	private Integer innerUUID;
	@ApiModelProperty("指标值数组")
	private List<String> value;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getInnerUUID() {
		return innerUUID;
	}
	public void setInnerUUID(Integer innerUUID) {
		this.innerUUID = innerUUID;
	}
	public List<String> getValue() {
		return value;
	}
	public void setValue(List<String> value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "ExtendIndicatorDataDetailParam [id=" + id + ", innerUUID=" + innerUUID + ", value=" + value + "]";
	}
	
}
