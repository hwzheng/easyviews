package com.greattimes.ev.indicator.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("bpmFCS")
public class BpmFCS extends Model<BpmFCS> {
	private static final long serialVersionUID = 1L;
	private Date date;
	private Long time;
	private Integer uuid;
	private Integer applicationId;
	private Integer level;
	private Integer componentId;
	private Integer extendId;
	private Integer productType;
	private Integer indicatorid;
	private Double fcsdata;
	private Double factor;
	private Integer model;


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Long getTime() {
		return time;
	}


	public void setTime(Long time) {
		this.time = time;
	}


	public Integer getUuid() {
		return uuid;
	}


	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}


	public Integer getApplicationId() {
		return applicationId;
	}


	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}


	public Integer getLevel() {
		return level;
	}


	public void setLevel(Integer level) {
		this.level = level;
	}


	public Integer getComponentId() {
		return componentId;
	}


	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}


	public Integer getExtendId() {
		return extendId;
	}


	public void setExtendId(Integer extendId) {
		this.extendId = extendId;
	}


	public Integer getProductType() {
		return productType;
	}


	public void setProductType(Integer productType) {
		this.productType = productType;
	}


	public Integer getIndicatorid() {
		return indicatorid;
	}


	public void setIndicatorid(Integer indicatorid) {
		this.indicatorid = indicatorid;
	}


	public Double getFcsdata() {
		return fcsdata;
	}


	public void setFcsdata(Double fcsdata) {
		this.fcsdata = fcsdata;
	}


	public Double getFactor() {
		return factor;
	}


	public void setFactor(Double factor) {
		this.factor = factor;
	}


	public Integer getModel() {
		return model;
	}


	public void setModel(Integer model) {
		this.model = model;
	}


	public BpmFCS() {
		super();
	}


	public BpmFCS(Long time, Integer uuid, Integer indicator) {
		super();
		this.time = time;
		this.uuid=uuid;
		this.indicatorid=indicator;
		this.fcsdata=null;
	}


	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}

}
