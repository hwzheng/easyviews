package com.greattimes.ev.indicator.param.req;

import java.io.Serializable;
import java.util.Date;

public class OriginalParam implements Serializable {
	private static final long serialVersionUID = 1L;
	private String date; 
	private Long timestamp;
	private String uuid;
	private Integer applicationId;
	private Integer componentId;
	private Integer type;
	private String charset;
	private Integer asyn;
	public Long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public Integer getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public Integer getAsyn() {
		return asyn;
	}
	public void setAsyn(Integer asyn) {
		this.asyn = asyn;
	}
	@Override
	public String toString() {
		return "OriginalParam [date=" + date + ", timestamp=" + timestamp + ", uuid=" + uuid + ", applicationId="
				+ applicationId + ", componentId=" + componentId + ", type=" + type + ", charset=" + charset + ", asyn="
				+ asyn + "]";
	}


}
