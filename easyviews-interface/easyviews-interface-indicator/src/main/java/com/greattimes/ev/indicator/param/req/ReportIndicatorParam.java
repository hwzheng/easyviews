package com.greattimes.ev.indicator.param.req;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/4/1 19:50
 */
public class ReportIndicatorParam implements Serializable{
    private static final long serialVersionUID = 5485437118413413572L;

    /**
     * 图标类型 1 折线 ，2 柱状 ，3 ，饼图 ，4 ，表格，5  柱状top， 6  统计折线图
     */
    private Integer type;
    /**
     * 开始时间
     */
    private Long startTime;
    /**
     * 结束时间
     */
    private Long endTime;
    /**
     * 对比的开始时间
     */
    private Long compareStartTime;
    /**
     * 对比的结束时间
     */
    private Long compareEndTime;
    /**
     * 颗粒度类型 0 正常 1 广发
     */
    private Integer intervalType;

    /**
     * 时间集合
     */
    private List<Map<String,Object>> timePart;
    /**
     * 对比时间集合
     */
    private List<Map<String,Object>> timeCompare;

    /**
     * 时间类型 1 动态时间  2 静态时间
     */
    private Integer timeType;
    /**
     * 动态时间  0 今日， 1昨日   ，2上周  ，3上月   ，4过去7天，5过去14天，6过去30天，7过去180天
     */
    private Integer dynTime;
    /**
     * 对比  0 无对比 ， 1 上周期
     */
    private Integer compare;
    /**
     * 限制条数
     */
    private Integer limit;
    /**
     * 排序方式 0 升序 1 降序(表格,top)
     */
    private Integer sortType;
    /**
     * 指标id数组(应用)
     */
    private List<Integer> indicators;
    /**
     * 指标id数组(事件)
     */
    private Map<Integer, List<Integer>> customIndicatorMap;

    /**
     * 事件的指标id
     *  customId
     *  indicators
     */
    private List<JSONObject> customIndicators;

    /**
     * 事件id集合
     */
    private List<Integer> customIds;

    /**
     * 线标识map(预留)
     */
    private Map<Integer, String> seriesNameMap;
    /**
     * 动态开始时间
     */
    private Date start;
    /**
     * 动态结束时间
     */
    private Date end;
    /**
     * 数据颗粒度(秒为单位)
     */
    private Integer interval;
    /**
     * 数据类型 1 应用 2 事件 3 漏斗
     */
    private Integer dataType;
    /**
     * 是否维度 0 否 1是
     */
    private Integer isDimension;

    /**
     * 维度数组
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    private List<ReportDimensionParam> dimensions;
    /**
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    private List<ReportDimensionParam> rules;
    /**
     * 数据配置表
     */
    private List<ReportChartDataParam> chartData;
    /**
     * uuid
     */
    private List<Integer>  uuid;

    /**
     * 事件分析维度[extendPre:extendFields]
     */
    private List<String> fixDimensions;

    /**
     * 表格的类型
     * 小时:1  5分钟:2  5s或60s:0
     */
    private Integer tableType;

    /**
     * 应用id
     */
    private Integer applicationId;
    /**
     * 组件id
     */
    private Integer componentId;
    /**
     * 自定义id
     */
    private Integer customId;

    public List<Map<String, Object>> getTimePart() {
        return timePart;
    }

    public void setTimePart(List<Map<String, Object>> timePart) {
        this.timePart = timePart;
    }

    public List<Map<String, Object>> getTimeCompare() {
        return timeCompare;
    }

    public void setTimeCompare(List<Map<String, Object>> timeCompare) {
        this.timeCompare = timeCompare;
    }

    public Integer getIntervalType() {
        return intervalType;
    }

    public void setIntervalType(Integer intervalType) {
        this.intervalType = intervalType;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public List<JSONObject> getCustomIndicators() {
        return customIndicators;
    }

    public void setCustomIndicators(List<JSONObject> customIndicators) {
        this.customIndicators = customIndicators;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }

    public Long getCompareStartTime() {
        return compareStartTime;
    }

    public void setCompareStartTime(Long compareStartTime) {
        this.compareStartTime = compareStartTime;
    }

    public Long getCompareEndTime() {
        return compareEndTime;
    }

    public void setCompareEndTime(Long compareEndTime) {
        this.compareEndTime = compareEndTime;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public List<Integer> getCustomIds() {
        return customIds;
    }

    public void setCustomIds(List<Integer> customIds) {
        this.customIds = customIds;
    }

    public Integer getTableType() {
        return tableType;
    }

    public void setTableType(Integer tableType) {
        this.tableType = tableType;
    }

    public List<String> getFixDimensions() {
        return fixDimensions;
    }

    public void setFixDimensions(List<String> fixDimensions) {
        this.fixDimensions = fixDimensions;
    }

    public Map<Integer, List<Integer>> getCustomIndicatorMap() {
        return customIndicatorMap;
    }

    public void setCustomIndicatorMap(Map<Integer, List<Integer>> customIndicatorMap) {
        this.customIndicatorMap = customIndicatorMap;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }


    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getDynTime() {
        return dynTime;
    }

    public void setDynTime(Integer dynTime) {
        this.dynTime = dynTime;
    }

    public Integer getCompare() {
        return compare;
    }

    public void setCompare(Integer compare) {
        this.compare = compare;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getSortType() {
        return sortType;
    }

    public void setSortType(Integer sortType) {
        this.sortType = sortType;
    }

    public List<Integer> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Integer> indicators) {
        this.indicators = indicators;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getIsDimension() {
        return isDimension;
    }

    public void setIsDimension(Integer isDimension) {
        this.isDimension = isDimension;
    }

    public List<ReportDimensionParam> getDimensions() {
        return dimensions;
    }

    public void setDimensions(List<ReportDimensionParam> dimensions) {
        this.dimensions = dimensions;
    }

    public List<ReportDimensionParam> getRules() {
        return rules;
    }

    public void setRules(List<ReportDimensionParam> rules) {
        this.rules = rules;
    }

    public List<ReportChartDataParam> getChartData() {
        return chartData;
    }

    public void setChartData(List<ReportChartDataParam> chartData) {
        this.chartData = chartData;
    }

    public List<Integer> getUuid() {
        return uuid;
    }

    public void setUuid(List<Integer> uuid) {
        this.uuid = uuid;
    }

    public Map<Integer, String> getSeriesNameMap() {
        return seriesNameMap;
    }

    public void setSeriesNameMap(Map<Integer, String> seriesNameMap) {
        this.seriesNameMap = seriesNameMap;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "ReportIndicatorParam{" +
                "type=" + type +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", compareStartTime=" + compareStartTime +
                ", compareEndTime=" + compareEndTime +
                ", intervalType=" + intervalType +
                ", timePart=" + timePart +
                ", timeCompare=" + timeCompare +
                ", timeType=" + timeType +
                ", dynTime=" + dynTime +
                ", compare=" + compare +
                ", limit=" + limit +
                ", sortType=" + sortType +
                ", indicators=" + indicators +
                ", customIndicatorMap=" + customIndicatorMap +
                ", customIndicators=" + customIndicators +
                ", customIds=" + customIds +
                ", seriesNameMap=" + seriesNameMap +
                ", start=" + start +
                ", end=" + end +
                ", interval=" + interval +
                ", dataType=" + dataType +
                ", isDimension=" + isDimension +
                ", dimensions=" + dimensions +
                ", rules=" + rules +
                ", chartData=" + chartData +
                ", uuid=" + uuid +
                ", fixDimensions=" + fixDimensions +
                ", tableType=" + tableType +
                ", applicationId=" + applicationId +
                ", componentId=" + componentId +
                ", customId=" + customId +
                '}';
    }
}
