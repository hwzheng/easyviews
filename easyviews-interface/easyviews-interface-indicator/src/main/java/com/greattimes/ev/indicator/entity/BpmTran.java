package com.greattimes.ev.indicator.entity;

import java.io.Serializable;
import java.util.Date;


public class BpmTran implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer applicationId;
	private Date date;
	private Long time;
	private Integer uuid;


	private Integer allStart2ends;
	private Integer allStart2endsCP;
	private Double transCountPER;
	private Double transCountDIFF;


	private Integer allTransCount;
	private Integer allTransCountCP;
	private Double allTransCountPER;
	private Double allTransCountDIFF;

	private Integer responseTransCount;
	private Integer responseTransCountCP;
	private Double responseTransCountPER;
	private Double responseTransCountDIFF;

	private Integer successTransCount;
	private Integer successTransCountCP;
	private Double successTransCountPER;
	private Double successTransCountDIFF;

	private Double responseTime;
	private Double responseTimeCP;
	private Double responseTimePER;
	private Double responseTimeDIFF;

	private Double responseRate;
	private Double responseRateCP;
	private Double responseRatePER;
	private Double responseRateDIFF;

	private Double successRate;
	private Double successRateCP;
	private Double successRatePER;
	private Double successRateDIFF;

	private Long timeCP;

	private String name;

	public Integer getAllStart2ends() {
		return allStart2ends;
	}

	public void setAllStart2ends(Integer allStart2ends) {
		this.allStart2ends = allStart2ends;
	}

	public Integer getAllTransCount() {
		return allTransCount;
	}

	public void setAllTransCount(Integer allTransCount) {
		this.allTransCount = allTransCount;
	}


	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getResponseTransCount() {
		return responseTransCount;
	}


	public BpmTran() {
		super();
	}

	public BpmTran(Long time) {
		super();
		//添加指标需修改
		this.time = time;
		this.responseTime = null;
		this.responseRate = null;
		this.successRate = null;
		this.allTransCount=null;

	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getUuid() {
		return uuid;
	}

	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}

	public Integer getAllStart2endsCP() {
		return allStart2endsCP;
	}

	public void setAllStart2endsCP(Integer allStart2endsCP) {
		this.allStart2endsCP = allStart2endsCP;
	}

	public Double getTransCountPER() {
		return transCountPER;
	}

	public void setTransCountPER(Double transCountPER) {
		this.transCountPER = transCountPER;
	}


	public Integer getAllTransCountCP() {
		return allTransCountCP;
	}

	public void setAllTransCountCP(Integer allTransCountCP) {
		this.allTransCountCP = allTransCountCP;
	}

	public Double getAllTransCountPER() {
		return allTransCountPER;
	}

	public void setAllTransCountPER(Double allTransCountPER) {
		this.allTransCountPER = allTransCountPER;
	}

	public void setResponseTransCount(Integer responseTransCount) {
		this.responseTransCount = responseTransCount;
	}

	public Integer getResponseTransCountCP() {
		return responseTransCountCP;
	}

	public void setResponseTransCountCP(Integer responseTransCountCP) {
		this.responseTransCountCP = responseTransCountCP;
	}

	public Double getResponseTransCountPER() {
		return responseTransCountPER;
	}

	public void setResponseTransCountPER(Double responseTransCountPER) {
		this.responseTransCountPER = responseTransCountPER;
	}

	public Integer getSuccessTransCount() {
		return successTransCount;
	}

	public void setSuccessTransCount(Integer successTransCount) {
		this.successTransCount = successTransCount;
	}

	public Integer getSuccessTransCountCP() {
		return successTransCountCP;
	}

	public void setSuccessTransCountCP(Integer successTransCountCP) {
		this.successTransCountCP = successTransCountCP;
	}

	public Double getSuccessTransCountPER() {
		return successTransCountPER;
	}

	public void setSuccessTransCountPER(Double successTransCountPER) {
		this.successTransCountPER = successTransCountPER;
	}


	public Double getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Double responseTime) {
		this.responseTime = responseTime;
	}

	public Double getResponseTimeCP() {
		return responseTimeCP;
	}

	public void setResponseTimeCP(Double responseTimeCP) {
		this.responseTimeCP = responseTimeCP;
	}

	public Double getResponseTimePER() {
		return responseTimePER;
	}

	public void setResponseTimePER(Double responseTimePER) {
		this.responseTimePER = responseTimePER;
	}

	public Double getResponseTimeDIFF() {
		return responseTimeDIFF;
	}

	public void setResponseTimeDIFF(Double responseTimeDIFF) {
		this.responseTimeDIFF = responseTimeDIFF;
	}

	public Double getResponseRate() {
		return responseRate;
	}

	public void setResponseRate(Double responseRate) {
		this.responseRate = responseRate;
	}

	public Double getResponseRateCP() {
		return responseRateCP;
	}

	public void setResponseRateCP(Double responseRateCP) {
		this.responseRateCP = responseRateCP;
	}

	public Double getResponseRatePER() {
		return responseRatePER;
	}

	public void setResponseRatePER(Double responseRatePER) {
		this.responseRatePER = responseRatePER;
	}

	public Double getResponseRateDIFF() {
		return responseRateDIFF;
	}

	public void setResponseRateDIFF(Double responseRateDIFF) {
		this.responseRateDIFF = responseRateDIFF;
	}

	public Double getSuccessRate() {
		return successRate;
	}

	public void setSuccessRate(Double successRate) {
		this.successRate = successRate;
	}

	public Double getSuccessRateCP() {
		return successRateCP;
	}

	public void setSuccessRateCP(Double successRateCP) {
		this.successRateCP = successRateCP;
	}

	public Double getSuccessRatePER() {
		return successRatePER;
	}

	public void setSuccessRatePER(Double successRatePER) {
		this.successRatePER = successRatePER;
	}

	public Double getSuccessRateDIFF() {
		return successRateDIFF;
	}

	public void setSuccessRateDIFF(Double successRateDIFF) {
		this.successRateDIFF = successRateDIFF;
	}

	public Long getTimeCP() {
		return timeCP;
	}

	public void setTimeCP(Long timeCP) {
		this.timeCP = timeCP;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getTransCountDIFF() {
		return transCountDIFF;
	}

	public void setTransCountDIFF(Double transCountDIFF) {
		this.transCountDIFF = transCountDIFF;
	}

	public Double getAllTransCountDIFF() {
		return allTransCountDIFF;
	}

	public void setAllTransCountDIFF(Double allTransCountDIFF) {
		this.allTransCountDIFF = allTransCountDIFF;
	}

	public Double getResponseTransCountDIFF() {
		return responseTransCountDIFF;
	}

	public void setResponseTransCountDIFF(Double responseTransCountDIFF) {
		this.responseTransCountDIFF = responseTransCountDIFF;
	}

	public Double getSuccessTransCountDIFF() {
		return successTransCountDIFF;
	}

	public void setSuccessTransCountDIFF(Double successTransCountDIFF) {
		this.successTransCountDIFF = successTransCountDIFF;
	}
	
	public BpmTran(Integer uuid) {
		//添加指标需修改
		super();
		this.uuid = uuid;
		this.responseTime = null;
		this.responseRate = null;
		this.successRate = null;
		this.allTransCount=null;
	}


	@Override
	public String toString() {
		return "BpmTran{" +
				"applicationId=" + applicationId +
				", date=" + date +
				", time=" + time +
				", uuid=" + uuid +
				", allStart2ends=" + allStart2ends +
				", allStart2endsCP=" + allStart2endsCP +
				", transCountPER=" + transCountPER +
				", transCountDIFF=" + transCountDIFF +
				", allTransCount=" + allTransCount +
				", allTransCountCP=" + allTransCountCP +
				", allTransCountPER=" + allTransCountPER +
				", allTransCountDIFF=" + allTransCountDIFF +
				", responseTransCount=" + responseTransCount +
				", responseTransCountCP=" + responseTransCountCP +
				", responseTransCountPER=" + responseTransCountPER +
				", responseTransCountDIFF=" + responseTransCountDIFF +
				", successTransCount=" + successTransCount +
				", successTransCountCP=" + successTransCountCP +
				", successTransCountPER=" + successTransCountPER +
				", successTransCountDIFF=" + successTransCountDIFF +
				", responseTime=" + responseTime +
				", responseTimeCP=" + responseTimeCP +
				", responseTimePER=" + responseTimePER +
				", responseTimeDIFF=" + responseTimeDIFF +
				", responseRate=" + responseRate +
				", responseRateCP=" + responseRateCP +
				", responseRatePER=" + responseRatePER +
				", responseRateDIFF=" + responseRateDIFF +
				", successRate=" + successRate +
				", successRateCP=" + successRateCP +
				", successRatePER=" + successRatePER +
				", successRateDIFF=" + successRateDIFF +
				", timeCP=" + timeCP +
				", name='" + name + '\'' +
				'}';
	}
}