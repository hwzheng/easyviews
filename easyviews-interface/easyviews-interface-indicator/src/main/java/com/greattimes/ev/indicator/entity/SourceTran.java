package com.greattimes.ev.indicator.entity;

import java.io.Serializable;
import java.util.Date;

public class SourceTran implements Serializable {
	private static final long serialVersionUID = 1L;
	private Date date;
	private Long time;
	private String innerUUID;
	private Integer dataSourceId;
	private Integer indicatorid;
	private Float value;



	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Long getTime() {
		return time;
	}


	public void setTime(Long time) {
		this.time = time;
	}


	public String getInnerUUID() {
		return innerUUID;
	}


	public void setInnerUUID(String innerUUID) {
		this.innerUUID = innerUUID;
	}


	public Integer getDataSourceId() {
		return dataSourceId;
	}


	public void setDataSourceId(Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Integer getIndicatorid() {
		return indicatorid;
	}


	public void setIndicatorid(Integer indicatorid) {
		this.indicatorid = indicatorid;
	}


	public Float getValue() {
		return value;
	}


	public void setValue(Float value) {
		this.value = value;
	}


	public SourceTran() {
		super();
	}


	public SourceTran(Long time, Integer indicator) {
		super();
		this.time = time;
		this.indicatorid=indicator;
		this.value=null;
	}



}
