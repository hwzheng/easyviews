package com.greattimes.ev.indicator.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/3/28 15:58
 */
public class ReportChartDataParam implements Serializable{

    private static final long serialVersionUID = -5021111922782261176L;
    /**
     * 图id
     */
    private Integer  chartId;
    /**
     * 应用id
     */
    private Integer applicationId;
    /**
     * 监控id
     */
    private Integer monitorId;
    /**
     * 组件id
     */
    private Integer componentId;
    /**
     * ipId
     */
    private Integer ipId;
    /**
     * 端口id
     */
    private Integer portId;
    /**
     * 数据类型 数据类型 1 应用 ，2 监控点 ，3 组件 ，4IP ，5 端口 ，6事件
     */
    private Integer dataType;
    /**
     * 自定义id
     */
    private Integer customId;

    /**
     * 事件指标
     */
    private List<Integer> indicators;

    public Integer getChartId() {
        return chartId;
    }

    public void setChartId(Integer chartId) {
        this.chartId = chartId;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public Integer getIpId() {
        return ipId;
    }

    public void setIpId(Integer ipId) {
        this.ipId = ipId;
    }

    public Integer getPortId() {
        return portId;
    }
    public void setPortId(Integer portId) {
        this.portId = portId;
    }


    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public List<Integer> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Integer> indicators) {
        this.indicators = indicators;
    }

    @Override
    public String toString() {
        return "ReportChartDataParam{" +
                "chartId=" + chartId +
                ", applicationId=" + applicationId +
                ", monitorId=" + monitorId +
                ", componentId=" + componentId +
                ", ipId=" + ipId +
                ", portId=" + portId +
                ", dataType=" + dataType +
                ", customId=" + customId +
                ", indicators=" + indicators +
                '}';
    }

}
