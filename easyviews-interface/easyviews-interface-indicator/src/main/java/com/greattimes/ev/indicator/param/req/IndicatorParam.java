package com.greattimes.ev.indicator.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/9/26 15:29
 */
@ApiModel
public class IndicatorParam implements Serializable{

    private static final long serialVersionUID = -5295604994500088318L;
    /**
     * 应用id
     */
    private Integer applicationId;
    /**
     * 自定义id
     */
    private Integer customId;

    /**
     * 指标维度UUID
     */
    @ApiModelProperty("指标维度UUID")
    private List<Integer> uuid;
    /**
     * 开始时间戳
     */
    @ApiModelProperty("开始时间戳")
    private Long start;
    /**
     * 结束时间戳
     */
    @ApiModelProperty("结束时间戳")
    private Long end;
    /**
     * 指标id
     */
    @ApiModelProperty("指标id")
    private List<Integer> indicator;

    /**
     * 是否开启权限  1 开启 0 or null 关闭
     */
    @ApiModelProperty("是否开启权限")
    private Integer isAuthority;
    /**
     * 维度筛选
     *   dimensionId	int	是	维度值id
     *   rule	int	是	维度筛选规则（1包含，2不包含，3等于，4不等于）
     *   value	string	是	值
     */
    @ApiModelProperty("维度筛选")
    private List<JSONObject> filterDimension;
    /**
     * 告警类型
     *  1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度 ， 7 监控点告警 , 8 数据源告警；
     *  9自定义-- 业务 ，10自定义-- 统计维度， 11自定义-- 普通维度， 12自定义--  多维度'
     */
    @ApiModelProperty("告警类型")
    private Integer ruleType;
    @ApiModelProperty("1 组件 2 业务")
    private Integer type;

    @ApiModelProperty("1 分钟  0 5s")
    private Integer timeType;
    /**
     * 指标集合
     */
    private List<Indicator> indicatorList;

    /**
     * 告警条件关系 1 任一条件 ，2 全部条件
     */
    private Integer relation;

    /**
     * 是否开启校验告警规则
     */
    private boolean checkRule;

    /**
     * 特殊指标id集合
     */
    private List<Integer> specialIndicators;

    /**
     * 其它告警模板数据
     */
    private List<Long> abnormalTime;
    /**
     * 大于颗粒度模板与其它模板告警是否进行合并
     */
    private boolean needMerge;

    private Integer interval;

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    @Override
    public String toString() {
        return "IndicatorParam{" +
                "applicationId=" + applicationId +
                ", customId=" + customId +
                ", uuid=" + uuid +
                ", start=" + start +
                ", end=" + end +
                ", indicator=" + indicator +
                ", isAuthority=" + isAuthority +
                ", filterDimension=" + filterDimension +
                ", ruleType=" + ruleType +
                ", type=" + type +
                ", indicatorList=" + indicatorList +
                ", relation=" + relation +
                ", checkRule=" + checkRule +
                ", specialIndicators=" + specialIndicators +
                ", abnormalTime=" + abnormalTime +
                ", needMerge=" + needMerge +
                ", interval=" + interval +
                '}';
    }

    public boolean isNeedMerge() {
        return needMerge;
    }

    public void setNeedMerge(boolean needMerge) {
        this.needMerge = needMerge;
    }

    public List<Long> getAbnormalTime() {
        return abnormalTime;
    }

    public void setAbnormalTime(List<Long> abnormalTime) {
        this.abnormalTime = abnormalTime;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public List<Integer> getUuid() {
        return uuid;
    }

    public void setUuid(List<Integer> uuid) {
        this.uuid = uuid;
    }

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public List<Integer> getIndicator() {
        return indicator;
    }

    public void setIndicator(List<Integer> indicator) {
        this.indicator = indicator;
    }

    public Integer getIsAuthority() {
        return isAuthority;
    }

    public void setIsAuthority(Integer isAuthority) {
        this.isAuthority = isAuthority;
    }

    public List<JSONObject> getFilterDimension() {
        return filterDimension;
    }

    public void setFilterDimension(List<JSONObject> filterDimension) {
        this.filterDimension = filterDimension;
    }

    public Integer getRuleType() {
        return ruleType;
    }

    public void setRuleType(Integer ruleType) {
        this.ruleType = ruleType;
    }

    public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}



    public Integer getRelation() {
        return relation;
    }

    public void setRelation(Integer relation) {
        this.relation = relation;
    }

    public boolean isCheckRule() {
        return checkRule;
    }

    public void setCheckRule(boolean checkRule) {
        this.checkRule = checkRule;
    }

    public List<Indicator> getIndicatorList() {
        return indicatorList;
    }

    public void setIndicatorList(List<Indicator> indicatorList) {
        this.indicatorList = indicatorList;
    }

    public List<Integer> getSpecialIndicators() {
        return specialIndicators;
    }

    public void setSpecialIndicators(List<Integer> specialIndicators) {
        this.specialIndicators = specialIndicators;
    }

	public Integer getTimeType() {
		return timeType;
	}

	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}

}
