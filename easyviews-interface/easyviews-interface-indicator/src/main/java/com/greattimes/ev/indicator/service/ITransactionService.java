package com.greattimes.ev.indicator.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.indicator.entity.BpmExtendTran;
import com.greattimes.ev.indicator.entity.BpmTran;
import com.greattimes.ev.indicator.param.req.ExtendIndicatorParam;
import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportChartParam;
import com.greattimes.ev.indicator.param.req.ReportIndicatorParam;
import com.greattimes.ev.indicator.param.req.ReportTableParam;
import com.greattimes.ev.indicator.param.req.TpsParam;
import com.greattimes.ev.indicator.param.resp.ExtendIndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;
import com.greattimes.ev.indicator.param.resp.ReportDataChartParam;
import com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam;
import com.greattimes.ev.indicator.param.resp.TransactionExtendTableParam;
import com.greattimes.ev.indicator.param.resp.TransactionTableParam;
import com.greattimes.ev.indicator.param.resp.TransactionTpsParam;

public interface ITransactionService {

	/**
	 * 交易指标图表类
	 * @author cgc  
	 * @date 2018年9月28日  
	 * @param param
	 * @return
	 */
	IndicatorDataParam chart(IndicatorParam param);

    /**
     * 告警查询指标图表类(包括拓展,其中包含对指标值进行标红逻辑) 只处理基础最小颗粒度(5s)
     * @author NJ
     * @date 2018/10/22 14:16
     * @param param
     * @return com.greattimes.ev.indicator.param.resp.IndicatorDataParam
     */
    IndicatorDataParam bpmAndExtendChart(IndicatorParam param);

	/**
	 * 交易指标表格
	 * @author cgc  
	 * @date 2018年9月29日  
	 * @param param
	 * @return
	 */
	List<TransactionTableParam> table(IndicatorParam param);

	/**
	 * 路径图交易量钢琴键查询
	 * @author NJ
	 * @date 2018/9/29 14:53
	 * @param param
	 * @return java.util.Map<java.lang.String,java.lang.Object>
	 */
	Map<String, Object> selectAppTransByMap(Map<String, Object> param);

	/**
	 * 基线查询
	 * @author cgc  
	 * @date 2018年9月30日  
	 * @param param
	 * @param interval 
	 * @return
	 */
	IndicatorDataParam baseline(IndicatorParam param, int interval);


	/**    
	 * 查询普普通和拓展指标数据(路径图组件弹框)
	 * @author NJ  
	 * @date 2018/9/30 15:35
	 * @param param  
	 * @return java.util.List<com.greattimes.ev.indicator.param.resp.TransactionTableParam>  
	 */
	List<Map<String, Object>>  selectTransByMap(Map<String, Object> param);

	/**
	 * 扩展指标查询
	 * @param param
	 * @return
	 */
	IndicatorDataParam extendChart(IndicatorParam param);



	/**扩展指标表格
	 * @param param
	 * @return
	 */
	List<TransactionExtendTableParam> extendTable(IndicatorParam param);


	/**
	 * 告警数据的查询
	 * @author NJ
	 * @date 2018/10/20 12:01
	 * @param param
	 * @return com.greattimes.ev.indicator.param.resp.IndicatorDataParam
	 */
	IndicatorDataParam selectIndicatorDataByAlarmRule(IndicatorParam param);


	/**
	 * 查询预处理指标数据 只处理基础最小颗粒度(5s)
	 * @param param
	 * @return
	 */
	IndicatorDataParam getPreIndicatorByDimension(IndicatorParam param);

	/**报表饼图数据
	 * @param re
	 * @return
	 */
	List<Map<String, Object>> getPieChartData(ReportChartParam re);


	/**
	 * 应用交易指标查询
	 * @author NJ
	 * @date 2019/4/1 20:21
	 * @param param
	 * @return com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam
	 */
    ReportLineIndicatorParam getReportLineIndicator(ReportIndicatorParam param);


	/**
	 * 事件指标查询
	 * @author NJ
	 * @date 2019/4/2 16:50
	 * @param param
	 * @return com.greattimes.ev.indicator.param.resp.ReportLineIndicatorParam
	 */
	ReportLineIndicatorParam getExtendReportLineIndicator(ReportIndicatorParam param);

	/**
	 * 应用数据表格数据
	 * @author NJ
	 * @date 2019/4/15 12:26
	 * @param page
	 * @param param
	 * @return com.baomidou.mybatisplus.plugins.Page<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	Page<BpmTran> selectBpmTablePage(Page<BpmTran> page, ReportTableParam param);
	
	/**   
	 * 报表top表格数据
	 * @author NJ  
	 * @date 2019/5/24 15:39 
	 * @param param   
	 * @return java.util.List<com.greattimes.ev.indicator.entity.BpmTran>  
	 */  
	List<BpmTran> selectBpmTable(ReportTableParam param);
	
	
	/**
	 * 拓展数据表格
	 * @author NJ
	 * @date 2019/4/16 14:43
	 * @param page
	 * @param param
	 * @return com.baomidou.mybatisplus.plugins.Page<com.greattimes.ev.indicator.entity.BpmExtendTran>
	 */
	Page<BpmExtendTran> selectExtendBpmTablePage(Page<BpmExtendTran> page, ReportTableParam param);
	
	/**   
	 * 拓展报表top表格数据
	 * @author NJ  
	 * @date 2019/5/24 15:40
	 * @param param   
	 * @return java.util.List<com.greattimes.ev.indicator.entity.BpmExtendTran>  
	 */  
	List<BpmExtendTran> selectExtendBpmTable(ReportTableParam param);

	/**
	 * 智能报表数值图
	 * @param re
	 * @return
	 */
	ReportDataChartParam getDataChart(ReportChartParam re);

	/**
	 * 第三方指标查询
	 * @author CGC
	 * @param param
	 * @return
	 */
	ExtendIndicatorDataParam extendIndicatorChart(ExtendIndicatorParam param);



	/**
	 * 多维和路径图钢琴键
	 * @author NJ
	 * @date 2019/6/18 15:17
	 * @param type
	 * @param uuid
	 * @param start
	 * @param end
	 * @return java.util.Map<java.lang.String,java.lang.Object>
	 */
	Map<String, Object> transCountPiano(int type, int uuid, long start, long end);

	/**
	 * 事件聚合图表类接口
	 * @author CGC
	 * @param param
	 * @return
	 */
	IndicatorDataParam extendCombineChart(IndicatorParam param);

	/**
	 * 事件指标
	 * @author CGC
	 * @param param
	 * @return
	 */
	TransactionExtendTableParam extendCombineTable(IndicatorParam param);

	/**
	 * bpmTran根据uuid进行汇总
	 * @author NJ
	 * @date 2019/9/4 17:13
	 * @param param
	 * @return java.util.Map<java.lang.String,java.lang.Object>
	 */
	Map<String, Object> selectCombineBpmTranTable(IndicatorParam param);

	/**事件指标查询
	 * @author CGC
	 * @param param
	 * @return
	 */
	List<TransactionExtendTableParam> extendTableGroupByUuid(IndicatorParam param);
	
	/**bpm指标查询
	 * @author CGC
	 * @param param
	 * @return
	 */
	List<TransactionTableParam> tableGroupByUuid(IndicatorParam param);

	/**获取tps峰值
	 * @author CGC
	 * @param param
	 * @return
	 */
	List<TransactionTpsParam> getTpsMax(TpsParam param);

	/**获取聚合tps峰值
	 * @author CGC
	 * @param param
	 * @return
	 */
	TransactionTpsParam getCombineTpsMax(TpsParam param);
}
