package com.greattimes.ev.indicator.param.resp;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 交易指标返回结果包装类
 * 
 * @author cgc
 *
 */
@ApiModel
public class TransactionExtendTableParam implements Serializable {
	private static final long serialVersionUID = 1L;
	@ApiModelProperty("時间")
	private Long time;
	@ApiModelProperty("指标维度id")
	private Integer uuid;
	@ApiModelProperty("响应时间")
	private Double responseTime;
	@ApiModelProperty("响应率")
	private Double responseRate;
	@ApiModelProperty("成功率")
	private Double successRate;
	@ApiModelProperty("金额")
	private Double money;
	@ApiModelProperty("业务成功率")
	private Double businessSuccessRate;
	@ApiModelProperty("交易量")
	private Integer allTransCount;
	@ApiModelProperty("列名")
	private String name;
	public Long getTime() {
		return time;
	}
	public void setTime(Long time) {
		this.time = time;
	}
	public Integer getUuid() {
		return uuid;
	}
	public void setUuid(Integer uuid) {
		this.uuid = uuid;
	}
	public Double getResponseTime() {
		return responseTime;
	}
	public void setResponseTime(Double responseTime) {
		this.responseTime = responseTime;
	}
	public Double getResponseRate() {
		return responseRate;
	}
	public void setResponseRate(Double responseRate) {
		this.responseRate = responseRate;
	}
	public Double getSuccessRate() {
		return successRate;
	}
	public void setSuccessRate(Double successRate) {
		this.successRate = successRate;
	}
	public Integer getAllTransCount() {
		return allTransCount;
	}
	public void setAllTransCount(Integer allTransCount) {
		this.allTransCount = allTransCount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getMoney() {
		return money;
	}
	public void setMoney(Double money) {
		this.money = money;
	}
	public Double getBusinessSuccessRate() {
		return businessSuccessRate;
	}
	public void setBusinessSuccessRate(Double businessSuccessRate) {
		this.businessSuccessRate = businessSuccessRate;
	}
	@Override
	public String toString() {
		return "TransactionTableParam{" +
				"time=" + time +
				", uuid=" + uuid +
				", responseTime=" + responseTime +
				", responseRate=" + responseRate +
				", successRate=" + successRate +
				", allTransCount=" + allTransCount +
				", name='" + name + '\'' +
				'}';
	}

	public TransactionExtendTableParam(Long time) {
		super();
		this.time = time;
		this.responseTime = null;
		this.responseRate = null;
		this.successRate = null;
		this.allTransCount=null;
	}
	public TransactionExtendTableParam() {
		super();
	}
	
}
