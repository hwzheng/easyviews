package com.greattimes.ev.indicator.service;


import com.greattimes.ev.indicator.param.req.IndicatorParam;
import com.greattimes.ev.indicator.param.resp.IndicatorDataParam;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/26 14:49
 */
public interface INetPerformanceService {
    /**
     * 10.2.1图表类接口(网络性能)
     * @param param
     * @return
     */
    IndicatorDataParam selectNetPerformanceListForChart(IndicatorParam param);

    /**
     * 10.2.2表格类接口(网络性能)
     * @param param
     * @return
     */
    List<Map<String, Object>> selectNetPerformanceListForTable(IndicatorParam param);


    /**
     * 图表类接口(应用性能)
     * @param param
     * @return
     */
    IndicatorDataParam selectAppNetPerformanceListForChart(IndicatorParam param);

    /**
     * 表格类接口(应用性能)
     * @param param
     * @return
     */
    List<Map<String, Object>> selectAppNetPerformanceListForTable(IndicatorParam param);




}
