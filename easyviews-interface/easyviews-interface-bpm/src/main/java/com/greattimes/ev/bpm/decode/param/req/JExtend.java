package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 解析扩展字段
 * @author MS
 * @date   2018 上午9:23:28
 */
public class JExtend implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//扩展id
	private Integer id;
	//扩展标识
	private String key;
	//扩展名称
	private String name;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "JExtend [id=" + id + ", key=" + key + ", name=" + name + "]";
	}
}
