package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;


/**
 * <p>
 * 智能报表图表指标表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_chart_indicator")
public class ReportChartIndicator extends Model<ReportChartIndicator> {

	private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	 * 指标id
	 */
	private Integer indicatorId;
	/**
	 * 图表id
	 */
	private Integer chartId;
	/**
	 * 名称
	 */
	@TableField(exist = false)
	private String name;
	/**
	 * 别名
	 */
	@TableField(exist = false)
	private String otherName;
	/**
	 * 单位
	 */
	@TableField(exist = false)
	private String unit;
	/**
	 * 事件id
	 */
	@TableField(exist = false)
	private Integer customId;
	/**
	 * 指标列名
	 */
	private String ename;

	public ReportChartIndicator() {
	}

	public ReportChartIndicator(Integer indicatorId, String name, String otherName, String unit, Integer customId, String ename) {
		this.indicatorId = indicatorId;
		this.name = name;
		this.otherName = otherName;
		this.unit = unit;
		this.customId = customId;
		this.ename = ename;
	}

	public ReportChartIndicator(Integer indicatorId, Integer chartId) {
		this.indicatorId = indicatorId;
		this.chartId = chartId;
	}

	public ReportChartIndicator(Integer id, Integer indicatorId, Integer chartId) {
		this.id = id;
		this.indicatorId = indicatorId;
		this.chartId = chartId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	@Override
	public String toString() {
		return "ReportChartIndicator{" +
				"id=" + id +
				", indicatorId=" + indicatorId +
				", chartId=" + chartId +
				", name='" + name + '\'' +
				", otherName='" + otherName + '\'' +
				", unit='" + unit + '\'' +
				", customId=" + customId +
				", ename='" + ename + '\'' +
				'}';
	}
}
