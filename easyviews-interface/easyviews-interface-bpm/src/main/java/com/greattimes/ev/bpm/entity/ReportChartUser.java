package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能报表图表所属人员表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_chart_user")
public class ReportChartUser extends Model<ReportChartUser> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 图表id
     */
	private Integer chartId;
    /**
     * 用户id
     */
	private Integer userId;

	public ReportChartUser() {
	}

	public ReportChartUser(Integer chartId, Integer userId) {
		this.chartId = chartId;
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ReportChartUser{" +
			"id=" + id +
			", chartId=" + chartId +
			", userId=" + userId +
			"}";
	}
}
