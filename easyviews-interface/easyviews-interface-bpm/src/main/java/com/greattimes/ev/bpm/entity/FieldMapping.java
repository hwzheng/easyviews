package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 维度字段映射表
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
@TableName("bpm_field_mapping")
public class FieldMapping extends Model<FieldMapping> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	private Integer id;
	/**
	 * 待翻译值
	 */
	private String value;
	/**
	 * 翻译值
	 */
	private String mapping;
	/**
	 * 配置id
	 */
	private Integer configId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FieldMapping{" + "id=" + id + ", value=" + value + ", mapping=" + mapping + ", configId=" + configId
				+ "}";
	}
}
