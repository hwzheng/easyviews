package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 告警信息发送类型
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
@TableName("bpm_alarm_send_type")
public class AlarmSendType extends Model<AlarmSendType> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value = "id", type = IdType.AUTO)
	private Integer id;
    /**
     * 告警组
     */
	private Integer alarmGroupId;
    /**
     * 告警发送类型
     */
	private Integer sendType;
    /**
     * 压缩条数
     */
	private Integer compressNum;
    /**
     * 压缩时间
     */
	private Integer compressTime;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlarmGroupId() {
		return alarmGroupId;
	}

	public void setAlarmGroupId(Integer alarmGroupId) {
		this.alarmGroupId = alarmGroupId;
	}

	public Integer getSendType() {
		return sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	public Integer getCompressNum() {
		return compressNum;
	}

	public void setCompressNum(Integer compressNum) {
		this.compressNum = compressNum;
	}

	public Integer getCompressTime() {
		return compressTime;
	}

	public void setCompressTime(Integer compressTime) {
		this.compressTime = compressTime;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmSendType{" +
			"id=" + id +
			", alarmGroupId=" + alarmGroupId +
			", sendType=" + sendType +
			", compressNum=" + compressNum +
			", compressTime=" + compressTime +
			"}";
	}
}
