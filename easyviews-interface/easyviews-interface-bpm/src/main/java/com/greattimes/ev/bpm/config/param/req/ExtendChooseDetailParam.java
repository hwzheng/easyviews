package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;
import java.util.List;

import com.alibaba.fastjson.JSONObject;

import io.swagger.annotations.ApiModel;

/**
 * @author CGC
 * @date 2018/9/26 15:29
 */
@ApiModel
public class ExtendChooseDetailParam implements Serializable{

    private static final long serialVersionUID = -5295604994500088318L;
    /**
     * 组件id
     */
    private Integer componentId;
    /**
     * 节点id
     */
    private Integer id;

    /**
     * 节点类型(0组件 1 ip 2 port)
     */
    private Integer type;

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}



}
