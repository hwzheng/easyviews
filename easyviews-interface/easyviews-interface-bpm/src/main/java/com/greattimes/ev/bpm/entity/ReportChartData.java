package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 智能报表图表数据表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_chart_data")
public class ReportChartData extends Model<ReportChartData> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 应用id
     */
	private Integer applicationId;
    /**
     * 监控id
     */
	private Integer monitorId;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * ipId
     */
	private Integer ipId;
    /**
     * 端口id
     */
	private Integer portId;
    /**
     * 数据类型
     */
	private Integer dataType;
    /**
     * 自定义id
     */
	private Integer customId;

	/**
	 * 图表id
	 */
	private Integer chartId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getIpId() {
		return ipId;
	}

	public void setIpId(Integer ipId) {
		this.ipId = ipId;
	}

	public Integer getPortId() {
		return portId;
	}

	public void setPortId(Integer portId) {
		this.portId = portId;
	}

	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	@Override
	public String toString() {
		return "ReportChartData{" +
				"id=" + id +
				", applicationId=" + applicationId +
				", monitorId=" + monitorId +
				", componentId=" + componentId +
				", ipId=" + ipId +
				", portId=" + portId +
				", dataType=" + dataType +
				", customId=" + customId +
				", chartId=" + chartId +
				'}';
	}
}
