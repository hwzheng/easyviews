package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 组件指标约束
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_comp_ind_constraint")
public class CompIndConstraint extends Model<CompIndConstraint> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Integer id;
    /**
     * 约束组id
     */
	private Integer constraintGroupId;
    /**
     * 字段id
     */
	private Integer fieldId;
    /**
     * 约束值
     */
	private String value;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getConstraintGroupId() {
		return constraintGroupId;
	}

	public void setConstraintGroupId(Integer constraintGroupId) {
		this.constraintGroupId = constraintGroupId;
	}

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
