package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 报文长度标记
 * @author MS
 * @date   2018 上午10:42:17
 */
public class JLengthMark implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//标记所占字节数
	private Integer occupyBytes;
	//标记取值类型 0：HEX、1：ASCII、2：BCD
	private Integer valueType;
	//标记值是否为完整报文长度（包含自身所占字节数）
	private Boolean allLength;
	//标记定位
	private Integer location;
	//位于标记位置之前的数据是否参与完整报文长度计算
	private Boolean calculateLocation;
	//报文长度计算偏移
	private Integer calculateOffset;
	
	public Integer getOccupyBytes() {
		return occupyBytes;
	}
	public void setOccupyBytes(Integer occupyBytes) {
		this.occupyBytes = occupyBytes;
	}
	public Integer getValueType() {
		return valueType;
	}
	public void setValueType(Integer valueType) {
		this.valueType = valueType;
	}
	public Boolean getAllLength() {
		return allLength;
	}
	public void setAllLength(Boolean allLength) {
		this.allLength = allLength;
	}
	public Integer getLocation() {
		return location;
	}
	public void setLocation(Integer location) {
		this.location = location;
	}
	public Boolean getCalculateLocation() {
		return calculateLocation;
	}
	public void setCalculateLocation(Boolean calculateLocation) {
		this.calculateLocation = calculateLocation;
	}
	public Integer getCalculateOffset() {
		return calculateOffset;
	}
	public void setCalculateOffset(Integer calculateOffset) {
		this.calculateOffset = calculateOffset;
	}
	
	@Override
	public String toString() {
		return "JLengthMark [occupyBytes=" + occupyBytes + ", valueType="
				+ valueType + ", allLength=" + allLength + ", location="
				+ location + ", calculateLocation=" + calculateLocation
				+ ", calculateOffset=" + calculateOffset + "]";
	}
}
