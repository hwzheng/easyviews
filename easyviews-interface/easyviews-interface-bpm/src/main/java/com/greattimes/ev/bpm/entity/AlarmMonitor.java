package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 告警监控点表
 * </p>
 *
 * @author NJ
 * @since 2018-08-09
 */
@TableName("bpm_alarm_monitor")
public class AlarmMonitor extends Model<AlarmMonitor> {

    private static final long serialVersionUID = 1L;

    /**
     * 告警id
     */
	private Integer alarmId;
    /**
     * 监控点id
     */
	private Integer monitorId;


	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	@Override
	protected Serializable pkVal() {
		return alarmId;
	}

	@Override
	public String toString() {
		return "AlarmMonitor{" +
			"alarmId=" + alarmId +
			", monitorId=" + monitorId +
			"}";
	}
}
