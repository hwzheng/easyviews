package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * pca下载任务
 * </p>
 *
 * @author cgc
 * @since 2018-11-07
 */
@TableName("bpm_pcap_task")
public class PcapTask extends Model<PcapTask> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 开始时间
     */
	private Date startTime;
    /**
     * 结束时间
     */
	private Date endTime;
    /**
     * 目的ip
     */
	private String dip;
    /**
     * 源ip
     */
	private String sip;
    /**
     * 端口
     */
	private Integer port;
    /**
     * 端口类型 1 已配置 2 未配置
     */
	private Integer portType;
    /**
     * 创建者
     */
	private Integer createBy;
    /**
     * 创建时间
     */
	private Date createTime;
    /**
     * 下载类型 1 IP, 2 端口
     */
	private Integer downType;
    /**
     * 状态  0 已创建  1正在执行  2执行成功  3执行失败 4 任务取消
     */
	private Integer state;
    /**
     * 文件地址
     */
	private String href;
	 /**
     * 文件地址
     */
	private String error;
    /**
     * 中心
     */
	private Integer centerId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getDip() {
		return dip;
	}

	public void setDip(String dip) {
		this.dip = dip;
	}

	public String getSip() {
		return sip;
	}

	public void setSip(String sip) {
		this.sip = sip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getPortType() {
		return portType;
	}

	public void setPortType(Integer portType) {
		this.portType = portType;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getDownType() {
		return downType;
	}

	public void setDownType(Integer downType) {
		this.downType = downType;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PcapTask{" +
			"id=" + id +
			", startTime=" + startTime +
			", endTime=" + endTime +
			", dip=" + dip +
			", sip=" + sip +
			", port=" + port +
			", portType=" + portType +
			", createBy=" + createBy +
			", createTime=" + createTime +
			", downType=" + downType +
			", state=" + state +
			", href=" + href +
			", centerId=" + centerId +
			"}";
	}
}
