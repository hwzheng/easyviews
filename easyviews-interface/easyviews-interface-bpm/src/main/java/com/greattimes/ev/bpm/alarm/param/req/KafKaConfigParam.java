package com.greattimes.ev.bpm.alarm.param.req;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Administrator on 2019/12/26.
 */
public class KafKaConfigParam implements Serializable{
    private static final long serialVersionUID = 1L;
    private Integer size;
    private String topic;
    private Map<String ,String > appRelevant;

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Map<String, String> getAppRelevant() {
        return appRelevant;
    }

    public void setAppRelevant(Map<String, String> appRelevant) {
        this.appRelevant = appRelevant;
    }

    @Override
    public String toString() {
        return "KafKaConfigParam{" +
                "size=" + size +
                ", topic='" + topic + '\'' +
                ", appRelevant=" + appRelevant +
                '}';
    }
}
