package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 交易判定依据
 * @author MS
 * @date   2018 上午11:05:48
 */
public class JJudgement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//交易判定结果 0 失败  1 成功
	private Integer judgeResult;
	//外层关系 1 and  2  or
 	private Integer relation;
	//返回码项
	private JJudgementRetCode retCode;
	//响应报文项
	private JJudgementResponseMsg responseMsg;
	
	public Integer getJudgeResult() {
		return judgeResult;
	}

	public void setJudgeResult(Integer judgeResult) {
		this.judgeResult = judgeResult;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}


	public JJudgementRetCode getRetCode() {
		return retCode;
	}

	public void setRetCode(JJudgementRetCode retCode) {
		this.retCode = retCode;
	}

	public JJudgementResponseMsg getResponseMsg() {
		return responseMsg;
	}

	public void setResponseMsg(JJudgementResponseMsg responseMsg) {
		this.responseMsg = responseMsg;
	}

	@Override
	public String toString() {
		return "JJudgement [judgeResult=" + judgeResult + ", relation="
				+ relation + ", retCode=" + retCode + ", responseMsg="
				+ responseMsg + "]";
	}
}
