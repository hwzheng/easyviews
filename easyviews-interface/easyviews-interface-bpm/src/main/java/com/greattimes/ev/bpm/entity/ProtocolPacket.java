package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 协议报文分组
 * </p>
 *
 * @author NJ
 * @since 2018-08-21
 */
@TableName("bpm_protocol_packet")
public class ProtocolPacket extends Model<ProtocolPacket> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 协议id
     */
	private Integer protocolId;
    /**
     * 协议报文
     */
	private byte[] message;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProtocolId() {
		return protocolId;
	}

	public void setProtocolId(Integer protocolId) {
		this.protocolId = protocolId;
	}

	public byte[] getMessage() {
		return message;
	}

	public void setMessage(byte[] message) {
		this.message = message;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ProtocolPacket{" +
			"id=" + id +
			", protocolId=" + protocolId +
			", message=" + message +
			"}";
	}
}
