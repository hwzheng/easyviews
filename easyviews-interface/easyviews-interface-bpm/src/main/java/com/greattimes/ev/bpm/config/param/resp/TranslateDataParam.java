package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;

/**
 * 翻譯返回结果包装类
 * 
 * @author cgc
 *
 */
@ApiModel
public class TranslateDataParam implements Serializable {
	private static final long serialVersionUID = 1L;
	private String value;
	private String mapping;
	private Integer dimensionId;
	private String ename;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dimensionId == null) ? 0 : dimensionId.hashCode());
		result = prime * result + ((ename == null) ? 0 : ename.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TranslateDataParam other = (TranslateDataParam) obj;
		if (dimensionId == null) {
			if (other.dimensionId != null)
				return false;
		} else if (!dimensionId.equals(other.dimensionId))
			return false;
		if (ename == null) {
			if (other.ename != null)
				return false;
		} else if (!ename.equals(other.ename))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}


}
