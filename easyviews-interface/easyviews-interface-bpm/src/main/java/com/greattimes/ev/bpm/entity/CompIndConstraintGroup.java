package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 组件指标约束
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_comp_ind_constraint_group")
public class CompIndConstraintGroup extends Model<CompIndConstraintGroup> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Integer id;
    /**
     * 组件指标组id
     */
	private Integer compIndicatorsId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCompIndicatorsId() {
		return compIndicatorsId;
	}

	public void setCompIndicatorsId(Integer compIndicatorsId) {
		this.compIndicatorsId = compIndicatorsId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CompIndConstraintGroup{" +
			"id=" + id +
			", compIndicatorsId=" + compIndicatorsId +
			"}";
	}
}
