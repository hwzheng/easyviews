package com.greattimes.ev.bpm.config.param.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author NJ
 * @date 2018/5/28 15:00
 */
@ApiModel
public class ApplicationParam implements  Serializable{

    private static final long serialVersionUID = 4485643047501659767L;
    /**
     * 主键
     */
    @ApiModelProperty("应用id")
    private Integer id;
    /**
     * 名称
     */
    @NotEmpty(message = "应用名称不能为空！  ")
    @ApiModelProperty("应用名称")
    private String name;
    /**
     * 激活状态：0 关闭 1开启
     */
    @ApiModelProperty("激活状态：0 关闭 1开启")
    private Integer active;
    /**
     * 告警状态:：0关闭 1开启
     */
    @ApiModelProperty("告警状态:：0关闭 1开启")
    private Integer alarmState;
    /**
     * 新建人
     */
    @ApiModelProperty("新建人")
    private String createByUser;
    /**
     * 新建时间
     */
    @ApiModelProperty("新建时间")
    private Date createTime;

    /**
     * 排序值
     */
    @ApiModelProperty("排序值")
    private Integer sort;

    /**
     * 中心id
     */
    @NotEmpty(message = "数据中心不能为空！")
    @ApiModelProperty("中心id")
    List<Integer> centerIds;

    @NotNull(message="监控点数量不能为空！")
    @ApiModelProperty("监控点数量")
    private Integer monitorNum;

    @ApiModelProperty("所属中心")
    private String centerName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getAlarmState() {
        return alarmState;
    }

    public void setAlarmState(Integer alarmState) {
        this.alarmState = alarmState;
    }

    public String getCreateByUser() {
        return createByUser;
    }

    public void setCreateByUser(String createByUser) {
        this.createByUser = createByUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public List<Integer> getCenterIds() {
        return centerIds;
    }

    public void setCenterIds(List<Integer> centerIds) {
        this.centerIds = centerIds;
    }

    public Integer getMonitorNum() {
        return monitorNum;
    }

    public void setMonitorNum(Integer monitorNum) {
        this.monitorNum = monitorNum;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public ApplicationParam() {
    }

    @Override
    public String toString() {
        return "ApplicationParam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", alarmState=" + alarmState +
                ", createByUser='" + createByUser + '\'' +
                ", createTime=" + createTime +
                ", sort=" + sort +
                ", centerIds=" + centerIds +
                ", monitorNum=" + monitorNum +
                ", centerName='" + centerName + '\'' +
                '}';
    }

    public ApplicationParam(Integer id, String name, Integer active, Integer alarmState, List<Integer> centerIds, Integer monitorNum) {
        this.id = id;
        this.name = name;
        this.active = active;
        this.alarmState = alarmState;
        this.centerIds = centerIds;
        this.monitorNum = monitorNum;
    }

    public ApplicationParam(String name, Integer active, Integer alarmState, List<Integer> centerIds, Integer monitorNum) {
        this.name = name;
        this.active = active;
        this.alarmState = alarmState;
        this.centerIds = centerIds;
        this.monitorNum = monitorNum;
    }
}
