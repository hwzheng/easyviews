package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 重复排期详情包装类
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
@ApiModel
public class MultiScheduleDetailParam implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 主键
	 */
	@ApiModelProperty("主键")
	private Integer id;
	/**
	 * 状态
	 */
	@ApiModelProperty("状态")
	private Integer active;
	/**
	 * 排期类型 1 ：全天 2 ：自定义时段
	 */
	@ApiModelProperty("排期类型 1 ：全天 2 ：自定义时段")
	private Integer types;
	/**
	 * 日期
	 */
	@ApiModelProperty("日期")
	private String day;
	/**
	 * 时间
	 */
	@ApiModelProperty("时间")
	private String time;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getTypes() {
		return types;
	}

	public void setTypes(Integer types) {
		this.types = types;
	}

	public String getDay() {
		return day;
	}

	public void setDay(String day) {
		this.day = day;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

}
