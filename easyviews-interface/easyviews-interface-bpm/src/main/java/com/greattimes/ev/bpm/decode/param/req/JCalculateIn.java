package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 计算操作in
 * @author MS
 * @date   2018 下午5:24:44
 */
public class JCalculateIn implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1612768894094186552L;
	//操作数1主键
	private Integer srcFieldId;
	//操作数1来源
	private Integer srcOrigin;
	//操作符
	private Integer operator;
	//操作数2主键
	private Integer dstFieldId;
	//操作数2来源
	private Integer dstOrigin;
	//操作数2值
	private Integer dstVal;
	
	public Integer getSrcFieldId() {
		return srcFieldId;
	}
	public void setSrcFieldId(Integer srcFieldId) {
		this.srcFieldId = srcFieldId;
	}
	public Integer getSrcOrigin() {
		return srcOrigin;
	}
	public void setSrcOrigin(Integer srcOrigin) {
		this.srcOrigin = srcOrigin;
	}
	public Integer getOperator() {
		return operator;
	}
	public void setOperator(Integer operator) {
		this.operator = operator;
	}
	public Integer getDstFieldId() {
		return dstFieldId;
	}
	public void setDstFieldId(Integer dstFieldId) {
		this.dstFieldId = dstFieldId;
	}
	public Integer getDstOrigin() {
		return dstOrigin;
	}
	public void setDstOrigin(Integer dstOrigin) {
		this.dstOrigin = dstOrigin;
	}
	public Integer getDstVal() {
		return dstVal;
	}
	public void setDstVal(Integer dstVal) {
		this.dstVal = dstVal;
	}
	@Override
	public String toString() {
		return "JCalculateIn [srcFieldId=" + srcFieldId + ", srcOrigin="
				+ srcOrigin + ", operator=" + operator + ", dstFieldId="
				+ dstFieldId + ", dstOrigin=" + dstOrigin + ", dstVal="
				+ dstVal + "]";
	}
	
}
