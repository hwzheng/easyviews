package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警 数据源维度 表
 * </p>
 *
 * @author NJ
 * @since 2018-08-16
 */
@TableName("bpm_alarm_datasource_dimension")
public class AlarmDatasourceDimension extends Model<AlarmDatasourceDimension> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 数据源关联约束表id
     */
	private Integer relevantContraintId;
    /**
     * 告警数据源id
     */
	private Integer alarmDataSourceId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRelevantContraintId() {
		return relevantContraintId;
	}

	public void setRelevantContraintId(Integer relevantContraintId) {
		this.relevantContraintId = relevantContraintId;
	}

	public Integer getAlarmDataSourceId() {
		return alarmDataSourceId;
	}

	public void setAlarmDataSourceId(Integer alarmDataSourceId) {
		this.alarmDataSourceId = alarmDataSourceId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmDatasourceDimension{" +
			"id=" + id +
			", relevantContraintId=" + relevantContraintId +
			", alarmDataSourceId=" + alarmDataSourceId +
			"}";
	}
}
