package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 监控点-组件映射表
 * </p>
 *
 * @author NJ
 * @since 2018-08-07
 */
@TableName("bpm_monitor_component")
public class MonitorComponent extends Model<MonitorComponent> {

    private static final long serialVersionUID = 1L;

    /**
     * 监控id
     */
	private Integer monitorId;
    /**
     * 组件id
     */
	private Integer componentId;


	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@Override
	protected Serializable pkVal() {
		return this.monitorId;
	}

	public MonitorComponent(Integer monitorId, Integer componentId) {
		this.monitorId = monitorId;
		this.componentId = componentId;
	}

	@Override
	public String toString() {
		return "MonitorComponent{" +
			"monitorId=" + monitorId +
			", componentId=" + componentId +
			"}";
	}
}
