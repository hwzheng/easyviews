package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 组件分析维度
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
@TableName("bpm_component_dimension")
public class ComponentDimension extends Model<ComponentDimension> {

    private static final long serialVersionUID = 1L;

    /**
     * 协议维度id
     */
	private Integer dimensionId;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * 排序
     */
	private Integer sort;


	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}


	@Override
	public String toString() {
		return "ComponentDimension{" +
			"dimensionId=" + dimensionId +
			", componentId=" + componentId +
			", sort=" + sort +
			"}";
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}
}
