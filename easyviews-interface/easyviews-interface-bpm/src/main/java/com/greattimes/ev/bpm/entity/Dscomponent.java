package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 数据源组件表
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_component")
public class Dscomponent extends Model<Dscomponent> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Integer id;
    /**
     * 父id
     */
	private Integer parentId;
    /**
     * 数据源id
     */
	private Integer sourcesId;
    /**
     * 名称
     */
	private String name;
    /**
     * 状态
     */
	private Integer active;
    /**
     * 排序
     */
	private Integer sort;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public Integer getSourcesId() {
		return sourcesId;
	}

	public void setSourcesId(Integer sourcesId) {
		this.sourcesId = sourcesId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Component{" +
			"id=" + id +
			", parentId=" + parentId +
			", sourcesId=" + sourcesId +
			", name=" + name +
			", active=" + active +
			", sort=" + sort +
			"}";
	}
}
