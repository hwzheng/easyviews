package com.greattimes.ev.bpm.analysis.param.resp;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author NJ
 * @date 2018/9/11 21:51
 */
public class ApplicationTree implements Serializable{

    private static final long serialVersionUID = 8252728194175170719L;

    /**
     * 节点名称
     */
    private String title;
    /**
     * id
     */
    private Integer id;
    /**
     * 父节点id
     */
    private Integer parentId;
    /**
     * 节点类型 1:应用 2:监控点 3:组件 4监控点数据
     */
    private Integer nodeType;
    /**
     * 关联节点id
     */
    private Integer relevanceId;
    /**
     * 额外的数据
     */
    private JSONObject extraData;
    /**
     * 子节点数组
     */
    private List<ApplicationTree> children = new ArrayList<>();


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getNodeType() {
        return nodeType;
    }

    public void setNodeType(Integer nodeType) {
        this.nodeType = nodeType;
    }

    public Integer getRelevanceId() {
        return relevanceId;
    }

    public void setRelevanceId(Integer relevanceId) {
        this.relevanceId = relevanceId;
    }

    public List<ApplicationTree> getChildren() {
        return children;
    }

    public void setChildren(List<ApplicationTree> children) {
        this.children = children;
    }

    public ApplicationTree() {
    }

    public JSONObject getExtraData() {
        return extraData;
    }

    public void setExtraData(JSONObject extraData) {
        this.extraData = extraData;
    }

    public ApplicationTree(String title, Integer id, Integer nodeType, List<ApplicationTree> children) {
        this.title = title;
        this.id = id;
        this.nodeType = nodeType;
        this.children = children;
    }

    public ApplicationTree(String title, Integer id, Integer parentId, Integer nodeType, List<ApplicationTree> children) {
        this.title = title;
        this.id = id;
        this.parentId = parentId;
        this.nodeType = nodeType;
        this.children = children;
    }

    public ApplicationTree(String title, Integer id, Integer parentId, Integer nodeType, JSONObject extraData, List<ApplicationTree> children) {
        this.title = title;
        this.id = id;
        this.parentId = parentId;
        this.nodeType = nodeType;
        this.extraData = extraData;
        this.children = children;
    }

    @Override
    public String toString() {
        return "ApplicationTree{" +
                "title='" + title + '\'' +
                ", id=" + id +
                ", parentId=" + parentId +
                ", nodeType=" + nodeType +
                ", relevanceId=" + relevanceId +
                ", children=" + children +
                '}';
    }
}
