package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 用户应用表
 * </p>
 *
 * @author Cgc
 * @since 2018-05-18
 */
@TableName("bpm_user_application")
@ApiModel
public class UserApplication extends Model<UserApplication> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @TableId("userId")
    @ApiModelProperty("用户id")
	private Integer userId;
    /**
     * 应用id
     */
    @ApiModelProperty("应用id")
	private Integer applicationId;


	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	protected Serializable pkVal() {
		return this.userId;
	}

	@Override
	public String toString() {
		return "UserApplication{" +
			"userId=" + userId +
			", applicationId=" + applicationId +
			"}";
	}
}
