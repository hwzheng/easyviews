package com.greattimes.ev.bpm.trace.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/10/24 14:27
 */
@ApiModel
public class TraceParam implements Serializable{
    private static final long serialVersionUID = 6219272205875184659L;

    @ApiModelProperty("回话ID")
    private String sessionId;
    @ApiModelProperty("每页条数 默认10")
    private Integer pageSize;
    @ApiModelProperty("查询页数 默认1")
    private Integer pageNum;
    /**
     * key:rule  排序规则1 升序 2降序 int
     *     dimension  排序字段 String
     */
    @ApiModelProperty("排序")
    private List<JSONObject> order;
    @ApiModelProperty("应用id")
    private Integer applicationId;
    @ApiModelProperty("组件id")
    private Integer compontentId;
    @ApiModelProperty("查询维度uuid")
    private Long uuid;
    @ApiModelProperty("1 组件 2 业务")
    private Integer type;
    @ApiModelProperty("开始时间戳")
    private Long startTime;
    @ApiModelProperty("结束时间戳")
    private Long endTime;
    @ApiModelProperty("自定义过滤规则表达式 ，type=2时生效")
    private String corn;
    @ApiModelProperty("交易结果 0 成功 1 失败2  全部(defalut)")
    private Integer transState;
    @ApiModelProperty("响应结果 0 有响应 1 无响 2  全部(defalut)")
    private Integer responseState;
    /**
     * key:rule  指标筛选规则(1大于，2小于，3等于，4不等于) int
     *     value  值  num
     */
    @ApiModelProperty("指标筛选")
    private JSONObject responseTime;

    /**
     * key:dimension  维度key string
     *     rule  维度筛选规则（1包含，2不包含，3等于，4不等于 5字符串包含 ，6 字符串不包含）  int
     *     value 值 string
     */
    @ApiModelProperty("维度筛选")
    private List<JSONObject> filterDimension;

    /**
	 * 下载需要的表头
	 * headers
	 *	name
	 *	value
	 */
    @ApiModelProperty("下载需要的表头")
	private List<JSONObject> headers;
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Long getUuid() {
        return uuid;
    }

    public void setUuid(Long uuid) {
        this.uuid = uuid;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getCorn() {
        return corn;
    }

    public void setCorn(String corn) {
        this.corn = corn;
    }

    public Integer getTransState() {
        return transState;
    }

    public void setTransState(Integer transState) {
        this.transState = transState;
    }

    public Integer getResponseState() {
        return responseState;
    }

    public void setResponseState(Integer responseState) {
        this.responseState = responseState;
    }

    public Integer getCompontentId() {
        return compontentId;
    }

    public void setCompontentId(Integer compontentId) {
        this.compontentId = compontentId;
    }

    public List<JSONObject> getFilterDimension() {
        return filterDimension;
    }

    public void setFilterDimension(List<JSONObject> filterDimension) {
        this.filterDimension = filterDimension;
    }

    public List<JSONObject> getOrder() {
        return order;
    }

    public void setOrder(List<JSONObject> order) {
        this.order = order;
    }

    public JSONObject getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(JSONObject responseTime) {
        this.responseTime = responseTime;
    }

    public List<JSONObject> getHeaders() {
		return headers;
	}

	public void setHeaders(List<JSONObject> headers) {
		this.headers = headers;
	}

	@Override
    public String toString() {
        return "TraceParam{" +
                "sessionId='" + sessionId + '\'' +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", order=" + order +
                ", applicationId=" + applicationId +
                ", compontentId=" + compontentId +
                ", uuid=" + uuid +
                ", type=" + type +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", corn='" + corn + '\'' +
                ", transState=" + transState +
                ", responseState=" + responseState +
                ", responseTime=" + responseTime +
                ", filterDimension=" + filterDimension +
                '}';
    }
}
