package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * http协议参数
 * @author MS
 * @date   2018 上午10:31:19
 */
public class JHttp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//是否将请求路径用作交易类型
	private Boolean uriAsServiceCode;
	//是否抛出网页静态资源
	private Boolean throwStaticResource;
	//是否发送请求报文体内容
	private Boolean reqSendMsgBody;
	//是否发送响应报文体内容
	private Boolean respSendMsgBody;
	
	public Boolean getUriAsServiceCode() {
		return uriAsServiceCode;
	}
	public void setUriAsServiceCode(Boolean uriAsServiceCode) {
		this.uriAsServiceCode = uriAsServiceCode;
	}
	public Boolean getThrowStaticResource() {
		return throwStaticResource;
	}
	public void setThrowStaticResource(Boolean throwStaticResource) {
		this.throwStaticResource = throwStaticResource;
	}
	public Boolean getReqSendMsgBody() {
		return reqSendMsgBody;
	}
	public void setReqSendMsgBody(Boolean reqSendMsgBody) {
		this.reqSendMsgBody = reqSendMsgBody;
	}
	public Boolean getRespSendMsgBody() {
		return respSendMsgBody;
	}
	public void setRespSendMsgBody(Boolean respSendMsgBody) {
		this.respSendMsgBody = respSendMsgBody;
	}
	
	@Override
	public String toString() {
		return "JHttp [uriAsServiceCode=" + uriAsServiceCode
				+ ", throwStaticResource=" + throwStaticResource
				+ ", reqSendMsgBody=" + reqSendMsgBody + ", respSendMsgBody="
				+ respSendMsgBody + "]";
	}
}
