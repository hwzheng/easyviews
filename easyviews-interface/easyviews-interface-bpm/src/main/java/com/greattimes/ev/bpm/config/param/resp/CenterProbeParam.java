package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 中心探针表
 * </p>
 *
 * @author cgc
 * @since 2018-08-08
 */
@ApiModel
public class CenterProbeParam extends Model<CenterProbeParam> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
	private Integer id;
    /**
     * 中心id
     */
    @ApiModelProperty("中心id")
	private Integer centerId;
    /**
     * 中心名
     */
    @ApiModelProperty("中心名")
	private String centerName;
    /**
     * 探针ip
     */
    @ApiModelProperty("探针ip")
	@TableField("IP")
	private String ip;
    /**
     * 端口类型
     */
    @ApiModelProperty("端口类型")
	private String portType;
    /**
     * 端口数量
     */
    @ApiModelProperty("端口数量")
	private Integer portNum;
    /**
     * 探针编号
     */
    @ApiModelProperty("探针编号")
	private String code;
    /**
     * 探针名称
     */
    @ApiModelProperty("探针名称")
	private String name;
    /**
     * 端口号
     */
    @ApiModelProperty("端口号")
	private Integer portCode;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}

	public String getCenterName() {
		return centerName;
	}

	public void setCenterName(String centerName) {
		this.centerName = centerName;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}


	public Integer getPortNum() {
		return portNum;
	}

	public void setPortNum(Integer portNum) {
		this.portNum = portNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getPortCode() {
		return portCode;
	}

	public void setPortCode(Integer portCode) {
		this.portCode = portCode;
	}

	@Override
	public String toString() {
		return "CenterProbe{" +
			"id=" + id +
			", centerId=" + centerId +
			", ip=" + ip +
			", portType=" + portType +
			", protNum=" + portNum +
			", code=" + code +
			", name=" + name +
			"}";
	}
}
