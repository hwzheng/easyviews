package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 数据源关联表
 * </p>
 *
 * @author NJ
 * @since 2018-08-17
 */
@TableName("bpm_datasource_relevant")
@ApiModel
public class DatasourceRelevant extends Model<DatasourceRelevant> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	@ApiModelProperty("id")
	private Integer id;
    /**
     * 组件id
     */
	@ApiModelProperty("组件id")
	private Integer componentId;
    /**
     * 数据源id
     */
	@ApiModelProperty("数据源id")
	private Integer dataSourceId;
    /**
     * 指标组
     */
	@ApiModelProperty("指标组")
	private Integer indicatorGroupId;
    /**
     * 创建人
     */
	@ApiModelProperty("创建人")
	private Integer createBy;
    /**
     * 创建时间
     */
	@ApiModelProperty("创建时间")
	private Date createTime;
    /**
     * 修改人
     */
	@ApiModelProperty("修改人")
	private Integer updateBy;
    /**
     * 修改时间
     */
	@ApiModelProperty("修改时间")
	private Date updateTime;
    /**
     * 显示维度： 0否 1 是
     */
	@ApiModelProperty("显示维度： 0否 1 是")
	private Integer showDimension;
    /**
     * 关联层级  0组件 1 ip 2 port
     */
	@ApiModelProperty("关联层级  1 ip 2 port")
	private Integer relevantField;
	@ApiModelProperty("挂载点  0组件 1 ip 2 port")
	private Integer mount;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public Integer getIndicatorGroupId() {
		return indicatorGroupId;
	}

	public void setIndicatorGroupId(Integer indicatorGroupId) {
		this.indicatorGroupId = indicatorGroupId;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getShowDimension() {
		return showDimension;
	}

	public void setShowDimension(Integer showDimension) {
		this.showDimension = showDimension;
	}

	public Integer getRelevantField() {
		return relevantField;
	}

	public void setRelevantField(Integer relevantField) {
		this.relevantField = relevantField;
	}

	public Integer getMount() {
		return mount;
	}

	public void setMount(Integer mount) {
		this.mount = mount;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DatasourceRelevant{" +
			"id=" + id +
			", componentId=" + componentId +
			", dataSourceId=" + dataSourceId +
			", indicatorGroupId=" + indicatorGroupId +
			", createBy=" + createBy +
			", createTime=" + createTime +
			", updateBy=" + updateBy +
			", updateTime=" + updateTime +
			", showDimension=" + showDimension +
			", relevantField=" + relevantField +
			", mount=" + mount +
			"}";
	}
}
