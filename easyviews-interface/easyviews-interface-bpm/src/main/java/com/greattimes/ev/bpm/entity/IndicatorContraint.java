package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据源指标约束
 * </p>
 *
 * @author NJ
 * @since 2018-08-17
 */
@TableName("ds_indicator_contraint")
public class IndicatorContraint extends Model<IndicatorContraint> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 外部uuid
     */
	private String outerUuid;
    /**
     * 指标id
     */
	private Integer indicatorId;
    /**
     * 约束值
     */
	private String value;
    /**
     * 指标组id
     */
	private Integer indicatorGroupId;
    /**
     * 已关联标记 0 否 1 是
     */
	private Integer mark;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getOuterUuid() {
		return outerUuid;
	}

	public void setOuterUuid(String outerUuid) {
		this.outerUuid = outerUuid;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getIndicatorGroupId() {
		return indicatorGroupId;
	}

	public void setIndicatorGroupId(Integer indicatorGroupId) {
		this.indicatorGroupId = indicatorGroupId;
	}

	public Integer getMark() {
		return mark;
	}

	public void setMark(Integer mark) {
		this.mark = mark;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "IndicatorContraint{" +
				"id=" + id +
				", outerUuid='" + outerUuid + '\'' +
				", indicatorId=" + indicatorId +
				", value='" + value + '\'' +
				", indicatorGroupId=" + indicatorGroupId +
				", mark=" + mark +
				'}';
	}
}
