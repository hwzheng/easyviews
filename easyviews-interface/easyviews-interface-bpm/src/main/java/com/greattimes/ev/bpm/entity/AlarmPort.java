package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 告警port表
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
@TableName("bpm_alarm_port")
public class AlarmPort extends Model<AlarmPort> {

    private static final long serialVersionUID = 1L;

    /**
     * 告警id
     */
	private Integer alarmId;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * ip
     */
	private String ip;
    /**
     * port
     */
	private Integer port;


	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}


	@Override
	public String toString() {
		return "AlarmPort{" +
			"alarmId=" + alarmId +
			", componentId=" + componentId +
			", ip=" + ip +
			", port=" + port +
			"}";
	}
	@Override
	protected Serializable pkVal() {
		return null;
	}
}
