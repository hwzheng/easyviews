package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 探针口表
 * </p>
 *
 * @author cgc
 * @since 2018-08-08
 */
@ApiModel
public class CenterProbePortParam extends Model<CenterProbePortParam> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
	private Integer id;
    /**
     * 探针id
     */
    @ApiModelProperty("探针id")
	private Integer probeId;
    /**
     * 端口类型
     */
    @ApiModelProperty("端口类型")
	private String portType;
    /**
     * 端口号
     */
    @ApiModelProperty("端口号")
	private Integer portCode;
	/**
	 * 探针编号
	 */
    @ApiModelProperty("探针编号")
	private String code;
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProbeId() {
		return probeId;
	}

	public void setProbeId(Integer probeId) {
		this.probeId = probeId;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}


	public Integer getPortCode() {
		return portCode;
	}

	public void setPortCode(Integer portCode) {
		this.portCode = portCode;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CenterProbePort{" +
			"id=" + id +
			", probeId=" + probeId +
			", portType=" + portType +
			", protCode=" + portCode +
			"}";
	}
}
