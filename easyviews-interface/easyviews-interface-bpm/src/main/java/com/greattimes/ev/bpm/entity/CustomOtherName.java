package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 别名表
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_field_other_name")
public class CustomOtherName extends Model<CustomOtherName> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	private Integer id;
    /**
     * 自定义id
     */
	private Integer customId;
    /**
     * 字段id
     */
	private Integer fieldId;
    /**
     * 别名
     */
	private String othername;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public String getOthername() {
		return othername;
	}

	public void setOthername(String othername) {
		this.othername = othername;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FieldOtherName{" +
			"id=" + id +
			", customId=" + customId +
			", fieldId=" + fieldId +
			", othername=" + othername +
			"}";
	}
}
