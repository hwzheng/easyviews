package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * 解析分组
 * @author MS
 * @date   2018 上午10:05:30
 */
public class JParseGroup implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//解析分组主键
	private Integer id;
	//分组类型 1：TEXT、2：JSON、3：KVP、4：SOP 5：TLV、6：XML、7：8583 110：HTTP、111：HESSIAN、112：SOAP、113：HTML 120：JOLT、130：TNS、140：WMQ
	private Integer type;
	//分组范围 0：请求、1：响应
	private Integer scope;
	//分组字符集 0：GB18030、1：UTF-8
	private Integer charset;
	//分组编码类型 0：NONE、1：BASE64、2：URI 3：UNICODE、4：EBCDIC
	private Integer encoding;
	//排序索引 
	private Integer index;
	//是否为子组
	private Boolean isLeaf;
	//分组开始位置
	private Integer startLocation;
	//分组结束位置
	private Integer endLocation;
	//分组报文内容
	private String message;
	//分组参数
	private JParseGroupParams params;
	//解析字段
	private List<JField> field;
	//协议识别字段
	private List<JOriginField> originField;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getScope() {
		return scope;
	}
	public void setScope(Integer scope) {
		this.scope = scope;
	}
	public Integer getCharset() {
		return charset;
	}
	public void setCharset(Integer charset) {
		this.charset = charset;
	}
	public Integer getEncoding() {
		return encoding;
	}
	public void setEncoding(Integer encoding) {
		this.encoding = encoding;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public Boolean getIsLeaf() {
		return isLeaf;
	}
	public void setIsLeaf(Boolean isLeaf) {
		this.isLeaf = isLeaf;
	}
	public Integer getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(Integer startLocation) {
		this.startLocation = startLocation;
	}
	public Integer getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(Integer endLocation) {
		this.endLocation = endLocation;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public JParseGroupParams getParams() {
		return params;
	}
	public void setParams(JParseGroupParams params) {
		this.params = params;
	}
	public List<JField> getField() {
		return field;
	}
	public void setField(List<JField> field) {
		this.field = field;
	}
	public List<JOriginField> getOriginField() {
		return originField;
	}
	public void setOriginField(List<JOriginField> originField) {
		this.originField = originField;
	}
	
	@Override
	public String toString() {
		return "JParseGroup [id=" + id + ", type=" + type + ", scope=" + scope
				+ ", charset=" + charset + ", encoding=" + encoding
				+ ", index=" + index + ", isLeaf=" + isLeaf
				+ ", startLocation=" + startLocation + ", endLocation="
				+ endLocation + ", message=" + message + ", params=" + params
				+ ", field=" + field + ", originField=" + originField + "]";
	}
}
