package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartDataParam;

import java.io.Serializable;

/**
 * <p>
 * 智能报表事件指标标表
 * </p>
 *
 * @author NJ
 * @since 2019-03-29
 */
@TableName("ai_report_chart_data_indicator")
public class ReportChartDataIndicator extends Model<ReportChartDataIndicator> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 指标id
     */
	private Integer indicatorId;
    /**
     * 数据id
     */
	private Integer chartDataId;

	public ReportChartDataIndicator(ReportChartDataParam x, int reportChartDataId) {
	}

	public ReportChartDataIndicator(Integer id, Integer indicatorId, Integer chartDataId) {
		this.id = id;
		this.indicatorId = indicatorId;
		this.chartDataId = chartDataId;
	}

	public ReportChartDataIndicator(Integer indicatorId, Integer chartDataId) {
		this.indicatorId = indicatorId;
		this.chartDataId = chartDataId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public Integer getChartDataId() {
		return chartDataId;
	}

	public void setChartDataId(Integer chartDataId) {
		this.chartDataId = chartDataId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ReportChartDataIndicator{" +
			"id=" + id +
			", indicatorId=" + indicatorId +
			", chartDataId=" + chartDataId +
			"}";
	}
}
