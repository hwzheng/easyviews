package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 裁剪操作
 * @author MS
 * @date   2018 下午5:17:41
 */
public class JClip implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//输出值主键
	private Integer dstFieldId;
	//输出值来源
	private Integer dstOrigin;
	//输入值主键
	private Integer srcFieldId;
	//输入值来源
	private Integer srcOrigin;
	//裁剪类型
	private Integer type;
	//裁剪开始位置
	private Integer startLocation;
	//裁剪结束位置
	private Integer endLocation;
	//排序索引
	private Integer index;
	public Integer getDstFieldId() {
		return dstFieldId;
	}
	public void setDstFieldId(Integer dstFieldId) {
		this.dstFieldId = dstFieldId;
	}
	public Integer getDstOrigin() {
		return dstOrigin;
	}
	public void setDstOrigin(Integer dstOrigin) {
		this.dstOrigin = dstOrigin;
	}
	public Integer getSrcFieldId() {
		return srcFieldId;
	}
	public void setSrcFieldId(Integer srcFieldId) {
		this.srcFieldId = srcFieldId;
	}
	public Integer getSrcOrigin() {
		return srcOrigin;
	}
	public void setSrcOrigin(Integer srcOrigin) {
		this.srcOrigin = srcOrigin;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getStartLocation() {
		return startLocation;
	}
	public void setStartLocation(Integer startLocation) {
		this.startLocation = startLocation;
	}
	public Integer getEndLocation() {
		return endLocation;
	}
	public void setEndLocation(Integer endLocation) {
		this.endLocation = endLocation;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	@Override
	public String toString() {
		return "JClip [dstFieldId=" + dstFieldId + ", dstOrigin=" + dstOrigin
				+ ", srcFieldId=" + srcFieldId + ", srcOrigin=" + srcOrigin
				+ ", type=" + type + ", startLocation=" + startLocation
				+ ", endLocation=" + endLocation + ", index=" + index + "]";
	}
}
