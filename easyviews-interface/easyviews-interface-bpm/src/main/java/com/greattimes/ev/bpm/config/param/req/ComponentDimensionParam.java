package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 组件维度保存请求实体
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
@ApiModel
public class ComponentDimensionParam implements Serializable {

	/**
	 */
	private static final long serialVersionUID = -4285379243476143321L;
	/**
	 * 组件id
	 */
	@ApiModelProperty("组件id")
	private Integer componentId;
	/**
	 * 维度数组
	 */
	@ApiModelProperty("维度数组")
	private List<DimensionParam> dimension;

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public List<DimensionParam> getDimension() {
		return dimension;
	}

	public void setDimension(List<DimensionParam> dimension) {
		this.dimension = dimension;
	}

}
