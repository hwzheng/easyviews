package com.greattimes.ev.bpm.trace.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/7/1 9:43
 */
public class FilterDimensionParam  implements Serializable{
    private static final long serialVersionUID = -8768428724373434554L;
    /**
     * 维度key
     */
    private String dimension;
    /**
     * 字段筛选规则（1包含，2不包含，3等于，4不等于 5字符串包含 ，6 字符串不包含）
     */
    private Integer rule;
    /**
     * Array[string] 值
     */
    private List<String> value;

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public Integer getRule() {
        return rule;
    }

    public void setRule(Integer rule) {
        this.rule = rule;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "FilterDimensionParam{" +
                "dimension='" + dimension + '\'' +
                ", rule=" + rule +
                ", value=" + value +
                '}';
    }
}
