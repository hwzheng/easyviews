package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警 维度 表
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
@TableName("bpm_alarm_dimension")
public class AlarmDimension extends Model<AlarmDimension> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 告警维度
     */
	private Integer dimensionId;
    /**
     * 告警id
     */
	private Integer alarmId;
	/**
	 * 告警 0 默认 1 统计维度
	 */
	private Integer type;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "AlarmDimension{" +
				"id=" + id +
				", dimensionId=" + dimensionId +
				", alarmId=" + alarmId +
				", type=" + type +
				'}';
	}
}
