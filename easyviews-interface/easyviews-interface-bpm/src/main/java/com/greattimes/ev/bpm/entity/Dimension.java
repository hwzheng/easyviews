package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 协议可选维度配置表
 * </p>
 *
 * @author cgc
 * @since 2019-02-14
 */
@TableName("bpm_dimension")
public class Dimension extends Model<Dimension> {

	private static final long serialVersionUID = 1L;
	@NotNull(message = "字段编码不能为空！")
	private Integer id;
	@NotEmpty(message = "字段名称不能为空！")
	@Length(min = 1, max = 16, message = "字段名称不能超过16位")
	private String name;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Dimension{" + "id=" + id + ", name=" + name + "}";
	}
}
