package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 通用分组参数
 * @author MS
 * @date   2018 上午9:56:40
 */
public class JCommon implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//开始定位
	private JCommonParam begin;
	//结束定位
	private JCommonParam end;
	//自定义分组处理器类名
	private String customHandler;
	
	public JCommonParam getBegin() {
		return begin;
	}
	public void setBegin(JCommonParam begin) {
		this.begin = begin;
	}
	public JCommonParam getEnd() {
		return end;
	}
	public void setEnd(JCommonParam end) {
		this.end = end;
	}
	public String getCustomHandler() {
		return customHandler;
	}
	public void setCustomHandler(String customHandler) {
		this.customHandler = customHandler;
	}
	@Override
	public String toString() {
		return "JCommon [begin=" + begin + ", end=" + end + ", customHandler="
				+ customHandler + "]";
	}
}
