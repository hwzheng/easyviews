package com.greattimes.ev.bpm.custom.param.resp;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义分析配置筛选条件和成功条件包装类
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
@ApiModel
public class CustomBasicParam extends Model<CustomBasicParam> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty("id")
	private Integer id;
	/**
	 * 过滤类型 0 普通 1 表达式
	 */
	@ApiModelProperty("过滤类型 0 普通 1 表达式")
	private Integer type;
	/**
	 * 表达式
	 */
	@ApiModelProperty("过滤表达式")
	private String expressions;
	/**
	 * 关系 (过滤类型为1时有效)  0  and  ，1 or 
	 */
	@ApiModelProperty("关系 (过滤类型为0时有效)  0  and  ，1 or ")
	private Integer relation;
	@ApiModelProperty("条件数组")
	private List<CustomFieldValueParam> fieldValue;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getExpressions() {
		return expressions;
	}

	public void setExpressions(String expressions) {
		this.expressions = expressions;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public List<CustomFieldValueParam> getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(List<CustomFieldValueParam> fieldValue) {
		this.fieldValue = fieldValue;
	}

	@Override
	protected Serializable pkVal() {
		return null;
	}

}
