package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 告警组件表
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@TableName("bpm_alarm_component")
public class AlarmComponent extends Model<AlarmComponent> {

	private static final long serialVersionUID = 1L;

	/**
	 * 告警id
	 */
	private Integer alarmId;
	/**
	 * 组件id
	 */
	private Integer componentId;

	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@Override
	public String toString() {
		return "AlarmComponent{" + "alarmId=" + alarmId + ", componentId=" + componentId + "}";
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.alarmId;
	}
}
