package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 合并操作out
 * @author MS
 * @date   2018 下午5:12:52
 */
public class JMergeOut implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//输出值主键
	private  Integer fieldId;
	//输出值来源
	private Integer origin;
	
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	public Integer getOrigin() {
		return origin;
	}
	public void setOrigin(Integer origin) {
		this.origin = origin;
	}
	
	@Override
	public String toString() {
		return "JMergeOut [fieldId=" + fieldId + ", origin=" + origin + "]";
	}
	
}
