package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警维度值表
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
@TableName("bpm_alarm_filter_value")
public class AlarmFilterValue extends Model<AlarmFilterValue> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 告警维度id
     */
	private Integer alarmDimensionId;
    /**
     * 维度值
     */
	private String value;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlarmDimensionId() {
		return alarmDimensionId;
	}

	public void setAlarmDimensionId(Integer alarmDimensionId) {
		this.alarmDimensionId = alarmDimensionId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmFilterValue{" +
			"id=" + id +
			", alarmDimensionId=" + alarmDimensionId +
			", value=" + value +
			"}";
	}
}
