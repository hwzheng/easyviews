package com.greattimes.ev.bpm.datasources.param.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/7/11 06:28
 */
@ApiModel
public class IndicatorsParam implements Serializable{

    private static final long serialVersionUID = -1317650088394949076L;
    /**
     * 指标id
     */
    private Integer id;
    /**
     * 指标名称
     */
    @ApiModelProperty("指标名称")
    private String name;
    /**
     * 指标单位
     */
    @ApiModelProperty("指标单位")
    private Integer unit;
    /**
     * 指标显示顺序
     */
    @ApiModelProperty("指标显示顺序")
    private Integer sort;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
