package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotEmpty;

/**
 * 协议详情
 * @author MS
 * @date   2018 上午10:56:13
 */
public class JProtocolDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5333259726882187994L;
	//协议主键
	private Integer protocolId;
	//协议标识
	@NotEmpty(message="protocolKey不能为空！")
	private String protocolKey;
	//协议类型编码
	private String protocolCode;
	//报文类型（报文格式）
	private String messageType;
	//协议描述
	@NotEmpty(message="协议名称不能为空！")
	private String desc;
	//传输模式 0：同步、1：异步
	private Integer transportMode;
	//是否为内置协议 	内置协议不支持启用、停用和删除，仅允许使用或作为模板 目前阶段始终为false
	private Boolean isBuiltin;
	//协议状态  -1：删除、0：停用、1：启用
	private Integer state;
	//交易判定依据
	private JJudgement tradeJudgement;
	//协议参数
	private JParams params;
	//协议分组
	private List<JParseGroup> parseGroup;
	//解析扩展
	private List<JExtend> extend;
	//协议变量
	private List<JVariable> variable;
	//协议规则
	private List<JRule> rule;
	//客户端原始报文
	private String clientSendBytes;
	//服务端原始报文
	private String serverSendBytes;
	//错误提示  0 正确 1 参数配置错误  2 分组配置
	private Integer error;
	
	public Integer getError() {
		return error;
	}
	public void setError(Integer error) {
		this.error = error;
	}
	public Integer getProtocolId() {
		return protocolId;
	}
	public void setProtocolId(Integer protocolId) {
		this.protocolId = protocolId;
	}
	public String getProtocolKey() {
		return protocolKey;
	}
	public void setProtocolKey(String protocolKey) {
		this.protocolKey = protocolKey;
	}
	public String getProtocolCode() {
		return protocolCode;
	}
	public void setProtocolCode(String protocolCode) {
		this.protocolCode = protocolCode;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public Integer getTransportMode() {
		return transportMode;
	}
	public void setTransportMode(Integer transportMode) {
		this.transportMode = transportMode;
	}
	public Boolean getIsBuiltin() {
		return isBuiltin;
	}
	public void setIsBuiltin(Boolean isBuiltin) {
		this.isBuiltin = isBuiltin;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public JParams getParams() {
		return params;
	}
	public void setParams(JParams params) {
		this.params = params;
	}
	public List<JParseGroup> getParseGroup() {
		return parseGroup;
	}
	public void setParseGroup(List<JParseGroup> parseGroup) {
		this.parseGroup = parseGroup;
	}
	public List<JExtend> getExtend() {
		return extend;
	}
	public void setExtend(List<JExtend> extend) {
		this.extend = extend;
	}
	public List<JVariable> getVariable() {
		return variable;
	}
	public void setVariable(List<JVariable> variable) {
		this.variable = variable;
	}
	public List<JRule> getRule() {
		return rule;
	}
	public void setRule(List<JRule> rule) {
		this.rule = rule;
	}
	
	public String getClientSendBytes() {
		return clientSendBytes;
	}
	public void setClientSendBytes(String clientSendBytes) {
		this.clientSendBytes = clientSendBytes;
	}
	public String getServerSendBytes() {
		return serverSendBytes;
	}
	public void setServerSendBytes(String serverSendBytes) {
		this.serverSendBytes = serverSendBytes;
	}
	
	public JJudgement getTradeJudgement() {
		return tradeJudgement;
	}
	public void setTradeJudgement(JJudgement tradeJudgement) {
		this.tradeJudgement = tradeJudgement;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@Override
	public String toString() {
		return "JProtocolDetail [protocolId=" + protocolId + ", protocolKey="
				+ protocolKey + ", protocolCode=" + protocolCode
				+ ", messageType=" + messageType + ", desc=" + desc
				+ ", transportMode=" + transportMode + ", isBuiltin="
				+ isBuiltin + ", state=" + state + ", tradeJudgement="
				+ tradeJudgement + ", params=" + params + ", parseGroup="
				+ parseGroup + ", extend=" + extend + ", variable=" + variable
				+ ", rule=" + rule + ", clientSendBytes=" + clientSendBytes
				+ ", serverSendBytes=" + serverSendBytes + "]";
	}
	
}
