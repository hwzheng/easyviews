package com.greattimes.ev.bpm.service.datasources;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.datasources.param.req.ComponentIndicatorsParam;
import com.greattimes.ev.bpm.datasources.param.req.IndicatorGroupParam;
import com.greattimes.ev.bpm.entity.*;

/**
 * <p>
 * 数据源配置表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
public interface IDataSourcesService extends IService<DataSources> {

	/**
	 * 数据源组件查询
	 *
	 * @param intValue
	 * @return
	 */
	List<Map<String, Object>> selectComponent(Integer sourcesId);

	/**
	 * 数据源组件添加
	 *
	 * @param sourcesId
	 * @param treeId
	 * @param title
	 */
	int addComponent(Integer sourcesId, Integer treeId, String title);

	/**
	 * 数据源组件删除
	 *
	 * @param sourcesId
	 */
	void deleteComponent(Integer sourcesId);

	/**
	 * 校验组件名
	 *
	 * @param ds
	 * @param i
	 * @return
	 */
	Boolean checkName(Dscomponent ds, int i);

	/**
	 * 数据源组件编辑
	 *
	 * @param sourcesId
	 * @param treeId
	 * @param title
	 */
	void editComponent(Integer sourcesId, Integer treeId, String title);

	/**
	 * 指标组下拉列表查询
	 *
	 * @param intValue
	 * @return
	 */
	List<ComponentIndicatorsParam> selectIndicatorGroup(Integer sourcesId);

	/**
	 * 指标组列表查询
	 *
	 * @param intValue
	 * @param intValue2
	 * @return
	 */
	List<ComponentIndicatorsParam> selectContraint(Integer componentId);

	/**
	 * 指标同步状态更改
	 *
	 * @param active
	 * @param id
	 */
	void updateActive(Integer id, Integer active);

	/**
	 * @param param
	 * @return
	 */
	void addContraint(ComponentIndicatorsParam param);

	/**
	 * 数据配置编辑
	 *
	 * @param param
	 */
	void editContraint(ComponentIndicatorsParam param);

	/**
	 * 数据配置删除
	 *
	 * @param intValue
	 */
	void deleteContraint(int intValue);

	/**
	 * 判断有没有子节点
	 *
	 * @param componentId
	 * @param sourcesId
	 * @return
	 */
	boolean checkChildren(Integer componentId, Integer sourcesId);

	/***
	 * 查询
	 * @author NJ
	 * @date 2018/7/10 17:01
	 * @param
	 * @return java.util.List<com.greattimes.ev.bpm.entity.DataSources>
	 */
	List<Map<String, Object>> select();

	/**
	 * 按照id删除数据源
	 *
	 * @param id
	 * @author NJ
	 * @date 2018/7/10 17:26
	 */
	void deleteDataSourceById(int id);

	/**
	 * 新增数据源
	 *
	 * @param dataSources
	 * @param userId
	 * @return com.greattimes.ev.bpm.entity.DataSources
	 * @author NJ
	 * @date 2018/7/12 15:20
	 */
	DataSources saveOrUpdate(DataSources dataSources, int userId);

	/**
	 * 根据数据源id查找指标组列表
	 *
	 * @param sourcesId
	 * @return java.util.List<java.util.List < java.util.Map < java.lang.String , java.lang.Object>>>
	 * @author NJ
	 * @date 2018/7/11 11:17
	 */
	List<Map<String, Object>> selectIndicatorGroupBySourcesId(int sourcesId);

	/**
	 * 指标组保存 返回值为新增的id
	 *
	 * @param indicatorGroupParam
	 * @return int
	 * @author NJ
	 * @date 2018/7/11 15:29
	 */
	int saveOrUpdateIndicator(IndicatorGroupParam indicatorGroupParam);

	/**
	 * 指标组查询
	 *
	 * @param id
	 * @return com.greattimes.ev.bpm.config.param.req.IndicatorGroupParam
	 * @author NJ
	 * @date 2018/7/11 15:34
	 */
	IndicatorGroupParam indicatorDetail(int id);

	/**
	 * 是否含有相同的指标组名
	 *
	 * @param name
	 * @param id
	 * @return boolean
	 * @author NJ
	 * @date 2018/7/12 10:14
	 */
	boolean isHasSameIndicatorGroupName(String name, int id, int sourecsId);

	/**
	 * 删除指标组
	 *
	 * @param id
	 * @return int
	 * @author NJ
	 * @date 2018/7/12 11:43
	 */
	int deleteGroupById(int id);

	/**
	 * 是否含有相同的数据源名称
	 *
	 * @param name
	 * @param id
	 * @return boolean
	 * @author NJ
	 * @date 2018/7/12 15:51
	 */
	boolean isHasSameDataSourceName(String name, int id);


	/**
	 * 根据指标组id查询相应的指标
	 *
	 * @param groupId
	 * @return
	 */
	List<Indicators> selectDsIndicatorsByGroupId(int groupId);

	/**
	 * 根据指标组id查询相应的指标,并将其放入name作为key,id作为value的map中
	 *
	 * @param groupId
	 * @return
	 */
	Map<String, Integer> selectDsIndicatorsMapByGroupId(int groupId);

	/**
	 * 批量添加数据源约束
	 *
	 * @param indicatorContraints
	 */
	void insertBatchDsIndicatorContraint(List<IndicatorContraint> indicatorContraints);

	/**
	 * 根据指标组id查询数据源关联关系
	 * @param id
	 * @return
	 */
	List<IndicatorContraint> selectIndicatorContraintByGroupId(int id);


	/**
	 * 根据相应参数查询数据源指标约束
	 * @param componentId
	 * @param dataSourceId
	 * @return
	 */
	 List<DictionaryParam> selectRelevantContraintDict(int componentId, int dataSourceId, int groupId);

	 /**
	  * 根据参数删除
	  * @author NJ
	  * @date 2018/8/23 16:37
	  * @param
	  * @return void
	  */
	 void deleteIndicatorContraintByMap(Map<String,Object> map);

}
