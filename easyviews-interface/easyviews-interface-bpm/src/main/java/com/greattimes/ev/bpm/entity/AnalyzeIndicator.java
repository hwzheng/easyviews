package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 自定义分析指标
 * </p>
 *
 * @author cgc
 * @since 2018-06-27
 */
@TableName("ct_analyze_indicator")
public class AnalyzeIndicator extends Model<AnalyzeIndicator> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 统计指标
     */
	private Integer indicatorId;
    /**
     * 自定义分析id
     */
	private Integer customId;
    /**
     * 别名
     */
	private String othername;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public String getOthername() {
		return othername;
	}

	public void setOthername(String othername) {
		this.othername = othername;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AnalyzeIndicator{" +
			"id=" + id +
			", indicatorId=" + indicatorId +
			", customId=" + customId +
			", othername=" + othername +
			"}";
	}
}
