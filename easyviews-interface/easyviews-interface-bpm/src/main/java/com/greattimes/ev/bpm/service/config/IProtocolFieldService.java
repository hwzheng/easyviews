package com.greattimes.ev.bpm.service.config;

import com.greattimes.ev.bpm.entity.ProtocolField;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 协议字段表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-10-22
 */
public interface IProtocolFieldService extends IService<ProtocolField> {

}
