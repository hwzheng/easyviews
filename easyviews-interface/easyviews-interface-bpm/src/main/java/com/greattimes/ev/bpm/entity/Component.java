package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 应用组件表
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
@TableName("bpm_component")
@ApiModel
public class Component extends Model<Component> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.INPUT)
	@ApiModelProperty("组件id")
	private Integer id;
    /**
     * 应用id
     */
	@ApiModelProperty("应用id")
	private Integer applicationId;
    /**
     * 名称
     */
	@ApiModelProperty("名称")
	private String name;
    /**
     * 激活状态0 否 1是  
     */
	@ApiModelProperty("激活状态0 否 1是 ")
	private Integer active;
    /**
     * 组件数据统计模式1 组件 2 ip 3端口 
     */
	@ApiModelProperty("组件数据统计模式1 组件 2 ip 3端口 ")
	private Integer statisticalMode;
    /**
     * 连接模式（常规tcp 0，tcp长连接 1，异步双工长连接 2）
     */
	@ApiModelProperty("连接模式（常规tcp 0，tcp长连接 1，异步双工长连接 2）")
	private Integer connectMode;
    /**
     * 协议
     */
	@ApiModelProperty("协议")
	private Integer prototcolId;
    /**
     * 数据来源  0:流量1 ：日志
     */
	@ApiModelProperty("数据来源  0:流量1 ：日志")
	private Integer dataSource;
    /**
     * vlan
     */
	@ApiModelProperty("vlan")
	private Integer vlan;
    /**
     * 中心
     */
	@ApiModelProperty("中心")
	private Integer centerId;
    /**
     * 二级分组
     */
	@ApiModelProperty("二级分组")
	private Integer group;
    /**
     * 组件数据是否参与业务统计 0 否 1是
     */
	@ApiModelProperty("组件数据是否参与业务统计 0 否 1是")
	private Integer isStatistics;
    /**
     * 排序
     */
	@ApiModelProperty("排序")
	private Integer sort;
    /**
     * 组件组
     */
	@ApiModelProperty("组件组")
	private Integer component;
    /**
     * 服务模式 0 服务端 1 客户端
     */
	@ApiModelProperty("服务模式 0 服务端 1 客户端")
	private Integer serviceType;

	/**
	 * 协议方向 0 普通 1反向 2 会话
	 */
	private Integer protocolType;

	/**
	 * 配对方式 0 全局流水号  1  时序流水号 2 session时序流水号
	 */
	private Integer patternType;
	/**
	 * 前 配对字段
	 */
	private Integer patternFieldUp;
	/**
	 * 后  配对字段
	 */
	private Integer patternFieldDown;

	/**
	 * 响应模式（ 1 服务端  0 客户端）
	 */
	private Integer responseType;
	/**
	 * 报文发送：0否 1是
	 */
	private Integer sendMsgBody;
	/**
	 * 报文长度限制 默认2048
	 */
	private Integer msgLimit;
	/**
	 * 解码日志级别  1 debug 2 info 3 warn4 error
	 */
	private Integer logLevel;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getStatisticalMode() {
		return statisticalMode;
	}

	public void setStatisticalMode(Integer statisticalMode) {
		this.statisticalMode = statisticalMode;
	}

	public Integer getConnectMode() {
		return connectMode;
	}

	public void setConnectMode(Integer connectMode) {
		this.connectMode = connectMode;
	}

	public Integer getPrototcolId() {
		return prototcolId;
	}

	public void setPrototcolId(Integer prototcolId) {
		this.prototcolId = prototcolId;
	}

	public Integer getDataSource() {
		return dataSource;
	}

	public void setDataSource(Integer dataSource) {
		this.dataSource = dataSource;
	}

	public Integer getVlan() {
		return vlan;
	}

	public void setVlan(Integer vlan) {
		this.vlan = vlan;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}

	public Integer getGroup() {
		return group;
	}

	public void setGroup(Integer group) {
		this.group = group;
	}

	public Integer getIsStatistics() {
		return isStatistics;
	}

	public void setIsStatistics(Integer isStatistics) {
		this.isStatistics = isStatistics;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getComponent() {
		return component;
	}

	public void setComponent(Integer component) {
		this.component = component;
	}

	public Integer getServiceType() {
		return serviceType;
	}

	public void setServiceType(Integer serviceType) {
		this.serviceType = serviceType;
	}

	public Integer getProtocolType() {
		return protocolType;
	}

	public void setProtocolType(Integer protocolType) {
		this.protocolType = protocolType;
	}

	public Integer getPatternType() {
		return patternType;
	}

	public void setPatternType(Integer patternType) {
		this.patternType = patternType;
	}

	public Integer getPatternFieldUp() {
		return patternFieldUp;
	}

	public void setPatternFieldUp(Integer patternFieldUp) {
		this.patternFieldUp = patternFieldUp;
	}

	public Integer getPatternFieldDown() {
		return patternFieldDown;
	}

	public void setPatternFieldDown(Integer patternFieldDown) {
		this.patternFieldDown = patternFieldDown;
	}

	public Integer getResponseType() {
		return responseType;
	}

	public void setResponseType(Integer responseType) {
		this.responseType = responseType;
	}

	public Integer getSendMsgBody() {
		return sendMsgBody;
	}

	public void setSendMsgBody(Integer sendMsgBody) {
		this.sendMsgBody = sendMsgBody;
	}

	public Integer getMsgLimit() {
		return msgLimit;
	}

	public void setMsgLimit(Integer msgLimit) {
		this.msgLimit = msgLimit;
	}

	public Integer getLogLevel() {
		return logLevel;
	}

	public void setLogLevel(Integer logLevel) {
		this.logLevel = logLevel;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Component{" +
				"id=" + id +
				", applicationId=" + applicationId +
				", name='" + name + '\'' +
				", active=" + active +
				", statisticalMode=" + statisticalMode +
				", connectMode=" + connectMode +
				", prototcolId=" + prototcolId +
				", dataSource=" + dataSource +
				", vlan=" + vlan +
				", centerId=" + centerId +
				", group=" + group +
				", isStatistics=" + isStatistics +
				", sort=" + sort +
				", component=" + component +
				", serviceType=" + serviceType +
				", protocolType=" + protocolType +
				", patternType=" + patternType +
				", patternFieldUp=" + patternFieldUp +
				", patternFieldDown=" + patternFieldDown +
				", responseType=" + responseType +
				", sendMsgBody=" + sendMsgBody +
				", msgLimit=" + msgLimit +
				", logLevel=" + logLevel +
				'}';
	}
}
