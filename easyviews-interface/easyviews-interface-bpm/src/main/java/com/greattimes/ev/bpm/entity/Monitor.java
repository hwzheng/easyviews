package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 监控点表
 * </p>
 *
 * @author NJ
 * @since 2018-08-07
 */
@TableName("bpm_monitor")
public class Monitor extends Model<Monitor> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.INPUT)
	private Integer id;
    /**
     * 名称
     */
	private String name;
    /**
     * 激活状态：0 关闭 1开启
     */
	private Integer active;
    /**
     * 排序: 
     */
	private Integer sort;
    /**
     * 应用id
     */
	private Integer applicationid;

	/**
	 * 组件数量
	 */
	private Integer componentId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getApplicationid() {
		return applicationid;
	}

	public void setApplicationid(Integer applicationid) {
		this.applicationid = applicationid;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Monitor{" +
				"id=" + id +
				", name='" + name + '\'' +
				", active=" + active +
				", sort=" + sort +
				", applicationid=" + applicationid +
				", componentId=" + componentId +
				'}';
	}
}
