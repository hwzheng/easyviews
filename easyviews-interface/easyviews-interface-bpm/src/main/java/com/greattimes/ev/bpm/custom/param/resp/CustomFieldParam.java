package com.greattimes.ev.bpm.custom.param.resp;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义配置维度和协议查询包装类
 * </p>
 *
 * @author Cgc
 * @since 2018-05-23
 */
@ApiModel
public class CustomFieldParam extends Model<CustomFieldParam> {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("字段id")
	private Integer fieldId;
	@ApiModelProperty("名称")
	private String name;
	@ApiModelProperty("排序")
	private Integer sort;
	@ApiModelProperty("别名")
	private String otherName;

	@Override
	protected Serializable pkVal() {
		return null;
	}

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

}
