package com.greattimes.ev.bpm.analysis.param.req;


import java.io.Serializable;

import java.util.Date;
import java.util.List;

/**
 * @author NJ
 * @date 2019/3/28 13:08
 */
public class ReportChartParam implements Serializable{
    private static final long serialVersionUID = 8439758096120126420L;

    /**
     * 报表id
     */
    private Integer reportId;
    /**
     * 图表id
     */
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 图标类型 1 折线 ，2 柱状 ，3 ，饼图 ，4 ，表格，5  柱状top
     */
    private Integer type;
    /**
     * 开始时间
     */
    private Date startTime;
    /**
     * 结束时间
     */
    private Date endTime;
    /**
     * 时间类型 1 动态时间  2 静态时间
     */
    private Integer timeType;
    /**
     * 动态时间  0 今日， 1昨日   ，2上周  ，3上月   ，4过去7天，5过去14天，6过去30天，7过去180天
     */
    private Integer dynTime;
    /**
     * 对比  0 无对比 ， 1 上周期
     */
    private Integer compare;
    /**
     * 限制条数
     */
    private Integer limit;
    /**
     * 排序方式 0 升序 1 降序
     */
    private Integer sortType;
    /**
     * 共享人员的id
     */
    private List<Integer> userIds;

    /**
     * 维度数组
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    private List<ReportDimensionParam> dimensions;
    /**
     *  dimensionId	int	是	纬度id
     *  rule	Int	是	规则
     *  dimensionValue	String	是	维度值
     */
    private List<ReportDimensionParam> rules;
    /**
     * 指标id数组
     */
    private List<Integer> indicators;
    /**
     * 数据配置表
     */
    private List<ReportChartDataParam> chartData;
    /**
     * 数据颗粒度
     */
    private Integer interval;
    /**
     * 数据类型 1 应用 2 事件 3 漏斗
     */
    private Integer dataType;
    /**
     * 是否维度 0 否 1是
     */
    private Integer isDimension;
    /**
     * 报表创建人
     */
    private Integer createBy;
    /**
     * 动态结束时间
     */
    private Date end;
    /**
     * 动态开始时间
     */
    private Date start;

    /**
     * 是否时间维度 0 否 1 是
     */
    private Integer isTimeDimension;
    /**
     * 自定义开始时间
     */
    private Date customStartTime;
    /**
     * 自定义结束时间
     */
    private Date customEndTime;
    /**
     * 指标排序(指标id) 仅限表格类型
     */
    private Integer orderId;

    /**
     * 排序字段的
     */
    private String orderField;

    /**
     * 排序类型
     */
    private Integer orderType;

    /**
     * 表格列
     */
    List<String> fields;
    /**
     * 数字是否显示
     */
    private Integer dataShow;
    /**
     * 排序方式
     */
    private Integer alignType;

    /**
     * 图表类目 1趋势 2、类目 3、占比 4、数值 5、表格
     */
    private Integer chartClassify;

    /**
     * 图表数据展示类型 1、正常 2、堆叠 3、百分比
     */
    private Integer chartDataType;

    private List<ReportChartIndicatorFilterParam> filterIndicator;

    /**
     * 新增报表自定义颜色
     * @author nj
     * @date 2020-01-13
     */
    private List<String> inputColorList;

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getDynTime() {
        return dynTime;
    }

    public void setDynTime(Integer dynTime) {
        this.dynTime = dynTime;
    }

    public Integer getCompare() {
        return compare;
    }

    public void setCompare(Integer compare) {
        this.compare = compare;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getSortType() {
        return sortType;
    }

    public void setSortType(Integer sortType) {
        this.sortType = sortType;
    }

    public List<Integer> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Integer> userIds) {
        this.userIds = userIds;
    }

    public List<ReportDimensionParam> getDimensions() {
        return dimensions;
    }

    public void setDimensions(List<ReportDimensionParam> dimensions) {
        this.dimensions = dimensions;
    }

    public List<ReportDimensionParam> getRules() {
        return rules;
    }

    public void setRules(List<ReportDimensionParam> rules) {
        this.rules = rules;
    }

    public List<Integer> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Integer> indicators) {
        this.indicators = indicators;
    }

    public List<ReportChartDataParam> getChartData() {
        return chartData;
    }

    public void setChartData(List<ReportChartDataParam> chartData) {
        this.chartData = chartData;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getIsDimension() {
        return isDimension;
    }

    public void setIsDimension(Integer isDimension) {
        this.isDimension = isDimension;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Integer getIsTimeDimension() {
        return isTimeDimension;
    }

    public void setIsTimeDimension(Integer isTimeDimension) {
        this.isTimeDimension = isTimeDimension;
    }

    public Date getCustomStartTime() {
        return customStartTime;
    }

    public void setCustomStartTime(Date customStartTime) {
        this.customStartTime = customStartTime;
    }

    public Date getCustomEndTime() {
        return customEndTime;
    }

    public void setCustomEndTime(Date customEndTime) {
        this.customEndTime = customEndTime;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public List<ReportChartIndicatorFilterParam> getFilterIndicator() {
        return filterIndicator;
    }

    public void setFilterIndicator(List<ReportChartIndicatorFilterParam> filterIndicator) {
        this.filterIndicator = filterIndicator;
    }

    public String getOrderField() {
        return orderField;
    }

    public void setOrderField(String orderField) {
        this.orderField = orderField;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public Integer getDataShow() {
        return dataShow;
    }

    public void setDataShow(Integer dataShow) {
        this.dataShow = dataShow;
    }

    public Integer getAlignType() {
        return alignType;
    }

    public void setAlignType(Integer alignType) {
        this.alignType = alignType;
    }


    public Integer getChartClassify() {
        return chartClassify;
    }

    public void setChartClassify(Integer chartClassify) {
        this.chartClassify = chartClassify;
    }

    public Integer getChartDataType() {
        return chartDataType;
    }

    public void setChartDataType(Integer chartDataType) {
        this.chartDataType = chartDataType;
    }

    public List<String> getInputColorList() {
        return inputColorList;
    }

    public void setInputColorList(List<String> inputColorList) {
        this.inputColorList = inputColorList;
    }

    @Override
    public String toString() {
        return "ReportChartParam{" +
                "reportId=" + reportId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", type=" + type +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", timeType=" + timeType +
                ", dynTime=" + dynTime +
                ", compare=" + compare +
                ", limit=" + limit +
                ", sortType=" + sortType +
                ", userIds=" + userIds +
                ", dimensions=" + dimensions +
                ", rules=" + rules +
                ", indicators=" + indicators +
                ", chartData=" + chartData +
                ", interval=" + interval +
                ", dataType=" + dataType +
                ", isDimension=" + isDimension +
                ", createBy=" + createBy +
                ", end=" + end +
                ", start=" + start +
                ", isTimeDimension=" + isTimeDimension +
                ", customStartTime=" + customStartTime +
                ", customEndTime=" + customEndTime +
                ", orderId=" + orderId +
                ", orderField='" + orderField + '\'' +
                ", orderType=" + orderType +
                ", fields=" + fields +
                ", dataShow=" + dataShow +
                ", alignType=" + alignType +
                ", chartClassify=" + chartClassify +
                ", chartDataType=" + chartDataType +
                ", filterIndicator=" + filterIndicator +
                ", inputColorList=" + inputColorList +
                '}';
    }

}
