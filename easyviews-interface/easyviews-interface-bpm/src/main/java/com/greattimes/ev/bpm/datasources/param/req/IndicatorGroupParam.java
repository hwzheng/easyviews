package com.greattimes.ev.bpm.datasources.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/7/11 13:02
 */
@ApiModel
public class IndicatorGroupParam implements Serializable{

    private static final long serialVersionUID = 3331443414437219797L;

    /**
     * 主键
     */
    @ApiModelProperty("主键id")
    private Integer id;
    /**
     * 组名称
     */
    @ApiModelProperty("组名称")
    private String name;
    /**
     * 约束层级
     */
    @ApiModelProperty("约束层级")
    private Integer level;
    /**
     * 数据颗粒度
     */
    @ApiModelProperty("数据颗粒度")
    private Integer interval;
    /**
     * 单位
     */
    @ApiModelProperty("单位")
    private Integer unit;

    /**
     * 单位名称
     */
    @ApiModelProperty("单位名称")
    private String unitName;


    /**
     * 数据源id
     */
    @ApiModelProperty("数据源id")
    private Integer sourcesId;

    /**
     * 约束,
     * name	String
     * sort	int
     */
    @ApiModelProperty("约束")
    private List<ConstraintFieldParam> constraints;

    /**
     * 指标
     * name	String
     * unit	int
     * sort	int
     */
    @ApiModelProperty("指标")
    private List<IndicatorsParam> indicators;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public Integer getSourcesId() {
        return sourcesId;
    }

    public void setSourcesId(Integer sourcesId) {
        this.sourcesId = sourcesId;
    }

    public List<ConstraintFieldParam> getConstraints() {
        return constraints;
    }

    public void setConstraints(List<ConstraintFieldParam> constraints) {
        this.constraints = constraints;
    }

    public List<IndicatorsParam> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<IndicatorsParam> indicators) {
        this.indicators = indicators;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

}
