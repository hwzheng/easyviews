package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 触发规则
 * @author MS
 * @date   2018 下午5:06:12
 */
public class JTriggerRule implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	//规则id
	private Integer id;
	
	//排序索引
	private Integer index;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	@Override
	public String toString() {
		return "JtriggerRule [id=" + id + ", index=" + index + "]";
	}
}
