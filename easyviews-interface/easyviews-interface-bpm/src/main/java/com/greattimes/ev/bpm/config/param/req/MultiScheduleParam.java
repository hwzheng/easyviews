package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 重复排期包装类
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
@ApiModel
public class MultiScheduleParam implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 应用id
	 */
	@ApiModelProperty("应用id")
	private Integer applicationId;
	/**
	 * 详情
	 */
	@ApiModelProperty("详情")
	private List<MultiScheduleDetailParam> multis;

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public List<MultiScheduleDetailParam> getMultis() {
		return multis;
	}

	public void setMultis(List<MultiScheduleDetailParam> multis) {
		this.multis = multis;
	}

}
