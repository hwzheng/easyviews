package com.greattimes.ev.bpm.service.datasources;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.datasources.param.req.DatasourceRelevantParam;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;
import com.greattimes.ev.bpm.entity.DatasourceRelevantDetail;
import com.greattimes.ev.bpm.entity.IndicatorContraint;
import com.greattimes.ev.bpm.entity.Indicators;
import com.greattimes.ev.bpm.entity.RelevantContraint;

/**
 * <p>
 * 数据源关联表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
public interface IDatasourceRelevantService extends IService<DatasourceRelevant> {

	/**
	 * 数据源配置新增
	 * @param userId
	 * @param relevant
	 */
	void add(int userId, DatasourceRelevantParam relevant);

	/**外部数据配置查询
	 * @param applicationId
	 * @return
	 */
	List<DatasourceRelevantParam> select(int applicationId);

	/**
	 * 外部数据配置删除
	 * @param id
	 */
	void delete(int id);

	/**
	 * 外部数据配置修改
	 * @param relevant
	 * @param userId
	 */
	void edit(DatasourceRelevantParam relevant, int userId);

	/**
	 * 根据配置id查询指标
	 * @param id
	 * @return
	 */
	List<Indicators> getIndicators(int id);

	/**
	 * 获取外部数据配置
	 * @param id
	 * @return
	 */
	DatasourceRelevant getDatasource(int id);

	/**
	 * 获取数据源详情
	 * @param dataSourceRelevantId
	 * @param indicator
	 * @return
	 */
	List<DatasourceRelevantDetail> selectDetail(int dataSourceRelevantId, int indicator);

	/**
	 * 查询指标
	 * @param id
	 * @param indicatorName
	 * @return
	 */
	List<DatasourceRelevantDetail> getIndicators(int groupId, String indicatorName);

	/**
	 * 批量插入详情表
	 * @param details
	 */
	void insertRelevantDetail(List<DatasourceRelevantDetail> details);

	/**是否显示维度
	 * @param intValue
	 * @param intValue2
	 */
	void updateActive(int intValue, int intValue2);

	/**
	 * 获取全部关联约束
	 * @param componentId
	 * @param indicatorGroupId
	 * @return
	 */
	List<RelevantContraint> slectRelevant(Integer componentId, Integer indicatorGroupId);

	/**获取指标约束
	 * @param indicatorGroupId
	 * @return
	 */
	List<IndicatorContraint> selectIndicator(Integer indicatorGroupId);

	/**
	 * 上传插入
	 * @param details
	 * @param datasourseId 
	 */
	void insert(List<RelevantContraint> details, int datasourseId);

	/**
	 * 数据源指标约束
	 * @param groupId
	 * @param i 0未关联 1 关联
	 * @return
	 */
	List<IndicatorContraint> selectIndicator(int groupId, int i);

	/**
	 * 根据数据源id删除
	 * @param id 数据源id
	 */
	void deleteRelevantBySourcesId(Integer id);

	/**
	 * 根据指标组id删除
	 * @param id
	 */
	void deleteRelevantByGroupId(Integer id);

	/**
	 * 根据组件id级联删除外部数据源
	 * @param ids
	 * @return
	 */
	boolean deleteDatasourceRelevantByComponentIds(List<Integer> ids);
	/**
	 * 根据约束id删除
	 * @param ids
	 */
	void deleteRelevantByFieldId(List<Integer> ids);

	
}
