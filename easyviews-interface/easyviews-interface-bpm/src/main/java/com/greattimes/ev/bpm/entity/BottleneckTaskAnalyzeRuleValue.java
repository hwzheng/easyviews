package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能瓶颈报表分析规则字段value表
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@TableName("rp_bottleneck_task_analyze_rule_value")
public class BottleneckTaskAnalyzeRuleValue extends Model<BottleneckTaskAnalyzeRuleValue> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 分析规则id
     */
	private Integer analyzeRuleId;
    /**
     * 值
     */
	private String value;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnalyzeRuleId() {
		return analyzeRuleId;
	}

	public void setAnalyzeRuleId(Integer analyzeRuleId) {
		this.analyzeRuleId = analyzeRuleId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "BottleneckTaskAnalyzeRuleValue{" +
			"id=" + id +
			", analyzeRuleId=" + analyzeRuleId +
			", value=" + value +
			"}";
	}
}
