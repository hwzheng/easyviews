package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;
import java.util.List;

import com.greattimes.ev.bpm.entity.AlarmSend;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class AlarmSendListParam implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 告警发送list
	 */
	@ApiModelProperty("告警发送信息List")
	private List<AlarmSend> alarmSend;

	public List<AlarmSend> getAlarmSend() {
		return alarmSend;
	}

	public void setAlarmSend(List<AlarmSend> alarmSend) {
		this.alarmSend = alarmSend;
	}

}
