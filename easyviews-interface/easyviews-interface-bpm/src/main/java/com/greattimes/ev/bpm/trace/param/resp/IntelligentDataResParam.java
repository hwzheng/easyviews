package com.greattimes.ev.bpm.trace.param.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/7/2 17:37
 */
public class IntelligentDataResParam implements Serializable{
    private static final long serialVersionUID = 1687139468751231595L;
    /**
     * 组件id
     */
    private Integer componentId;
    /**
     * 已查询总条数
     */
    private Integer total;

    /**
     * requuid
     * respUuid
     * timestamp
     * responseTime
     * responseState
     * transState
     * transChannel
     * transChannelTranslate
     * transType
     * transTypeTranslate
     * .....
     */
    List<Map<String, Object>> detail;

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Map<String, Object>> getDetail() {
        return detail;
    }

    public void setDetail(List<Map<String, Object>> detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "IntelligentDataResParam{" +
                "componentId=" + componentId +
                ", total=" + total +
                ", detail=" + detail +
                '}';
    }
}
