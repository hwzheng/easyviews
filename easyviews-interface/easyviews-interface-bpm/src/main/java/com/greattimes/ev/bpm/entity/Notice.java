package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 通知表
 * </p>
 *
 * @author cgc
 * @since 2018-11-20
 */
@TableName("sys_notice")
public class Notice extends Model<Notice> {

    private static final long serialVersionUID = 1L;

    /**
     * code
     */
	private Integer code;
    /**
     * 名称
     */
	private String name;
    /**
     * 类型 0大数据 1解码
     */
	private Integer type;
    /**
     * 消费状态 0未消费 1消费中2 消费成功 3消费失败
     */
	private Integer state;
    /**
     * 入库时间
     */
	private Date time;

	/**
     * 中心id
     */
	private Integer centerId;
	
	/**
	 * 页面传递的时间
	 */
	@TableField(exist = false)
	private Long timeStamp;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}

	public Long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Long timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	protected Serializable pkVal() {
		return this.code;
	}

	@Override
	public String toString() {
		return "Notice [code=" + code + ", name=" + name + ", type=" + type + ", state=" + state + ", time=" + time
				+ ", centerId=" + centerId + ", timeStamp=" + timeStamp + "]";
	}

}
