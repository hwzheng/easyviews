package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.util.Objects;

/**
 * <p>
 * 
 * </p>
 *
 * @author NJ
 * @since 2018-06-15
 */
@TableName("bpm_indicator")
public class Indicator extends Model<Indicator> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 指标名称
     */
	private String name;
    /**
     * 字段名称
     */
	private String columnName;
    /**
     * 字段类型
     */
	private String columnType;
    /**
     * 计算规则
     */
	private String rule;
    /**
     * 单位
     */
	private String unit;
    /**
     * 业务指标 ：0否， 1是
     */
	private Integer businessIndicator;
    /**
     * 别名
     */
	private String otherName;
    /**
	 * 1、BPM基础指标 2、扩展基础指标 3、网络指标 4、应用指标
     */
	private Integer type;

	/**
	 * 指标筛选规则（1大于，2小于，3等于，4不等于 ）
	 */
	@TableField(exist=false)
	private Integer computeRule;

	/**
	 * 模板编号
	 * 1.大于累计模板：过去X分钟指标Y，大于，阀值A  指标（Y）：单选，阈值（A）
	 * 2.大于颗粒度模板：过去X分钟内每分钟指标Y，大于，阀值A，发生Z次
	 * 3.小于累计模板：过去X分钟指标Y，小于，阀值A 指标（Y）：单选，阈值（A）
	 * 4.小于颗粒度模板：过去X分钟内每分钟指标Y，小于，阀值A，发生Z次 指标（Y）：单选，阈值（A），异常数（Z）：单选
	 */
	@TableField(exist=false)
	private Integer templateCode;

	/**
	 * 阈值
	 */
	@TableField(exist=false)
	private Double threshlod;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getColumnType() {
		return columnType;
	}

	public void setColumnType(String columnType) {
		this.columnType = columnType;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getBusinessIndicator() {
		return businessIndicator;
	}

	public void setBusinessIndicator(Integer businessIndicator) {
		this.businessIndicator = businessIndicator;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getComputeRule() {
		return computeRule;
	}

	public void setComputeRule(Integer computeRule) {
		this.computeRule = computeRule;
	}

	public Integer getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(Integer templateCode) {
		this.templateCode = templateCode;
	}

	public Double getThreshlod() {
		return threshlod;
	}

	public void setThreshlod(Double threshlod) {
		this.threshlod = threshlod;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Indicator indicator = (Indicator) o;
		return Objects.equals(id, indicator.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public String toString() {
		return "Indicator{" +
				"id=" + id +
				", name='" + name + '\'' +
				", columnName='" + columnName + '\'' +
				", columnType='" + columnType + '\'' +
				", rule='" + rule + '\'' +
				", unit='" + unit + '\'' +
				", businessIndicator=" + businessIndicator +
				", otherName='" + otherName + '\'' +
				", type=" + type +
				", computeRule=" + computeRule +
				", templateCode=" + templateCode +
				", threshlod=" + threshlod +
				'}';
	}
}
