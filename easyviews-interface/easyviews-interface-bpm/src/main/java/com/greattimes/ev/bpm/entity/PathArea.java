package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 路径图分组区域表
 * </p>
 *
 * @author NJ
 * @since 2018-09-11
 */
@TableName("path_area")
public class PathArea extends Model<PathArea> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * x (左侧边距)
     */
	private Integer left;
    /**
     * y（顶部边距）
     */
	private Integer top;
    /**
     * 节点样式 
     */
	private Integer nodeStyle;
    /**
     * 视图id
     */
	private Integer viewId;
    /**
     * 名称
     */
	private String name;
    /**
     * 高度
     */
	private Integer height;
    /**
     * 宽度
     */
	private Integer width;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLeft() {
		return left;
	}

	public void setLeft(Integer left) {
		this.left = left;
	}

	public Integer getTop() {
		return top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public Integer getNodeStyle() {
		return nodeStyle;
	}

	public void setNodeStyle(Integer nodeStyle) {
		this.nodeStyle = nodeStyle;
	}

	public Integer getViewId() {
		return viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PathArea{" +
			"id=" + id +
			", left=" + left +
			", top=" + top +
			", nodeStyle=" + nodeStyle +
			", viewId=" + viewId +
			", name=" + name +
			", height=" + height +
			", width=" + width +
			"}";
	}
}
