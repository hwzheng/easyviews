package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 协议维度表
 * </p>
 *
 * @author cgc
 * @since 2019-02-14
 */
@TableName("bpm_protocol_dimension")
public class ProtocolDimension extends Model<ProtocolDimension> {

    private static final long serialVersionUID = 1L;

    /**
     * 协议id
     */
	private Integer protocolId;
    /**
     * 维度id
     */
	private Integer dimensionId;


	public Integer getProtocolId() {
		return protocolId;
	}

	public void setProtocolId(Integer protocolId) {
		this.protocolId = protocolId;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}


	@Override
	public String toString() {
		return "ProtocolDimension{" +
			"protocolId=" + protocolId +
			", dimensionId=" + dimensionId +
			"}";
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}
}
