package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警数据表
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
@TableName("bpm_alarm_data")
public class AlarmData extends Model<AlarmData> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 应用id
     */
	private Integer applicationId;
    /**
     * 告警组id
     */
	private Integer alarmGroupId;
    /**
     * 类型1 组件 2 事件
     */
	private Integer type;
    /**
     * 告警id  :type=1 组件id, type=2 事件id
     */
	private Integer alarmId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getAlarmGroupId() {
		return alarmGroupId;
	}

	public void setAlarmGroupId(Integer alarmGroupId) {
		this.alarmGroupId = alarmGroupId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmData{" +
			"id=" + id +
			", applicationId=" + applicationId +
			", alarmGroupId=" + alarmGroupId +
			", type=" + type +
			", alarmId=" + alarmId +
			"}";
	}
}
