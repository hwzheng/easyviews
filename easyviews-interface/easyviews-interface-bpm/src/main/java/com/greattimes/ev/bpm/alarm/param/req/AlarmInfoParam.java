package com.greattimes.ev.bpm.alarm.param.req;

import com.alibaba.fastjson.annotation.JSONField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/10/18 20:52
 */
@ApiModel
public class AlarmInfoParam implements Serializable{

    private static final long serialVersionUID = 2327807435764171832L;

    /**
     * 开始时间
     */
    @ApiModelProperty("开始时间")
    private Long start;
    /**
     * 结束时间
     */
    @ApiModelProperty("结束时间")
    private Long end;
    /**
     * 数据源类型 1、业务2、事件 3、数据源
     */
    @ApiModelProperty("数据源类型")
    private Integer productType;
    /**
     * 应用id
     */
    @ApiModelProperty("应用id")
    private Integer applicationId;

    /**
     * 带有用户权限的应用id
     */
    private List<Integer> authAppIds;

    /**
     * 组件id
     */
    @ApiModelProperty("组件id")
    private Integer componentId;
    /**
     * 事件id
     */
    @ApiModelProperty("事件id")
    private Integer customId;
    /**
     * 告警级别
     */
    @ApiModelProperty("告警级别")
    private Integer alarmlevel;
    /**
     * 告警数据
     */
    @ApiModelProperty("告警数据")
    private String constraintDesc;

    @JSONField(serialize=false)
    private Integer pageSize;

    @JSONField(serialize=false)
    private Integer pageNum;

    public List<Integer> getAuthAppIds() {
        return authAppIds;
    }

    public void setAuthAppIds(List<Integer> authAppIds) {
        this.authAppIds = authAppIds;
    }

    /**
     * 颗粒度
     */
    private Integer bpmInterval;

    public Long getStart() {
        return start;
    }

    public void setStart(Long start) {
        this.start = start;
    }

    public Long getEnd() {
        return end;
    }

    public void setEnd(Long end) {
        this.end = end;
    }

    public Integer getProductType() {
        return productType;
    }

    public void setProductType(Integer productType) {
        this.productType = productType;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public Integer getAlarmlevel() {
        return alarmlevel;
    }

    public void setAlarmlevel(Integer alarmlevel) {
        this.alarmlevel = alarmlevel;
    }

    public String getConstraintDesc() {
        return constraintDesc;
    }

    public void setConstraintDesc(String constraintDesc) {
        this.constraintDesc = constraintDesc;
    }

    public Integer getBpmInterval() {
        return bpmInterval;
    }

    public void setBpmInterval(Integer bpmInterval) {
        this.bpmInterval = bpmInterval;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    @Override
    public String toString() {
        return "AlarmInfoParam{" +
                "start=" + start +
                ", end=" + end +
                ", productType=" + productType +
                ", applicationId=" + applicationId +
                ", authAppIds=" + authAppIds +
                ", componentId=" + componentId +
                ", customId=" + customId +
                ", alarmlevel=" + alarmlevel +
                ", constraintDesc='" + constraintDesc + '\'' +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", bpmInterval=" + bpmInterval +
                '}';
    }
}
