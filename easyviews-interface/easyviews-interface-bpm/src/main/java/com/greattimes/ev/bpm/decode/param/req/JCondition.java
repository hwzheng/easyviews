package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 规则条件
 * @author MS
 * @date   2018 下午5:51:02
 */
public class JCondition implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//条件主键
	private Integer id;
	//条件类型
	private Integer type;
	//条件值
	private Integer srcFieldId;
	//条件值来源
	private Integer srcOrigin;
	//目标值id
	private Integer dstFieldId;
	//目标值来源
	private Integer dstOrigin;
	//目标值
	private String dstVal;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getSrcFieldId() {
		return srcFieldId;
	}
	public void setSrcFieldId(Integer srcFieldId) {
		this.srcFieldId = srcFieldId;
	}
	public Integer getSrcOrigin() {
		return srcOrigin;
	}
	public void setSrcOrigin(Integer srcOrigin) {
		this.srcOrigin = srcOrigin;
	}
	public Integer getDstFieldId() {
		return dstFieldId;
	}
	public void setDstFieldId(Integer dstFieldId) {
		this.dstFieldId = dstFieldId;
	}
	public Integer getDstOrigin() {
		return dstOrigin;
	}
	public void setDstOrigin(Integer dstOrigin) {
		this.dstOrigin = dstOrigin;
	}
	public String getDstVal() {
		return dstVal;
	}
	public void setDstVal(String dstVal) {
		this.dstVal = dstVal;
	}
	@Override
	public String toString() {
		return "JCondition [id=" + id + ", type=" + type + ", srcFieldId="
				+ srcFieldId + ", srcOrigin=" + srcOrigin + ", dstFieldId="
				+ dstFieldId + ", dstOrigin=" + dstOrigin + ", dstVal="
				+ dstVal + "]";
	}
	
}
