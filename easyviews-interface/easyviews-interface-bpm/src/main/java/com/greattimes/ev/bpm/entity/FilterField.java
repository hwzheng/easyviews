package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 自定义数据过滤字段配置表
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_filter_field")
public class FilterField extends Model<FilterField> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 过滤字段维度id
     */
	private Integer dimensionId;
    /**
     * 自定义分析配置表id
     */
	private Integer customId;
    /**
     * 定义字典
     */
	private Integer relation;
    /**
     * 条件序列号
     */
	private Integer index;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FilterField{" +
			"id=" + id +
			", dimensionId=" + dimensionId +
			", customId=" + customId +
			", relation=" + relation +
			", index=" + index +
			"}";
	}
}
