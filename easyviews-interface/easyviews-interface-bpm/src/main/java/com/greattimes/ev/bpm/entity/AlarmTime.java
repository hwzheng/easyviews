package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * <p>
 * 告警时间表
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@TableName("bpm_alarm_time")
@ApiModel
public class AlarmTime extends Model<AlarmTime> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	@ApiModelProperty("告警时间id")
	private Integer id;
	/**
	 * 告警配置id
	 */
	@ApiModelProperty("告警配置id")
	private Integer alarmId;
	/**
	 * 开始时间
	 */
	@NotNull(message = "告警时间表开始时间不能为空！")
	@ApiModelProperty("开始时间")
	private Date start;
	/**
	 * 结束时间
	 */
	@NotNull(message = "告警时间表结束时间不能为空！")
	@ApiModelProperty("结束时间")
	private Date end;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmTime{" + "id=" + id + ", alarmId=" + alarmId + ", start=" + start + ", end=" + end + "}";
	}
}
