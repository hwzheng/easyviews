package com.greattimes.ev.bpm.config.param.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 通用查询返回的字典表实体
 * @author NJ
 * @date 2018/5/25 10:45
 */
@ApiModel
public class DictionaryParam implements Serializable{
    @ApiModelProperty("字典表label")
    private String label;
    @ApiModelProperty("字典表value")
    private Object value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DictionaryParam{" +
                "label='" + label + '\'' +
                ", value=" + value +
                '}';
    }
}
