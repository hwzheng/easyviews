package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 重复排期时间表
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
@TableName("bpm_schedule_repeat_times")
public class ScheduleRepeatTimes extends Model<ScheduleRepeatTimes> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 重复排期Id
     */
	private Integer scheduleId;
    /**
     * 开始时间
     */
	private Date start;
    /**
     * 结束时间
     */
	private Date end;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ScheduleRepeatTimes{" +
			"id=" + id +
			", scheduleId=" + scheduleId +
			", start=" + start +
			", end=" + end +
			"}";
	}
}
