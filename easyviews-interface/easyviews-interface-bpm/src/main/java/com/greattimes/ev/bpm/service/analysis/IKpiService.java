package com.greattimes.ev.bpm.service.analysis;


import java.util.List;
import java.util.Map;

import com.greattimes.ev.bpm.config.param.req.ExtendChooseParam;
import com.greattimes.ev.bpm.config.param.resp.ExtendChooseDataParam;


/**
 * @author NJ
 * @date 2018/9/11 19:41
 */
public interface IKpiService {

	List<Map<String, Object>> getIndicatorGroup();

	List<ExtendChooseDataParam> getExtendChoose(ExtendChooseParam param);


}
