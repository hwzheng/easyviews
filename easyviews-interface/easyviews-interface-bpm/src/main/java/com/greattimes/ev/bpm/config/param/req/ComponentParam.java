package com.greattimes.ev.bpm.config.param.req;



import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/5/28 14:20
 */
@ApiModel
public class ComponentParam implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * 组件id
     */
    @ApiModelProperty("组件id")
    private Integer id;

    /**
     * 应用id
     */
    @ApiModelProperty("应用id")
    private Integer applicationId;
    /**
     * 名称
     */
    @NotEmpty(message = "组件名称不能为空！")
    @ApiModelProperty("名称")
    private String name;

    /**
     * 组件数据统计模式1 组件 2 ip 3端口
     */
    @NotNull(message = "组件数据统计模式不能为空！")
    @ApiModelProperty("组件数据统计模式1 组件 2 ip 3端口")
    private Integer statisticalMode;
    /**
     * 协议
     */
    @NotNull(message = "协议不能为空！")
    @ApiModelProperty("协议")
    private Integer prototcolId;
    /**
     * 数据来源  0:流量1 ：日志
     */
    @ApiModelProperty("数据来源  0:流量1 ：日志")
    private Integer dataSource;
    /**
     * vlan
     */
    @ApiModelProperty("vlan")
    private Integer vlan;
    /**
     * 中心
     */
    @NotNull(message = "中心id不能为空！")
    @ApiModelProperty("中心")
    private Integer centerId;
    /**
     * 二级分组
     */
    @ApiModelProperty("二级分组")
    private Integer group;
    /**
     * 组件数据是否参与业务统计 0 否 1是
     */
    @NotNull(message = "是否参与业务统计不能为空！")
    @ApiModelProperty("组件数据是否参与业务统计 0 否 1是")
    private Integer isStatistics;
    /**
     * 连接模式（常规tcp 0，tcp长连接 1，异步双工长连接 2）
     */
    @NotNull(message = "连接模式不能为空！")
    @ApiModelProperty("连接模式（常规tcp 0，tcp长连接 1，异步双工长连接 2）")
    private Integer connectMode;
    /**
     * 组件组
     */
    @ApiModelProperty("组件组")
    private Integer component;
    /**
     * 服务模式 0 服务端 1 客户端
     */
    @ApiModelProperty("服务模式 0 服务端 1 客户端")
    private Integer serviceType;

    /**
     * 服务器ip[ip:string, port:string]
     */
    @NotEmpty(message="ip地址不能为空！")
    @ApiModelProperty("服务器ip[ip:string, port:string]")
    private List<JSONObject> server;

    private Integer sort;
    /**
     * 监控id
     */
    private Integer  monitorId;

    /**
     * 监控点名称
     */
    private String  monitorName;

    /**
     * 中心名称
     */
    private String centerName;

    /**
     * 是否激活
     */
    private Integer active;

    /**
     * 协议方向 0 普通 1反向 2 会话
     */
    @ApiModelProperty("协议方向 0 普通 1反向 2 会话")
    private Integer protocolType;

    /**
     * 配对方式 0 全局流水号  1  时序流水号 2 session时序流水号
     */
    @ApiModelProperty("配对方式 0 全局流水号  1  时序流水号 2 session时序流水号")
    private Integer patternType;
    /**
     * 前 配对字段
     */
    @ApiModelProperty("前 配对字段")
    private Integer patternFieldUp;
    /**
     * 后  配对字段
     */
    @ApiModelProperty("后  配对字段")
    private Integer patternFieldDown;

    /**
     * 响应模式（ 1 服务端  0 客户端）
     */
    @ApiModelProperty("响应模式（1 服务端 0 客户端")
    private Integer responseType;
    /**
     * 报文发送：0否 1是
     */
    @ApiModelProperty("报文发送：0否 1是")
    private Integer sendMsgBody;
    /**
     * 报文长度限制 默认2048
     */
    @ApiModelProperty("报文长度限制 默认2048")
    private Integer msgLimit;
    /**
     * 解码日志级别  1 debug 2 info 3 warn4 error
     */
    @ApiModelProperty("解码日志级别 1debug 2info 3warn 4error")
    private Integer logLevel;


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatisticalMode() {
        return statisticalMode;
    }

    public void setStatisticalMode(Integer statisticalMode) {
        this.statisticalMode = statisticalMode;
    }

    public Integer getPrototcolId() {
        return prototcolId;
    }

    public void setPrototcolId(Integer prototcolId) {
        this.prototcolId = prototcolId;
    }

    public Integer getDataSource() {
        return dataSource;
    }

    public void setDataSource(Integer dataSource) {
        this.dataSource = dataSource;
    }

    public Integer getVlan() {
        return vlan;
    }

    public void setVlan(Integer vlan) {
        this.vlan = vlan;
    }

    public Integer getCenterId() {
        return centerId;
    }

    public void setCenterId(Integer centerId) {
        this.centerId = centerId;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getIsStatistics() {
        return isStatistics;
    }

    public void setIsStatistics(Integer isStatistics) {
        this.isStatistics = isStatistics;
    }

    public Integer getConnectMode() {
        return connectMode;
    }

    public void setConnectMode(Integer connectMode) {
        this.connectMode = connectMode;
    }

    public Integer getComponent() {
        return component;
    }

    public void setComponent(Integer component) {
        this.component = component;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public List<JSONObject> getServer() {
        return server;
    }

    public void setServer(List<JSONObject> server) {
        this.server = server;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getProtocolType() {
        return protocolType;
    }

    public void setProtocolType(Integer protocolType) {
        this.protocolType = protocolType;
    }

    public Integer getPatternType() {
        return patternType;
    }

    public void setPatternType(Integer patternType) {
        this.patternType = patternType;
    }

    public Integer getPatternFieldUp() {
        return patternFieldUp;
    }

    public void setPatternFieldUp(Integer patternFieldUp) {
        this.patternFieldUp = patternFieldUp;
    }

    public Integer getPatternFieldDown() {
        return patternFieldDown;
    }

    public void setPatternFieldDown(Integer patternFieldDown) {
        this.patternFieldDown = patternFieldDown;
    }

    public Integer getResponseType() {
        return responseType;
    }

    public void setResponseType(Integer responseType) {
        this.responseType = responseType;
    }

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public Integer getSendMsgBody() {
        return sendMsgBody;
    }

    public void setSendMsgBody(Integer sendMsgBody) {
        this.sendMsgBody = sendMsgBody;
    }

    public Integer getMsgLimit() {
        return msgLimit;
    }

    public void setMsgLimit(Integer msgLimit) {
        this.msgLimit = msgLimit;
    }

    public Integer getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(Integer logLevel) {
        this.logLevel = logLevel;
    }

    @Override
    public String toString() {
        return "ComponentParam{" +
                "id=" + id +
                ", applicationId=" + applicationId +
                ", name='" + name + '\'' +
                ", statisticalMode=" + statisticalMode +
                ", prototcolId=" + prototcolId +
                ", dataSource=" + dataSource +
                ", vlan=" + vlan +
                ", centerId=" + centerId +
                ", group=" + group +
                ", isStatistics=" + isStatistics +
                ", connectMode=" + connectMode +
                ", component=" + component +
                ", serviceType=" + serviceType +
                ", server=" + server +
                ", sort=" + sort +
                ", monitorId=" + monitorId +
                ", monitorName='" + monitorName + '\'' +
                ", centerName='" + centerName + '\'' +
                ", active=" + active +
                ", protocolType=" + protocolType +
                ", patternType=" + patternType +
                ", patternFieldUp=" + patternFieldUp +
                ", patternFieldDown=" + patternFieldDown +
                ", responseType=" + responseType +
                ", sendMsgBody=" + sendMsgBody +
                ", msgLimit=" + msgLimit +
                ", logLevel=" + logLevel +
                '}';
    }
}
