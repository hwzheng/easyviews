package com.greattimes.ev.bpm.service.notice;


import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.entity.Notice;

/**
 * <p>
 * 通知表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-11-20
 */
public interface INoticeService extends IService<Notice> {

    /**
     * 按照分钟级插入，如果存在则不做操作
     * @author NJ
     * @date 2018/11/20 20:57
     * @param notice
     * @return boolean
     */
    boolean insertNoticeByTime(Notice notice);

    boolean insertNoticeByParam(Integer code, String name, Integer type, Integer state);
}
