package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 自定义告警
 * </p>
 *
 * @author NJ
 * @since 2018-08-28
 */
@TableName("bpm_alarm_custom")
public class AlarmCustom extends Model<AlarmCustom> {

    private static final long serialVersionUID = 1L;

    /**
     * 告警id
     */
	private Integer alarmId;
    /**
     * 自定义id
     */
	private Integer customId;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;


	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	protected Serializable pkVal() {
		return this.alarmId;
	}

	@Override
	public String toString() {
		return "AlarmCustom{" +
			"alarmId=" + alarmId +
			", customId=" + customId +
			", id=" + id +
			"}";
	}
}
