package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 协议表
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@TableName("bpm_protocol")
@ApiModel
public class Protocol extends Model<Protocol> {

	private static final long serialVersionUID = 1L;

	private Integer id;
	/**
	 * 协议名称
	 */
	@ApiModelProperty("协议名称")
	private String name;
	/**
	 * 协议标识 （英文）
	 */
	@ApiModelProperty("协议标识 （英文）")
	private String key;
	/**
	 * 协议类型
	 */
	@ApiModelProperty("协议类型")
	private String type;
	/**
	 * 协议格式
	 */
	@ApiModelProperty("协议格式")
	private String messageFormat;
	/**
	 * 协议描述
	 */
	@ApiModelProperty("协议描述")
	private String desc;
	/**
	 * 协议状态 :-1：删除、0：停用、1：启用
	 */
	@ApiModelProperty("协议状态 :-1：删除、0：停用、1：启用")
	private Integer state;
	/**
	 * 是否为内置协议 0：非内置协议  1：内置协议
	 */
	@ApiModelProperty("是否为内置协议 0：非内置协议  1：内置协议")
	private Integer builtin;
	/**
	 * 通信协议模式  0 同步  1 异步
	 */
	@ApiModelProperty("通信协议模式  0 同步  1 异步")
	private Integer protocolMode;
	/**
	 * 创建人
	 */
	@ApiModelProperty("创建人")
	private String createBy;
	/**
	 * 创建时间
	 */
	@ApiModelProperty("创建时间")
	private Date createDate;
	/**
	 * 更新人
	 */
	@ApiModelProperty("更新人")
	private String updateBy;
	/**
	 * 更新时间
	 */
	@ApiModelProperty("更新时间")
	private Date updateDate;
	/**
     * 协议json
     */
	@ApiModelProperty("协议json")
	private byte[] content;	
	/**
     * 应用协议补丁文件流
     */
	@ApiModelProperty("应用协议补丁文件流")
	private byte[] patch;
	/**
	 * 是否为模版协议 0：应用协议  1：模版协议
	 */
	@ApiModelProperty("是否为模版协议 0：应用协议  1：模版协议")
	private Integer flag;
	/**
	 * 1可解析协议 0 不可解析协议
	 */
	@ApiModelProperty("1可解析协议 0 不可解析协议")
	private Integer analyzeflag;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMessageFormat() {
		return messageFormat;
	}

	public void setMessageFormat(String messageFormat) {
		this.messageFormat = messageFormat;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Integer getBuiltin() {
		return builtin;
	}

	public void setBuiltin(Integer builtin) {
		this.builtin = builtin;
	}

	public Integer getProtocolMode() {
		return protocolMode;
	}

	public void setProtocolMode(Integer protocolMode) {
		this.protocolMode = protocolMode;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	public byte[] getPatch() {
		return patch;
	}

	public void setPatch(byte[] patch) {
		this.patch = patch;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getAnalyzeflag() {
		return analyzeflag;
	}

	public void setAnalyzeflag(Integer analyzeflag) {
		this.analyzeflag = analyzeflag;
	}

	@Override
	public String toString() {
		return "Protocol [id=" + id + ", name=" + name + ", key=" + key + ", type=" + type + ", messageFormat="
				+ messageFormat + ", desc=" + desc + ", state=" + state + ", builtin=" + builtin + ", protocolMode="
				+ protocolMode + ", createBy=" + createBy + ", createDate=" + createDate + ", updateBy=" + updateBy
				+ ", updateDate=" + updateDate + ", content=" + Arrays.toString(content) + ", patch="
				+ Arrays.toString(patch) + ", flag=" + flag + ", analyzeflag=" + analyzeflag + "]";
	}



}
