package com.greattimes.ev.bpm.analysis.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/6/25 16:08
 */
public class ReportChartDimensionParam implements Serializable {
    private static final long serialVersionUID = -4401390070607171512L;

    private Integer dimensionId;
    private String ename;
    private String name;
    private String othername;
    private Integer type;
    private List<String> values;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOthername() {
        return othername;
    }

    public void setOthername(String othername) {
        this.othername = othername;
    }

    public Integer getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(Integer dimensionId) {
        this.dimensionId = dimensionId;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "ReportChartDimensionParam{" +
                "dimensionId=" + dimensionId +
                ", ename='" + ename + '\'' +
                ", name='" + name + '\'' +
                ", othername='" + othername + '\'' +
                ", type=" + type +
                ", values=" + values +
                '}';
    }
}
