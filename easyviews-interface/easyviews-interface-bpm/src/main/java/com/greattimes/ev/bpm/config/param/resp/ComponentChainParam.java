package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/11/27 14:36
 */
public class ComponentChainParam implements Serializable{

    private static final long serialVersionUID = -6934038545800456312L;

    private  Integer componentId;

    private  String componentName;
    /**
     * 前追踪点
     */
    private  String leftValue;
    /**
     * 后追踪点
     */
    private  String rightValue;

    /**
     * 追踪字段列表
     */
    List<TrackParam> trackSelect;

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getLeftValue() {
        return leftValue;
    }

    public void setLeftValue(String leftValue) {
        this.leftValue = leftValue;
    }

    public String getRightValue() {
        return rightValue;
    }

    public void setRightValue(String rightValue) {
        this.rightValue = rightValue;
    }

    public List<TrackParam> getTrackSelect() {
        return trackSelect;
    }

    public void setTrackSelect(List<TrackParam> trackSelect) {
        this.trackSelect = trackSelect;
    }

    @Override
    public String toString() {
        return "ComponentChainParam{" +
                "componentId=" + componentId +
                ", componentName='" + componentName + '\'' +
                ", leftValue=" + leftValue +
                ", rightValue=" + rightValue +
                ", trackSelect=" + trackSelect +
                '}';
    }
}
