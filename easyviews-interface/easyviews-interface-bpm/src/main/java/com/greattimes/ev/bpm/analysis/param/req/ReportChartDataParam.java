package com.greattimes.ev.bpm.analysis.param.req;

import com.greattimes.ev.bpm.entity.ReportChartIndicator;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/3/28 15:58
 */
public class ReportChartDataParam implements Serializable{

    private static final long serialVersionUID = -5021111922782261176L;
    /**
     * 数据类型 1 应用 2 事件 3 漏斗
     */
    private Integer type;
    /**
     * uuid
     */
    private Integer uuid;
    /**
     * 图id
     */
    private Integer  chartId;
    /**
     * 应用id
     */
    private Integer applicationId;
    /**
     * 监控id
     */
    private Integer monitorId;
    /**
     * 组件id
     */
    private Integer componentId;
    /**
     * ipId
     */
    private Integer ipId;
    /**
     * 端口id
     */
    private Integer portId;
    /**
     * 数据类型 数据类型 1 应用 ，2 监控点 ，3 组件 ，4IP ，5 端口 ，6事件
     */
    private Integer dataType;
    /**
     * 自定义id
     */
    private Integer customId;
    /**
     * 事件指标
     */
    private List<Integer> indicators;
    private String applicationName;
    private String monitorName;
    private String componentName;
    private String ip;
    private String port;
    @Deprecated
    private Integer isDimension;
    private String customName;

    /**
     * dimension
     * ename
     * dimensionName
     * List<String> values 维度值
     */
    private List<ReportChartDimensionParam> dimensionValues;

//    /**
//     * 指标数据
//     */
//    private List<JSONObject> indicatorValues;

    /**
     * 指标数据
     */
    private List<ReportChartIndicator> indicatorValues;

    /**
     * 维度类型: 0 非维度 1 组件维度 2 事件维度
     */
    private Integer dimensionType;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getUuid() {
        return uuid;
    }

    public void setUuid(Integer uuid) {
        this.uuid = uuid;
    }

    public Integer getChartId() {
        return chartId;
    }

    public void setChartId(Integer chartId) {
        this.chartId = chartId;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public Integer getIpId() {
        return ipId;
    }

    public void setIpId(Integer ipId) {
        this.ipId = ipId;
    }

    public Integer getPortId() {
        return portId;
    }

    public void setPortId(Integer portId) {
        this.portId = portId;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public List<Integer> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Integer> indicators) {
        this.indicators = indicators;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Integer getIsDimension() {
        return isDimension;
    }

    public void setIsDimension(Integer isDimension) {
        this.isDimension = isDimension;
    }

    public List<ReportChartDimensionParam> getDimensionValues() {
        return dimensionValues;
    }

    public void setDimensionValues(List<ReportChartDimensionParam> dimensionValues) {
        this.dimensionValues = dimensionValues;
    }

    public List<ReportChartIndicator> getIndicatorValues() {
        return indicatorValues;
    }

    public void setIndicatorValues(List<ReportChartIndicator> indicatorValues) {
        this.indicatorValues = indicatorValues;
    }

    public Integer getDimensionType() {
        return dimensionType;
    }

    public void setDimensionType(Integer dimensionType) {
        this.dimensionType = dimensionType;
    }

    @Override
    public String toString() {
        return "ReportChartDataParam{" +
                "type=" + type +
                ", uuid=" + uuid +
                ", chartId=" + chartId +
                ", applicationId=" + applicationId +
                ", monitorId=" + monitorId +
                ", componentId=" + componentId +
                ", ipId=" + ipId +
                ", portId=" + portId +
                ", dataType=" + dataType +
                ", customId=" + customId +
                ", indicators=" + indicators +
                ", applicationName='" + applicationName + '\'' +
                ", monitorName='" + monitorName + '\'' +
                ", componentName='" + componentName + '\'' +
                ", ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", isDimension=" + isDimension +
                ", dimensionValues=" + dimensionValues +
                ", indicatorValues=" + indicatorValues +
                ", dimensionType=" + dimensionType +
                '}';
    }
}
