package com.greattimes.ev.bpm.service.common;

import java.util.List;
import java.util.Set;

/**
 * @author NJ
 * @date 2018/9/4 14:21
 */
public interface IDeleteRuleService {

    /**
     * 编辑保存组件ipport改变导致的删除逻辑
     * @author NJ
     * @date 2018/9/6 13:03
     * @param ipStr
     * @param ipPortStr
     * @param componentId
     * @return boolean
     */
    boolean deleteAlarmByIpPorts(Set<String> ipStr, Set<String> ipPortStr, int componentId);

    /**
     * 删除组件
     * @author NJ
     * @date 2018/9/4 14:27
     * @param ids
     * @return boolean
     */
    boolean deleteComponent(List<Integer> ids);

    /**
     * 删除监控点
     * @author NJ
     * @date 2018/9/4 14:29
     * @param monitorId
     * @return boolean
     */
    boolean deleteMonitor(int monitorId);


    /**
     * 删除维度
     * @param dimensionIds
     * @param type  '告警 0 默认 1 统计维度'
     * @return
     */
    boolean deleteDimension(List<Integer> dimensionIds,int type);



    /**
     * 删除协议字段后续操作
     * @param ids
     * @return
     */
    boolean deleteProtocolField(List<Integer> ids);
    
    /**
     * 数据源删除
     * @param id
     * @return
     */
    boolean deleteDataSource(Integer id);
    
    /**
     * 删除指标组
     * @param id
     * @return
     */
    boolean deleteIndicatorGroup(Integer id);
    
    /**
     * 删除数据源约束
     * @param id
     * @return
     */
    boolean deleteConstraintField(List<Integer> id);

    /**
     * 外部数据删除
     * @param id
     * @return
     */
    boolean deleteDatasourceRelevant(Integer id);

	/**
	 * 组件ip port更改删除外部数据
	 * @param componentId
	 * @return
	 */
	boolean deleteRelevantData(Integer componentId);

}
