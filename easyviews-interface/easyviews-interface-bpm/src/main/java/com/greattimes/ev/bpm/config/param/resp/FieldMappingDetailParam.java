package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 映射配置详细信息
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
@ApiModel
public class FieldMappingDetailParam implements Serializable{

    /**
	 */
	private static final long serialVersionUID = 5913049445576249159L;
	/**
     * 组件id
     */
	@ApiModelProperty("组件id")
	private Integer componentId;
    /**
     * 组件名称
     */
	@ApiModelProperty("组件名称")
	private String componentName;
    /**
     * 维度id
     */
	@ApiModelProperty("维度id")
	private Integer dimensionId;
	/**
     * 维度名称
     */
	@ApiModelProperty("维度名称")
	private String dimensionName;
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public String getComponentName() {
		return componentName;
	}
	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}
	public Integer getDimensionId() {
		return dimensionId;
	}
	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}
	public String getDimensionName() {
		return dimensionName;
	}
	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

}
