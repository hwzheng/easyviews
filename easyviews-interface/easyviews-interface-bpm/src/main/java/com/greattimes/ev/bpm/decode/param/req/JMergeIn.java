package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 合并操作in
 * @author MS
 * @date   2018 下午5:12:52
 */
public class JMergeIn implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//输入值主键
	private  Integer fieldId;
	//输入值默认值
	private String value;
	//输入值来源
	private Integer origin;
	
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Integer getOrigin() {
		return origin;
	}
	public void setOrigin(Integer origin) {
		this.origin = origin;
	}
	@Override
	public String toString() {
		return "JMergeIn [fieldId=" + fieldId + ", value=" + value
				+ ", origin=" + origin + "]";
	}
}
