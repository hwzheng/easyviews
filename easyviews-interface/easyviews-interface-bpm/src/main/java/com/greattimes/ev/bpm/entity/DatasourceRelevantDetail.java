package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据源关联详情表
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
@TableName("bpm_datasource_relevant_detail")
public class DatasourceRelevantDetail extends Model<DatasourceRelevantDetail> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 关联字段
     */
	private String RelevantField;
    /**
     * 数据源id
     */
	private Integer dataSourceRelevantId;
    /**
     * 关联维度
     */
	private String dimension;
    /**
     * 指标
     */
	private Integer indicator;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRelevantField() {
		return RelevantField;
	}

	public void setRelevantField(String RelevantField) {
		this.RelevantField = RelevantField;
	}

	public Integer getDataSourceRelevantId() {
		return dataSourceRelevantId;
	}

	public void setDataSourceRelevantId(Integer dataSourceRelevantId) {
		this.dataSourceRelevantId = dataSourceRelevantId;
	}

	public String getDimension() {
		return dimension;
	}

	public void setDimension(String dimension) {
		this.dimension = dimension;
	}

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DatasourceRelevantDetail{" +
			"id=" + id +
			", RelevantField=" + RelevantField +
			", dataSourceRelevantId=" + dataSourceRelevantId +
			", dimension=" + dimension +
			", indicator=" + indicator +
			"}";
	}
}
