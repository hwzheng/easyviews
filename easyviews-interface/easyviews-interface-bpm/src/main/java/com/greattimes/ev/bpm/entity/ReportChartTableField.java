package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能报表表格展示字段
 * </p>
 *
 * @author NJ
 * @since 2019-05-28
 */
@TableName("ai_report_chart_table_field")
public class ReportChartTableField extends Model<ReportChartTableField> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 图表id
     */
	private Integer chartId;
    /**
     * 字段名称
     */
	private String field;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ReportChartTableField{" +
			"id=" + id +
			", chartId=" + chartId +
			", field=" + field +
			"}";
	}
}
