package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能报表图表位置表
 * </p>
 *
 * @author cgc
 * @since 2019-03-27
 */
@TableName("ai_report_chart_location")
public class ReportChartLocation extends Model<ReportChartLocation> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 左边距
     */
	private Double left;
    /**
     * 上边距
     */
	private Double top;
    /**
     * 高度
     */
	private Double height;
    /**
     * 宽度
     */
	private Double width;
    /**
     * 关系id
     */
	private Integer chartRelationId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getLeft() {
		return left;
	}

	public void setLeft(Double left) {
		this.left = left;
	}

	public Double getTop() {
		return top;
	}

	public void setTop(Double top) {
		this.top = top;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Integer getChartRelationId() {
		return chartRelationId;
	}

	public void setChartRelationId(Integer chartRelationId) {
		this.chartRelationId = chartRelationId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}


	@Override
	public String toString() {
		return "ReportChartLocation{" +
			"id=" + id +
			", left=" + left +
			", top=" + top +
			", height=" + height +
			", width=" + width +
			", chartId=" + chartRelationId +
			"}";
	}
}
