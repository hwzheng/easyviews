package com.greattimes.ev.bpm.custom.param.resp;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义分析配置
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
@ApiModel
public class CustomParam implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty("id")
	private Integer id;
	/**
	 * 名称
	 */
	@ApiModelProperty("名称")
	private String name;
	/**
	 * 应用
	 */
	@ApiModelProperty("应用")
	private String applicationName;
	/**
	 * 组件
	 */
	@ApiModelProperty("组件")
	private String componentName;
	/**
	 * 激活状态 0 否 1是
	 */
	@ApiModelProperty("激活状态 0 否 1是")
	private Integer active;
	/**
	 * 告警状态 0关闭 1开启
	 */
	@ApiModelProperty("告警状态 0关闭 1开启")
	private Integer alarmState;
	/**
	 * 排序
	 */
	@ApiModelProperty("排序")
	private Integer sort;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getAlarmState() {
		return alarmState;
	}

	public void setAlarmState(Integer alarmState) {
		this.alarmState = alarmState;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

}
