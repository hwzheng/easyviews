package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能瓶颈报表分析规则阈值表
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@TableName("rp_bottleneck_task_analyze_rule_threshold")
public class BottleneckTaskAnalyzeRuleThreshold extends Model<BottleneckTaskAnalyzeRuleThreshold> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 分析字段id
     */
	private Integer analyzeRuleId;
    /**
     * 阈值
     */
	private Integer analyzeThreshold;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnalyzeRuleId() {
		return analyzeRuleId;
	}

	public void setAnalyzeRuleId(Integer analyzeRuleId) {
		this.analyzeRuleId = analyzeRuleId;
	}

	public Integer getAnalyzeThreshold() {
		return analyzeThreshold;
	}

	public void setAnalyzeThreshold(Integer analyzeThreshold) {
		this.analyzeThreshold = analyzeThreshold;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "BottleneckTaskAnalyzeRuleThreshold{" +
			"id=" + id +
			", analyzeRuleId=" + analyzeRuleId +
			", analyzeThreshold=" + analyzeThreshold +
			"}";
	}
}
