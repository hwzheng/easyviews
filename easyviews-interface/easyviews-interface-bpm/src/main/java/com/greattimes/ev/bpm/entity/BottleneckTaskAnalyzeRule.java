package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能瓶颈报表分析规则表
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@TableName("rp_bottleneck_task_analyze_rule")
public class BottleneckTaskAnalyzeRule extends Model<BottleneckTaskAnalyzeRule> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 分析字段id
     */
	private Integer analyzeFieldId;
    /**
     * 分析规则 1 等于 2 不等于 3大于 4小于
     */
	private Integer analyzeRule;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAnalyzeFieldId() {
		return analyzeFieldId;
	}

	public void setAnalyzeFieldId(Integer analyzeFieldId) {
		this.analyzeFieldId = analyzeFieldId;
	}

	public Integer getAnalyzeRule() {
		return analyzeRule;
	}

	public void setAnalyzeRule(Integer analyzeRule) {
		this.analyzeRule = analyzeRule;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "BottleneckTaskAnalyzeRule{" +
			"id=" + id +
			", analyzeFieldId=" + analyzeFieldId +
			", analyzeRule=" + analyzeRule +
			"}";
	}
}
