package com.greattimes.ev.bpm.analysis.param.req;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/5/22 14:22
 */
public class ReportChartIndicatorFilterParam implements Serializable{
    private static final long serialVersionUID = -3892388717046020153L;

    private Integer id;
    /**
     * 指标id
     */
    private Integer indicatorId;
    /**
     * 图表id
     */
    private Integer chartId;
    /**
     * 规则
     */
    private Integer rule;
    /**
     * 指标值
     */
    private Double value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(Integer indicatorId) {
        this.indicatorId = indicatorId;
    }

    public Integer getChartId() {
        return chartId;
    }

    public void setChartId(Integer chartId) {
        this.chartId = chartId;
    }

    public Integer getRule() {
        return rule;
    }

    public void setRule(Integer rule) {
        this.rule = rule;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ReportChartIndIndicatorFilterParam{" +
                "id=" + id +
                ", indicatorId=" + indicatorId +
                ", chartId=" + chartId +
                ", rule=" + rule +
                ", value=" + value +
                '}';
    }
}
