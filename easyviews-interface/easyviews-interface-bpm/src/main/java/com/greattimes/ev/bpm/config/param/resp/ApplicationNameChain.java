package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/5/7 12:57
 */
public class ApplicationNameChain  implements Serializable{

    private Integer applicationId;
    private String applicationName;
    private Integer monitorId;
    private String monitorName;
    private Integer componentId;
    private String componentName;
    private Integer ipId;
    private String ip;
    private Integer portId;
    private String port;

    private Integer customId;
    private String customName;
    /**
     * 1 应用 2 监控点 3 组件4 ip 5 port 6 事件
     */
    private Integer type;

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public String getMonitorName() {
        return monitorName;
    }

    public void setMonitorName(String monitorName) {
        this.monitorName = monitorName;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public Integer getIpId() {
        return ipId;
    }

    public void setIpId(Integer ipId) {
        this.ipId = ipId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPortId() {
        return portId;
    }

    public void setPortId(Integer portId) {
        this.portId = portId;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public Integer getCustomId() {
        return customId;
    }

    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "ApplicationNameChain{" +
                "applicationId=" + applicationId +
                ", applicationName='" + applicationName + '\'' +
                ", monitorId=" + monitorId +
                ", monitorName='" + monitorName + '\'' +
                ", componentId=" + componentId +
                ", componentName='" + componentName + '\'' +
                ", ipId=" + ipId +
                ", ip='" + ip + '\'' +
                ", portId=" + portId +
                ", port='" + port + '\'' +
                ", customId=" + customId +
                ", customName='" + customName + '\'' +
                ", type=" + type +
                '}';
    }
}
