package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 中心探针表
 * </p>
 *
 * @author cgc
 * @since 2018-08-08
 */
@TableName("bpm_center_probe")
@ApiModel
public class CenterProbe extends Model<CenterProbe> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
	private Integer id;
    /**
     * 中心id
     */
    @NotNull(message="中心id不能为空！")
    @ApiModelProperty("中心id")
	private Integer centerId;
    /**
     * 探针ip
     */
    @NotEmpty(message="探针ip不能为空！")
    @Pattern(regexp="(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)",message="请输入正确的ip!")
	@ApiModelProperty("探针ip")
	private String ip;
    /**
     * 端口类型
     */
    @NotEmpty(message="端口类型不能为空！")
    @ApiModelProperty("端口类型")
	private String portType;
    /**
     * 端口数量
     */
    @NotNull(message="端口数量不能为空！")
    @ApiModelProperty("端口数量")
	private Integer portNum;
    /**
     * 探针编号
     */
    @NotEmpty(message="探针编号不能为空！")
	@Length(min=1, max=20,message="探针编号不能超过20位")
    @ApiModelProperty("探针编号")
	private String code;
    /**
     * 探针名称
     */
    @NotEmpty(message="探针名称不能为空！")
	@Length(min=1, max=50,message="探针名称不能超过20位")
    @ApiModelProperty("探针名称")
	private String name;
    /**
     * 端口号
     */
    @ApiModelProperty("端口号")
	private Integer portCode;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}


	public Integer getPortNum() {
		return portNum;
	}

	public void setPortNum(Integer portNum) {
		this.portNum = portNum;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getPortCode() {
		return portCode;
	}

	public void setPortCode(Integer portCode) {
		this.portCode = portCode;
	}

	@Override
	public String toString() {
		return "CenterProbe{" +
			"id=" + id +
			", centerId=" + centerId +
			", ip=" + ip +
			", portType=" + portType +
			", protNum=" + portNum +
			", code=" + code +
			", name=" + name +
			"}";
	}
}
