package com.greattimes.ev.bpm.config.param.req;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.annotation.JSONField;
import com.greattimes.ev.bpm.entity.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/6/4 10:02
 */
@ApiModel
public class AlarmParam implements Serializable{

    private static final long serialVersionUID = 1516836440316608223L;
    /**
     * 告警id
     */
    @ApiModelProperty("告警id")
    private Integer id;
    /**
     * 应用id
     */
    @NotNull(message = "应用id不能为空！")
    @ApiModelProperty("应用id")
    private Integer applicationId;
    /**
     * 高警级别
     */
    @NotNull(message = "告警级别不能为空！")
    @ApiModelProperty("告警级别")
    private Integer level;
    /**
     * 告警时段类型 1 每天 2 工作日 3 非工作日
     */
    @NotNull(message = "告警时段类型不能为空！")
    @ApiModelProperty("告警时段类型 1 每天 2 工作日 3 非工作日")
    private Integer timeType;
    /**
     * 时间窗口  配置字典 单位分钟
     */
    @NotNull(message = "时间窗口不能为空！")
    @ApiModelProperty("时间窗口  配置字典 单位分钟")
    private Integer timeFrame;
    /**
     * '规则描述'
     */
    @ApiModelProperty("规则描述-非必填")
    private String desc;
    /**
     * 激活状态 0 关闭 1开启
     */
    @ApiModelProperty("激活状态 0 关闭 1开启")
    private Integer active;
    /**
     * 条件关系 1 任一条件 ，2 全部条件
     */
    @ApiModelProperty("条件关系 1 任一条件 ，2 全部条件")
    private Integer relation;
    /**
     * 告警类型 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度
     */
    @ApiModelProperty("告警类型 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度")
    private Integer type;
    /**
     * 告警时间段
     */
    @NotEmpty(message="告警时间段不能为空！")
    @ApiModelProperty("告警时间段")
    private List<@Valid AlarmTime> time;
    /**
     * 规则模板和其它
     */
    @NotEmpty(message="规则模板不能为空！")
    @ApiModelProperty("规则模板和其它")
    private List<AlarmTemplateReqParam> condition;
    /**
     * 组件
     */
    @JSONField(serialize=false)
    private List<AlarmComponent> components;
    /**
     * 组件ids
     */
    @ApiModelProperty("组件ids")
    private List<Integer>  componentIds;
    /**
     * 组件id
     */
    @ApiModelProperty("组件id")
    private Integer componentId;
    /**
     * 维度id
     */
    @ApiModelProperty("维度id")
    private Integer dimensionId;
    /**
     * ips
     */
    @ApiModelProperty("ips")
    private List<Integer> ips;
    /**
     * ip对象
     */
    @JSONField(serialize=false)
    private List<AlarmIp> alarmIps;
    /**
     * ip/port
     */
    @ApiModelProperty("ip:port")
    private List<String> ipPorts;
    /**
     * 过滤条件
     */
    @ApiModelProperty("过滤条件")
    private List<AlarmFilterParam>  filter;
    /**
     * 维度值
     */
    @JSONField(serialize=false)
    private List<AlarmDimension> alarmDimensions;
    /**
     * 告警维度ids
     */
    @ApiModelProperty("告警维度ids")
    private List<Integer> dimensionIds;

    /**
     * 告警维度ids，单独给自定义维度告警使用
     */
    @ApiModelProperty("自定义维度ids")
    private List<JSONObject> customDimensionId;

    /**
     * 监控点
     */
    @ApiModelProperty("监控点")
    private List<AlarmMonitor> monitors;
    /**
     * 监控点id集合
     */
    private List<Integer> monitorIds;
    /**
     * 监控点id
     */
    @ApiModelProperty("监控点id")
    private Integer monitorId;
    /**
     * 数据源id
     */
    @ApiModelProperty("数据源id")
    private Integer dataSourceId;
    /**
     * 指标组id
     */
    @ApiModelProperty("指标组id")
    private Integer indcatorGroupId;

    /**
     * 数据源关联约束ids
     */
    private List<Integer> relevantContraintIds;
    /**
     * 数据源关联约束实体类接收
     */
    private List<AlarmDatasourceDimension> alarmDatasourceDimensions;
    /**
     * 自定义分析配置的id
     */
    private Integer customId;
    /**
     * 告警维度值ids
     */
    private List<Integer> dimensionValueIds;

    /**
     * 告警描述flag(手动or自动)
     */
    private Boolean descFlag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public Integer getTimeFrame() {
        return timeFrame;
    }

    public void setTimeFrame(Integer timeFrame) {
        this.timeFrame = timeFrame;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getRelation() {
        return relation;
    }

    public void setRelation(Integer relation) {
        this.relation = relation;
    }

    public List<AlarmTime> getTime() {
        return time;
    }

    public void setTime(List<AlarmTime> time) {
        this.time = time;
    }

    public List<AlarmTemplateReqParam> getCondition() {
        return condition;
    }

    public void setCondition(List<AlarmTemplateReqParam> condition) {
        this.condition = condition;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public List<Integer> getComponentIds() {
        return componentIds;
    }

    public void setComponentIds(List<Integer> componentIds) {
        this.componentIds = componentIds;
    }

    public List<AlarmComponent> getComponents() {
        return components;
    }

    public void setComponents(List<AlarmComponent> components) {
        this.components = components;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }


    public List<AlarmIp> getAlarmIps() {
        return alarmIps;
    }

    public void setAlarmIps(List<AlarmIp> alarmIps) {
        this.alarmIps = alarmIps;
    }

    public List<String> getIpPorts() {
        return ipPorts;
    }

    public void setIpPorts(List<String> ipPorts) {
        this.ipPorts = ipPorts;
    }

    public Integer getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(Integer dimensionId) {
        this.dimensionId = dimensionId;
    }

    public List<AlarmFilterParam> getFilter() {
        return filter;
    }

    public void setFilter(List<AlarmFilterParam> filter) {
        this.filter = filter;
    }

    public List<Integer> getDimensionIds() {
        return dimensionIds;
    }

    public void setDimensionIds(List<Integer> dimensionIds) {
        this.dimensionIds = dimensionIds;
    }

    public List<AlarmDimension> getAlarmDimensions() {
        return alarmDimensions;
    }

    public void setAlarmDimensions(List<AlarmDimension> alarmDimensions) {
        this.alarmDimensions = alarmDimensions;
    }

    public List<Integer> getIps() {
        return ips;
    }

    public void setIps(List<Integer> ips) {
        this.ips = ips;
    }

    public List<Integer> getMonitorIds() {
        return monitorIds;
    }

    public void setMonitorIds(List<Integer> monitorIds) {
        this.monitorIds = monitorIds;
    }

    public List<AlarmMonitor> getMonitors() {
        return monitors;
    }

    public void setMonitors(List<AlarmMonitor> monitors) {
        this.monitors = monitors;
    }

    public Integer getMonitorId() {
        return monitorId;
    }

    public void setMonitorId(Integer monitorId) {
        this.monitorId = monitorId;
    }

    public Integer getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(Integer dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public Integer getIndcatorGroupId() {
        return indcatorGroupId;
    }

    public void setIndcatorGroupId(Integer indcatorGroupId) {
        this.indcatorGroupId = indcatorGroupId;
    }

    public List<Integer> getRelevantContraintIds() {
        return relevantContraintIds;
    }

    public void setRelevantContraintIds(List<Integer> relevantContraintIds) {
        this.relevantContraintIds = relevantContraintIds;
    }

    public List<AlarmDatasourceDimension> getAlarmDatasourceDimensions() {
        return alarmDatasourceDimensions;
    }

    public void setAlarmDatasourceDimensions(List<AlarmDatasourceDimension> alarmDatasourceDimensions) {
        this.alarmDatasourceDimensions = alarmDatasourceDimensions;
    }
    public Integer getCustomId() {
        return customId;
    }
    public void setCustomId(Integer customId) {
        this.customId = customId;
    }

    public List<Integer> getDimensionValueIds() {
        return dimensionValueIds;
    }

    public void setDimensionValueIds(List<Integer> dimensionValueIds) {
        this.dimensionValueIds = dimensionValueIds;
    }

    public List<JSONObject> getCustomDimensionId() {
        return customDimensionId;
    }

    public void setCustomDimensionId(List<JSONObject> customDimensionId) {
        this.customDimensionId = customDimensionId;
    }

    public Boolean getDescFlag() {
        return descFlag;
    }

    public void setDescFlag(Boolean descFlag) {
        this.descFlag = descFlag;
    }

    @Override
    public String toString() {
        return "AlarmParam{" +
                "id=" + id +
                ", applicationId=" + applicationId +
                ", level=" + level +
                ", timeType=" + timeType +
                ", timeFrame=" + timeFrame +
                ", desc='" + desc + '\'' +
                ", active=" + active +
                ", relation=" + relation +
                ", type=" + type +
                ", time=" + time +
                ", condition=" + condition +
                ", components=" + components +
                ", componentIds=" + componentIds +
                ", componentId=" + componentId +
                ", dimensionId=" + dimensionId +
                ", ips=" + ips +
                ", alarmIps=" + alarmIps +
                ", ipPorts=" + ipPorts +
                ", filter=" + filter +
                ", alarmDimensions=" + alarmDimensions +
                ", dimensionIds=" + dimensionIds +
                ", customDimensionId=" + customDimensionId +
                ", monitors=" + monitors +
                ", monitorIds=" + monitorIds +
                ", monitorId=" + monitorId +
                ", dataSourceId=" + dataSourceId +
                ", indcatorGroupId=" + indcatorGroupId +
                ", relevantContraintIds=" + relevantContraintIds +
                ", alarmDatasourceDimensions=" + alarmDatasourceDimensions +
                ", customId=" + customId +
                ", dimensionValueIds=" + dimensionValueIds +
                ", descFlag=" + descFlag +
                '}';
    }
}
