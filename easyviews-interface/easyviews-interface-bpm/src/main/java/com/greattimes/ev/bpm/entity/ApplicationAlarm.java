package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author NJ
 * @since 2018-09-20
 */
@TableName("applicationalarm")
public class ApplicationAlarm extends Model<ApplicationAlarm> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	@JSONField(serialize=false)
	private Integer id;
    /**
     * 时间戳
     */
	@JSONField(serialize=false)
	private Long timestamp;
    /**
     * 应用id
     */
	@JSONField(serialize=false)
	private Integer applicationid;
    /**
     * 1级告警数
     */
	private Integer oneLevelAlarmCount;
    /**
     * 2级告警数
     */
	private Integer secondLevelAlarmCount;
    /**
     * 3级告警数
     */
	private Integer threeLevelAlarmCount;
    /**
     * 4级告警数
     */
	private Integer fourLevelAlarmCount;
    /**
     * 5级告警数
     */
	private Integer fiveLevelAlarmCount;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public Integer getApplicationid() {
		return applicationid;
	}

	public void setApplicationid(Integer applicationid) {
		this.applicationid = applicationid;
	}

	public Integer getOneLevelAlarmCount() {
		return oneLevelAlarmCount;
	}

	public void setOneLevelAlarmCount(Integer oneLevelAlarmCount) {
		this.oneLevelAlarmCount = oneLevelAlarmCount;
	}

	public Integer getSecondLevelAlarmCount() {
		return secondLevelAlarmCount;
	}

	public void setSecondLevelAlarmCount(Integer secondLevelAlarmCount) {
		this.secondLevelAlarmCount = secondLevelAlarmCount;
	}

	public Integer getThreeLevelAlarmCount() {
		return threeLevelAlarmCount;
	}

	public void setThreeLevelAlarmCount(Integer threeLevelAlarmCount) {
		this.threeLevelAlarmCount = threeLevelAlarmCount;
	}

	public Integer getFourLevelAlarmCount() {
		return fourLevelAlarmCount;
	}

	public void setFourLevelAlarmCount(Integer fourLevelAlarmCount) {
		this.fourLevelAlarmCount = fourLevelAlarmCount;
	}

	public Integer getFiveLevelAlarmCount() {
		return fiveLevelAlarmCount;
	}

	public void setFiveLevelAlarmCount(Integer fiveLevelAlarmCount) {
		this.fiveLevelAlarmCount = fiveLevelAlarmCount;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public ApplicationAlarm(){
	}

	public ApplicationAlarm(Integer id, Long timestamp, Integer applicationid, Integer oneLevelAlarmCount, Integer secondLevelAlarmCount, Integer threeLevelAlarmCount, Integer fourLevelAlarmCount, Integer fiveLevelAlarmCount) {
		this.id = id;
		this.timestamp = timestamp;
		this.applicationid = applicationid;
		this.oneLevelAlarmCount = oneLevelAlarmCount;
		this.secondLevelAlarmCount = secondLevelAlarmCount;
		this.threeLevelAlarmCount = threeLevelAlarmCount;
		this.fourLevelAlarmCount = fourLevelAlarmCount;
		this.fiveLevelAlarmCount = fiveLevelAlarmCount;
	}

	public ApplicationAlarm(Long timestamp) {
		this.timestamp = timestamp;
		this.oneLevelAlarmCount = 0;
		this.secondLevelAlarmCount = 0;
		this.threeLevelAlarmCount = 0;
		this.fourLevelAlarmCount = 0;
		this.fiveLevelAlarmCount = 0;
	}

	@Override
	public String toString() {
		return "Applicationalarm{" +
			"id=" + id +
			", timestamp=" + timestamp +
			", applicationid=" + applicationid +
			", oneLevelAlarmCount=" + oneLevelAlarmCount +
			", secondLevelAlarmCount=" + secondLevelAlarmCount +
			", threeLevelAlarmCount=" + threeLevelAlarmCount +
			", fourLevelAlarmCount=" + fourLevelAlarmCount +
			", fiveLevelAlarmCount=" + fiveLevelAlarmCount +
			"}";
	}
}
