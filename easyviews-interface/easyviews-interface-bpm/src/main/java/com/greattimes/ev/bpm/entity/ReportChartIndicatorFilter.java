package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能报表图表指标过滤条件
 * </p>
 *
 * @author NJ
 * @since 2019-05-22
 */
@TableName("ai_report_chart_indicator_filter")
public class ReportChartIndicatorFilter extends Model<ReportChartIndicatorFilter> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 指标id
     */
	private Integer indicatorId;
    /**
     * 图表id
     */
	private Integer chartId;
    /**
     * 规则
     */
	private Integer rule;
    /**
     * 指标值
     */
	private Double value;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	public Integer getRule() {
		return rule;
	}

	public void setRule(Integer rule) {
		this.rule = rule;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ReportChartIndicatorFilter{" +
			"id=" + id +
			", indicatorId=" + indicatorId +
			", chartId=" + chartId +
			", rule=" + rule +
			", value=" + value +
			"}";
	}
}
