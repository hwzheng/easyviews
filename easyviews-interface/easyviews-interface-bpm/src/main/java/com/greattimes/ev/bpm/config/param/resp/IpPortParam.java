package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

/**
 * ip_port视图映射类
 * @author: nj
 * @date: 2020-03-12 16:37
 * @version: 0.0.1
 */
public class IpPortParam implements Serializable {

    private static final long serialVersionUID = -8276022441651591486L;

    private Integer applicationId;

    private String appName;

    private Integer appActive;

    private Integer appState;

    private Integer componentId;

    private String compName;

    private Integer compActive;

    /**
     * port id
     */
    private Integer id;
    /**
     * 组件ip组
     */
    private Integer serverGroupId;
    /**
     * IP
     */
    private String ip;
    /**
     * intIp
     */
    private Integer intIp;
    /**
     * port
     */
    private Integer port;

    /**
     * portStr
     */
    private String portStr;

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public Integer getAppActive() {
        return appActive;
    }

    public void setAppActive(Integer appActive) {
        this.appActive = appActive;
    }

    public Integer getAppState() {
        return appState;
    }

    public void setAppState(Integer appState) {
        this.appState = appState;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public String getCompName() {
        return compName;
    }

    public void setCompName(String compName) {
        this.compName = compName;
    }

    public Integer getCompActive() {
        return compActive;
    }

    public void setCompActive(Integer compActive) {
        this.compActive = compActive;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getServerGroupId() {
        return serverGroupId;
    }

    public void setServerGroupId(Integer serverGroupId) {
        this.serverGroupId = serverGroupId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getIntIp() {
        return intIp;
    }

    public void setIntIp(Integer intIp) {
        this.intIp = intIp;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getPortStr() {
        return portStr;
    }

    public void setPortStr(String portStr) {
        this.portStr = portStr;
    }

    @Override
    public String toString() {
        return "IpPortParam{" +
                "applicationId=" + applicationId +
                ", appName='" + appName + '\'' +
                ", appActive=" + appActive +
                ", appState=" + appState +
                ", componentId=" + componentId +
                ", compName='" + compName + '\'' +
                ", compActive=" + compActive +
                ", id=" + id +
                ", serverGroupId=" + serverGroupId +
                ", ip='" + ip + '\'' +
                ", intIp=" + intIp +
                ", port=" + port +
                ", portStr='" + portStr + '\'' +
                '}';
    }
}