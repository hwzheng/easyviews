package com.greattimes.ev.bpm.service.config;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.ApplicationParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.resp.MonitorParam;
import com.greattimes.ev.bpm.entity.Application;
import com.greattimes.ev.bpm.entity.MonitorComponent;

/**
 * <p>
 * 应用表 服务类
 * </p>
 *
 * @author Cgc
 * @since 2018-05-18
 */
public interface IApplicationService extends IService<Application> {

	/**
	 * 查询用户业务
	 * 
	 * @param userId
	 * @return
	 */
	List<ApplicationParam> selectApplications(int userId);

	/**
	 * 用户应用保存
	 * 
	 * @param userId
	 * @param userAppIds
	 */
	void saveUserApplication(int userId, List<Integer> userAppIds);


	/**
	 * 根据用户id查询业务相应名称和id，通用接口
	 * @param userId
	 * @return
	 */
	List<DictionaryParam> selectAppAsDictonaryByUserId(int userId);

	/**
	 * 根据用户id查询业务列表
	 * @author NJ
	 * @date 2018/5/25 16:05
	 * @param userId
	 * @return java.util.List<com.greattimes.ev.bpm.config.param.req.ApplicationParam>
	 */
	List<ApplicationParam> selectApplicationByUserId(int userId);
	/**
	 * 查询监控状态on应用
	 * @author NJ
	 * @date 2018/5/25 16:05
	 * @param userId
	 * @return java.util.List<com.greattimes.ev.bpm.config.param.req.ApplicationParam>
	 */
	List<ApplicationParam> selectActiveApplication(int userId);
	
	/**
	 * 更新激活状态
	 * @author NJ
	 * @date 2018/5/25 16:16
	 * @param id
	 * @param active
	 * @return int
	 */
	int updateActive(int id, int active);

	/**
	 * 批量激活应用状态
	 * @param id
	 * @param active
	 * @return
	 */
	int updateBatchActive(int id, int active);

	/**
	 * 更新告警状态
	 * @author NJ
	 * @date 2018/5/25 16:41
	 * @param id
	 * @param alarmState
	 * @return void
	 */
	int updateAlarmStatus(int id, int alarmState);

	/**
	 * 批量更新排序值
	 * @author NJ
	 * @date 2018/5/25 17:45
	 * @param list
	 * @return void
	 */
	void updateSort(List<ApplicationParam> list);

	/**
	 * 应用删除
	 * @author NJ
	 * @date 2018/5/25 17:56
	 * @param id
	 * @return int
	 */
	int delete(int id);

	/**
	 *  应用新增 返回应用id
	 * @param userId
	 * @param param
	 * @return
	 */
	int add(int userId,ApplicationParam param);


	/**
	 * 应用修改
	 * @author NJ
	 * @date 2018/5/29 16:22
	 * @param userId
	 * @param param
	 * @return
	 */
	int edit(int userId, ApplicationParam param);

	/**
	 * 批量告警状态
	 * @author NJ
	 * @date 2018/6/7 14:16
	 * @param userId
	 * @param alarmState
	 * @return int
	 */
	int updateBatchAlarmStatus(int userId, int alarmState);

	/**
	 * 获取应用详情
	 * @author NJ
	 * @date 2018/6/13 16:51
	 * @param id
	 * @return com.greattimes.ev.bpm.config.param.req.ApplicationParam
	 */
	ApplicationParam getDetail(int id);

	/**
	 * 应用名称是否重复
	 * @author NJ
	 * @date 2018/6/13 16:53
	 * @param id
	 * @param name
	 * @return boolean
	 */
	boolean isHasSameAppName(int id, String name);

	/**
	 *  监控点保存
	 * @param name
	 * @param componentNum
	 * @param applicationId
	 * @param id
	 * @return
	 */
	boolean saveOrUpdateMonitor(String name, Integer componentNum, Integer applicationId, Integer id);

	/**
	 * 监控点名称是否重复(同一个应用下)
	 * @author NJ
	 * @date 2018/11/21 17:55
	 * @param appId
	 * @param id
	 * @param name
	 * @param isAdd
	 * @return boolean
	 */
	boolean isHasSameMonitorName(int appId, int id, String name, boolean isAdd);


	/**
	 * 监控点删除
	 * @author NJ
	 * @date 2018/8/8 11:43
	 * @param id
	 * @return boolean
	 */
	boolean deleteMonitor(int id );

	/**
	 * 批量更新监控点的sort
	 * @param list
	 */
	void updateMonitorSort(List<MonitorParam> list);

	/**
	 * 根据应用id查询监控点列表
	 * @return
	 */
	List<MonitorParam> selectMonitorList(int applicationId);

	/**
	 * 查询监控点详情
	 * @param id
	 * @return
	 */
	Map<String, Object> selectMonitorDetail(int id);

	/**
	 * 根据应用查询所属监控点
	 * @param appId
	 * @return
	 */
	List<DictionaryParam> selectMonitorDictionaryByAppId(int appId);

	/**
	 * 根据监控点查询所属组件
	 * @param monitorId
	 * @return
	 */
	List<DictionaryParam> selectComponentDictionaryByMonitorId(int monitorId);
	/**
	 * 查询全部应用
	 * @return
	 */
	List<Application> selectApplications();

	/**
	 * 用户应用关系删除
	 * @param userId
	 * @param applicationId
	 */
	void deleteRelation(Integer userId, Integer applicationId);

	/**
	 * 根据监控点id查询关联表
	 * @param monitorId
	 * @return
	 */
	List<MonitorComponent> selectMonitorComponentByMonitorId(int monitorId);

	/**
	 * 菜单查询
	 * @author cgc  
	 * @date 2018年9月17日  
	 * @param userId 
	 * @param type 0 普通进入（新建）  1 其他页面进入（编辑）
	 * @param checkedId 要选中的id
	 * @param level 查询层级  1应用 2监控点
	 * @param id 根节点id
	 * @param flag 0kpi菜单 1多维菜单
	 * @param applicationId  要返回的应用节点
	 * @return
	 */
	List<Map<String, Object>> selectMenu(int userId, int type, List<Integer> checkedId, Integer level, Integer id ,Integer flag, int applicationId);

	/**
	 * 组件tree为路径图提供
	 * @author NJ
	 * @date 2018/9/19 20:15
	 * @param componentId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectComponentTree(int componentId);

	/**
	 * 根据用户id查询该用户所有的uuid(ct_uuid && bpm_uuid)
	 * @author NJ
	 * @date 2018/10/11 16:10
	 * @param userId
	 * @return java.util.Set<java.lang.Long>
	 */
	@Deprecated
	Set<Long> selectAllUuidByUserId(int userId);

	List<Map<String, Object>> getAlarmGrouopComponentTree(int userId);

	List<Map<String, Object>> getAlarmGrouopEventTree(int userId);

	/**
	 * 根据uuid获取各个应用各个层级的名称  应用-监控点-组件-ip-port
	 * @author NJ
	 * @date 2019/5/6 19:56
	 * @param uuids
	 * @return java.util.Map<java.lang.Integer,java.lang.String>
	 */
	Map<Integer, String> namesMap(List<Integer> uuids);

	/***
	 * 根据uuid获取各个应用事件的名称  应用-组件-事件
	 * @author NJ
	 * @date 2019/5/6 20:36
	 * @param uuids
	 * @return java.util.Map<java.lang.Integer,java.lang.String>
	 */
	Map<Integer, String> customNameMap(List<Integer> uuids);


	/**
	 * 通过用户id和告警查询用户ID
	 * @param userId
	 * @return java.util.List<java.lang.Integer>
	 * @throw
	 * @author nj
	 * @date 2020-08-17 14:13
	*/
	List<Integer> findAppIdByAlarmInfo(int userId, long stat, long end, int limit);

  }
