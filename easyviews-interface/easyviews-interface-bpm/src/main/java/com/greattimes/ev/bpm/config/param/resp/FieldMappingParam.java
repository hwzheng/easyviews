package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 映射配置返回信息包装类
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
@ApiModel
public class FieldMappingParam implements Serializable{

	/**
	 */
	private static final long serialVersionUID = -8640807862634209918L;
	private Integer id;
	/**
	 * 更新时间
	 */
	@ApiModelProperty("更新时间")
	private Date updateTime;
	/**
	 * 更新人
	 */
	@ApiModelProperty("更新人")
	private String updateUser;
	/**
	 * 应用ID
	 */
	@ApiModelProperty("应用ID")
	private Integer applicationId;
	/**
	 * 详情
	 */
	@ApiModelProperty("详情")
	private List<FieldMappingDetailParam> detail;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public List<FieldMappingDetailParam> getDetail() {
		return detail;
	}

	public void setDetail(List<FieldMappingDetailParam> detail) {
		this.detail = detail;
	}

}
