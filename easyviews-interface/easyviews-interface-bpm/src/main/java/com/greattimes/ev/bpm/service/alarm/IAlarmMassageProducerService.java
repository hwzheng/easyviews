package com.greattimes.ev.bpm.service.alarm;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.bpm.alarm.param.req.AlarmGroupConfigParam;

/**
 * @author NJ
 * @date 2019/1/7 18:52
 */
public interface IAlarmMassageProducerService {

    /**
     *
     * @param bpmInterval
     * @param alarmDelay
     * @return
     */
    /**
     * 告警发送信息生产
     * @param jsonObject
     *        bpmInterval  bpm颗粒度
     *        alarmDelay   告警延迟
     *        sendInterval 告警发送压制时间
     * @return
     */
    int messageProducer(JSONObject jsonObject);

    /**
     * 告警分发
     * @param alarmConfig
     */
    void insertTypeDistribute(AlarmGroupConfigParam alarmConfig);

    /**
     * 清除map
     * @return
     */
    boolean clearMessageMap(int configId);

    /**
     * 全局map重新赋值
     * @return
     */
    boolean resetMessageMap();
}
