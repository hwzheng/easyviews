package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author NJ
 * @since 2018-12-03
 */
public class Abnormalinfo extends Model<Abnormalinfo> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 时间戳
     */
	private Long time;
    /**
     * 应用id
     */
	private Integer applicationId;
    /**
     * 监控点id
     */
	private Integer monitorId;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * 业务id
     */
	private Integer extendId;
    /**
     * 数据源id
     */
	private Integer sourceId;
    /**
     * 规则id
     */
	private Integer ruleId;
    /**
     * 条件id
     */
	private Integer condtionId;
    /**
     * 异常值:基线时为分数或者级别;递增递减为幅度
     */
	private Double abnormalValue;
    /**
     * 产品类型 1bpm 2扩展bpm 3第三方数据源
     */
	private Integer productType;
    /**
     * 告警数据层级
     */
	private Integer datalevel;
    /**
     * 异常key
     */
	private String abnormalKey;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getExtendId() {
		return extendId;
	}

	public void setExtendId(Integer extendId) {
		this.extendId = extendId;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getCondtionId() {
		return condtionId;
	}

	public void setCondtionId(Integer condtionId) {
		this.condtionId = condtionId;
	}

	public Double getAbnormalValue() {
		return abnormalValue;
	}

	public void setAbnormalValue(Double abnormalValue) {
		this.abnormalValue = abnormalValue;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getDatalevel() {
		return datalevel;
	}

	public void setDatalevel(Integer datalevel) {
		this.datalevel = datalevel;
	}

	public String getAbnormalKey() {
		return abnormalKey;
	}

	public void setAbnormalKey(String abnormalKey) {
		this.abnormalKey = abnormalKey;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Abnormalinfo{" +
			"id=" + id +
			", time=" + time +
			", applicationId=" + applicationId +
			", monitorId=" + monitorId +
			", componentId=" + componentId +
			", extendId=" + extendId +
			", sourceId=" + sourceId +
			", ruleId=" + ruleId +
			", condtionId=" + condtionId +
			", abnormalValue=" + abnormalValue +
			", productType=" + productType +
			", datalevel=" + datalevel +
			", abnormalKey=" + abnormalKey +
			"}";
	}
}
