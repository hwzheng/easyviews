package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 组件指标约束UUID表
 * </p>
 *
 * @author cgc
 * @since 2018-08-20
 */
@TableName("ds_uuid")
public class DsUuid extends Model<DsUuid> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 数据源id
     */
	private Integer dataSourceId;
    /**
     * 约束值
     */
	private String value;
    /**
     * 状态
     */
	private Integer active;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DsUuid{" +
			"id=" + id +
			", dataSourceId=" + dataSourceId +
			", value=" + value +
			", active=" + active +
			"}";
	}
}
