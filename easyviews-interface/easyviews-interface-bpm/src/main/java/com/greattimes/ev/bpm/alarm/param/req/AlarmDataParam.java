package com.greattimes.ev.bpm.alarm.param.req;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/1/7 19:44
 */
public class AlarmDataParam implements Serializable{
    private static final long serialVersionUID = 3967184135787723038L;

    private Integer id;
    /**
     * 应用id
     */
    private Integer applicationId;
    /**
     * 告警组id
     */
    private Integer alarmGroupId;
    /**
     * 类型1 组件 2 事件
     */
    private Integer type;
    /**
     * 告警id  :type=1 组件id, type=2 事件id
     */
    private Integer alarmId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getAlarmGroupId() {
        return alarmGroupId;
    }

    public void setAlarmGroupId(Integer alarmGroupId) {
        this.alarmGroupId = alarmGroupId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getAlarmId() {
        return alarmId;
    }

    public void setAlarmId(Integer alarmId) {
        this.alarmId = alarmId;
    }

    @Override
    public String toString() {
        return "AlarmDataParam{" +
                "id=" + id +
                ", applicationId=" + applicationId +
                ", alarmGroupId=" + alarmGroupId +
                ", type=" + type +
                ", alarmId=" + alarmId +
                '}';
    }
}
