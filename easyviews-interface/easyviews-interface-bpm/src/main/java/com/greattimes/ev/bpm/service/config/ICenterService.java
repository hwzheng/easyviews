package com.greattimes.ev.bpm.service.config;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.resp.CenterProbeParam;
import com.greattimes.ev.bpm.config.param.resp.CenterProbePortParam;
import com.greattimes.ev.bpm.entity.Center;
import com.greattimes.ev.bpm.entity.CenterProbe;
import com.greattimes.ev.bpm.entity.CenterProbePort;

/**
 * <p>
 * 中心表 服务类
 * </p>
 *
 * @author Cgc
 * @since 2018-05-22
 */
public interface ICenterService extends IService<Center> {

	/**
	 * 查询
	 */
	List<Center> getCenters(int active);

	/**
	 * 新增
	 * 
	 * @param center
	 */
	void save(Center center);

	/**
	 * 校验中心名
	 * 
	 * @param center
	 * @param flag
	 * @return
	 */
	Boolean checkCenterName(Center center, int flag);

	/**
	 * 编辑
	 * 
	 * @param center
	 */
	void update(Center center);

	/**
	 * 删除
	 * 
	 * @param id
	 */
	void delete(Integer id);

	/**
	 * 探针查询
	 * @param id 
	 * @return
	 */
	CenterProbe selectProbe(Integer id);

	/**
	 * 探针查询
	 * @return
	 */
	List<CenterProbeParam> selectProbe();

	/**
	 * 校验探针名
	 * @param centerprobe
	 * @param i
	 * @return
	 */
	Boolean checkProbeName(CenterProbe centerprobe, int i);

	/**
	 * 新增探针
	 * @param centerprobe
	 */
	void save(CenterProbe centerprobe);

	/**
	 * 编辑探针
	 * @param centerprobe
	 */
	void update(CenterProbe centerprobe);

	/**
	 * 删除探针
	 * @param id
	 */
	void deleteProbe(Integer id);

	/**
	 * 探针口查询
	 * @param id 
	 * @return
	 */
	List<CenterProbePortParam> selectProbePort(Integer id);

	/**
	 * 探针口保存
	 * @param port
	 */
	void save(CenterProbePort port);

	/**
	 * 探针口编辑
	 * @param port
	 */
	void update(CenterProbePort port);

	/**
	 * 探针口删除
	 * @param id
	 */
	void deletePort(Integer id);
	
	/**    
	 * 更新中心状态
	 * @author NJ  
	 * @date 2018/9/10 15:54
	 * @param id
	 * @param active  
	 * @return boolean  
	 */  
	boolean updateCenterActive(int id, int active);

	/**校验探针IP
	 * @param centerprobe
	 * @param i 1新增
	 * @return
	 */
	Boolean checkProbeIp(CenterProbe centerprobe, int i);

	/**组件下拉-采集口信息查询
	 * @param centerId
	 * @return
	 */
	List<Integer> getProbeInfo(Integer centerId);

}
