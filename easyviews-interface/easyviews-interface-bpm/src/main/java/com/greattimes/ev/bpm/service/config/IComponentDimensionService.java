package com.greattimes.ev.bpm.service.config;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.ComponentDimensionParam;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.config.param.resp.DimensionLableParam;
import com.greattimes.ev.bpm.config.param.resp.FieldMappingParam;
import com.greattimes.ev.bpm.entity.ComponentDimension;
import com.greattimes.ev.bpm.entity.FieldMapping;
import com.greattimes.ev.bpm.entity.ProtocolField;

/**
 * <p>
 * 组件分析维度 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
public interface IComponentDimensionService extends IService<ComponentDimension> {

	/**
	 * 分析维度信息查询
	 * @param componentId 组件id
	 * @return
	 */
	List<Map<String, Object>> selectDimension(int componentId);

	/**
	 * 分析维度名称查询(正名和异名)
	 * @author NJ
	 * @date 2018/10/18 9:58
	 * @param componentId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectDimensionNameBycomponentId(int componentId);

	/**
	 * 根据组件id集合查询组件分析维度名称
	 * @param componentIds
	 * @return
	 */
	@Deprecated
	Map<Integer, List<Map<String, Object>>> selectDimensionNameByIds(List<Integer> componentIds);

	/**
	 * 根据组件id集合查询组件分析维度名称  key:componentId map[key:ename value:data]
	 * @author NJ
	 * @date 2019/4/11 16:35
	 * @param componentIds
	 * @return java.util.Map<java.lang.Integer,java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.lang.Object>>>
	 */
	Map<Integer, Map<String, Map<String, Object>>> selectDimensionNameMapByIds(List<Integer> componentIds);

	/**
	 * 分析维度信息保存
	 * @param param
	 */
	void saveComponentDimens(ComponentDimensionParam param);

	/**
	 * 别名查询
	 * @param componentId
	 * @return
	 */
	List<DimensionParam> selectOtherName(int componentId);

	/**
	 * 别名保存
	 * @param param
	 */
	void saveOtherName(DimensionParam param);

	/**
	 * 映射配置查询
	 * @param applicationId
	 * @return
	 */
	List<FieldMappingParam> selectFieldMapping(int applicationId);

	/**
	 * 映射配置新增
	 * @param map
	 * @param loginName 
	 */
	void addFieldMapping(Map<String, Object> map, String loginName);

	/**
	 * 映射配置编辑
	 * @param map
	 * @param loginName
	 */
	void editFieldMapping(Map<String, Object> map, String loginName);

	/**
	 * 映射配置删除
	 * @param map
	 */
	void deleteFieldMapping(Integer id);

	/**
	 * 查询维度字段映射表的信息
	 * @param configId
	 * @return
	 */
	List<FieldMapping> selectMapping(Integer configId);

	/**
	 * 映射配置上传时维度字段映射表新增和维度字段映射组件表更新
	 * @param details
	 * @param loginName
	 * @param configId
	 * @param applictionId 
	 */
	void insertFieldMapping(List<FieldMapping> details, String loginName, int configId, int applictionId);

	/**
	 * 根据componentId 查询对应维度
	 * @param componentId
	 * @return 
	 */
	List<ProtocolField> selectDimensionByComponentId(int componentId);

	/**
	 * 根据applicationId查询组件和维度的id name
	 * @param applicationId
	 * @return
	 */
	List<DimensionLableParam> loadSelect(int applicationId);

	/**
	 * 删除映射
	 * @param ids 维度id
	 */
	void deleteFieldMapping(List<Integer> ids);

	void updateSort(List<DimensionParam> list);

	/**
	 * 根据组件id查询最大的sort值
	 * @author NJ
	 * @date 2019/2/18 11:34
	 * @param componentId
	 * @return int
	 */
	int selectMaxSortByComponentId(int componentId);
}
