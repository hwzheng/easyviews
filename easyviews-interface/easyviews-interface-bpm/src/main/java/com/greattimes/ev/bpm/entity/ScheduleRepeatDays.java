package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 重复排期日期
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
@TableName("bpm_schedule_repeat_days")
public class ScheduleRepeatDays extends Model<ScheduleRepeatDays> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 日期 1：周一 ，2：周二 ，3周三 .......
     */
	private Integer day;
    /**
     * 重复排期id
     */
	private Integer scheduleId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDay() {
		return day;
	}

	public void setDay(Integer day) {
		this.day = day;
	}

	public Integer getScheduleId() {
		return scheduleId;
	}

	public void setScheduleId(Integer scheduleId) {
		this.scheduleId = scheduleId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ScheduleRepeatDays{" +
			"id=" + id +
			", day=" + day +
			", scheduleId=" + scheduleId +
			"}";
	}
}
