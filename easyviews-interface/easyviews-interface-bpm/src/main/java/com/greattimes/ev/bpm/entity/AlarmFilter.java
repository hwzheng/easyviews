package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警数据过滤表
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
@TableName("bpm_alarm_filter")
public class AlarmFilter extends Model<AlarmFilter> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 告警id
     */
	private Integer alarmId;
    /**
     * 维度id
     */
	private Integer dimensionId;
    /**
     * 告警规则 0 like ，1 notlike
     */
	private Integer rule;
	/**
	 * 告警 0 默认 1 统计维度
	 */
	private Integer type;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getRule() {
		return rule;
	}

	public void setRule(Integer rule) {
		this.rule = rule;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "AlarmFilter{" +
				"id=" + id +
				", alarmId=" + alarmId +
				", dimensionId=" + dimensionId +
				", rule=" + rule +
				", type=" + type +
				'}';
	}
}
