package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 变量
 * @author MS
 * @date   2018 上午9:13:55
 */
public class JVariable implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//变量主键
	private Integer id;
	//变量标识
	private String key;
	//变量名称
	private String name;
	//变量别名
	private String additionalKey;
	//变量别名id
	private Integer additionalId;
	//数据类型   1：字符串、2：数值、3：布尔值、4：字节数组 5：字符串集合、6：数值集合、7：字节数组集合 8：字典（Map）、9：操作集合
	private Integer type;
	//变量值
	private JVariableValue value;
	//是否传输变量  控制是否将该变量的值发送至Kafka
	private Boolean isTransport;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public JVariableValue getValue() {
		return value;
	}
	public void setValue(JVariableValue value) {
		this.value = value;
	}
	public Boolean getIsTransport() {
		return isTransport;
	}
	public void setIsTransport(Boolean isTransport) {
		this.isTransport = isTransport;
	}
	
	public String getAdditionalKey() {
		return additionalKey;
	}
	public void setAdditionalKey(String additionalKey) {
		this.additionalKey = additionalKey;
	}
	public Integer getAdditionalId() {
		return additionalId;
	}
	public void setAdditionalId(Integer additionalId) {
		this.additionalId = additionalId;
	}
	@Override
	public String toString() {
		return "JVariable [id=" + id + ", key=" + key + ", name=" + name
				+ ", type=" + type + ", value=" + value + ", isTransport="
				+ isTransport + "]";
	}
	
}
