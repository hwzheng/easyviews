package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * tcp协议请求/响应报文
 * @author MS
 * @date   2018 上午10:47:49
 */
public class JTcpReqRespMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//报文标记类型
	private Integer type;
	//报文长度标记
	private JLengthMark lengthMark;
	//报文结束标记
	private List<String> endMark;
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public JLengthMark getLengthMark() {
		return lengthMark;
	}
	public void setLengthMark(JLengthMark lengthMark) {
		this.lengthMark = lengthMark;
	}
	public List<String> getEndMark() {
		return endMark;
	}
	public void setEndMark(List<String> endMark) {
		this.endMark = endMark;
	}
	
	@Override
	public String toString() {
		return "JTcpReqRespMsg [type=" + type + ", lengthMark=" + lengthMark
				+ ", endMark=" + endMark + "]";
	}
}
