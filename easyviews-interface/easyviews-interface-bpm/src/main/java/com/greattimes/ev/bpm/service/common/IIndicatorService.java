package com.greattimes.ev.bpm.service.common;


import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/4 14:21
 */
public interface IIndicatorService {

   
    /**
     * 获取指标名colname
     * @author cgc  
     * @date 2018年9月28日  
     * @param indicator
     * @return
     */
    Map<Integer, String> getIndicatorName(List<Integer> indicator);

    /**
     * 根据id查询指标 key:indicatorId value:indicatorMap
     * @author NJ
     * @date 2019/4/11 17:15
     * @param ids
     * @return java.util.Map<java.lang.Integer,java.util.Map<java.lang.String,java.lang.Object>>
     */
    Map<Integer, Map<String, Object>> getIndicatorMapByIds(List<Integer> ids);



}
