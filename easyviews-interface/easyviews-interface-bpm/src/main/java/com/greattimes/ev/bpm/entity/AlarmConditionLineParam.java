package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 基线告警模板参数表
 * </p>
 *
 * @author NJ
 * @since 2018-11-13
 */
@TableName("bpm_alarm_condition_line_param")
public class AlarmConditionLineParam extends Model<AlarmConditionLineParam> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 条件号
     */
	private Integer conditionId;
    /**
     * 上线分数
     */
	private Integer upperScore;
    /**
     * 下线分数
     */
	private Integer lowerScore;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getConditionId() {
		return conditionId;
	}

	public void setConditionId(Integer conditionId) {
		this.conditionId = conditionId;
	}

	public Integer getUpperScore() {
		return upperScore;
	}

	public void setUpperScore(Integer upperScore) {
		this.upperScore = upperScore;
	}

	public Integer getLowerScore() {
		return lowerScore;
	}

	public void setLowerScore(Integer lowerScore) {
		this.lowerScore = lowerScore;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmConditionLineParam{" +
			"id=" + id +
			", conditionId=" + conditionId +
			", upperScore=" + upperScore +
			", lowerScore=" + lowerScore +
			"}";
	}
}
