package com.greattimes.ev.bpm.datasources.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 组件指标约束
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@ApiModel
public class CompIndConstraintGroupParam implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 约束组id
	 */
	@ApiModelProperty("约束组id")
	private Integer constraintGroupId;
	/**
	 * 约束
	 */
	@ApiModelProperty("约束")
	private List<CompIndContraintParam> constraintGroup;

	public Integer getConstraintGroupId() {
		return constraintGroupId;
	}

	public void setConstraintGroupId(Integer constraintGroupId) {
		this.constraintGroupId = constraintGroupId;
	}

	public List<CompIndContraintParam> getConstraintGroup() {
		return constraintGroup;
	}

	public void setConstraintGroup(List<CompIndContraintParam> constraintGroup) {
		this.constraintGroup = constraintGroup;
	}



}
