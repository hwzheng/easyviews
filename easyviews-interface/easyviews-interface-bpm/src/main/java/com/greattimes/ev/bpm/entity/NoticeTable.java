package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 功能号映射表
 * </p>
 *
 * @author cgc
 * @since 2018-11-20
 */
@TableName("sys_notice_table")
public class NoticeTable extends Model<NoticeTable> {

    private static final long serialVersionUID = 1L;

    /**
     * 表名
     */
	private String tableName;
    /**
     * 通知类型 0大数据 1解码
     */
	private Integer type;
    /**
     * 通知编码
     */
	private Integer code;


	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}


	@Override
	public String toString() {
		return "NoticeTable{" +
			"tableName=" + tableName +
			", type=" + type +
			", code=" + code +
			"}";
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}
}
