package com.greattimes.ev.bpm.analysis.param.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author cgc
 * @date 2018/9/11 21:51
 */
public class MetaParam implements Serializable {

	private static final long serialVersionUID = 1L;

	//private List<List<Object>> stringCols;
	private Map<String, Object> headerMap;
	private List<String> stringCols;
	private List<List<Object>> numericCols;
	private String label;
	private String labelOp;
	private String labelValue;



	/*public List<List<Object>> getStringCols() {
		return stringCols;
	}

	public void setStringCols(List<List<Object>> stringCols) {
		this.stringCols = stringCols;
	}*/
	

	public List<List<Object>> getNumericCols() {
		return numericCols;
	}

	public Map<String, Object> getHeaderMap() {
		return headerMap;
	}

	public void setHeaderMap(Map<String, Object> headerMap) {
		this.headerMap = headerMap;
	}

	public List<String> getStringCols() {
		return stringCols;
	}

	public void setStringCols(List<String> stringCols) {
		this.stringCols = stringCols;
	}

	public void setNumericCols(List<List<Object>> numericCols) {
		this.numericCols = numericCols;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getLabelOp() {
		return labelOp;
	}

	public void setLabelOp(String labelOp) {
		this.labelOp = labelOp;
	}

	public String getLabelValue() {
		return labelValue;
	}

	public void setLabelValue(String labelValue) {
		this.labelValue = labelValue;
	}

}
