package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义分析配置
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
@TableName("ct_custom")
@ApiModel
public class Custom extends Model<Custom> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.INPUT)
	@ApiModelProperty("id")
	private Integer id;
	/**
	 * 名称
	 */
	@ApiModelProperty("名称")
	private String name;
	/**
	 * 应用
	 */
	@ApiModelProperty("应用ID")
	private Integer applicationId;
	/**
	 * 组件
	 */
	@ApiModelProperty("组件id")
	private Integer componentId;
	/**
	 * 激活状态 0 否 1是
	 */
	@ApiModelProperty("激活状态 0 否 1是")
	private Integer active;
	/**
	 * 告警状态 0关闭 1开启
	 */
	@ApiModelProperty("告警状态 0关闭 1开启")
	private Integer alarmState;
	/**
	 * 过滤类型 0 普通 1 表达式
	 */
	@ApiModelProperty("过滤类型 0 普通 1 表达式")
	private Integer filterType;
	/**
	 * 过滤表达式
	 */
	@ApiModelProperty("过滤表达式")
	private String filterExpressions;
	/**
	 * 过滤关系 (过滤类型为1时有效)  0  and  ，1 or 
	 */
	@ApiModelProperty("过滤关系 (过滤类型为0时有效)  0  and  ，1 or ")
	private Integer filterRelation;
	/**
	 * 成功判断表达式
	 */
	@ApiModelProperty("成功判断表达式")
	private String successExpressions;
	/**
	 * 成功判断类型：0 普通 1 表达式
	 */
	@ApiModelProperty("成功判断类型：0 普通 1 表达式")
	private Integer successType;
	/**
	 * 成功判断关系 (过滤类型为1时有效)  0  and  ，1 or 
	 */
	@ApiModelProperty("成功判断关系 (过滤类型为0时有效)  0  and  ，1 or")
	private Integer successRelation;

	/**
	 * 排序
	 */
	@ApiModelProperty("排序")
	private Integer sort;
	 /**
     * 步骤状态逗号分隔 例如1,0,0,0  1 完成 0 未完成
     */
	@ApiModelProperty("步骤状态")
	private String step;

	public String getStep() {
		return step;
	}

	public void setStep(String step) {
		this.step = step;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getAlarmState() {
		return alarmState;
	}

	public void setAlarmState(Integer alarmState) {
		this.alarmState = alarmState;
	}

	public Integer getFilterType() {
		return filterType;
	}

	public void setFilterType(Integer filterType) {
		this.filterType = filterType;
	}

	public String getFilterExpressions() {
		return filterExpressions;
	}

	public void setFilterExpressions(String filterExpressions) {
		this.filterExpressions = filterExpressions;
	}

	public Integer getFilterRelation() {
		return filterRelation;
	}

	public void setFilterRelation(Integer filterRelation) {
		this.filterRelation = filterRelation;
	}

	public String getSuccessExpressions() {
		return successExpressions;
	}

	public void setSuccessExpressions(String successExpressions) {
		this.successExpressions = successExpressions;
	}

	public Integer getSuccessType() {
		return successType;
	}

	public void setSuccessType(Integer successType) {
		this.successType = successType;
	}

	public Integer getSuccessRelation() {
		return successRelation;
	}

	public void setSuccessRelation(Integer successRelation) {
		this.successRelation = successRelation;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Custom{" + "id=" + id + ", name=" + name + ", applicationId=" + applicationId + ", componentId="
				+ componentId + ", active=" + active + ", alarmState=" + alarmState + ", filterType=" + filterType
				+ ", filterExpressions=" + filterExpressions + ", filterRelation=" + filterRelation
				+ ", successExpressions=" + successExpressions + ", successType=" + successType + ", successRelation="
				+ successRelation + "}";
	}
}
