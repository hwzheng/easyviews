package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 协议识别字段
 * @author MS
 * @date   2018 上午10:08:52
 */
public class JOriginField implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//字段标识
	private String key;
	//字段内容
	private String value;
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "JOriginField [key=" + key + ", value=" + value + "]";
	}
	
}
