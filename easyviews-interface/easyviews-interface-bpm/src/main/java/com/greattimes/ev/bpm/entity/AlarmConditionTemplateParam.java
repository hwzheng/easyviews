package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警模板参数表
 * </p>
 *
 * @author NJ
 * @since 2018-06-05
 */
@TableName("bpm_alarm_condition_template_param")
public class AlarmConditionTemplateParam extends Model<AlarmConditionTemplateParam> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 指标id
     */
	private Integer indicatorId;
    /**
     * 阈值A 或者 topA
     */
	private Double threshold;
    /**
     * 发生次数Z
     */
	private Integer times;
    /**
     * 条件号
     */
	private Integer conditionId;

	/**
	 * 告警id
	 */
	@TableField(exist=false)
	private Integer ruleId;

	/**
	 * 告警条件
	 */
	@TableField(exist=false)
	private Integer relation;

	/**
	 * 模板编号
	 */
	@TableField(exist=false)
	private Integer templateCode;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}

	public Integer getTimes() {
		return times;
	}

	public void setTimes(Integer times) {
		this.times = times;
	}

	public Integer getConditionId() {
		return conditionId;
	}

	public void setConditionId(Integer conditionId) {
		this.conditionId = conditionId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public Integer getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(Integer templateCode) {
		this.templateCode = templateCode;
	}

	@Override
	public String toString() {
		return "AlarmConditionTemplateParam{" +
				"id=" + id +
				", indicatorId=" + indicatorId +
				", threshold=" + threshold +
				", times=" + times +
				", conditionId=" + conditionId +
				", ruleId=" + ruleId +
				", relation=" + relation +
				", templateCode=" + templateCode +
				'}';
	}
}
