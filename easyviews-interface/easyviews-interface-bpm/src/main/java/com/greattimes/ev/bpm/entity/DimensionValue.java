package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_dimension_value")
@ApiModel
public class DimensionValue extends Model<DimensionValue> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.INPUT)
	private Integer id;
    /**
     * 统计名称
     */
	@ApiModelProperty("统计名称")
	private String name;
    /**
     * 统计值
     */
	@ApiModelProperty("统计值")
	private String value;
    /**
     * 统计维度id
     */
	@ApiModelProperty("统计维度id")
	private Integer statisticsDimensionId;

	@ApiModelProperty("自定义业务id")
	@TableField(exist=false)
	private Integer customId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getStatisticsDimensionId() {
		return statisticsDimensionId;
	}

	public void setStatisticsDimensionId(Integer statisticsDimensionId) {
		this.statisticsDimensionId = statisticsDimensionId;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DimensionValue{" +
				"id=" + id +
				", name='" + name + '\'' +
				", value='" + value + '\'' +
				", statisticsDimensionId=" + statisticsDimensionId +
				", customId=" + customId +
				'}';
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((statisticsDimensionId == null) ? 0 : statisticsDimensionId.hashCode());
		return result;
	}
 
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DimensionValue other = (DimensionValue) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		if (statisticsDimensionId == null) {
			if (other.statisticsDimensionId != null) {
				return false;
			}
		} else if (!statisticsDimensionId.equals(other.statisticsDimensionId)) {
			return false;
		}
		return true;
	}

}
