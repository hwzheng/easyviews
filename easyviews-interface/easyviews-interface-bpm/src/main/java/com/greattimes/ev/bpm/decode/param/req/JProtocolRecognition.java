package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * 协议识别实体
 * @author：NJ
 * @date：2018年3月19日 下午4:12:13
 */
public class JProtocolRecognition implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 服务端ip
	 */
	private String serverIp;
	/**
	 * 服务端端口
	 */
	private Integer serverPort;
	/**
	 * 协议类型
	 */
	private String protocolType;
	/**
	 * 报文类型
	 */
	private String messageType;
	/**
	 * 会话数量
	 */
	private Integer conversationCount;
	/**
	 * 会话量
	 */
	private List<JProtocolConversation> conversations;
	
	public JProtocolRecognition() {
		super();
	}
	
	public JProtocolRecognition(String serverIp, Integer serverPort,
			String protocolType, String messageType, Integer conversationCount,
			List<JProtocolConversation> conversations) {
		super();
		this.serverIp = serverIp;
		this.serverPort = serverPort;
		this.protocolType = protocolType;
		this.messageType = messageType;
		this.conversationCount = conversationCount;
		this.conversations = conversations;
	}

	public String getServerIp() {
		return serverIp;
	}
	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}
	public Integer getServerPort() {
		return serverPort;
	}
	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}
	public String getProtocolType() {
		return protocolType;
	}
	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	public Integer getConversationCount() {
		return conversationCount;
	}
	public void setConversationCount(Integer conversationCount) {
		this.conversationCount = conversationCount;
	}
	public List<JProtocolConversation> getConversations() {
		return conversations;
	}
	public void setConversations(List<JProtocolConversation> conversations) {
		this.conversations = conversations;
	}
	
	
}
