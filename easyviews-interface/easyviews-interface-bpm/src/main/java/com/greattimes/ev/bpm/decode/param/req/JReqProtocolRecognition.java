package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 协议识别解析获取会话详情接收实体
 * @author：NJ
 * @date：2018年3月22日 上午10:22:32
 */
public class JReqProtocolRecognition implements Serializable {
	
	private static final long serialVersionUID = 2543651583868845928L;
	/**
	 * 协议类型
	 */
	private String protocolType;
	/**
	 * 报文类型
	 */
	private String messageType;
	/**
	 * c-s原始报文
	 */
	private String clientSendBytes;
	/**
	 * s-c 原始报文
	 */
	private String serverSendBytes;
	
	public String getProtocolType() {
		return protocolType;
	}
	public void setProtocolType(String protocolType) {
		this.protocolType = protocolType;
	}
	public String getClientSendBytes() {
		return clientSendBytes;
	}
	public void setClientSendBytes(String clientSendBytes) {
		this.clientSendBytes = clientSendBytes;
	}
	public String getServerSendBytes() {
		return serverSendBytes;
	}
	public void setServerSendBytes(String serverSendBytes) {
		this.serverSendBytes = serverSendBytes;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@Override
	public String toString() {
		return "JReqProtocolRecognition [protocolType=" + protocolType
				+ ", messageType=" + messageType + ", clientSendBytes="
				+ clientSendBytes + ", serverSendBytes=" + serverSendBytes
				+ "]";
	}
}
