package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 告警IP表
 * </p>
 *
 * @author NJ
 * @since 2018-06-04
 */
@TableName("bpm_alarm_ip")
public class AlarmIp extends Model<AlarmIp> {

    private static final long serialVersionUID = 1L;

    /**
     * 告警id
     */
	private Integer alarmId;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * ip
     */
	private String ip;


	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Override
	public String toString() {
		return "AlarmIp{" +
			"alarmId=" + alarmId +
			", componentId=" + componentId +
			", ip=" + ip +
			"}";
	}
	@Override
	protected Serializable pkVal() {
		return this.alarmId;
	}
}
