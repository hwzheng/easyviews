package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author NJ
 * @since 2018-09-20
 */
@TableName("alarminfo")
public class AlarmInfo extends Model<AlarmInfo> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 时间戳
     */
	private Long time;
    /**
     * 应用id
     */
	private Integer applicationId;
	/**
	 * 应用名称
	 */
	@TableField(exist = false)
	private String applicationName;
    /**
     * 监控点id
     */
	private Integer monitorId;
    /**
     * 组件id
     */
	private Integer componentId;
	/**
	 * 组件名称
	 */
	@TableField(exist = false)
	private String componentName;
    /**
     * 业务id
     */
	private Integer extendId;
	/**
	 * 业务名称
	 */
	@TableField(exist = false)
	private String customName;
    /**
     * 数据源id
     */
	private Integer sourceId;
    /**
     * 规则id
     */
	private Integer ruleId;
    /**
     * 产品类型 1bpm 2扩展bpm 3第三方数据源
     */
	private Integer productType;
    /**
     * 告警数据层级
     */
	private Integer datalevel;
    /**
     * 告警级别
     */
	private Integer alarmlevel;
    /**
     * 告警key
     */
	private String alarmKey;
    /**
     * 告警分组key
     */
	private String groupKey;
    /**
     * 告警数据描述
     */
	private String constraintDesc;
    /**
     * 告警持续时间
     */
	private Integer duration;
    /**
     * 告警处理意见
     */
	private String managerdsec;
    /**
     * 告警处理状态 0未处理 1正常 2异常
     */
	private Integer manageState;
	/**
	 * 告警描述
	 */
	@TableField(exist = false)
	private String ruledesc;
	/**
	 * 告警的开始时间
	 */
	@TableField(exist = false)
	private Long startTime;
	/**
	 * 维度id
	 */
	@TableField(exist = false)
	private Integer dimensionId;
	/**
	 * 维度id
	 */
	@TableField(exist = false)
	private Integer dimensionType;
	/**
	 * 维度值
	 */
	@TableField(exist = false)
	private String dimensionValue;
	/**
	 * 告警类型
	 */
	@TableField(exist = false)
	private Integer ruleType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getExtendId() {
		return extendId;
	}

	public void setExtendId(Integer extendId) {
		this.extendId = extendId;
	}

	public Integer getSourceId() {
		return sourceId;
	}

	public void setSourceId(Integer sourceId) {
		this.sourceId = sourceId;
	}

	public Integer getRuleId() {
		return ruleId;
	}

	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public Integer getProductType() {
		return productType;
	}

	public void setProductType(Integer productType) {
		this.productType = productType;
	}

	public Integer getDatalevel() {
		return datalevel;
	}

	public void setDatalevel(Integer datalevel) {
		this.datalevel = datalevel;
	}

	public Integer getAlarmlevel() {
		return alarmlevel;
	}

	public void setAlarmlevel(Integer alarmlevel) {
		this.alarmlevel = alarmlevel;
	}

	public String getAlarmKey() {
		return alarmKey;
	}

	public void setAlarmKey(String alarmKey) {
		this.alarmKey = alarmKey;
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getConstraintDesc() {
		return constraintDesc;
	}

	public void setConstraintDesc(String constraintDesc) {
		this.constraintDesc = constraintDesc;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getManagerdsec() {
		return managerdsec;
	}

	public void setManagerdsec(String managerdsec) {
		this.managerdsec = managerdsec;
	}

	public Integer getManageState() {
		return manageState;
	}

	public void setManageState(Integer manageState) {
		this.manageState = manageState;
	}

	public String getRuledesc() {
		return ruledesc;
	}

	public void setRuledesc(String ruledesc) {
		this.ruledesc = ruledesc;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getDimensionType() {
		return dimensionType;
	}

	public void setDimensionType(Integer dimensionType) {
		this.dimensionType = dimensionType;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public Integer getRuleType() {
		return ruleType;
	}

	public void setRuleType(Integer ruleType) {
		this.ruleType = ruleType;
	}

	@Override
	public String toString() {
		return "AlarmInfo{" +
				"id=" + id +
				", time=" + time +
				", applicationId=" + applicationId +
				", applicationName='" + applicationName + '\'' +
				", monitorId=" + monitorId +
				", componentId=" + componentId +
				", componentName='" + componentName + '\'' +
				", extendId=" + extendId +
				", customName='" + customName + '\'' +
				", sourceId=" + sourceId +
				", ruleId=" + ruleId +
				", productType=" + productType +
				", datalevel=" + datalevel +
				", alarmlevel=" + alarmlevel +
				", alarmKey='" + alarmKey + '\'' +
				", groupKey='" + groupKey + '\'' +
				", constraintDesc='" + constraintDesc + '\'' +
				", duration=" + duration +
				", managerdsec='" + managerdsec + '\'' +
				", manageState=" + manageState +
				", ruledesc='" + ruledesc + '\'' +
				", startTime=" + startTime +
				", dimensionId=" + dimensionId +
				", dimensionType=" + dimensionType +
				", dimensionValue='" + dimensionValue + '\'' +
				", ruleType=" + ruleType +
				'}';
	}
}
