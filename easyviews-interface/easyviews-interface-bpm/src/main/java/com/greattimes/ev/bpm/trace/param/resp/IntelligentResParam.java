package com.greattimes.ev.bpm.trace.param.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2019/6/21 8:09
 */
public class IntelligentResParam implements Serializable{
    private static final long serialVersionUID = -4081251159298413216L;

    /**
     * 组件id
     */
    private Integer id;
    /**
     * 匹配维度
     */
    private String dimension;

    IntelligentDataResParam data;

    public IntelligentDataResParam getData() {
        return data;
    }

    public void setData(IntelligentDataResParam data) {
        this.data = data;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Override
    public String toString() {
        return "IntelligentResParam{" +
                "id=" + id +
                ", dimension='" + dimension + '\'' +
                ", data=" + data +
                '}';
    }
}
