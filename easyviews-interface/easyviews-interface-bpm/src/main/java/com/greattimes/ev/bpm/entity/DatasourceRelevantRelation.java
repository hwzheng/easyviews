package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据源关联关系表
 * </p>
 *
 * @author cgc
 * @since 2018-08-16
 */
@TableName("bpm_datasource_relevant_relation")
public class DatasourceRelevantRelation extends Model<DatasourceRelevantRelation> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 关联字段
     */
	private Integer relevantField;
    /**
     * 数据源id
     */
	private Integer dataSourceRelevantId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRelevantField() {
		return relevantField;
	}

	public void setRelevantField(Integer relevantField) {
		this.relevantField = relevantField;
	}

	public Integer getDataSourceRelevantId() {
		return dataSourceRelevantId;
	}

	public void setDataSourceRelevantId(Integer dataSourceRelevantId) {
		this.dataSourceRelevantId = dataSourceRelevantId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DatasourceRelevantRelation{" +
			"id=" + id +
			", relevantField=" + relevantField +
			", dataSourceRelevantId=" + dataSourceRelevantId +
			"}";
	}
}
