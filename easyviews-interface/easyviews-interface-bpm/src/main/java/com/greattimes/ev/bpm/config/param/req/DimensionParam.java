package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 组件分析维度
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
@ApiModel
public class DimensionParam implements Serializable{
	/**
	 */
	private static final long serialVersionUID = 183187294958067349L;
	/**
	 * 组件id
	 */
	@ApiModelProperty("组件id")
	private Integer componentId;
	/**
	 * 协议维度id
	 */
	@ApiModelProperty("协议维度id")
	private Integer fieldId;
	/**
	 * 排序
	 */
	@ApiModelProperty("排序")
	private Integer sort;
	/**
	 * 维度名
	 */
	@ApiModelProperty("维度名")
	private String fieldName;
	/**
	 * 别名
	 */
	@ApiModelProperty("别名")
	private String otherName;
	
	@ApiModelProperty("自定义配置id")
	private Integer customId;
	
	@ApiModelProperty("列表id (0代表维度,1代表指标)")
	private Integer menuId;
	 
	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

}
