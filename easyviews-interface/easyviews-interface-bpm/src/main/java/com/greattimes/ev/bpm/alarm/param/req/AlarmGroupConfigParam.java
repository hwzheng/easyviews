package com.greattimes.ev.bpm.alarm.param.req;


import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/1/7 19:32
 */
public class AlarmGroupConfigParam  implements Serializable{
    private static final long serialVersionUID = 1516784214025683577L;
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 激活状态
     */
    private Integer active;

    /**
     * 告警发送类型
     */
    private Integer sendType;
    /**
     * 压缩条数
     */
    private Integer compressNum;
    /**
     * 压缩时间
     */
    private Integer compressTime;
    /**
     * 发送方式 1 邮件 ，2 短信 ，3app ，4外呼
     */
    private List<String> typeList;
    /**
     * 告警级别
     */
    private List<AlarmLevelParam> alarmLevels;

    /**
     * 告警数据表
     */
    private List<AlarmDataParam> alarmData;

    /**
     * 告警用户
     */
    private List<AlarmGroupUserParam> groupUsers;
    /**
     * 告警结束时间戳
     */
    private Long time;
    /**
     * 开始时间
     */
    private Long startTime;
    /**
     * bpm时间颗粒度
     */
    private int interval;
    /**
     * 告警压制时间(分钟)
     */
    private int alarmSendInterval;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getSendType() {
        return sendType;
    }

    public void setSendType(Integer sendType) {
        this.sendType = sendType;
    }

    public Integer getCompressNum() {
        return compressNum;
    }

    public void setCompressNum(Integer compressNum) {
        this.compressNum = compressNum;
    }

    public Integer getCompressTime() {
        return compressTime;
    }

    public void setCompressTime(Integer compressTime) {
        this.compressTime = compressTime;
    }

    public List<String> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<String> typeList) {
        this.typeList = typeList;
    }

    public List<AlarmLevelParam> getAlarmLevels() {
        return alarmLevels;
    }

    public void setAlarmLevels(List<AlarmLevelParam> alarmLevels) {
        this.alarmLevels = alarmLevels;
    }

    public List<AlarmDataParam> getAlarmData() {
        return alarmData;
    }

    public void setAlarmData(List<AlarmDataParam> alarmData) {
        this.alarmData = alarmData;
    }

    public List<AlarmGroupUserParam> getGroupUsers() {
        return groupUsers;
    }

    public void setGroupUsers(List<AlarmGroupUserParam> groupUsers) {
        this.groupUsers = groupUsers;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getAlarmSendInterval() {
        return alarmSendInterval;
    }

    public void setAlarmSendInterval(int alarmSendInterval) {
        this.alarmSendInterval = alarmSendInterval;
    }

    @Override
    public String toString() {
        return "AlarmGroupConfigParam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", active=" + active +
                ", sendType=" + sendType +
                ", compressNum=" + compressNum +
                ", compressTime=" + compressTime +
                ", typeList=" + typeList +
                ", alarmLevels=" + alarmLevels +
                ", alarmData=" + alarmData +
                ", groupUsers=" + groupUsers +
                ", time=" + time +
                ", startTime=" + startTime +
                ", interval=" + interval +
                ", alarmSendInterval=" + alarmSendInterval +
                '}';
    }
}
