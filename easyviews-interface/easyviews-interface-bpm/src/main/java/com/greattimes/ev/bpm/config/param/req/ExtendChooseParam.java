package com.greattimes.ev.bpm.config.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;

/**
 * @author CGC
 * @date 2018/9/26 15:29
 */
@ApiModel
public class ExtendChooseParam implements Serializable{

    private static final long serialVersionUID = -5295604994500088318L;
    /**
     * 数据源id
     */
    private Integer sourcesId;
    /**
     * 指标组id
     */
    private Integer groupId;

    /**
     * 节点
     */
    private List<ExtendChooseDetailParam> choose;

	public Integer getSourcesId() {
		return sourcesId;
	}

	public void setSourcesId(Integer sourcesId) {
		this.sourcesId = sourcesId;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public List<ExtendChooseDetailParam> getChoose() {
		return choose;
	}

	public void setChoose(List<ExtendChooseDetailParam> choose) {
		this.choose = choose;
	}


}
