package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * 通用分组参数开始定位/结束定位参数
 * @author MS
 * @date   2018 上午9:45:11
 */
public class JCommonParam implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//开始类型/结束类型  0：位置索引、1：字节数组、2：字符串
	private Integer type;
	//开始字符/结束字符
	private String string;
	//开始标记/结束标记
	private List<String> mark;
	//开始标记类型/结束标记类型
	private Integer markType;
	//开始位置/结束位置
	private Integer location;
	
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getString() {
		return string;
	}
	public void setString(String string) {
		this.string = string;
	}
	public List<String> getMark() {
		return mark;
	}
	public void setMark(List<String> mark) {
		this.mark = mark;
	}
	public Integer getMarkType() {
		return markType;
	}
	public void setMarkType(Integer markType) {
		this.markType = markType;
	}
	public Integer getLocation() {
		return location;
	}
	public void setLocation(Integer location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "JCommonBegin [type=" + type + ", string=" + string + ", mark="
				+ mark + ", markType=" + markType + ", location=" + location
				+ "]";
	}
}
