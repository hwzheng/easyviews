package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据源关联约束
 * </p>
 *
 * @author NJ
 * @since 2018-08-17
 */
@TableName("ds_relevant_contraint")
public class RelevantContraint extends Model<RelevantContraint> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * ip
     */
	private String ip;
    /**
     * port
     */
	private Integer port;
    /**
     * 约束维度
     */
	private String value;
    /**
     * 指标组id
     */
	private Integer indicatorGroupId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getIndicatorGroupId() {
		return indicatorGroupId;
	}

	public void setIndicatorGroupId(Integer indicatorGroupId) {
		this.indicatorGroupId = indicatorGroupId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "RelevantContraint{" +
			"id=" + id +
			", componentId=" + componentId +
			", ip=" + ip +
			", port=" + port +
			", value=" + value +
			", indicatorGroupId=" + indicatorGroupId +
			"}";
	}
}
