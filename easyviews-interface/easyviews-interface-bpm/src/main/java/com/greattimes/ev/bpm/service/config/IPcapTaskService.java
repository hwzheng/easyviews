package com.greattimes.ev.bpm.service.config;

import com.greattimes.ev.bpm.entity.PcapTask;

import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * pca下载任务 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-11-07
 */
public interface IPcapTaskService extends IService<PcapTask> {

	/**列表查询
	 * @param page
	 * @param map
	 * @return
	 */
	Page<Map<String,Object>> selectPcapTaskByMap(Page<Map<String, Object>> page, Map<String, Object> map);

	/**校验ip
	 * @param ip
	 * @param centerId 
	 * @param port 
	 * @return
	 */
	boolean checkIp(String ip, Integer centerId, String port);

	/**任务新建
	 * @param userId
	 * @param jsonObject
	 * @return
	 */
	int insertTask(Integer userId, JSONObject jsonObject);

}
