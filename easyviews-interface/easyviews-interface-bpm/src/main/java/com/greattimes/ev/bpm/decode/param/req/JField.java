package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 解析字段
 * @author MS
 * @date   2018 上午9:33:17
 */
public class JField implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//字段主键
	private Integer id;
	//字段标识
	private String key;
	//字段ename
	private String useForKey;
	//数据类型 1：字符串、2：数值、3：布尔值、4：字节数组
	private Integer dataType;
	//字段长度
	private Integer length;
	//内联分组
	private Integer asGroup;
	//字段用途  1：交易类型、2：交易流水号、3：交易渠道、4：交易时间、5：返回码 6：自定义、7：拆分子组
	private Integer useFor;
	//字段样例值
	private String sampleVal;
	//取值表达式1
	private String valExpr1;
	//取值表达式2
	private String valExpr2;
	//是否需要级联
	private Boolean isCascade;
	//是否传输字段  控制是否将该字段的内容发送至Kafka
	private Boolean isTransport;
	//排序索引
	private Integer index;
	//绑定扩展主键
	private Integer extendId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getUseForKey() {
		return useForKey;
	}
	public void setUseForKey(String useForKey) {
		this.useForKey = useForKey;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Integer getDataType() {
		return dataType;
	}
	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getAsGroup() {
		return asGroup;
	}
	public void setAsGroup(Integer asGroup) {
		this.asGroup = asGroup;
	}
	public Integer getUseFor() {
		return useFor;
	}
	public void setUseFor(Integer useFor) {
		this.useFor = useFor;
	}
	public String getSampleVal() {
		return sampleVal;
	}
	public void setSampleVal(String sampleVal) {
		this.sampleVal = sampleVal;
	}
	public String getValExpr1() {
		return valExpr1;
	}
	public void setValExpr1(String valExpr1) {
		this.valExpr1 = valExpr1;
	}
	public String getValExpr2() {
		return valExpr2;
	}
	public void setValExpr2(String valExpr2) {
		this.valExpr2 = valExpr2;
	}
	public Boolean getIsCascade() {
		return isCascade;
	}
	public void setIsCascade(Boolean isCascade) {
		this.isCascade = isCascade;
	}
	public Boolean getIsTransport() {
		return isTransport;
	}
	public void setIsTransport(Boolean isTransport) {
		this.isTransport = isTransport;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public Integer getExtendId() {
		return extendId;
	}
	public void setExtendId(Integer extendId) {
		this.extendId = extendId;
	}
	
	
	@Override
	public String toString() {
		return "JField [id=" + id + ", key=" + key + ", dataType=" + dataType + ", length=" + length + ", asGroup="
				+ asGroup + ", useFor=" + useFor + ", useForKey=" + useForKey + ", sampleVal=" + sampleVal
				+ ", valExpr1=" + valExpr1 + ", valExpr2=" + valExpr2 + ", isCascade=" + isCascade + ", isTransport="
				+ isTransport + ", index=" + index + ", extendId=" + extendId + "]";
	}

}
