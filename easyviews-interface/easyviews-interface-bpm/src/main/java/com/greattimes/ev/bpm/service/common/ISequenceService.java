package com.greattimes.ev.bpm.service.common;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author LuoX
 * @since 2017-04-19
 */
public interface ISequenceService {
	int sequenceNextVal(String sequenceName);
}
