package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 告警规则表
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
@TableName("bpm_alarm_rule")
public class AlarmRule extends Model<AlarmRule> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 应用id
	 */
	private Integer applicationId;
	/**
	 * 告警时段类型 1 每天 2 工作日 3 非工作日
	 */
	private Integer timeType;
	/**
	 * 时间窗口  配置字典 单位分钟
	 */
	private Integer timeFrame;
	/**
	 * 告警类型 1 应用 ，2 组件， 3ip， 4port，5二级维度 ，6 多维度
	 */
	private Integer type;
	/**
	 * 条件关系 1 任一条件 ，2 全部条件
	 */
	private Integer relation;
	/**
	 * 激活状态 0 关闭 1开启
	 */
	private Integer active;
	/**
	 * 高警级别
	 */
	private Integer level;
	/**
	 * '规则描述'
	 */
	private String desc;

	/**
	 * 告警描述flag(手动or自动)
	 */
	private Boolean descFlag;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getTimeType() {
		return timeType;
	}

	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}

	public Integer getTimeFrame() {
		return timeFrame;
	}

	public void setTimeFrame(Integer timeFrame) {
		this.timeFrame = timeFrame;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Boolean getDescFlag() {
		return descFlag;
	}

	public void setDescFlag(Boolean descFlag) {
		this.descFlag = descFlag;
	}

	@Override
	public String toString() {
		return "AlarmRule{" +
				"id=" + id +
				", applicationId=" + applicationId +
				", timeType=" + timeType +
				", timeFrame=" + timeFrame +
				", type=" + type +
				", relation=" + relation +
				", active=" + active +
				", level=" + level +
				", desc='" + desc + '\'' +
				", descFlag=" + descFlag +
				'}';
	}
}
