package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 探针口表
 * </p>
 *
 * @author cgc
 * @since 2018-08-08
 */
@TableName("bpm_center_probe_port")
@ApiModel
public class CenterProbePort extends Model<CenterProbePort> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
	private Integer id;
    /**
     * 探针id
     */
    @ApiModelProperty("探针id")
	private Integer probeId;
    /**
     * 端口类型
     */
    @ApiModelProperty("端口类型")
	private String portType;
    /**
     * 端口号
     */
    @ApiModelProperty("端口号")
	private Integer portCode;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProbeId() {
		return probeId;
	}

	public void setProbeId(Integer probeId) {
		this.probeId = probeId;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}


	public Integer getPortCode() {
		return portCode;
	}

	public void setPortCode(Integer portCode) {
		this.portCode = portCode;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "CenterProbePort{" +
			"id=" + id +
			", probeId=" + probeId +
			", portType=" + portType +
			", protCode=" + portCode +
			"}";
	}
}
