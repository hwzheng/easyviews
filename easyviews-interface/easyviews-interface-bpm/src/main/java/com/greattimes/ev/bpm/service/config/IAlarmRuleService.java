package com.greattimes.ev.bpm.service.config;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.AlarmParam;
import com.greattimes.ev.bpm.entity.AlarmConditionTemplateParam;
import com.greattimes.ev.bpm.entity.AlarmMonitor;
import com.greattimes.ev.bpm.entity.AlarmRule;

/**
 * <p>
 * 告警规则表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
public interface IAlarmRuleService extends IService<AlarmRule> {

	/**
	 * 告警查询
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectAlarmRule(Map<String, Object> map);

	/**
	 * 告警删除
	 * @param id
	 */
	void deleteAlarmRule(int id);

	/**
	 * 告警状态激活
	 * @param id
	 * @param active
	 * @return
	 */
	int  updateActive(int id, int active);

	/**
	 * 告警状态批量激活
	 * @author NJ
	 * @date 2018/6/7 14:38
	 * @param
	 * @return int
	 */
	int updateBatchActive(int appid, int active, int type);


	/**
	 * 应用告警详情
	 * @param alarmId
	 * @RETURN
	 */
	AlarmParam getApplicationAlarmDetail(int alarmId,List<String> indicatorIds);


	void addAlarm(AlarmParam param, List<String> indicatorIds);

	void updateAlarm(AlarmParam param, List<String> indicatorIds);

	/**
	 * 组件告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getComponentAlarmDetail(int alarmId, List<String> indicatorIds);

	/**
	 * ip 告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getIpAlarmDetail(int alarmId, List<String> indicatorIds);

	/**
	 * ip/port 告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getIpPortAlarmDetail(int alarmId,List<String> indicatorIds);

	/**
	 * 单维度告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getSingleDimensionDetail(int alarmId,List<String> indicatorIds);

	/**
	 * 多维度告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getMultiDimensionDetail(int alarmId,List<String> indicatorIds);


	/**
	 * 监控点告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getMonitorDimensionDetail(int alarmId,List<String> indicatorIds);

	/**
	 * 数据源告警详情
	 * @param alarmId
	 * @return
	 */
	AlarmParam getDataSourceAlarmDetail(int alarmId,List<String> indicatorIds);

	/**
	 * 根据状态批量激活
	 * @param ids
	 * @param active
	 * @return
	 */
	boolean updateBatchActiveByIds(List<Integer> ids, int active);


	/**
	 * 根据状态批量激活(自定义告警)
	 * @param customId
	 * @param active
	 * @param type
	 * @return
	 */
	boolean updateBatchActiveCustom(int customId, int active, int type);


	/**
	 * 自定义告警查询告警查询
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectCustomAlarmRule(Map<String, Object> map);

	/**
	 * 根据组件删除告警
	 * @param componentIds
	 * @return
	 */
	boolean deleteAlarmRuleByComponentIds(List<Integer> componentIds);

	/**
	 * 组件更新ip.port，删除对应的告警
	 * @param ipStr
	 * @param ipPortStr
	 * @return
	 */
	boolean deleteAlarmRuleByIpPorts(Set<String> ipStr, Set<String> ipPortStr, int componentId);

	/**
	 * 根据id删除告警
	 * @param ids
	 * @return
	 */
	boolean  deleteAlarmRuleByIds(List<Integer> ids);

	/**
	 * 根据监控点查询bpm_alarm_monitor
	 * @param monitorId
	 * @return
	 */
	List<AlarmMonitor>  selectAlarmMonitorByMonitorId(int monitorId);

	/**
	 * 根据维度id删除告警
	 * @param dimensionIds
	 * @return
	 */
	boolean deleteAlarmRuleByDimensionIds(List<Integer> dimensionIds, int type);

	/**
	 * 根据告警规则id查询告警模板参数
	 * @param ruleId
	 * @return
	 */
	List<AlarmConditionTemplateParam> selectTemplateParamByRuleId(int ruleId);

	/**
	 * 根据组件id查询告警信息
	 * @param componentIds
	 * @author: nj
	 * @date: 2020-03-12 19:31
	 * @version: 0.0.1
	 * @return: java.util.List<com.greattimes.ev.bpm.entity.AlarmRule>
	 */
	List<AlarmRule> selectAlarmRuleByComponentIds(List<Integer> componentIds);
}
