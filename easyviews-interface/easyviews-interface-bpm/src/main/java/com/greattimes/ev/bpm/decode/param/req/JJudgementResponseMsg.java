package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 交易判定响应报文项
 * @author MS
 * @date   2018 上午11:02:32
 */
public class JJudgementResponseMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//关系  1 包含指定内容/ 2匹配正则表达式
	private Integer relationship;
	//类型值
	private String value;
	
	public Integer getRelationship() {
		return relationship;
	}

	public void setRelationship(Integer relationship) {
		this.relationship = relationship;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "JJudgementRetCode [relationship=" + relationship + ", value="
				+ value + "]";
	}
}
