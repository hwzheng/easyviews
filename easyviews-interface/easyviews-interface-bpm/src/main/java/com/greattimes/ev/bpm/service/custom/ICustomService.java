package com.greattimes.ev.bpm.service.custom;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.config.param.req.DimensionParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomBasicParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomFieldParam;
import com.greattimes.ev.bpm.custom.param.resp.CustomParam;
import com.greattimes.ev.bpm.entity.AnalyzeIndicator;
import com.greattimes.ev.bpm.entity.Custom;
import com.greattimes.ev.bpm.entity.DimensionValue;
import com.greattimes.ev.bpm.entity.StatisticsDimension;
/**
 * <p>
 * 自定义分析配置 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-06-25
 */
public interface ICustomService extends IService<Custom> {

	/**
	 * 自定义配置列表查询
	 * @param user 
	 * @return
	 */
	List<CustomParam> selectCustom(Integer userId);

	/**
	 * 自定义配置排序
	 * @param javaList
	 */
	void updateSort(List<CustomParam> ids);

	/**
	 * 自定义配置删除
	 * @param id
	 * @param type  1 外部删除 2 基本配置组件改变时删除
	 */
	void deleteCustom(int id, int type);

	/**
	 * 自定义配置批量激活修改
	 * @param javaList
	 */
	void updateActive(List<CustomParam> javaList);

	/**
	 * 自定义配置批量告警状态修改
	 * @param javaList
	 */
	void updateState(List<CustomParam> javaList);

	/**
	 * 分析维度信息列表查询
	 * @param customId
	 * @return
	 */
	List<CustomFieldParam> selectDimension(int customId);

	/**
	 * 指标信息列表查询
	 * @param customId
	 * @return
	 */
	List<Map<String, Object>> selectIndicator(int customId);

	/**
	 * 指标信息保存
	 * @param customId
	 * @param list 
	 */
	void addIndicator(int customId, List<CustomFieldParam> list);

	/**
	 * 维度信息保存
	 * @param customId
	 * @param list
	 */
	void addDimension(int customId, List<CustomFieldParam> list);

	/**
	 * 统计维度列表查询 更新步骤状态
	 * @param customId
	 * @return
	 */
	List<StatisticsDimension> updateStepAndselectStatistics(int customId);

	/**
	 * 别名查询
	 * @param customId
	 * @param menuId
	 * @return
	 */
	List<DimensionParam> selectOtherName(Integer customId, Integer menuId);

	/**
	 * 别名保存
	 * @param param
	 */
	void saveOtherName(DimensionParam param);

	/**
	 * 基本信息新增
	 * @param applicationId
	 * @param componentId
	 * @param name
	 * @return
	 */
	int addBasicMsg(int applicationId, int componentId, String name);

	/**
	 * 基本信息编辑
	 * @param id
	 * @param applicationId
	 * @param componentId
	 * @param name
	 * @param change
	 */
	void editBasicMsg(int id, int applicationId, int componentId, String name, Boolean change);

	/**
	 * 基本信息查询
	 * @param id
	 * @return
	 */
	Custom selectBasicMsg(int id);

	/**
	 * 上传信息查看
	 * @param statisticsDimensionId
	 * @return
	 */
	List<DimensionValue> selectDimensionDetail(int statisticsDimensionId);

	/**
	 * 统计维度编辑
	 * @param id
	 * @param name
	 * @param useDictionary 
	 */
	void editDimension(int id, int DimensionId, String name, Integer useDictionary);

	/**
	 * 统计维度删除
	 * @param id
	 */
	void deleteDimension(int id);

	/**
	 * 过滤条件查询
	 * @param customId
	 * @return
	 */
	CustomBasicParam updateStepAndselectFilter(int customId);

	/**
	 * 成功条件查询  更新步骤状态
	 * @param mapIntValue
	 * @return
	 */
	CustomBasicParam updateStepAndselectSuccess(int customId);

	/**
	 * 过滤条件保存
	 * @param param
	 */
	void saveFilter(CustomBasicParam param);

	/**
	 * 成功条件保存
	 * @param param
	 */
	void saveSuccess(CustomBasicParam param);

	/**
	 * 文件上传添加
	 * @param details
	 * @param statisticsDimensionId
	 */
	void insertDimensionValue(List<DimensionValue> details, int statisticsDimensionId);

	/**
	 * 统计维度新增
	 * @param dimension
	 * @return
	 */
	Integer addstatisticsDimension(StatisticsDimension dimension);

	/**
	 * 校验名称
	 * @param statisticsDimension
	 * @param i 0：编辑 1：新增
	 * @return
	 */
	Boolean checkName(StatisticsDimension statisticsDimension, int i);

	/**
	 * 自定义配置name校验
	 * @param cs
	 * @param componentId
	 * @param type 0：编辑 1：新增
	 * @return
	 */
	Boolean checkCustomName(CustomParam cs, int componentId, int type);

	/**
	 * 步骤查询
	 * @param customId
	 * @return
	 */
	String selectStep(int customId);

	/**
	 * 保存Uuid
	 * @param statisticsDimensionId
	 */
	void saveUuid(int statisticsDimensionId);

	/**
	 * 根据自定义分析查询维度，包括自定义分析维度(type=0)自定义统计维度(type=1) 以及而且全部 type=-1
	 * @author NJ
	 * @date 2018/8/29 14:40
	 * @param customId
	 * @param type
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectDimensionByCustomIdAndType(int customId, int type);
	
	/**    
	 * 根据自定义分析查询维度，包括自定义分析维度(type=0)自定义统计维度(type=1) 以及而且全部 type=-1
	 * 翻译或者别名存在truename字段中
	 * @author NJ  
	 * @date 2018/10/17 21:01
	 * @param customId
	 * @param type  
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>  
	 */  
	List<Map<String, Object>> selectDimensionOtherNameByCustomIdAndType(int customId, int type);

	/**
	 * 根据自定义分析查询维度，包括自定义分析维度(type=0)自定义统计维度(type=1) 以及而且全部 type=-1
	 * 不包含翻译
	 * @author NJ
	 * @date 2018/12/19 17:23
	 * @param customId
	 * @param type
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectCustomDimensionByCustomIdAndType(int customId, int type);

	/**
	 * 根据组件id和业务id查询交易追踪字段
	 * @author NJ
	 * @date 2018/11/27 14:06
	 * @param
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String,Object>> selectTraceDimensionByParam(Map<String, Object> param);

	/**
	 * 根据id查询统计纬度值
	 * @author NJ
	 * @date 2018/8/29 18:14
	 * @param id
	 * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
	 */
	List<DictionaryParam> selectStatisticsDimensionValueDict(int id);
	
	/**
	 * 筛选条件删除
	 * @param ids 维度ID
	 */
	void deleteFilterField(List<Integer> ids);

	/**
	 * 统计维度删除
	 * @param ids 维度ID
	 */
	void deletectStatisticsDimension(List<Integer> ids);

	/**
	 ** 统计维度删除
	 * @param ids 维度ID
	 */
	void deletectSuccessField(List<Integer> ids);


	/**
	 * 根据组件id删除自定义配置
	 * @author NJ
	 * @date 2018/9/4 19:52
	 * @param ids
	 * @return boolean
	 */
	boolean deleteCustomByComponentIds(List<Integer> ids);



	/**
	 * 根据组件id查询自定义业务指标(别名单位...)
	 * @author NJ
	 * @date 2018/9/29 10:22
	 * @param componentId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> findCustomIndicatorByComponentId(int componentId);

	/**
	 * 根据应id用查询自定义业务tree
	 * @author NJ
	 * @date 2018/10/9 13:11
	 * @param appId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectCustomTreeByAppId(int appId);

	List<Custom> selectCustomBycomponentId(Map<String, Object> map);
	/**查询分析维度和类型为普通的统计维度
	 * @param customId
	 * @return
	 */
	List<Map<String, Object>> selectCommonDimension(@Param("customId") Integer customId);

	/**
	 * 事件树
	 * @param map
	 * @return
	 */
	List<ApplicationTree> selectCustomTreeForAi(Map<String, Object> map);

	/**
	 * 根据事件的id查询指标名称
	 * @author NJ
	 * @date 2019/4/8 8:53
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectCustomIndicatorNameByCustomIds(Map<String, Object> map);

	/**
	 * 根据事件id查询
	 * @author NJ
	 * @date 2019/4/8 16:34
	 * @param map
	 * @return java.util.List<com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree>
	 */
	List<ApplicationTree> selectStatisticsDimensionForTree(Map<String, Object> map);



}
