package com.greattimes.ev.bpm.service.decode;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.bpm.entity.Protocol;

import java.util.Map;

/**
 * @author NJ
 * @date 2018/8/21 14:30
 */
public interface ICustomProtocolService {

    /**
     * 分页查询自定义协议列表
     * @param page
     * @param map
     * @return
     */
    Page<Protocol> queryCustomProtocol(Page<Protocol> page, Map<String, Object> map);


}
