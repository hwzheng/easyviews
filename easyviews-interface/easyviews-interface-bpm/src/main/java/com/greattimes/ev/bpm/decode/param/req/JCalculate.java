package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 计算操作
 * @author MS
 * @date   2018 下午5:31:53
 */
public class JCalculate implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7211038135037032740L;
	//输入
	private JCalculateIn in;
	//输出
	private JCalculateOut out;
	//排序
	private Integer index;
	
	public JCalculateIn getIn() {
		return in;
	}
	public void setIn(JCalculateIn in) {
		this.in = in;
	}
	public JCalculateOut getOut() {
		return out;
	}
	public void setOut(JCalculateOut out) {
		this.out = out;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	@Override
	public String toString() {
		return "JCalculate [in=" + in + ", out=" + out + ", index=" + index
				+ "]";
	}
}
