package com.greattimes.ev.bpm.custom.param.resp;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义分析配置筛选条件和成功条件具体值包装类
 * </p>
 *
 * @author Cgc
 * @since 2018-05-23
 */
@ApiModel
public class CustomFieldValueParam extends Model<CustomFieldValueParam> {

	private static final long serialVersionUID = 1L;
	@ApiModelProperty("维度id")
	private Integer dimensionId;
	@ApiModelProperty("关系")
	private Integer relation;
	@ApiModelProperty("序号")
	private Integer index;
	@ApiModelProperty("过滤值")
	private String values;

	@Override
	protected Serializable pkVal() {
		return null;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getRelation() {
		return relation;
	}

	public void setRelation(Integer relation) {
		this.relation = relation;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getValues() {
		return values;
	}

	public void setValues(String values) {
		this.values = values;
	}

}
