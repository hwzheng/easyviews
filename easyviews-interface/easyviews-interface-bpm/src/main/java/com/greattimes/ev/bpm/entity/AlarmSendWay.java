package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警发送方式
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
@TableName("bpm_alarm_send_way")
public class AlarmSendWay extends Model<AlarmSendWay> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 告警组id
     */
	private Integer alarmGroupId;
    /**
     * 发送方式 1 邮件 ，2 短信 ，3app ，4外呼   (英文逗号分隔)
     */
	private Integer type;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAlarmGroupId() {
		return alarmGroupId;
	}

	public void setAlarmGroupId(Integer alarmGroupId) {
		this.alarmGroupId = alarmGroupId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmSendWay{" +
			"id=" + id +
			", alarmGroupId=" + alarmGroupId +
			", type=" + type +
			"}";
	}
}
