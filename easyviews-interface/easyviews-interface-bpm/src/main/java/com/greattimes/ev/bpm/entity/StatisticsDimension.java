package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义分析统计维度表
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_statistics_dimension")
@ApiModel
public class StatisticsDimension extends Model<StatisticsDimension> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value="id", type= IdType.INPUT)
	@ApiModelProperty("id")
	private Integer id;
	@NotNull(message="customId不能为空！")
	@ApiModelProperty("自定义配置id")
	private Integer customId;
	/**
	 * 协议维度id
	 */
	@NotNull(message="dimensionId不能为空！")
	@ApiModelProperty("协议维度id")
	private Integer dimensionId;
	/**
	 * 别名
	 */
	@NotEmpty(message="别名不能为空！")
	@Length(min=1, max=50,message="别名不能超过50位")
	@ApiModelProperty("别名")
	private String name;
	/**
	 * 数据类型 : 0 普通 1 省份 2 地市 3 地区
	 */
	@ApiModelProperty("数据类型 : 0 普通 1 省份 2 地市 3 地区")
	private Integer type;
	@ApiModelProperty("地市")
	private Integer cityId;
	@ApiModelProperty("省份")
	private Integer provinceId;
	@ApiModelProperty("使用字典")
	private Integer useDictionary;
	@TableField(exist=false)
	@ApiModelProperty("维度名")
	private String dimensionName;
	@TableField(exist=false)
	@ApiModelProperty("ename")
	private String ename;

	/**
	 * 自定义业务名称
	 */
	@ApiModelProperty("自定义业务名称")
	@TableField(exist = false)
	private String customName;

	/**
	 * 维度值id
	 */
	@ApiModelProperty("维度值id")
	@TableField(exist = false)
	private Integer dimensionValueId;

	/**
	 * 维度值
	 */
	@ApiModelProperty("维度值")
	@TableField(exist = false)
	private String dimensionValue;


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		StatisticsDimension that = (StatisticsDimension) o;
		return that.getId().equals(this.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public String getDimensionName() {
		return dimensionName;
	}

	public void setDimensionName(String dimensionName) {
		this.dimensionName = dimensionName;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	public Integer getDimensionValueId() {
		return dimensionValueId;
	}

	public void setDimensionValueId(Integer dimensionValueId) {
		this.dimensionValueId = dimensionValueId;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public Integer getUseDictionary() {
		return useDictionary;
	}

	public void setUseDictionary(Integer useDictionary) {
		this.useDictionary = useDictionary;
	}

	@Override
	public String toString() {
		return "StatisticsDimension [id=" + id + ", customId=" + customId + ", dimensionId=" + dimensionId + ", name="
				+ name + ", type=" + type + ", cityId=" + cityId + ", provinceId=" + provinceId + ", useDictionary="
				+ useDictionary + ", dimensionName=" + dimensionName + ", ename=" + ename + ", customName=" + customName
				+ ", dimensionValueId=" + dimensionValueId + ", dimensionValue=" + dimensionValue + "]";
	}

}
