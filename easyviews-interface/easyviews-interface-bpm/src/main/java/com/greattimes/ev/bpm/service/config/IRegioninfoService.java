package com.greattimes.ev.bpm.service.config;

import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.entity.Regioninfo;

import java.util.List;

import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author cgc
 * @since 2018-07-02
 */
public interface IRegioninfoService extends IService<Regioninfo> {

	/**
	 * 省份查询
	 * @return
	 */
	List<DictionaryParam> selectProvince();

	/**
	 * 地市查询
	 * @param provinceId
	 * @return
	 */
	List<DictionaryParam> selectCity(int provinceId);

	/**
	 * 获取地市
	 * @param province
	 * @param cityId
	 * @return
	 */
	List<Regioninfo> getCity(Integer province, Integer cityId);

}
