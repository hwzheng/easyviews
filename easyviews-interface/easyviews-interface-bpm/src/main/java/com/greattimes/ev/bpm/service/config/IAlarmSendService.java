package com.greattimes.ev.bpm.service.config;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.AlarmSendListParam;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.greattimes.ev.bpm.entity.AlarmSend;

/**
 * <p>
 * 告警信息发送配置表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
public interface IAlarmSendService extends IService<AlarmSend> {

	/**
	 * 告警发送列表查询
	 * @param map
	 * @return
	 */
	List<AlarmSend> selectAlarmSend(Map<String, Object> map);

	/**
	 * 告警发送列表新增
	 * @param alarmSend
	 */
	void addAlarmSend(AlarmSend alarmSend);

	/**
	 * 告警发送列表编辑
	 * @param alarmSend
	 */
	void editAlarmSend(AlarmSend alarmSend);

	/**
	 * 告警组查询
	 * @return
	 */
	List<AlarmGroup> selectAlarmGroup();

	/**
	 * 告警发送列表删除
	 * @param id
	 */
	void deleteAlarmSend(int id);

	/**
	 * 告警发送列表状态更改
	 * @param param
	 */
	void updateAlarmSendActive(AlarmSendListParam param);

}
