package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 路径图节点表
 * </p>
 *
 * @author NJ
 * @since 2018-09-11
 */
@TableName("path_node")
public class PathNode extends Model<PathNode> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 关联id
     */
	private Integer relevanceId;
    /**
     * 节点类型 1 应用 2 监控点 3 组件 4 子节点
     */
	private Integer nodeType;
    /**
     * x (左侧边距)
     */
	private Integer left;
    /**
     * y（顶部边距）
     */
	private Integer top;
    /**
     * 节点样式 
     */
	private Integer nodeStyle;
    /**
     * 视图id
     */
	private Integer viewId;

	/**
	 * 节点名称(表中不存在，用作前台使用)
	 */
	@TableField(exist = false)
	private String name;

    /**
     * 是否激活
     */
    @TableField(exist = false)
	private Integer active;
    /**
     * 应用id
     */
    @TableField(exist = false)
	private Integer applicationId;

	/**
	 * 监控点id
	 */
	@TableField(exist = false)
	private Integer monitorId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRelevanceId() {
		return relevanceId;
	}

	public void setRelevanceId(Integer relevanceId) {
		this.relevanceId = relevanceId;
	}

	public Integer getNodeType() {
		return nodeType;
	}

	public void setNodeType(Integer nodeType) {
		this.nodeType = nodeType;
	}

	public Integer getLeft() {
		return left;
	}

	public void setLeft(Integer left) {
		this.left = left;
	}

	public Integer getTop() {
		return top;
	}

	public void setTop(Integer top) {
		this.top = top;
	}

	public Integer getNodeStyle() {
		return nodeStyle;
	}

	public void setNodeStyle(Integer nodeStyle) {
		this.nodeStyle = nodeStyle;
	}

	public Integer getViewId() {
		return viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

	public Integer getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(Integer monitorId) {
		this.monitorId = monitorId;
	}

	@Override
	public String toString() {
		return "PathNode{" +
				"id=" + id +
				", relevanceId=" + relevanceId +
				", nodeType=" + nodeType +
				", left=" + left +
				", top=" + top +
				", nodeStyle=" + nodeStyle +
				", viewId=" + viewId +
				", name='" + name + '\'' +
				", active=" + active +
				", applicationId=" + applicationId +
				", monitorId=" + monitorId +
				'}';
	}
}
