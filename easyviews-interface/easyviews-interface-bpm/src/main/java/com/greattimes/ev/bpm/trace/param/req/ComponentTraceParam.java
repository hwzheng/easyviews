package com.greattimes.ev.bpm.trace.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/6/19 16:46
 */
public class ComponentTraceParam implements Serializable{
    private static final long serialVersionUID = -3756845353022925891L;

    @ApiModelProperty("组件id")
    private Integer id;
    @ApiModelProperty("开始时间戳")
    private Long startTime;
    @ApiModelProperty("结束时间戳")
    private Long endTime;
    @ApiModelProperty("每页条数 默认10")
    private Integer pageSize;
    @ApiModelProperty("查询页数 默认1")
    private Integer pageNum;


    /**
     * key: rule  排序规则1 升序 2降序 int
     *     dimension  排序字段 String
     *     value	String	是	值（多值逗号分隔）
     */
    @ApiModelProperty("排序")
    private OrderParam order;

    /**
     * key:dimension  维度key string
     *     rule  维度筛选规则（1包含，2不包含，3等于，4不等于 5字符串包含 ，6 字符串不包含）  int
     *     value 值 string
     */
    @ApiModelProperty("维度筛选")
    private List<FilterDimensionParam> filterDimension;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public OrderParam getOrder() {
        return order;
    }

    public void setOrder(OrderParam order) {
        this.order = order;
    }

    public List<FilterDimensionParam> getFilterDimension() {
        return filterDimension;
    }

    public void setFilterDimension(List<FilterDimensionParam> filterDimension) {
        this.filterDimension = filterDimension;
    }

    @Override
    public String toString() {
        return "ComponentTraceParam{" +
                "id=" + id +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", pageSize=" + pageSize +
                ", pageNum=" + pageNum +
                ", order=" + order +
                ", filterDimension=" + filterDimension +
                '}';
    }
}
