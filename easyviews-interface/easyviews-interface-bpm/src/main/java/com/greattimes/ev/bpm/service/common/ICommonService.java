package com.greattimes.ev.bpm.service.common;

import com.greattimes.ev.bpm.config.param.resp.TranslateDataParam;
import com.greattimes.ev.bpm.entity.*;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/27 20:55
 */
public interface ICommonService {

    /**
     * 查询协议字段表数据
     *
     * @param ids
     * @return java.util.List<com.greattimes.ev.bpm.entity.ProtocolField>
     * @author NJ
     * @date 2018/9/27 20:57
     */
    List<ProtocolField> findFieldByIds(List<Integer> ids);

    /**
     * 查询指标表数据
     *
     * @param ids
     * @return java.util.List<com.greattimes.ev.bpm.entity.Indicator>
     * @author NJ
     * @date 2018/9/27 20:57
     */
    List<Indicator> findIndicatorByIds(List<Integer> ids);


    /**
     * 查询指标表数据map key:id
     * @author NJ
     * @date 2018/10/22 21:53
     * @param ids
     * @return java.util.Map<java.lang.Integer,com.greattimes.ev.bpm.entity.Indicator>
     */
	Map<Integer, Indicator> findIndicatorMapByIds(List<Integer> ids);

    /**
     * 根据组件id查询serverIp
     *
     * @param componentIds
     * @return java.util.List<com.greattimes.ev.bpm.entity.ServerIp>
     * @author NJ
     * @date 2018/10/8 10:07
     */
    List<ServerIp> findServerIpsByComponentIds(List<Integer> componentIds);

    /**
     * 根据ipId查询server
     *
     * @param iPId
     * @return java.util.List<com.greattimes.ev.bpm.entity.Server>
     * @author NJ
     * @date 2018/10/8 14:37
     */
    List<Server> selectPortByIpId(int iPId);

    /**
     * 根据组件id查询ct_custom
     *
     * @param componentId
     * @return java.util.List<com.greattimes.ev.bpm.entity.Custom>
     * @author NJ
     * @date 2018/10/8 14:43
     */
    List<Custom> selectCustomByComponentId(int componentId);
    
    /**
     * 根据customid查询compont
     * @author CGC
     * @param customId
     * @return
     */
    Component selectComponentByCustomId(int customId);

    /**
     * @author NJ
     * @date 2018/10/8 15:08
     * @param statisDimensionId
     * @return java.util.List<com.greattimes.ev.bpm.entity.DimensionValue>
     */
    List<DimensionValue> selectDimensionValueByStatisticsDimensionId(int statisDimensionId);

	/**
	 * 获取组建维度的翻译值
	 * @author cgc  
	 * @date 2018年10月10日  
	 * @param uuid
	 * @return
	 */
	List<TranslateDataParam> getComponentTranslate(Long uuid);

	/**
	 * 获取业务维度的翻译值
	 * @author cgc  
	 * @date 2018年10月11日  
	 * @param uuid
	 * @return
	 */
	List<TranslateDataParam> getCustomTranslate(Long uuid);
	
	/**生成kpi的key
	 * @param type 1 指标 2维度
	 * @param map
	 * @return
	 */
	String getKpiKey(int type,Map<String, Object> map);

	/**
	 * 根据自定义id查询自定义业务下的所有指标名称
	 * @author NJ
	 * @date 2019/4/8 9:02
	 * @param map
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> selectCustomIndicatorNamesByMap(Map<String, Object> map);
    
	/**查询指标返回ename
     * @author CGC
     * @param type 指标类型
     * @return
     */
    List<String> selectIndicator(List<Integer> type);
}

