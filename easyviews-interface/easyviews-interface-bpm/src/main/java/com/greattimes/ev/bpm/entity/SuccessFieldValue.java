package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 自定义成功字段值
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_success_field_value")
public class SuccessFieldValue extends Model<SuccessFieldValue> {

    private static final long serialVersionUID = 1L;

    /**
     * 过滤字表id
     */
	private Integer successFieldId;
    /**
     * 过滤值
     */
	private String value;



	public Integer getSuccessFieldId() {
		return successFieldId;
	}

	public void setSuccessFieldId(Integer successFieldId) {
		this.successFieldId = successFieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	protected Serializable pkVal() {
		return null;
	}

}
