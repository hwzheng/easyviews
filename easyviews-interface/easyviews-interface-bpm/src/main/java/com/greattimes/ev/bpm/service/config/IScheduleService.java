package com.greattimes.ev.bpm.service.config;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.MultiScheduleDetailParam;
import com.greattimes.ev.bpm.config.param.req.MultiScheduleParam;
import com.greattimes.ev.bpm.entity.ScheduleSingle;

/**
 * <p>
 * 单次排期表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
public interface IScheduleService extends IService<ScheduleSingle> {

	/**
	 * 单次排期列表查询
	 * @param applicationId
	 * @return
	 */
	List<ScheduleSingle> selectSingleSchedule(int applicationId);

	/**
	 * 单次排期新增
	 * @param map
	 */
	void addSingleSchedule(Map<String, Object> map);

	/**
	 * 单次排期编辑
	 * @param map
	 */
	void editSingleSchedule(Map<String, Object> map);

	/**
	 * 重复排期列表查询
	 * @param applicationId
	 * @return
	 */
	List<MultiScheduleDetailParam> selectMultiSchedule(int applicationId);

	/**
	 * 重复排期保存
	 * @param param
	 */
	void saveMultiSchedule(MultiScheduleParam param);

	/**
	 * 单次排期删除
	 * @param id
	 */
	void deleteSingleSchedule(int id);

}
