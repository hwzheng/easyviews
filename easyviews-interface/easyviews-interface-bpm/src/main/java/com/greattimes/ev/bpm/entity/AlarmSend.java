package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 告警信息发送配置表
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@TableName("bpm_alarm_send")
@ApiModel
public class AlarmSend extends Model<AlarmSend> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 应用id
	 */
	@ApiModelProperty("应用id")
	private Integer applicationId;
	/**
	 * 告警组
	 */
	@ApiModelProperty("告警组id")
	private Integer alarmGroupId;
	/**
	 * 告警发送类型
	 */
	@ApiModelProperty("告警发送类型")
	private Integer sendType;
	/**
	 * 压缩条数
	 */
	@ApiModelProperty("压缩条数")
	private Integer compressNum;
	/**
	 * 压缩时间
	 */
	@ApiModelProperty("压缩时间")
	private Integer compressTime;
	/**
	 * 发送方式 1 邮件 ，2 短信 ，3app ，4外呼   (英文逗号分隔)
	 */
	@ApiModelProperty("发送方式 1 邮件 ，2 短信 ，3app ，4外呼   (英文逗号分隔)")
	private String sendWay;
	/**
	 * 是否激活 0 否 1是
	 */
	@ApiModelProperty("是否激活 0 否 1是")
	private Integer active;
	/**
	 * 告警级别
	 */
	@ApiModelProperty("告警级别")
	private Integer level;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getAlarmGroupId() {
		return alarmGroupId;
	}

	public void setAlarmGroupId(Integer alarmGroupId) {
		this.alarmGroupId = alarmGroupId;
	}

	public Integer getSendType() {
		return sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

	public Integer getCompressNum() {
		return compressNum;
	}

	public void setCompressNum(Integer compressNum) {
		this.compressNum = compressNum;
	}

	public Integer getCompressTime() {
		return compressTime;
	}

	public void setCompressTime(Integer compressTime) {
		this.compressTime = compressTime;
	}

	public String getSendWay() {
		return sendWay;
	}

	public void setSendWay(String sendWay) {
		this.sendWay = sendWay;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmSend{" + "id=" + id + ", applicationId=" + applicationId + ", alarmGroupId=" + alarmGroupId
				+ ", sendType=" + sendType + ", compressNum=" + compressNum + ", compressTime=" + compressTime
				+ ", sendWay=" + sendWay + ", active=" + active + ", level=" + level + "}";
	}
}
