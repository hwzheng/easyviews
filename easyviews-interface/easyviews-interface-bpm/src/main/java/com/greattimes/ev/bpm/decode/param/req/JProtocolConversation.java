package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 协议是识别会话
 * @author：NJ
 * @date：2018年3月19日 下午4:12:13
 */
public class JProtocolConversation implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 客户端ip
	 */
	private String clientIp;
	/**
	 * 客户端端口
	 */
	private Integer clientPort;
	/**
	 * c-s字节数
	 */
	private Integer clientSendBytesCount;
	/**
	 * c-s原始报文
	 */
	private String clientSendBytes;
	/**
	 * s-c字节数
	 */
	private Integer serverSendBytesCount;
	/**
	 * s-c 原始报文
	 */
	private String serverSendBytes;
	/**
	 * 标志位
	 */
	private String flag;
	
	
	public JProtocolConversation() {
		super();
	}
	public JProtocolConversation(String clientIp, Integer clientPort,
			Integer clientSendBytesCount, String clientSendBytes,
			Integer serverSendBytesCount, String serverSendBytes, String flag) {
		super();
		this.clientIp = clientIp;
		this.clientPort = clientPort;
		this.clientSendBytesCount = clientSendBytesCount;
		this.clientSendBytes = clientSendBytes;
		this.serverSendBytesCount = serverSendBytesCount;
		this.serverSendBytes = serverSendBytes;
		this.flag = flag;
	}
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	public Integer getClientPort() {
		return clientPort;
	}
	public void setClientPort(Integer clientPort) {
		this.clientPort = clientPort;
	}
	public Integer getClientSendBytesCount() {
		return clientSendBytesCount;
	}
	public void setClientSendBytesCount(Integer clientSendBytesCount) {
		this.clientSendBytesCount = clientSendBytesCount;
	}
	public String getClientSendBytes() {
		return clientSendBytes;
	}
	public void setClientSendBytes(String clientSendBytes) {
		this.clientSendBytes = clientSendBytes;
	}
	public Integer getServerSendBytesCount() {
		return serverSendBytesCount;
	}
	public void setServerSendBytesCount(Integer serverSendBytesCount) {
		this.serverSendBytesCount = serverSendBytesCount;
	}
	public String getServerSendBytes() {
		return serverSendBytes;
	}
	public void setServerSendBytes(String serverSendBytes) {
		this.serverSendBytes = serverSendBytes;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
}
