package com.greattimes.ev.bpm.service.config;

import com.greattimes.ev.bpm.config.param.resp.AlarmGroupParam;
import com.greattimes.ev.bpm.entity.AlarmGroup;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 告警組表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-11-30
 */
public interface IAlarmGroupService extends IService<AlarmGroup> {

	/** 校验组名
	 * @param alarmGroup
	 * @param i 1 新增
	 * @return
	 */
	boolean checkName(AlarmGroup alarmGroup, int i);

	void deleteAlarmGroup(int id);

	AlarmGroupParam selectAlarmGroup(Integer id);

	void saveGroup(AlarmGroupParam param);

}
