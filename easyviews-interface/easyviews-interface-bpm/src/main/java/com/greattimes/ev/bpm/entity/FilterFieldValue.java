package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 自定义数据过滤字段值表
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_filter_field_value")
public class FilterFieldValue extends Model<FilterFieldValue> {

    private static final long serialVersionUID = 1L;

    /**
     * 过滤字表id
     */
	private Integer filterFieldId;
    /**
     * 过滤值
     */
	private String value;


	public Integer getFilterFieldId() {
		return filterFieldId;
	}

	public void setFilterFieldId(Integer filterFieldId) {
		this.filterFieldId = filterFieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "FilterFieldValue{" +
			"filterFieldId=" + filterFieldId +
			", value=" + value +
			"}";
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}
}
