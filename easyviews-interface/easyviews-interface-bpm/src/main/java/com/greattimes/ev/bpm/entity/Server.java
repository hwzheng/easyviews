package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 组件ip/port组
 * </p>
 *
 * @author NJ
 * @since 2018-09-18
 */
@TableName("bpm_server")
public class Server extends Model<Server> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.INPUT)
	private Integer id;
    /**
     * 组件服务器组Id
     */
	private Integer serverGroupId;
    /**
     * IP
     */
	private String ip;
    /**
     * intIp
     */
	private Integer intIp;
	private Integer port;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getServerGroupId() {
		return serverGroupId;
	}

	public void setServerGroupId(Integer serverGroupId) {
		this.serverGroupId = serverGroupId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getIntIp() {
		return intIp;
	}

	public void setIntIp(Integer intIp) {
		this.intIp = intIp;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Server{" +
			"id=" + id +
			", serverGroupId=" + serverGroupId +
			", ip=" + ip +
			", intIp=" + intIp +
			", port=" + port +
			"}";
	}
}
