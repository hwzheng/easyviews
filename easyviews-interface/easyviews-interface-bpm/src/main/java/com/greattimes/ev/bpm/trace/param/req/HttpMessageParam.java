package com.greattimes.ev.bpm.trace.param.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/10/30 20:52
 */
@ApiModel
public class HttpMessageParam implements Serializable{
    private static final long serialVersionUID = 6209727727334496184L;
    @ApiModelProperty("任务id")
    private String taskId;
    @ApiModelProperty("所属中心")
    private String centerId;
    @ApiModelProperty("组件id")
    private Integer componentId;
    @ApiModelProperty("起始时间")
    private long startTime;
    @ApiModelProperty("开始时间")
    private long endTime;
    @ApiModelProperty("源id")
    private String srcIp;
    @ApiModelProperty("源端口")
    private Integer srcPort;
    @ApiModelProperty("目的ip")
    private String dstIp;
    @ApiModelProperty("目的端口")
    private Integer dstPort;
    @ApiModelProperty("字符编码")
    private String charset;
    @ApiModelProperty("异步响应session信息")
    private String responseSession;
    @ApiModelProperty("0—>有响应，1—>无响应")
    private Integer responseStatus;
    @ApiModelProperty("请求所在探针标识")
    private String reqMark;
    @ApiModelProperty("响应所在探针标识(异步使用)")
    private String resMark;
    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCenterId() {
        return centerId;
    }

    public void setCenterId(String centerId) {
        this.centerId = centerId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getSrcIp() {
        return srcIp;
    }

    public void setSrcIp(String srcIp) {
        this.srcIp = srcIp;
    }

    public Integer getSrcPort() {
        return srcPort;
    }

    public void setSrcPort(Integer srcPort) {
        this.srcPort = srcPort;
    }

    public String getDstIp() {
        return dstIp;
    }

    public void setDstIp(String dstIp) {
        this.dstIp = dstIp;
    }


    public Integer getDstPort() {
		return dstPort;
	}

	public void setDstPort(Integer dstPort) {
		this.dstPort = dstPort;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getResponseSession() {
		return responseSession;
	}

	public void setResponseSession(String responseSession) {
		this.responseSession = responseSession;
	}

	public Integer getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(Integer responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getReqMark() {
		return reqMark;
	}

	public void setReqMark(String reqMark) {
		this.reqMark = reqMark;
	}

	public String getResMark() {
		return resMark;
	}

	public void setResMark(String resMark) {
		this.resMark = resMark;
	}

	@Override
	public String toString() {
		return "HttpMessageParam [taskId=" + taskId + ", centerId=" + centerId + ", componentId=" + componentId
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", srcIp=" + srcIp + ", srcPort=" + srcPort
				+ ", dstIp=" + dstIp + ", dstPort=" + dstPort + ", charset=" + charset + ", responseSession="
				+ responseSession + ", responseStatus=" + responseStatus + ", reqMark=" + reqMark + ", resMark="
				+ resMark + "]";
	}



	
}
