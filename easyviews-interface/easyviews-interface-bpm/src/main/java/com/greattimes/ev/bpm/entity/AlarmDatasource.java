package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 告警数据源表
 * </p>
 *
 * @author NJ
 * @since 2018-08-15
 */
@TableName("bpm_alarm_datasource")
public class AlarmDatasource extends Model<AlarmDatasource> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;

    /**
     * 告警id
     */
	private Integer alarmId;
    /**
     * 数据源id
     */
	private Integer dataSourceId;
    /**
     * 指标组id
     */
	private Integer indcatorGroupId;


	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public Integer getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public Integer getIndcatorGroupId() {
		return indcatorGroupId;
	}

	public void setIndcatorGroupId(Integer indcatorGroupId) {
		this.indcatorGroupId = indcatorGroupId;
	}

	@Override
	protected Serializable pkVal() {
		return this.alarmId;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}



}
