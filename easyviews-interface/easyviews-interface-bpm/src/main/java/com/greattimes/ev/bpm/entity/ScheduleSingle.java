package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 单次排期表
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@TableName("bpm_schedule_single")
@ApiModel
public class ScheduleSingle extends Model<ScheduleSingle> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 开始时间
	 */
	@ApiModelProperty("开始时间")
	private Timestamp start;
	/**
	 * 结束时间
	 */
	@ApiModelProperty("结束时间")
	private Timestamp end;
	/**
	 * 排期类型：0 ：全部 ，1 ：告警 ，2动态基线
	 */
	@ApiModelProperty("排期类型：0 ：全部 ，1 ：告警 ，2动态基线")
	private Integer type;
	/**
	 * 描述
	 */
	@ApiModelProperty("描述")
	private String remarks;
	/**
	 * 应用id
	 */
	@ApiModelProperty("应用id")
	private Integer applicationId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getStart() {
		return start;
	}

	public void setStart(Timestamp start) {
		this.start = start;
	}

	public Timestamp getEnd() {
		return end;
	}

	public void setEnd(Timestamp end) {
		this.end = end;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ScheduleSingle{" + "id=" + id + ", start=" + start + ", end=" + end + ", type=" + type + ", remarks="
				+ remarks + ", applicationId=" + applicationId + "}";
	}
}
