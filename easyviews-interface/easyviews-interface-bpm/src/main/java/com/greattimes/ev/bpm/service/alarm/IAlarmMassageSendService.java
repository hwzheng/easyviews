package com.greattimes.ev.bpm.service.alarm;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.bpm.alarm.param.req.KafKaConfigParam;

/**
 * @author CGC
 * @date 2019年11月21日 11:28:49
 */
public interface IAlarmMassageSendService {

    /**
     * 告警推送
     * @param jsonObject
     * @return
     */
    void messageSend(JSONObject jsonObject);
    /**
     * 告警状态更改发送
     * @param config
     * @return
     */
    void updateMessageAndSend(KafKaConfigParam config);

}
