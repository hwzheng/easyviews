package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 
 * </p>
 *
 * @author NJ
 * @since 2018-09-20
 */
@TableName("alarmdimension")
public class AlarmDimensionInfo extends Model<AlarmDimensionInfo> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 时间戳
     */
	private Long time;
    /**
     * 维度id
     */
	private Integer dimensionId;
    /**
     * 维度Type
     */
	private Integer dimensionType;
    /**
     * 告警key
     */
	private String alarmKey;
    /**
     * 维度值
     */
	private String dimensionValue;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getDimensionType() {
		return dimensionType;
	}

	public void setDimensionType(Integer dimensionType) {
		this.dimensionType = dimensionType;
	}

	public String getAlarmKey() {
		return alarmKey;
	}

	public void setAlarmKey(String alarmKey) {
		this.alarmKey = alarmKey;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Alarmdimension{" +
			"id=" + id +
			", time=" + time +
			", dimensionId=" + dimensionId +
			", dimensionType=" + dimensionType +
			", alarmKey=" + alarmKey +
			", dimensionValue=" + dimensionValue +
			"}";
	}
}
