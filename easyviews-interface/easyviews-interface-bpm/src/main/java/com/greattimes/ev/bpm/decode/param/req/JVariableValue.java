package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 变量值
 * @author MS
 * @date   2018 上午9:15:31
 */
public class JVariableValue implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//当数据类型为1时，此值为变量值
	public String type_1;
	//当数据类型为2时，此值为变量值
	public Integer type_2;
	//当数据类型为3时，此值为变量值
	public Boolean type_3;
	//当数据类型为4时，此值为变量值
	public String type_4;
	//当数据类型为5时，此值为变量值
	public List<String> type_5;
	//当数据类型为6时，此值为变量值
	public List<Integer> type_6;
	//当数据类型为7时，此值为变量值
	public List<String> type_7;
	//当数据类型为8时，此值为变量值
	//public List<Map<String,Object>> type_8;
	public Map<String,Object> type_8;

	public String getType_1() {
		return type_1;
	}

	public void setType_1(String type_1) {
		this.type_1 = type_1;
	}

	public Integer getType_2() {
		return type_2;
	}

	public void setType_2(Integer type_2) {
		this.type_2 = type_2;
	}

	public Boolean getType_3() {
		return type_3;
	}

	public void setType_3(Boolean type_3) {
		this.type_3 = type_3;
	}

	public String getType_4() {
		return type_4;
	}

	public void setType_4(String type_4) {
		this.type_4 = type_4;
	}

	public List<String> getType_5() {
		return type_5;
	}

	public void setType_5(List<String> type_5) {
		this.type_5 = type_5;
	}

	public List<Integer> getType_6() {
		return type_6;
	}

	public void setType_6(List<Integer> type_6) {
		this.type_6 = type_6;
	}

	public List<String> getType_7() {
		return type_7;
	}

	public void setType_7(List<String> type_7) {
		this.type_7 = type_7;
	}

	public Map<String,Object> getType_8() {
		return type_8;
	}

	public void setType_8(Map<String,Object> type_8) {
		this.type_8 = type_8;
	}

	@Override
	public String toString() {
		return "JVariableValue [type_1=" + type_1 + ", type_2=" + type_2
				+ ", type_3=" + type_3 + ", type_4=" + type_4 + ", type_5="
				+ type_5 + ", type_6=" + type_6 + ", type_7=" + type_7
				+ ", type_8=" + type_8 + "]";
	}
}
