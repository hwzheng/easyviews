package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 智能报表图表绑定关系表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_chart_relation")
public class ReportChartRelation extends Model<ReportChartRelation> {

    private static final long serialVersionUID = 1L;
    @TableId(value="id", type= IdType.AUTO)
    private Integer id;

    /**
     * 报表id
     */
	private Integer reportId;
    /**
     * 图表id
     */
	private Integer chartId;


	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public ReportChartRelation() {
	}

	public ReportChartRelation(Integer reportId, Integer chartId) {
		this.reportId = reportId;
		this.chartId = chartId;
	}

	@Override
	public String toString() {
		return "ReportChartRelation [id=" + id + ", reportId=" + reportId + ", chartId=" + chartId + "]";
	}

}
