package com.greattimes.ev.bpm.config.param.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/6/5 11:10
 */
@ApiModel
public class AlarmFilterParam implements Serializable{

    private static final long serialVersionUID = 8642567017322591731L;

    /**
     * filterId
     */
    @ApiModelProperty("过滤条件Id")
    private Integer filterId;


    /**
     * 维度id
     */
    @ApiModelProperty("维度id")
    private Integer dimensionId;

    /**
     * 告警规则 0 like ，1 notlike
     */
    @ApiModelProperty("告警规则 0 like ，1 notlike")
    private Integer rule;

    /**
     * 告警值数组
     */
    @ApiModelProperty("告警值数组")
    private List<String> value;
    /**
     * 告警 0 默认 1 统计维度
     */
    private Integer type;

    public Integer getRule() {
        return rule;
    }

    public void setRule(Integer rule) {
        this.rule = rule;
    }

    public List<String> getValue() {
        return value;
    }

    public void setValue(List<String> value) {
        this.value = value;
    }

    public Integer getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(Integer dimensionId) {
        this.dimensionId = dimensionId;
    }

    public Integer getFilterId() {
        return filterId;
    }

    public void setFilterId(Integer filterId) {
        this.filterId = filterId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "AlarmFilterParam{" +
                "filterId=" + filterId +
                ", dimensionId=" + dimensionId +
                ", rule=" + rule +
                ", value=" + value +
                ", type=" + type +
                '}';
    }
}
