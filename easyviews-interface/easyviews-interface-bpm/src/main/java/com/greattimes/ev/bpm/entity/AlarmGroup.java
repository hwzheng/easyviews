package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 告警組表
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@TableName("bpm_alarm_group")
@ApiModel
public class AlarmGroup extends Model<AlarmGroup> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 名称
     */
	@NotEmpty(message="名称不能为空！")
	@Length(min=1, max=20,message="名称不能超过20位")
	@ApiModelProperty("名称")
	private String name;
    /**
     * 激活状态
     */
	@ApiModelProperty("激活状态")
	private Integer active;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmGroup{" +
			"id=" + id +
			", name=" + name +
			", active=" + active +
			"}";
	}
}
