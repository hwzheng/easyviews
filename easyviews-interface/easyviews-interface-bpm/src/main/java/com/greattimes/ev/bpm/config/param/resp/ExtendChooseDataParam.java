package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import io.swagger.annotations.ApiModel;

/**
 * 指标图返回结果包装类
 * 
 * @author cgc
 *
 */
@ApiModel
public class ExtendChooseDataParam implements Serializable {
	private static final long serialVersionUID = 1L;
	/**
	 * 组件id
	 */
	private Integer componentId;
	/**
	 * 关联关系（0组件 1 ip 2 port）
	 */
	private Integer type;
	/**
	 * 下拉列表
	 */
	private List<Map<String, Object>> choose;
	public Integer getComponentId() {
		return componentId;
	}
	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public List<Map<String, Object>> getChoose() {
		return choose;
	}
	public void setChoose(List<Map<String, Object>> choose) {
		this.choose = choose;
	}
	
}
