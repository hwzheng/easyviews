package com.greattimes.ev.bpm.config.param.req;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.bpm.entity.AlarmConditionMultiDimension;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/6/4 11:08
 */
@ApiModel
public class AlarmTemplateReqParam implements Serializable{

    private static final long serialVersionUID = 5842128588029794711L;

    /**
     * 模板id
     */
    @ApiModelProperty("模板id")
    private Integer tempId;

    /**
     * 模板code 类型
     */
    @ApiModelProperty("模板code 类型")
    private Integer templateId;
    /**
     * 指标id
     */
    @ApiModelProperty("指标id")
    private Integer indicatorId;
    /**
     * 阈值A 或者 topA
     */
    @ApiModelProperty("阈值A 或者 topA")
    private Double threshold;
    /**
     * 发生次数Z
     */
    @ApiModelProperty("阈值A 或者 topA")
    private Integer times;

    //--------------以下字段为多维度需要查询的字段---------------------//
    /**
     * 输出维度id
     */
    @ApiModelProperty("输出维度id")
    private List<Integer> dimensionIds;

    /**
     * id + type
     */
    @ApiModelProperty("输出维度id和类型")
    private List<JSONObject> customDimensionId;

    /**
     * 多维度条件维度
     */
    private List<AlarmConditionMultiDimension> multiDimensions;

    /**
     * 过滤条件
     */
    @ApiModelProperty("过滤条件")
    private List<AlarmFilterParam> filter;

    /**
     * 条件描述
     */
    @ApiModelProperty("条件描述")
    private String  remarks;

    /**
     * 基线上线
     */
    private Integer upperScore;
    /**
     * 基线下线
     */
    private Integer lowerScore;

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public Integer getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(Integer indicatorId) {
        this.indicatorId = indicatorId;
    }

    public Double getThreshold() {
        return threshold;
    }

    public void setThreshold(Double threshold) {
        this.threshold = threshold;
    }

    public Integer getTimes() {
        return times;
    }

    public void setTimes(Integer times) {
        this.times = times;
    }

    public List<Integer> getDimensionIds() {
        return dimensionIds;
    }

    public void setDimensionIds(List<Integer> dimensionIds) {
        this.dimensionIds = dimensionIds;
    }

    public List<AlarmFilterParam> getFilter() {
        return filter;
    }

    public void setFilter(List<AlarmFilterParam> filter) {
        this.filter = filter;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getTempId() {
        return tempId;
    }

    public void setTempId(Integer tempId) {
        this.tempId = tempId;
    }

    public List<AlarmConditionMultiDimension> getMultiDimensions() {
        return multiDimensions;
    }

    public void setMultiDimensions(List<AlarmConditionMultiDimension> multiDimensions) {
        this.multiDimensions = multiDimensions;
    }
    public List<JSONObject> getCustomDimensionId() {
        return customDimensionId;
    }
    public void setCustomDimensionId(List<JSONObject> customDimensionId) {
        this.customDimensionId = customDimensionId;
    }

    public Integer getUpperScore() {
        return upperScore;
    }

    public void setUpperScore(Integer upperScore) {
        this.upperScore = upperScore;
    }

    public Integer getLowerScore() {
        return lowerScore;
    }

    public void setLowerScore(Integer lowerScore) {
        this.lowerScore = lowerScore;
    }

    @Override
    public String toString() {
        return "AlarmTemplateReqParam{" +
                "tempId=" + tempId +
                ", templateId=" + templateId +
                ", indicatorId=" + indicatorId +
                ", threshold=" + threshold +
                ", times=" + times +
                ", dimensionIds=" + dimensionIds +
                ", customDimensionId=" + customDimensionId +
                ", multiDimensions=" + multiDimensions +
                ", filter=" + filter +
                ", remarks='" + remarks + '\'' +
                ", upperScore=" + upperScore +
                ", lowerScore=" + lowerScore +
                '}';
    }
}
