package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能瓶颈报表任务分析字段表
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@TableName("rp_bottleneck_task_analyze_field")
public class BottleneckTaskAnalyzeField extends Model<BottleneckTaskAnalyzeField> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 任务id
     */
	private Integer taskItemId;
    /**
     * 名称
     */
	private String name;
    /**
     * 字段key
     */
	private String key;
    /**
     * 字段用途 1 目标属性 2 相关属性 3 忽略
     */
	private Integer use;
    /**
     * 字段类型 1 数字 2 类别
     */
	private Integer type;
    /**
     * 字段属性 1 维度 2 指标
     */
	private Integer property;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskItemId() {
		return taskItemId;
	}

	public void setTaskItemId(Integer taskItemId) {
		this.taskItemId = taskItemId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getUse() {
		return use;
	}

	public void setUse(Integer use) {
		this.use = use;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getProperty() {
		return property;
	}

	public void setProperty(Integer property) {
		this.property = property;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public BottleneckTaskAnalyzeField(Integer taskItemId, String name, String key, Integer use, Integer type,
			Integer property) {
		super();
		this.taskItemId = taskItemId;
		this.name = name;
		this.key = key;
		this.use = use;
		this.type = type;
		this.property = property;
	}

	public BottleneckTaskAnalyzeField() {
		super();
	}

	@Override
	public String toString() {
		return "BottleneckTaskAnalyzeField{" +
			"id=" + id +
			", taskItemId=" + taskItemId +
			", name=" + name +
			", key=" + key +
			", use=" + use +
			", type=" + type +
			", property=" + property +
			"}";
	}
}
