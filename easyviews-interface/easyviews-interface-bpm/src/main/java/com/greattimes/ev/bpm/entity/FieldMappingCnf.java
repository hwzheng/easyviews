package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 维度字段映射组件表
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
@TableName("bpm_field_mapping_cnf")
@ApiModel
public class FieldMappingCnf extends Model<FieldMappingCnf> {

	private static final long serialVersionUID = 1L;

	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 更新时间
	 */
	@ApiModelProperty("更新时间")
	private Timestamp updateTime;
	/**
	 * 更新人
	 */
	@ApiModelProperty("更新人")
	private String updateUser;
	@ApiModelProperty("应用id")
	private Integer applicationId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FieldMappingCnf{" + "id=" + id + ", updateTime=" + updateTime + ", updateUser=" + updateUser
				+ ", applicationId=" + applicationId + "}";
	}
}
