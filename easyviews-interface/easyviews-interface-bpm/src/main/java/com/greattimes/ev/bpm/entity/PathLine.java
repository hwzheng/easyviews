package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 路径图连线表
 * </p>
 *
 * @author NJ
 * @since 2018-09-11
 */
@TableName("path_line")
public class PathLine extends Model<PathLine> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 视图id
     */
	private Integer viewId;
    /**
     * 开始节点
     */
	private Integer start;
    /**
     * 结束节点
     */
	private Integer end;
    /**
     * 连线类型   1  实直线箭头   2   虚直线箭头...
     */
	private Integer type;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getViewId() {
		return viewId;
	}

	public void setViewId(Integer viewId) {
		this.viewId = viewId;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "PathLine{" +
			"id=" + id +
			", viewId=" + viewId +
			", start=" + start +
			", end=" + end +
			", type=" + type +
			"}";
	}
}
