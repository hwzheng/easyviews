package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * 规则操作
 * @author MS
 * @date   2018 下午5:38:25
 */
public class JOperation implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//操作主键
	private Integer id;
	//操作类型
	private Integer type;
	//触发规则
	private JTriggerRule rule;
	//赋值操作
	private List<JAssign> assign;
	//合并操作
	private List<JMerge> merge;
	//裁剪操作
	private List<JClip> clip;
	//计算操作
	private List<JCalculate> calculate;
	//扩展解析id列表
	private List<Integer> fieldIds;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public JTriggerRule getRule() {
		return rule;
	}
	public void setRule(JTriggerRule rule) {
		this.rule = rule;
	}
	public List<JAssign> getAssign() {
		return assign;
	}
	public void setAssign(List<JAssign> assign) {
		this.assign = assign;
	}
	public List<JMerge> getMerge() {
		return merge;
	}
	public void setMerge(List<JMerge> merge) {
		this.merge = merge;
	}
	public List<JClip> getClip() {
		return clip;
	}
	public void setClip(List<JClip> clip) {
		this.clip = clip;
	}
	public List<JCalculate> getCalculate() {
		return calculate;
	}
	public void setCalculate(List<JCalculate> calculate) {
		this.calculate = calculate;
	}
	public List<Integer> getFieldIds() {
		return fieldIds;
	}
	public void setFieldIds(List<Integer> fieldIds) {
		this.fieldIds = fieldIds;
	}
	
	@Override
	public String toString() {
		return "JOperation [id=" + id + ", type=" + type + ", rule=" + rule
				+ ", assign=" + assign + ", merge=" + merge + ", clip=" + clip
				+ ", calculate=" + calculate + ", fieldIds=" + fieldIds + "]";
	}
}
