package com.greattimes.ev.bpm.datasources.param.req;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组件指标约束包装类
 * @author cgc
 * @date 2018/7/11 06:28
 */
@ApiModel
public class CompIndContraintParam implements Serializable{

    private static final long serialVersionUID = -1L;
    /**
     * 指标名称
     */
    @ApiModelProperty("指标名称")
    private String name;
    /**
     * 约束id
     */
    @ApiModelProperty("约束Id")
    private Integer fieldId;
    /**
     * 约束值
     */
    @ApiModelProperty("指标名称")
    private String value;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
