package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 告警条件模板表
 * </p>
 *
 * @author NJ
 * @since 2018-06-06
 */
@TableName("bpm_alarm_condition")
public class AlarmCondition extends Model<AlarmCondition> {


    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 模板编号 
1.大于累计模板：过去X分钟指标Y，大于，阀值A                                  
2.大于颗粒度模板：过去X分钟内每分钟指标Y，大于，阀值A，发生Z次               
3.小于累计模板：过去X分钟指标Y，小于，阀值A                                  
4.小于颗粒度模板：过去X分钟内每分钟指标Y，小于，阀值A，发生Z次               
5.递增模板：过去X分钟内每分钟指标Y，递增，阈值A%，发生Z次                    
6.递减模板：过去X分钟内每分钟指标Y，递减，阈值A%，发生Z次                    
7.基线模板：过去X分钟内每分钟指标Y异常，发生Z次                              
8.TOP正序模板：过去1分钟，按指标Y正序，数量A                                 
9.TOP倒序模板：过去1分钟，按指标Y倒叙，数量A                                 
10.分组累计模板：过去X分钟指标Y，按照维度列表B分组，大于阈值A                
11.分组颗粒度模板：过去X分钟内每分钟指标Y，按照维度B分组，大于阈值A，发生Z次 

     */
	private Integer templateCode;
    /**
     * 告警id
     */
	private Integer alarmId;
	private String remarks;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTemplateCode() {
		return templateCode;
	}

	public void setTemplateCode(Integer templateCode) {
		this.templateCode = templateCode;
	}

	public Integer getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(Integer alarmId) {
		this.alarmId = alarmId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "AlarmCondition{" +
			"id=" + id +
			", templateCode=" + templateCode +
			", alarmId=" + alarmId +
			", remarks=" + remarks +
			"}";
	}
}
