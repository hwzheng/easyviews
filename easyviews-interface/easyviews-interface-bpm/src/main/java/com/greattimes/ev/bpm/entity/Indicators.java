package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据源指标表
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_indicators")
public class Indicators extends Model<Indicators> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Integer id;
    /**
     * 组Id
     */
	private Integer groupId;
    /**
     * 指标名称
     */
	private String name;
    /**
     * 指标单位
     */
	private Integer unit;
    /**
     * 指标显示顺序
     */
	private Integer sort;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Indicators{" +
			"id=" + id +
			", groupId=" + groupId +
			", name=" + name +
			", unit=" + unit +
			", sort=" + sort +
			"}";
	}
}
