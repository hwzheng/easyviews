package com.greattimes.ev.bpm.trace.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/6/19 16:43
 */
public class IntelligentTraceParam implements Serializable{

    private static final long serialVersionUID = -1546260426929149886L;

    private Integer componentId;

    /**
     * 追踪数据
     */
    private Object originData;

    private List<ComponentTraceParam> compontent;

    public Object getOriginData() {
        return originData;
    }

    public void setOriginData(Object originData) {
        this.originData = originData;
    }

    public List<ComponentTraceParam> getCompontent() {
        return compontent;
    }

    public void setCompontent(List<ComponentTraceParam> compontent) {
        this.compontent = compontent;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    @Override
    public String toString() {
        return "IntelligentTraceParam{" +
                "componentId=" + componentId +
                ", originData=" + originData +
                ", compontent=" + compontent +
                '}';
    }
}
