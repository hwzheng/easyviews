package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 协议参数
 * @author MS
 * @date   2018 上午10:53:31
 */
public class JParams implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//tcp协议参数
	private JTcp tcp;
	//http协议参数
	private JHttp http;
	
	public JTcp getTcp() {
		return tcp;
	}
	public void setTcp(JTcp tcp) {
		this.tcp = tcp;
	}
	public JHttp getHttp() {
		return http;
	}
	public void setHttp(JHttp http) {
		this.http = http;
	}
	
	@Override
	public String toString() {
		return "JParams [tcp=" + tcp + ", http=" + http + "]";
	}
}
