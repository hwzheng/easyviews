package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 别名表
 * </p>
 *
 * @author Cgc
 * @since 2018-05-24
 */
@TableName("bpm_field_other_name")
public class FieldOtherName extends Model<FieldOtherName> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 组件id
     */
	private Integer componentId;
    /**
     * 字段id
     */
	private Integer fieldId;
    /**
     * 别名
     */
	private String othername;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

	public String getOthername() {
		return othername;
	}

	public void setOthername(String othername) {
		this.othername = othername;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FieldOtherName{" +
			"id=" + id +
			", componentId=" + componentId +
			", fieldId=" + fieldId +
			", othername=" + othername +
			"}";
	}
}
