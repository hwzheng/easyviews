package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * 解析规则
 * @author MS
 * @date   2018 下午6:21:34
 */
public class JRule implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//规则主键
	private Integer id;
	//规则名称
	private String name;
	//是否为内置规则  0：否、1：是
	private Integer isBuiltin;
	//条件匹配类型 0：全部（AND）、1：任何（OR）
	private Integer matchType;
	//触发方向 -1：任何、0：请求、1：响应
	private Integer direction;
	//触发时机 1：报文解析之前 2：标准字段解析之后 3：扩展字段解析之后
	private Integer triggerType;
	//排序索引
	private Integer index;
	//规则条件
	private List<JCondition> condition;
	//规则操作
	private List<JOperation> operation;
	//规则类型  3：合并、4：裁剪、5：计算 6：复合操作、7：解析扩展
	private Integer type;
	//是否是被动规则 0 否 1是
	private Integer isPassive;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getIsBuiltin() {
		return isBuiltin;
	}
	public void setIsBuiltin(Integer isBuiltin) {
		this.isBuiltin = isBuiltin;
	}
	public Integer getMatchType() {
		return matchType;
	}
	public void setMatchType(Integer matchType) {
		this.matchType = matchType;
	}
	public Integer getDirection() {
		return direction;
	}
	public void setDirection(Integer direction) {
		this.direction = direction;
	}
	public Integer getTriggerType() {
		return triggerType;
	}
	public void setTriggerType(Integer triggerType) {
		this.triggerType = triggerType;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public List<JCondition> getCondition() {
		return condition;
	}
	public void setCondition(List<JCondition> condition) {
		this.condition = condition;
	}
	public List<JOperation> getOperation() {
		return operation;
	}
	public void setOperation(List<JOperation> operation) {
		this.operation = operation;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public Integer getIsPassive() {
		return isPassive;
	}
	public void setIsPassive(Integer isPassive) {
		this.isPassive = isPassive;
	}
	@Override
	public String toString() {
		return "JRule [id=" + id + ", name=" + name + ", isBuiltin="
				+ isBuiltin + ", matchType=" + matchType + ", direction="
				+ direction + ", triggerType=" + triggerType + ", index="
				+ index + ", condition=" + condition + ", operation="
				+ operation + ", type=" + type + ", isPassive=" + isPassive
				+ "]";
	}
	
}
