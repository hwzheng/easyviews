package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 智能报表图表表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_chart")
public class ReportChart extends Model<ReportChart> {

	private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	/**
	 * 名称
	 */
	private String name;
	/**
	 * 创建日期
	 */
	private Date createTime;
	/**
	 * 创建人
	 */
	private Integer createBy;
	/**
	 * 更改日期
	 */
	private Date updateTime;
	private Integer type;
	private Integer interval;
	/**
	 * 开始时间
	 */
	private Date startTime;
	/**
	 * 结束时间
	 */
	private Date endTime;
	/**
	 * 时间类型 1 动态时间  2 静态时间
	 */
	private Integer timeType;
	/**
	 * 动态时间  0 今日， 1昨日   ，2上周  ，3上月   ，4过去7天，5过去14天，6过去30天，7过去180天
	 */
	private Integer dynTime;
	/**
	 * 数据类型 1 应用 2 事件 3 漏斗
	 */
	private Integer dataType;
	/**
	 * 限制条数
	 */
	private Integer limit;
	/**
	 * 排序方式 0 升序 1 降序
	 */
	private Integer sortType;
	/**
	 * 是否维度 0 否 1是
	 */
	private Integer isDimension;
	/**
	 * 动态开始时间
	 */
	private Date start;
	/**
	 * 动态结束时间
	 */
	private Date end;
	/**
	 * 对比  0 无对比 ， 1 上周期
	 */
	private Integer compare;
	/**
	 * 是否时间维度 0 否 1 是
	 */
	private Integer isTimeDimension;
	/**
	 * 自定义开始时间
	 */
	private Date customStartTime;
	/**
	 * 自定义结束时间
	 */
	private Date customEndTime;

	/**
	 * 指标排序(指标id) 仅限表格类型
	 */
	private Integer orderId;

	/**
	 * 指标排序字段
	 */

	private String orderField;
	/**
	 * 排序类型
	 */
	private Integer orderType;
	/**
	 * 数字是否显示
	 */
	private Integer dataShow;
	/**
	 * 排序方式
	 */
	private Integer alignType;
	/**
	 * 图表类目 1趋势 2、类目 3、占比 4、数值 5、表格
	 */
	private Integer chartClassify;

	/**
	 * 图表数据展示类型 1、正常 2、堆叠 3、百分比
	 */
	private Integer chartDataType;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Integer getTimeType() {
		return timeType;
	}

	public void setTimeType(Integer timeType) {
		this.timeType = timeType;
	}

	public Integer getDynTime() {
		return dynTime;
	}

	public void setDynTime(Integer dynTime) {
		this.dynTime = dynTime;
	}

	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getSortType() {
		return sortType;
	}

	public void setSortType(Integer sortType) {
		this.sortType = sortType;
	}

	public Integer getIsDimension() {
		return isDimension;
	}

	public void setIsDimension(Integer isDimension) {
		this.isDimension = isDimension;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public Integer getCompare() {
		return compare;
	}

	public void setCompare(Integer compare) {
		this.compare = compare;
	}

	public Integer getIsTimeDimension() {
		return isTimeDimension;
	}

	public void setIsTimeDimension(Integer isTimeDimension) {
		this.isTimeDimension = isTimeDimension;
	}

	public Date getCustomStartTime() {
		return customStartTime;
	}

	public void setCustomStartTime(Date customStartTime) {
		this.customStartTime = customStartTime;
	}

	public Date getCustomEndTime() {
		return customEndTime;
	}

	public void setCustomEndTime(Date customEndTime) {
		this.customEndTime = customEndTime;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getOrderField() {
		return orderField;
	}

	public void setOrderField(String orderField) {
		this.orderField = orderField;
	}

	public Integer getOrderType() {
		return orderType;
	}

	public void setOrderType(Integer orderType) {
		this.orderType = orderType;
	}
	public Integer getDataShow() {
		return dataShow;
	}

	public void setDataShow(Integer dataShow) {
		this.dataShow = dataShow;
	}

	public Integer getAlignType() {
		return alignType;
	}

	public void setAlignType(Integer alignType) {
		this.alignType = alignType;
	}

	public Integer getChartClassify() {
		return chartClassify;
	}

	public void setChartClassify(Integer chartClassify) {
		this.chartClassify = chartClassify;
	}

	public Integer getChartDataType() {
		return chartDataType;
	}

	public void setChartDataType(Integer chartDataType) {
		this.chartDataType = chartDataType;
	}

	@Override
	public String toString() {
		return "ReportChart{" +
				"id=" + id +
				", name='" + name + '\'' +
				", createTime=" + createTime +
				", createBy=" + createBy +
				", updateTime=" + updateTime +
				", type=" + type +
				", interval=" + interval +
				", startTime=" + startTime +
				", endTime=" + endTime +
				", timeType=" + timeType +
				", dynTime=" + dynTime +
				", dataType=" + dataType +
				", limit=" + limit +
				", sortType=" + sortType +
				", isDimension=" + isDimension +
				", start=" + start +
				", end=" + end +
				", compare=" + compare +
				", isTimeDimension=" + isTimeDimension +
				", customStartTime=" + customStartTime +
				", customEndTime=" + customEndTime +
				", orderId=" + orderId +
				", orderField='" + orderField + '\'' +
				", orderType=" + orderType +
				", dataShow=" + dataShow +
				", alignType=" + alignType +
				", chartClassify=" + chartClassify +
				", chartDataType=" + chartDataType +
				'}';
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
