package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/11/27 14:39
 */
public class TrackParam implements Serializable{

    private static final long serialVersionUID = 266744929250580287L;

    /**
     * 字段key
     */
    private  String key;
    /**
     * 字段名
     */
    private  String label;

    /**
     * 后追踪点
     */
    private  Integer value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "TrackParam{" +
                "key='" + key + '\'' +
                ", label='" + label + '\'' +
                ", value=" + value +
                '}';
    }
}
