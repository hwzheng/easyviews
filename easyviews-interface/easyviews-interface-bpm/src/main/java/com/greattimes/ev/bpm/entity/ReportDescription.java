package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 智能报表描述表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_description")
public class ReportDescription extends Model<ReportDescription> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 名称
     */
	private String name;
    /**
     * 描述详情
     */
	private String detail;
    /**
     * 报表id
     */
	private Integer reportId;
	/**
     *是否是图
     */
	@TableField(exist=false)
	private boolean isChart;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Integer getReportId() {
		return reportId;
	}

	public void setReportId(Integer reportId) {
		this.reportId = reportId;
	}

	public boolean getIsChart() {
		return isChart;
	}

	public void setIsChart(boolean isChart) {
		this.isChart = isChart;
	}

	public ReportDescription(String name, String detail, Integer reportId) {
		super();
		this.name = name;
		this.detail = detail;
		this.reportId = reportId;
	}

	public ReportDescription() {
		super();
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ReportDescription{" +
			"id=" + id +
			", name=" + name +
			", detail=" + detail +
			", reportId=" + reportId +
			"}";
	}
}
