package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;
import java.util.List;

/**
 * 合并操作
 * @author MS
 * @date   2018 下午5:35:13
 */
public class JMerge implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//输入
	private List<JMergeIn> in;
	//输出
	private JMergeOut out;
	//索引
	private Integer index;
	
	public List<JMergeIn> getIn() {
		return in;
	}
	public void setIn(List<JMergeIn> in) {
		this.in = in;
	}
	public JMergeOut getOut() {
		return out;
	}
	public void setOut(JMergeOut out) {
		this.out = out;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	@Override
	public String toString() {
		return "JMerge [in=" + in + ", out=" + out + ", index=" + index + "]";
	}
	
}
