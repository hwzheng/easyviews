package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 指标组
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_indicator_group")
public class IndicatorGroup extends Model<IndicatorGroup> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 组名称
     */
	private String name;
    /**
     * 约束层级
     */
	private Integer level;
    /**
     * 数据颗粒度
     */
	private Integer interval;
    /**
     * 单位
     */
	private Integer unit;
    /**
     * 存储模式
     */
	private Integer storageMode;
    /**
     * 数据源id
     */
	private Integer sourcesId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getInterval() {
		return interval;
	}

	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	public Integer getUnit() {
		return unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	public Integer getStorageMode() {
		return storageMode;
	}

	public void setStorageMode(Integer storageMode) {
		this.storageMode = storageMode;
	}

	public Integer getSourcesId() {
		return sourcesId;
	}

	public void setSourcesId(Integer sourcesId) {
		this.sourcesId = sourcesId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "IndicatorGroup{" +
			"id=" + id +
			", name=" + name +
			", level=" + level +
			", interval=" + interval +
			", unit=" + unit +
			", storageMode=" + storageMode +
			", sourcesId=" + sourcesId +
			"}";
	}
}
