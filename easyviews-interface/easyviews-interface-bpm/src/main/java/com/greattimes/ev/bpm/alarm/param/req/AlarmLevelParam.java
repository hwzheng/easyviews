package com.greattimes.ev.bpm.alarm.param.req;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/1/7 19:36
 */
public class AlarmLevelParam implements Serializable{
    private static final long serialVersionUID = -904924267986698846L;

    private Integer id;
    /**
     * 告警组id
     */
    private Integer alarmGroupId;
    /**
     * 告警级别
     */
    private Integer level;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getAlarmGroupId() {
        return alarmGroupId;
    }

    public void setAlarmGroupId(Integer alarmGroupId) {
        this.alarmGroupId = alarmGroupId;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "AlarmLevelParam{" +
                "id=" + id +
                ", alarmGroupId=" + alarmGroupId +
                ", level=" + level +
                '}';
    }
}
