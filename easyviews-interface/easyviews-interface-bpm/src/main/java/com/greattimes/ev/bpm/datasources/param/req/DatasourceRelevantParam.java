package com.greattimes.ev.bpm.datasources.param.req;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 数据源关联表包装类
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
@ApiModel
public class DatasourceRelevantParam extends Model<DatasourceRelevantParam> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@ApiModelProperty("id")
	private Integer id;
    /**
     * 组件id
     */
	@NotNull(message="组件id不能为空！")
	@ApiModelProperty("组件id")
	private Integer componentId;
	/**
     * 组件name
     */
	@ApiModelProperty("组件名")
	private String componentName;
    /**
     * 数据源id
     */
	@NotNull(message="数据源id不能为空！")
	@ApiModelProperty("数据源id")
	private Integer dataSourceId;
	/**
     * 数据源name
     */
	@ApiModelProperty("数据源名")
	private String dataSourceName;
    /**
     * 指标组
     */
	@NotNull(message="指标组不能为空！")
	@ApiModelProperty("指标组")
	private Integer indicatorGroupId;
	/**
     * 指标组名
     */
	@ApiModelProperty("指标组名")
	private String indicatorGroupName;
    /**
     * 创建人
     */
	@ApiModelProperty("创建人")
	private String createBy;
    /**
     * 创建时间
     */
	@ApiModelProperty("创建时间")
	private Date createTime;
    /**
     * 修改人
     */
	@ApiModelProperty("修改人")
	private String updateBy;
    /**
     * 修改时间
     */
	@ApiModelProperty("修改时间")
	private Date updateTime;
    /**
     * 显示维度： 0否 1 是
     */
	@NotNull(message="显示维度不能为空！")
	@ApiModelProperty("显示维度： 0否 1 是")
	private Integer showDimension;
    /**
     * 关联层级   1 ip 2 port
     */
	@ApiModelProperty("关联层级 1 ip 2 port")
	private Integer relevantField;
	/**
     * 挂载点 0组件 1 ip 2 port
     */
	@NotNull(message="挂载点不能为空！")
	@ApiModelProperty("挂载点  0组件 1 ip 2 port")
	private Integer mount;
	/**
     * 约束id
     */
	@ApiModelProperty("约束id")
	private List<Integer> field;
	/**
     * 约束名
     */
	@ApiModelProperty("约束id")
	private List<String> fieldName;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
	}
	
	public String getComponentName() {
		return componentName;
	}

	public void setComponentName(String componentName) {
		this.componentName = componentName;
	}

	public String getDataSourceName() {
		return dataSourceName;
	}

	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public String getIndicatorGroupName() {
		return indicatorGroupName;
	}

	public void setIndicatorGroupName(String indicatorGroupName) {
		this.indicatorGroupName = indicatorGroupName;
	}

	public Integer getIndicatorGroupId() {
		return indicatorGroupId;
	}

	public void setIndicatorGroupId(Integer indicatorGroupId) {
		this.indicatorGroupId = indicatorGroupId;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getShowDimension() {
		return showDimension;
	}

	public void setShowDimension(Integer showDimension) {
		this.showDimension = showDimension;
	}


	public Integer getRelevantField() {
		return relevantField;
	}

	public void setRelevantField(Integer relevantField) {
		this.relevantField = relevantField;
	}

	public Integer getMount() {
		return mount;
	}

	public void setMount(Integer mount) {
		this.mount = mount;
	}


	public List<Integer> getField() {
		return field;
	}

	public void setField(List<Integer> field) {
		this.field = field;
	}

	public List<String> getFieldName() {
		return fieldName;
	}

	public void setFieldName(List<String> fieldName) {
		this.fieldName = fieldName;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DatasourceRelevant{" +
			"id=" + id +
			", componentId=" + componentId +
			", dataSourceId=" + dataSourceId +
			", indicatorGroupId=" + indicatorGroupId +
			", createBy=" + createBy +
			", createTime=" + createTime +
			", updateBy=" + updateBy +
			", updateTime=" + updateTime +
			", showDimension=" + showDimension +
			", relevantField=" + relevantField +
			"}";
	}
}
