package com.greattimes.ev.bpm.service.alarm;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.bpm.alarm.param.req.AlarmInfoParam;
import com.greattimes.ev.bpm.entity.AlarmInfo;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/20 19:37
 */
public interface IAlarmService {
    /**

     * 11.1应用告警查询(根据应用id查询)
     * @author NJ
     * @date 2018/9/21 10:11
     * @param applicationIds
     * @param start
     * @param end
     * @param interval
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    Map<String, Object> selectApplicationAlarm(List<Integer> applicationIds, Long start, Long end,int interval);


    /**
     * 其它告警数量的查询(多种类型告警)
     * @author NJ
     * @date 2018/9/20 20:50
     * @param map
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    Map<String, Object> selectOtherAlarmAmount(Map<String, Object> map);

    /**
     * 其它告警数量的查询(单值告警)
     * @author NJ
     * @date 2018/10/10 12:11
     * @param map
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<Map<String, Object>> selectOtherSingleAlarmAmount(Map<String, Object> map);


    /**
     * 批量更新意见
     * @author NJ
     * @date 2018/10/22 16:53
     * @param ids
     * @param manageState
     * @param managerdsec
     * @return void
     */
    void updateAlarmInfoOpinion(List<Integer> ids,  int manageState, String  managerdsec);

    /**    
     * 11.3告警处理意见详情
     * @author NJ  
     * @date 2018/9/20 20:01  
     * @param id  
     * @return com.greattimes.ev.bpm.entity.AlarmInfo  
     */  
    AlarmInfo selectAlarmInfoById(int id);


    /**
     * 告警列表查询
     * @author NJ
     * @date 2018/9/20 20:59
     * @param page
     * @param map
     * @return com.baomidou.mybatisplus.plugins.Page<com.greattimes.ev.bpm.entity.AlarmInfo>
     */
    @Deprecated
    Page<AlarmInfo> selectPageByIdAndType(Page<AlarmInfo> page, Map<String, Object> map);


    /**
     * 告警浏览器条件查询
     * @author NJ
     * @date 2018/10/19 10:54
     * @param info
     * @return java.util.List<com.greattimes.ev.bpm.entity.AlarmInfo>
     */
    List<AlarmInfo> selectAlarmBrowserByParams(AlarmInfoParam info);

    /**
     * 告警浏览器条件查询分页
     * @author NJ
     * @date 2018/10/19 10:54
     * @param page
     * @param info
     * @return java.util.List<com.greattimes.ev.bpm.entity.AlarmInfo>
     */
    Page<AlarmInfo> selectAlarmBrowserPageByParams(Page<AlarmInfo> page, AlarmInfoParam info);

    /**
     * 根据id查询告警描述和阈值告警
    * @author NJ
     * @date 2018/10/19 11:34
     * @param id
     * @return com.greattimes.ev.bpm.entity.AlarmInfo
     */
    List<AlarmInfo> selectAlarmInfoAndDimensionById(int id);


    /**
     * 查询组其它模板的告警
     * @author NJ
     * @date 2018/12/11 19:46
     * @param map
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<Map<String, Object>> selectAbnormalinfo(Map<String, Object> map);


    /**
     * 根据id查询告警
     * @author NJ
     * @date 2018/12/18 17:25
     * @param map
     * @return
     */
    List<AlarmInfo> selectAlarmInfoByIds(Map<String, Object> map);


    /**
     * 根据不同的颗粒度统计单组件和单事件的告警数量
     * @author NJ
     * @date 2019/6/18 10:49
     * @param map
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<Map<String, Object>> selectAlarmNumByInterval(Map<String, Object> map);

}
