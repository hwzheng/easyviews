package com.greattimes.ev.bpm.config.param.resp;


import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/8/8 10:28
 */
public class MonitorParam implements Serializable{

    private static final long serialVersionUID = 1173133971155762787L;
    /**
     * 主键
     */
    private Integer id;
    /**
     * 监控名称
     */
    private String name;
    /**
     * 组件数量
     */
    private Integer componentNum;
    /**
     * 是否激活
     */
    private Integer active;
    /**
     * 所属中心
     */
    private String centerNames;

    /**
     * 排序
     */
    private Integer sort;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getComponentNum() {
        return componentNum;
    }

    public void setComponentNum(Integer componentNum) {
        this.componentNum = componentNum;
    }

    public String getCenterNames() {
        return centerNames;
    }

    public void setCenterNames(String centerNames) {
        this.centerNames = centerNames;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }
}
