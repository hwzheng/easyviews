package com.greattimes.ev.bpm.service.analysis;

import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import com.greattimes.ev.bpm.entity.PathArea;
import com.greattimes.ev.bpm.entity.PathLine;
import com.greattimes.ev.bpm.entity.PathNode;

import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/9/11 19:41
 */
public interface IPathViewConfigService {

    List<ApplicationTree> findApplicationTree(Integer userId, List<Integer> applicationIds);

    Map<String, Object> findPathViewDetail(int applicationId);

    boolean savePathViewConfig(Map<String, Object> map);

    /**
     * 根据组件路径图相关表数据(path_node,path_line) 不考虑区域的删除
     * 节点类型 1 应用 2 监控点 3 组件 4 子节点
     * @author NJ
     * @date 2018/12/13 20:01
     * @param ids
     * @return boolean
     */
    boolean deletePathViewByComponentIds(List<Integer> ids);

    /**
     * 根据监控点删除路径图相关表数据(path_node,path_line) 不考虑区域的删除
     * 不级联往下删除
     * @author NJ
     * @date 2018/12/13 20:40
     * @param ids
     * @return boolean
     */
    boolean deletePathViewByMonitorIds(List<Integer> ids);

}
