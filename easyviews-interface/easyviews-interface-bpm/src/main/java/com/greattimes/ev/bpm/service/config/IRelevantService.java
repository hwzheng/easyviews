package com.greattimes.ev.bpm.service.config;


import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.entity.DatasourceRelevant;

/**
 * <p>
 * 数据源关联表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-08-15
 */
public interface IRelevantService extends IService<DatasourceRelevant> {

	/**
	 * 外部数据配置删除
	 * @param id
	 */
	void delete(int componentId);

}
