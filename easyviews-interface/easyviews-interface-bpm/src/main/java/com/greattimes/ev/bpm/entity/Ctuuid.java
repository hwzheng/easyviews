package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 自定义约束UUID表
 * </p>
 *
 * @author cgc
 * @since 2018-07-09
 */
@TableName("ct_uuid")
public class Ctuuid extends Model<Ctuuid> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.INPUT)
	private Long id;
    /**
     * 约束组id
     */
	private Integer contraintGroupId;
    /**
     * 约束值 例：applicationid:1 customid:1,dimension:recode,value:0
     */
	private String value;
    /**
     * 状态 1有效
     */
	private Integer active;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getContraintGroupId() {
		return contraintGroupId;
	}

	public void setContraintGroupId(Integer contraintGroupId) {
		this.contraintGroupId = contraintGroupId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Uuid{" +
			"id=" + id +
			", contraintGroupId=" + contraintGroupId +
			", value=" + value +
			", active=" + active +
			"}";
	}
}
