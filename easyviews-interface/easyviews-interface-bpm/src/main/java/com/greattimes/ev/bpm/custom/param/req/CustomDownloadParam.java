package com.greattimes.ev.bpm.custom.param.req;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义配置统计维度下载请求包装类
 * </p>
 *
 * @author cgc
 * @since 2018-05-23
 */
@ApiModel
public class CustomDownloadParam implements Serializable {
	/**
	 */
	private static final long serialVersionUID = 183187294958067349L;
	/**
	 * 统计维度表id
	 */
	@ApiModelProperty("统计维度表id")
	private Integer id;
	/**
	 * 数据类型 : 0 普通 1 省份 2 地市 3 地区
	 */
	@ApiModelProperty("数据类型 : 0 普通 1 省份 2 地市 3 地区")
	private Integer type;
	/**
	 * 地市id  -1 表示全部
	 */
	@ApiModelProperty("地市id  -1 表示全部")
	private Integer cityId;
	/**
	 * 省id -1 表示全部
	 */
	@ApiModelProperty("省id -1 表示全部")
	private Integer provinceId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

}
