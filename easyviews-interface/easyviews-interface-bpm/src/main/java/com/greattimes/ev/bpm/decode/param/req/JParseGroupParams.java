package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 解析分组参数
 * @author MS
 * @date   2018 上午10:00:54
 */
public class JParseGroupParams implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//通用分组参数
	private JCommon common;
	//http分组参数
	private JPraseGroupParamsHttp http;
	
	public JCommon getCommon() {
		return common;
	}
	public void setCommon(JCommon common) {
		this.common = common;
	}
	public JPraseGroupParamsHttp getHttp() {
		return http;
	}
	public void setHttp(JPraseGroupParamsHttp http) {
		this.http = http;
	}
	@Override
	public String toString() {
		return "JParseGroupParams [common=" + common + ", http=" + http + "]";
	}
}
