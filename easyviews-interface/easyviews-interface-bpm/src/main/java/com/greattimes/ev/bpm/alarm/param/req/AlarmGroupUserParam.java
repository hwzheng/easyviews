package com.greattimes.ev.bpm.alarm.param.req;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/1/7 19:46
 */
public class AlarmGroupUserParam  implements Serializable{
    private static final long serialVersionUID = 7919911793077941149L;

    private Integer id;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 告警组id
     */
    private Integer alarmGroupId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getAlarmGroupId() {
        return alarmGroupId;
    }

    public void setAlarmGroupId(Integer alarmGroupId) {
        this.alarmGroupId = alarmGroupId;
    }

    @Override
    public String toString() {
        return "AlarmGroupUserParam{" +
                "id=" + id +
                ", userId=" + userId +
                ", alarmGroupId=" + alarmGroupId +
                '}';
    }
}
