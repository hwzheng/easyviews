package com.greattimes.ev.bpm.trace.param.req;

import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2018/10/24 14:50
 */
@ApiModel
public class SingleTraceParam implements Serializable{
    private static final long serialVersionUID = 427672911987342077L;
    @ApiModelProperty("应用id")
    private Integer applicationId;
    @ApiModelProperty("组件")
    private Integer componentId;
    @ApiModelProperty("时间戳")
    private Long time;
    @ApiModelProperty("左查询字段值")
    private String leftValue;
    @ApiModelProperty("右查询字段值")
    private String rightValue;
    /**
     *   componentId	Int	是	组件id
     *   leftKey	string	是	左查询字段key
     *   rightKey	string	是	右查询字段key
     */
    @ApiModelProperty("追踪链条")
    private List<JSONObject> traceChains;

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public Integer getComponentId() {
        return componentId;
    }

    public void setComponentId(Integer componentId) {
        this.componentId = componentId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public String getLeftValue() {
        return leftValue;
    }

    public void setLeftValue(String leftValue) {
        this.leftValue = leftValue;
    }

    public String getRightValue() {
        return rightValue;
    }

    public void setRightValue(String rightValue) {
        this.rightValue = rightValue;
    }

    public List<JSONObject> getTraceChains() {
        return traceChains;
    }

    public void setTraceChains(List<JSONObject> traceChains) {
        this.traceChains = traceChains;
    }

    @Override
    public String toString() {
        return "SingleTraceParam{" +
                "applicationId=" + applicationId +
                ", componentId=" + componentId +
                ", time=" + time +
                ", leftValue='" + leftValue + '\'' +
                ", rightValue='" + rightValue + '\'' +
                ", traceChains=" + traceChains +
                '}';
    }
}
