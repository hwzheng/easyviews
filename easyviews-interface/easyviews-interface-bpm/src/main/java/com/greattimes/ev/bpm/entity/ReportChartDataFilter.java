package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能报表数据维度过滤表
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
@TableName("ai_report_chart_data_filter")
public class ReportChartDataFilter extends Model<ReportChartDataFilter> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 维度
     */
	private String dimensionValue;
    /**
     * 维度值
     */
	private Integer dimensionId;
    /**
     * 规则
     */
	private Integer rule;
    /**
     * 图表id
     */
	private Integer chartId;

	/**
	 * 维度列名
	 */
	private String ename;

	/**
	 * 维度类型: 0分析维度 1统计维度
	 */
	private Integer type;

	public ReportChartDataFilter() {
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public ReportChartDataFilter(Integer id, String dimensionValue, Integer dimensionId, Integer rule, Integer chartId) {
		this.id = id;
		this.dimensionValue = dimensionValue;
		this.dimensionId = dimensionId;
		this.rule = rule;
		this.chartId = chartId;
	}

	public ReportChartDataFilter(String dimensionValue, Integer dimensionId, Integer rule, Integer chartId) {
		this.dimensionValue = dimensionValue;
		this.dimensionId = dimensionId;
		this.rule = rule;
		this.chartId = chartId;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDimensionValue() {
		return dimensionValue;
	}

	public void setDimensionValue(String dimensionValue) {
		this.dimensionValue = dimensionValue;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getRule() {
		return rule;
	}

	public void setRule(Integer rule) {
		this.rule = rule;
	}

	public Integer getChartId() {
		return chartId;
	}

	public void setChartId(Integer chartId) {
		this.chartId = chartId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ReportChartDataFilter{" +
				"id=" + id +
				", dimensionValue='" + dimensionValue + '\'' +
				", dimensionId=" + dimensionId +
				", rule=" + rule +
				", chartId=" + chartId +
				", ename='" + ename + '\'' +
				", type=" + type +
				'}';
	}
}
