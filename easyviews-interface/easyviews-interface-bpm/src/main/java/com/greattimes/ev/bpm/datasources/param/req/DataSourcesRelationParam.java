package com.greattimes.ev.bpm.datasources.param.req;

import java.io.Serializable;

/**
 *
 * @author cgc
 * @since 2018-08-17
 */
public class DataSourcesRelationParam implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * IP：port
	 */
	private String value;
	/**
	 * 端口
	 */
	private Integer port;
	/**
	 * IP
	 */
	private String ip;
	/**
	 * 指标约束id
	 */
	private Integer indicatorContraintId;
	/**
	 * 关联约束id
	 */
	private Integer relevantContraintId;
	/**
	 * intIp
	 */
	private Integer intIp;
	/**
	 * outerUuid
	 */
	private String outerUuid;
	/**
	 * innerUuid
	 */
	private Integer innerUuid;
	/**
	 * indicatorId
	 */
	private Integer indicatorId;

	public Integer getInnerUuid() {
		return innerUuid;
	}

	public void setInnerUuid(Integer innerUuid) {
		this.innerUuid = innerUuid;
	}

	public String getOuterUuid() {
		return outerUuid;
	}

	public void setOuterUuid(String outerUuid) {
		this.outerUuid = outerUuid;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getIntIp() {
		return intIp;
	}

	public void setIntIp(Integer intIp) {
		this.intIp = intIp;
	}

	public Integer getIndicatorContraintId() {
		return indicatorContraintId;
	}

	public void setIndicatorContraintId(Integer indicatorContraintId) {
		this.indicatorContraintId = indicatorContraintId;
	}

	public Integer getRelevantContraintId() {
		return relevantContraintId;
	}

	public void setRelevantContraintId(Integer relevantContraintId) {
		this.relevantContraintId = relevantContraintId;
	}

	@Override
	public String toString() {
		return "DataSourcesIpParam [value=" + value + ", port=" + port + ", ip=" + ip + ", intIp=" + intIp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((intIp == null) ? 0 : intIp.hashCode());
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + ((port == null) ? 0 : port.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataSourcesRelationParam other = (DataSourcesRelationParam) obj;
		if (intIp == null) {
			if (other.intIp != null)
				return false;
		} else if (!intIp.equals(other.intIp))
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (port == null) {
			if (other.port != null)
				return false;
		} else if (!port.equals(other.port))
			return false;
		return true;
	}

}
