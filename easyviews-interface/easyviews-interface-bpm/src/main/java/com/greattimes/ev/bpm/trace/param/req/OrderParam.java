package com.greattimes.ev.bpm.trace.param.req;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/7/1 9:41
 */
public class OrderParam implements Serializable{

    private static final long serialVersionUID = 6647469855897251431L;

    /**
     * 排序规则1 升序 2降序
     */
    private Integer rule;
    /**
     * 排序字段
     */
    private String dimension;


    public Integer getRule() {
        return rule;
    }

    public void setRule(Integer rule) {
        this.rule = rule;
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    @Override
    public String toString() {
        return "OrderParam{" +
                "rule=" + rule +
                ", dimension='" + dimension + '\'' +
                '}';
    }
}
