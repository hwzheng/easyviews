package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Objects;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 自定义分析维度
 * </p>
 *
 * @author cgc
 * @since 2018-06-26
 */
@TableName("ct_analyze_dimension")
@ApiModel
public class AnalyzeDimension extends Model<AnalyzeDimension> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @ApiModelProperty("id")
	private Integer id;
    /**
     * 分析维度id
     */
    @ApiModelProperty("分析维度id")
	private Integer dimensionId;
    /**
     * 自定义分析Id
     */
    @ApiModelProperty("自定义分析Id")
	private Integer customId;
    /**
     * 排序
     */
    @ApiModelProperty("排序")
	private Integer sort;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getCustomId() {
		return customId;
	}

	public void setCustomId(Integer customId) {
		this.customId = customId;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}


	@Override
	public String toString() {
		return "AnalyzeDimension{" +
				"id=" + id +
				", dimensionId=" + dimensionId +
				", customId=" + customId +
				", sort=" + sort +
				'}';
	}
}
