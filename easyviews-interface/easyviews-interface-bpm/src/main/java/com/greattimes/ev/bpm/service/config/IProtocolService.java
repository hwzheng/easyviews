package com.greattimes.ev.bpm.service.config;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.decode.param.req.JProtocolDetail;
import com.greattimes.ev.bpm.entity.Dimension;
import com.greattimes.ev.bpm.entity.Protocol;

/**
 * <p>
 * 协议表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-05-30
 */
public interface IProtocolService extends IService<Protocol> {

	/**
	 * 协议信息查询
	 * @return
	 */
	List<Protocol> selectProtocol();

	/**
	 * 协议删除
	 * @param map
	 */
	void deleteProtocol(Map<String, Object> map);

	/**
	 * 协议输出字段查询
	 * @param id
	 * @param type 查询类型 1 协议 2 组件 3 自定义id
	 * @param flag 
	 * @return
	 */
	List<Map<String, Object>> selectProtocolField(int id, int type, int flag);


	/**
	 * 分页查询自定义协议列表--解码模块
	 * @author NJ
	 * @date 2018/8/21 15:01
	 * @param page
	 * @return com.baomidou.mybatisplus.plugins.Page<com.greattimes.ev.bpm.entity.Protocol>
	 */
	Page<Protocol> queryCustomProtocol(Page<Protocol> page,String name);


    /**
     * 更新协议的状态并通知大数据
     * @author NJ
     * @date 2018/8/21 15:35
     * @param protocol
     * @param url
     * @return String
     */
    String updateProtocolState(Protocol protocol, String url);


	/**
	 * 查询协议报文
	 * @author NJ
	 * @date 2018/8/22 18:03
	 * @param protocolId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> queryProtocolMessage(Integer protocolId);

	/**
	 * 查询分组协议报文
	 * @author NJ
	 * @date 2018/8/22 18:12
	 * @param protocolId
	 * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
	 */
	List<Map<String, Object>> queryProtocolPacketMessage(int protocolId);

	/**
	 * 保存协议
	 * @author NJ
	 * @date 2018/8/22 16:22
	 * @param url
	 * @param username
	 * @param jProtocolDetail
	 * @param flag 1 新增  
	 * @return java.lang.String
	 */
	String saveOrUpdateProtocolDetail(String url,String username,JProtocolDetail jProtocolDetail, int flag) throws Exception;

	/**
	 * 查询应用协议
	 * @author CGC
	 * @param userId
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectAppProtocol(Integer userId, Map<String, Object> map);

	/**
	 * 保存未识别协议
	 * @param name
	 * @param str
	 * @return
	 */
	boolean addJson(String name, String str);


	/**
	 * 校验协议名
	 * @param name
	 * @param flag 1 添加
	 * @return
	 */
	Boolean checkProtocolName(int id, String name, int flag);
	
	/**
	 * 校验协议key
	 * @param name
	 * @param flag 1 添加
	 * @return
	 */
	Boolean checkProtocolKey(int id, String key, int flag);

	/**
	 * 删除组件级联删除应用协议相关的四张表(物理删除)
	 * @author NJ
	 * @date 2018/9/7 11:12
	 * @param ids
	 * @return java.lang.boolean
	 */
	boolean deleteProtocolPhysical(List<Integer> ids);

	/**上传json更新协议内容
	 * @param name
	 * @param str
	 * @param protocolId
	 * @return
	 */
	Boolean updateByUploadJson(String name, String str, int protocolId);

	/**
	 * 协议可用维度字段查询
	 * @param protocolId
	 * @return
	 */
	List<Dimension> selectProtocolDimension(Integer protocolId);

	/**
	 * 保存协议维度
	 * @author NJ
	 * @date 2019/2/15 16:57
	 * @param dimension
	 * @return boolean
	 */
	boolean saveOrUpdateProDimension(Dimension dimension);

	/**
	 * 查询协议维度
	 * @author NJ
	 * @date 2019/2/15 16:57
	 * @return java.util.List<com.greattimes.ev.bpm.entity.Dimension>
	 */
	List<Dimension> selectProDimension();
	/**
	 * 根据id查询协议维度
	 * @author NJ
	 * @date 2019/2/15 16:57
	 * @param id
	 * @return com.greattimes.ev.bpm.entity.Dimension
	 */
	Dimension selectProDimensionById(int id);

	/**
	 * 根据协议的维度名称查询维度
	 * @author NJ
	 * @date 2019/2/18 10:24
	 * @param name
	 * @return java.util.List<com.greattimes.ev.bpm.entity.Dimension>
	 */
	List<Dimension> selectProDimensionByName(String name);

	/**
	 * 上传日志协议
	 * @param name
	 * @param str
	 * @return
	 */
	boolean addLogsJson(String name, String str);

	/**
	 * 上传json更新日志协议
	 * @param name
	 * @param str
	 * @param protocolId
	 * @return
	 */
	Boolean updateLogsByUploadJson(String name, String str, int protocolId);


}
