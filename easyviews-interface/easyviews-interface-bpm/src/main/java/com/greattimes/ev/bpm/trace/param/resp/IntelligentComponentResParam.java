package com.greattimes.ev.bpm.trace.param.resp;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2019/7/3 10:57
 */
public class IntelligentComponentResParam  implements Serializable{

    private static final long serialVersionUID = -6371193615231491997L;

    private IntelligentResParam component;

    public IntelligentResParam getComponent() {
        return component;
    }

    public void setComponent(IntelligentResParam component) {
        this.component = component;
    }

    @Override
    public String toString() {
        return "IntelligentComponentResParam{" +
                "component=" + component +
                '}';
    }
}
