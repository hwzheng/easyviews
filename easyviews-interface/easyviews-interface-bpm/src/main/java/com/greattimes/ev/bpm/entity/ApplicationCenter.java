package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 应用中心表
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
@TableName("bpm_application_center")
public class ApplicationCenter extends Model<ApplicationCenter> {

    private static final long serialVersionUID = 1L;

    /**
     * 应用id
     */
	private Integer applicationId;
    /**
     * 中心id
     */
	private Integer centerId;

	public ApplicationCenter(Integer applicationId, Integer centerId) {
		this.applicationId = applicationId;
		this.centerId = centerId;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getCenterId() {
		return centerId;
	}

	public void setCenterId(Integer centerId) {
		this.centerId = centerId;
	}

	@Override
	protected Serializable pkVal() {
		return this.applicationId;
	}

	@Override
	public String toString() {
		return "ApplicationCenter{" +
			"applicationId=" + applicationId +
			", centerId=" + centerId +
			"}";
	}
}
