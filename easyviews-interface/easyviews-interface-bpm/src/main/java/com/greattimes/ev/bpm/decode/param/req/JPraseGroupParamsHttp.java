package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * http 分组参数
 * @author MS
 * @date   2018 上午9:40:35
 */
public class JPraseGroupParamsHttp implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//是否解析 Cookie 参数
	private Boolean parseCookies;
	//是否解析 GET 请求中 URI 携带的参数
	private Boolean parseGetUriParams;
	
	public Boolean getParseCookies() {
		return parseCookies;
	}
	public void setParseCookies(Boolean parseCookies) {
		this.parseCookies = parseCookies;
	}
	public Boolean getParseGetUriParams() {
		return parseGetUriParams;
	}
	public void setParseGetUriParams(Boolean parseGetUriParams) {
		this.parseGetUriParams = parseGetUriParams;
	}
	
	@Override
	public String toString() {
		return "JHttp [parseCookies=" + parseCookies + ", parseGetUriParams="
				+ parseGetUriParams + "]";
	}
	
	
}
