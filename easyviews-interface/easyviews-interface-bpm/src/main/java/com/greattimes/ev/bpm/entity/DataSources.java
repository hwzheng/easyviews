package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 数据源配置表
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_data_sources")
@ApiModel
public class DataSources extends Model<DataSources> {

    private static final long serialVersionUID = 1L;
	@ApiModelProperty("主键")
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 数据源名称
     */
	@ApiModelProperty("数据源名称")
	private String name;
    /**
     * 数据源类型
     */
	@ApiModelProperty("数据源类型")
	private Integer type;
    /**
     * 同步状态
     */
	@ApiModelProperty("同步状态")
	private Integer syncType;
    /**
     * 激活状态
     */
	@ApiModelProperty("激活状态")
	private Integer active;
    /**
     * 更新人
     */
	@ApiModelProperty("更新人")
	private Integer updateBy;
    /**
     * 更新时间
     */
	@ApiModelProperty("更新时间")
	private Date updateTime;
    /**
     * 创建人
     */
	@ApiModelProperty("创建人")
	private Integer createBy;
    /**
     * 创建时间
     */
	@ApiModelProperty("创建时间")
	private Date createTime;
    /**
     * 描述
     */
	@ApiModelProperty("描述")
	private String desc;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getSyncType() {
		return syncType;
	}

	public void setSyncType(Integer syncType) {
		this.syncType = syncType;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DataSources{" +
			"id=" + id +
			", name=" + name +
			", type=" + type +
			", syncType=" + syncType +
			", active=" + active +
			", updateBy=" + updateBy +
			", updateTime=" + updateTime +
			", createBy=" + createBy +
			", createTime=" + createTime +
			", desc=" + desc +
			"}";
	}
}
