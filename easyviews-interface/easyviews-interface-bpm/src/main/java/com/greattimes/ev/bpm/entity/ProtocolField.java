package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 协议字段表
 * </p>
 *
 * @author Cgc
 * @since 2018-05-23
 */
@ApiModel
@TableName("bpm_protocol_field")
public class ProtocolField extends Model<ProtocolField>{

    private static final long serialVersionUID = 1L;
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
	private Integer protocolId;
	@ApiModelProperty("名称")
	private String name;
	@ApiModelProperty("别名")
	private String otherName;
	@ApiModelProperty("字段类型")
	private Integer type;
	@ApiModelProperty("字段英文名")
	private String ename;
	@ApiModelProperty("是否激活1激活0未激活")
	private Integer active;
	@ApiModelProperty("0普通    <1 代表分析可用  =2 代表追踪不可用")
	private Integer flag;



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getProtocolId() {
		return protocolId;
	}

	public void setProtocolId(Integer protocolId) {
		this.protocolId = protocolId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOtherName() {
		return otherName;
	}

	public void setOtherName(String otherName) {
		this.otherName = otherName;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}


	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getFlag() {
		return flag;
	}

	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	@Override
	public String toString() {
		return "ProtocolField [id=" + id + ", protocolId=" + protocolId + ", name=" + name + ", otherName=" + otherName
				+ ", type=" + type + ", ename=" + ename + ", active=" + active + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ename == null) ? 0 : ename.hashCode());
		result = prime * result + ((protocolId == null) ? 0 : protocolId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProtocolField other = (ProtocolField) obj;
		if (ename == null) {
			if (other.ename != null)
				return false;
		} else if (!ename.equals(other.ename))
			return false;
		if (protocolId == null) {
			if (other.protocolId != null)
				return false;
		} else if (!protocolId.equals(other.protocolId))
			return false;
		return true;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return this.id;
	}


}
