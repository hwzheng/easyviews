package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author NJ
 * @since 2018-11-15
 */
@TableName("bpm_data_group_info")
public class DataGroupInfo extends Model<DataGroupInfo> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * topic名称
     */
	private String groupName;
    /**
     * 分组id
     */
	private Integer groupId;
    /**
     * 应用或者组件id
     */
	private Integer value;
    /**
     *  2、告警应用组 3告警组件组 1、同步消息头分组 4、异步消息头分组
     */
	private Integer groupType;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Integer getGroupType() {
		return groupType;
	}

	public void setGroupType(Integer groupType) {
		this.groupType = groupType;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DataGroupInfo{" +
			"id=" + id +
			", groupName=" + groupName +
			", groupId=" + groupId +
			", value=" + value +
			", groupType=" + groupType +
			"}";
	}
}
