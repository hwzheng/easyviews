package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能瓶颈报表日期表
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@TableName("rp_bottleneck_task_day")
public class BottleneckTaskDay extends Model<BottleneckTaskDay> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 子任务id
     */
	private Integer taskItemId;
    /**
     * 步骤
     */
	private Integer step;
    /**
     * 日期
     */
	private Date date;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskItemId() {
		return taskItemId;
	}

	public void setTaskItemId(Integer taskItemId) {
		this.taskItemId = taskItemId;
	}

	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "BottleneckTaskDay{" +
			"id=" + id +
			", taskItemId=" + taskItemId +
			", step=" + step +
			", date=" + date +
			"}";
	}
}
