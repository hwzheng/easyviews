package com.greattimes.ev.bpm.trace.param.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/10/24 15:20
 */
public class TraceListResParam implements Serializable{
    private static final long serialVersionUID = 8461216429340178710L;

    private Integer total;
    private Integer state;
    private List<Map<String, Object>> detail;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public List<Map<String, Object>> getDetail() {
        return detail;
    }

    public void setDetail(List<Map<String, Object>> detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "TraceListResParam{" +
                "total=" + total +
                ", state=" + state +
                ", detail=" + detail +
                '}';
    }
}
