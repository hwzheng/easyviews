package com.greattimes.ev.bpm.analysis.param.resp;

import java.io.Serializable;

/**
 * <p>
 * 智能报表图表位置表
 * </p>
 *
 * @author cgc
 * @since 2019-03-27
 */
public class ReportChartLocationParam implements Serializable {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 左边距
     */
	private Double x;
    /**
     * 上边距
     */
	private Double y;
    /**
     * 高度
     */
	private Double h;
    /**
     * 宽度
     */
	private Double w;
    /**
     * 关系id
     */
	private Integer chartRelationId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getX() {
		return x;
	}

	public void setX(Double x) {
		this.x = x;
	}

	public Double getY() {
		return y;
	}

	public void setY(Double y) {
		this.y = y;
	}

	public Double getH() {
		return h;
	}

	public void setH(Double h) {
		this.h = h;
	}

	public Double getW() {
		return w;
	}

	public void setW(Double w) {
		this.w = w;
	}

	public Integer getChartRelationId() {
		return chartRelationId;
	}

	public void setChartRelationId(Integer chartRelationId) {
		this.chartRelationId = chartRelationId;
	}

	@Override
	public String toString() {
		return "ReportChartLocationParam [id=" + id + ", x=" + x + ", y=" + y + ", h=" + h + ", w=" + w
				+ ", chartRelationId=" + chartRelationId + "]";
	}

}
