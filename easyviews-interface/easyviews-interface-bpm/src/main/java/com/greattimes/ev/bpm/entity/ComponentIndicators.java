package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 组件指标表
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_component_indicators")
public class ComponentIndicators extends Model<ComponentIndicators> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Integer id;
    /**
     * 指标组id
     */
	private Integer indicatorGroupId;
    /**
     * 状态
     */
	private Integer active;
    /**
     * 组件id
     */
	private Integer componentId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorGroupId() {
		return indicatorGroupId;
	}

	public void setIndicatorGroupId(Integer indicatorGroupId) {
		this.indicatorGroupId = indicatorGroupId;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ComponentIndicators{" +
			"id=" + id +
			", indicatorGroupId=" + indicatorGroupId +
			", active=" + active +
			", componentId=" + componentId +
			"}";
	}
}
