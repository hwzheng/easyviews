package com.greattimes.ev.bpm.service.config;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Map;

import com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree;
import com.greattimes.ev.bpm.config.param.resp.ComponentChainParam;
import com.greattimes.ev.bpm.config.param.resp.IpPortParam;
import com.greattimes.ev.bpm.entity.ServerIp;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.bpm.config.param.req.ComponentParam;
import com.greattimes.ev.bpm.config.param.req.DictionaryParam;
import com.greattimes.ev.bpm.datasources.param.req.DataSourcesRelationParam;
import com.greattimes.ev.bpm.entity.Component;

/**
 * <p>
 * 应用组件表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface IComponentService extends IService<Component> {
    /**
     * 根据业务id查询组件相关信息
     * @author NJ
     * @date 2018/5/25 9:57
     * @param businessId
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<DictionaryParam> selectComponentNameAndIdByBusinessId(@Param("businessId") int businessId);

    /**
     * 根据id查询组件ip
     * @author NJ
     * @date 2018/5/25 11:11
     * @param id
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
     */
    List<DictionaryParam> selectComponentIpById(int id);
    /**
     * 根据ip查询组件端口
     * @author NJ
     * @date 2018/5/25 11:40
     * @param ip
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
     */
    List<DictionaryParam> selectComponentPortByIp(int ip);

    /**
     * 新增&编辑组件
     * @author NJ
     * @date 2018/5/28 9:49
     * @param param
     * @param name
     * @param topicNum
     * @return int
     */
    Component addOrUpdate(ComponentParam param, String name, int topicNum) throws InvocationTargetException, IllegalAccessException;

    /**
     * 组件查询
     * @author NJ
     * @date 2018/8/10 14:13
     * @param businessId
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.ComponentParam>
     */
    List<ComponentParam> selectComponentParamListByAppId(int businessId);


    /**
     * 组件排序
     * @author NJ
     * @date 2018/5/28 17:54
     * @param list
     * @return void
     */
    void updateBatchSort(List<ComponentParam> list);

    /**
     * 修改激活状态
     * @author NJ
     * @date 2018/5/28 18:06
     * @param id
     * @param active
     * @return void
     */
    int updateActive(int id, int active);


    /**
     * 组件详情
     * @author NJ
     * @date 2018/5/30 15:50
     * @param id
     * @return com.greattimes.ev.bpm.config.param.req.ComponentParam
     */
    ComponentParam getComponentDetail(int id) throws InvocationTargetException, IllegalAccessException;

    /***
     * 根据组件id和ip查询端口
     * @author NJ
     * @date 2018/7/3 14:19
     * @param componentId
     * @param ip
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
     */
    List<DictionaryParam> selectPortByComponentIdAndIp(int componentId, int ip);

    /***
     * 根据组件id和ip组查询端口组
     * @author NJ
     * @date 2018/7/3 14:19
     * @param componentId
     * @param ips
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
     */
    List<DictionaryParam> selectPortByComponentIdAndIps(int componentId, List<Integer> ips);


    /**
     * 是否有重复名称
     * @author NJ
     * @date 2018/6/15 10:43
     * @param id
     * @param monitorId
     * @param name
     * @return boolean
     */
    boolean isHasSameName(int id, int monitorId, String name);

    /**
     * 通过组件id查询ip端口
     * @author NJ
     * @date 2018/6/15 10:50
     * @param componentId
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
     */
    List<DictionaryParam> selectIpPortsByComponentId(int componentId);


    /**
     * 查找指标
     * @author NJ
     * @param type 
     * @date 2018/6/15 11:31
     * @param
     * @return java.util.List<com.greattimes.ev.bpm.config.param.req.DictionaryParam>
     */
    List<Map<String,Object>> selectIndicator(List<Integer> type);

	/**
	 * 删除组件，只删除应用模块相关表
	 * @author NJ
	 * @date 2018/7/3 14:20
	 * @param ids
	 * @return java.util.List<com.greattimes.ev.bpm.entity.Component>
	 */
	List<Component> delete(List<Integer> ids);

	/**
	 * 获取组建下ip
	 * @param componentId
	 * @return
	 */
	List<DataSourcesRelationParam> selectByComponentId(Integer componentId);

	/**获取组建下ip:port
	 * @param componentId
	 * @return
	 */
	List<DataSourcesRelationParam> selectIpAndport(Integer componentId);

	List<Component> selectComponentByType(Map<String, Object> map);

	/**获取中心ip
	 * @param componentId
	 * @return
	 */
	String getCenterIpBycomponentId(Integer componentId);

	/**
	 * 根据应用查询该应用下所有的组件的前后追踪字段
	 * @author NJ
	 * @date 2018/11/27 14:42
	 * @param appId
	 * @return java.util.List<com.greattimes.ev.bpm.config.param.resp.ComponentChainParam>
	 */
	List<ComponentChainParam> getComponentChainParamByAppId(int appId);


	/**
	 * 根据组件id 查询组件名称,应用名称,监控点名称(组件id为key, 数据为value)
	 * @author NJ
	 * @date 2018/12/3 19:36
	 * @param map
	 * @return java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.lang.Object>>
	 */
	Map<Integer, Map<String, Object>> getComponentNameByMap(Map<String, Object> map);
	

	/**   
	 * 查询组件数据
	 * @author NJ  
	 * @date 2019/3/27 11:16
	 * @param map   
	 * @return java.util.List<com.greattimes.ev.bpm.entity.Component>
	 * todo
	 */

	List<Component> selectAiApplicationTreeByType(Map<String, Object> map);

	/**
	 *
	 * @author NJ
	 * @date 2019/3/27 16:46
	 * @return java.util.Map<java.lang.String,java.util.List<com.greattimes.ev.bpm.analysis.param.resp.ApplicationTree>>
	 */
	Map<String, List<ApplicationTree>> selectAiApplicationTree(Map<String, Object> map);

	/**
	 * 应用相关的name属性(热点数据可以放入redis缓存中,后期考虑)
	 * @author NJ
	 * @date 2019/4/2 14:23
	 * @param map
	 * @return java.util.Map<java.lang.Integer,java.lang.String>
	 */
	Map<Integer, String> selectConfigName(Map<String, Object> map);

	/**
	 * 根据ip查询 
	 * @param ip
	 * @author: nj
	 * @date: 2020-03-12 10:54
	 * @version: 0.0.1
	 * @return: java.util.List<com.greattimes.ev.bpm.entity.ServerIp>
	 */ 
	List<ServerIp> selectIpByIpStr(List<String> ip);

	/**
	 * 根据port查询相应的IpPortParam
	 * @param portStr
	 * @author: nj
	 * @date: 2020-03-12 16:51
	 * @version: 0.0.1
	 * @return: java.util.List<com.greattimes.ev.bpm.config.param.resp.IpPortParam>
	 */
	List<IpPortParam> selectIpPortParamByPortStr(List<String> portStr);


}
