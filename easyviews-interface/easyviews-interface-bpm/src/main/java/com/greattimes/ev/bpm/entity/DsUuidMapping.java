package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * UUID映射表
 * </p>
 *
 * @author cgc
 * @since 2018-08-20
 */
@TableName("ds_uuid_mapping")
public class DsUuidMapping extends Model<DsUuidMapping> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.AUTO)
	private Long id;
    /**
     * 指标id
     */
	private Integer indicatorId;
    /**
     * 数据源id
     */
	private Integer dataSourceId;
    /**
     * 数据源内部uuid
     */
	private Integer innerUUID;
    /**
     * 外部uuid
     */
	private String outerUUID;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getIndicatorId() {
		return indicatorId;
	}

	public void setIndicatorId(Integer indicatorId) {
		this.indicatorId = indicatorId;
	}

	public Integer getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Integer dataSourceId) {
		this.dataSourceId = dataSourceId;
	}

	public Integer getInnerUUID() {
		return innerUUID;
	}

	public void setInnerUUID(Integer innerUUID) {
		this.innerUUID = innerUUID;
	}

	public String getOuterUUID() {
		return outerUUID;
	}

	public void setOuterUUID(String outerUUID) {
		this.outerUUID = outerUUID;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DsUuidMapping{" +
			"id=" + id +
			", indicatorId=" + indicatorId +
			", dataSourceId=" + dataSourceId +
			", innerUUID=" + innerUUID +
			", outerUUID=" + outerUUID +
			"}";
	}
}
