package com.greattimes.ev.bpm.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 应用表
 * </p>
 *
 * @author Cgc
 * @since 2018-05-18
 */
@TableName("bpm_application")
@ApiModel
public class Application extends Model<Application> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	@TableId(value="id", type= IdType.INPUT)
	private Integer id;
    /**
     * 名称
     */
	@ApiModelProperty("名称")
	private String name;
    /**
     * 激活状态：0 关闭 1开启
     */
	@ApiModelProperty("激活状态：0 关闭 1开启")
	private Integer active;
    /**
     * 告警状态:：0关闭 1开启
     */
	@ApiModelProperty("告警状态:：0关闭 1开启")
	private Integer alarmState;
    /**
     * 排序: 
     */
	@ApiModelProperty("排序")
	private Integer sort;
    /**
     * 创建人
     */
	@ApiModelProperty("创建人")
	private Integer createBy;
    /**
     * 创建时间
     */
	@ApiModelProperty("创建时间")
	private Date createTime;
    /**
     * 修改人
     */
	@ApiModelProperty("修改人")
	private Integer updateBy;
    /**
     * 修改时间
     */
	@ApiModelProperty("修改时间")
	private Date updateTime;

	/**
	 * 0表示未删除1表示已删除
	 */
	@ApiModelProperty("0表示未删除1表示已删除")
	private Integer state;
	
	/**
	 * 监控点数量
	 */
	@ApiModelProperty("监控点数量")
	private Integer monitorNum;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getAlarmState() {
		return alarmState;
	}

	public void setAlarmState(Integer alarmState) {
		this.alarmState = alarmState;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getMonitorNum() {
		return monitorNum;
	}

	public void setMonitorNum(Integer monitorNum) {
		this.monitorNum = monitorNum;
	}

	@Override
	public String toString() {
		return "Application{" +
				"id=" + id +
				", name='" + name + '\'' +
				", active=" + active +
				", alarmState=" + alarmState +
				", sort=" + sort +
				", createBy=" + createBy +
				", createTime=" + createTime +
				", updateBy=" + updateBy +
				", updateTime=" + updateTime +
				", state=" + state +
				", monitorNum=" + monitorNum +
				'}';
	}
}
