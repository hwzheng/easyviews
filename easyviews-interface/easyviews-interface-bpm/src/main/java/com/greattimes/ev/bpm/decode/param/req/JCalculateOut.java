package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;

/**
 * 计算操作in
 * @author MS
 * @date   2018 下午5:24:44
 */
public class JCalculateOut implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//操作数1主键
	private Integer fieldId;
	//操作数1来源
	private Integer origin;
	
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	public Integer getOrigin() {
		return origin;
	}
	public void setOrigin(Integer origin) {
		this.origin = origin;
	}
	
	@Override
	public String toString() {
		return "JCalculateOut [fieldId=" + fieldId + ", origin=" + origin + "]";
	}
	
}
