package com.greattimes.ev.bpm.service.analysis;

import com.greattimes.ev.bpm.analysis.param.resp.MetaParam;
import com.greattimes.ev.bpm.entity.BottleneckTask;
import com.greattimes.ev.bpm.entity.BottleneckTaskAnalyzeField;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;

import java.text.ParseException;

/**
 * <p>
 * 智能瓶颈报表任务表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
public interface IBottleneckTaskService extends IService<BottleneckTask> {

	/**获取报表任务分析字段
	 * @param taskId
	 * @return
	 */
	List<BottleneckTaskAnalyzeField> getAnalyzeField(int taskId);

	/**获取瓶颈meta
	 * @param taskId
	 * @return
	 */
	MetaParam getMeta(String taskId);

    /**获取单词任务详情
     * @param parseInt
     * @return
     */
    List<Map<String, Object>> getSingleTaskDetail(int parseInt);

	/**获取定时任务详情
	 * @param parseInt
	 * @return
	 */
	List<Map<String, Object>> getTimeTaskDetail(int parseInt);

    /**
     * 根据itemDayId查询json数据
     * @author NJ
     * @date 2018/10/25 19:21
     * @param itemDayId
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    Map<String, Object>  selectTaskDataByItemDayId(String itemDayId);

    /**
     * 保存json
     * @author NJ
     * @date 2018/10/25 20:58
     * @param itemDayId
     * @param jsonObject
     * @return void
     */
    void insertTaskData(String itemDayId, JSONObject jsonObject) throws ParseException;

	/**分析数据集添加
	 * @param jsonObject
	 * @param userId 
	 * @throws ParseException 
	 */
	Integer addTask(JSONObject jsonObject, Integer userId) throws ParseException;

	/**分析数据集查询
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> selectTask(Map<String, Object> map);

	/**详情表头查询
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> selectColumns(int id);

	/**分析数据集编辑
	 * @param jsonObject
	 * @param userId
	 */
	void updateTask(JSONObject jsonObject, Integer userId);

	/**分析数据集删除
	 * @param intValue
	 */
	void deleteTask(int intValue);

	/**任务新建
	 * @param jsonObject
	 * @return 
	 */
	Integer addItem(JSONObject jsonObject);

	/**任务编辑
	 * @param jsonObject
	 */
	void updateItem(JSONObject jsonObject);

	/**任务详情
	 * @param id
	 * @return
	 */
	Map<String, Object> getItemDetail(int id);

	/**任务删除
	 * @param ids
	 * @return
	 */
	void deleteItem(List<Integer> ids);

	/**分析初始化
	 * @param taskId
	 * @throws ParseException 
	 */
	void updateStep(String taskId) throws ParseException;

	/**定时任务日期列表查询
	 * @param id
	 * @return
	 */
	List<Long> selectDate(int id);

	/**分析进度
	 * @param taskId
	 * @return
	 * @throws ParseException 
	 */
	Map<String, Object> getStep(String taskId) throws ParseException;


	/**配置详情查询
	 * @param map
	 * @return
	 */
	Map<String, Object> selectTaskDetail(Map<String, Object> map);

	/**校验任务name
	 * @param id
	 * @param name
	 * @param flag 1添加
	 * @return
	 */
	Boolean checkTaskName(Integer id, String name, int flag);

	/**校验子任务name
	 * @param id
	 * @param name
	 * @param flag 1添加
	 * @return
	 */
	Boolean checkItemName(Integer id, String name, int flag);

}
