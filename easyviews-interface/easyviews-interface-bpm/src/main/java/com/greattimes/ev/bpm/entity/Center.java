package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 中心表
 * </p>
 *
 * @author cgc
 * @since 2018-08-08
 */
@TableName("bpm_center")
@ApiModel
public class Center extends Model<Center> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	@ApiModelProperty("id")
	private Integer id;
    /**
     * 名称
     */
	@NotEmpty(message="名称不能为空！")
	@Length(min=1, max=50,message="名称不能超过50位")
	@ApiModelProperty("名称")
	private String name;
    /**
     * 大数据ip
     */
	@NotEmpty(message="大数据IP不能为空！")
	@Pattern(regexp="(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)\\.(25[0-5]|2[0-4]\\d|[0-1]\\d{2}|[1-9]?\\d)",message="请输入正确的ip!")
	@ApiModelProperty("大数据ip")
	private String ip;
	@ApiModelProperty("探针数量")
	private Integer probeNum;

	@ApiModelProperty("激活状态0 否 1是")
	private Integer active;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Integer getProbeNum() {
		return probeNum;
	}

	public void setProbeNum(Integer probeNum) {
		this.probeNum = probeNum;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Center{" +
				"id=" + id +
				", name='" + name + '\'' +
				", ip='" + ip + '\'' +
				", probeNum=" + probeNum +
				", active=" + active +
				'}';
	}
}
