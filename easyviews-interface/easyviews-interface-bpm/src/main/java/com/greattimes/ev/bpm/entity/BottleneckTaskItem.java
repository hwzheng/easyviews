package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 智能瓶颈报表任务子表
 * </p>
 *
 * @author cgc
 * @since 2018-10-25
 */
@TableName("rp_bottleneck_task_item")
public class BottleneckTaskItem extends Model<BottleneckTaskItem> {

    private static final long serialVersionUID = 1L;

	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 名称
     */
	private String name;
    /**
     * 任务id
     */
	private Integer taskId;
    /**
     * 激活状态  :0关闭 1 开启
     */
	private Integer active;
	
	/**
	 * 步骤
	 */
	@TableField(exist=false)
	private Integer step;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}


	public Integer getStep() {
		return step;
	}

	public void setStep(Integer step) {
		this.step = step;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public BottleneckTaskItem(String name, Integer taskId, Integer active) {
		super();
		this.name = name;
		this.taskId = taskId;
		this.active = active;
	}

	public BottleneckTaskItem() {
		super();
	}

	@Override
	public String toString() {
		return "BottleneckTaskItem{" +
			"id=" + id +
			", name=" + name +
			", taskId=" + taskId +
			", active=" + active +
			"}";
	}
}
