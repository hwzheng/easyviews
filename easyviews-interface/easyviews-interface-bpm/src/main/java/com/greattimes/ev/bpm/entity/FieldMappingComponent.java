package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 维度字段映射配置表
 * </p>
 *
 * @author cgc
 * @since 2018-05-25
 */
@TableName("bpm_field_mapping_component")
public class FieldMappingComponent extends Model<FieldMappingComponent> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 维度字段id
	 */
	private Integer dimensionId;
	/**
	 * 组件id
	 */
	private Integer componentId;
	/**
	 * 配置id
	 */
	private Integer configId;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDimensionId() {
		return dimensionId;
	}

	public void setDimensionId(Integer dimensionId) {
		this.dimensionId = dimensionId;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "FieldMappingComponent{" + "id=" + id + ", dimensionId=" + dimensionId + ", componentId=" + componentId
				+ ", configId=" + configId + "}";
	}
}
