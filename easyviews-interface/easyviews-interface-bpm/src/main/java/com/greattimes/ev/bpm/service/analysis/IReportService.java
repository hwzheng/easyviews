package com.greattimes.ev.bpm.service.analysis;

import com.greattimes.ev.bpm.analysis.param.req.ReportChartDataParam;
import com.greattimes.ev.bpm.analysis.param.req.ReportChartParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportChartLocationParam;
import com.greattimes.ev.bpm.analysis.param.resp.ReportDescriptionLocationParam;
import com.greattimes.ev.bpm.entity.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 智能报表表 服务类
 * </p>
 *
 * @author cgc
 * @since 2019-03-22
 */
public interface IReportService extends IService<Report> {

	/**校验报表名称
	 * @param id
	 * @param name
	 * @param flag 1新增
	 * @return
	 */
	boolean checkReportName(Integer id, String name, int flag);

	/**添加报表
	 * @param jsonObject
	 * @param userId
	 * @return
	 */
	Integer addReport(JSONObject jsonObject, Integer userId);

	/**编辑报表
	 * @param jsonObject
	 * @param userId
	 * @return
	 */
	void editReport(JSONObject jsonObject, Integer userId);

	/**报表查看
	 * @param id
	 * @return
	 */
	Map<String, Object> queryReport(Integer id);

	/**删除报表
	 * @param integer
	 */
	void deleteReport(Integer integer);

	/**
	 * 报表列表查询
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> queryReportList(Map<String, Object> map);

	/**添加描述
	 * @param description
	 * @return
	 */
	Integer addDescription(ReportDescription description);

	/**编辑描述
	 * @param description
	 */
	void editDescription(ReportDescription description);

	/**删除描述
	 * @param id
	 */
	void deleteDescription(Integer id);

	/**描述列表查询
	 * @param reportId
	 * @return
	 */
	List<Map<String, Object>> queryDescriptionList(Integer reportId);

	/**添加图
	 * @param reportId
	 * @param charts
	 */
	void insertChartRelation(Integer reportId, List<Map<String, Object>> charts);

	/**移除图
	 * @param reportId
	 * @param id
	 */
	void deleteChartRelation(Integer reportId, Integer id);

	/**查询可用图表
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> queryAvailableChart(Map<String, Object> map);

	/**
	 * 保存图位置
	 * @param description
	 * @param chart
	 */
	void saveLocation(List<ReportDescriptionLocationParam> description, List<ReportChartLocationParam> chart);

	/**保存描述位置
	 * @param lo
	 */
	void saveDescriptionLocation(ReportDescriptionLocation lo);

	/**展示图表
	 * @param reportId
	 * @param userId 
	 * @return
	 */
	List<Map<String, Object>> queryChartList(Integer reportId, Integer userId);

	/**
	 * 保存报表图表
	 * @author NJ
	 * @date 2019/3/28 12:57
     * @param reportChartParam
	 * @return boolean
	 */
	ReportChart saveOrUpdateReportChart(ReportChartParam reportChartParam);
	/**
	 * 校验报表是否重名
	 * 规则:当前用户是否重名
	 * @author NJ
	 * @date 2019/4/24 16:31
	 * @param Id
	 * @param userId
	 * @param name
	 * @return boolean
	 */
	boolean checkReportCharName(Integer Id,Integer userId, String name);
	/**
	 * 根据id删除图表
	 * @author NJ
	 * @date 2019/3/28 16:55
	 * @param ids
	 * @return boolean
	 */
	boolean deleteReportChartByIds(List<Integer> ids);

	/**
	 * 根据id查询应用图表
	 * @author NJ
	 * @date 2019/3/29 15:24
	 * @param map
	 * @return com.greattimes.ev.bpm.analysis.param.req.ReportChartParam
	 */
	ReportChartParam  findReportChartByMap(Map<String, Object> map);

	/**
	 * 根据id查询事件图表
	 * @author NJ
	 * @date 2019/3/31 17:48
	 * @param map
	 * @return com.greattimes.ev.bpm.analysis.param.req.ReportChartParam
	 */
	ReportChartParam findEventReportChartByMap(Map<String, Object> map);


	/**
	 * 未报表数据提供配置信息
	 * @author NJ
	 * @date 2019/4/2 12:53
	 * @param map
	 * @return com.greattimes.ev.bpm.analysis.param.req.ReportChartParam
	 */
	@Deprecated
	ReportChartParam selectReportForIndicatorData(Map<String, Object> map);


	/**
	 * 根据chartId查询报表数据配置接口
	 * @param ids
	 * @return
	 */
	@Deprecated
	Map<String, Object> selectReportChartDataByChartIds(List<Integer> ids);


	/**
	 * 根据uuid和type查询名称
	 * @param list
	 * @return
	 */
	Map<String, Object> selectReportChartDataByUuid(List<ReportChartDataParam> list);

	/**
	 * 根据图表ids删除相应图表
	 * 规则：自己创建图表删除图表的配置
	 * 		分享给自己的图表删除关系
	 * @param ids
	 * @param userId
	 * @return boolean
	 * @throw
	 * @author NJ
	 * @date 2019-04-29 11:41
	*/
	boolean deleteChartByIds(Set<Integer> ids, Integer userId);

	/**更改描述位置
	 * @param lo
	 */
	void updateDescriptionLocation(ReportDescriptionLocation lo);

	/**重新添加单图
	 * @author CGC
	 * @param reportId
	 * @param chartId
	 * @param chartRelationId
	 */
	void editChartRelation(Integer reportId, Integer chartId, Integer chartRelationId);
}
