package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 告警組表
 * </p>
 *
 * @author cgc
 * @since 2018-06-04
 */
@ApiModel
public class AlarmGroupParam implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	private Integer id;
	
	@ApiModelProperty("告警人员信息")
	@NotNull(message="告警人员信息不能为空！")
	private List<Map<String, Object>> user;
	
	@ApiModelProperty("基本信息")
	@NotNull(message="基本信息不能为空！")
	private Map<String, Object> basic;
	
	@ApiModelProperty("告警组件数据")
	private List<Map<String, Object>> alarmComponentData;
	
	@ApiModelProperty("告警事件数据")
	private List<Map<String, Object>> alarmEventData;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public List<Map<String, Object>> getUser() {
		return user;
	}

	public void setUser(List<Map<String, Object>> user) {
		this.user = user;
	}

	public Map<String, Object> getBasic() {
		return basic;
	}

	public void setBasic(Map<String, Object> basic) {
		this.basic = basic;
	}

	public List<Map<String, Object>> getAlarmComponentData() {
		return alarmComponentData;
	}

	public void setAlarmComponentData(List<Map<String, Object>> alarmComponentData) {
		this.alarmComponentData = alarmComponentData;
	}

	public List<Map<String, Object>> getAlarmEventData() {
		return alarmEventData;
	}

	public void setAlarmEventData(List<Map<String, Object>> alarmEventData) {
		this.alarmEventData = alarmEventData;
	}

	@Override
	public String toString() {
		return "AlarmGroupParam [id=" + id + ", user=" + user + ", basic=" + basic + ", alarmComponentData="
				+ alarmComponentData + ", alarmEventData=" + alarmEventData + "]";
	}
	
	
}
