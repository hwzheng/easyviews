package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 重复排期表
 * </p>
 *
 * @author cgc
 * @since 2018-06-05
 */
@TableName("bpm_schedule_repeat")
public class ScheduleRepeat extends Model<ScheduleRepeat> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 应用id
     */
	private Integer applicationId;
    /**
     * 排期类型 1 ：全天 2 ：自定义时段
     */
	private Integer types;
    /**
     * 激活状态：1开启 ，0 关闭
     */
	private Integer active;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getTypes() {
		return types;
	}

	public void setTypes(Integer types) {
		this.types = types;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ScheduleRepeat{" +
			"id=" + id +
			", applicationId=" + applicationId +
			", types=" + types +
			", active=" + active +
			"}";
	}
}
