package com.greattimes.ev.bpm.decode.param.req;

import java.io.Serializable;


/**
 * tcp协议参数
 * @author MS
 * @date   2018 上午10:50:49
 */
public class JTcp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//报文是否采用 EBCDIC 编码
	private Boolean useEBCDIC;
	//请求报文
	private JTcpReqRespMsg reqMsg;
	//响应报文
	private JTcpReqRespMsg respMsg;

	public JTcpReqRespMsg getReqMsg() {
		return reqMsg;
	}
	public void setReqMsg(JTcpReqRespMsg reqMsg) {
		this.reqMsg = reqMsg;
	}
	public JTcpReqRespMsg getRespMsg() {
		return respMsg;
	}
	public void setRespMsg(JTcpReqRespMsg respMsg) {
		this.respMsg = respMsg;
	}
	public Boolean getUseEBCDIC() {
		return useEBCDIC;
	}
	public void setUseEBCDIC(Boolean useEBCDIC) {
		this.useEBCDIC = useEBCDIC;
	}
	@Override
	public String toString() {
		return "JTcp [useEBCDIC=" + useEBCDIC + ", reqMsg=" + reqMsg
				+ ", respMsg=" + respMsg + "]";
	}

}
