package com.greattimes.ev.bpm.datasources.param.req;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 组件指标包装类
 * @author cgc
 * @date 2018/7/11 13:02
 */
@ApiModel
public class ComponentIndicatorsParam implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 组件指标组 id
	 */
	@ApiModelProperty("组件指标组 id")
	private Integer id;
	@ApiModelProperty("指标组 id")
	private Integer indicatorGroupId;
	/**
	 * 组名称
	 */
	@ApiModelProperty("组名称")
	private String name;
	/**
	 * 同步状态 开启为1 关闭为0
	 */
	@ApiModelProperty("同步状态 开启为1 关闭为0")
	private Integer active;
	/**
	 * 组件id
	 */
	@ApiModelProperty("组件id")
	private Integer componentId;

	/**
	 * 数据源id
	 */
	@ApiModelProperty("数据源id")
	private Integer sourcesId;

	/**
	 * 约束
	 */
	@ApiModelProperty("约束")
	private List<CompIndContraintParam> constraint;
	
	/**
	 * 约束组
	 */
	@ApiModelProperty("约束组")
	private List<CompIndConstraintGroupParam> constraintValue;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIndicatorGroupId() {
		return indicatorGroupId;
	}

	public void setIndicatorGroupId(Integer indicatorGroupId) {
		this.indicatorGroupId = indicatorGroupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getComponentId() {
		return componentId;
	}

	public void setComponentId(Integer componentId) {
		this.componentId = componentId;
	}

	public Integer getSourcesId() {
		return sourcesId;
	}

	public void setSourcesId(Integer sourcesId) {
		this.sourcesId = sourcesId;
	}

	public List<CompIndContraintParam> getConstraint() {
		return constraint;
	}

	public void setConstraint(List<CompIndContraintParam> constraint) {
		this.constraint = constraint;
	}

	public List<CompIndConstraintGroupParam> getConstraintValue() {
		return constraintValue;
	}

	public void setConstraintValue(List<CompIndConstraintGroupParam> constraintValue) {
		this.constraintValue = constraintValue;
	}


}
