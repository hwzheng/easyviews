package com.greattimes.ev.bpm.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 数据源约束字段表
 * </p>
 *
 * @author cgc
 * @since 2018-07-10
 */
@TableName("ds_constraint_field")
public class ConstraintField extends Model<ConstraintField> {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
	private Integer id;
    /**
     * 组Id
     */
	private Integer groupId;
    /**
     * 指标名称
     */
	private String name;
    /**
     * 指标显示顺序
     */
	private Integer sort;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "ConstraintField{" +
			"id=" + id +
			", groupId=" + groupId +
			", name=" + name +
			", sort=" + sort +
			"}";
	}
}
