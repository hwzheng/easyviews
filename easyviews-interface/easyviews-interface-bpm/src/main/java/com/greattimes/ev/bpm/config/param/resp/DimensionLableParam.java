package com.greattimes.ev.bpm.config.param.resp;

import java.io.Serializable;
import java.util.List;

import com.greattimes.ev.bpm.entity.ProtocolField;

/**
 * 查询组件及对应维度返回实体
 * @author cgc
 * @date 2018/5/25 10:45
 */
public class DimensionLableParam implements Serializable {

	/**
	 */
	private static final long serialVersionUID = 7461613839294533906L;
	private int id;
	private String name;
	private List<ProtocolField> dimension;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ProtocolField> getDimension() {
		return dimension;
	}
	public void setDimension(List<ProtocolField> dimension) {
		this.dimension = dimension;
	}
	
}
