package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.Message;
import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.system.param.req.AlarmGroupUserParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 短信 发送表 服务类
 * </p>
 *
 * @author LiHua
 * @since 2018-10-10
 */
public interface IMessageService extends IService<Message> {
    /**
     * 根据条件，查找待发送的告警信息
     * @param type 待发送类型：  1 邮件 ，2 短信 ，3app ，4外呼 5微信
     * @param state 发送状态：状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
     * @return
     */
    List<Message> selectAlarmMessageByParam(int type, int state);

    /**
     * 查询所有激活告警组的相关人员
     * @param map
     * @return
     */
    List<AlarmGroupUserParam> selectAlarmUserList(Map<String, Object> map);


    /**
     * 根据键查询配置的参数值（已激活状态的值）
     * @param alarmMailFromName 邮件告警发送人
     * @return
     */
    String selectParameterByKey(String alarmMailFromName);


    /**
     * 更新告警信息状态
     * @param id 告警信息表id
     * @param state 更新后的状态
     */
    void updateAlarmMessageState(Integer id, int state);

    /**
     * 平安更新事件告警消息
     * @param ids
     * @param state
     * @return
     */
    boolean updateAlarmMessageEventState(List<Integer> ids, int state);

    /**
     * 平安更新kafka告警状态
     * @param messages
     * @param state
     * @return
     */
    boolean updateAlarmMessageKafkaState(List<Message> messages, int state);

}
