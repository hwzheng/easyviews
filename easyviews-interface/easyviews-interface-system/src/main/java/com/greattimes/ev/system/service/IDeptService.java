package com.greattimes.ev.system.service;

import java.util.List;
import java.util.Map;

import com.greattimes.ev.system.entity.Dept;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.param.req.DeptUserParam;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
public interface IDeptService {

	/**
	 * 用户群组查询
	 * @return
	 */
	List<Map<String, Object>> selectDept();

	/**
	 * 删除部门
	 * 
	 * @param id
	 */
	void deleteDept(Integer id);

	/**
	 * 编辑部门
	 * 
	 * @param dept
	 */
	void updateDept(Dept dept);

	/**
	 * 校验部门名称
	 * 
	 * @param dept
	 * @param flag
	 * @return
	 */
	Boolean checkName(Dept dept, int flag);

	/**
	 * 新增部门
	 * 
	 * @param dept
	 * @return 
	 */
	Integer addDept(Dept dept);

	/**
	 * 用户保存
	 * @param groupId
	 * @param ids
	 */
	void saveUser(Integer groupId, List<Integer> ids);

	/**
	 * 用户删除
	 * @param groupId
	 * @param id
	 */
	void deleteUser(Integer groupId, Integer id);

	/**
	 * 查询群组用户
	 * @return
	 */
	List<DeptUserParam> selectDeptAndUser();

	/**
	 * 根据用户组名称查询相应的用户
	 * @author NJ
	 * @date 2019/3/4 15:56
	 * @param name
	 * @return java.util.List<com.greattimes.ev.system.entity.User>
	 */
	List<User>  selectUserByDeptName(String name);

	/**
	 * 事件台缺失服务查询
	 * @param list
	 * @return
	 */
	List<String> selectMissService(List<String> list);

	/**
	 * 事件台查询所有的服务
	 * @return
	 */
	Integer getAllServiceNum();

	List<Dept> selectDeptByType(Integer type);

	/**
	 * 查询事件台配置的服务 
	 * @param 
	 * @author: nj
	 * @date: 2020-03-06 10:56
	 * @version: 0.0.1
	 * @return: java.util.Map<java.lang.String,java.lang.Integer>
	 */ 
	Map<String, Integer> selectAllService();




}
