package com.greattimes.ev.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.system.entity.Menu;
import com.greattimes.ev.system.entity.User;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-03-23
 */
public interface IMenuService extends IService<Menu> {
	/**
	 * 根据角色id查询菜单id
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> findRoleMenuTree(int id);

	/**
	 * 查询用户下的路由
	 * @param user
	 * @param ename
	 * @param isShow 查menu传1，查button传0
	 * @return
	 */
	List<Map<String, Object>> getRouters(User user, String ename, int isShow);

	/**查询全部路由
	 * @param user
	 * @return
	 */
	List<Map<String, Object>> getAllRouters(User user);
	
	/**
	 * 获取角色菜单
	 * @return
	 */
	
	List<Map<String, Object>> getRoleMenus();
}
