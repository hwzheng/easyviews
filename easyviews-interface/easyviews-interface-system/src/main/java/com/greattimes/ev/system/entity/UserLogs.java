package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户操作日志表
 * </p>
 *
 * @author cgc
 * @since 2020-02-25
 */
@TableName("sys_user_logs")
public class UserLogs extends Model<UserLogs> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 时间
     */
	private Date time;
    /**
     * 操作类型：0登录、1 退出登录、2 新建用户、3 删除用户、4 激活用户、5 禁用用户、6 修改密码、7 重置密码 8 下线通知
     */
	private Integer type;
    /**
     * 0 成功、1 失败
     */
	private Integer state;
    /**
     * 操作用户 
     */
	private String user;
    /**
     * 日志描述
     */
	private String desc;
    /**
     * login 登录、ownspace 个人中心user 系统管理 / 用户管理
     */
	private String module;
	/**
	 * 操作用户角色类型
	 */
	private Integer roleType;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}


	public UserLogs() {
	}

	public UserLogs(Date time, Integer type, Integer state, String user, String desc, String module, Integer roleType) {
		this.time = time;
		this.type = type;
		this.state = state;
		this.user = user;
		this.desc = desc;
		this.module = module;
		this.roleType = roleType;
	}

	@Override
	public String toString() {
		return "UserLogs{" +
				"id=" + id +
				", time=" + time +
				", type=" + type +
				", state=" + state +
				", user='" + user + '\'' +
				", desc='" + desc + '\'' +
				", module='" + module + '\'' +
				", roleType=" + roleType +
				'}';
	}
}
