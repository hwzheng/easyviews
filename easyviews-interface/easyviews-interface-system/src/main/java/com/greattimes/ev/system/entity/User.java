package com.greattimes.ev.system.entity;

import java.io.Serializable;

import org.jboss.logging.Field;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 系统用户表
 * </p>
 *
 * @author cgc
 * @since 2018-05-11
 */
@TableName("sys_user")
@ApiModel
public class User extends Model<User> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@TableId(value = "id", type = IdType.AUTO)
	private Integer id;
	/**
	 * 登录名
	 */
	@ApiModelProperty("登录名")
	private String loginName;

	/**
	 * 密码
	 */
	private String password;
	/**
	 * 真实名称
	 */
	@ApiModelProperty("用户真实姓名")
	private String name;
	/**
	 * 邮件
	 */
	private String email;
	/**
	 * 电话
	 */
	private Long phone;
	/**
	 * 激活状态 0 关闭 1 激活
	 */
	private Integer active;
	/**
	 * 用户类型 : 0 普通用户 1 域用户 2 其他
	 */
	private Integer type;
	/**
	 * 角色类型
	 */
	@TableField(exist=false)
	private Role role;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String phoneToString(){
		return this.phone == null ? null : this.phone.toString();
	}



	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", loginName=" + loginName + ", password=" + password + ", name=" + name
				+ ", email=" + email + ", phone=" + phone + ", active=" + active + ", type=" + type + "}";
	}
}
