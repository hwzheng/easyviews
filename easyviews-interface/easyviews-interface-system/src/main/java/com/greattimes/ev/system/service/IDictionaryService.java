package com.greattimes.ev.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.system.entity.Dictionary;
import com.greattimes.ev.system.entity.DictionaryType;

/**
 * <p>
 * 系统字典表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-05-25
 */
public interface IDictionaryService extends IService<Dictionary> {
    /**
     * 根据字典表类型查询字典表
     * @author NJ
     * @date 2018/5/25 11:06
     * @param typeCode
     * @return java.util.List<java.util.Map<java.lang.String,java.lang.Object>>
     */
    List<Map<String, Object>> selectDicNameAndValueByDictTypeCode(String typeCode);

    /**
     * 查询所有Dictionarytype数据
     * @return
     */
    List<DictionaryType> getDictionaryType();

	/**
	 * 字典类型新增
	 * @param dictionaryType
	 * @return 
	 */
	Integer saveDictionaryType(DictionaryType dictionaryType);

	/**
	 * 字典类型编辑
	 * @param dictionaryType
	 */
	void editDictionaryType(DictionaryType dictionaryType);

	/**
	 * 字典类型删除
	 * @param id
	 */
	void deleteDictionaryType(Integer id);

	/**
	 * 字典查询
	 * @param id
	 * @return
	 */
	List<Dictionary> getDictionary(Integer id);

	/**
	 * 字典添加
	 * @param dictionary
	 */
	void saveDictionary(Dictionary dictionary);

	/**
	 * 字典编辑
	 * @param dictionary
	 */
	void editDictionary(Dictionary dictionary);

	/**
	 * 字典删除
	 * @param id
	 */
	void deleteDictionary(Integer id);

	/**
	 * 字典状态修改
	 * @param dictionary
	 */
	void updateActive(Dictionary dictionary);

	/**
	 * 校验typecode 
	 * @param dictionaryType
	 * @param i
	 * @return
	 */
	boolean checkType(DictionaryType dictionaryType, int i);

	/**
	 * 校验name
	 * @param dictionaryType
	 * @param i
	 * @return
	 */
	boolean checkName(DictionaryType dictionaryType, int i);

	/**
	 * 获取字典详情
	 * @param id
	 * @return
	 */
	List<Dictionary> getDictionaryDetail(Integer id);

	/**校验字典
	 * @param dictionary
	 * @param flag 0 编辑
	 * @return
	 */
	boolean checkDictionaryValue(Dictionary dictionary, int flag);

	/**校验字典名
	 * @param dictionary
	 * @param flag 0 编辑
	 * @return
	 */
	boolean checkDictionaryName(Dictionary dictionary, int flag);
}
