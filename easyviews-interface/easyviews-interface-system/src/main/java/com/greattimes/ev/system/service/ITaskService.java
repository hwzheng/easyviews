package com.greattimes.ev.system.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.system.entity.Task;
import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.param.req.TaskParam;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 定时任务表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-07-17
 */
public interface ITaskService extends IService<Task> {

    /**
     * 定时任务查询
     * @author NJ
     * @date 2018/7/17 15:58
     * @param map
     * @return java.util.List<com.greattimes.ev.system.param.req.TaskParam>
     */
    List<TaskParam> selectListByMap(Map<String, Object> map);

    /**
     * 分页查询定时任务日志
     * @author NJ
     * @date 2018/7/17 16:28
     * @param page
     * @param param
     * @return com.baomidou.mybatisplus.plugins.Page<java.util.Map<java.lang.String,java.lang.Object>>
     */
    Page<Map<String,Object>> selectLogsListByMap(Page<Map<String, Object>> page, Map<String,Object> param);

}
