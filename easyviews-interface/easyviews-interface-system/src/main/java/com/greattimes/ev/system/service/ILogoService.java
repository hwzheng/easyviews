package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.Logo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * logo表 服务类
 * </p>
 *
 * @author cgc
 * @since 2019-05-06
 */
public interface ILogoService extends IService<Logo> {

}
