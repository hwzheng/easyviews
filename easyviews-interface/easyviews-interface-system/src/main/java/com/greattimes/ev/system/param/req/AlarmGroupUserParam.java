package com.greattimes.ev.system.param.req;


import java.io.Serializable;
import java.util.List;

/**
 * @author NJ
 * @date 2019/1/17 14:48
 */
public class AlarmGroupUserParam  implements Serializable{
    private static final long serialVersionUID = -5015379416752476517L;

    private Integer id;

    private int active;

    List<UserParam> users;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public List<UserParam> getUsers() {
        return users;
    }

    public void setUsers(List<UserParam> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "AlarmGroupUserParam{" +
                "id=" + id +
                ", active=" + active +
                ", users=" + users +
                '}';
    }
}
