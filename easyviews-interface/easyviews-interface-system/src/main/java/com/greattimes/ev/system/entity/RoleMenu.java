package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 角色菜单表
 * </p>
 *
 * @author NiuJian
 * @since 2018-05-14
 */
@TableName("sys_role_menu")
public class RoleMenu extends Model<RoleMenu> {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色
	 */
	private Integer roleId;
	/**
	 * 菜单
	 */
	private Integer menuId;


	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getMenuId() {
		return menuId;
	}

	public void setMenuId(Integer menuId) {
		this.menuId = menuId;
	}

	@Override
	protected Serializable pkVal() {
		return this.roleId;
	}

	@Override
	public String toString() {
		return "RoleMenu{" +
				"roleId=" + roleId +
				", menuId=" + menuId +
				"}";
	}
}
