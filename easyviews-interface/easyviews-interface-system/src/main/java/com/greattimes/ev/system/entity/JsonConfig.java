package com.greattimes.ev.system.entity;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

/**
 * <p>
 * 系统 json参数配置表
 * </p>
 *
 * @author cgc
 * @since 2019-03-05
 */
@TableName("sys_json_config")
public class JsonConfig extends Model<JsonConfig> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 编码
     */
	@NotEmpty(message="编码不能为空！")
	@Length(min=1, max=30,message="编码不能超过30位")
	@Pattern(regexp = "^[A-Z_]+$",message="编码由大写字母和下划线组成！")
	private String code;
    /**
     * 名称
     */
	@NotEmpty(message="名称不能为空！")
	@Length(min=1, max=50,message="名称不能超过50位")
	private String name;
    /**
     * 参数
     */
	private String param;
    /**
     * 修改时间
     */
	private Date updateTime;
    /**
     * 修改人
     */
	private Integer updateBy;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "JsonConfig{" +
			"id=" + id +
			", code=" + code +
			", name=" + name +
			", param=" + param +
			", updateTime=" + updateTime +
			", updateBy=" + updateBy +
			"}";
	}
}
