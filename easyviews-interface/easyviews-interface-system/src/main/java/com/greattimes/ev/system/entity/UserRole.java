package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
@TableName("sys_user_role")
public class UserRole extends Model<UserRole> {

	private static final long serialVersionUID = 1L;

	/**
	 * 角色id
	 */
	private Integer roleId;
	/**
	 * 用户id
	 */
	private Integer userId;

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	protected Serializable pkVal() {
		return this.roleId;
	}

	@Override
	public String toString() {
		return "UserRole{" + "roleId=" + roleId + ", userId=" + userId + "}";
	}
}
