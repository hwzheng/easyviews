package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.Role;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-03-23
 */
public interface IRoleService extends IService<Role> {

    /**
     * 查询所有角色
     * @return
     */
    List<Role> findList();

    /**
     * 保存角色
     * @param role
     * @return
     */
    int saveOrUpdateRole(Role role);

    /**
     * 根据id删除角色以角色相关菜单权限
     * @param id
     * @return
     */
    int deleteRoleMenus(int id);

    /**
     * 根据id查询
     * @param id
     * @return
     */
    Role selectRoleById(int id);

    /**
     * 校验角色名称是否重复
     * @param id
     * @param name
     * @return
     */
    boolean isHasSameName(Integer id, String name);

    /**
     * 根据userId查找用户角色列表
     * @param id
     * @return
     */
    List<Role> findRoleByUserId(int id);

}
