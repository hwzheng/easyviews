package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 定时任务表执行日志表
 * </p>
 *
 * @author NJ
 * @since 2018-07-17
 */
@TableName("sys_task_logs")
public class TaskLogs extends Model<TaskLogs> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 定时任务id
     */
	private Integer taskId;
    /**
     * 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败 
     */
	private Integer stat;
    /**
     * 日志详情
     */
	private String detail;
    /**
     * 执行开始时间
     */
	private Date starttimes;
    /**
     * 执行时长(秒)
     */
	private Integer duration;
    /**
     * 入库时间
     */
	private Date insertTime;
    /**
     * 数据时间；处理数据条数
     */
	private String desc;

	@TableField(exist = false)
	private String className;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTaskId() {
		return taskId;
	}

	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}

	public Integer getStat() {
		return stat;
	}

	public void setStat(Integer stat) {
		this.stat = stat;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getStarttimes() {
		return starttimes;
	}

	public void setStarttimes(Date starttimes) {
		this.starttimes = starttimes;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "TaskLogs{" +
				"id=" + id +
				", taskId=" + taskId +
				", stat=" + stat +
				", detail='" + detail + '\'' +
				", starttimes=" + starttimes +
				", duration=" + duration +
				", insertTime=" + insertTime +
				", desc='" + desc + '\'' +
				", className='" + className + '\'' +
				'}';
	}
}
