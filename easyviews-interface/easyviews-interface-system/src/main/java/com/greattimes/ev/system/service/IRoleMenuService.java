package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.RoleMenu;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author NJ
 * @since 2018-03-23
 */
public interface IRoleMenuService extends IService<RoleMenu> {
    /**
     * 根据roleId 批量插入角色菜单
     * @param roleId
     * @param menuIds
     * @return
     */
    int insertBatch(Integer roleId,  List<Integer> menuIds);

    /**
     * 保存角色菜单权限
     * @author NJ
     * @date 2018/5/24 17:09
     * @param roleId
     * @param menuIds
     * @return int
     */
    int saveRoleMenus(Integer roleId,  List<Integer> menuIds);
}
