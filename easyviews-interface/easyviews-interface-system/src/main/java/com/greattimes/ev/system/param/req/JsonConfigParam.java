package com.greattimes.ev.system.param.req;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 系统 json参数配置表
 * </p>
 *
 * @author cgc
 * @since 2019-03-05
 */
public class JsonConfigParam implements Serializable{

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	private Integer id;
    /**
     * 编码
     */
	private String code;
    /**
     * 名称
     */
	private String name;
    /**
     * 参数
     */
	private String param;
    /**
     * 修改时间
     */
	private Date updateTime;
    /**
     * 修改人
     */
	private Integer updateBy;
	
	/**
	 * 用户姓名
	 */
	private String userName;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


}
