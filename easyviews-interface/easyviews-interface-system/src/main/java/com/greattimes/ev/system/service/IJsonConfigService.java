package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.JsonConfig;
import com.greattimes.ev.system.param.req.JsonConfigParam;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 系统 json参数配置表 服务类
 * </p>
 *
 * @author cgc
 * @since 2019-03-05
 */
public interface IJsonConfigService extends IService<JsonConfig> {

	/** 校验类型
	 * @param param
	 * @param i 1新增
	 * @return
	 */
	boolean checkType(JsonConfig param, int i);

	/**校验name
	 * @param param
	 * @param i
	 * @return
	 */
	boolean checkName(JsonConfig param, int i);

	/**json类型保存
	 * @param param
	 * @param userId
	 * @return
	 */
	Integer saveJsonType(JsonConfig param, Integer userId);

	/**编辑
	 * @param param
	 * @param userId
	 */
	void editJsonType(JsonConfig param, Integer userId);

	JsonConfigParam getJsonDetail(Integer id, String code);

}
