package com.greattimes.ev.system.param.req;

import java.io.Serializable;
import java.util.List;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 * @since 2018-05-15
 */
@ApiModel
public class DeptUserParam extends Model<DeptUserParam> {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户组id
	 */
	@ApiModelProperty("groupId")
	private Integer groupId;
	/**
	 * 用户组名
	 */
	@ApiModelProperty("用户组名")
	private String groupName;

	/**
	 * 用户
	 */
	@ApiModelProperty("用户信息")
	private List<UserSimpleParam> user;

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public List<UserSimpleParam> getUser() {
		return user;
	}

	public void setUser(List<UserSimpleParam> user) {
		this.user = user;
	}

	@Override
	protected Serializable pkVal() {
		return null;
	}
}
