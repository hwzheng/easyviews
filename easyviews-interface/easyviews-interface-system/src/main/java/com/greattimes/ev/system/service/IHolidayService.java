package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.Holiday;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 节假日表 服务类
 * </p>
 *
 * @author cgc
 * @since 2019-09-24
 */
public interface IHolidayService extends IService<Holiday> {

	Page<Holiday> selectHoliday(Page<Holiday> page);

	/**批量导入
	 * @author CGC
	 * @param details
	 */
	void insert(List<Holiday> details);

	/**获取节假日
	 * @author CGC
	 * @param param
	 * @return
	 */
	List<String> getday(Map<String, Object> param);

}
