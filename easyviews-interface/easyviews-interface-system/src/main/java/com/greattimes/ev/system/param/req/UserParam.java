package com.greattimes.ev.system.param.req;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 * @since 2018-05-15
 */
@ApiModel
public class UserParam extends Model<UserParam> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty("id")
	private Integer id;
	/**
	 * userId
	 */
	@ApiModelProperty("用户id")
	private Integer userId;
	/**
	 * 登录名
	 */
	@NotEmpty(message="登录名不能为空！")
	@Length(min=1, max=20,message="登录名不能超过20位")
	@ApiModelProperty("登录名")
	private String loginName;

	/**
	 * 密码
	 */
	@ApiModelProperty("密码")
	private String password;
	/**
	 * 真实名称
	 */
	@ApiModelProperty("真实名称")
	private String name;
	/**
	 * 邮件
	 */
	@Email
	@ApiModelProperty("邮件")
	private String email;
	/**
	 * 电话
	 */
	@ApiModelProperty("电话")
	private Long phone;
	/**
	 * 激活状态 0 关闭 1 激活
	 */
	@ApiModelProperty("激活状态 0 关闭 1 激活")
	private Integer active;
	/**
	 * 用户类型 : 0 普通用户 1 域用户 2 其他
	 */
	@NotNull(message="类型不能为空！")
	@ApiModelProperty("用户类型 : 0 普通用户 1 域用户 2 其他")
	private Integer type;

	/**
	 * 用户部门
	 */
	@ApiModelProperty("用户部门")
	private Integer[] deptId;
	/**
	 * 用户角色
	 */
	@ApiModelProperty("用户角色")
	private Integer roleId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer[] getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer[] deptId) {
		this.deptId = deptId;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "User{" + "id=" + id + ", loginName=" + loginName + ", password=" + password + ", name=" + name
				+ ", email=" + email + ", phone=" + phone + ", active=" + active + ", type=" + type + "}";
	}
}
