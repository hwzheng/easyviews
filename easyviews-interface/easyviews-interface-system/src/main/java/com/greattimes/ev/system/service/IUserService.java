package com.greattimes.ev.system.service;

import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.service.IService;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.entity.UserDept;
import com.greattimes.ev.system.entity.UserLogs;
import com.greattimes.ev.system.param.req.UserLogsParam;
import com.greattimes.ev.system.param.req.UserParam;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author LiHua
 * @since 2018-03-13
 */
/**
 * @author Administrator
 *
 */
public interface IUserService extends IService<User>{

	/**
	 * 获取用户
	 * 
	 * @param user
	 * @return
	 */
	User getUser(User user);

	/**
	 * 获取用户功能列表
	 * 
	 * @param map
	 * @return
	 */
	List<Map<String, Object>> getUserMenu(Integer paranetId, Integer userId);

	/**
	 * 删除用户
	 * 
	 * @param id
	 */
	void deleteUser(Integer id);

	/**
	 * 修改用户状态
	 * 
	 * @param id
	 * @param state˙
	 */
	void updateUserState(int id, int state);

	/**
	 * 用户新增
	 * 
	 * @param user
	 * @return
	 */
	int addUser(UserParam user);

	/**
	 * 用户编辑
	 * 
	 * @param user
	 * @return
	 */
	int editUser(UserParam user);

	/**
	 * 校验用户名
	 * 
	 * @param user
	 * @param flag 0编辑
	 * @return
	 */
	Boolean checkLoginName(UserParam user, int flag);

	/**
	 * 查询用户列表
	 * 
	 * @param user
	 * @return
	 */
	List<Map<String, Object>> selectUser();

	/**
	 * 根据id判断是否是预制超管
	 * @param id
	 * @return
	 */
	boolean isDefaultAdmin(Integer id);
	/**
	 * 根据id判断是否是预制超管
	 * @param userId
	 * @return
	 */
	Role getUserRole(Integer userId);

	/**
	 * 根据ID password查询用户
	 * @param id
	 * @param password
	 * @return
	 */
	Boolean selectUser(Integer id, String password);

	/**
	 * 查询同群组用户
	 * @param groupId
	 * @return
	 */
	List<Map<String, Object>> selectUserByDept(Integer groupId);

	/**
	 * 个人中心查询
	 * @param user
	 * @return
	 */
	List<Map<String, Object>> selectUserPersonal(User user);

	/**
	 * 个人中心信息修改
	 * @param map
	 */
	void updatePassword(Map<String, Object> map);

	/**
	 * 重置密码
	 * @param id
	 * @param password 
	 * @param password
	 */
	void updatePassword(Integer id, String password);

	/**
	 * 用户列表详情
	 * @param id
	 * @return
	 */
	List<Map<String, Object>> selectDetail(Integer id);


    /**
     * 校验用户登录逻辑(包含AD域的校验)
     * @param user
     * @param param
     * @return
     */
	User checkUser(User user, Map<String, String > param);

	/**查询同组用户
	 * @param userId
	 * @return
	 */
	List<Map<String, Object>> selectGroupUser(Integer userId);

	/**
	 * 根据报表查询报表从属人员
	 * @author NJ
	 * @date 2019/4/1 17:04
     * @param id
	 * @return java.util.List<com.greattimes.ev.system.entity.User>
	 */
	List<Map<String, Object>> selectUserByReportId(int id);

	/**
	 * find all users except myself
	 * @param userId
	 * @param active
	 * @return
	 */
	List<Map<String, Object>> selectUsersExcludeSelf(Integer userId, Integer active);

	/**
	 * insert userlogs
	 * @param userlog
	 */
    void insertUserLogs(UserLogs userlog);

	List<UserLogs> selectUserLogs(UserLogsParam param);
}
