package com.greattimes.ev.system.entity;

import java.io.Serializable;

import java.util.Date;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 短信 发送表
 * </p>
 *
 * @author LiHua
 * @since 2018-10-10
 */
@TableName("sys_message")
public class Message extends Model<Message> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 数据时间戳
     */
	private Long timestamp;
    /**
     * 待发送消息
     */
	private String message;
    /**
     * 发送类型 （同告警发送方式一致） 1 邮件 ，2 短信 ，3app ，4外呼
     */
	private String type;
    /**
     * 数据插入时间
     */
	private Date insertTime;
    /**
     * 告警发送组id
     */
	private String alarmGroupId;
    /**
     * 状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
     */
	private Integer state;

	/**
	 * 告警数据
	 */
	@TableField(exist = false)
	private String constraintDesc;

	/**
	 * 告警规则描述
	 */
	@TableField(exist = false)
	private String ruleDesc;

	/**
	 * 告警的持续时间
	 */
	@TableField(exist = false)
	private Integer duration;

	/**
	 * '告警级别'
	 */
	private Integer alarmLevel;
	/**
	 * '告警唯一标识'
	 */
	private String alarmKey;
	/**
	 * 应用id
	 */
	private Integer applicationId;
	/**
	 * 应用名称
	 */
	@TableField(exist = false)
	private String applicationName;

	/**
	 * '告警类型 0 正常告警 1 恢复告警'
	 */
	private Integer alarmType;
	/**
	 * '告警的发送类型'
	 */
	private Integer sendType;
    /**
     * 事件的发送状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
     */
	private Integer eventState;
	/**
	 * kafka推送状态 0：待发送 ，1 发送中， 2：发送成功 ，3 发送失败
	 */
	private Integer kafkaState;
	/**
	 * 告警规则id
	 */
	private Integer ruleId;
	/**
	 * group key
	 */
	@TableField(exist = false)
	private String groupKey;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getInsertTime() {
		return insertTime;
	}

	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	public String getAlarmGroupId() {
		return alarmGroupId;
	}

	public void setAlarmGroupId(String alarmGroupId) {
		this.alarmGroupId = alarmGroupId;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public String getConstraintDesc() {
		return constraintDesc;
	}

	public void setConstraintDesc(String constraintDesc) {
		this.constraintDesc = constraintDesc;
	}

	public String getRuleDesc() {
		return ruleDesc;
	}

	public void setRuleDesc(String ruleDesc) {
		this.ruleDesc = ruleDesc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getAlarmLevel() {
		return alarmLevel;
	}

	public void setAlarmLevel(Integer alarmLevel) {
		this.alarmLevel = alarmLevel;
	}

	public String getAlarmKey() {
		return alarmKey;
	}

	public void setAlarmKey(String alarmKey) {
		this.alarmKey = alarmKey;
	}

	public Integer getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}

	public Integer getAlarmType() {
		return alarmType;
	}

	public void setAlarmType(Integer alarmType) {
		this.alarmType = alarmType;
	}

	public Integer getSendType() {
		return sendType;
	}

	public void setSendType(Integer sendType) {
		this.sendType = sendType;
	}

    public Integer getEventState() {
        return eventState;
    }

    public void setEventState(Integer eventState) {
        this.eventState = eventState;
    }

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Integer getKafkaState() {
		return kafkaState;
	}

	public void setKafkaState(Integer kafkaState) {
		this.kafkaState = kafkaState;
	}

	public Integer getRuleId() {
		return ruleId;
	}
	public void setRuleId(Integer ruleId) {
		this.ruleId = ruleId;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getGroupKey() {
		return groupKey;
	}

	@Override
	public String toString() {
		return "Message{" +
				"id=" + id +
				", timestamp=" + timestamp +
				", message='" + message + '\'' +
				", type='" + type + '\'' +
				", insertTime=" + insertTime +
				", alarmGroupId='" + alarmGroupId + '\'' +
				", state=" + state +
				", constraintDesc='" + constraintDesc + '\'' +
				", ruleDesc='" + ruleDesc + '\'' +
				", duration=" + duration +
				", alarmLevel=" + alarmLevel +
				", alarmKey='" + alarmKey + '\'' +
				", applicationId=" + applicationId +
				", applicationName='" + applicationName + '\'' +
				", alarmType=" + alarmType +
				", sendType=" + sendType +
				", eventState=" + eventState +
				", kafkaState=" + kafkaState +
				", ruleId=" + ruleId +
				", groupKey='" + groupKey + '\'' +
				'}';
	}
}
