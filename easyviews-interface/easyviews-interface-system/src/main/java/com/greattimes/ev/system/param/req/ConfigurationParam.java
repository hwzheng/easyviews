package com.greattimes.ev.system.param.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/5/10 13:53
 */
@ApiModel
public class ConfigurationParam implements Serializable{

    @ApiModelProperty("配置id")
    private Integer id;

    @NotEmpty(message="key不能为空！")
    @ApiModelProperty("key")
    private String key;
    @NotEmpty(message="value不能为空！")
    @ApiModelProperty("value")
    private String value;
    @NotEmpty(message="备注不能为空！")
    @ApiModelProperty("备注")
    private String remarks;
    /**
     * 0 解码相关 1 web 2 其它
     */
//    @Range(min=0,max=2,message="类型参数不正确！")
    @ApiModelProperty("配置参数类型 0 解码相关 1 web 2 其它")
    private Integer type;
    /**
     * 是否激活（0：否 1：是）
     */
    @Range(min=0,max=1,message="激活参数不正确！")
    @ApiModelProperty("是否激活（0：否 1：是）")
    private Integer active;

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "ConfigurationParam{" + "id=" + id + ", key='" + key + '\'' + ", value='" + value + '\'' + ", remarks='" + remarks + '\'' + ", type=" + type + ", active=" + active + '}';
    }
}
