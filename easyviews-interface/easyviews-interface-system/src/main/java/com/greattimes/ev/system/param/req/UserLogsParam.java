package com.greattimes.ev.system.param.req;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 用户操作日志表
 * </p>
 *
 * @author cgc
 * @since 2020-02-25
 */
@ApiModel
public class UserLogsParam implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 时间
     */
	@ApiModelProperty("开始时间")
	private Long startTime;
	/**
	 * 时间
	 */
	@ApiModelProperty("结束时间")
	private Long endTime;
	/**
     * 操作类型：0登录、1 退出登录、2 新建用户、3 删除用户、4 激活用户、5 禁用用户、6 修改密码、7 重置密码
     */
	@ApiModelProperty("操作类型")
	private Integer type;
    /**
     * 操作用户 
     */
	@ApiModelProperty("操作用户")
	private String user;
	/**
	 * 角色类型
	 */
	@ApiModelProperty("角色类型")
	private Integer roleType;

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Integer getRoleType() {
		return roleType;
	}

	public void setRoleType(Integer roleType) {
		this.roleType = roleType;
	}

	@Override
	public String toString() {
		return "UserLogsParam{" +
				"startTime=" + startTime +
				", endTime=" + endTime +
				", type=" + type +
				", user='" + user + '\'' +
				", roleType=" + roleType +
				'}';
	}
}
