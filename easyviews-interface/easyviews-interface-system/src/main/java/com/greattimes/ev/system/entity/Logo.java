package com.greattimes.ev.system.entity;

import java.io.Serializable;

import java.sql.Blob;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * logo表
 * </p>
 *
 * @author cgc
 * @since 2019-05-06
 */
@TableName("sys_logo")
public class Logo extends Model<Logo> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 系统logo图片
     */
	private byte[] logo;
    /**
     * 图片类型 0 png, 1 jpg, 2 svg, 3 gif
     */
	private Integer type;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Logo{" +
			"id=" + id +
			", logo=" + logo +
			", type=" + type +
			"}";
	}
}
