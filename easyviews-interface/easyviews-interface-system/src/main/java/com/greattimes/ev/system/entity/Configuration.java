package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 配置表
 * </p>
 *
 * @author LiHua
 * @since 2018-03-22
 */
@TableName("sys_configuration")
@ApiModel
public class Configuration extends Model<Configuration> {

    private static final long serialVersionUID = 1L;

    /**
     * id,主键
     */
	@TableId(value="id", type= IdType.AUTO)
	@ApiModelProperty("id")
	private Integer id;
    /**
     * 该值由开发人员维护
     */
	@ApiModelProperty("key")
	private String key;
	@ApiModelProperty("value")
	private String value;
    /**
     * 备注
     */
	@ApiModelProperty("备注")
	private String remarks;
    /**
     * 0:其他 ,1:大数据 ，2：解码 ，3：系统配置
     */
	@ApiModelProperty("0:其他 ,1:大数据 ，2：解码 ，3：系统配置")
	private Integer type;
    /**
     * 是否激活（0：否 1：是）
     */
	@ApiModelProperty("是否激活（0：否 1：是）")
	private Integer active;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Configuration{" +
			"id=" + id +
			", key=" + key +
			", value=" + value +
			", remarks=" + remarks +
			", type=" + type +
			", active=" + active +
			"}";
	}
}
