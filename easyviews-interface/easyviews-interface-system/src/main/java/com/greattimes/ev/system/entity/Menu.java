package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 用户菜单表
 * </p>
 *
 * @author NiuJian
 * @since 2018-05-14
 */
@TableName("sys_menu")
public class Menu extends Model<Menu> {

	private static final long serialVersionUID = 1L;

	/**
	 * 菜单表
	 */
	private Integer id;
	private Integer parentId;
	private String ename;
	private String cname;
	private String href;
	private String icon;
	private Integer active;
	private String permission;
	private Integer isLeaf;
	private Integer isRefresh;
	private Integer isButton;
	private Integer sort;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getHref() {
		return href;
	}

	public void setHref(String href) {
		this.href = href;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public String getPermission() {
		return permission;
	}

	public void setPermission(String permission) {
		this.permission = permission;
	}

	public Integer getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(Integer isLeaf) {
		this.isLeaf = isLeaf;
	}

	public Integer getIsRefresh() {
		return isRefresh;
	}

	public void setIsRefresh(Integer isRefresh) {
		this.isRefresh = isRefresh;
	}

	public Integer getIsButton() {
		return isButton;
	}

	public void setIsButton(Integer isButton) {
		this.isButton = isButton;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	@Override
	public String toString() {
		return "Menu{" +
				"id=" + id +
				", parentId=" + parentId +
				", ename='" + ename + '\'' +
				", cname='" + cname + '\'' +
				", href='" + href + '\'' +
				", icon='" + icon + '\'' +
				", active=" + active +
				", permission='" + permission + '\'' +
				", isLeaf=" + isLeaf +
				", isRefresh=" + isRefresh +
				", isButton=" + isButton +
				", sort=" + sort +
				'}';
	}
}
