package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * <p>
 * 用户角色
 * </p>
 *
 * @author NiuJian
 * @since 2018-05-14
 */
@TableName("sys_role")
@ApiModel
public class Role extends Model<Role> {

	private static final long serialVersionUID = 1L;

	/**
	 * id
	 */
	@ApiModelProperty("角色id")
	private Integer id;
	/**
	 * 角色名称
	 */
	@ApiModelProperty("角色名称")
	private String name;
	/**
	 * 角色英文名称
	 */
	@ApiModelProperty("角色英文名称")
	private String ename;
	/**
	 * 是否是系统默认 0 否 1是
	 */
	@ApiModelProperty("是否是系统默认 0 否 1是")
	private Integer isDefault;
	/**
	 * 角色类型 0 超级管理 员 1 普通管理员 2 监控角色
	 */
	@ApiModelProperty("角色类型 0 超级管理 员 1 普通管理员 2 监控角色")
	private Integer type;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public Integer getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(Integer isDefault) {
		this.isDefault = isDefault;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Role{" +
				"id=" + id +
				", name=" + name +
				", ename=" + ename +
				", isDefault=" + isDefault +
				", type=" + type +
				"}";
	}
}
