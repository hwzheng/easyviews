package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * <p>
 * 用户部门表
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
@TableName("sys_user_dept")
public class UserDept extends Model<UserDept> {

	private static final long serialVersionUID = 1L;

	private Integer deptId;
	private Integer userId;

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Override
	protected Serializable pkVal() {
		return this.deptId;
	}

	@Override
	public String toString() {
		return "UserDept{" + "deptId=" + deptId + ", userId=" + userId + "}";
	}
}
