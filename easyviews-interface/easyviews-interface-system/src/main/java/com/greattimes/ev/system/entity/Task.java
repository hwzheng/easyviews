package com.greattimes.ev.system.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 定时任务表
 * </p>
 *
 * @author NJ
 * @since 2018-07-17
 */
@TableName("sys_task")
public class Task extends Model<Task> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	private Integer id;
    /**
     * 定时任务名称
     */
	private String name;
    /**
     * 表达式
     */
	private String cron;
    /**
     * 类名称
     */
	private String className;
    /**
     * 是否激活 0否 1是
     */
	private Integer active;
    /**
     * 所属对象 1 通用  2 平安 3 民生 4 辽宁
     */
	private Integer target;
    /**
     * 启动参数 json
     */
	private String param;
    /**
     * 定时任务详细描述
     */
	private String remarks;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getTarget() {
		return target;
	}

	public void setTarget(Integer target) {
		this.target = target;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Task{" +
			"id=" + id +
			", name=" + name +
			", cron=" + cron +
			", className=" + className +
			", active=" + active +
			", target=" + target +
			", param=" + param +
			", remarks=" + remarks +
			"}";
	}
}
