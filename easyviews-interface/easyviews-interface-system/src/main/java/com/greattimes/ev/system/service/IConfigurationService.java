package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.Configuration;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 配置表 服务类
 * </p>
 *
 * @author NJ
 * @since 2018-03-22
 */
public interface IConfigurationService extends IService<Configuration> {

    /**
     * 查询参数列表包含条件
     * @param param
     * @return
     */
    List<Configuration> queryConfiguration(Map<String, Object> param);

    /***
     * 更新参数配置
     * @param configuration
     * @return
     */
    int update(Configuration configuration);

    /***
     * 保存参数配置
     * @param configuration
     * @return
     */
    int save(Configuration configuration);

    /**
     * 根据id删除参数配置
     * @param id
     * @return
     */
    Configuration delete(int id);


    /**
     * 更新参数状态
     * @param id
     * @param state
     * @return
     */
    Configuration updateConfigurationState(int id, int state);

    /**
     * 判断是否含有相同的key
     * @param key
     * @return
     */
    boolean isHasSameKey(String key,Integer id);

    /**
     * 根据key查找相应的记录
     * @param key
     * @return
     */
    Configuration findConfigurationByKey(String key);

}
