package com.greattimes.ev.system.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 系统字典表
 * </p>
 *
 * @author NJ
 * @since 2018-05-24
 */
@TableName("sys_dictionary")
@ApiModel
public class Dictionary extends Model<Dictionary> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	@ApiModelProperty("字典id")
	private Integer id;
    /**
     * 名称
     */
	@NotEmpty(message="名称不能为空！")
	@Length(min=1, max=60,message="名称不能超过60位")
	@ApiModelProperty("名称")
	private String name;
    /**
     * 值
     */
	@NotEmpty(message="字典值不能为空！")
	@Length(min=1, max=60,message="字典值不能超过60位")
	@ApiModelProperty("值")
	private String value;
    /**
     * 类型编码
     */
	@NotEmpty(message="类型不能为空！")
	@ApiModelProperty("类型编码")
	private String typeCode;
    /**
     * 是否激活
     */
	@ApiModelProperty("是否激活")
	private Integer active;
    /**
     * 父级编号
     */
	@ApiModelProperty("父级编号")
	private Integer parentId;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public Integer getActive() {
		return active;
	}

	public void setActive(Integer active) {
		this.active = active;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Dictionary{" +
			"id=" + id +
			", name=" + name +
			", value=" + value +
			", typeCode=" + typeCode +
			", active=" + active +
			", parentId=" + parentId +
			"}";
	}
}
