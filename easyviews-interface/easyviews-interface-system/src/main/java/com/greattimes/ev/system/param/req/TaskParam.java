package com.greattimes.ev.system.param.req;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * @author NJ
 * @date 2018/7/17 15:08
 */
@ApiModel
public class TaskParam implements Serializable{

    private static final long serialVersionUID = 1470895641909342905L;

    /**
     * id
     */
    @ApiModelProperty("id")
    private Integer id;
    /**
     * 定时任务名称
     */
    @ApiModelProperty("定时任务名称")
    private String name;
    /**
     * 表达式
     */
    @ApiModelProperty("表达式")
    private String cron;
    /**
     * 类名称
     */
    @ApiModelProperty("类名称")
    private String className;
    /**
     * 是否激活 0否 1是
     */
    @ApiModelProperty("是否激活 0否 1是")
    private Integer active;
    /**
     * 所属对象 1 通用  2 平安 3 民生 4 辽宁
     */
    @ApiModelProperty("所属对象")
    private String target;
    /**
     * 启动参数 json
     */
    @ApiModelProperty("启动参数 json")
    private String param;
    /**
     * 定时任务详细描述
     */
    @ApiModelProperty("定时任务详细描述")
    private String remarks;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCron() {
        return cron;
    }

    public void setCron(String cron) {
        this.cron = cron;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        return "TaskParam{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cron='" + cron + '\'' +
                ", className='" + className + '\'' +
                ", active=" + active +
                ", target='" + target + '\'' +
                ", param='" + param + '\'' +
                ", remarks='" + remarks + '\'' +
                '}';
    }
}
