package com.greattimes.ev.system.param.resp;

import com.greattimes.ev.system.entity.Message;

import java.io.Serializable;
import java.util.Map;

/**
 * Created by Administrator on 2019/12/26.
 */
public class KafKaAlarmMessageParam implements Serializable{
    private static final long serialVersionUID = 1L;
    private Message labels;
    private Map<String,Object> annotations;
    /**
     * 告警开始时间
     */
    private Long  startsAt;
    /**
     * 告警结束时间
     */
    private Long  endsAt;
    private String generatorURL;
    /**
     * 告警的唯一标识
     */
    private String trackId;
    private String dsu;
    /**
     * 系统ID的映射
     */
    private Integer subsysid;
    /**
     * 告警规则id
     */
    private Integer ruleId;
    /**
     * 对应关系：1级Critical，2级Major，3级Minor，4级Warning，5级Info
     */
    private String level;
    /**
     * 告警源
     */
    private String dataSrc;
    /**
     * 告警类型，这里接入的EasyViews和F16都属于app
     */
    private String type;
    /**
     * 告警内容
     */
    private String summary;
    /**
     * except/normal 告警状态，异常/正常
     */
    private String status;
    /**
     * 本条信息发出时间
     */
    private Long timestamp;

    public Message getLabels() {
        return labels;
    }

    public void setLabels( Message labels) {
        this.labels = labels;
    }

    public Map<String, Object> getAnnotations() {
        return annotations;
    }

    public void setAnnotations(Map<String, Object> annotations) {
        this.annotations = annotations;
    }

    public Long getStartsAt() {
        return startsAt;
    }

    public void setStartsAt(Long startsAt) {
        this.startsAt = startsAt;
    }

    public Long getEndsAt() {
        return endsAt;
    }

    public void setEndsAt(Long endsAt) {
        this.endsAt = endsAt;
    }

    public String getGeneratorURL() {
        return generatorURL;
    }

    public void setGeneratorURL(String generatorURL) {
        this.generatorURL = generatorURL;
    }

    public String getTrackId() {
        return trackId;
    }

    public void setTrackId(String trackId) {
        this.trackId = trackId;
    }

    public String getDsu() {
        return dsu;
    }

    public void setDsu(String dsu) {
        this.dsu = dsu;
    }

    public Integer getSubsysid() {
        return subsysid;
    }

    public void setSubsysid(Integer subsysid) {
        this.subsysid = subsysid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDataSrc() {
        return dataSrc;
    }

    public void setDataSrc(String dataSrc) {
        this.dataSrc = dataSrc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getRuleId() {
        return ruleId;
    }

    public void setRuleId(Integer ruleId) {
        this.ruleId = ruleId;
    }

    @Override
    public String toString() {
        return "KafKaAlarmMessageParam{" +
                "labels=" + labels +
                ", annotations=" + annotations +
                ", startsAt=" + startsAt +
                ", endsAt=" + endsAt +
                ", generatorURL='" + generatorURL + '\'' +
                ", trackId='" + trackId + '\'' +
                ", dsu='" + dsu + '\'' +
                ", subsysid=" + subsysid +
                ", ruleId=" + ruleId +
                ", level='" + level + '\'' +
                ", dataSrc='" + dataSrc + '\'' +
                ", type='" + type + '\'' +
                ", summary='" + summary + '\'' +
                ", status='" + status + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
