package com.greattimes.ev.system.param.req;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author cgc
 * @since 2018-05-15
 */
@ApiModel
public class UserSimpleParam extends Model<UserSimpleParam> {

	private static final long serialVersionUID = 1L;

	/**
	 * userId
	 */
	@ApiModelProperty("用户id")
	private Integer userId;

	/**
	 * 真实名称
	 */
	@ApiModelProperty("真实名称")
	private String name;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	protected Serializable pkVal() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
