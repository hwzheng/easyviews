package com.greattimes.ev.system.service;

import com.greattimes.ev.system.entity.TaskLogs;

import java.util.Map;

/**
 * @author NJ
 * @date 2018/8/20 15:25
 */
public interface IDataManageService {
    /**
     * 查询定时任务id列表
     * @return
     */
    Map<String, Integer> selectTaskIdMap();


    /**
     * 插入定时任务记录日志
     * @param taskLogs
     */
    void insertTaskLogs(TaskLogs taskLogs);
}
