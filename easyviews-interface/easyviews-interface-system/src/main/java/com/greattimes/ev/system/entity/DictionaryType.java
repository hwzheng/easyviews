package com.greattimes.ev.system.entity;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * <p>
 * 系统字典类型表
 * </p>
 *
 * @author cgc
 * @since 2018-07-17
 */
@TableName("sys_dictionary_type")
@ApiModel
public class DictionaryType extends Model<DictionaryType> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	@TableId(value="id", type= IdType.AUTO)
	@ApiModelProperty("字典组id")
	private Integer id;
    /**
     * 名称
     */
	@NotEmpty(message="名称不能为空！")
	@Length(min=1, max=60,message="名称不能超过60位")
	@ApiModelProperty("名称")
	private String name;
    /**
     * 类型编码
     */
	@Length(min=1, max=60,message="字典类型值不能超过60位！")
	@NotEmpty(message="字典类型值不能为空！")
	@ApiModelProperty("类型编码")
	private String typeCode;
    /**
     * 描述
     */
	@Length(min=0, max=60,message="描述不能超过60位！")
	@ApiModelProperty("描述")
	private String remark;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeCode() {
		return typeCode;
	}

	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "DictionaryType{" +
			"id=" + id +
			", name=" + name +
			", typeCode=" + typeCode +
			", remark=" + remark +
			"}";
	}
}
