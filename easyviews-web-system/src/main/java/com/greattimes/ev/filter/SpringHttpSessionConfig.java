package com.greattimes.ev.filter;

import com.greattimes.ev.common.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.data.redis.config.annotation.web.http.RedisHttpSessionConfiguration;
import org.springframework.session.web.http.CookieSerializer;
import org.springframework.session.web.http.DefaultCookieSerializer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author: nj
 * @date: 2020-06-14 13:50
 * @version: 0.0.1
 */

@Configuration
@PropertySource(value = {"classpath:config.properties"})
//@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 60, redisNamespace = "SpringXX")
public class SpringHttpSessionConfig extends RedisHttpSessionConfiguration{


    Logger log = LoggerFactory.getLogger(SpringHttpSessionConfig.class);

//    @Value("${session.namespace}")
//    private String redisNameSpace;
//
//    @Value("${session.timeout}")
//    private Integer maxInactiveIntervalInSeconds;


//    @Value("${maxInactiveIntervalInSeconds}")
//    private Integer maxInactiveIntervalInSeconds;
//
//    @Bean
//    public RedisHttpSessionConfiguration SpringHttpSessionConfig(){
//        RedisHttpSessionConfiguration session = new RedisHttpSessionConfiguration();
//        session.setMaxInactiveIntervalInSeconds(maxInactiveIntervalInSeconds);
//        session.setRedisNamespace("springXXX:");
//        return session;
//    }


    public SpringHttpSessionConfig() {
        super();
        Properties properties = new Properties();
        //system.getEnv与system.getProperty区别(系统与线程关系)
        String nameSpace = System.getenv("session.namespace");
        String sessionTimeout = System.getenv("session.timeout");
        if(StringUtils.isBlank(nameSpace) && StringUtils.isBlank(sessionTimeout)){
            log.info("############httpSession参数从配置文件获取#####");
            try {
                InputStream inputStream = SpringHttpSessionConfig.class.getClassLoader().getResourceAsStream("config.properties");
                properties.load(inputStream);
                nameSpace = properties.getProperty("session.namespace");
                sessionTimeout = properties.getProperty("session.timeout");
            } catch (IOException e) {
                e.printStackTrace();
                log.info("############httpSession参数从配置文件获取失败！！！#####");
                nameSpace = "Spring-session:";
                sessionTimeout = "180";
            }
        }else{
            log.info("############httpSession参数从rancher全局变量配置#####");
        }
        log.info("############httpSession获取,namespace:"+ nameSpace +",timeout:"+ sessionTimeout);

        super.setMaxInactiveIntervalInSeconds(Integer.parseInt(sessionTimeout));
        super.setRedisNamespace(nameSpace);

    }


//    @Autowired
//    private RedisOperationsSessionRepository sessionRepository;
//
//    @PostConstruct
//    private void afterPropertiesSet() {
//        log.info("setting sesion 超时时间 = [{}]分钟",sessionTimeoutInMinuts);
//        sessionRepository.setDefaultMaxInactiveInterval(sessionTimeoutInMinuts * 60);
//        log.info("setting sesion 存储到redis时的namespace = [{}]",redisnamespace);
//        sessionRepository.setRedisKeyNamespace(redisnamespace);
//    }

    @Bean
    public CookieSerializer httpSessionIdResolver(){
        DefaultCookieSerializer cookieSerializer = new DefaultCookieSerializer();
        cookieSerializer.setCookieName("token");
        cookieSerializer.setUseHttpOnlyCookie(false);
        cookieSerializer.setSameSite(null);
        return cookieSerializer;
    }

}