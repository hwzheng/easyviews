package com.greattimes.ev.base;

/**
 * @author NJ
 * @date 2018/6/1 16:15
 */
public class Constant {
    /**
     * 删除成功提示信息
     */
    public  static final String DELETE_SUCCESS_MSG = "删除成功！";
    /**
     * 删除失败提示信息
     */
    public  static final String DELETE_ERROR_MSG = "删除失败！";
    /**
     * 保存成功提示信息
     */
    public static final String  SAVE_SUCCESS_MSG = "保存成功！";
    /**
     * 保存失败提示信息
     */
    public static final String  SAVE_ERROR_MSG = "保存失败！";
    /**
     * 修改成功提示信息
     */
    public static final String  EDIT_SUCCESS_MSG = "修改成功！";
    /**
     * 修改失败提示信息
     */
    public static final String  EDIT_ERROR_MSG = "修改失败！";
    /**
     * 状态成功提示信息
     */
    public static final String STATE_SUCCESS_MSG = "状态修改成功！";
    /**
     * 状态失败提示信息
     */
    public static final String STATE_ERROR_MSG = "状态修改失败！";
    /**
     * 没有数据记录
     */
    public static final String HAS_NO_RECORDS = "没有该条记录！";
    /**
     * 下载成功提示信息
     */
    public static final String DOWNLOAD_SUCCESS_MSG = "下载成功！";
    /**
     * 上传成功提示信息
     */
    public static final String UPLOAD_SUCCESS_MSG = "上传成功！";
    /**
     * 上传失败提示信息
     */
    public static final String UPLOAD_ERROR_MSG = "上传失败！";

    /**
     * 参数错误
     */
    public static final String PARAMETER_ERROR_MSG = "参数错误！";
    /**
     * 初始化成功
     */
    public static final String INIT_SUCCESS_MSG = "初始化成功！";
    /**
     * 应用告警类型
     */
    public static final Integer ALARMTYPE_APPLICATION = 1;
    /**
     * 组件告警类型
     */
    public static final Integer ALARMTYPE_COMPONENT = 2;
    /**
     * ip告警类型
     */
    public static final Integer ALARMTYPE_IP = 3;
    /**
     * ipPort告警类型
     */
    public static final Integer ALARMTYPE_IPPORT = 4;
    /**
     * 单维度告警类型
     */
    public static final Integer ALARMTYPE_SINGLEDIMENSION = 5;
    /**
     * 多维度告警类型
     */
    public static final Integer ALARMTYPE_MULTIEDIMENSION = 6;

    /**
     * 监控点告警类型
     */
    public static final Integer ALARMTYPE_MONITOR = 7;

    /**
     * 数据源告警类型
     */
    public static final Integer ALARMTYPE_DATASOURCE = 8;
    /**
     * 自定义业务告警-业务
     */
    public static final  int ALARMTYPE_CUSTOM_BUSINESS = 9;
    /**
     * 自定义业务告警-统计维度
     */
    public static final int ALARMTYPE_CUSTOM_STATISTICS_DIMENSION = 10;
    /**
     * 自定义业务告警-普通维度
     */
    public static final int ALARMTYPE_CUSTOM_NORMAL_DIMENSION = 11;
    /**
     * 自定义业务告警-多维维度
     */
    public static final int ALARMTYPE_CUSTOM__MULTI_DIMENSION = 12;

    /**
     * 应用时间延迟key
     */
    public static final String APPLICATION_DELAY = "application_data_delay";
    /**
     * 告警时间延迟
     */
    public static final String ALARM_DELAY = "alarm_data_delay";
    /**
     * bpm数据颗粒度(s)
     */
    public static final String BPM_INTERVAL = "bpm_interval";

    /**
     * 是否使用清华的基线
     */
    public static final String USE_QINGHUA = "use_qinghua";

    /**
     * 清华基线主机
     */
    public static final String QINGHUA_BASELINE_HOST = "qinghua_baseline_host";

    /**
     * 清华基线灵敏度URL
     */
    public static final String QINGHUA_BASELINE_SENSITIVITY = "qinghua_baseline_sensitivity";

    /**
     * topic 创建的程序组数量
     */
    public static final String TOPIC_NUM = "topic_num";


    /**
     * 组件port数量最大值
     */
    public static final String PORT_MAX_NUM = "port_max_num";

    /**
     * 五种的维度的列名
     * 客户端ip(组件分析维度必有)
     */
    public static final String CLIENT_IP = "clientIp";

    /**
     * 服务端ip(组件分析维度必有)
     */
    public static final String SERVER_IP= "serverIp";

    /**
     * 交易类型
     */
    public static final String TRANSACTION_TYPE = "transactionType";

    /**
     * 交易渠道
     */
    public static final String TRANSACTION_CHANNEL = "transactionChannel";

    /**
     * 返回码
     */
    public static final String RET_CODE = "retCode";

    /**
     * 服务端端口
     */
    public static final String SERVER_PORT = "serverPort";

    /**
     * 交易追踪主机地址
     */
    public static final String TRANSACTION_TRACE_HOST = "transaction_trace_host";

    /**
     * 交易追踪初始化接口
     */
    public static final String TRANSACTION_TRACE_INIT = "transaction_trace_init";

    /**
     * 智能交易追踪接口
     */
    public static final String TRANSACTION_TRACE_INTELLIGENT = "transaction_trace_intelligent";


    /**
     * 交易追踪刷新接口
     */
    public static final String TRANSACTION_TRACE_REFRESH = "transaction_trace_refresh";

    /**
     * 交易追踪分页排序接口
     */
    public static final String TRANSACTION_TRACE_ORDER = "transaction_trace_order";


    /**
     * 单笔交易追踪主机名称
     */
    public static final String TRANSACTION_TRACE_SINGLE_HOST = "transaction_trace_single_host";

    /**
     * 单笔交易追踪接口
     */
    public static final String TRANSACTION_TRACE_SINGLE = "transaction_trace_single";

    /**
     * 告警短信恢复通知时间间隔
     */
    public static final String ALARM_SEND_INTERVAL_THRESHOLD = "alarm_send_interval_threshold";

    /**
     * 平安kafka发送条数
     */
    public static final String PINGAN_ALARM_KAFKA_BATCH_SIZE = "pingan_alarm_kafka_batch_size";

    /**
     * 平安kafka发送topic
     */
    public static final String PINGAN_ALARM_KAFKA_TOPIC = "pingan_alarm_kafka_topic";


}
