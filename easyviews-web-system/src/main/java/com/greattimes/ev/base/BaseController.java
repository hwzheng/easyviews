package com.greattimes.ev.base;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.system.entity.User;

public class BaseController {
	
	Logger logger =LoggerFactory.getLogger(this.getClass());
	protected final static String SUC_CODE="0";
	private final static String ERR_CODE="1";
	private final static String CHECK_CODE="2";
	
	/**
	 * 获取session中的user对象
	 */
	public final User getSessionUser(HttpServletRequest request){
		String u= (String) request.getSession().getAttribute("member");
		return JSON.parseObject(u, User.class) ;
	}
	
	/**
	 * 设置session中的user对象
	 */
	public final void setSessionUser(HttpServletRequest request,User user){
		request.getSession().setAttribute("member", JSON.toJSONString(user));
	}
	
	/**
	 * 执行成功 返回
	 * @return
	 */
	public  JsonResult<Object> execSuccess(String msg){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(SUC_CODE);
		ret.setMsg(msg);
		return ret;
	}
	
	/**
	 * 执行成功 返回
	 * @return
	 */
	public  JsonResult<Object> execSuccess(Object t){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(SUC_CODE);
		ret.setData(t);
		return ret;
	}
	/**
	 * 执行成功 返回
	 * @return
	 */
	public  JsonResult<Object> execSuccess(Object t, String msg){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(SUC_CODE);
		ret.setData(t);
		ret.setMsg(msg);
		return ret;
	}

	/**
	 * 执行出错 返回
	 * @param msg
	 * @param exception
	 * @return
	 */
	public  JsonResult<Object> execError(String msg,String exception){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(ERR_CODE);
		ret.setMsg(msg);
		ret.setException(exception);
		return ret;
	}

	/**
	 * 逻辑出错 返回
	 * @param msg
	 * @return
	 */
	public  JsonResult<Object> returnError(String msg){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(ERR_CODE);
		ret.setMsg("发生未知错误！");
		ret.setException(msg);
		return ret;
	}

	/**
	 * 执行校验失败
	 * @param msg
	 * @return
	 */
	public  JsonResult<Object> execCheckError(String msg){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(CHECK_CODE);
		ret.setMsg(msg);
		return ret;
	}

	public  JsonResult<Object> execCheckError(Object t, String msg){
		JsonResult<Object> ret=new JsonResult<Object>();
		ret.setCode(CHECK_CODE);
		ret.setMsg(msg);
		ret.setData(t);
		return ret;
	}

	/** 异常处理 */
	@ExceptionHandler(Exception.class)
	public ModelAndView exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex)
			throws Exception {
		logger.error(ex.getMessage());
		ex.printStackTrace();
		ModelAndView model=new ModelAndView();
		//if(WebUtil.isAjaxRequest(request)){//是否ajax 是返回json
			try {
				 response.setHeader("Content-type", "application/Json;charset=UTF-8");  
				 //这句话的意思，是告诉servlet用UTF-8转码，而不是用默认的ISO8859  
				 response.setCharacterEncoding("UTF-8");
				 PrintWriter writer = response.getWriter();
				 writer.write(JSON.toJSONString(this.returnError(ex.getMessage())));
				 writer.flush();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			return null;
		/*}else{
			model.setViewName("/common/error");
			model.addObject("ex", ex);
			return model;
		}*/
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ModelAndView exceptionValidationHandler(HttpServletRequest request, HttpServletResponse response, MethodArgumentNotValidException ex) {
		logger.error(ex.getMessage());
		ex.printStackTrace();
		try {
			response.setHeader("Content-type", "application/Json;charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter writer = response.getWriter();
			List<FieldError> fieldErrors =  ex.getBindingResult().getFieldErrors();
			Map<String, Object> map = new HashMap<>();
			for(FieldError error : fieldErrors){
				map.put(error.getField(), error.getDefaultMessage());
			}
			Map<String, Object> result = new HashMap<>();
			result.put("detail", map);
//				List<ObjectError> errorsList = ex.getBindingResult().getAllErrors();
//				StringBuilder sb = new StringBuilder();
//				errorsList.forEach(error -> sb.append(error.getDefaultMessage()).append("\n"));
			writer.write(JSON.toJSONString(this.execCheckError(result,"所接收的参数错误！")));
			writer.flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}

