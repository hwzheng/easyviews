package com.greattimes.ev.task.alarm.send;

import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.message.CMBC.ShortMessageOfCMBC;
import com.greattimes.ev.message.PAB.ShortMessageOfPA;
import com.greattimes.ev.message.guangfa.WeChatMessageOfGF;
import com.greattimes.ev.message.liaoning.ShortMessageOfLN;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.service.IDataManageService;
import com.greattimes.ev.utils.SpringUtils;
import com.greattimes.ev.utils.StaticUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;

import java.util.Date;
import java.util.Map;


/**
 * 告警短信发送任务
 * @author NJ
 * @date 2018/8/15 11:10
 */
@DisallowConcurrentExecution
public class AlarmMessageSendJob implements Job {

    Logger log = LoggerFactory.getLogger(this.getClass());
    /**
     * 告警发送模式  1 平安http模式  2.辽宁socket模式 3.民生 socket短连接模式
     */
    private static final String ALARM_SEND_PATTERN = "alarm_send_pattern";

    public static Map<String,Integer> taskIdMap = null;

    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageSendJob";

    @Override
    public void execute(JobExecutionContext context) {
        Date execTime = new Date();
        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(execTime);
        taskLogs.setStat(2);
        taskLogs.setClassName(className);
        ConfigurationCache configurationCache  = SpringUtils.getBean("redisConfiguration");
        try {
            //获得告警发送模式
            String alarmSendPattern = configurationCache.getValue(ALARM_SEND_PATTERN);
            log.info("告警发送模式："+alarmSendPattern);
            if("1".equals(alarmSendPattern)){
                //平安http模式 获取benan 执行build方法
                ShortMessageOfPA shortMessageOfPA = SpringUtils.getBean("shortMessageOfPA");
                shortMessageOfPA.build();
            }else if("2".equals(alarmSendPattern)){
                //辽宁socket模式
                ShortMessageOfLN shortMessageOfLN = SpringUtils.getBean("shortMessageOfLN");
                shortMessageOfLN.build();
            }else if("3".equals(alarmSendPattern)){
                //民生socket模式
                ShortMessageOfCMBC shortMessageOfCMBC = SpringUtils.getBean("shortMessageOfCMBC");
                shortMessageOfCMBC.build();
            }else if("4".equals(alarmSendPattern)){
                //广发微信告警
                WeChatMessageOfGF weChatMessageOfGF = SpringUtils.getBean("weChatMessageOfGF");
                weChatMessageOfGF.build();
            }
        } catch (Exception e) {
            IDataManageService dataManageService = SpringUtils.getBean("dataManageService");
            if(taskIdMap == null || taskIdMap.size()==0){
                //初始化timerIdMap
                taskIdMap = dataManageService.selectTaskIdMap();
            }
            Integer taskId = taskIdMap.get(taskLogs.getClassName());
            taskLogs.setTaskId(taskId);
            e.printStackTrace();
            //失败(所有都失败)
            taskLogs.setStat(3);
            taskLogs.setDesc("告警短信任务执行失败");
            taskLogs.setDuration(((Long)(System.currentTimeMillis()- execTime.getTime())).intValue());
            taskLogs.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }
        log.info("短信告警任务执行完成，花费时间：{}ms",System.currentTimeMillis()-execTime.getTime());
    }
}
