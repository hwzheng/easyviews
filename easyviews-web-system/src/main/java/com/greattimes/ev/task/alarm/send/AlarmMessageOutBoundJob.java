package com.greattimes.ev.task.alarm.send;

import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.message.PAB.OutBoundMessageOfPA;
import com.greattimes.ev.system.entity.TaskLogs;
import com.greattimes.ev.system.service.IDataManageService;
import com.greattimes.ev.utils.SpringUtils;
import com.greattimes.ev.utils.StaticUtil;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * 外呼告警(目前只有平安)
 * @author NJ
 * @date 2019/3/25 14:44
 */
@DisallowConcurrentExecution
public class AlarmMessageOutBoundJob implements Job{

    Logger log = LoggerFactory.getLogger(this.getClass());
    private static String className = "com.greattimes.ev.task.alarm.send.AlarmMessageOutBoundJob";
    public static Map<String,Integer> taskIdMap = null;
    /**
     * 告警发送模式  1 平安http模式  2.辽宁socket模式
     */
    private static final String ALARM_SEND_PATTERN = "alarm_send_pattern";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("平安外呼短信告警定时任务开始,时间:{}", DateUtils.currentDatetime());
        Date execTime = new Date();
        TaskLogs taskLogs = new TaskLogs();
        // stat 执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败
        taskLogs.setStarttimes(execTime);
        taskLogs.setStat(2);
        taskLogs.setClassName(className);
        ConfigurationCache configurationCache  = SpringUtils.getBean("redisConfiguration");

        //获得告警发送模式
        String alarmSendPattern = configurationCache.getValue(ALARM_SEND_PATTERN);
        log.info("告警发送模式："+ alarmSendPattern);
        try {
            //平安http模式
            if("1".equals(alarmSendPattern)){
                OutBoundMessageOfPA outBoundMessageOfPA = SpringUtils.getBean("outBoundMessageOfPA");
                outBoundMessageOfPA.build();
            }
        } catch (Exception e) {
            e.printStackTrace();
            IDataManageService dataManageService = SpringUtils.getBean("dataManageService");
            if(taskIdMap == null || taskIdMap.size()==0){
                //初始化timerIdMap
                taskIdMap = dataManageService.selectTaskIdMap();
            }
            Integer taskId = taskIdMap.get(taskLogs.getClassName());
            taskLogs.setTaskId(taskId);
            e.printStackTrace();
            //失败(所有都失败)
            taskLogs.setStat(3);
            taskLogs.setDesc("外呼告警短信任务执行失败");
            taskLogs.setDuration(((Long)(System.currentTimeMillis()- execTime.getTime())).intValue());
            taskLogs.setDetail((e.getMessage()!=null && e.getMessage().length()>500)? e.getMessage().substring(0, 500):e.getMessage());
            StaticUtil.taskLogsQueue.add(taskLogs);
        }
        log.info("外呼短信告警任务执行完成，花费时间：{}ms",System.currentTimeMillis()-execTime.getTime());
    }
}
