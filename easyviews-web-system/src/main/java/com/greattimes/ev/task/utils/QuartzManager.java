package com.greattimes.ev.task.utils;

import com.greattimes.ev.system.entity.Task;
import org.apache.commons.lang3.StringUtils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.util.Map;

/**
 * quartz 管理
 * <可动态添加 删除 修改定时任务>
 * 
 * */

public class QuartzManager {

	private final static String JOBGROUP="job_group";
	private final static String TRIGGERGROUP="trigger_group";
	private final static String TIGGERNAME_PEIFIX="trigger_";

	private static SchedulerFactory  scheduler;
	static {
		if(scheduler==null){
			scheduler=new  StdSchedulerFactory();
		}
	}
	public final static String PARAMS_KEY="params";
	
	
	/**
	 * <添加定时任务>
	 * @param name 任务名
	 * @param groupName 任务组名
	 * @param triggerName 触发器名
	 * @param triggerGroupName 触发器组名
	 * @param jobClass 任务类
	 * @param cron	定时任务时间参数
	 */
	public static void addJob(String name,String groupName,String triggerName ,
			String triggerGroupName ,@SuppressWarnings("rawtypes") Class jobClass, String cron ){
		try {
			Scheduler sc=scheduler.getScheduler();
			@SuppressWarnings("unchecked")
			JobDetail detail=JobBuilder.newJob(jobClass).withIdentity(name, groupName).build();  // 任务名，任务组，任务执行类
			TriggerBuilder<Trigger> trigger=TriggerBuilder.newTrigger();
			trigger.withIdentity(triggerName, triggerGroupName);
			trigger.startNow();
			trigger.withSchedule(CronScheduleBuilder.cronSchedule(cron)); // 触发器时间设定  
			CronTrigger cronTrigger=(CronTrigger) trigger.build(); // 创建Trigger对象
			sc.scheduleJob(detail,cronTrigger);   // 调度容器设置JobDetail和Trigger
			if (!sc.isShutdown()) {  
               sc.start();  
            }  
		} catch (Exception e) {
			// TODO: handle exception
			throw  new RuntimeException(e);
		}
	}
	
	/**
	 * <添加带初始化参数的定时任务>
	 * @param name 任务名
	 * @param groupName 任务组名
	 * @param triggerName 触发器名
	 * @param triggerGroupName 触发器组名
	 * @param jobClass 任务类
	 * @param cron	定时任务时间参数
	 * @param params 自定义参数
	 */
	public static void addJobWithParams(String name,String groupName,String triggerName ,
			String triggerGroupName ,@SuppressWarnings("rawtypes") Class jobClass, String cron,Map<String, Object> params){
		try {
			Scheduler sc=scheduler.getScheduler();
			@SuppressWarnings("unchecked")
			JobDetail detail=JobBuilder.newJob(jobClass).withIdentity(name, groupName).build();  // 任务名，任务组，任务执行类
			detail.getJobDataMap().put(PARAMS_KEY, params);
			TriggerBuilder<Trigger> trigger=TriggerBuilder.newTrigger();
			trigger.withIdentity(triggerName, triggerGroupName);
			trigger.startNow();
			trigger.withSchedule(CronScheduleBuilder.cronSchedule(cron)); // 触发器时间设定  
			CronTrigger cronTrigger=(CronTrigger) trigger.build(); // 创建Trigger对象
			sc.scheduleJob(detail,cronTrigger);   // 调度容器设置JobDetail和Trigger
			if (!sc.isShutdown()) {  
               sc.start();  
            }  
		} catch (Exception e) {
			// TODO: handle exception
			throw  new RuntimeException(e);
		}
	}
	
	
	/**
	 * <添加带初始化参数的定时任务>
	 * @param name 任务名
	 * @param groupName 任务组名
	 * @param triggerName 触发器名
	 * @param triggerGroupName 触发器组名
	 * @param jobClass 任务类
	 * @param cron	定时任务时间参数
	 * @param params 自定义参数
	 */
	public static void addJobWithParams(String name,String groupName,String triggerName ,
			String triggerGroupName ,@SuppressWarnings("rawtypes") Class jobClass, String cron,String params){
		try {
			Scheduler sc=scheduler.getScheduler();
			@SuppressWarnings("unchecked")
			JobDetail detail=JobBuilder.newJob(jobClass).withIdentity(name, groupName).build();  // 任务名，任务组，任务执行类
			detail.getJobDataMap().put(PARAMS_KEY, params);
			TriggerBuilder<Trigger> trigger=TriggerBuilder.newTrigger();
			trigger.withIdentity(triggerName, triggerGroupName);
			trigger.startNow();
			trigger.withSchedule(CronScheduleBuilder.cronSchedule(cron)); // 触发器时间设定  
			CronTrigger cronTrigger=(CronTrigger) trigger.build(); // 创建Trigger对象
			sc.scheduleJob(detail,cronTrigger);   // 调度容器设置JobDetail和Trigger
			if (!sc.isShutdown()) {  
               sc.start();  
            }  
		} catch (Exception e) {
			// TODO: handle exception
			throw  new RuntimeException(e);
		}
	}
	
	 /** 
     * @Description: 修改一个任务的触发时间
     * @param jobName 
     * @param jobGroupName
     * @param triggerName 触发器名
     * @param triggerGroupName 触发器组名 
     * @param cron   时间设置，参考quartz说明文档   
     */  
    public static void modifyJobTime(String jobName, 
            String jobGroupName, String triggerName, String triggerGroupName, String cron) {  
        try {  
            Scheduler sched = scheduler.getScheduler();  
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
            CronTrigger trigger = (CronTrigger) sched.getTrigger(triggerKey);  
            if (trigger == null) {  
                return;  
            }  
            String oldTime = trigger.getCronExpression();  
            if (!oldTime.equalsIgnoreCase(cron)) { 
                /** 方式一 ：调用 rescheduleJob 开始 */
                // 触发器  
                TriggerBuilder<Trigger> triggerBuilder = TriggerBuilder.newTrigger();
                // 触发器名,触发器组  
                triggerBuilder.withIdentity(triggerName, triggerGroupName);
                triggerBuilder.startNow();
                // 触发器时间设定  
                triggerBuilder.withSchedule(CronScheduleBuilder.cronSchedule(cron));   
                // 创建Trigger对象
                trigger = (CronTrigger) triggerBuilder.build();
                // 方式一 ：修改一个任务的触发时间
                sched.rescheduleJob(triggerKey, trigger);
            }  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }  
    
    /** 
     * @Description: 移除一个任务 
     *  
     * @param jobName 
     * @param jobGroupName 
     * @param triggerName 
     * @param triggerGroupName 
     */  
    public static void removeJob(String jobName, String jobGroupName,  
    		String triggerName, String triggerGroupName) {  
        try {  
            Scheduler sched = scheduler.getScheduler();  
            TriggerKey triggerKey = TriggerKey.triggerKey(triggerName, triggerGroupName);
            sched.pauseTrigger(triggerKey);// 停止触发器  
            sched.unscheduleJob(triggerKey);// 移除触发器  
            sched.deleteJob(JobKey.jobKey(jobName, jobGroupName));// 删除任务  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }  
    
    /** 
     * @Description:启动所有定时任务 
     */  
    public static void startJobs() {  
        try {  
            Scheduler sched = scheduler.getScheduler();  
            sched.start();  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }  

    /** 
     * @Description:关闭所有定时任务 
     */  
    public static void shutdownJobs() {  
        try {  
            Scheduler sched = scheduler.getScheduler();  
            if (!sched.isShutdown()) {  
                sched.shutdown();  
            }  
        } catch (Exception e) {  
            throw new RuntimeException(e);  
        }  
    }

	public static void addJob(Task task) throws ClassNotFoundException {
		String param=task.getParam();
		@SuppressWarnings("rawtypes")
		Class clazz=Class.forName(task.getClassName());
		if(StringUtils.isBlank(param)){
			QuartzManager.addJob(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP, clazz, task.getCron());
		}else{
			QuartzManager.addJobWithParams(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP, clazz, task.getCron(), task.getParam());
		}
	}
	public static void addJobWithParams(Task task, Map<String, Object> params)throws ClassNotFoundException{
		@SuppressWarnings("rawtypes")
		Class clazz = Class.forName(task.getClassName());
		QuartzManager.addJobWithParams(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP, clazz, task.getCron(), params);
	}
	public static void modifyJobTime(Task task) {
		QuartzManager.modifyJobTime(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP, task.getCron());
	}
	public static void removeJob(Task task) {
		QuartzManager.removeJob(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP);
	}
}
