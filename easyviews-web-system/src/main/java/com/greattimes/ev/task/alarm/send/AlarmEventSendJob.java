package com.greattimes.ev.task.alarm.send;

import com.greattimes.ev.message.PAB.EventMessageOfPA;
import com.greattimes.ev.utils.SpringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 告警的的事件处理
 * @author NJ
 * @date 2019/9/23 14:13
 */
@DisallowConcurrentExecution
public class AlarmEventSendJob implements Job{

    Logger log = LoggerFactory.getLogger(this.getClass());

//    private static String CLASSNAME = "com.greattimes.ev.task.alarm.send.AlarmEventSendJob";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        long start = System.currentTimeMillis();
        try {
            EventMessageOfPA eventMessageOfPA = SpringUtils.getBean("eventMessageOfPA");
            eventMessageOfPA.sendMessage();
            log.info("平安事件告警推送通知耗时：{} ms", (System.currentTimeMillis() - start)/1000);
        }catch (Exception e) {
            log.info("平安事件告警推送通知处理失败！原因：{}", e.getMessage());
        }
    }
}
