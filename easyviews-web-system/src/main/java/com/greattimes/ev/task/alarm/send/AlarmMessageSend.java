package com.greattimes.ev.task.alarm.send;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageSendService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.system.param.req.JsonConfigParam;
import com.greattimes.ev.system.service.IJsonConfigService;
import com.greattimes.ev.utils.SpringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 * 告警信息推送kafka
 * @author CGC
 * @date 2019年11月20日 19:19:08
 */
//@DisallowConcurrentExecution
public class AlarmMessageSend implements Job{

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("告警信息推送定时任务开始时间:{}", DateUtils.currentDatetime());
        ConfigurationCache configurationCache  = SpringUtils.getBean("redisConfiguration");
        IAlarmMassageSendService alarmSendService = SpringUtils.getBean("alarmMassageSendService");
        IJsonConfigService jsonConfigService = SpringUtils.getBean("jsonConfigService");
        //获取json配置信息
        JsonConfigParam jsonparam=jsonConfigService.getJsonDetail(null,"EVENT_ALARM_PARAM");
        if(null==jsonparam) {
        	return;
        }
    	JSONArray appIds = JSONObject.parseObject(jsonparam.getParam()).getJSONArray("appIds");
    	if(null==appIds) {
    		return;
    	}
        //获取定时任务参数
        //JobDataMap jdMap=context.getJobDetail().getJobDataMap();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("alarm_kafka_topic", configurationCache.getDefaultValue(Constant.PINGAN_ALARM_KAFKA_TOPIC));
        jsonObject.put("alarm_kafka_batch_size", configurationCache.getDefaultValue(Constant.PINGAN_ALARM_KAFKA_BATCH_SIZE));
        jsonObject.put("appIds", appIds);
        //jsonObject.put("quartzparam", jdMap.get("params"));
        alarmSendService.messageSend(jsonObject);
        stopWatch.stop();
        log.info("告警信息推送定时任务执行耗时(ms):{}", stopWatch.getTotalTimeMillis());

    }
}
