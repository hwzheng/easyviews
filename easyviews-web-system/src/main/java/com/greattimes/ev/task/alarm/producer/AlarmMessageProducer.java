package com.greattimes.ev.task.alarm.producer;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageProducerService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.utils.SpringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

/**
 * @author NJ
 * @date 2019/1/7 17:20
 */
//@DisallowConcurrentExecution
public class AlarmMessageProducer implements Job{

    Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("消息生产定时任务开始时间:{}", DateUtils.currentDatetime());
        ConfigurationCache configurationCache  = SpringUtils.getBean("redisConfiguration");
        IAlarmMassageProducerService iAlarmMassageProducerService = SpringUtils.getBean("alarmMassageProducerService");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("bpmInterval", configurationCache.getValue(Constant.BPM_INTERVAL));
        jsonObject.put("alarmDelay", configurationCache.getValue(Constant.ALARM_DELAY));
        jsonObject.put("sendInterval", configurationCache.getValue(Constant.ALARM_SEND_INTERVAL_THRESHOLD));
        iAlarmMassageProducerService.messageProducer(jsonObject);
        stopWatch.stop();
        log.info("消息生产定时任务执行耗时(ms):{}", stopWatch.getTotalTimeMillis());

    }
}
