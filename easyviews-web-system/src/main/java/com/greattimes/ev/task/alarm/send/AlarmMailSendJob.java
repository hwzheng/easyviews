package com.greattimes.ev.task.alarm.send;

import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.email.MailSend;
import com.greattimes.ev.utils.SpringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;


/**
 * @author NJ
 * @date 2018/8/15 11:10
 */
@DisallowConcurrentExecution
public class AlarmMailSendJob implements Job {

    org.slf4j.Logger log = LoggerFactory.getLogger(AlarmMailSendJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("邮件定时任务开始,时间:{}", DateUtils.currentDatetime());
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        MailSend mailSend = SpringUtils.getBean("mailSend");
        mailSend.sendMail();
        stopWatch.stop();
        log.info("邮件定时任务执行耗时(ms):{}", stopWatch.getTotalTimeMillis());
    }

}
