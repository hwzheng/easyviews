package com.greattimes.ev.task.init;

import com.greattimes.ev.system.entity.Task;
import com.greattimes.ev.system.service.ITaskService;
import com.greattimes.ev.task.utils.QuartzManager;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * @author NJ
 * @date 2018/8/20 17:01
 */
@Component
public class TaskInit {

    public final static String JOBGROUP="job_group";
    public final static String TRIGGERGROUP="trigger_group";
    public final static String TIGGERNAME_PEIFIX="trigger_";

    @Autowired
    private ITaskService iTaskService;

    Logger log = LoggerFactory.getLogger(this.getClass());

    @PostConstruct
    public void initTask(){
            log.info("init timer task start!");
            List<Task> tasks = iTaskService.selectByMap(null);
            for (Task task : tasks) {
                if(task.getActive()==0){
                    log.warn("timer is not activated："+task.getClassName());
                    continue;
                }
                Class c = null;
                try {
                    c = Class.forName(task.getClassName());
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    log.error("timer class not find："+task.getClassName());
                    e.printStackTrace();
                }
                String param=task.getParam();
                if(StringUtils.isBlank(param)){
                    QuartzManager.addJob(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP, c, task.getCron());
                }else{
                    QuartzManager.addJobWithParams(task.getName(), JOBGROUP, TIGGERNAME_PEIFIX+task.getId(), TRIGGERGROUP, c, task.getCron(), task.getParam());
                }
            }
            log.info("init timer task finished!");
    }
}
