package com.greattimes.ev.system.controller;

import com.greattimes.ev.base.Constant;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.utils.SpringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.common.model.JsonResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/api")
@RestController
public class TestController extends BaseController{

	Logger log = LoggerFactory.getLogger(this.getClass());
	
	@RequestMapping(value="/test",method=RequestMethod.POST)
	public JsonResult<Object> test() {
		return execError("测试", "ssssssssssss");
	}
	
	@RequestMapping(value="/test2",method=RequestMethod.GET)
	public JsonResult<Object> test2() {
		return execError("测试", "ssssssssssss");
	}

	@RequestMapping(value="/test3",method=RequestMethod.GET)
	public JsonResult<Object> testRedis(){
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		ConfigurationCache configurationCache  = SpringUtils.getBean("redisConfiguration");
		String interval = configurationCache.getValue("bpm_interval");
		String alarm_kafka_topic = configurationCache.getDefaultValue("alarm_kafka_topic");

//		System.out.println("###===interval:" + interval +"==alarmTopic:"+ alarm_kafka_topic + (!"5".equals(interval) ||
//				!"dmv-alert-easyviews".equals(alarm_kafka_topic) ? "---false" : ""));

		System.out.println( (!"5".equals(interval) || !"dmv-alert-easyviews".equals(alarm_kafka_topic) ? "######---false" : ""));

		stopWatch.stop();
		log.info("线程id:{},耗时:{}", Thread.currentThread().getId(), stopWatch.getLastTaskTimeMillis());
		return execSuccess("成功！");

	}

	public static void main(String[] args) {

		List<Integer> ids = new ArrayList<>();
		for (int i = 1; i <= 7; i++) {
			ids.add(i);
		}
		List<Integer> appIds = new ArrayList<>();
//		for (int i = 3; i <= 5 ; i++) {
//			appIds.add(i);
//		}
		System.out.println("ids:" + ids.toString());
		System.out.println("appIds:" + appIds.toString());
		int size = appIds.size();
		if(appIds.size() < 6) {
			ids.removeAll(appIds);
			appIds.addAll(ids.subList(0, 6 - size));
		}

		System.out.println(appIds.toString());
	}
}
