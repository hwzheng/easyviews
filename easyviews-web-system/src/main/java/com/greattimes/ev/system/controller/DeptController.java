package com.greattimes.ev.system.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import com.greattimes.ev.common.constants.OperaterType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Dept;
import com.greattimes.ev.system.param.req.DeptUserParam;
import com.greattimes.ev.system.service.IDeptService;
import com.greattimes.ev.system.service.IUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 用户群组表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
@RestController
@Api(value = "DeptController", tags = "用户群组管理模块")
@RequestMapping("/system/group")
@RequirePermission("group")
public class DeptController extends BaseController {

	@Autowired
	private IDeptService deptService;
	@Autowired
	private IUserService userService;
	
	/***
	 * 查询群组用户
	 * 
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "查询群组用户", notes = "查询群组用户", response = DeptUserParam.class, responseContainer = "List")
	@RequestMapping(value = "", method = RequestMethod.GET)
	public JsonResult<Object> select() {
		return execSuccess(deptService.selectDeptAndUser());
	}

	/***
	 * 用户群组查询
	 * 
	 * @return
	 */
	@ApiOperation(value = "用户群组查询", notes = "用户群组查询", response = Dept.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.GET)
	public JsonResult<Object> selectDept() {
		return execSuccess(deptService.selectDept());
	}
	/***
	 * 用户群组查询(根据type)
	 * 
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "用户群组查询", notes = "用户群组查询", response = Dept.class, responseContainer = "List")
	@RequestMapping(value = "/select/type", method = RequestMethod.POST)
	public JsonResult<Object> selectDept(@RequestBody JSONObject jsonObject) {
		Integer type=jsonObject.getInteger("type");
		return execSuccess(deptService.selectDeptByType(type));
	}

	/***
	 * 用户群组删除
	 * 
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "用户群组删除", notes = "用户群组删除")
	@OperateLog(type = OperaterType.DELETE, desc = "用户群组管理-删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteDept(HttpServletRequest req, @RequestBody Dept dept) {
		Integer id = dept.getId();
		deptService.deleteDept(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 用户群组编辑
	 * 
	 * @param req
	 * @param dept
	 * @return
	 */
	@ApiOperation(value = "用户群组编辑", notes = "用户群组编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "用户群组管理-编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> updateDept(HttpServletRequest req, @RequestBody @Validated Dept dept) {
		if(null==dept.getId()) {
			return execCheckError("用户群组id为空！");
		}
		// 校验用户名
		Boolean flag = deptService.checkName(dept, 0);
		if (flag) {
			deptService.updateDept(dept);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		} else {
			return execCheckError("用户群组名称不能重复！");
		}

	}

	/**
	 * 用户群组新增
	 * 
	 * @param req
	 * @param dept
	 * @return
	 */
	@ApiOperation(value = "用户群组新增", notes = "用户群组新增")
	@OperateLog(type = OperaterType.INSERT, desc = "用户群组管理-新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> addDept(HttpServletRequest req, @RequestBody @Validated Dept dept) {
		// 校验用户名
		Boolean flag = deptService.checkName(dept, 1);
		if (flag) {
			Integer id=deptService.addDept(dept);
			Map<String, Object> map=new HashMap<>();
			map.put("id", id);
			return execSuccess(map,Constant.SAVE_SUCCESS_MSG);
		} else {
			return execCheckError("用户群组名称不能重复！");
		}
	}

	/**
	 * 用户查询
	 * @param p
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "用户查询", notes = "用户查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "groupId", value = "用户群组id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/user/select", method = RequestMethod.POST)
	public JsonResult<Object> selectUser(@RequestBody JSONObject p) {
		Integer groupId = p.getInteger("groupId");
		return execSuccess(userService.selectUserByDept(groupId));
	}
	
	/**
	 * 用户保存
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "用户保存", notes = "用户保存")
	@ApiImplicitParams({ @ApiImplicitParam(name = "groupId", value = "用户群组id", dataType = "int", paramType = "query"), 
	@ApiImplicitParam(name="ids",value="用户id数组",dataType = "int", allowMultiple = true, paramType = "query")
	})
	@OperateLog(type = OperaterType.INSERT, desc = "用户群组管理-用户保存")
	@RequestMapping(value = "/user/save", method = RequestMethod.POST)
	public JsonResult<Object> saveUser(@RequestBody Map<String,Object> map) {
		Integer groupId = evUtil.getMapIntValue(map, "groupId");
		List<Integer> ids=(List<Integer>) map.get("ids");
		deptService.saveUser(groupId,ids);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
	
	/**
	 * 用户删除
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "用户删除", notes = "用户删除")
	@ApiImplicitParams({ @ApiImplicitParam(name = "groupId", value = "用户群组id", dataType = "int", paramType = "query"), 
	@ApiImplicitParam(name="id",value="用户id",dataType = "int", paramType = "query")
	})
	@OperateLog(type = OperaterType.DELETE, desc = "用户群组管理-用户删除")
	@RequestMapping(value = "/user/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteUser(@RequestBody Map<String,Object> map) {
		Integer groupId = evUtil.getMapIntValue(map, "groupId");
		Integer id = evUtil.getMapIntValue(map, "id");
		deptService.deleteUser(groupId,id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
}
