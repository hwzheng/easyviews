package com.greattimes.ev.system.controller;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Holiday;
import com.greattimes.ev.system.service.IHolidayService;
import com.greattimes.ev.utils.DownloadUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
/**
 * <p>
 * 节假日表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2019-09-24
 */
@RestController
@RequestMapping("/system/holiday")
@Api(value = "HolidayController", tags = "节假日管理")
@RequirePermission("holiday")
public class HolidayController extends BaseController{
	@Autowired
    private IHolidayService holidayService;
	Logger log = LoggerFactory.getLogger(this.getClass());
	@ApiOperation(value = "节假日列表", notes = "节假日列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "size", value = "每页数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "page", value = "当前页码", dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/select", method = RequestMethod.POST)
    public JsonResult<Object> selectHoliday(@RequestBody JSONObject param){
        Integer pageNumber = param.getInteger("page");
        Integer pageSize= param.getInteger("size");
        Page<Holiday> page = new Page<>(pageNumber,pageSize);
        return execSuccess(holidayService.selectHoliday(page));
    }
	
	/**
	 * 删除
	 */
	@ApiOperation(value="删除", notes="删除")
	@OperateLog(type=OperaterType.DELETE,desc="节假日删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> delete(@RequestBody Holiday holiday) {
		Integer id = holiday.getId();
		holidayService.deleteById(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
	
	/**
	 * 编辑
	 */
	@ApiOperation(value="编辑", notes="编辑")
	@OperateLog(type=OperaterType.UPDATE,desc="节假日编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> edit(@RequestBody Holiday holiday) {
		EntityWrapper<Holiday> wrapper=new EntityWrapper<>();
		wrapper.eq("date", holiday.getDate()).ne("id", holiday.getId());
		if(evUtil.listIsNullOrZero(holidayService.selectList(wrapper))) {
			holidayService.updateById(holiday);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		}else {
			return execCheckError("列表中已有该日期！");
		}
		
	}
	
	/**
	 * 模版下载
	 * @param req
	 * @param resp
	 * @param map
	 * @return
	 * @throws IOException
	 */
	@ApiOperation(value = "模版下载", notes = "模版下载")
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	public JsonResult<Object> download(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		// 第一步，创建一个webbook，对应一个Excel文件
		XSSFWorkbook wb = new XSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		XSSFSheet sheet = wb.createSheet("节假日模版");
		// 第三步，在sheet中添加表头第0行
		XSSFRow row = sheet.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		XSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(XSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式

		XSSFCell cell = row.createCell((short) 0);
		cell.setCellValue("假期名称");
		cell.setCellStyle(style);
		sheet.setColumnWidth(0,( "假期名称".getBytes().length)*256);
		cell = row.createCell((short) 1);
		cell.setCellValue("日期（yyyy/MM/dd）");
		cell.setCellStyle(style);
		sheet.setColumnWidth(1,( "日期（yyyy/MM/dd）".getBytes().length)*256);
		cell = row.createCell((short) 2);
		cell.setCellValue("工作日（0否1是）");
		cell.setCellStyle(style);
		sheet.setColumnWidth(2,( "工作日（0否1是）".getBytes().length)*256);

		// 第六步，将文件存到指定位置
		resp.setCharacterEncoding("utf-8");
		resp.setContentType("multipart/form-data");
		resp.setHeader("Content-Disposition", "attachment;fileName=holidayDetail.xlsx");
		OutputStream os = null;
		try {
			os = resp.getOutputStream();
			wb.write(os);
			// 这里主要关闭。
			if (os != null) {
				os.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (os != null) {
					os.close();
				}
				if (wb != null) {
					wb.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return execSuccess(Constant.DOWNLOAD_SUCCESS_MSG);
	}

	/**
	 * 节假日上传
	 * @param file
	 * @param req
	 * @param map
	 * @return
	 * @throws Exception
	 */
	@ApiOperation(value = "节假日上传", notes = "节假日上传")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<Object> upload(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req) throws IOException {
		List<Holiday> details = new ArrayList<>();
		String fileName = file.getOriginalFilename();
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		if (!"xlsx".equals(fieldType)) {
			return execCheckError("文件上传失败！文件类型错误");
		}
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}
		InputStream in = file.getInputStream();
		XSSFWorkbook xssfWorkbook = new XSSFWorkbook(in);
		XSSFSheet xssfSheet = xssfWorkbook.getSheetAt(0);
		if (xssfSheet == null) {
			xssfWorkbook.close();
			return execCheckError("文件解析失败！请确认是否是下载的文件类型");
		}
		for (int i = 1; i <= xssfSheet.getLastRowNum(); i++) {
			XSSFRow xssfRow = xssfSheet.getRow(i);
			if (xssfRow != null) {
				Holiday holiday=new Holiday();
				XSSFCell name = xssfRow.getCell(0);
				XSSFCell date = xssfRow.getCell(1);
				XSSFCell workday = xssfRow.getCell(2);
				String nameStr = "";
				String dateStr = "";
				int workdayInt = 0;
				
				if (name == null && date == null && workday==null) {
					continue;
				} else if (name == null) {
					xssfWorkbook.close();
					return execCheckError("第" + i + "行假期名称为空");
				} else if (date == null) {
					xssfWorkbook.close();
					return execCheckError("第" + i + "行日期为空");
				} else if (workday == null) {
					xssfWorkbook.close();
					return execCheckError("第" + i + "行工作日为空");
				}
				if (workday.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					workdayInt =(int) workday.getNumericCellValue();
				} 
				if (name.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					nameStr = name.getStringCellValue();
				} else if (name.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					nameStr = (int)name.getNumericCellValue()+"";
				}
				if (date.getCellType() == HSSFCell.CELL_TYPE_STRING) {
					dateStr = date.getStringCellValue();
				}else if (date.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
					dateStr = DateUtils.getDateStr(date.getDateCellValue());
				}
				holiday.setName(nameStr);
				holiday.setDate(dateStr);
				holiday.setWorkday(workdayInt);
				details.add(holiday);
			}
		}
		if (details.size() > 0) {
			holidayService.insert(details);
			//sys_notice
		}
		xssfWorkbook.close();
		return execSuccess(Constant.UPLOAD_SUCCESS_MSG);
	}
	/**
	 * 节假日查询
	 */
	@RequirePermission("common")
	@ApiOperation(value="节假日查询", notes="节假日查询")
	@ApiImplicitParams({
        @ApiImplicitParam(name = "date", value = "日期", dataType = "String", paramType = "query"),
        @ApiImplicitParam(name = "days", value = "查询几天", dataType = "int", paramType = "query")
	})
	@RequestMapping(value = "/getday", method = RequestMethod.POST)
	public JsonResult<Object> getday(@RequestBody Map<String, Object> param) {
		List<String> days=holidayService.getday(param);
		Map<String, Object> result=new HashMap<String, Object>();
		result.put("days", days);
		return execSuccess(result);
	}
}

