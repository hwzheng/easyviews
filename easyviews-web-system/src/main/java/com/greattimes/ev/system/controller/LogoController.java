package com.greattimes.ev.system.controller;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Logo;
import com.greattimes.ev.system.service.ILogoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * <p>
 * logo表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2019-05-06
 */
@RestController
@Api(value="LogoController",tags="logo管理")
@RequestMapping("/system/logo")
@RequirePermission("dictionary")
public class LogoController extends BaseController{
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private ILogoService logoService;
	/**
	 * logo上传
	 * @param file
	 * @param req
	 * @param resp
	 * @return
	 */
	@ApiOperation(value = "logo上传", notes = "logo上传")
	@OperateLog(type=OperaterType.INSERT,desc="logo上传")
	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public JsonResult<Object> addLogo(
			@ApiParam(required = false, value = "上传的文件") @RequestParam(value = "file", required = false) MultipartFile file,
			HttpServletRequest req,HttpServletResponse resp) {
		long len = file.getSize();//上传文件的大小, 单位为字节
		double fileSize = (double)len/(1024);//转换为G
		if (file.isEmpty()) {
			return execCheckError("文件上传失败！文件为空");
		}if(fileSize>100) {
			return execCheckError("文件上传失败！文件过大");
		}
		int type;
		String fileName = file.getOriginalFilename();
		String[] strArray = fileName.split("\\.");
		String fieldType=strArray[strArray.length -1];
		switch (fieldType) {
		case "png":
			type=0;
			break;
		case "jpg":
			type=1;
			break;
		case "svg":
			type=2;
			break;
		case "gif":
			type=3;
			break;
		default:
			return execCheckError("文件上传失败！文件类型错误");
		}
		byte[] b =null;
		try {
			InputStream jar = file.getInputStream();
			b = IOUtils.toByteArray(jar);
		} catch (IOException e) {
			log.info("文件上传异常！IOException");
			e.printStackTrace();
			return execError("文件上传异常！", e.getMessage());
		}
		Logo entity=new Logo();
		entity.setType(type);
		entity.setLogo(b);
		logoService.delete(null);
		if(logoService.insert(entity)) {
			// 将文件存到指定位置
			resp.setCharacterEncoding("utf-8");
			if(type==2) {
				resp.setContentType("image/svg+xml");
			}else {
				resp.setContentType("application/octet-stream");
			}
			resp.setHeader("Content-Disposition", "attachment;fileName=logo."+fieldType);
			OutputStream os = null;
			try {
				os = resp.getOutputStream();
				os.write(b);
				os.close();
			} catch (IOException e) {
				log.info("获取图片异常！IOException");
				e.printStackTrace();
				return execError("获取图片异常！", e.getMessage());
			}
			return execSuccess(entity);
		}
		return execCheckError(Constant.UPLOAD_ERROR_MSG);
	}
	
	/**
	 * logo上传
	 * @param file
	 * @param req
	 * @param resp
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "logo显示", notes = "logo显示")
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	public JsonResult<Object> selectLogo(HttpServletRequest req,HttpServletResponse resp) {
		String fieldType=null;
		byte[] b =null;
		Logo entity=new Logo();
		List<Logo> list=logoService.selectList(null);
		if(evUtil.listIsNullOrZero(list)) {
			return execSuccess(list);
		} else {
			entity = list.get(0);
			Integer type = entity.getType();
			b=entity.getLogo();
			switch (type) {
			case 0:
				fieldType = "png";
				break;
			case 1:
				fieldType = "jpg";
				break;
			case 2:
				fieldType = "svg";
				break;
			case 3:
				fieldType = "gif";
				break;
			default:
				break;
			}

			// 将文件存到指定位置
			resp.setCharacterEncoding("utf-8");
			if(type.equals(2)) {
				resp.setContentType("image/svg+xml");
			}else {
				resp.setContentType("application/octet-stream");
			}
			resp.setHeader("Content-Disposition", "attachment;fileName=logo." + fieldType);
			OutputStream os = null;
			try {
				os = resp.getOutputStream();
				os.write(b);
				os.close();
			} catch (IOException e) {
				log.info("获取图片异常！IOException");
				e.printStackTrace();
				return execError("获取图片异常！", e.getMessage());
			}
			return execSuccess(entity);

		}
	}
}

