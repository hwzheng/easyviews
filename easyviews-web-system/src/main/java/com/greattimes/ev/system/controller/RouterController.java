package com.greattimes.ev.system.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.service.IMenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value = "RouterController", tags = "路由获取模块")
@RequestMapping("/routers")
public class RouterController extends BaseController {

	@Autowired
	private IMenuService iMenuService;

	/**获取路由
	 * @param req
	 * @param p
	 * @return
	 */
	@ApiOperation(value = "获取路由", notes = "获取路由")
	@ApiImplicitParams({ @ApiImplicitParam(name = "ename", value = "菜单名", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型-查menu传1，查button传0", dataType = "int", paramType = "query") })
	@RequestMapping(method = RequestMethod.POST)
	public JsonResult<Object> getRouters(HttpServletRequest req, @RequestBody JSONObject p) {
		String ename = p.getString("ename");
		int isShow = p.getIntValue("type");
		User user = super.getSessionUser(req);
		return execSuccess(iMenuService.getRouters(user, ename, isShow));
	}
	
	/**获取全部路由
	 * @param req
	 * @param p
	 * @return
	 */
	@ApiOperation(value = "获取全部路由", notes = "获取全部路由")
	@RequestMapping(value="/all", method = RequestMethod.GET)
	public JsonResult<Object> getRouters(HttpServletRequest req) {
		User user = super.getSessionUser(req);
		return execSuccess(iMenuService.getAllRouters(user));
	}
	
	
}
