package com.greattimes.ev.system.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.greattimes.ev.base.Constant;
import com.greattimes.ev.system.entity.UserLogs;
import com.greattimes.ev.system.param.req.UserLogsParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.StringUtils;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.param.req.UserParam;
import com.greattimes.ev.system.service.IUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-05-15
 */
@RestController
@Api(value = "UserController", tags = "用户管理模块")
@RequestMapping("/system/user")
@RequirePermission("user")
public class UserController extends BaseController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IUserService userService;
	/**
	 * 无效的用户角色
	 */
	private static final Integer INVALID_ROLETYPE = 3;
	/***
	 * 用户列表查询
	 * 
	 * @param req
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "用户列表查询", notes = "用户列表查询")
	@RequestMapping(method = RequestMethod.GET)
	public JsonResult<Object> select() {
		return execSuccess(userService.selectUser());
	}
	
	/***
	 * 用户列表详情
	 * @param map
	 * @return
	 */
	@ApiOperation(value = "用户列表详情", notes = "用户列表详情")
	@ApiImplicitParams({ @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/detail",method = RequestMethod.POST)
	public JsonResult<Object> detail(@RequestBody Map<String, Object> map) {
		Integer id = evUtil.getMapIntValue(map, "userId");
		return execSuccess(userService.selectDetail(id));
	}
	
	/***
	 * 个人中心查询
	 * @param req
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "个人中心查询", notes = "个人中心查询")
	@RequestMapping(value = "/personal",method = RequestMethod.GET)
	public JsonResult<Object> personal(HttpServletRequest req) {
		User user = super.getSessionUser(req);
		return execSuccess(userService.selectUserPersonal(user));
	}
	/**
	 * 删除用户
	 * 
	 * @param req
	 * @param userinfo
	 * @return
	 */
	@ApiOperation(value = "删除用户", notes = "删除用户")
	@OperateLog(type = OperaterType.DELETE, desc = "系统管理-用户删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteUser(HttpServletRequest req, @RequestBody UserParam userinfo) {
		if(null==userinfo.getUserId()) {
			return execCheckError("用户id为空！");
		}
		Integer id = userinfo.getUserId();
		if (userService.isDefaultAdmin(id)) {
			return execCheckError("超级管理员不能删除");
		}
		User u=userService.selectById(id);
		User operate=getSessionUser(req);
		userService.deleteUser(id);
		UserLogs userlog=new UserLogs();
		userlog.setDesc("【"+operate.getLoginName()+"】删除用户【"+u.getLoginName()+"】成功！");
		userlog.setModule("user");
		userlog.setState(0);
		userlog.setTime(new Date(System.currentTimeMillis()));
		userlog.setType(3);
		userlog.setUser(operate.getLoginName());
		userlog.setRoleType(operate.getRole()==null?INVALID_ROLETYPE:operate.getRole().getType());
		userService.insertUserLogs(userlog);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 修改用户状态
	 * 
	 * @param req
	 * @param userinfo
	 * @return
	 */
	@ApiOperation(value = "修改用户状态", notes = "修改用户状态")
	@OperateLog(type = OperaterType.UPDATE, desc = "系统管理-用户状态修改")
	@RequestMapping(value = "/active", method = RequestMethod.POST)
	public JsonResult<Object> updateUserState(HttpServletRequest req, @RequestBody UserParam userinfo) {
		if(null==userinfo.getUserId()) {
			return execCheckError("用户id为空！");
		}
		int id = userinfo.getUserId();
		if (userService.isDefaultAdmin(id)) {
			return execCheckError("超级管理员不能更改状态");
		}
		int state = userinfo.getActive();
		userService.updateUserState(id, state);
		User u=userService.selectById(id);
		User operate=getSessionUser(req);
		UserLogs userlog=new UserLogs();
		String stateStr=state==0?"禁用":"激活";
		userlog.setDesc("【"+operate.getLoginName()+"】"+stateStr+"用户【"+u.getLoginName()+"】成功！");
		userlog.setModule("user");
		userlog.setState(0);
		userlog.setTime(new Date(System.currentTimeMillis()));
		userlog.setType(state==0?5:4);
		userlog.setUser(operate.getLoginName());
		userlog.setRoleType(operate.getRole()==null?INVALID_ROLETYPE:operate.getRole().getType());
		userService.insertUserLogs(userlog);
		return execSuccess(Constant.STATE_SUCCESS_MSG);
	}

	/**
	 * 用户新增
	 * 
	 * @param req
	 * @param user
	 * @return
	 */
	@ApiOperation(value = "用户新增", notes = "用户新增")
	@OperateLog(type = OperaterType.INSERT, desc = "系统管理-用户新增")
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public JsonResult<Object> addUser(HttpServletRequest req, @RequestBody @Validated UserParam user) {
		Integer roleId=user.getRoleId()==null?-1:user.getRoleId();
		if(roleId.equals(0)) {
			return execCheckError("不能新建超级管理员！");
		}
		// 校验用户名
		Boolean flag = userService.checkLoginName(user, 1);
		if (flag) {
			User u=getSessionUser(req);
			userService.addUser(user);
			UserLogs userlog=new UserLogs();
			userlog.setDesc("【"+u.getLoginName()+"】新增用户【"+user.getLoginName()+"】成功！");
			userlog.setModule("user");
			userlog.setState(0);
			userlog.setTime(new Date(System.currentTimeMillis()));
			userlog.setType(2);
			userlog.setUser(u.getLoginName());
			userlog.setRoleType(u.getRole()==null?INVALID_ROLETYPE:u.getRole().getType());
			userService.insertUserLogs(userlog);
			return execSuccess(Constant.SAVE_SUCCESS_MSG);
		} else {
			return execCheckError("用户名不能重复！");
		}

	}

	/**
	 * 用户编辑
	 * 
	 * @param user
	 * @return
	 */
	@ApiOperation(value = "用户编辑", notes = "用户编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "系统管理-用户编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> editUser(HttpServletRequest req,@RequestBody @Validated UserParam user) {
		if(null==user.getUserId()) {
			return execCheckError("用户id为空！");
		}
		if (userService.isDefaultAdmin(user.getUserId())) {
			return execCheckError("超级管理员不可被编辑");
		}
		Integer roleId=user.getRoleId()==null?-1:user.getRoleId();
		User u=getSessionUser(req);
		if(roleId.equals(0)) {
			return execCheckError("不能修改为超级管理员！");
		}
		//角色权限大于当前才能编辑
		Role role=userService.getUserRole(user.getUserId());
		if(null==u.getRole().getType()) {
			return execCheckError("无编辑权限！");
		}
		if(null!=role) {
			if(u.getRole().getType()>=role.getType()) {
				return execCheckError("无编辑权限！");
			}
		}
		//不能修改自己的角色
		if(u.getId().equals(user.getId())) {
			Integer userRole=role==null?null:role.getId();
			user.setRoleId(userRole);
			// 校验用户名
			Boolean flag = userService.checkLoginName(user, 0);
			if (flag) {
				userService.editUser(user);
				return execSuccess("不能修改自己的角色!");
			} else {
				return execCheckError("该用户名已被占用！");
			}
		}
		// 校验用户名
		Boolean flag = userService.checkLoginName(user, 0);
		if (flag) {
			userService.editUser(user);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		} else {
			return execCheckError("该用户名已被占用！");
		}
	}

	/**
	 * 密码修改
	 * 
	 * @param req
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "密码修改", notes = "密码修改")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "用户id", dataType = "int", paramType = "query"),
			@ApiImplicitParam(name = "oldPass", value = "原密码", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "newPass", value = "新密码", dataType = "String", paramType = "query"), 
			@ApiImplicitParam(name = "loginName", value = "用户名", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "name", value = "用户姓名", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "phone", value = "手机号码", dataType = "String", paramType = "query"),
			@ApiImplicitParam(name = "email", value = "联系邮箱", dataType = "String", paramType = "query")})
	@OperateLog(type = OperaterType.UPDATE, desc = "系统管理-密码修改")
	@RequestMapping(value = "/password/edit", method = RequestMethod.POST)
	public JsonResult<Object> editPassword(HttpServletRequest req,@RequestBody JSONObject param) {
		Integer id = param.getIntValue("id");
		String oldPass = param.getString("oldPass");
		String newPass = param.getString("newPass");
		String loginName = param.getString("loginName");
		String name = param.getString("name");
		String phone = param.getString("phone");
		String email = param.getString("email");
		if (!StringUtils.isBlank(newPass) && !StringUtils.isBlank(oldPass)) {
			Boolean flag = userService.selectUser(id, oldPass);
			if (flag) {
				Map<String, Object> map = new HashMap<>();
				map.put("email", email);
				map.put("phone", phone);
				map.put("name", name);
				map.put("loginName", loginName);
				map.put("id", id);
				map.put("password", newPass);
				userService.updatePassword(map);
				User u=getSessionUser(req);
				UserLogs userlog=new UserLogs();
				userlog.setDesc("用户【"+u.getLoginName()+"】密码修改成功！");
				userlog.setModule("ownspace");
				userlog.setUser(loginName);
				userlog.setState(0);
				userlog.setTime(new Date(System.currentTimeMillis()));
				userlog.setType(6);
				userlog.setUser(u.getLoginName());
				userlog.setRoleType(u.getRole()==null?INVALID_ROLETYPE:u.getRole().getType());
				userService.insertUserLogs(userlog);
				return execSuccess(Constant.EDIT_SUCCESS_MSG);
			} else {
				return execCheckError("原密码错误！");
			}
		}else {
			UserParam user=new UserParam();
			user.setLoginName(loginName);
			user.setUserId(id);
			// 校验用户名
			Boolean flag = userService.checkLoginName(user, 0);
			if (flag) {
				User entity=new User();
				entity.setLoginName(loginName);
				entity.setId(id);
				entity.setEmail(email);
				entity.setPhone(Long.parseLong(phone));
				entity.setName(name);
				userService.updateById(entity);
				return execSuccess(Constant.EDIT_SUCCESS_MSG);
			} else {
				return execCheckError("该用户名已被占用！");
			}
		}
	}

	/**
	 * 密码重置
	 * 
	 * @param req
	 * @return
	 */
	@ApiOperation(value = "密码重置", notes = "密码重置")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "用户id", dataType = "int", paramType = "query") })
	@OperateLog(type = OperaterType.UPDATE, desc = "系统管理-密码重置")
	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public JsonResult<Object> resetPassword(HttpServletRequest req,@RequestBody JSONObject param) {
		Integer id = param.getIntValue("id");
		String password=param.getString("password");
		userService.updatePassword(id,password);
		User u=userService.selectById(id);
		User operate=getSessionUser(req);
		UserLogs userlog=new UserLogs();
		userlog.setDesc("【"+operate.getLoginName()+"】重置【"+u.getLoginName()+"】密码成功！");
		userlog.setModule("user");
		userlog.setState(0);
		userlog.setTime(new Date(System.currentTimeMillis()));
		userlog.setType(7);
		userlog.setUser(operate.getLoginName());
		userlog.setRoleType(operate.getRole()==null?INVALID_ROLETYPE:operate.getRole().getType());
		userService.insertUserLogs(userlog);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}
	
	/**
	 * 同组用户查询
	 * @param p
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "同组用户查询", notes = "同组用户查询")
	@ApiImplicitParams({ @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/group/select", method = RequestMethod.POST)
	public JsonResult<Object> selectUser(@RequestBody JSONObject p) {
		Integer userId = p.getInteger("userId");
		return execSuccess(userService.selectGroupUser(userId));
	}

	/**
	 * 用户操作日志查询
	 * @param param
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "用户操作日志查询", notes = "用户操作日志查询")
	@RequestMapping(value = "/logs", method = RequestMethod.POST)
	public JsonResult<Object> selectUserLogs(HttpServletRequest req,@RequestBody UserLogsParam param) {
		User u=getSessionUser(req);
		param.setRoleType(u.getRole().getType());
		return execSuccess(userService.selectUserLogs(param));
	}
}
