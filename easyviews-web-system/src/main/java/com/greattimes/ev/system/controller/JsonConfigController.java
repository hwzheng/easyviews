package com.greattimes.ev.system.controller;


import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.JsonConfig;
import com.greattimes.ev.system.param.req.JsonConfigParam;
import com.greattimes.ev.system.service.IJsonConfigService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 系统 json参数配置表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2019-03-05
 */
@RestController
@Api(value = "JsonConfigController", tags = "json管理模块")
@RequestMapping("/system/json/config")
@RequirePermission("json")
public class JsonConfigController extends BaseController  {
	
	@Autowired
	private IJsonConfigService jsonConfigService;
	
	/***
	 * 查询
	 * 
	 * @return
	 */
	@ApiOperation(value = "查询", notes = "查询", response = JsonConfig.class, responseContainer = "List")
	@RequestMapping(value = "/type/select", method = RequestMethod.GET)
	public JsonResult<Object> select() {
		EntityWrapper<JsonConfig> wrapper=new EntityWrapper<>();
		wrapper.orderBy("id");
		return execSuccess(jsonConfigService.selectList(wrapper));
	}
	
	/***
	 * 查询
	 * @param param
	 * @return
	 */
	@RequirePermission("common")
	@ApiOperation(value = "查询", notes = "查询", response = JsonConfigParam.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> selectOne(@RequestBody JsonConfig param) {
		Integer id=param.getId();
		String code=param.getCode();
		JsonConfigParam result=jsonConfigService.getJsonDetail(id,code);
		return execSuccess(result);
	}
	
	/**
	 * json类型新增
	 * 
	 * @param req
	 * @param param
	 * @return
	 */
	@RequirePermission("json")
	@ApiOperation(value = "json类型新增", notes = "json类型新增")
	@OperateLog(type = OperaterType.INSERT, desc = "json类型新增")
	@RequestMapping(value = "/type/save", method = RequestMethod.POST)
	public JsonResult<Object> saveType(HttpServletRequest req,@RequestBody @Validated JsonConfig param) {
		Integer userId=getSessionUser(req).getId();
		boolean flag = jsonConfigService.checkType(param, 1);
		boolean nameFlag = jsonConfigService.checkName(param, 1);
		if (flag && nameFlag) {
			Integer id = jsonConfigService.saveJsonType(param,userId);
			return execSuccess(id, Constant.SAVE_SUCCESS_MSG);
		}
		if (!nameFlag&&!flag) {
			return execCheckError("重复的类型值和类型名称");
		}
		if (flag == false) {
			return execCheckError("重复的类型值");
		}
		 else {
			return execCheckError("重复的类型名称");
		}
	}
	
	/**
	 * json类型编辑
	 * @param req
	 * @param param
	 * @return
	 */
	@RequirePermission("json")
	@ApiOperation(value = "json类型编辑", notes = "json类型编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "json类型编辑")
	@RequestMapping(value = "/type/edit", method = RequestMethod.POST)
	public JsonResult<Object> editType(HttpServletRequest req,@RequestBody @Validated JsonConfig param) {
		if(null==param.getId()) {
			return execCheckError("id为空！");
		}
		Integer userId=getSessionUser(req).getId();
		boolean flag = jsonConfigService.checkType(param, 0);
		boolean nameFlag = jsonConfigService.checkName(param, 0);
		if (flag && nameFlag) {
			jsonConfigService.editJsonType(param,userId);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		}
		if (!nameFlag&&!flag) {
			return execCheckError("重复的类型值和类型名称");
		}
		if (flag == false) {
			return execCheckError("重复的类型值");
		}
		 else {
			return execCheckError("重复的类型名称");
		}
	}
	
	/**
	 * json类型删除
	 * 
	 * @param param
	 * @return
	 */
	@RequirePermission("json")
	@ApiOperation(value = "json类型删除", notes = "json类型删除")
	@OperateLog(type = OperaterType.DELETE, desc = "json类型删除")
	@RequestMapping(value = "/type/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteType(@RequestBody JsonConfig param) {
		if(null==param.getId()) {
			return execCheckError("id为空！");
		}
		jsonConfigService.deleteById(param.getId());
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}
	
	/**
	 * json保存
	 * 
	/**
	 * @param param
	 * @return
	 */
	@RequirePermission("json")
	@ApiOperation(value = "json保存", notes = "json保存")
	@OperateLog(type = OperaterType.INSERT, desc = "json保存")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> save(HttpServletRequest req,@RequestBody JsonConfig param) {
		if(null==param.getId()) {
			return execCheckError("id为空！");
		}
		Integer userId=getSessionUser(req).getId();
		param.setUpdateBy(userId);
		param.setUpdateTime(new Date());
		jsonConfigService.updateById(param);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}
}

