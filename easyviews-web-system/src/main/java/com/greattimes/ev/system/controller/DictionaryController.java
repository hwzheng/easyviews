package com.greattimes.ev.system.controller;

import com.greattimes.ev.base.Constant;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Dictionary;
import com.greattimes.ev.system.entity.DictionaryType;
import com.greattimes.ev.system.service.IDictionaryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * <p>
 * 系统字典表 前端控制器
 * </p>
 *
 * @author cgc
 * @since 2018-07-17
 */
@RestController
@RequestMapping("/system/dictionary")
@Api(value = "DictionaryController", tags = "字典管理")
@RequirePermission("dictionary")
public class DictionaryController extends BaseController {
	@Autowired
	private IDictionaryService dictionaryService;

	/**
	 * 查询所有dictionaryType数据
	 * 
	 * @return
	 */
	@ApiOperation(value = "字典组查询", notes = "字典组查询", response = DictionaryType.class, responseContainer = "List")
	@RequestMapping(value = "/dictionarytype/select", method = RequestMethod.GET)
	public JsonResult<Object> getDictionaryType() {
		return execSuccess(dictionaryService.getDictionaryType());
	}

	/**
	 * 字典类型新增
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典类型新增", notes = "字典类型新增")
	@OperateLog(type = OperaterType.INSERT, desc = "字典类型新增")
	@RequestMapping(value = "/dictionarytype/save", method = RequestMethod.POST)
	public JsonResult<Object> saveDictionaryType(@RequestBody @Validated DictionaryType dictionaryType) {
		boolean flag = dictionaryService.checkType(dictionaryType, 1);
		boolean nameFlag = dictionaryService.checkName(dictionaryType, 1);
		if (flag && nameFlag) {
			Integer id = dictionaryService.saveDictionaryType(dictionaryType);
			return execSuccess(id, Constant.SAVE_SUCCESS_MSG);
		}
		if (!nameFlag&&!flag) {
			return execCheckError("重复的类型值和类型名称");
		}
		if (flag == false) {
			return execCheckError("重复的类型值");
		}
		 else {
			return execCheckError("重复的类型名称");
		}
	}

	/**
	 * 字典类型编辑
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典类型编辑", notes = "字典类型编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "字典类型编辑")
	@RequestMapping(value = "/dictionarytype/edit", method = RequestMethod.POST)
	public JsonResult<Object> editDictionaryType(@RequestBody @Validated DictionaryType dictionaryType) {
		if(null==dictionaryType.getId()) {
			return execCheckError("字典类型id为空！");
		}
		boolean flag = dictionaryService.checkType(dictionaryType, 0);
		boolean nameFlag = dictionaryService.checkName(dictionaryType, 0);
		if (flag && nameFlag) {
			dictionaryService.editDictionaryType(dictionaryType);
			return execSuccess(Constant.EDIT_SUCCESS_MSG);
		}
		if (!nameFlag&&!flag) {
			return execCheckError("重复的类型值和类型名称");
		}
		if (flag == false) {
			return execCheckError("重复的类型值");
		}
		 else {
			return execCheckError("重复的类型名称");
		}
	}

	/**
	 * 字典类型删除
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典类型删除", notes = "字典类型删除")
	@OperateLog(type = OperaterType.DELETE, desc = "字典类型删除")
	@RequestMapping(value = "/dictionarytype/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteDictionaryType(@RequestBody DictionaryType dictionaryType) {
		if(null==dictionaryType.getId()) {
			return execCheckError("字典类型id为空！");
		}
		Integer id = dictionaryType.getId();
		dictionaryService.deleteDictionaryType(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 查询字典数据
	 * 
	 * @return
	 */
	@ApiOperation(value = "字典查询", notes = "字典查询", response = Dictionary.class, responseContainer = "List")
	@RequestMapping(value = "/select", method = RequestMethod.POST)
	public JsonResult<Object> getDictionary(@RequestBody DictionaryType dictionaryType) {
		Integer id = dictionaryType.getId();
		return execSuccess(dictionaryService.getDictionary(id));
	}
	
	/**
	 * 查询字典详情
	 * 
	 * @return
	 */
	@ApiOperation(value = "字典详情", notes = "字典详情", response = Dictionary.class, responseContainer = "List")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "字典id", dataType = "int", paramType = "query") })
	@RequestMapping(value = "/detail", method = RequestMethod.POST)
	public JsonResult<Object> getDictionaryDetail(@RequestBody Map<String, Object> map) {
		Integer id = evUtil.getMapIntValue(map, "id");
		return execSuccess(dictionaryService.getDictionaryDetail(id));
	}

	/**
	 * 字典新增
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典新增", notes = "字典新增")
	@OperateLog(type = OperaterType.INSERT, desc = "字典新增")
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public JsonResult<Object> saveDictionary(@RequestBody @Validated Dictionary dictionary) {
		boolean flag = dictionaryService.checkDictionaryValue(dictionary, 1);
		boolean nameFlag = dictionaryService.checkDictionaryName(dictionary,1);
		if(!flag) {
			return execCheckError("字典值重复！");
		}
		if(!nameFlag) {
			return execCheckError("字典名称重复！");
		}
		dictionaryService.saveDictionary(dictionary);
		return execSuccess(Constant.SAVE_SUCCESS_MSG);
	}

	/**
	 * 字典编辑
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典编辑", notes = "字典编辑")
	@OperateLog(type = OperaterType.UPDATE, desc = "字典编辑")
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public JsonResult<Object> editDictionary(@RequestBody @Validated Dictionary dictionary) {
		if(null==dictionary.getId()) {
			return execCheckError("字典id为空！");
		}
		boolean flag = dictionaryService.checkDictionaryValue(dictionary, 0);
		boolean nameFlag = dictionaryService.checkDictionaryName(dictionary,0);
		if(!flag) {
			return execCheckError("字典值重复！");
		}
		if(!nameFlag) {
			return execCheckError("字典名称重复！");
		}
		dictionaryService.editDictionary(dictionary);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}

	/**
	 * 字典删除
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典删除", notes = "字典删除")
	@OperateLog(type = OperaterType.DELETE, desc = "字典删除")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public JsonResult<Object> deleteDictionary(@RequestBody Dictionary dictionary) {
		if(null==dictionary.getId()) {
			return execCheckError("字典id为空！");
		}
		Integer id = dictionary.getId();
		dictionaryService.deleteDictionary(id);
		return execSuccess(Constant.DELETE_SUCCESS_MSG);
	}

	/**
	 * 字典状态修改
	 * 
	 * @param dictionaryType
	 * @return
	 */
	@ApiOperation(value = "字典状态修改", notes = "字典状态修改")
	@OperateLog(type = OperaterType.UPDATE, desc = "字典状态修改")
	@RequestMapping(value = "/active", method = RequestMethod.POST)
	public JsonResult<Object> updateActive(@RequestBody Dictionary dictionary) {
		if(null==dictionary.getId()) {
			return execCheckError("字典id为空！");
		}
		dictionaryService.updateActive(dictionary);
		return execSuccess(Constant.EDIT_SUCCESS_MSG);
	}
}
