package com.greattimes.ev.system.controller;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.MethodCost;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.ConfigConstants;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.entity.UserLogs;
import com.greattimes.ev.system.service.IMenuService;
import com.greattimes.ev.system.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class LoginController extends BaseController{
	
	Logger log=LoggerFactory.getLogger(this.getClass());

	@Autowired
	private IUserService userService;
	@Autowired
	private IMenuService menuService;

	@Autowired
	private ConfigurationCache cnfCache;

	/**
	 * 无效的用户角色
	 */
	private static final Integer INVALID_ROLETYPE = 3;
	/**
	 * 用户认证
	 *
	 * @param req
	 * @param model
	 * @param user
	 * @return
	 */
	@MethodCost(desc="用户认证")
	@RequestMapping(value = "/auth", method = RequestMethod.POST)
	public String login(HttpServletRequest req, RedirectAttributes model,  @RequestBody User user) {
		try{
			Map<String, Object> columnMap = new HashMap<String, Object>();
			columnMap.put("loginName", user.getLoginName());
			user.setActive(1);
			User u = userService.getUser(user);
			if(u!=null){
				List<Map<String, Object>> menuList=userService.getUserMenu(0, u.getId());
				req.getSession().setAttribute("menu", menuList);
				setSessionUser(req, u);
				log.info( "登录操作：【{}】 登录成功！",user.getLoginName());
			}else{
				User us=new User();
				us.setLoginName(user.getLoginName());
				us.setActive(1);
				User uss = userService.getUser(us);
				log.info( "登录操作：【{}】 登录失败 ",user.getLoginName());
				model.addFlashAttribute("name", user.getLoginName());
				if(uss==null){
					model.addFlashAttribute("msg1", "用户不存在！");
				}else{
					model.addFlashAttribute("msg2", "用户密码错误！");
				}
				return "redirect:/login";
			}
			return "system/home";
		}catch(Exception e){
			log.error("登录异常",e.getMessage());
			model.addAttribute("msg3", "登录异常！");
			return "redirect:/login";
		}
	}


    /**
     *  用户认证
     * @param req
     * @param user
     * @return
     */
	@MethodCost(desc="用户认证")
	@RequestMapping(value = "/user/auth", method = RequestMethod.POST)
	@ResponseBody
	public JsonResult<Object> auth(HttpServletRequest req, @RequestBody User user) {
		UserLogs userlog=new UserLogs();
		try{
			Map<String, Object> columnMap = new HashMap<String, Object>();
			columnMap.put("login_name", user.getLoginName());
			user.setActive(1);
			//校验参数
			String loginName = user.getLoginName();
			if(loginName == null){
				return execCheckError("用户名不能为空！");
			}
			Map<String, Object> map = new HashMap<>(3);
			map.put("loginName", user.getLoginName());
			List<User> userList = userService.selectByMap(map);
			if(evUtil.listIsNullOrZero(userList)){
				log.info( "登录操作：【{}】 登录失败 ",user.getLoginName());
				userlog.setDesc("登录操作：【"+user.getLoginName()+"】 登录失败！用户不存在！");
				userlog.setModule("login");
				userlog.setState(1);
				userlog.setTime(new Date(System.currentTimeMillis()));
				userlog.setType(0);
				userlog.setUser(user.getLoginName());
				userlog.setRoleType(INVALID_ROLETYPE);
				userService.insertUserLogs(userlog);
				return execCheckError("用户不存在！");
			}
			User originUser = userList.get(0);
			if(originUser.getActive() != 1){
				Role role=userService.getUserRole(originUser.getId());
				if(null!=role) {
					user.setRole(role);
				}
				log.info( "登录操作：【{}】 登录失败 ",user.getLoginName());
				userlog.setDesc("登录操作：【"+user.getLoginName()+"】 登录失败！该用户未激活！");
				userlog.setModule("login");
				userlog.setState(1);
				userlog.setTime(new Date(System.currentTimeMillis()));
				userlog.setType(0);
				userlog.setUser(user.getLoginName());
				userlog.setRoleType(user.getRole()==null?INVALID_ROLETYPE:user.getRole().getType());
				userService.insertUserLogs(userlog);
				return execCheckError("该用户未激活！");
			}
			//是否为AD域校验
			int type = originUser.getType();
			Map<String, String> paramMap = new HashMap<>(5);

//			String has_ad_verification = cnfCache.getValue(ConfigConstants.HAS_AD_VERIFICATION);
			String has_ad_verification = cnfCache.getDefaultValue(ConfigConstants.HAS_AD_VERIFICATION);
			//AD域,判断是否开启ad域校验,项目的版本号1.0含有ad域
			if(type == 1 && has_ad_verification != null && ConfigConstants.ONE_STRING.equals(has_ad_verification)){
				paramMap.put(ConfigConstants.AD_HOST, cnfCache.getValue(ConfigConstants.AD_HOST));
				paramMap.put(ConfigConstants.AD_DOMAIN,cnfCache.getValue(ConfigConstants.AD_DOMAIN));
				paramMap.put(ConfigConstants.AD_PORT,cnfCache.getValue(ConfigConstants.AD_PORT));
				paramMap.put(ConfigConstants.AD_SEARCHBASE,cnfCache.getValue(ConfigConstants.AD_SEARCHBASE));
				log.info("ad域校验所需参数Map:{}",paramMap);
			}
			originUser.setPassword(user.getPassword());
			User u = userService.checkUser(originUser, paramMap);
			if(u != null){
				List<Map<String, Object>> menuList = menuService.getAllRouters(u);
				Role role=userService.getUserRole(u.getId());
				if(null!=role) {
					u.setRole(role);
				}
				req.getSession().setAttribute("menu", menuList);
				setSessionUser(req, u);
				log.info( "登录操作：【{}】 登录成功！",user.getLoginName());
				//userlogs
				userlog.setDesc("登录操作：【"+user.getLoginName()+"】 登录成功！");
				userlog.setModule("login");
				userlog.setState(0);
				userlog.setTime(new Date(System.currentTimeMillis()));
				userlog.setType(0);
				userlog.setUser(user.getLoginName());
				userlog.setRoleType(u.getRole()==null?INVALID_ROLETYPE:u.getRole().getType());
				userService.insertUserLogs(userlog);
				columnMap.clear();
				columnMap.put("menu", menuList);
				columnMap.put("user", u);
				columnMap.put("tooken", req.getSession().getId());
				columnMap.put("SESSION", req.getSession().getId());
				return execSuccess(columnMap);
			}else{
				User us=new User();
				us.setLoginName(user.getLoginName());
				us.setActive(1);
				User uss = userService.getUser(us);
				log.info( "登录操作：【{}】 登录失败 ",user.getLoginName());
				userlog.setModule("login");
				userlog.setState(1);
				userlog.setTime(new Date(System.currentTimeMillis()));
				userlog.setType(0);
				userlog.setUser(user.getLoginName());
				if(uss==null){
					userlog.setRoleType(user.getRole()==null?INVALID_ROLETYPE:user.getRole().getType());
					userlog.setDesc("登录操作：【"+user.getLoginName()+"】 登录失败！用户不存在！");
					userService.insertUserLogs(userlog);
					return execCheckError("用户不存在！");
				}else{
					Role role=userService.getUserRole(uss.getId());
					if(null!=role) {
						user.setRole(role);
					}
					userlog.setRoleType(user.getRole()==null?INVALID_ROLETYPE:user.getRole().getType());
					userlog.setDesc("登录操作：【"+user.getLoginName()+"】 登录失败！用户密码错误！");
					userService.insertUserLogs(userlog);
					return execCheckError("用户密码错误！");
				}
			}
		}catch(Exception e){
			log.error("登录异常",e.getMessage());
			userlog.setDesc("登录操作：【"+user.getLoginName()+"】 登录异常！"+e.getMessage());
			userlog.setModule("login");
			userlog.setState(1);
			userlog.setTime(new Date(System.currentTimeMillis()));
			userlog.setType(0);
			userlog.setUser(user.getLoginName());
			userlog.setRoleType(INVALID_ROLETYPE);
			userService.insertUserLogs(userlog);
			return returnError("登录异常!");
		}
	}
	
	
	@RequestMapping(value={"/index","/login" ,"/",""},method=RequestMethod.GET)
	public String index(@ModelAttribute("msg1")String msg1,@ModelAttribute("msg2")String msg2,@ModelAttribute("msg3")String msg3){
		return "system/login";
	}
	
	
	
	@RequirePermission("bpm")
	@MethodCost(desc="测试")
	@OperateLog(desc="测试" ,type=OperaterType.SELECT)
	@RequestMapping(value="test",method=RequestMethod.GET)
	@ResponseBody
	public JsonResult<Object> Test(HttpServletRequest req){
		
		System.out.println("==>>>>>>>"+req.getRequestURI());
		User user=new User();
		user.setActive(1);
		user.setLoginName("admin");
		User u = userService.getUser(user);
		log.info(cnfCache.getValue("linedays"));
		return execSuccess(u);
	}
	
	
	@RequirePermission("BPM")
	@MethodCost(desc="测试")
	@OperateLog(desc="测试" ,type=OperaterType.SELECT)
	@RequestMapping(value="test2/{id}",method=RequestMethod.GET)
	@ResponseBody
	public JsonResult<Object> Test2(HttpServletRequest req,@PathVariable int id){
		System.out.println("==>>>>>>>"+req.getRequestURI());
		System.out.println("id==>>>>>>>"+id);
		User user=new User();
		user.setActive(1);
		user.setLoginName("admin");
		User u = userService.getUser(user);
		log.info(cnfCache.getValue("linedays"));
		return execSuccess(u);
	}
	
	/**
	 * 退出
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseBody
	public JsonResult<Object> logout(HttpServletRequest req) {
		User user=getSessionUser(req);
		req.getSession().invalidate();
		UserLogs userlog=new UserLogs();
		if(user != null){
			userlog.setDesc("退出登陆操作：【"+user.getLoginName()+"】 退出成功！");
			userlog.setModule("login");
			userlog.setState(0);
			userlog.setTime(new Date(System.currentTimeMillis()));
			userlog.setType(1);
			userlog.setUser(user.getLoginName());
			userlog.setRoleType(user.getRole()==null?INVALID_ROLETYPE:user.getRole().getType());
			userService.insertUserLogs(userlog);
		}
		return execSuccess("退出成功！");
	}
	
	/**
	 * 
	 * @param req
	 * @return
	 */
	@RequestMapping(value = "/unpermission", method = RequestMethod.GET)
	public String unPermission(HttpServletRequest req) {
		req.getSession().invalidate();
		return "system/unpermission";
	}
	
	@ResponseBody
	@RequestMapping(value = "/user/permission/{pid}", method = RequestMethod.GET)
	public JsonResult<Object> userPermission(HttpServletRequest req ,@PathVariable int pid) {
		List<Map<String, Object>> menuList=userService.getUserMenu(0, getSessionUser(req).getId());
		return execSuccess(menuList);
	}
}
