package com.greattimes.ev.system.controller;


import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.cache.redis.MenuCache;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Role;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.service.IRoleMenuService;
import com.greattimes.ev.system.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * @author NJ
 * @date 2018/5/14 9:44
 */
@RestController
@RequestMapping("/system/role")
@Api(value="RoleController",tags="角色管理")
@RequirePermission("role")
public class RoleController extends BaseController{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private  IRoleService iRoleService;
    @Autowired
    private  IRoleMenuService iRoleMenuService;
    @Autowired
    private  MenuCache menuCache;

    /***
     * 获取角色列表
     * @author NJ
     * @date 2018/5/24 16:55
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("common")
    @ApiOperation(value="获取角色列表", notes="获取角色列表", response = Role.class, responseContainer = "List")
    @RequestMapping(value="", method = RequestMethod.GET)
    public JsonResult<Object> getMenuList(){
        return execSuccess(iRoleService.findList());
    }

    /***
     * 删除角色以及角色相关菜单权限
     * @author NJ
     * @date 2018/5/24 16:56
     * @param req
     * @param p
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="删除角色", notes="删除角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="角色id",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.DELETE, desc = "删除角色")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
    public JsonResult<Object> deleteRoleAndRoleMenu(HttpServletRequest req,@RequestBody JSONObject p){
        Integer id = p.getIntValue("id");
        if(id == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        Role role = iRoleService.selectRoleById(id);
        if(role == null){
            return execCheckError(Constant.HAS_NO_RECORDS);
        }
        if(role.getIsDefault() == 1){
            return execCheckError("系统内置角色不能删除！");
        }
        int num = iRoleService.deleteRoleMenus(id);
        log.debug("角色、权限删除成功！ 更新条数" + num);
        menuCache.initCache();
        return execSuccess(Constant.DELETE_SUCCESS_MSG);
    }
    /**
     * 编辑角色
     * @author NJ
     * @date 2018/5/24 16:58
     * @param req
     * @param p
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="编辑角色", notes="编辑角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="角色id",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="name",value="角色名称",dataType="string", paramType = "query"),
            @ApiImplicitParam(name="type",value="角色类型",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.UPDATE, desc = "编辑角色")
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public JsonResult<Object> editMenu(HttpServletRequest req,@RequestBody JSONObject p){
        Integer id = p.getInteger("id");
        String name = p.getString("name");
        Integer type = p.getInteger("type");
        //拼接错误提示信息
        StringBuilder sb = new StringBuilder("");
        if(id == null){sb.append("角色id").append("，");}
        if(StringUtils.isBlank(name)){sb.append("角色名称").append("，");}
        if(!"".equals(sb.toString())){
            sb.deleteCharAt(sb.length()-1);
            return execCheckError(sb.append("不能为空！").toString());
        }
        Role role = iRoleService.selectRoleById(id);
        if(role == null) {
            return returnError("该角色不存在！");
        }
        if(iRoleService.isHasSameName(id,name))  {
            return returnError("角色名称不能重复！");
        }
        role.setName(name);
        role.setType(type);
        // 不是系统默认角色
        role.setIsDefault(0);
        iRoleService.saveOrUpdateRole(role);
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }
    /**
     * 新增角色
     * @author NJ
     * @date 2018/5/24 16:59
     * @param req
     * @param p
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="新增角色", notes="新增角色")
    @ApiImplicitParams({
            @ApiImplicitParam(name="name",value="角色名称",dataType="string", paramType = "query"),
            @ApiImplicitParam(name="type",value="角色类型",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.INSERT, desc = "新增角色")
    @RequestMapping(value="/add", method = RequestMethod.POST)
    public JsonResult<Object> addMenu(HttpServletRequest req,@RequestBody JSONObject p){
        String name = p.getString("name");
        Integer type = p.getInteger("type");
        StringBuilder sb = new StringBuilder("");
        if(type == null){sb.append("角色类型").append("，");}
        if(StringUtils.isBlank(name)){sb.append("角色名称").append("，");}
        if(!"".equals(sb.toString())){
            sb.deleteCharAt(sb.length() - 1);
            return execCheckError(sb.append("不能为空！").toString());
        }
        if(iRoleService.isHasSameName(null,name)){
            return execCheckError("角色名称不能重复！");
        }
        Role role = new Role();
        role.setName(name);
        role.setType(type);
        role.setEname("role");
        role.setIsDefault(0);
        iRoleService.saveOrUpdateRole(role);
        return execSuccess(Constant.SAVE_SUCCESS_MSG);
    }
    /**
     * 角色权限保存
     * @author NJ
     * @date 2018/5/24 16:59
     * @param req
     * @param map
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="角色权限保存", notes="角色权限保存")
    @ApiImplicitParams({
            @ApiImplicitParam(name="roleId",value="角色id",dataType="int", paramType = "query"),
            @ApiImplicitParam(name="menuId",value="菜单id数组",dataType = "int", allowMultiple = true, paramType = "query")})
    @OperateLog(type = OperaterType.INSERT, desc = "角色权限保存")
    @RequestMapping(value="/promission", method = RequestMethod.POST)
    public JsonResult<Object> promissionMenu(HttpServletRequest req,@RequestBody Map<String,Object> map){
        Integer roleId = evUtil.getMapIntValue(map,"roleId");
        List<Integer> menuIds = (List<Integer>)map.get("menuId");
        if(roleId == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        Role role = iRoleService.selectRoleById(roleId);
        if(role == null){
            return execCheckError(Constant.HAS_NO_RECORDS);
        }
        iRoleMenuService.saveRoleMenus(roleId, menuIds);
        menuCache.initCache();
        return execSuccess(Constant.SAVE_SUCCESS_MSG);
    }

    /**
     * 获取角色
     * @author NJ
     * @date 2018/5/24 16:59
     * @param req
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @RequirePermission("common")
    @ApiOperation(value="获取角色", notes="获取角色", response = Role.class, responseContainer = "List")
    @RequestMapping(value="/getrole",method = RequestMethod.GET)
    public JsonResult<Object>  findRoleBySession(HttpServletRequest req){
        User user = super.getSessionUser(req);
        Integer id = user.getId();
        return execSuccess(iRoleService.findRoleByUserId(id));
}

}
