package com.greattimes.ev.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.service.notice.INoticeService;
import com.greattimes.ev.cache.redis.ConfigurationCache;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Configuration;
import com.greattimes.ev.system.param.req.ConfigurationParam;
import com.greattimes.ev.system.service.IConfigurationService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author NJ
 * @date 2018/5/10 12:44
 * todo: 1、增删改未加入事物(redis && mysql) 2、redis 操作key异常未捕捉未提示给前端 3、可考虑提供后门接口:重新加载db到redis接口,防止数据不一致
 */
@RequirePermission("param")
@RestController
@RequestMapping("/system/param")
@Api(value="ConfigurationController",tags="配置管理")
public class ConfigurationController extends BaseController{

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private  IConfigurationService iConfigurationService;

    @Autowired
    private INoticeService noticeService;

    /**
     * redisCache
     */
    @Autowired
    private  ConfigurationCache configurationCache;


    /**
     * 查询参数列表
     * @author NJ
     * @date 2018/5/10 12:50
     * @param req
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="查询参数列表", notes="查询参数列表", response = Configuration.class, responseContainer = "List")
    @RequestMapping(value="", method = RequestMethod.GET)
    public JsonResult<Object> getConfList(HttpServletRequest req){
        return execSuccess(iConfigurationService.queryConfiguration(null));
    }


    /**
     * 根据id删除参数
     * @author NJ
     * @date 2018/5/10 12:56
     * @param req
     * @param p
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="根据id删除参数", notes="根据id删除参数")
    @ApiImplicitParams({
            @ApiImplicitParam(name="id",value="配置参数id",dataType="int", paramType = "query")})
    @OperateLog(type = OperaterType.DELETE, desc = "删除参数配置")
    @RequestMapping(value="/delete", method = RequestMethod.POST)
    public JsonResult<Object> deleteConf(HttpServletRequest req,@RequestBody JSONObject p){
        int id = p.getIntValue("id");
        Configuration configuration = iConfigurationService.delete(id);
        if(configuration == null){
            return execCheckError(Constant.HAS_NO_RECORDS);
        }
        configurationCache.delete(configuration.getKey());
        //sys_notice
        noticeService.insertNoticeByParam(2502,"参数管理删除",1,0);
        log.info("参数管理key:{},从redis缓存中移除成功！", configuration.getKey());
        return execSuccess(Constant.DELETE_SUCCESS_MSG);
    }

    /**
     * 编辑参数配置
     * @author NJ
     * @date 2018/5/10 14:12
     * @param configurationParam
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="编辑参数配置", notes="编辑参数配置")
    @OperateLog(type = OperaterType.UPDATE, desc = "编辑参数配置")
    @RequestMapping(value="/edit", method = RequestMethod.POST)
    public JsonResult<Object> editConf(@RequestBody @Validated ConfigurationParam configurationParam, BindingResult result) throws InvocationTargetException, IllegalAccessException {
        if(configurationParam == null || configurationParam.getId() == null){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        if(result.hasErrors()){
            return execCheckError(this.getErrorMsg(result));
        }
        Integer id = configurationParam.getId();
        Configuration configuration = iConfigurationService.selectById(id);
        if(configuration == null){
            return execCheckError(Constant.HAS_NO_RECORDS);
        }
        String oldKey = configuration.getKey();
        String newKey = configurationParam.getKey();
        //验证key是否重复
        if(iConfigurationService.isHasSameKey(newKey,id)){
            return execCheckError("参数配置key不能重复！");
        }
        Integer active = configuration.getActive();
        /**
         * 弃用org.apache.commons.beanutils.BeanUtils包...(dest,src) api
         * 改为使用org.springframework.beans.BeanUtils;(src,dest) 原因效率问题
         */
        BeanUtils.copyProperties(configurationParam, configuration);
        //active字段复制忽略
        configuration.setActive(active);
        iConfigurationService.update(configuration);
        //sys_notice
        noticeService.insertNoticeByParam(2503,"参数管理编辑",1,0);
        //key发生变化
        if(!oldKey.equals(newKey)){
            configurationCache.delete(oldKey);
            configurationCache.add(newKey, configuration);
        }else{
            configurationCache.update(newKey, configuration);
        }
        log.info("参数管理key:{},从redis缓存更新成功！",configuration.getKey());
        return execSuccess(Constant.EDIT_SUCCESS_MSG);
    }

    /**
     * 增加参数
     * @author NJ
     * @date 2018/5/10 14:15
     * @param req
     * @param configurationParam
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="增加参数", notes="增加参数")
    @OperateLog(type = OperaterType.INSERT, desc = "增加参数配置")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public JsonResult<Object> addConf(HttpServletRequest req, @RequestBody @Validated ConfigurationParam configurationParam, BindingResult result) throws InvocationTargetException, IllegalAccessException {
        if(result.hasErrors()){
            return returnError(this.getErrorMsg(result));
        }
        //key重复校验
        String key = configurationParam.getKey();
        if(iConfigurationService.isHasSameKey(key,null)){
            return execCheckError("参数配置key不能重复！");
        }
        Configuration configuration = new Configuration();
        BeanUtils.copyProperties(configurationParam,configuration);
        configuration.setId(null);
        Integer affectRows = iConfigurationService.save(configuration);
        if(affectRows > 0){
            //sys_notice
            noticeService.insertNoticeByParam(2501,"参数管理新增",1,0);
            configurationCache.add(key, iConfigurationService.findConfigurationByKey(key));
            log.info("参数管理key:{},从redis缓存添加成功！",configuration.getKey());
            return execSuccess(Constant.SAVE_SUCCESS_MSG);
        }else{
            return execSuccess(Constant.SAVE_ERROR_MSG);
        }
    }

    /***
     * 修改激活状态
     * @author NJ
     * @date 2018/5/24 15:45
     * @param req
     * @param id
     * @param state
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="修改激活状态", notes="修改激活状态")
    @OperateLog(type = OperaterType.UPDATE, desc = "修改激活状态")
    @RequestMapping(value="/{id}/{state}",method=RequestMethod.GET)
    public JsonResult<Object> updateParameterState(HttpServletRequest req, @ApiParam(required = true, name = "id", value = "配置id") @PathVariable int id,
                   @ApiParam(required = true, name = "state", value = "激活状态") @PathVariable int state){
        Configuration configuration = iConfigurationService.updateConfigurationState(id,state);
        if(configuration == null ){
            return execCheckError(Constant.HAS_NO_RECORDS);
        }
        //更新缓存
        configurationCache.update(configuration.getKey(), configuration);
        log.info("参数管理key:{},从redis缓存更新状态成功！", configuration.getKey());
        return execSuccess(Constant.STATE_SUCCESS_MSG);
    }

    /**    
     *  拼接error字符串
     * @author NJ  
     * @date 2018/5/10 16:22
     * @param result  
     * @return java.lang.String  
     */  
    private String getErrorMsg(BindingResult result){
        List<ObjectError> errorsList = result.getAllErrors();
        StringBuilder sb = new StringBuilder();
        errorsList.forEach(error -> sb.append(error.getDefaultMessage()).append("\n"));
        return sb.toString();
    }

}
