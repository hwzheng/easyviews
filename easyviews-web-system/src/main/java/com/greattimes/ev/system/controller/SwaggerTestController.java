package com.greattimes.ev.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.system.entity.User;
import com.greattimes.ev.system.service.IUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@Api(value="SwaggerTestController",tags="测试模块")
@RequestMapping(value="swagger")
public class SwaggerTestController  extends BaseController{
	
	
	@ApiOperation(value="测试", notes="测试接口" , response =User.class ,responseContainer="List")
	@RequestMapping(value="test", method=RequestMethod.POST )
	public JsonResult<User> test() {
		User u=new User();
		u.setLoginName("admin");
		JsonResult<User> jt=new JsonResult<User>();
		jt.setCode(SUC_CODE);
		jt.setData(u);
		return jt;
	}
	
	@ApiOperation(value="测试", notes="测试接口2")
	@RequestMapping(value="test2", method=RequestMethod.POST )
	
	public JsonResult<Object> test2(@RequestBody User user) {
		User u=new User();
		u.setLoginName("admin");
		/*JsonResult<User> jt=new JsonResult<User>();
		jt.setCode(SUC_CODE);
		jt.setData(u);*/
		return execSuccess(u);
	}
	
	@ApiOperation(value="测试", notes="测试接口3")
	@RequestMapping(value="test3", method=RequestMethod.POST )
	@ApiImplicitParams({
		  @ApiImplicitParam(name="name",value="用户名",dataType="string", paramType = "query",example="xingguo"),
		  @ApiImplicitParam(name="id",value="用户id",dataType="long", paramType = "query")})
	public JsonResult<Object> test3() {
		User u=new User();
		u.setLoginName("admin");
		/*JsonResult<User> jt=new JsonResult<User>();
		jt.setCode(SUC_CODE);
		jt.setData(u);*/
		return execSuccess(u);
	}
	
}
