package com.greattimes.ev.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.system.service.IMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.greattimes.ev.base.BaseController;


/**
 * @author NJ
 * @date 2018/5/24 17:00
 */
@RestController
@RequestMapping("/system")
@Api(value="MenuController",tags="菜单管理")
public class MenuController extends BaseController {

    @Autowired
    private  IMenuService iMenuService;

    /**
     * 根据roleid 初始化菜单
     * @author NJ
     * @date 2018/5/24 17:00
     * @param p
     * @return com.greattimes.ev.common.model.JsonResult<java.lang.Object>
     */
    @ApiOperation(value="根据角色初始化菜单", notes="根据角色初始化菜单")
    @ApiImplicitParams({
            @ApiImplicitParam(name="roleId",value="角色id",dataType="int", paramType = "query")})
    @RequestMapping(value="/menus", method = RequestMethod.POST)
    public JsonResult<Object> initMenus(@RequestBody JSONObject p){
        Integer roleId = p.getInteger("roleId");
        //角色校验
        //if(!iRoleService.isHasRoleById(roleId)) return returnError("角色不存在！");
        return execSuccess(iMenuService.findRoleMenuTree(roleId));
    }
}
