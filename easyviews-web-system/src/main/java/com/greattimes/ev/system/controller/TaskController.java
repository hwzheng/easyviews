package com.greattimes.ev.system.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.plugins.Page;
import com.greattimes.ev.base.BaseController;
import com.greattimes.ev.base.Constant;
import com.greattimes.ev.bpm.service.alarm.IAlarmMassageProducerService;
import com.greattimes.ev.common.annotation.OperateLog;
import com.greattimes.ev.common.constants.OperaterType;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.common.utils.evUtil;
import com.greattimes.ev.permission.RequirePermission;
import com.greattimes.ev.system.entity.Task;
import com.greattimes.ev.system.param.req.TaskParam;
import com.greattimes.ev.system.service.ITaskService;
import com.greattimes.ev.task.utils.QuartzManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 定时任务表 前端控制器
 * </p>
 *
 * @author NJ
 * @since 2018-07-17
 */
@RestController
@RequestMapping("/system/task")
@Api(value="TaskController",tags="定时任务")
@RequirePermission("timer")
public class TaskController extends BaseController{

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IAlarmMassageProducerService iAlarmMassageProducerService;


    private Logger logger = LoggerFactory.getLogger(TaskController.class);

    @ApiOperation(value = "定时任务列表", notes = "定时任务列表", response = TaskParam.class, responseContainer = "List")
    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public JsonResult<Object> select(){
        return execSuccess(taskService.selectListByMap(null));
    }


    @ApiOperation(value = "定时任务激活", notes = "定时任务激活")
    @ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "定时任务id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "active", value = "是否激活 0否 1是", dataType = "int", paramType = "query")
    })
    @OperateLog(type = OperaterType.UPDATE, desc = "定时任务激活状态修改")
    @RequestMapping(value = "/active", method = RequestMethod.POST)
    public JsonResult<Object> active(@RequestBody JSONObject param){
        Integer active = param.getInteger("active");
        if(active == null || active.intValue() > 1 || active.intValue() < 0){
            return execCheckError(Constant.PARAMETER_ERROR_MSG);
        }
        int id = param.getIntValue("id");
        Task task = taskService.selectById(id);

        if(active == 1){
            try {
                QuartzManager.addJob(task);
                iAlarmMassageProducerService.resetMessageMap();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                logger.error("定时任务启动失败", e);
                return execCheckError(Constant.STATE_ERROR_MSG+"原因：定时任务启动失败！");
            }
            logger.info("启动定时任务成功，参数为："+task);
        }else if(active == 0){
            //关闭
            logger.info("关闭定时任务，参数为："+task);
            QuartzManager.removeJob(task);
            //重置map
            iAlarmMassageProducerService.resetMessageMap();
        }

        Task entity = new Task();
        entity.setActive(active);
        entity.setId(id);
        if(taskService.updateById(entity)){
            logger.info("更新定时任务状态成功!参数为："+task);
            return execSuccess(Constant.STATE_SUCCESS_MSG);
        }else{
            logger.info("更新定时任务状态失败!参数为："+task);
            return execCheckError(Constant.STATE_ERROR_MSG);
        }

    }

    @ApiOperation(value = "定时任务详情", notes = "定时任务详情",  response = TaskParam.class)
    @ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "定时任务id", dataType = "int", paramType = "query")})
    @RequestMapping(value = "/detail", method = RequestMethod.POST)
    public JsonResult<Object> detail(@RequestBody JSONObject param){
        Map<String, Object> map = new HashMap<>(1);
        map.put("id",param.getString("id"));
        List<TaskParam> result = taskService.selectListByMap(map);
        if(evUtil.listIsNullOrZero(result)){
            return execCheckError(Constant.HAS_NO_RECORDS);
        }else{
            return execSuccess(result.get(0));
        }
    }


    @ApiOperation(value = "定时任务编辑", notes = "定时任务编辑",  response = TaskParam.class)
    @ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "定时任务id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "cron", value = "表达式", dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "param", value = "参数", dataType = "String", paramType = "query")
    })
    @OperateLog(type = OperaterType.UPDATE, desc = "定时任务修改")
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public JsonResult<Object> edit(@RequestBody JSONObject param){
        int id = param.getIntValue("id");
        String cron = param.getString("cron");
        String para = param.getString("param");
        Task taskOld = taskService.selectById(id);
        taskOld.setCron(cron);
        taskOld.setParam(para);
        if(taskOld.getActive().equals(1)){
            //如果只更改了cron表达式，则修改触发时间
            if(!taskOld.getCron().equals(cron) && isParamsEqual(taskOld.getParam(),para)){
                logger.info("更改定时任务时间，参数为"+taskOld);
                QuartzManager.modifyJobTime(taskOld);
                //如果修改了jobName，class_name或者params，则先移除再添加
            }else if(!isParamsEqual(taskOld.getParam(),para)){
                logger.info("更改定时任务名称、处理类或者参数，方法参数为："+taskOld);
                QuartzManager.removeJob(taskOld);
                try{
                    QuartzManager.addJob(taskOld);
                }catch(ClassNotFoundException e){
                    //将任务状态置为关闭
                    taskOld.setActive(0);
                    taskService.updateById(taskOld);
                    e.printStackTrace();
                    logger.error("编辑定时任务信息出错，参数："+taskOld, e);
                    return execCheckError("编辑定时任务信息出错！");
                }
            }
        }
        Task task = new Task();
        task.setId(id);
        task.setCron(cron);
        task.setParam(para);
        if(taskService.updateById(task)){
            logger.info("保存成功，调taskManageService接口修改定时任务成功，方法参数为："+taskOld);
            return execSuccess(Constant.SAVE_SUCCESS_MSG);
        }else{
            logger.info("保存失败，修改定时任务状态值失败，方法参数为："+taskOld);
            return execSuccess(Constant.SAVE_ERROR_MSG);
        }
    }


    @ApiOperation(value = "定时任务日志列表", notes = "定时任务日志列表",  response = TaskParam.class)
    @ApiImplicitParams({ @ApiImplicitParam(name = "taskId", value = "定时任务id", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "stat", value = "执行成功状态 0:成功 ，1部分成功 ，2 执行成功 ，3 失败 ", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "截止时间", dataType = "long", paramType = "query"),
            @ApiImplicitParam(name = "pageSize", value = "每页数量", dataType = "int", paramType = "query"),
            @ApiImplicitParam(name = "pageNumber", value = "当前页码", dataType = "int", paramType = "query")
    })
    @RequestMapping(value = "/logs", method = RequestMethod.POST)
    public JsonResult<Object> selectLogs(@RequestBody JSONObject param){
        Integer pageNumber = param.getInteger("pageNumber");
        Integer pageSize= param.getInteger("pageSize");
        Page<Map<String,Object>> page = new Page<>(pageNumber,pageSize);
        Map<String,Object> map = new HashMap<>(4);
        map.put("taskId", param.getIntValue("taskId"));
        map.put("stat", param.getInteger("stat"));
        map.put("startTime", param.getLongValue("startTime"));
        map.put("endTime", param.getLongValue("endTime"));
        return execSuccess(taskService.selectLogsListByMap(page, map));
    }

    private boolean isParamsEqual(String paramsOld,String paramsNew){
        return (StringUtils.isBlank(paramsOld)&&StringUtils.isBlank(paramsNew))||paramsOld.equals(paramsNew);
    }
}

