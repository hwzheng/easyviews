package com.greattimes.ev.permission;


import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.alibaba.fastjson.JSON;
import com.greattimes.ev.cache.IMenu;
import com.greattimes.ev.common.model.JsonResult;
import com.greattimes.ev.system.entity.User;

/**
 * 认证 鉴权
 * @author LiHua
 * @since 
 */
public class Authentication extends HandlerInterceptorAdapter {
	
	org.slf4j.Logger log=LoggerFactory.getLogger(this.getClass()); 
	@Autowired
	private IMenu menu; 
	
	private final static String[] fifter= new String[]{"/login","/index","/logout","/auth","/api","/logs","/bpm/notification/check"
							,"/bpm/notification/down"};//免登陆
	private final static String [] allowDomain= {"http://192.168.0.19:8082", "http://192.168.0.19:8083","http://192.168.0.19:8080"}; //允许域名
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

//		if(request.getMethod().equals(RequestMethod.OPTIONS.name())) {
//			String str = RequestMethod.OPTIONS.name();
//			return super.preHandle(request, response, handler);
//		}

		//跨域处理
	//	log.info("seesionID>>>:{}",request.getSession().getId());
		Set<String> allowedOrigins= new HashSet<String>(Arrays.asList(allowDomain));
		String originHeader=request.getHeader("Origin");
		/*if (!allowedOrigins.contains(originHeader)){
			return false;
		}*/
	    response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, accept, content-type, token");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.setHeader("Access-Control-Allow-Origin", originHeader);
        response.setHeader("Access-Control-Allow-Credentials", "true");
//        response.setHeader("Set-Cookie", "SameSite=None;Secure");
		//校验登录
		User obj=JSON.parseObject((String)request.getSession().getAttribute("member"), User.class);// (User);
		if(!checkUser(request,obj)){
			responseNoLogin(request, response);
			//throw new Exception("user not login");
			return false;
		}
		if (!(handler instanceof HandlerMethod))
		return true;
		HandlerMethod at=(HandlerMethod) handler;
		
		//校验权限 
		int roleId=-1;
		if(obj!=null)		
		roleId=obj.getRole().getId(); 
		if(!checkPermission(at, roleId)){
			response(request, response);
			return false;
		};
		return true;
	}
	
	@Override
	public void postHandle(
			HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
	}

	/**
	 * This implementation is empty.
	 */
	@Override
	public void afterCompletion(
			HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		if(ex!=null){
			responseNoLogin(request, response);
		}
	}

	/**
	 * This implementation is empty.
	 */
	@Override
	public void afterConcurrentHandlingStarted(
			HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
	}
	
	/**
	 * 检验用户是否登录
	 * @param request
	 * @return
	 */
	private boolean checkUser(HttpServletRequest request,User obj){
		String uri=request.getRequestURI();
		for (String s : fifter)
        {
            if (uri.indexOf(s) != -1) {
            	// 如果uri中包含不过滤的uri，则不进行过滤
               return true;
            }
        }
		if(obj==null){
			return false;
		}
		return true;
	}
	
	/**
	 * 权限校验-permission校验
	 * @param at
	 * @param user
	 * @return
	 */
	private boolean checkPermission(HandlerMethod at,int roleId){
		RequirePermission rp=at.getMethodAnnotation(RequirePermission.class);
		RequirePermission rpOnBean=at.getBeanType().getAnnotation(RequirePermission.class);
		
		//方法注解属性为“common”直接放行
		if(rp!=null && "common".equals(rp.value())){
			return true;
		}
		//类注解属性 优先级高
		if(rpOnBean!=null){
			String ename=rpOnBean.value();
			String key=roleId+"."+ename;
			String permission= menu.getEname(key);
			if(permission==null){
				return false;
			}
		}
		//方法注解属性 优先级次之
		if(rp!=null){
			String ename=rp.value();
			String key=roleId+"."+ename;
			String permission= menu.getEname(key);
			if(permission==null){
				return false;
			}
		}
		return true;
	}
	/**
	 * 权限校验-url校验
	 * @param at
	 * @return
	 */
	private boolean checkUrl(HandlerMethod at,HttpServletRequest request,User user){
		RequireURL ru=at.getMethodAnnotation(RequireURL.class);
		String ctx= request.getContextPath();
		if(ru!=null){
		}
		return true;
	}	
	
	/**
	 * 无权限响应
	 */
	private void response(HttpServletRequest request,HttpServletResponse response){
		try {
			 response.setHeader("Content-type", "application/Json;charset=UTF-8");  
			 //这句话的意思，是告诉servlet用UTF-8转码，而不是用默认的ISO8859  
			 response.setCharacterEncoding("UTF-8");
			 PrintWriter writer = response.getWriter();
			 JsonResult ret=new JsonResult<>();
			 ret.setCode("1002");
			 ret.setMsg("无访问权限！");
			 writer.write(JSON.toJSONString(ret));
			 writer.flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
	
	private void responseNoLogin(HttpServletRequest request,HttpServletResponse response){
		try {
			 response.setHeader("Content-type", "application/Json;charset=UTF-8");  
			 //这句话的意思，是告诉servlet用UTF-8转码，而不是用默认的ISO8859  
			 response.setCharacterEncoding("UTF-8");
			 PrintWriter writer = response.getWriter();
			 JsonResult ret=new JsonResult<>();
			 ret.setCode("1001");
			 ret.setMsg("用户未登录！");
			 writer.write(JSON.toJSONString(ret));
			 writer.flush();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}
}
