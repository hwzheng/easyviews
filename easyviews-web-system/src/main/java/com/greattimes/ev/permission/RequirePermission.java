package com.greattimes.ev.permission;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.greattimes.ev.common.constants.PermissionRule;


@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RequirePermission {
	
	String value() default "";
	
	PermissionRule Rule() default PermissionRule.EQUAL;
	
}
