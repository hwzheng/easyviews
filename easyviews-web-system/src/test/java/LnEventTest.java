import com.greattimes.ev.common.utils.DateUtils;
import com.greattimes.ev.liaoning.service.ILiaoNingEventService;
import com.greattimes.ev.utils.SpringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author NJ
 * @date 2019/12/26 18:35
 */

//让单元测试运行于spring环境，保证拥有spring框架相关支持
@RunWith(SpringJUnit4ClassRunner.class)
//加载spring容器
@ContextConfiguration("classpath:/spring/spring-test.xml")
public class LnEventTest {

    @Autowired
    private ILiaoNingEventService liaoNingEventService;

    @Test
    public void insert() throws InterruptedException {

        String[] addrs = {"spubinq7","scmsapl1","sstsncs","sglsoths4","smpsapl1"};
//        String[] addrs = {"spubinq7","spubinq7","spubinq7","spubinq7","spubinq7"};
        //5,5,10,10,10
        int [] serIntArr = {5,4,10,9,5};
        Random random = new Random();
        /**
         * tuxedo_queue  sum(tuxTqueueSrvrCnt) = 938
         */
        List<Map<String, Object>> list = new ArrayList<>();
        while (true){
            for (int i = 0; i < 5; i++) {
                Date date = new Date();
                Map<String, Object> map = new HashMap<>();
                map.put("date", DateUtils.currentDate());
                map.put("tuxTqueueRqAddr", addrs[i].toString());
                map.put("tuxTqueueSrvrCnt", serIntArr[i]);
                map.put("tuxTqueueNqueued", random.nextInt(20));
                map.put("tuxTqueueState", "");
                map.put("tuxTqueueRqId", "");
                map.put("tuxTqueueTotNqueued", "");
                map.put("tuxTqueueTotWkQueued", "");
                map.put("tuxTqueueSource", "");
                map.put("tuxTqueueWkQueued", "");
                long time = DateUtils.longToOneMinuteTime(date.getTime()+8*60*60*1000);
                map.put("nowTime", new Date(time));
                map.put("nowTimeForLong", DateUtils.longToOneMinuteTime(date.getTime()));
                list.add(map);
            }
            liaoNingEventService.insertBatchTuxedoinfo(list);
            System.out.println("######执行辽宁事件台插入操作#####");
            Thread.sleep(40*1000);
        }
    }
    public static void main(String[] args) throws InterruptedException {
        Date date = new Date();
        System.out.println(DateUtils.format(date,"yyyy-MM-dd HH:mm:ss.SSS"));

        Long aLong = DateUtils.longToFiveMinuteTime(date.getTime());
        System.out.println(date.getTime());
        System.out.println("al:"+aLong);

        //		date                 Date,
        //		tuxTqueueRqAddr      String,
        //		tuxTqueueSrvrCnt     Int64,
        //		tuxTqueueNqueued     Int64,
        //		tuxTqueueState       String,
        //		tuxTqueueRqId        String,
        //		tuxTqueueTotNqueued  String,
        //		tuxTqueueTotWkQueued String,
        //		tuxTqueueSource      String,
        //		tuxTqueueWkQueued    String,
        //		nowTime              DateTime,
        //		nowTimeForLong       Int64
    }
}


